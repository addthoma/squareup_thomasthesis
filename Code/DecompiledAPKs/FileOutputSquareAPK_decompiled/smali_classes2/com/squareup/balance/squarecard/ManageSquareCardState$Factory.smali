.class public final Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
.super Ljava/lang/Object;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nManageSquareCardState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ManageSquareCardState.kt\ncom/squareup/balance/squarecard/ManageSquareCardState$Factory\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,195:1\n180#2:196\n56#3:197\n*E\n*S KotlinDebug\n*F\n+ 1 ManageSquareCardState.kt\ncom/squareup/balance/squarecard/ManageSquareCardState$Factory\n*L\n163#1:196\n163#1:197\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;",
        "",
        "orderCardStarter",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
        "orderedStarter",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
        "activatedWorkflow",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
        "cancelSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
        "addPhoneNumberWorkflow",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;",
        "(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V",
        "deserializeState",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "startAddPhoneNumber",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;",
        "startCancelingActivatedCard",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "startCancelingOrderedCard",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;",
        "startCardActivated",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;",
        "startCardOrdered",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;",
        "startOrderingCard",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;",
        "skipInitialScreens",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activatedWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

.field private final addPhoneNumberWorkflow:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

.field private final cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

.field private final orderCardStarter:Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;

.field private final orderedStarter:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderCardStarter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderedStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activatedWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelSquareCardWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addPhoneNumberWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderCardStarter:Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderedStarter:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->activatedWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->addPhoneNumberWorkflow:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    return-void
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ManageSquareCardState;
    .locals 3

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 196
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 164
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 165
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;-><init>(Z)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    return-object v0

    .line 169
    :cond_0
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    return-object p1

    .line 171
    :cond_1
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderCardStarter:Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;

    invoke-interface {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;->adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Z)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    return-object v0

    .line 175
    :cond_2
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 176
    new-instance p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->addPhoneNumberWorkflow:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    return-object p1

    .line 197
    :cond_3
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    .line 179
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    .line 181
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderedStarter:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;

    invoke-interface {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;->adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 182
    :cond_4
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->activatedWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 184
    :cond_5
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 185
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 187
    :cond_6
    const-class v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 188
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    :goto_0
    return-object v0

    .line 190
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startAddPhoneNumber()Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;
    .locals 2

    .line 159
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->addPhoneNumberWorkflow:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    return-object v0
.end method

.method public final startCancelingActivatedCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;
    .locals 2

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-object v0
.end method

.method public final startCancelingOrderedCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;
    .locals 2

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->cancelSquareCardWorkflow:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-object v0
.end method

.method public final startCardActivated(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;
    .locals 2

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->activatedWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-object v0
.end method

.method public final startCardOrdered(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;
    .locals 2

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderedStarter:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;

    invoke-interface {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;->adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-object v0
.end method

.method public final startOrderingCard(Z)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;
    .locals 2

    .line 139
    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->orderCardStarter:Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;

    invoke-interface {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;->adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Z)V

    return-object v0
.end method
