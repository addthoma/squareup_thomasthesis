.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;
.super Ljava/lang/Object;
.source "AuthSquareCardReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->checkIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "it",
        "",
        "apply",
        "(Ljava/lang/Long;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Long;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$getBizbankService$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)Lcom/squareup/balance/core/server/bizbank/BizbankService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->checkIdvStatus(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v1

    sget-object v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$parseIdvStatus(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;->apply(Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
