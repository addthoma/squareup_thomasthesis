.class final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;
.super Ljava/lang/Object;
.source "RealOrderSquareCardStampRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->fetchStamps()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderSquareCardStampRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderSquareCardStampRequester.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "kotlin.jvm.PlatformType",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_3

    .line 56
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;

    .line 58
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;->stamps:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 60
    sget-object p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;->INSTANCE:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;

    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    goto :goto_2

    .line 62
    :cond_2
    new-instance v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;->stamps:Ljava/util/List;

    const-string v1, "response.stamps"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;-><init>(Ljava/util/List;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    goto :goto_2

    .line 65
    :cond_3
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_4

    sget-object p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;->INSTANCE:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;

    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    :goto_2
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object p1

    return-object p1
.end method
