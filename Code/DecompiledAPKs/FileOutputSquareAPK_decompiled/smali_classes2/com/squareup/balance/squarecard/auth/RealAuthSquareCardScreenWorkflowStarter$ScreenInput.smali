.class final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;
.super Ljava/lang/Object;
.source "RealAuthSquareCardScreenWorkflowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScreenInput"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\tR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\tR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\tR\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\tR\u0017\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\tR\u0017\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\tR\u0017\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\t\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
        "",
        "reactor",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "(Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "cancel",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog$Event;",
        "getCancel",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "error",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "getError",
        "missingField",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$Event;",
        "getMissingField",
        "personalInfo",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
        "getPersonalInfo",
        "rejected",
        "getRejected",
        "retry",
        "getRetry",
        "ssnInfo",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
        "getSsnInfo",
        "verifying",
        "getVerifying",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancel:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final error:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final missingField:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final personalInfo:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final rejected:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final retry:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final ssnInfo:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final verifying:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->personalInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 98
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$ssnInfo$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$ssnInfo$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->ssnInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 105
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$missingField$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$missingField$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->missingField:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 111
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->verifying:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 119
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$error$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$error$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->error:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 127
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$retry$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$retry$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->retry:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 135
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$cancel$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$cancel$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->cancel:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 143
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$rejected$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$rejected$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->rejected:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public final getCancel()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog$Event;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->cancel:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getError()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->error:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getMissingField()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$Event;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->missingField:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getPersonalInfo()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->personalInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getRejected()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->rejected:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getRetry()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->retry:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getSsnInfo()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->ssnInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getVerifying()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->verifying:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
