.class final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivatedCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->updateToggleCard(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "<anonymous parameter 1>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;->$screen:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;->invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V
    .locals 0

    const-string p2, "<anonymous parameter 0>"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;->$screen:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ToggleCard;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ToggleCard;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
