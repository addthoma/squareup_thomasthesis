.class final Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ManageSquareCardState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;->wrapDelegateSnapshotWithOptionalCard(Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "O",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic $subSnapshot:Lcom/squareup/workflow/Snapshot;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->this$0:Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->$subSnapshot:Lcom/squareup/workflow/Snapshot;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 90
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->this$0:Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 123
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->$subSnapshot:Lcom/squareup/workflow/Snapshot;

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    .line 124
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method
