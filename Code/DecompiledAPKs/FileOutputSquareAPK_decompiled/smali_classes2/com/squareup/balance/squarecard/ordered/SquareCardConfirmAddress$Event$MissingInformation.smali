.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;
.super Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;
.source "SquareCardConfirmAddressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MissingInformation"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0019\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
        "title",
        "",
        "message",
        "(II)V",
        "getMessage",
        "()I",
        "getTitle",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:I

.field private final title:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;->title:I

    iput p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;->message:I

    return-void
.end method


# virtual methods
.method public final getMessage()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;->message:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;->title:I

    return v0
.end method
