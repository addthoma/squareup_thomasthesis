.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderSquareCardBusinessNameCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$EmptyLinkSpan;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardBusinessNameCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardBusinessNameCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,117:1\n1103#2,7:118\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSquareCardBusinessNameCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator\n*L\n89#1,7:118\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0002\u001f B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0014H\u0016J\u0010\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0014H\u0002J\u0016\u0010\u0019\u001a\u00020\u00162\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001bH\u0002J\u0016\u0010\u001c\u001a\u00020\u00162\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001bH\u0002J(\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00142\u0016\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameScreen;",
        "businessNameFormatter",
        "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
        "(Lio/reactivex/Observable;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V",
        "accountOwnerNameRow",
        "Landroid/widget/TextView;",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "businessNameHelp",
        "businessNameRow",
        "businessNameWarning",
        "Landroid/widget/ImageView;",
        "continueButton",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "configureActionBar",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "handleBack",
        "update",
        "screen",
        "EmptyLinkSpan",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountOwnerNameRow:Landroid/widget/TextView;

.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

.field private businessNameHelp:Landroid/widget/TextView;

.field private businessNameRow:Landroid/widget/TextView;

.field private businessNameWarning:Landroid/widget/ImageView;

.field private continueButton:Landroid/view/View;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;>;",
            "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessNameFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 104
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 105
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_business_account_name_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->accountOwnerNameRow:Landroid/widget/TextView;

    .line 106
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_business_name_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameRow:Landroid/widget/TextView;

    .line 107
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_business_name_warning:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameWarning:Landroid/widget/ImageView;

    .line 108
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_business_name_help_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameHelp:Landroid/widget/TextView;

    .line 109
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_business_name_continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;)V"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 97
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 98
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_business_info_action_bar:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 99
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$configureActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$configureActionBar$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;)V"
        }
    .end annotation

    .line 93
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;)V"
        }
    .end annotation

    .line 62
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->configureActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->accountOwnerNameRow:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "accountOwnerNameRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;->getOwnerName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;->formatBusinessName(Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameRow:Landroid/widget/TextView;

    if-nez v1, :cond_1

    const-string v2, "businessNameRow"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;->getText()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameWarning:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    const-string v2, "businessNameWarning"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroid/view/View;

    instance-of v2, v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Truncated;

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 74
    instance-of v0, v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Unchanged;

    if-eqz v0, :cond_3

    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_business_info_help:I

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_6

    .line 75
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_business_info_help_truncation:I

    .line 78
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_business_info_help_location:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "view.resources.getString\u2026iness_info_help_location)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 82
    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$EmptyLinkSpan;

    sget v4, Lcom/squareup/marin/R$color;->marin_medium_gray_pressed:I

    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v3, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$EmptyLinkSpan;-><init>(I)V

    check-cast v3, Landroid/text/style/CharacterStyle;

    .line 81
    invoke-static {v2, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    .line 79
    check-cast v1, Ljava/lang/CharSequence;

    .line 87
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->businessNameHelp:Landroid/widget/TextView;

    if-nez v2, :cond_4

    const-string v3, "businessNameHelp"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 85
    :cond_4
    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "settings_section"

    .line 86
    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->continueButton:Landroid/view/View;

    if-nez p1, :cond_5

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 118
    :cond_5
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 75
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->bindViews(Landroid/view/View;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
