.class public final Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;
.super Ljava/lang/Object;
.source "CardOrderingAuthSsnInfoLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthSsnInfoLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthSsnInfoLayoutRunner.kt\ncom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,60:1\n1103#2,7:61\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthSsnInfoLayoutRunner.kt\ncom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner\n*L\n39#1,7:61\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00112\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H\u0002J\u0018\u0010\u000e\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "ownerSsn",
        "Lcom/squareup/widgets/SelectableEditText;",
        "configureActionBar",
        "",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final continueButton:Landroid/view/View;

.field private final ownerSsn:Lcom/squareup/widgets/SelectableEditText;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->view:Landroid/view/View;

    .line 25
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 26
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_ssn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.owner_ssn)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    .line 27
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->continue_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.continue_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->continueButton:Landroid/view/View;

    .line 30
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-static {}, Lcom/squareup/text/TinFormatter;->createSsnFormatter()Lcom/squareup/text/TinFormatter;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Scrubber;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v0, v1, v2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public static final synthetic access$getOwnerSsn$p(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method private final configureActionBar(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;)V
    .locals 4

    .line 51
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 48
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 49
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 50
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$configureActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$configureActionBar$1;-><init>(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->continueButton:Landroid/view/View;

    .line 61
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->configureActionBar(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;)V

    .line 44
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;->getSsn()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
