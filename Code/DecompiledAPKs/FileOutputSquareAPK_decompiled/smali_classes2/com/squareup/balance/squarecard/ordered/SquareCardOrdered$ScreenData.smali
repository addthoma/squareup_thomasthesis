.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;
.super Ljava/lang/Object;
.source "SquareCardOrderedScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0018\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003JE\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\u00052\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\rR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
        "",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "allowBack",
        "",
        "orderedMessage",
        "",
        "activateMessage",
        "enableActivation",
        "showGetHelpWithCard",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V",
        "getActivateMessage",
        "()I",
        "getAllowBack",
        "()Z",
        "getCard",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getEnableActivation",
        "getOrderedMessage",
        "getShowGetHelpWithCard",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activateMessage:I

.field private final allowBack:Z

.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field private final enableActivation:Z

.field private final orderedMessage:I

.field private final showGetHelpWithCard:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    iput p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    iput p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    iput-boolean p5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    iput-boolean p6, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZILjava/lang/Object;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;
    .locals 8

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    iget v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    iget v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    iget-boolean p1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActivateMessage()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    return v0
.end method

.method public final getAllowBack()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    return v0
.end method

.method public final getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final getEnableActivation()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    return v0
.end method

.method public final getOrderedMessage()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    return v0
.end method

.method public final getShowGetHelpWithCard()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->allowBack:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", orderedMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->orderedMessage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", activateMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->activateMessage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", enableActivation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->enableActivation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showGetHelpWithCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->showGetHelpWithCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
