.class final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePayAddress(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $nameOnCard:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->$nameOnCard:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/googlepay/GooglePayAddress;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
            ">;)",
            "Lcom/squareup/googlepay/GooglePayAddress;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->$nameOnCard:Ljava/lang/String;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;->billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const-string v2, "successOrFailure.response.billing_address"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$toGooglePayAddress(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/googlepay/GooglePayAddress;

    move-result-object p1

    goto :goto_0

    .line 118
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/googlepay/GooglePayAddress;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->$nameOnCard:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xfe

    const/4 v10, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v10}, Lcom/squareup/googlepay/GooglePayAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/googlepay/GooglePayAddress;

    move-result-object p1

    return-object p1
.end method
