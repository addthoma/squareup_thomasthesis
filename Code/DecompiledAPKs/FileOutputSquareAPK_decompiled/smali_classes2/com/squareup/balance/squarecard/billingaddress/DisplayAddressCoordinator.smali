.class public final Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DisplayAddressCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B+\u0008\u0007\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0005H\u0002J\u0008\u0010\u0014\u001a\u00020\u000eH\u0002J\u0018\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "address",
        "Lcom/squareup/address/NohoAddressLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "screen",
        "initToolbar",
        "updateUi",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private address:Lcom/squareup/address/NohoAddressLayout;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->handleBack(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V

    return-void
.end method

.method public static final synthetic access$updateUi(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;Landroid/view/View;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->updateUi(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 61
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->view_billing_address_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.v\u2026w_billing_address_layout)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/address/NohoAddressLayout;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->address:Lcom/squareup/address/NohoAddressLayout;

    return-void
.end method

.method private final handleBack(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V
    .locals 1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Event$OnExit;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Event$OnExit;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final initToolbar()V
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 37
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 38
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->view_billing_address_label:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateUi(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;Landroid/view/View;)V
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->address:Lcom/squareup/address/NohoAddressLayout;

    const-string v1, "address"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/address/NohoAddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->address:Lcom/squareup/address/NohoAddressLayout;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/address/NohoAddressLayout;->setEnabled(Z)V

    .line 49
    new-instance v0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$updateUi$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$updateUi$1;-><init>(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 52
    iget-object p2, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v0, "actionBar"

    if-nez p2, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 50
    :cond_2
    iget-object v1, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$updateUi$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$updateUi$2;-><init>(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->bindViews(Landroid/view/View;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->initToolbar()V

    .line 32
    iget-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$1;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens.map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v1, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;-><init>(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
