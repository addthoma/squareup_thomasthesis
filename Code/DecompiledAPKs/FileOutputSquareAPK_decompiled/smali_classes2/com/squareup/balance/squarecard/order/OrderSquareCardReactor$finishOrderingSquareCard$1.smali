.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;
.super Ljava/lang/Object;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->finishOrderingSquareCard(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;",
        "orderResult",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/mailorder/OrderWorkflowResult;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;",
            ">;"
        }
    .end annotation

    .line 531
    sget-object v0, Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;->INSTANCE:Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 532
    :cond_0
    sget-object v0, Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithBack;->INSTANCE:Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 533
    new-instance v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    .line 534
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSkippedInitialScreens()Z

    move-result v1

    .line 535
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v3

    .line 536
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v4

    .line 537
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getStampsToRestore()Ljava/util/List;

    move-result-object v6

    .line 538
    sget-object v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;->CHECKING:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    const/4 v5, 0x0

    move-object v0, v7

    .line 533
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    .line 532
    invoke-direct {p1, v7}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/mailorder/OrderWorkflowResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;->call(Lcom/squareup/mailorder/OrderWorkflowResult;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
