.class public final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "CancelSquareCardWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelSquareCardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,278:1\n32#2,12:279\n149#3,5:291\n149#3,5:296\n*E\n*S KotlinDebug\n*F\n+ 1 CancelSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow\n*L\n95#1,12:279\n113#1,5:291\n195#1,5:296\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001)Bq\u0008\u0007\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000b\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000b\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000b\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u000b\u0012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u000b\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000b\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ2\u0010\u001c\u001a\u0018\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u0007j\u0008\u0012\u0004\u0012\u00020\u001d`\u001f2\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040!H\u0002J\u001a\u0010\"\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u00022\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0016JN\u0010&\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010#\u001a\u00020\u00022\u0006\u0010\'\u001a\u00020\u00032\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040!H\u0016J\u0010\u0010(\u001a\u00020%2\u0006\u0010\'\u001a\u00020\u0003H\u0016R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cancelSquareCardReasonsWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
        "cancelSquareCardConfirmWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
        "cancelingSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
        "cancelSquareCardSuccessWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
        "cancelSquareCardFailedWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
        "maybeCancelBizbankWorkflow",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
        "submitFeedbackWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
        "analytics",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V",
        "cancelCardReasonsBody",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

.field private final cancelSquareCardConfirmWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelSquareCardFailedWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelSquareCardReasonsWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelSquareCardSuccessWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelingSquareCardWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeCancelBizbankWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final submitFeedbackWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cancelSquareCardReasonsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelSquareCardConfirmWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelingSquareCardWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelSquareCardSuccessWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelSquareCardFailedWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeCancelBizbankWorkflow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "submitFeedbackWorkflow"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardReasonsWorkflow:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardConfirmWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelingSquareCardWorkflow:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardSuccessWorkflow:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardFailedWorkflow:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->maybeCancelBizbankWorkflow:Ljavax/inject/Provider;

    iput-object p7, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->submitFeedbackWorkflow:Ljavax/inject/Provider;

    iput-object p8, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    return-object p0
.end method

.method private final cancelCardReasonsBody(Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardReasonsWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "cancelSquareCardReasonsWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 297
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 298
    const-class v1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 299
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 297
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 279
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 284
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 286
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 287
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 288
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 290
    :cond_3
    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 95
    :cond_4
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->initialState(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    check-cast p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->render(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p3}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelCardReasonsBody(Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    .line 105
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :goto_0
    move-object v4, p1

    goto/16 :goto_1

    .line 106
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;

    if-eqz v0, :cond_1

    .line 107
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardConfirmWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "cancelSquareCardConfirmWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v5

    const/4 v6, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 292
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 293
    const-class p3, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 294
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 292
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    move-object v4, p2

    goto/16 :goto_1

    .line 114
    :cond_1
    instance-of v0, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$CancelingCard;

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelingSquareCardWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "cancelingSquareCardWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 117
    new-instance v5, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;

    .line 119
    move-object v0, p2

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$CancelingCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$CancelingCard;->getCancelReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v0

    .line 117
    invoke-direct {v5, p1, v0}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    const/4 v6, 0x0

    .line 121
    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$2;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 115
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 132
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 133
    :cond_2
    instance-of p1, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccess;

    if-eqz p1, :cond_3

    .line 135
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardSuccessWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "cancelSquareCardSuccessWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 136
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccess;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccess;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v5

    const/4 v6, 0x0

    .line 137
    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$3;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 134
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 143
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 144
    :cond_3
    instance-of p1, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    if-eqz p1, :cond_4

    .line 146
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->maybeCancelBizbankWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "maybeCancelBizbankWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 147
    new-instance v5, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;

    .line 148
    check-cast p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object p1

    .line 149
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive()Z

    move-result p2

    .line 147
    invoke-direct {v5, p1, p2}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V

    const/4 v6, 0x0

    .line 151
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 145
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 159
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 160
    :cond_4
    instance-of p1, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFailure;

    if-eqz p1, :cond_5

    .line 162
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelSquareCardFailedWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "cancelSquareCardFailedWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 163
    check-cast p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFailure;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFailure;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v5

    const/4 v6, 0x0

    .line 164
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$5;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$5;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 161
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 165
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 166
    :cond_5
    instance-of p1, p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFeedback;

    if-eqz p1, :cond_7

    .line 168
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->submitFeedbackWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "submitFeedbackWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 169
    check-cast p2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFeedback;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFeedback;->getFeedbackSource()Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    move-result-object v5

    const/4 v6, 0x0

    .line 170
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$6;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$6;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 167
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto/16 :goto_0

    .line 173
    :goto_1
    instance-of p1, v4, Lcom/squareup/workflow/legacy/Screen;

    if-eqz p1, :cond_6

    .line 174
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/container/PosLayering$Companion;->sheetStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 179
    :cond_6
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_2
    return-object p1

    .line 167
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
