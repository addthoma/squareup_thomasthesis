.class public final Lcom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt;
.super Ljava/lang/Object;
.source "AnalyticsLoggingHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAnalyticsLoggingHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AnalyticsLoggingHelper.kt\ncom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt\n*L\n1#1,43:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u001a\u0014\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u001a\u0014\u0010\u0007\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "logCancelReasonChosen",
        "",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "analytics",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "logCanceledCard",
        "logCanceledCardSuccess",
        "logReplaceCard",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final logCancelReasonChosen(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1

    const-string v0, "$this$logCancelReasonChosen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReportLostCard()V

    goto :goto_0

    .line 20
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReportStolenCard()V

    goto :goto_0

    .line 21
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReportNeverReceivedCard()V

    goto :goto_0

    .line 22
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReportCancelCardForGenericReason()V

    :cond_3
    :goto_0
    return-void
.end method

.method public static final logCanceledCard(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1

    const-string v0, "$this$logCanceledCard"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logDeactivateLostCard()V

    goto :goto_0

    .line 38
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logDeactivateStolenCard()V

    goto :goto_0

    .line 39
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logDeactivateNeverReceivedCard()V

    goto :goto_0

    .line 40
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logDeactivateCardGeneric()V

    :cond_3
    :goto_0
    return-void
.end method

.method public static final logCanceledCardSuccess(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1

    const-string v0, "$this$logCanceledCardSuccess"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logLostCardDeactivatedScreen()V

    goto :goto_0

    .line 11
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logStolenCardDeactivatedScreen()V

    goto :goto_0

    .line 12
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logNeverReceivedCardDeactivatedScreen()V

    goto :goto_0

    .line 13
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logGenericCardDeactivatedScreen()V

    :cond_3
    :goto_0
    return-void
.end method

.method public static final logReplaceCard(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1

    const-string v0, "$this$logReplaceCard"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReplaceLostCard()V

    goto :goto_0

    .line 29
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReplaceStolenCard()V

    goto :goto_0

    .line 30
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logReplaceNeverReceivedCard()V

    :goto_0
    return-void

    .line 31
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected deactivation reason "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
