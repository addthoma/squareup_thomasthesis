.class final Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;
.super Ljava/lang/Object;
.source "EnterPhoneNumberCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->updateSubmitButtonHandler(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;->this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;->$screen:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 67
    iget-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;->$screen:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event$OnSubmitPhoneNumber;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;->this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->access$phoneNumberUnformatted(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event$OnSubmitPhoneNumber;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
