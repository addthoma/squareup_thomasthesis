.class public final Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "ConfirmBizbankStatusWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J$\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00022\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00030\tH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
    .locals 12

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 39
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 40
    new-instance v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;->getActionBarText()I

    move-result v2

    .line 42
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->canceled_card_success_title:I

    .line 43
    sget v5, Lcom/squareup/common/strings/R$string;->finish:I

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x74

    const/4 v10, 0x0

    move-object v1, v11

    .line 40
    invoke-direct/range {v1 .. v10}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 45
    new-instance p1, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 39
    invoke-direct {v0, v11, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;->render(Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    move-result-object p1

    return-object p1
.end method
