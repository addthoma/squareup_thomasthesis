.class public final Lcom/squareup/balance/squarecard/common/SquareCardProgressScreenKt;
.super Ljava/lang/Object;
.source "SquareCardProgressScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003*2\u0008\u0007\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "SquareCardProgressScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "Lkotlin/Deprecated;",
        "message",
        "Use BizBankProgressScreen",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic SquareCardProgressScreen$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use BizBankProgressScreen"
    .end annotation

    return-void
.end method
