.class final Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AddPhoneNumberWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
        "",
        "result",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;->this$0:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 77
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$Success;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;->this$0:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->access$finishAndExit(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 78
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 79
    new-instance v1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;

    .line 80
    check-cast p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 79
    invoke-direct {v1, v2, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    const/4 v2, 0x0

    .line 78
    invoke-static {v0, v1, v2, p1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;->invoke(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
