.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 608
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->DRAW:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$setMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;)V

    .line 609
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    return-void
.end method
