.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "it",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$backToDepositsInfoOrFinish(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$2;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSquareCardProgressScreen;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
