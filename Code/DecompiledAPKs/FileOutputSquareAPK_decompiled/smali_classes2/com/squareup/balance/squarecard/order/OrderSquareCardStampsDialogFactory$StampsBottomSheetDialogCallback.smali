.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;
.super Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;
.source "OrderSquareCardStampsDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StampsBottomSheetDialogCallback"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0018\u0010\r\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;",
        "Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;",
        "bottomSheetDialog",
        "Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
        "(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V",
        "getBottomSheetDialog",
        "()Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
        "onSlide",
        "",
        "bottomSheet",
        "Landroid/view/View;",
        "slideOffset",
        "",
        "onStateChanged",
        "newState",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bottomSheetDialog:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
            ")V"
        }
    .end annotation

    const-string v0, "bottomSheetDialog"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;

    .line 164
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;->bottomSheetDialog:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    return-void
.end method


# virtual methods
.method public final getBottomSheetDialog()Lcom/google/android/material/bottomsheet/BottomSheetDialog;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;->bottomSheetDialog:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    return-object v0
.end method

.method public onSlide(Landroid/view/View;F)V
    .locals 0

    const-string p2, "bottomSheet"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onStateChanged(Landroid/view/View;I)V
    .locals 1

    const-string v0, "bottomSheet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x5

    if-ne p2, p1, :cond_0

    .line 177
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;->bottomSheetDialog:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->dismiss()V

    :cond_0
    return-void
.end method
