.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureAllControlsButStamps()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 OrderSquareCardSignatureCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator\n*L\n1#1,1322:1\n388#2,21:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->canBeCleared()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    .line 1326
    :cond_0
    new-instance p1, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p1}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v0, 0x0

    check-cast v0, Lkotlin/jvm/functions/Function0;

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 1327
    new-instance v1, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 1328
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->canBeCleared()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1329
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$1;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$1;-><init>(Lcom/squareup/cardcustomizations/signature/SignatureView;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$modifySignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lkotlin/jvm/functions/Function0;)V

    .line 1330
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$2;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 1332
    :cond_1
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1333
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$3;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$3;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$modifyStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lkotlin/jvm/functions/Function0;)V

    .line 1334
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$4;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$4;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    iput-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 1336
    :cond_2
    iget-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    if-nez v0, :cond_3

    iget-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_4

    .line 1337
    :cond_3
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getUndoStack$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/ArrayDeque;

    move-result-object v0

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$5;

    invoke-direct {v2, p1, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1$lambda$5;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 1342
    :cond_4
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    :goto_0
    return-void
.end method
