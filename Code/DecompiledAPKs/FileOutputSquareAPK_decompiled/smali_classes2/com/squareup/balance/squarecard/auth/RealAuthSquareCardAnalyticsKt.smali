.class public final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalyticsKt;
.super Ljava/lang/Object;
.source "RealAuthSquareCardAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "IDV_CANCEL_CANCEL",
        "",
        "IDV_CANCEL_TRY_AGAIN",
        "IDV_FAILED_CLOSE",
        "IDV_FAILED_SCREEN",
        "IDV_FAILED_TRY_AGAIN",
        "IDV_MISSING_INFO_SCREEN",
        "IDV_PERSONAL_INFO_CONTINUE",
        "IDV_PERSONAL_INFO_SCREEN",
        "IDV_REJECTED_SCREEN",
        "IDV_SSN_CONTINUE",
        "IDV_SSN_SCREEN",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final IDV_CANCEL_CANCEL:Ljava/lang/String; = "IDV: Cancel Verification Cancel Ordering"

.field private static final IDV_CANCEL_TRY_AGAIN:Ljava/lang/String; = "IDV: Cancel Verification Try Again"

.field private static final IDV_FAILED_CLOSE:Ljava/lang/String; = "IDV: Unable To Verify Close"

.field private static final IDV_FAILED_SCREEN:Ljava/lang/String; = "IDV: Unable To Verify Personal Information"

.field private static final IDV_FAILED_TRY_AGAIN:Ljava/lang/String; = "IDV: Unable To Verify Try Again"

.field private static final IDV_MISSING_INFO_SCREEN:Ljava/lang/String; = "IDV: Information Invalid"

.field private static final IDV_PERSONAL_INFO_CONTINUE:Ljava/lang/String; = "IDV: Personal Information Continue"

.field private static final IDV_PERSONAL_INFO_SCREEN:Ljava/lang/String; = "IDV: Personal Information Page"

.field private static final IDV_REJECTED_SCREEN:Ljava/lang/String; = "IDV: Not Eligible For Square Card"

.field private static final IDV_SSN_CONTINUE:Ljava/lang/String; = "IDV: SSN Continue"

.field private static final IDV_SSN_SCREEN:Ljava/lang/String; = "IDV: SSN Page"
