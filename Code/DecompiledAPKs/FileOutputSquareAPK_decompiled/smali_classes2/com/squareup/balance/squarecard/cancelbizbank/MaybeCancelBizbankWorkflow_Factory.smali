.class public final Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;
.super Ljava/lang/Object;
.source "MaybeCancelBizbankWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ")",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;"
        }
    .end annotation

    .line 63
    new-instance v7, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;
    .locals 6

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    invoke-static/range {v0 .. v5}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->get()Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;

    move-result-object v0

    return-object v0
.end method
