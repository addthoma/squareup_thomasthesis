.class public final Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;
.super Ljava/lang/Object;
.source "SquareCardTwoFactorAuthInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;",
        "",
        "cardData",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "toolbarTitle",
        "",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)V",
        "getCardData",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getToolbarTitle",
        "()I",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field private final toolbarTitle:I


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)V
    .locals 1

    const-string v0, "cardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput p2, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;IILjava/lang/Object;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;
    .locals 1

    const-string v0, "cardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    iget p1, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final getToolbarTitle()I
    .locals 1

    .line 8
    iget v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SquareCardTwoFactorAuthInput(cardData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->cardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", toolbarTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->toolbarTitle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
