.class public abstract Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;
.super Ljava/lang/Object;
.source "NotificationPreferencesWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;,
        Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayError;,
        Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0008\u0012\u00060\u0003j\u0002`\u00040\u0001:\u0003\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u0003*\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0008\u0012\u00060\u0003j\u0002`\u00040\u0007H\u0016\u0082\u0001\u0003\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesFinished;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "DisplayError",
        "DisplayPreferences",
        "Exit",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayError;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    instance-of v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingPreferences;

    move-object v1, p0

    check-cast v1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;->getResult()Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :cond_0
    instance-of v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayError;

    if-eqz v0, :cond_1

    .line 111
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingError;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingError;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 113
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method
