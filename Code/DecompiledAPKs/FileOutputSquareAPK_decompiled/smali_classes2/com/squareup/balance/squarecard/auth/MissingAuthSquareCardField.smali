.class public final enum Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;
.super Ljava/lang/Enum;
.source "MissingAuthSquareCardField.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;",
        "",
        "title",
        "",
        "message",
        "(Ljava/lang/String;III)V",
        "getMessage",
        "()I",
        "getTitle",
        "ADDRESS",
        "BIRTHDATE",
        "SSN",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

.field public static final enum ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

.field public static final enum BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

.field public static final enum SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;


# instance fields
.field private final message:I

.field private final title:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    new-instance v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    .line 12
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_address_title:I

    .line 13
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_address_message:I

    const/4 v4, 0x0

    const-string v5, "ADDRESS"

    .line 11
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    .line 16
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_birthdate_title:I

    .line 17
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_birthdate_message:I

    const/4 v4, 0x1

    const-string v5, "BIRTHDATE"

    .line 15
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    .line 20
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_ssn_title:I

    .line 21
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_missing_ssn_message:I

    const/4 v4, 0x2

    const-string v5, "SSN"

    .line 19
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->$VALUES:[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->title:I

    iput p4, p0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->message:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;
    .locals 1

    const-class v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    return-object p0
.end method

.method public static values()[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;
    .locals 1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->$VALUES:[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v0}, [Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    return-object v0
.end method


# virtual methods
.method public final getMessage()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->message:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 8
    iget v0, p0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->title:I

    return v0
.end method
