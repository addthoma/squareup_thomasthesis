.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;",
        "it",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 370
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 371
    new-instance v9, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    .line 372
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v2

    .line 373
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getSkippedInitialScreens()Z

    move-result v3

    .line 374
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v4

    .line 375
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v5

    .line 376
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v6

    .line 377
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v7

    .line 378
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getStampsToRestore()Ljava/util/List;

    move-result-object v8

    move-object v1, v9

    .line 371
    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;-><init>(Lcom/squareup/cardcustomizations/signature/InkLevel;ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    .line 370
    invoke-direct {v0, v9}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6$3;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
