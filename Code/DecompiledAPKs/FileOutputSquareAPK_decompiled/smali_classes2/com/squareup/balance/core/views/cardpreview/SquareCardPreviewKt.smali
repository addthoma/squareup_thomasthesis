.class public final Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;
.super Ljava/lang/Object;
.source "SquareCardPreview.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000c\n\u0002\u0008\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "NO_MAX_WIDTH",
        "",
        "OBFUSCATED_PAN_BLOCK",
        "",
        "OBFUSCATION_CHAR",
        "",
        "PAN_PREFIX",
        "PAN_SPLIT_BY",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_MAX_WIDTH:I = -0x1

.field private static final OBFUSCATED_PAN_BLOCK:Ljava/lang/String;

.field private static final OBFUSCATION_CHAR:C = '\u2022'

.field private static final PAN_PREFIX:Ljava/lang/String;

.field private static final PAN_SPLIT_BY:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x2022

    .line 43
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x4

    .line 44
    invoke-static {v0, v1}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->OBFUSCATED_PAN_BLOCK:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->OBFUSCATED_PAN_BLOCK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->PAN_PREFIX:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getOBFUSCATED_PAN_BLOCK$p()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->OBFUSCATED_PAN_BLOCK:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getPAN_PREFIX$p()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->PAN_PREFIX:Ljava/lang/String;

    return-object v0
.end method
