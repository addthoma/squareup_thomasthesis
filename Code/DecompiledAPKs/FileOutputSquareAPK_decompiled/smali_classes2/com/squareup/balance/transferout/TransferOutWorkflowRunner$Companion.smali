.class public final Lcom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "TransferOutWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferOutWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferOutWorkflowRunner.kt\ncom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion\n*L\n1#1,45:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "startNewWorkflow",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "props",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowProps;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/balance/transferout/TransferOutWorkflowProps;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    .line 38
    check-cast p1, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;

    .line 39
    invoke-static {p1, p2}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;->access$setProps$p(Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;Lcom/squareup/balance/transferout/TransferOutWorkflowProps;)V

    .line 40
    invoke-static {p1}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;)V

    return-void
.end method
