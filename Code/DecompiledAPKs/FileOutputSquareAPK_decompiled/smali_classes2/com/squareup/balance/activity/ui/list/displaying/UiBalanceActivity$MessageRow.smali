.class public final Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;
.super Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
.source "UiBalanceActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MessageRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$Creator;,
        Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0001\u001bB\u0019\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "message",
        "",
        "clickAction",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;",
        "(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)V",
        "getClickAction",
        "()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;",
        "getMessage",
        "()I",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "ClickAction",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

.field private final message:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)V
    .locals 1

    const-string v0, "clickAction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 40
    sget-object p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction$NoAction;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction$NoAction;

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->copy(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    return v0
.end method

.method public final component2()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    return-object v0
.end method

.method public final copy(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;
    .locals 1

    const-string v0, "clickAction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    iget v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    iget v1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getClickAction()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    return-object v0
.end method

.method public final getMessage()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MessageRow(message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", clickAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->message:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;->clickAction:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
