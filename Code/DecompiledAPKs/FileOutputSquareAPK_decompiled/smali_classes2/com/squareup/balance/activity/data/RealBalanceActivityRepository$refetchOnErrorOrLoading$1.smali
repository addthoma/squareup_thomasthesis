.class final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lio/reactivex/functions/BiPredicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->refetchOnErrorOrLoading(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiPredicate<",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "prev",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "current",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;

    invoke-direct {v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/balance/activity/data/BalanceActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)Z
    .locals 1

    const-string v0, "prev"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 163
    :cond_0
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    check-cast p2, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;->test(Lcom/squareup/balance/activity/data/BalanceActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)Z

    move-result p1

    return p1
.end method
