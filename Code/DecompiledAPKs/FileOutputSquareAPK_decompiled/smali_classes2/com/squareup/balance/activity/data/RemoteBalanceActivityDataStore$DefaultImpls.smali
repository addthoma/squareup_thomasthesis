.class public final Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore$DefaultImpls;
.super Ljava/lang/Object;
.source "RemoteBalanceActivityDataStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic fetchCompletedBalanceActivity$default(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const-string p3, ""

    .line 17
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;->fetchCompletedBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: fetchCompletedBalanceActivity"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic fetchPendingBalanceActivity$default(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const-string p3, ""

    .line 11
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;->fetchPendingBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: fetchPendingBalanceActivity"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
