.class public final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsSuccessScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;,
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;,
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;,
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;,
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008)\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0005JKLMNB\u009f\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0002\u0010\u001cJ\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\u000f\u00105\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u00c6\u0003J\u000f\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u00c6\u0003J\u000f\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u00c6\u0003J\u000f\u00108\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u00c6\u0003J\u0015\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\t\u0010:\u001a\u00020\u0005H\u00c6\u0003J\t\u0010;\u001a\u00020\u0007H\u00c6\u0003J\t\u0010<\u001a\u00020\tH\u00c6\u0003J\u000f\u0010=\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000bH\u00c6\u0003J\t\u0010>\u001a\u00020\rH\u00c6\u0003J\t\u0010?\u001a\u00020\u000fH\u00c6\u0003J\t\u0010@\u001a\u00020\u0011H\u00c6\u0003J\t\u0010A\u001a\u00020\u0011H\u00c6\u0003J\u00bf\u0001\u0010B\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00112\u000e\u0008\u0002\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u000e\u0008\u0002\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u000e\u0008\u0002\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u000e\u0008\u0002\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0014\u0008\u0002\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0001J\u0013\u0010C\u001a\u00020\u00112\u0008\u0010D\u001a\u0004\u0018\u00010EH\u00d6\u0003J\t\u0010F\u001a\u00020GH\u00d6\u0001J\t\u0010H\u001a\u00020IH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u0012\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010 R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010(R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0017\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010,R\u0017\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010,R\u001d\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010,R\u0017\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010,R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103\u00a8\u0006O"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "Lcom/squareup/util/ViewString;",
        "merchantImage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
        "amount",
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "mainDescription",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
        "itemizedDescriptions",
        "",
        "footnoteMessage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
        "expenseType",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;",
        "displayExpenseTypeError",
        "",
        "isUpdatingCategories",
        "onErrorDisplayed",
        "Lkotlin/Function0;",
        "",
        "onPersonalExpenseTypeSelected",
        "onBusinessExpenseTypeSelected",
        "onBack",
        "onCommunityRewardTapped",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        "(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V",
        "getAmount",
        "()Lcom/squareup/balance/activity/ui/common/Amount;",
        "getDisplayExpenseTypeError",
        "()Z",
        "getExpenseType",
        "()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;",
        "getFootnoteMessage",
        "()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
        "getItemizedDescriptions",
        "()Ljava/util/List;",
        "getMainDescription",
        "()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
        "getMerchantImage",
        "()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnBusinessExpenseTypeSelected",
        "getOnCommunityRewardTapped",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnErrorDisplayed",
        "getOnPersonalExpenseTypeSelected",
        "getTitle",
        "()Lcom/squareup/util/ViewString;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Description",
        "ExpenseType",
        "FootnoteMessage",
        "MerchantImage",
        "OnClick",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/balance/activity/ui/common/Amount;

.field private final displayExpenseTypeError:Z

.field private final expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

.field private final footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

.field private final isUpdatingCategories:Z

.field private final itemizedDescriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;"
        }
    .end annotation
.end field

.field private final mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

.field private final merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onErrorDisplayed:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final title:Lcom/squareup/util/ViewString;


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
            "Lcom/squareup/balance/activity/ui/common/Amount;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantImage"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDescription"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemizedDescriptions"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "footnoteMessage"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expenseType"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onErrorDisplayed"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPersonalExpenseTypeSelected"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBusinessExpenseTypeSelected"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCommunityRewardTapped"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    iput-object p5, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    iput-object p7, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    iput-boolean p8, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    iput-boolean p9, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    iput-object p10, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    iput-object p11, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    iput-object p12, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    iput-object p13, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p14, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    goto :goto_d

    :cond_d
    move-object/from16 v1, p14

    :goto_d
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->copy(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final component10()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component11()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component12()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component13()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component14()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    return-object v0
.end method

.method public final component3()Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    return-object v0
.end method

.method public final component4()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    return-object v0
.end method

.method public final component7()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    return-object v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    return v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    return v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
            "Lcom/squareup/balance/activity/ui/common/Amount;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;"
        }
    .end annotation

    const-string v0, "title"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantImage"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDescription"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemizedDescriptions"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "footnoteMessage"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expenseType"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onErrorDisplayed"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPersonalExpenseTypeSelected"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBusinessExpenseTypeSelected"

    move-object/from16 v13, p12

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    move-object/from16 v14, p13

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCommunityRewardTapped"

    move-object/from16 v15, p14

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-object v1, v0

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v15}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    iget-boolean v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    iget-boolean v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    return-object v0
.end method

.method public final getDisplayExpenseTypeError()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    return v0
.end method

.method public final getExpenseType()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    return-object v0
.end method

.method public final getFootnoteMessage()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    return-object v0
.end method

.method public final getItemizedDescriptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    return-object v0
.end method

.method public final getMainDescription()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    return-object v0
.end method

.method public final getMerchantImage()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    return-object v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnBusinessExpenseTypeSelected()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnCommunityRewardTapped()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnErrorDisplayed()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnPersonalExpenseTypeSelected()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_9
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_a
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_b
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_c
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_d
    add-int/2addr v0, v1

    return v0
.end method

.method public final isUpdatingCategories()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BalanceActivityDetailsSuccessScreen(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->title:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", merchantImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->merchantImage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->amount:Lcom/squareup/balance/activity/ui/common/Amount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mainDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->mainDescription:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemizedDescriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->itemizedDescriptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", footnoteMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->footnoteMessage:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expenseType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->expenseType:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayExpenseTypeError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->displayExpenseTypeError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isUpdatingCategories="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onErrorDisplayed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onErrorDisplayed:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onPersonalExpenseTypeSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onPersonalExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBusinessExpenseTypeSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBusinessExpenseTypeSelected:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCommunityRewardTapped="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->onCommunityRewardTapped:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
