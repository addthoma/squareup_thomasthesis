.class public final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "DisplayBalanceActivityWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayBalanceActivityWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayBalanceActivityWorkflow.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,186:1\n32#2,12:187\n41#3:199\n56#3,2:200\n85#3:203\n276#4:202\n276#4:205\n240#5:204\n*E\n*S KotlinDebug\n*F\n+ 1 DisplayBalanceActivityWorkflow.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow\n*L\n46#1,12:187\n69#1:199\n69#1,2:200\n81#1:203\n69#1:202\n81#1:205\n81#1:204\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001:\u0001\u0017B\u0017\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\u00022\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J \u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0003H\u0016J,\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00032\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0003H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        "balanceActivityRepository",
        "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
        "mapper",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;",
        "(Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

.field private final mapper:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "balanceActivityRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mapper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->mapper:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;

    return-void
.end method

.method public static final synthetic access$getBalanceActivityRepository$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;)Lcom/squareup/balance/activity/data/BalanceActivityRepository;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    return-object p0
.end method

.method public static final synthetic access$getMapper$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->mapper:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 187
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 192
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 194
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 195
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 196
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 198
    :cond_3
    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 46
    :cond_4
    sget-object p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->initialState(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "new"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of p1, p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    if-eqz p1, :cond_0

    move-object p1, p3

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object p2

    if-eq p1, p2, :cond_0

    sget-object p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;

    move-object p3, p1

    check-cast p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    :cond_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;

    check-cast p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->onPropsChanged(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
            "-",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object p1

    .line 69
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    invoke-interface {v1, p1}, Lcom/squareup/balance/activity/data/BalanceActivityRepository;->balanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;

    move-result-object v1

    .line 199
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    const-string v2, "this.toFlowable(BUFFER)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_0

    .line 201
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 202
    const-class v2, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v3, Lcom/squareup/workflow/Worker;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityType;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$1;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 68
    invoke-interface {p3, v3, v1, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 74
    sget-object p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Loading;

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    goto :goto_0

    .line 201
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 76
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    if-eqz p1, :cond_2

    .line 77
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getActivities()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object p1

    :goto_0
    move-object v2, p1

    goto :goto_1

    .line 79
    :cond_2
    instance-of p1, p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    if-eqz p1, :cond_3

    .line 81
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    move-object v1, p2

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    invoke-virtual {v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/squareup/balance/activity/data/BalanceActivityRepository;->fetchMoreBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Single;

    move-result-object p1

    .line 203
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 204
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 205
    const-class v2, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 82
    new-instance p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    .line 80
    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 85
    invoke-virtual {v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;->getActivities()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object p1

    goto :goto_0

    .line 89
    :goto_1
    new-instance p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    .line 91
    new-instance p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;

    invoke-direct {p3, v0, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)V

    move-object v3, p3

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 96
    new-instance p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;

    invoke-direct {p3, p2, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/Sink;)V

    move-object v4, p3

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 101
    new-instance p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$3;

    invoke-direct {p3, p2, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$3;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/Sink;)V

    move-object v5, p3

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 106
    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$4;

    invoke-direct {p2, p0, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$4;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 112
    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$5;

    invoke-direct {p2, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function0;

    move-object v1, p1

    .line 89
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object p1

    .line 85
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->render(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->snapshotState(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
