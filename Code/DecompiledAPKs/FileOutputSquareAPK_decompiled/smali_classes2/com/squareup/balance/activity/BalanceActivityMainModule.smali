.class public abstract Lcom/squareup/balance/activity/BalanceActivityMainModule;
.super Ljava/lang/Object;
.source "BalanceActivityMainModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H!J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH!J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH!J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H!\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/balance/activity/BalanceActivityMainModule;",
        "",
        "()V",
        "bindsBalanceActivityDataStore",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
        "realRemoteBalanceActivityDataStore",
        "Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;",
        "bindsBalanceActivityDetailsDataStore",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;",
        "realRemoteBalanceActivityDetailsDataStore",
        "Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;",
        "bindsBalanceActivityDetailsRepository",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
        "realBalanceActivityDetailsRepository",
        "Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;",
        "bindsBalanceActivityDetailsWorkflow",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
        "balanceActivityDetailsWorkflow",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;",
        "bindsBalanceActivityMapper",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
        "realBalanceActivityMapper",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;",
        "bindsBalanceActivityRepository",
        "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
        "realBalanceActivityRepository",
        "Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;",
        "bindsBalanceActivityViewFactoryFactory",
        "Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;",
        "realBalanceActivityViewFactory",
        "Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;",
        "bindsBalanceActivityWorkflow",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;",
        "realBalanceActivityWorkflow",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindsBalanceActivityDataStore(Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;)Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityDetailsDataStore(Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;)Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityDetailsRepository(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;)Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityDetailsWorkflow(Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;)Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityMapper(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityRepository(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/balance/activity/data/BalanceActivityRepository;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityViewFactoryFactory(Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;)Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsBalanceActivityWorkflow(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;)Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
