.class public final Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;
.super Ljava/lang/Object;
.source "RealRemoteBalanceActivityDataStore.kt"

# interfaces
.implements Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRemoteBalanceActivityDataStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRemoteBalanceActivityDataStore.kt\ncom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore\n*L\n1#1,123:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J.\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J&\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000c\u001a\u00020\rH\u0016J&\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u001e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u0008H\u0002J\u0014\u0010\u0017\u001a\u00020\u0018*\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
        "remoteService",
        "Lcom/squareup/balance/activity/data/service/BalanceActivityService;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/balance/activity/data/service/BalanceActivityService;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "fetchActivityData",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "type",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "batchToken",
        "",
        "activityState",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;",
        "maxLatestEventAt",
        "Lcom/squareup/protos/common/time/DateTime;",
        "fetchCompletedBalanceActivity",
        "fetchPendingBalanceActivity",
        "mapResponse",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;",
        "toFilters",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BALANCE_ACTIVITY_CARD_SPEND:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field private static final BALANCE_ACTIVITY_TRANSFERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_BATCH_SIZE:I = 0x14


# instance fields
.field private final remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->Companion:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;

    const/4 v0, 0x2

    new-array v1, v0, [Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 106
    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 107
    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 105
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_CARD_SPEND:Ljava/util/List;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 111
    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->STANDARD_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    aput-object v2, v1, v3

    .line 112
    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->DEBIT_CARD_TRANSFER_IN:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    aput-object v2, v1, v4

    .line 113
    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->INSTANT_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    aput-object v2, v1, v0

    .line 110
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_TRANSFERS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/activity/data/service/BalanceActivityService;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

    iput-object p2, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static final synthetic access$getBALANCE_ACTIVITY_CARD_SPEND$cp()Ljava/util/List;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_CARD_SPEND:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getBALANCE_ACTIVITY_TRANSFERS$cp()Ljava/util/List;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_TRANSFERS:Ljava/util/List;

    return-object v0
.end method

.method private final fetchActivityData(Lcom/squareup/balance/activity/data/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/common/time/DateTime;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;",
            "Lcom/squareup/protos/common/time/DateTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/protos/bizbank/BatchRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/BatchRequest$Builder;-><init>()V

    const/16 v1, 0x14

    .line 57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/BatchRequest$Builder;->batch_size(Ljava/lang/Integer;)Lcom/squareup/protos/bizbank/BatchRequest$Builder;

    move-result-object v0

    .line 60
    move-object v1, p2

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 61
    invoke-virtual {v0, p2}, Lcom/squareup/protos/bizbank/BatchRequest$Builder;->pagination_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/BatchRequest$Builder;

    .line 64
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/BatchRequest$Builder;->build()Lcom/squareup/protos/bizbank/BatchRequest;

    move-result-object p2

    .line 66
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p3}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->activity_state_category(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    move-result-object p3

    .line 69
    invoke-direct {p0, p1, p4}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->toFilters(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->filters(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1, p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->batch_request(Lcom/squareup/protos/bizbank/BatchRequest;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;

    move-result-object p1

    .line 73
    iget-object p2, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/activity/data/service/BalanceActivityService;->getUnifiedActivities(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;)Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->mapResponse(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final mapResponse(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;",
            ">;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation

    .line 93
    sget-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$mapResponse$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$mapResponse$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "map { result ->\n      wh\u2026re -> Error\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toFilters(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 82
    sget-object p1, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_TRANSFERS:Ljava/util/List;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 81
    :cond_1
    sget-object p1, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->BALANCE_ACTIVITY_CARD_SPEND:Ljava/util/List;

    goto :goto_0

    .line 80
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 85
    :goto_0
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;-><init>()V

    .line 86
    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->include_activity_types(Ljava/util/List;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->max_latest_event_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    move-result-object p1

    const-string p2, "Filters.Builder()\n      \u2026EventAt)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public fetchCompletedBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxLatestEventAt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "batchToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->COMPLETED:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    invoke-direct {p0, p1, p3, v0, p2}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->fetchActivityData(Lcom/squareup/balance/activity/data/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/common/time/DateTime;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public fetchPendingBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxLatestEventAt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "batchToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    invoke-direct {p0, p1, p3, v0, p2}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->fetchActivityData(Lcom/squareup/balance/activity/data/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/common/time/DateTime;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
