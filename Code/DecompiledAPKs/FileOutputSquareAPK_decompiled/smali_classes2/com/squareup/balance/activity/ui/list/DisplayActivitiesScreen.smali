.class public final Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;
.super Ljava/lang/Object;
.source "DisplayActivitiesScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00080\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00080\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "tabSelected",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "childScreenData",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "onTabSelected",
        "Lkotlin/Function1;",
        "(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V",
        "getChildScreenData",
        "()Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnTabSelected",
        "()Lkotlin/jvm/functions/Function1;",
        "getTabSelected",
        "()Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final childScreenData:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onTabSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tabSelected:Lcom/squareup/balance/activity/data/BalanceActivityType;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "tabSelected"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "childScreenData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTabSelected"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->tabSelected:Lcom/squareup/balance/activity/data/BalanceActivityType;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->childScreenData:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final getChildScreenData()Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->childScreenData:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    return-object v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnTabSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getTabSelected()Lcom/squareup/balance/activity/data/BalanceActivityType;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->tabSelected:Lcom/squareup/balance/activity/data/BalanceActivityType;

    return-object v0
.end method
