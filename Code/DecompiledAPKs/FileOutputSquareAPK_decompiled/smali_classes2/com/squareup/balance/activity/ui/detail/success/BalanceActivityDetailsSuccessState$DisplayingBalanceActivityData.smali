.class public final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;
.super Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;
.source "BalanceActivityDetailsSuccessState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayingBalanceActivityData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00052\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;",
        "details",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "displayError",
        "",
        "(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V",
        "getDetails",
        "()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "getDisplayError",
        "()Z",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

.field private final displayError:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V
    .locals 1

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iput-boolean p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 12
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;ZILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->copy(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;
    .locals 1

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    iget-boolean p1, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    return-object v0
.end method

.method public final getDisplayError()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisplayingBalanceActivityData(details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->displayError:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
