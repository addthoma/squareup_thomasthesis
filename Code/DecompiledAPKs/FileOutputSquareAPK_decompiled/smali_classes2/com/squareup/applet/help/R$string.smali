.class public final Lcom/squareup/applet/help/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/help/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final about:I = 0x7f12001c

.field public static final about_help:I = 0x7f120020

.field public static final announcements:I = 0x7f1200cc

.field public static final ask_community:I = 0x7f1200e0

.field public static final ask_community_subtext:I = 0x7f1200e1

.field public static final call_support:I = 0x7f12027d

.field public static final change_bank_account:I = 0x7f1203ed

.field public static final change_bank_account_url:I = 0x7f1203ee

.field public static final chip_card_reader_tutorial:I = 0x7f12041a

.field public static final chip_card_reader_tutorial_subtext:I = 0x7f12041b

.field public static final contact:I = 0x7f120496

.field public static final contactless_plus_chip_reader_tutorial:I = 0x7f1204ab

.field public static final contactless_plus_chip_reader_tutorial_subtext:I = 0x7f1204ac

.field public static final create_item_tutorial:I = 0x7f1205c0

.field public static final create_item_tutorial_subtext:I = 0x7f1205d5

.field public static final diagnostics:I = 0x7f120864

.field public static final email_support_ledger:I = 0x7f1209a5

.field public static final feature_tour:I = 0x7f120ab3

.field public static final feature_tour_subtext:I = 0x7f120ab4

.field public static final fee_walkthrough:I = 0x7f120ab5

.field public static final fee_walkthrough_subtext:I = 0x7f120ab6

.field public static final first_invoice_walkthrough:I = 0x7f120ace

.field public static final first_invoice_walkthrough_subtext:I = 0x7f120acf

.field public static final first_payment_walkthrough:I = 0x7f120ad4

.field public static final first_payment_walkthrough_subtext:I = 0x7f120ad5

.field public static final getting_started_accept_payment:I = 0x7f120af4

.field public static final getting_started_accept_payment_link:I = 0x7f120af5

.field public static final getting_started_customer_display_position:I = 0x7f120af6

.field public static final getting_started_customer_display_position_link:I = 0x7f120af7

.field public static final hardware_disclaimer:I = 0x7f120b4a

.field public static final hardware_disclaimer_detail:I = 0x7f120b4b

.field public static final help:I = 0x7f120b4f

.field public static final hs__conversation_redacted_status:I = 0x7f120b68

.field public static final hs__new_conversation_btn:I = 0x7f120ba5

.field public static final jedi_content_unavailable_text:I = 0x7f120e70

.field public static final jedi_network_unavailable_text:I = 0x7f120e71

.field public static final legal:I = 0x7f120ebd

.field public static final libraries:I = 0x7f120ec7

.field public static final licenses:I = 0x7f120eca

.field public static final link_bank_account:I = 0x7f120ecd

.field public static final link_bank_account_subtext:I = 0x7f120ed3

.field public static final loyalty_adjust_points:I = 0x7f120f04

.field public static final loyalty_adjust_points_subtext:I = 0x7f120f0b

.field public static final loyalty_enroll:I = 0x7f120f25

.field public static final loyalty_enroll_subtext:I = 0x7f120f39

.field public static final loyalty_redeem:I = 0x7f120f45

.field public static final loyalty_redeem_subtext:I = 0x7f120f4c

.field public static final message_us:I = 0x7f120fc5

.field public static final messages_notification_content:I = 0x7f120fe6

.field public static final messages_notification_title:I = 0x7f120fe7

.field public static final new_tutorial_notification_content_plural:I = 0x7f12106f

.field public static final new_tutorial_notification_content_singular:I = 0x7f121070

.field public static final new_tutorial_notification_title_plural:I = 0x7f121071

.field public static final new_tutorial_notification_title_singular:I = 0x7f121072

.field public static final new_word:I = 0x7f121073

.field public static final no_announcements_description:I = 0x7f12107d

.field public static final no_announcements_title:I = 0x7f12107e

.field public static final no_results:I = 0x7f12108c

.field public static final no_results_subtext:I = 0x7f12108d

.field public static final ntep_certification_number:I = 0x7f1210c0

.field public static final ntep_model:I = 0x7f1210c1

.field public static final ntep_title:I = 0x7f1210c2

.field public static final order_a_reader:I = 0x7f1211d7

.field public static final order_history:I = 0x7f1211f9

.field public static final order_history_url:I = 0x7f1211fa

.field public static final order_items_none:I = 0x7f1211fb

.field public static final order_items_one:I = 0x7f1211fc

.field public static final order_items_some:I = 0x7f1211fd

.field public static final order_not_sent:I = 0x7f121200

.field public static final order_number:I = 0x7f121201

.field public static final order_reader_all_hardware:I = 0x7f121203

.field public static final order_reader_contactless:I = 0x7f121204

.field public static final order_reader_order_now:I = 0x7f121208

.field public static final order_status_awaiting_return:I = 0x7f121209

.field public static final order_status_canceled:I = 0x7f12120a

.field public static final order_status_declined:I = 0x7f12120b

.field public static final order_status_declined_and_refunded:I = 0x7f12120c

.field public static final order_status_declined_refund_pending:I = 0x7f12120d

.field public static final order_status_delivered:I = 0x7f12120e

.field public static final order_status_out_for_delivery:I = 0x7f12120f

.field public static final order_status_partially_delivered:I = 0x7f121210

.field public static final order_status_partially_delivered_with_errors:I = 0x7f121211

.field public static final order_status_processing:I = 0x7f121212

.field public static final order_status_returned:I = 0x7f121213

.field public static final order_status_returned_and_refunded:I = 0x7f121214

.field public static final order_status_returned_to_sender:I = 0x7f121215

.field public static final order_status_shipped:I = 0x7f121216

.field public static final order_status_shipped_with_errors:I = 0x7f121217

.field public static final order_status_shipping_error:I = 0x7f121218

.field public static final order_status_unknown:I = 0x7f121219

.field public static final orders:I = 0x7f1212ec

.field public static final orders_error_message:I = 0x7f1212ed

.field public static final orders_error_title:I = 0x7f1212ee

.field public static final orders_none_message:I = 0x7f1212ef

.field public static final orders_none_title:I = 0x7f1212f0

.field public static final orders_reload:I = 0x7f1212f1

.field public static final orders_track:I = 0x7f1212f2

.field public static final orders_view_full_history:I = 0x7f1212f3

.field public static final pending:I = 0x7f121428

.field public static final please_check_your_network_connection_try_again:I = 0x7f121440

.field public static final privacy_policy:I = 0x7f1214ee

.field public static final quickamounts_walkthrough:I = 0x7f12151d

.field public static final quickamounts_walkthrough_subtext:I = 0x7f12151e

.field public static final referrals_notification_content:I = 0x7f121626

.field public static final referrals_notification_title:I = 0x7f121627

.field public static final seller_agreement:I = 0x7f1217bc

.field public static final seller_community_url:I = 0x7f1217bd

.field public static final send_diagnostic_report:I = 0x7f1217c0

.field public static final setup_guide:I = 0x7f1217da

.field public static final spoc_version:I = 0x7f12186c

.field public static final square_fees:I = 0x7f121891

.field public static final square_fees_url:I = 0x7f121892

.field public static final support_center_url:I = 0x7f1218dc

.field public static final support_title:I = 0x7f1218dd

.field public static final support_twitter:I = 0x7f1218de

.field public static final support_version:I = 0x7f1218df

.field public static final system_status:I = 0x7f12190d

.field public static final system_status_subtext:I = 0x7f12190e

.field public static final system_status_url:I = 0x7f12190f

.field public static final tap_get_help_to_view_messaging:I = 0x7f121917

.field public static final tap_to_view_conversation:I = 0x7f12191e

.field public static final tap_to_view_message_reply:I = 0x7f12191f

.field public static final terms:I = 0x7f121957

.field public static final titlecase_support:I = 0x7f1219e3

.field public static final troubleshoot_pos:I = 0x7f121a5e

.field public static final troubleshoot_pos_url:I = 0x7f121a5f

.field public static final troubleshooting:I = 0x7f121a60

.field public static final troubleshooting_customer_display_connection:I = 0x7f121a61

.field public static final troubleshooting_customer_display_connection_link:I = 0x7f121a62

.field public static final troubleshooting_hardware_connection:I = 0x7f121a63

.field public static final troubleshooting_hardware_connection_link:I = 0x7f121a64

.field public static final troubleshooting_network_connection:I = 0x7f121a65

.field public static final troubleshooting_network_connection_link:I = 0x7f121a66

.field public static final tutorials:I = 0x7f121ab5

.field public static final understanding_your_register:I = 0x7f121ae4

.field public static final understanding_your_register_subtext:I = 0x7f121ae5

.field public static final upload_diagnostics_data_failure:I = 0x7f121af9

.field public static final upload_diagnostics_data_success:I = 0x7f121afa

.field public static final upload_support_ledger:I = 0x7f121b00

.field public static final upload_transaction_ledger_failure:I = 0x7f121b02

.field public static final upload_transaction_ledger_success:I = 0x7f121b03

.field public static final uppercase_account:I = 0x7f121b05

.field public static final uppercase_application:I = 0x7f121b07

.field public static final uppercase_frequently_asked_questions:I = 0x7f121b2a

.field public static final uppercase_getting_started:I = 0x7f121b2b

.field public static final uppercase_libraries:I = 0x7f121b4b

.field public static final uppercase_message_center:I = 0x7f121b4f

.field public static final uppercase_support:I = 0x7f121b87

.field public static final uppercase_support_legal:I = 0x7f121b88

.field public static final uppercase_troubleshooting:I = 0x7f121b8d

.field public static final version:I = 0x7f121ba1

.field public static final view_messages:I = 0x7f121bac

.field public static final view_sales_and_fees:I = 0x7f121bae

.field public static final view_sales_and_fees_url:I = 0x7f121baf

.field public static final visit_support:I = 0x7f121bb0

.field public static final visit_support_subtext:I = 0x7f121bb1

.field public static final whats_new:I = 0x7f121be1

.field public static final whats_new_subtext:I = 0x7f121be2

.field public static final x2_support_center_url:I = 0x7f121bec


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
