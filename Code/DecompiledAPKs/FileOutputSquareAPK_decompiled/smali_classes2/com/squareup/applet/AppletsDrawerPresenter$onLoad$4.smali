.class final Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;
.super Lkotlin/jvm/internal/Lambda;
.source "AppletsDrawerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Lcom/squareup/applet/AppletsDrawerLayout;

.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/applet/AppletsDrawerLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->$view:Lcom/squareup/applet/AppletsDrawerLayout;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 135
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 136
    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {v1}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$getAppletSelection$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/squareup/applet/AppletSelection;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v1

    .line 137
    new-instance v2, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const/4 v2, 0x0

    .line 145
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "sub"

    .line 146
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
