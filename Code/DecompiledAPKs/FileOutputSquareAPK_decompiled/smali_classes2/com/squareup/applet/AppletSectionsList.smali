.class public abstract Lcom/squareup/applet/AppletSectionsList;
.super Ljava/lang/Object;
.source "AppletSectionsList.java"


# instance fields
.field private final entryPoint:Lcom/squareup/applet/AppletEntryPoint;

.field protected final visibleEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletSectionsListEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/applet/AppletEntryPoint;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/applet/AppletSectionsList;->visibleEntries:Ljava/util/List;

    .line 17
    iput-object p1, p0, Lcom/squareup/applet/AppletSectionsList;->entryPoint:Lcom/squareup/applet/AppletEntryPoint;

    return-void
.end method


# virtual methods
.method public getLastSelectedSection()Lcom/squareup/applet/AppletSection;
    .locals 4

    .line 25
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsList;->entryPoint:Lcom/squareup/applet/AppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletEntryPoint;->getInitialScreenWithoutAccessChecking()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/squareup/applet/AppletSectionsList;->visibleEntries:Ljava/util/List;

    const-string v2, "Applets must have at least 1 visible section!"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 29
    iget-object v1, p0, Lcom/squareup/applet/AppletSectionsList;->visibleEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 30
    iget-object v3, v2, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    invoke-virtual {v3}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 31
    invoke-virtual {v3}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    iget-object v0, v2, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletSectionsList;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    .line 35
    iget-object v0, v2, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    return-object v0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsList;->visibleEntries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListEntry;

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    .line 41
    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletSectionsList;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    return-object v0
.end method

.method public getVisibleEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsList;->visibleEntries:Ljava/util/List;

    return-object v0
.end method

.method public setSelectedSection(Lcom/squareup/applet/AppletSection;)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsList;->entryPoint:Lcom/squareup/applet/AppletEntryPoint;

    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletEntryPoint;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
