.class public final Lcom/squareup/applet/AppletBanner;
.super Landroid/widget/LinearLayout;
.source "AppletBanner.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppletBanner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppletBanner.kt\ncom/squareup/applet/AppletBanner\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0008\u0010\u0017\u001a\u00020\u0014H\u0002J\u0008\u0010\u0018\u001a\u00020\u0014H\u0014J\u0008\u0010\u0019\u001a\u00020\u0014H\u0014J\u0010\u0010\u001a\u001a\u00020\u00142\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cJ\u000e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u0011J\u0018\u0010\u001f\u001a\u00020\u00142\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!H\u0002J\u0010\u0010#\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u0011H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000f\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/applet/AppletBanner;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "callToActionButton",
        "Landroid/widget/TextView;",
        "fadeDurationMs",
        "startDelayMs",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "visibilityRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/applet/BannerVisibility;",
        "kotlin.jvm.PlatformType",
        "attemptFadeIn",
        "",
        "view",
        "Landroid/view/View;",
        "hide",
        "onAttachedToWindow",
        "onFinishInflate",
        "setCallToActionClickListener",
        "onClickListener",
        "Landroid/view/View$OnClickListener;",
        "setVisibility",
        "visibility",
        "show",
        "callToActionText",
        "",
        "subtitleText",
        "updateVisibility",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private callToActionButton:Landroid/widget/TextView;

.field private final fadeDurationMs:I

.field private final startDelayMs:I

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private final visibilityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/applet/BannerVisibility;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/applet/AppletBanner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/applet/AppletBanner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/applet/AppletBanner;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/applet/AppletBanner;->startDelayMs:I

    .line 27
    invoke-virtual {p0}, Lcom/squareup/applet/AppletBanner;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/widgets/pos/R$integer;->activation_row_anim_duration:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/applet/AppletBanner;->fadeDurationMs:I

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<BannerVisibility>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/applet/AppletBanner;->visibilityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 p1, 0x1

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletBanner;->setOrientation(I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 22
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 23
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletBanner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$updateVisibility(Lcom/squareup/applet/AppletBanner;Lcom/squareup/applet/BannerVisibility;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletBanner;->updateVisibility(Lcom/squareup/applet/BannerVisibility;)V

    return-void
.end method

.method private final attemptFadeIn(Landroid/view/View;)V
    .locals 4

    .line 90
    invoke-static {p1}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpg-float v0, v0, v1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 94
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 95
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 96
    iget v2, p0, Lcom/squareup/applet/AppletBanner;->startDelayMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 98
    iget v1, p0, Lcom/squareup/applet/AppletBanner;->fadeDurationMs:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/squareup/applet/AppletBanner$attemptFadeIn$1;

    invoke-direct {v1, p1}, Lcom/squareup/applet/AppletBanner$attemptFadeIn$1;-><init>(Landroid/view/View;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private final hide()V
    .locals 0

    .line 82
    invoke-static {p0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final show(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 67
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 70
    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "subtitle"

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Lcom/squareup/applet/AppletBanner;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object p2, p0, Lcom/squareup/applet/AppletBanner;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/squareup/applet/AppletBanner;->attemptFadeIn(Landroid/view/View;)V

    goto :goto_0

    .line 74
    :cond_2
    iget-object p2, p0, Lcom/squareup/applet/AppletBanner;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 77
    :goto_0
    iget-object p2, p0, Lcom/squareup/applet/AppletBanner;->callToActionButton:Landroid/widget/TextView;

    const-string v0, "callToActionButton"

    if-nez p2, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/applet/AppletBanner;->callToActionButton:Landroid/widget/TextView;

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletBanner;->attemptFadeIn(Landroid/view/View;)V

    return-void
.end method

.method private final updateVisibility(Lcom/squareup/applet/BannerVisibility;)V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/applet/BannerVisibility$HideBanner;->INSTANCE:Lcom/squareup/applet/BannerVisibility$HideBanner;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/applet/AppletBanner;->hide()V

    goto :goto_0

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/squareup/applet/BannerVisibility$ShowBanner;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/applet/BannerVisibility$ShowBanner;

    invoke-virtual {p1}, Lcom/squareup/applet/BannerVisibility$ShowBanner;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/applet/BannerVisibility$ShowBanner;->getSubtitle()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/applet/AppletBanner;->show(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 4

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 51
    iget-object v0, p0, Lcom/squareup/applet/AppletBanner;->visibilityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "visibilityRelay\n        .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    move-object v1, p0

    check-cast v1, Landroid/view/View;

    new-instance v2, Lcom/squareup/applet/AppletBanner$onAttachedToWindow$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/applet/AppletBanner;

    invoke-direct {v2, v3}, Lcom/squareup/applet/AppletBanner$onAttachedToWindow$1;-><init>(Lcom/squareup/applet/AppletBanner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/squareup/pos/container/R$id;->section_button_text:I

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.section_button_text)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/applet/AppletBanner;->callToActionButton:Landroid/widget/TextView;

    .line 45
    sget v0, Lcom/squareup/pos/container/R$id;->section_button_subtitle:I

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.section_button_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/applet/AppletBanner;->subtitle:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public final setCallToActionClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/applet/AppletBanner;->callToActionButton:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "callToActionButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setVisibility(Lcom/squareup/applet/BannerVisibility;)V
    .locals 1

    const-string/jumbo v0, "visibility"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/applet/AppletBanner;->visibilityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
