.class final Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;
.super Ljava/lang/Object;
.source "AppletsDrawerPresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/applet/Applet;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "applet",
        "Lcom/squareup/applet/Applet;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fromSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->$fromSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/applet/Applet;)V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    iget-object v0, v0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {v0}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$getDrawerIsReady$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->$fromSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    iget-object v0, v0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->$view:Lcom/squareup/applet/AppletsDrawerLayout;

    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    iget-object v1, v1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {v1}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$getApplets$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletsDrawerLayout;->setSelectedItem(I)V

    goto :goto_0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    iget-object v0, v0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    const-string v1, "applet"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$itemSelected(Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/applet/Applet;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/applet/Applet;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4$sub$1;->accept(Lcom/squareup/applet/Applet;)V

    return-void
.end method
