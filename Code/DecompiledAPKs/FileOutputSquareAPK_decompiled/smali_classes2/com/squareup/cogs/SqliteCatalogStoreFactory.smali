.class Lcom/squareup/cogs/SqliteCatalogStoreFactory;
.super Ljava/lang/Object;
.source "SqliteCatalogStoreFactory.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogStore$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;,
        Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "catalog"


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private final fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

.field private final realCatalogUpdateDispatcher:Lcom/squareup/cogs/RealCatalogUpdateDispatcher;

.field private final syntheticTableReaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;"
        }
    .end annotation
.end field

.field private final syntheticTables:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/cogs/RealCatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/FileThreadEnforcer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/cogs/RealCatalogUpdateDispatcher;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Lcom/squareup/FileThreadEnforcer;",
            ")V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->applicationContext:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->realCatalogUpdateDispatcher:Lcom/squareup/cogs/RealCatalogUpdateDispatcher;

    .line 49
    iput-object p3, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->syntheticTableReaders:Ljava/util/List;

    .line 50
    iput-object p4, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    .line 51
    iput-object p5, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    .line 54
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 55
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    .line 56
    invoke-interface {p3}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;->sourceTables()Ljava/util/List;

    move-result-object p3

    if-nez p3, :cond_1

    goto :goto_0

    .line 61
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 62
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p5

    .line 63
    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    if-nez v0, :cond_2

    .line 65
    invoke-interface {p1, p5, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    if-ne v0, p4, :cond_3

    goto :goto_1

    .line 67
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Multiple instances of "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {p5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " registered"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 72
    :cond_4
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->syntheticTables:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cogs/SqliteCatalogStoreFactory;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->enforceOnFileThread()V

    return-void
.end method

.method private enforceOnFileThread()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    const-string v1, "Catalog operations must be performed on the IO thread"

    invoke-virtual {v0, v1}, Lcom/squareup/FileThreadEnforcer;->enforceOnFileThread(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public open(Ljava/io/File;)Lcom/squareup/shared/catalog/CatalogStore;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    .line 76
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 80
    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v3, "catalog.db"

    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    new-instance p1, Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;

    iget-object v3, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->applicationContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->syntheticTables:Ljava/util/Set;

    invoke-direct {p1, v3, v2, v4}, Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/util/Set;)V

    .line 82
    new-instance v3, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;

    invoke-direct {v3, p0, p1}, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;-><init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Landroid/database/sqlite/SQLiteOpenHelper;)V

    new-array p1, v1, [Ljava/lang/Object;

    .line 83
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Catalog: database in %s"

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->syntheticTableReaders:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    .line 85
    invoke-interface {v0, v3}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;->onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V

    goto :goto_0

    .line 87
    :cond_1
    new-instance p1, Lcom/squareup/shared/catalog/SqliteCatalogStore;

    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->realCatalogUpdateDispatcher:Lcom/squareup/cogs/RealCatalogUpdateDispatcher;

    iget-object v1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->syntheticTables:Ljava/util/Set;

    iget-object v2, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-direct {p1, v3, v0, v1, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;-><init>(Lcom/squareup/shared/sql/DatabaseHelper;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/Set;Lcom/squareup/shared/catalog/logging/Clock;)V

    return-object p1

    .line 77
    :cond_2
    :goto_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-array v1, v1, [Ljava/lang/Object;

    if-nez p1, :cond_3

    const/4 p1, 0x0

    goto :goto_2

    .line 78
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    :goto_2
    aput-object p1, v1, v0

    const-string p1, "Invalid directory %s"

    .line 77
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
