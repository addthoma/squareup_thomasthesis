.class Lcom/squareup/cogs/CogsBuilder$1;
.super Ljava/lang/Object;
.source "CogsBuilder.java"

# interfaces
.implements Lcom/squareup/shared/catalog/logging/Clock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cogs/CogsBuilder;->build()Lcom/squareup/cogs/RealCogs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cogs/CogsBuilder;


# direct methods
.method constructor <init>(Lcom/squareup/cogs/CogsBuilder;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/cogs/CogsBuilder$1;->this$0:Lcom/squareup/cogs/CogsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getElapsedRealtime()J
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/cogs/CogsBuilder$1;->this$0:Lcom/squareup/cogs/CogsBuilder;

    invoke-static {v0}, Lcom/squareup/cogs/CogsBuilder;->access$100(Lcom/squareup/cogs/CogsBuilder;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUptimeMillis()J
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/cogs/CogsBuilder$1;->this$0:Lcom/squareup/cogs/CogsBuilder;

    invoke-static {v0}, Lcom/squareup/cogs/CogsBuilder;->access$100(Lcom/squareup/cogs/CogsBuilder;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
