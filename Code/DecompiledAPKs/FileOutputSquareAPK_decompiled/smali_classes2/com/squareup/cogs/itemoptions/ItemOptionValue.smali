.class public final Lcom/squareup/cogs/itemoptions/ItemOptionValue;
.super Ljava/lang/Object;
.source "ItemOptionValue.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cogs/itemoptions/ItemOptionValue$Creator;,
        Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemOptionValue.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemOptionValue.kt\ncom/squareup/cogs/itemoptions/ItemOptionValue\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u0000 (2\u00020\u0001:\u0001(B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0014H\u00d6\u0001J\u000e\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001d\u001a\n \u001f*\u0004\u0018\u00010\u001e0\u001eJ\u000e\u0010 \u001a\n \u001f*\u0004\u0018\u00010!0!J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "Landroid/os/Parcelable;",
        "optionId",
        "",
        "valueId",
        "name",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "idPair",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
        "getIdPair",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
        "getName",
        "()Ljava/lang/String;",
        "getOptionId",
        "getValueId",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "normalizedName",
        "locale",
        "Ljava/util/Locale;",
        "toCatalogItemOptionValue",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
        "kotlin.jvm.PlatformType",
        "toCatalogItemOptionValueBuilder",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "cogs_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;


# instance fields
.field private final name:Ljava/lang/String;

.field private final optionId:Ljava/lang/String;

.field private final valueId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->Companion:Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;

    new-instance v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Creator;

    invoke-direct {v0}, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Creator;-><init>()V

    sput-object v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "optionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    const-string v0, "optionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
    .locals 3

    .line 16
    new-instance v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionId()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueId()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final normalizedName(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final toCatalogItemOptionValue()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->toCatalogItemOptionValueBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v0

    return-object v0
.end method

.method public final toCatalogItemOptionValueBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;
    .locals 2

    .line 18
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;-><init>()V

    .line 19
    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setItemOptionID(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemOptionValue(optionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", valueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->optionId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->valueId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
