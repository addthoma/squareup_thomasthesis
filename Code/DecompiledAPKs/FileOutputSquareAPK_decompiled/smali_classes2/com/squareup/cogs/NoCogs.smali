.class public final Lcom/squareup/cogs/NoCogs;
.super Ljava/lang/Object;
.source "NoCogs.java"

# interfaces
.implements Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$shouldForegroundSync$0()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 62
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    .line 99
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;"
        }
    .end annotation

    .line 105
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingleOnline(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public asSingleOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TS;>;>;"
        }
    .end annotation

    .line 110
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingleOnlineThenLocal(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public close()V
    .locals 1

    .line 91
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cogsSyncProgress()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x64

    .line 131
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/cogs/NoLocal;

    invoke-direct {v0}, Lcom/squareup/cogs/NoLocal;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/CatalogResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method public executeOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/cogs/NoOnline;

    invoke-direct {v0}, Lcom/squareup/cogs/NoOnline;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogOnlineTask;->perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public executeOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TS;>;)V"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/cogs/NoLocal;

    invoke-direct {v0}, Lcom/squareup/cogs/NoLocal;-><init>()V

    new-instance v1, Lcom/squareup/cogs/NoOnline;

    invoke-direct {v1}, Lcom/squareup/cogs/NoOnline;-><init>()V

    .line 52
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;->perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object v1

    .line 51
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 57
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public foregroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 121
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/cogs/CogsTasks;->foregroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public isCloseEnqueued()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isReady()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;
    .locals 1

    .line 28
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public purge()V
    .locals 1

    .line 95
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 0

    .line 36
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 0

    .line 32
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 62
    sget-object p1, Lcom/squareup/cogs/-$$Lambda$NoCogs$sP_ejvKnawix29mSk-lW-TGKUME;->INSTANCE:Lcom/squareup/cogs/-$$Lambda$NoCogs$sP_ejvKnawix29mSk-lW-TGKUME;

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public shouldForegroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .line 127
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->shouldForegroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    .line 70
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public syncAsSingle()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 114
    invoke-static {p0}, Lcom/squareup/cogs/CogsTasks;->syncAsSingle(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public upsertCatalogConnectV2Object(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            "Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 79
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p4, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
