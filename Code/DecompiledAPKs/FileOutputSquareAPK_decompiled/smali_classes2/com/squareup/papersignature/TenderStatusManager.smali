.class public Lcom/squareup/papersignature/TenderStatusManager;
.super Ljava/lang/Object;
.source "TenderStatusManager.java"


# instance fields
.field private final expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

.field private final tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

.field private final tenderStatusCacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

.field private final tendersAwaitingTipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/activity/ExpiryCalculator;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusManager;->tendersAwaitingTipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    .line 31
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusManager;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    .line 32
    iput-object p3, p0, Lcom/squareup/papersignature/TenderStatusManager;->tenderStatusCacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    .line 33
    iput-object p4, p0, Lcom/squareup/papersignature/TenderStatusManager;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    return-void
.end method


# virtual methods
.method public cacheAsSettledAndScheduleNextUpdateIfNecessary(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 52
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/papersignature/TenderStatusManager;->withAllStatesSetTo(Ljava/util/Collection;Lcom/squareup/protos/client/bills/Tender$State;)Ljava/util/List;

    move-result-object p1

    .line 55
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusManager;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0, p1}, Lcom/squareup/papersignature/TenderStatusCache;->addTipsFromTenders(Ljava/util/Collection;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusManager;->tenderStatusCacheUpdater:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->scheduleNextUpdateIfNecessary()V

    .line 62
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 63
    iget-object v3, p0, Lcom/squareup/papersignature/TenderStatusManager;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v3, v2}, Lcom/squareup/activity/ExpiryCalculator;->isTipExpiringSoon(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusManager;->tendersAwaitingTipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    neg-int p1, p1

    neg-int v1, v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->addToCountUntilNextUpdate(II)V

    return-void
.end method

.method public processListTendersResult(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 91
    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusManager;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/papersignature/TenderStatusCache;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method withAllStatesSetTo(Ljava/util/Collection;Lcom/squareup/protos/client/bills/Tender$State;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/protos/client/bills/Tender$State;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {v1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    .line 75
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method
