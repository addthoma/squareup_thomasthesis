.class abstract Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;
.super Lcom/squareup/server/ErrorLoggingCallback;
.source "PaperSignatureBatchRetryingService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "RetryingServerCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TRequest:",
        "Ljava/lang/Object;",
        "TResponse:",
        "Ljava/lang/Object;",
        "TResult:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/server/ErrorLoggingCallback<",
        "TTResponse;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "TTResult;>;"
        }
    .end annotation
.end field

.field private numberOfRetries:I

.field private final request:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTRequest;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTRequest;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "TTResult;>;)V"
        }
    .end annotation

    .line 50
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    .line 51
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/server/ErrorLoggingCallback;-><init>(Ljava/lang/String;)V

    .line 52
    iput-object p2, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->request:Ljava/lang/Object;

    .line 53
    iput-object p3, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    const/4 p1, 0x0

    .line 54
    iput p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->numberOfRetries:I

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)V"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->handleResponse(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;Ljava/lang/Object;)V

    return-void
.end method

.method public clientError(Ljava/lang/Object;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;I)V"
        }
    .end annotation

    .line 109
    invoke-super {p0, p1, p2}, Lcom/squareup/server/ErrorLoggingCallback;->clientError(Ljava/lang/Object;I)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->onFailure(Ljava/lang/Object;)V

    return-void
.end method

.method protected abstract createResult(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)TTResult;"
        }
    .end annotation
.end method

.method protected abstract doRequest(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTRequest;)V"
        }
    .end annotation
.end method

.method protected abstract getErrorMessage(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method protected abstract getErrorTitle(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract getNextRequestBackoffSeconds(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)I"
        }
    .end annotation
.end method

.method getNumberOfRetries()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->numberOfRetries:I

    return v0
.end method

.method incrementNumberOfRetries()V
    .locals 1

    .line 67
    iget v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->numberOfRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->numberOfRetries:I

    return-void
.end method

.method public abstract isSuccess(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)Z"
        }
    .end annotation
.end method

.method public networkError()V
    .locals 1

    .line 99
    invoke-super {p0}, Lcom/squareup/server/ErrorLoggingCallback;->networkError()V

    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0, v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->onFailure(Ljava/lang/Object;)V

    return-void
.end method

.method public onFailure(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->getErrorTitle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->getErrorMessage(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_0
    iget-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResponse;)V"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->createResult(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public run()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->request:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->doRequest(Ljava/lang/Object;)V

    return-void
.end method

.method public serverError(I)V
    .locals 0

    .line 104
    invoke-super {p0, p1}, Lcom/squareup/server/ErrorLoggingCallback;->serverError(I)V

    const/4 p1, 0x0

    .line 105
    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->onFailure(Ljava/lang/Object;)V

    return-void
.end method
