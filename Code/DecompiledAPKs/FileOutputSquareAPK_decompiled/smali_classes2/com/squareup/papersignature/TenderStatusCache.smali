.class public Lcom/squareup/papersignature/TenderStatusCache;
.super Ljava/lang/Object;
.source "TenderStatusCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;,
        Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;
    }
.end annotation


# instance fields
.field private final cacheEntriesByTenderId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final localSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache;->localSetting:Lcom/squareup/settings/LocalSetting;

    .line 115
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusCache;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 116
    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 117
    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    :goto_0
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    .line 120
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache;->listeners:Ljava/util/Set;

    return-void
.end method

.method private fireOnTenderTipsChanged(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 216
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;

    .line 217
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    .line 218
    invoke-interface {v1, p1}, Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;->onTenderTipsChanged(Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getCacheEntry(Ljava/lang/String;)Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;
    .locals 3

    const-string v0, "tenderId"

    .line 203
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    if-eqz v0, :cond_0

    return-object v0

    .line 206
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tender ID not found in cache: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private saveToLocalSetting()V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->localSetting:Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addOnTenderTipsChangedListener(Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addTipsFromTenders(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 133
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 137
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    if-eqz v1, :cond_1

    .line 142
    invoke-static {v1}, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->fromTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    move-result-object v2

    .line 143
    iget-object v3, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 139
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "One or more tenders is null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 146
    :cond_2
    invoke-direct {p0}, Lcom/squareup/papersignature/TenderStatusCache;->saveToLocalSetting()V

    .line 147
    invoke-direct {p0, p1}, Lcom/squareup/papersignature/TenderStatusCache;->fireOnTenderTipsChanged(Ljava/util/Collection;)V

    return-void
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getTipAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/papersignature/TenderStatusCache;->getCacheEntry(Ljava/lang/String;)Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    if-eqz p1, :cond_0

    .line 162
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getUnprocessedTenderAndBillIds()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 172
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 174
    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 175
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 176
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    iget-object v2, v2, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    .line 177
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 180
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getUnprocessedTenderIds()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public markTendersAsProcessed(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 192
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 193
    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache;->cacheEntriesByTenderId:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to remove tender not found in cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 199
    :cond_1
    invoke-direct {p0}, Lcom/squareup/papersignature/TenderStatusCache;->saveToLocalSetting()V

    return-void
.end method

.method public removeOnTenderTipsChangedListener(Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
