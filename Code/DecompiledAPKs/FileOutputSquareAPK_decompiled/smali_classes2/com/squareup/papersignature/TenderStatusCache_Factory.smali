.class public final Lcom/squareup/papersignature/TenderStatusCache_Factory;
.super Ljava/lang/Object;
.source "TenderStatusCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/papersignature/TenderStatusCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final localSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache_Factory;->localSettingProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusCache_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/papersignature/TenderStatusCache_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/papersignature/TenderStatusCache_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/papersignature/TenderStatusCache_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/papersignature/TenderStatusCache_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/papersignature/TenderStatusCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/papersignature/TenderStatusCache;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/papersignature/TenderStatusCache;

    invoke-direct {v0, p0, p1}, Lcom/squareup/papersignature/TenderStatusCache;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/papersignature/TenderStatusCache;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache_Factory;->localSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/papersignature/TenderStatusCache_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/papersignature/TenderStatusCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/papersignature/TenderStatusCache_Factory;->get()Lcom/squareup/papersignature/TenderStatusCache;

    move-result-object v0

    return-object v0
.end method
