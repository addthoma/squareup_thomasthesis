.class public final Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;
.super Ljava/lang/Object;
.source "CashDrawerModule_ProvideCashDrawerExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/concurrent/Executor;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cashdrawer/CashDrawerModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cashdrawer/CashDrawerModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;->module:Lcom/squareup/cashdrawer/CashDrawerModule;

    return-void
.end method

.method public static create(Lcom/squareup/cashdrawer/CashDrawerModule;)Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;-><init>(Lcom/squareup/cashdrawer/CashDrawerModule;)V

    return-object v0
.end method

.method public static provideCashDrawerExecutor(Lcom/squareup/cashdrawer/CashDrawerModule;)Ljava/util/concurrent/Executor;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerModule;->provideCashDrawerExecutor()Ljava/util/concurrent/Executor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;->get()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/concurrent/Executor;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;->module:Lcom/squareup/cashdrawer/CashDrawerModule;

    invoke-static {v0}, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;->provideCashDrawerExecutor(Lcom/squareup/cashdrawer/CashDrawerModule;)Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method
