.class public final Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;
.super Ljava/lang/Object;
.source "ApgVasarioCashDrawer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final managerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->usbDiscovererProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->managerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;-><init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    iget-object v1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->managerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v0, v1, v2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->newInstance(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer_Factory;->get()Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    move-result-object v0

    return-object v0
.end method
