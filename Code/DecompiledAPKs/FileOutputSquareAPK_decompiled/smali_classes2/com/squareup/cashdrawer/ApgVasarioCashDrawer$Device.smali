.class final Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;
.super Ljava/lang/Object;
.source "ApgVasarioCashDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Device"
.end annotation


# instance fields
.field private final connection:Landroid/hardware/usb/UsbDeviceConnection;

.field private mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final statusEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method private constructor <init>(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 95
    iput-object p2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->statusEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 96
    iput-object p3, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method synthetic constructor <init>(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$1;)V
    .locals 0

    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;-><init>(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;)Z
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->isDrawerOpen()Z

    move-result p0

    return p0
.end method

.method private isDrawerOpen()Z
    .locals 5

    .line 110
    new-instance v0, Landroid/hardware/usb/UsbRequest;

    invoke-direct {v0}, Landroid/hardware/usb/UsbRequest;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->statusEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    const/4 v1, 0x2

    .line 113
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->queue(Ljava/nio/ByteBuffer;I)Z

    .line 115
    invoke-static {}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$200()[B

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->sendMessage([B)V

    .line 118
    iget-object v2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDeviceConnection;->requestWait()Landroid/hardware/usb/UsbRequest;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v2, v0, :cond_0

    .line 119
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 120
    aget-byte v1, v1, v4

    if-ne v1, v3, :cond_0

    const/4 v4, 0x1

    .line 128
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbRequest;->close()V

    return v4
.end method

.method private sendMessage([B)V
    .locals 9

    .line 104
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 106
    iget-object v1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    array-length v7, p1

    const/16 v2, 0x21

    const/16 v3, 0x9

    const/16 v4, 0x200

    const/4 v5, 0x0

    const/16 v8, 0x3a98

    move-object v6, p1

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    return-void
.end method


# virtual methods
.method public openCashDrawer()V
    .locals 1

    .line 100
    invoke-static {}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$100()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->sendMessage([B)V

    return-void
.end method
