.class public final enum Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;
.super Ljava/lang/Enum;
.source "CashDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashdrawer/CashDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisconnectReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

.field public static final enum DISCONNECTED:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

.field public static final enum FAILED_TO_CONNECT:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 14
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_DISCONNECTED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "DISCONNECTED"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->DISCONNECTED:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    new-instance v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_FAILED_TO_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const/4 v3, 0x1

    const-string v4, "FAILED_TO_CONNECT"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->FAILED_TO_CONNECT:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    .line 13
    sget-object v1, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->DISCONNECTED:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->FAILED_TO_CONNECT:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->$VALUES:[Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->$VALUES:[Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    invoke-virtual {v0}, [Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    return-object v0
.end method
