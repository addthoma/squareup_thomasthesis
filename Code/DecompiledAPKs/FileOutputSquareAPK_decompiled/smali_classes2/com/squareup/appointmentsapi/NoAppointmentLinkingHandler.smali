.class public final Lcom/squareup/appointmentsapi/NoAppointmentLinkingHandler;
.super Ljava/lang/Object;
.source "AppointmentLinkingHandler.kt"

# interfaces
.implements Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppointmentLinkingHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppointmentLinkingHandler.kt\ncom/squareup/appointmentsapi/NoAppointmentLinkingHandler\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,20:1\n151#2,2:21\n*E\n*S KotlinDebug\n*F\n+ 1 AppointmentLinkingHandler.kt\ncom/squareup/appointmentsapi/NoAppointmentLinkingHandler\n*L\n19#1,2:21\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/appointmentsapi/NoAppointmentLinkingHandler;",
        "Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;",
        "()V",
        "handle",
        "",
        "target",
        "Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;",
        "appointments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 21
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 22
    const-class v2, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    iput-object v0, p0, Lcom/squareup/appointmentsapi/NoAppointmentLinkingHandler;->$$delegate_0:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    return-void
.end method


# virtual methods
.method public handle(Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;)V
    .locals 1

    const-string v0, "target"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/appointmentsapi/NoAppointmentLinkingHandler;->$$delegate_0:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    invoke-interface {v0, p1}, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;->handle(Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;)V

    return-void
.end method
