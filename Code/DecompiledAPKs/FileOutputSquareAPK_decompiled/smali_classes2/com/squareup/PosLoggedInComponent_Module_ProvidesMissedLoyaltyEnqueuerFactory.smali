.class public final Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;
.super Ljava/lang/Object;
.source "PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
        ">;"
    }
.end annotation


# instance fields
.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->tasksProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)",
            "Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providesMissedLoyaltyEnqueuer(Ljavax/inject/Provider;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ")",
            "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/PosLoggedInComponent$Module;->providesMissedLoyaltyEnqueuer(Ljavax/inject/Provider;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->tasksProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/loyalty/LoyaltySettings;

    invoke-static {v0, v1}, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->providesMissedLoyaltyEnqueuer(Ljavax/inject/Provider;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvidesMissedLoyaltyEnqueuerFactory;->get()Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    move-result-object v0

    return-object v0
.end method
