.class final Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;
.super Ljava/lang/Object;
.source "PhysicalCard.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->animateToCardPosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/animation/ValueAnimator;",
        "kotlin.jvm.PlatformType",
        "onAnimationUpdate"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field final synthetic $bounds:Landroid/graphics/RectF;

.field final synthetic $realSignatureHeight:F

.field final synthetic $realSignatureLeft:F

.field final synthetic $realSignatureTop:F

.field final synthetic $realSignatureWidth:F

.field final synthetic $this_animateToCardPosition:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;FLandroid/graphics/RectF;FFF)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$this_animateToCardPosition:Landroid/view/View;

    iput p2, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureWidth:F

    iput-object p3, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$bounds:Landroid/graphics/RectF;

    iput p4, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureHeight:F

    iput p5, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureLeft:F

    iput p6, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureTop:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .line 98
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$this_animateToCardPosition:Landroid/view/View;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureWidth:F

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$bounds:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    const/4 v2, 0x1

    int-to-float v2, v2

    sub-float/2addr v1, v2

    const-string v3, "it"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v3

    mul-float v1, v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 99
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$this_animateToCardPosition:Landroid/view/View;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureHeight:F

    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$bounds:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v1, v4

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float v1, v1, v2

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 100
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$this_animateToCardPosition:Landroid/view/View;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureLeft:F

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 101
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$this_animateToCardPosition:Landroid/view/View;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$realSignatureTop:F

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;->$bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    mul-float v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method
