.class public Lcom/squareup/cardcustomizations/signature/SignatureView;
.super Landroid/view/View;
.source "SignatureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;,
        Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;
    }
.end annotation


# instance fields
.field private accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

.field private color:I

.field private listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

.field private painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

.field private signature:Lcom/squareup/cardcustomizations/signature/Signature;

.field private signaturePaint:Landroid/graphics/Paint;

.field private signatureStartBounds:Landroid/graphics/RectF;

.field private signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance p2, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    .line 39
    sget-object p2, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    const/high16 p2, -0x1000000

    .line 53
    iput p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/card/customization/R$dimen;->card_customizations_signature_stroke_width:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    iput p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    const-string p2, "accessibility"

    .line 61
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/accessibility/AccessibilityManager;

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-void
.end method

.method private setSignature(Lcom/squareup/cardcustomizations/signature/Signature;)V
    .locals 2

    .line 131
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz p1, :cond_0

    .line 132
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Signature;->hasGlyphs()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 133
    sget-object p1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    .line 134
    sget-object p1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    goto :goto_0

    .line 136
    :cond_0
    sget-object p1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    :goto_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 138
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    aput-object v1, p1, v0

    const-string v0, "%s.setSignature() - state = %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->invalidate()V

    return-void
.end method

.method private updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V
    .locals 1

    .line 274
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    .line 275
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    if-eqz v0, :cond_3

    .line 276
    sget-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$1;->$SwitchMap$com$squareup$cardcustomizations$signature$Signature$SignatureState:[I

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 284
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;->onSigned()V

    goto :goto_0

    .line 281
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;->onStartedSigning()V

    goto :goto_0

    .line 278
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;->onClearedSignature()V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public canBeCleared()Z
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    sget-object v1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public clear()V
    .locals 4

    .line 80
    sget-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->clear()V

    .line 82
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getHeight()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/Signature;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->invalidate()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "%s.clear()"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public getPainterProvider()Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    return-object v0
.end method

.method public getSignature()Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 7

    .line 123
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    iget v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    .line 125
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getHeight()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/Signature;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    return-object v0
.end method

.method public hasSigned()Z
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    sget-object v1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    .line 153
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 245
    instance-of v0, p1, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;

    if-eqz v0, :cond_0

    .line 246
    check-cast p1, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;

    .line 247
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 248
    invoke-static {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->access$100(Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setSignature(Lcom/squareup/cardcustomizations/signature/Signature;)V

    goto :goto_0

    .line 251
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 256
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v1, :cond_0

    .line 259
    new-instance v2, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;

    invoke-direct {v2, v0, v1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/squareup/cardcustomizations/signature/Signature;)V

    return-object v2

    :cond_0
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 7

    .line 143
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 144
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_0

    .line 145
    iget v3, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    move v1, p1

    move v2, p2

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;->convertIfNecessary(Lcom/squareup/cardcustomizations/signature/Signature;IIFILcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .line 158
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-nez v0, :cond_1

    .line 164
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    iget v6, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v7, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    .line 165
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getHeight()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/cardcustomizations/signature/Signature;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 168
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 170
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    .line 172
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_a

    const/16 v7, 0xa

    if-eq v5, v6, :cond_2

    const/4 v8, 0x2

    if-eq v5, v8, :cond_2

    const/4 v8, 0x7

    if-eq v5, v8, :cond_2

    const/16 v8, 0x9

    if-eq v5, v8, :cond_a

    if-eq v5, v7, :cond_2

    new-array v0, v6, [Ljava/lang/Object;

    .line 220
    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Ignored touch event: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 188
    :cond_2
    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    sget-object v8, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    if-eq v5, v8, :cond_3

    return v1

    .line 196
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_4

    .line 198
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v8

    .line 199
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v9

    .line 200
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v10

    .line 202
    iget-object v12, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v12, v8, v9, v10, v11}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 205
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v6, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v7, :cond_7

    .line 206
    :cond_5
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    if-eqz v1, :cond_6

    invoke-interface {v1}, Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;->onGlyphAdded()V

    .line 207
    :cond_6
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Signature;->finishGlyph()V

    .line 211
    :cond_7
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    .line 214
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v6, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-ne p1, v7, :cond_9

    .line 215
    :cond_8
    sget-object p1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    .line 224
    :cond_9
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {p1, p0}, Lcom/squareup/cardcustomizations/signature/Signature;->invalidateCurrentGlyph(Landroid/view/View;)V

    return v6

    .line 175
    :cond_a
    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureStartBounds:Landroid/graphics/RectF;

    if-eqz v5, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    invoke-virtual {v5, v7, p1}, Landroid/graphics/RectF;->contains(FF)Z

    move-result p1

    if-eqz p1, :cond_b

    goto :goto_1

    :cond_b
    return v1

    .line 176
    :cond_c
    :goto_1
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    .line 177
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {p1, v0, v2, v3, v4}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    .line 178
    sget-object p1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    return v6
.end method

.method public restore(Ljava/lang/String;)V
    .locals 3

    .line 230
    invoke-static {}, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->access$000()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/cardcustomizations/signature/Signature;->decode(Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setSignature(Lcom/squareup/cardcustomizations/signature/Signature;)V

    return-void
.end method

.method public setBitmapProvider(Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;)V
    .locals 7

    .line 97
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    .line 98
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_0

    .line 99
    iget v1, v0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    iget v3, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;->convertIfNecessary(Lcom/squareup/cardcustomizations/signature/Signature;IIFILcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    :cond_0
    return-void
.end method

.method public setListener(Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->listener:Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    return-void
.end method

.method public setPainterProvider(Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V
    .locals 7

    .line 109
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    .line 110
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_0

    .line 111
    iget v1, v0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    iget v3, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;->convertIfNecessary(Lcom/squareup/cardcustomizations/signature/Signature;IIFILcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    :cond_0
    return-void
.end method

.method public setSignatureColor(I)V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 67
    iput p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    return-void
.end method

.method public setSignatureStartBounds(Landroid/graphics/RectF;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureStartBounds:Landroid/graphics/RectF;

    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 7

    .line 71
    iput p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->strokeWidth:F

    .line 72
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_0

    .line 73
    iget v1, v0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->color:I

    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->bitmapProvider:Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    move v3, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;->convertIfNecessary(Lcom/squareup/cardcustomizations/signature/Signature;IIFILcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    :cond_0
    return-void
.end method

.method public undo()V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->undo()V

    .line 235
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->hasGlyphs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    sget-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    goto :goto_0

    .line 238
    :cond_0
    sget-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    .line 239
    sget-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-direct {p0, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->updateState(Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;)V

    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 241
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView;->signatureState:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    aput-object v2, v0, v1

    const-string v1, "%s.undo() - state = %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
