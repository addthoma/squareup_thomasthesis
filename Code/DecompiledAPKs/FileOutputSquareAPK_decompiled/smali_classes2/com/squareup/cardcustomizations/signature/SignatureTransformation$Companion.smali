.class public final Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;
.super Ljava/lang/Object;
.source "PhysicalCard.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0087\u0002\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;",
        "",
        "()V",
        "invoke",
        "Lcom/squareup/cardcustomizations/signature/SignatureTransformation;",
        "signatureView",
        "Lcom/squareup/cardcustomizations/signature/SignatureView;",
        "stampView",
        "Lcom/squareup/cardcustomizations/stampview/StampView;",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 269
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;-><init>()V

    return-void
.end method

.method public static synthetic invoke$default(Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;Lcom/squareup/cardcustomizations/signature/SignatureView;Lcom/squareup/cardcustomizations/stampview/StampView;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 273
    check-cast p2, Lcom/squareup/cardcustomizations/stampview/StampView;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;->invoke(Lcom/squareup/cardcustomizations/signature/SignatureView;Lcom/squareup/cardcustomizations/stampview/StampView;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final invoke(Lcom/squareup/cardcustomizations/signature/SignatureView;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;->invoke$default(Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;Lcom/squareup/cardcustomizations/signature/SignatureView;Lcom/squareup/cardcustomizations/stampview/StampView;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cardcustomizations/signature/SignatureView;Lcom/squareup/cardcustomizations/stampview/StampView;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "signatureView"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/signature/Signature;->bounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/4 v3, 0x2

    new-array v4, v3, [I

    .line 288
    invoke-virtual {v0, v4}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getLocationInWindow([I)V

    const/4 v5, 0x0

    .line 289
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aget v7, v4, v5

    int-to-float v7, v7

    const/4 v8, 0x1

    aget v9, v4, v8

    int-to-float v9, v9

    invoke-virtual {v2, v7, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 290
    new-instance v7, Landroid/graphics/RectF;

    const/4 v9, 0x0

    if-eqz v1, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->bounds()Landroid/graphics/RectF;

    move-result-object v10

    if-eqz v10, :cond_0

    goto :goto_0

    :cond_0
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v9, v9, v9, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    :goto_0
    invoke-direct {v7, v10}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    new-array v10, v3, [I

    if-eqz v1, :cond_1

    .line 292
    invoke-virtual {v1, v10}, Lcom/squareup/cardcustomizations/stampview/StampView;->getLocationInWindow([I)V

    sget-object v6, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_1

    :cond_1
    new-instance v11, Lkotlin/Pair;

    invoke-direct {v11, v6, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 293
    :goto_1
    aget v6, v10, v5

    int-to-float v6, v6

    aget v11, v10, v8

    int-to-float v11, v11

    invoke-virtual {v7, v6, v11}, Landroid/graphics/RectF;->offset(FF)V

    .line 295
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 296
    invoke-virtual {v6, v7}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 298
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-double v11, v2

    const v2, 0x4484c000    # 1062.0f

    float-to-double v13, v2

    div-double/2addr v11, v13

    .line 299
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v7

    float-to-double v13, v7

    const/high16 v7, 0x43b10000    # 354.0f

    move-object v15, v10

    float-to-double v9, v7

    div-double/2addr v13, v9

    .line 298
    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->max(DD)D

    move-result-wide v9

    .line 300
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v11

    const-string v12, "signatureView.signature"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/squareup/cardcustomizations/signature/Signature;->getStrokeWidth()F

    move-result v11

    float-to-double v13, v11

    mul-double v13, v13, v9

    int-to-double v2, v3

    div-double/2addr v13, v2

    double-to-float v13, v13

    neg-float v13, v13

    .line 301
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->getStrokeWidth()F

    move-result v0

    float-to-double v11, v0

    mul-double v9, v9, v11

    div-double/2addr v9, v2

    double-to-float v0, v9

    neg-float v0, v0

    .line 300
    invoke-virtual {v6, v13, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 303
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 304
    aget v2, v4, v5

    int-to-float v2, v2

    neg-float v2, v2

    aget v3, v4, v8

    int-to-float v3, v3

    neg-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 306
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 309
    new-instance v3, Landroid/graphics/RectF;

    const v9, 0x4484c000    # 1062.0f

    const/4 v10, 0x0

    invoke-direct {v3, v10, v10, v9, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 310
    sget-object v9, Landroid/graphics/Matrix$ScaleToFit;->END:Landroid/graphics/Matrix$ScaleToFit;

    .line 307
    invoke-virtual {v2, v0, v3, v9}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 313
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 314
    aget v6, v15, v5

    int-to-float v6, v6

    neg-float v6, v6

    aget v9, v15, v8

    int-to-float v9, v9

    neg-float v9, v9

    invoke-virtual {v3, v6, v9}, Landroid/graphics/RectF;->offset(FF)V

    .line 316
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 319
    new-instance v9, Landroid/graphics/RectF;

    const v10, 0x4484c000    # 1062.0f

    const/4 v11, 0x0

    invoke-direct {v9, v11, v11, v10, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 320
    sget-object v7, Landroid/graphics/Matrix$ScaleToFit;->END:Landroid/graphics/Matrix$ScaleToFit;

    .line 317
    invoke-virtual {v6, v3, v9, v7}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 323
    new-instance v7, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    .line 324
    new-instance v9, Landroid/graphics/Point;

    aget v10, v4, v5

    aget v4, v4, v8

    invoke-direct {v9, v10, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 325
    new-instance v4, Landroid/graphics/Point;

    aget v5, v15, v5

    aget v8, v15, v8

    invoke-direct {v4, v5, v8}, Landroid/graphics/Point;-><init>(II)V

    if-eqz v1, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_2
    check-cast v1, Ljava/util/Collection;

    move-object/from16 v23, v1

    move-object/from16 v16, v7

    move-object/from16 v17, v0

    move-object/from16 v18, v2

    move-object/from16 v19, v9

    move-object/from16 v20, v3

    move-object/from16 v21, v6

    move-object/from16 v22, v4

    .line 323
    invoke-direct/range {v16 .. v23}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;-><init>(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)V

    return-object v7
.end method
