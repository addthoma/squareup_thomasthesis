.class public Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;
.super Ljava/lang/Object;
.source "Spliner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/Spliner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Bezier"
.end annotation


# instance fields
.field private final control1:Lcom/squareup/cardcustomizations/signature/Point;

.field private final control2:Lcom/squareup/cardcustomizations/signature/Point;

.field private final endPoint:Lcom/squareup/cardcustomizations/signature/Point;

.field private final path:Landroid/graphics/Path;

.field private final startPoint:Lcom/squareup/cardcustomizations/signature/Point;


# direct methods
.method public constructor <init>(Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 1

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    .line 232
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->startPoint:Lcom/squareup/cardcustomizations/signature/Point;

    .line 233
    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->endPoint:Lcom/squareup/cardcustomizations/signature/Point;

    .line 234
    iput-object p3, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control1:Lcom/squareup/cardcustomizations/signature/Point;

    .line 235
    iput-object p4, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control2:Lcom/squareup/cardcustomizations/signature/Point;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control2:Lcom/squareup/cardcustomizations/signature/Point;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->startPoint:Lcom/squareup/cardcustomizations/signature/Point;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control1:Lcom/squareup/cardcustomizations/signature/Point;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->endPoint:Lcom/squareup/cardcustomizations/signature/Point;

    return-object p0
.end method


# virtual methods
.method public bounds()Landroid/graphics/RectF;
    .locals 3

    .line 247
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 248
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10

    .line 240
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 241
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->startPoint:Lcom/squareup/cardcustomizations/signature/Point;

    iget v1, v1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->startPoint:Lcom/squareup/cardcustomizations/signature/Point;

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 242
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control1:Lcom/squareup/cardcustomizations/signature/Point;

    iget v4, v0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control1:Lcom/squareup/cardcustomizations/signature/Point;

    iget v5, v0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control2:Lcom/squareup/cardcustomizations/signature/Point;

    iget v6, v0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->control2:Lcom/squareup/cardcustomizations/signature/Point;

    iget v7, v0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->endPoint:Lcom/squareup/cardcustomizations/signature/Point;

    iget v8, v0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->endPoint:Lcom/squareup/cardcustomizations/signature/Point;

    iget v9, v0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 243
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method
