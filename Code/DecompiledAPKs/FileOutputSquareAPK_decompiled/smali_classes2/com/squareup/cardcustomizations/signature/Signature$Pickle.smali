.class Lcom/squareup/cardcustomizations/signature/Signature$Pickle;
.super Ljava/lang/Object;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Pickle"
.end annotation


# instance fields
.field final color:I

.field final glyphs:[[[I

.field final height:I

.field final strokeWidth:F

.field final width:I


# direct methods
.method constructor <init>(Lcom/squareup/cardcustomizations/signature/Signature;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 396
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 397
    iget v2, v1, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    iput v2, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->width:I

    .line 398
    iget v2, v1, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    iput v2, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->height:I

    .line 399
    iget-object v2, v1, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    iput v2, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->color:I

    .line 400
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardcustomizations/signature/Signature;->access$100(Lcom/squareup/cardcustomizations/signature/Signature;)F

    move-result v2

    iput v2, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->strokeWidth:F

    .line 402
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardcustomizations/signature/Signature;->glyphs()Ljava/util/List;

    move-result-object v1

    .line 403
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const-wide/16 v4, 0x0

    goto :goto_0

    .line 404
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-virtual {v4}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->getStartTime()J

    move-result-wide v4

    .line 406
    :goto_0
    new-array v6, v2, [[[I

    iput-object v6, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->glyphs:[[[I

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v2, :cond_2

    .line 408
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 409
    invoke-virtual {v7}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->points()Ljava/util/List;

    move-result-object v7

    .line 410
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 411
    iget-object v9, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->glyphs:[[[I

    new-array v10, v8, [[I

    aput-object v10, v9, v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_2
    if-ge v9, v8, :cond_1

    .line 414
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 415
    iget-object v12, v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->glyphs:[[[I

    aget-object v12, v12, v6

    add-int/lit8 v13, v10, 0x1

    const/4 v14, 0x3

    new-array v14, v14, [I

    iget v15, v11, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    float-to-int v15, v15

    aput v15, v14, v3

    iget v15, v11, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    float-to-int v15, v15

    const/16 v16, 0x1

    aput v15, v14, v16

    const/4 v15, 0x2

    move-object/from16 p1, v1

    iget-wide v0, v11, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    sub-long/2addr v0, v4

    long-to-int v1, v0

    aput v1, v14, v15

    aput-object v14, v12, v10

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v10, v13

    goto :goto_2

    :cond_1
    move-object/from16 p1, v1

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p0

    goto :goto_1

    :cond_2
    return-void
.end method
