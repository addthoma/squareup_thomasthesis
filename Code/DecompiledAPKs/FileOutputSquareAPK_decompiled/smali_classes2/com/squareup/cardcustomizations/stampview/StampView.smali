.class public final Lcom/squareup/cardcustomizations/stampview/StampView;
.super Landroid/view/View;
.source "StampView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;,
        Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;,
        Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;,
        Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;,
        Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;,
        Lcom/squareup/cardcustomizations/stampview/StampView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStampView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Point.kt\nandroidx/core/graphics/PointKt\n+ 4 Rect.kt\nandroidx/core/graphics/RectKt\n*L\n1#1,505:1\n1574#2,2:506\n81#3,3:508\n121#3,3:511\n164#3:514\n72#3,3:515\n72#3,3:518\n159#3:521\n164#3:528\n72#3,3:529\n121#3,3:532\n221#4,3:522\n221#4,3:525\n*E\n*S KotlinDebug\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView\n*L\n135#1,2:506\n195#1,3:508\n216#1,3:511\n216#1:514\n216#1,3:515\n216#1,3:518\n216#1:521\n216#1:528\n216#1,3:529\n350#1,3:532\n216#1,3:522\n216#1,3:525\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0007\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 q2\u00020\u0001:\u0006qrstuvB\u0019\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010P\u001a\u00020/2\u0006\u0010Q\u001a\u00020R2\u0006\u0010S\u001a\u00020T2\u0006\u0010U\u001a\u00020\u000bJ\u000e\u0010P\u001a\u00020/2\u0006\u0010Q\u001a\u00020VJ\u0008\u0010W\u001a\u0004\u0018\u00010TJ\u0006\u0010X\u001a\u00020/J\u0006\u0010Y\u001a\u00020\u0008J\u0008\u0010#\u001a\u00020$H\u0002J\u0006\u0010Z\u001a\u00020\u0008J\u0010\u0010[\u001a\u00020/2\u0006\u0010\u001a\u001a\u00020\u001bH\u0014J\u0010\u0010\\\u001a\u00020/2\u0006\u0010]\u001a\u00020^H\u0014J\u0008\u0010_\u001a\u00020^H\u0014J(\u0010`\u001a\u00020/2\u0006\u0010a\u001a\u00020\u000b2\u0006\u0010b\u001a\u00020\u000b2\u0006\u0010c\u001a\u00020\u000b2\u0006\u0010d\u001a\u00020\u000bH\u0014J\u0010\u0010e\u001a\u0002092\u0006\u0010f\u001a\u00020gH\u0016J\u0008\u0010h\u001a\u00020/H\u0002J\u000c\u0010i\u001a\u0008\u0012\u0004\u0012\u00020V0jJ\u0006\u0010k\u001a\u00020/J\u0014\u0010l\u001a\u00020/*\u00020V2\u0006\u0010W\u001a\u00020\u0015H\u0002J\u001c\u0010m\u001a\u00020/*\u00020&2\u0006\u0010n\u001a\u00020o2\u0006\u0010p\u001a\u00020oH\u0002R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000RL\u0010\t\u001a4\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\u000e\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\u000f\u0012\u0004\u0012\u00020\u0008\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\u001f\u001a\u0004\u0008\u001c\u0010\u001dR\u001c\u0010 \u001a\u0004\u0018\u00010\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\u0017\"\u0004\u0008\"\u0010\u0019R\u0010\u0010#\u001a\u0004\u0018\u00010$X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\'\u001a\u0004\u0018\u00010&2\u0008\u0010%\u001a\u0004\u0018\u00010&@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u0008(\u0010)R\u0014\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010-\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00080\u00101\"\u0004\u00082\u00103R\"\u00104\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00085\u00101\"\u0004\u00086\u00103R7\u00107\u001a\u001f\u0012\u0013\u0012\u001109\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(:\u0012\u0004\u0012\u00020/\u0018\u000108X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008;\u0010<\"\u0004\u0008=\u0010>R\u000e\u0010?\u001a\u00020@X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010A\u001a\u00020@8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008D\u0010\u001f\u001a\u0004\u0008B\u0010CR$\u0010E\u001a\u00020\u000b2\u0006\u0010%\u001a\u00020\u000b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008F\u0010G\"\u0004\u0008H\u0010IR$\u0010K\u001a\u00020J2\u0006\u0010%\u001a\u00020J8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008L\u0010M\"\u0004\u0008N\u0010O\u00a8\u0006w"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "bitmapProvider",
        "Lkotlin/Function2;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "width",
        "height",
        "getBitmapProvider",
        "()Lkotlin/jvm/functions/Function2;",
        "setBitmapProvider",
        "(Lkotlin/jvm/functions/Function2;)V",
        "boundingBox",
        "Landroid/graphics/Rect;",
        "getBoundingBox",
        "()Landroid/graphics/Rect;",
        "setBoundingBox",
        "(Landroid/graphics/Rect;)V",
        "canvas",
        "Landroid/graphics/Canvas;",
        "getCanvas",
        "()Landroid/graphics/Canvas;",
        "canvas$delegate",
        "Lkotlin/Lazy;",
        "deleteStampArea",
        "getDeleteStampArea",
        "setDeleteStampArea",
        "locationOnScreen",
        "Landroid/graphics/Point;",
        "value",
        "Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;",
        "movingStamp",
        "setMovingStamp",
        "(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V",
        "snapshots",
        "Ljava/util/ArrayDeque;",
        "Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;",
        "stampAddedOrRemovedListener",
        "Lkotlin/Function0;",
        "",
        "getStampAddedOrRemovedListener",
        "()Lkotlin/jvm/functions/Function0;",
        "setStampAddedOrRemovedListener",
        "(Lkotlin/jvm/functions/Function0;)V",
        "stampMovedListener",
        "getStampMovedListener",
        "setStampMovedListener",
        "stampMovingListener",
        "Lkotlin/Function1;",
        "",
        "inDeleteArea",
        "getStampMovingListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setStampMovingListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "stampPaint",
        "Landroid/graphics/Paint;",
        "stampPaintOverride",
        "getStampPaintOverride",
        "()Landroid/graphics/Paint;",
        "stampPaintOverride$delegate",
        "strokeColor",
        "getStrokeColor",
        "()I",
        "setStrokeColor",
        "(I)V",
        "",
        "strokeWidth",
        "getStrokeWidth",
        "()F",
        "setStrokeWidth",
        "(F)V",
        "addStamp",
        "stamp",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "destination",
        "Landroid/graphics/RectF;",
        "minScale",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "bounds",
        "clear",
        "getBitmap",
        "makeBitmap",
        "onDraw",
        "onRestoreInstanceState",
        "state",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "onSizeChanged",
        "w",
        "h",
        "oldw",
        "oldh",
        "onTouchEvent",
        "event",
        "Landroid/view/MotionEvent;",
        "redraw",
        "stamps",
        "",
        "undo",
        "animateToBounds",
        "postTwoFingerTransforms",
        "firstFingerPoint",
        "Landroid/graphics/PointF;",
        "secondFingerPoint",
        "Companion",
        "MatrixAdapter",
        "MovingStamp",
        "Snapshot",
        "StampAdapter",
        "TransformedStamp",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private bitmapProvider:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private boundingBox:Landroid/graphics/Rect;

.field private final canvas$delegate:Lkotlin/Lazy;

.field private deleteStampArea:Landroid/graphics/Rect;

.field private locationOnScreen:Landroid/graphics/Point;

.field private movingStamp:Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

.field private final snapshots:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;",
            ">;"
        }
    .end annotation
.end field

.field private stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private stampMovedListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private stampMovingListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final stampPaint:Landroid/graphics/Paint;

.field private final stampPaintOverride$delegate:Lkotlin/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/cardcustomizations/stampview/StampView;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "canvas"

    const-string v5, "getCanvas()Landroid/graphics/Canvas;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "stampPaintOverride"

    const-string v4, "getStampPaintOverride()Landroid/graphics/Paint;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/cardcustomizations/stampview/StampView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    new-instance p1, Lcom/squareup/cardcustomizations/stampview/StampView$canvas$2;

    invoke-direct {p1, p0}, Lcom/squareup/cardcustomizations/stampview/StampView$canvas$2;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->canvas$delegate:Lkotlin/Lazy;

    .line 67
    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    .line 68
    new-instance p1, Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;

    invoke-direct {p1, p0}, Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaintOverride$delegate:Lkotlin/Lazy;

    .line 69
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    .line 87
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 90
    invoke-virtual {p0, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setSaveEnabled(Z)V

    return-void
.end method

.method public static final synthetic access$getStampPaint$p(Lcom/squareup/cardcustomizations/stampview/StampView;)Landroid/graphics/Paint;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic access$redraw(Lcom/squareup/cardcustomizations/stampview/StampView;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    return-void
.end method

.method private final animateToBounds(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/Rect;)V
    .locals 8

    .line 343
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 345
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v1

    .line 347
    invoke-static {v1}, Lcom/squareup/cardcustomizations/geometry/RectKt;->centerPoint(Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 348
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-static {v2, v3}, Lcom/squareup/cardcustomizations/geometry/PointKt;->insideBounds(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v3

    .line 349
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    xor-int/2addr v4, v5

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    .line 532
    new-instance v4, Landroid/graphics/PointF;

    iget v7, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v7, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 533
    iget v3, v2, Landroid/graphics/PointF;->x:F

    neg-float v3, v3

    iget v2, v2, Landroid/graphics/PointF;->y:F

    neg-float v2, v2

    invoke-virtual {v4, v3, v2}, Landroid/graphics/PointF;->offset(FF)V

    new-array v2, v6, [F

    .line 351
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 352
    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    new-instance v3, Landroid/graphics/PointF;

    const/4 v6, 0x0

    invoke-direct {v3, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 353
    new-instance v3, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;

    invoke-direct {v3, p1, v4, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    check-cast v3, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v6, 0x1

    .line 362
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2

    .line 363
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getMinHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_2

    .line 364
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/cardcustomizations/geometry/MatrixKt;->getScale(Landroid/graphics/Matrix;)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 365
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getMinHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getMinHeight()I

    move-result p2

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    :goto_1
    int-to-float p2, p2

    .line 366
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getPathBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getPathBounds()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    div-float/2addr p2, v1

    .line 367
    new-instance v1, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;

    invoke-direct {v1, p1, v2, p2}, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;FF)V

    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :goto_2
    if-eqz v5, :cond_4

    .line 375
    new-instance p1, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$3;

    invoke-direct {p1, p0}, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$3;-><init>(Lcom/squareup/cardcustomizations/stampview/StampView;)V

    check-cast p1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 p1, 0x2

    new-array p1, p1, [F

    .line 376
    fill-array-data p1, :array_0

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    const-wide/16 p1, 0xfa

    .line 377
    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 378
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_4
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private final getCanvas()Landroid/graphics/Canvas;
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->canvas$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/cardcustomizations/stampview/StampView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Canvas;

    return-object v0
.end method

.method private final getStampPaintOverride()Landroid/graphics/Paint;
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaintOverride$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/cardcustomizations/stampview/StampView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final locationOnScreen()Landroid/graphics/Point;
    .locals 4

    .line 383
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 385
    invoke-virtual {p0, v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getLocationOnScreen([I)V

    .line 387
    new-instance v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 388
    iput-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen:Landroid/graphics/Point;

    return-object v1
.end method

.method private final postTwoFingerTransforms(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4

    .line 329
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getSecondLocation()Landroid/graphics/PointF;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 330
    invoke-virtual {p1, p2, p3}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->computeScale(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    .line 331
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    .line 332
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object v3

    if-eqz v3, :cond_1

    iget v3, v3, Landroid/graphics/PointF;->y:F

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    .line 331
    :goto_1
    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 333
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, p2, p3}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->computeAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result p3

    .line 334
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    goto :goto_2

    :cond_2
    iget v1, p2, Landroid/graphics/PointF;->x:F

    :goto_2
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object p1

    if-eqz p1, :cond_3

    iget p1, p1, Landroid/graphics/PointF;->y:F

    goto :goto_3

    :cond_3
    iget p1, p2, Landroid/graphics/PointF;->y:F

    .line 333
    :goto_3
    invoke-virtual {v0, p3, v1, p1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    :cond_4
    return-void
.end method

.method private final redraw()V
    .locals 3

    .line 183
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 184
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->invalidate()V

    return-void
.end method

.method private final setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V
    .locals 1

    .line 82
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->movingStamp:Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method


# virtual methods
.method public final addStamp(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/RectF;I)V
    .locals 8

    const-string v0, "stamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destination"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 163
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getCanvasBounds()Landroid/graphics/RectF;

    move-result-object v0

    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v0, p2, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 164
    new-instance p2, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    int-to-float p3, p3

    const/high16 v0, 0x42c80000    # 100.0f

    div-float v4, p3, v0

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;-><init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 165
    invoke-virtual {p0, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->addStamp(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)V

    return-void
.end method

.method public final addStamp(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)V
    .locals 1

    const-string v0, "stamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;-><init>(Ljava/util/List;)V

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    invoke-virtual {v0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copyWith(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object v0

    .line 155
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {p1, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 156
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "add() - "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " snapshots"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 158
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public final bounds()Landroid/graphics/RectF;
    .locals 2

    .line 100
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->bounds()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final clear()V
    .locals 4

    .line 169
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    new-instance v1, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 170
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clear() - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " snapshots"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    :cond_0
    return-void
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    return-object v0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->makeBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 76
    iput-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final getBitmapProvider()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmapProvider:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getBoundingBox()Landroid/graphics/Rect;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->boundingBox:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getDeleteStampArea()Landroid/graphics/Rect;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->deleteStampArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getStampAddedOrRemovedListener()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getStampMovedListener()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovedListener:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getStampMovingListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovingListener:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getStrokeColor()I
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public final getStrokeWidth()F
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public final makeBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmapProvider:Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    .line 63
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Set a Bitmap Provider"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 106
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 109
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "instance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string/jumbo v0, "width"

    .line 111
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "height"

    .line 112
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_4

    if-nez v1, :cond_1

    goto :goto_1

    .line 117
    :cond_1
    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->clear()V

    .line 118
    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmapProvider:Lkotlin/jvm/functions/Function2;

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    .line 119
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    const-string v1, "count"

    .line 120
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_3

    .line 121
    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_3
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    :cond_4
    :goto_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .line 128
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 129
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "instance"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getWidth()I

    move-result v1

    const-string/jumbo v2, "width"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getHeight()I

    move-result v1

    const-string v2, "height"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    const-string v2, "count"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    check-cast v1, Ljava/lang/Iterable;

    .line 506
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    add-int/lit8 v4, v2, 0x1

    .line 136
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move v2, v4

    goto :goto_0

    .line 138
    :cond_0
    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 142
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 143
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    if-ne p3, p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    if-ne p1, p2, :cond_0

    return-void

    .line 145
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmapProvider:Lkotlin/jvm/functions/Function2;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getWidth()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getHeight()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    .line 146
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getCanvas()Landroid/graphics/Canvas;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 147
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 190
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    return v1

    .line 193
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->movingStamp:Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_2

    goto/16 :goto_4

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->movingStamp:Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    if-eqz v0, :cond_10

    .line 217
    sget-object v5, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v5, p1, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v5

    .line 218
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getDownLocation()Landroid/graphics/PointF;

    move-result-object v6

    .line 511
    new-instance v7, Landroid/graphics/PointF;

    iget v8, v5, Landroid/graphics/PointF;->x:F

    iget v9, v5, Landroid/graphics/PointF;->y:F

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 512
    iget v8, v6, Landroid/graphics/PointF;->x:F

    neg-float v8, v8

    iget v6, v6, Landroid/graphics/PointF;->y:F

    neg-float v6, v6

    invoke-virtual {v7, v8, v6}, Landroid/graphics/PointF;->offset(FF)V

    .line 219
    move-object v13, v4

    check-cast v13, [F

    .line 220
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    const/16 v8, 0x9

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_3

    :pswitch_0
    new-array v13, v8, [F

    .line 280
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/graphics/Matrix;->getValues([F)V

    .line 281
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object v3

    if-nez v3, :cond_3

    .line 282
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    iget v6, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 284
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-le v3, v2, :cond_4

    .line 285
    sget-object v3, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v3, p1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v0, v5, v3}, Lcom/squareup/cardcustomizations/stampview/StampView;->postTwoFingerTransforms(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 287
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xc

    const/4 v12, 0x0

    move-object v6, v3

    move-object v7, v5

    invoke-direct/range {v6 .. v12}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;-><init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_5
    move-object v3, v4

    :goto_0
    invoke-direct {p0, v3}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    .line 288
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    if-nez v3, :cond_6

    .line 289
    iget-object v3, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->boundingBox:Landroid/graphics/Rect;

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v6

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen()Landroid/graphics/Point;

    move-result-object v7

    .line 525
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 526
    iget v3, v7, Landroid/graphics/Point;->x:I

    neg-int v3, v3

    iget v7, v7, Landroid/graphics/Point;->y:I

    neg-int v7, v7

    invoke-virtual {v8, v3, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 289
    invoke-direct {p0, v6, v8}, Lcom/squareup/cardcustomizations/stampview/StampView;->animateToBounds(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/Rect;)V

    .line 291
    :cond_6
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    goto/16 :goto_3

    .line 222
    :pswitch_1
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    iget v6, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    const/4 v8, 0x0

    .line 225
    new-instance v9, Landroid/graphics/PointF;

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-direct {v9, v3, v6}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v10, 0x0

    const/16 v11, 0xa

    const/4 v12, 0x0

    move-object v6, v0

    move-object v7, v5

    .line 223
    invoke-static/range {v6 .. v12}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->copy$default(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    goto/16 :goto_3

    .line 295
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->undo()V

    .line 296
    move-object v3, v4

    check-cast v3, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-direct {p0, v3}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    goto/16 :goto_3

    :pswitch_3
    new-array v6, v8, [F

    .line 230
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/graphics/Matrix;->getValues([F)V

    .line 231
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getPivot()Landroid/graphics/PointF;

    move-result-object v8

    if-nez v8, :cond_7

    .line 232
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v8

    iget v9, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 234
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-le v7, v2, :cond_8

    .line 235
    sget-object v7, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v7, p1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v7

    invoke-direct {p0, v0, v5, v7}, Lcom/squareup/cardcustomizations/stampview/StampView;->postTwoFingerTransforms(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 239
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result p1

    if-ne p1, v2, :cond_b

    .line 240
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->deleteStampArea:Landroid/graphics/Rect;

    if-eqz p1, :cond_b

    .line 241
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen()Landroid/graphics/Point;

    move-result-object v7

    .line 514
    new-instance v8, Landroid/graphics/Point;

    iget v9, v5, Landroid/graphics/PointF;->x:F

    float-to-int v9, v9

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-int v5, v5

    invoke-direct {v8, v9, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 515
    new-instance v5, Landroid/graphics/Point;

    iget v9, v7, Landroid/graphics/Point;->x:I

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v9, v7}, Landroid/graphics/Point;-><init>(II)V

    .line 516
    iget v7, v8, Landroid/graphics/Point;->x:I

    iget v8, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v7, v8}, Landroid/graphics/Point;->offset(II)V

    .line 243
    invoke-static {p1}, Lcom/squareup/cardcustomizations/geometry/RectKt;->centerPoint(Landroid/graphics/Rect;)Landroid/graphics/Point;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/squareup/cardcustomizations/geometry/PointKt;->distance(Landroid/graphics/Point;Landroid/graphics/Point;)F

    move-result v7

    .line 244
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    cmpl-float v8, v7, v8

    if-lez v8, :cond_9

    goto/16 :goto_1

    .line 249
    :cond_9
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 251
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    mul-float v8, v8, v3

    .line 252
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    mul-float v10, v7, v9

    cmpg-float v10, v10, v8

    if-gez v10, :cond_a

    div-float v7, v8, v9

    .line 257
    :cond_a
    invoke-static {p1}, Lcom/squareup/cardcustomizations/geometry/RectKt;->centerPoint(Landroid/graphics/Rect;)Landroid/graphics/Point;

    move-result-object p1

    .line 518
    new-instance v8, Landroid/graphics/Point;

    iget v9, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-direct {v8, v9, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 519
    iget p1, v5, Landroid/graphics/Point;->x:I

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v8, p1, v5}, Landroid/graphics/Point;->offset(II)V

    .line 521
    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1, v8}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    .line 257
    invoke-static {p1, v3}, Lcom/squareup/cardcustomizations/geometry/PointKt;->times(Landroid/graphics/PointF;F)Landroid/graphics/PointF;

    move-result-object p1

    .line 258
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    iget v5, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v7, v7, v5, p1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 259
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getStampPaintOverride()Landroid/graphics/Paint;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    mul-float v3, v3, v7

    invoke-virtual {p1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 260
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object p1

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getStampPaintOverride()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->setPaintOverride(Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_b
    :goto_1
    const/4 v2, 0x0

    .line 264
    :goto_2
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 265
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object p1

    check-cast v4, Landroid/graphics/Paint;

    invoke-virtual {p1, v4}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->setPaintOverride(Landroid/graphics/Paint;)V

    .line 266
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/graphics/Matrix;->setValues([F)V

    .line 267
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovingListener:Lkotlin/jvm/functions/Function1;

    if-eqz p1, :cond_c

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_c
    return v1

    :pswitch_4
    new-array v13, v8, [F

    .line 272
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/graphics/Matrix;->getValues([F)V

    .line 273
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v3

    iget v6, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 274
    move-object v3, v4

    check-cast v3, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-direct {p0, v3}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    .line 275
    iget-object v3, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->boundingBox:Landroid/graphics/Rect;

    if-eqz v3, :cond_d

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v6

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen()Landroid/graphics/Point;

    move-result-object v7

    .line 522
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 523
    iget v3, v7, Landroid/graphics/Point;->x:I

    neg-int v3, v3

    iget v7, v7, Landroid/graphics/Point;->y:I

    neg-int v7, v7

    invoke-virtual {v8, v3, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 275
    invoke-direct {p0, v6, v8}, Lcom/squareup/cardcustomizations/stampview/StampView;->animateToBounds(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/Rect;)V

    .line 276
    :cond_d
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 300
    :goto_3
    iget-object v3, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovedListener:Lkotlin/jvm/functions/Function0;

    if-eqz v3, :cond_e

    invoke-interface {v3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/Unit;

    .line 301
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result p1

    if-ne p1, v2, :cond_10

    .line 302
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->deleteStampArea:Landroid/graphics/Rect;

    if-eqz p1, :cond_10

    .line 303
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->locationOnScreen()Landroid/graphics/Point;

    move-result-object v2

    .line 528
    new-instance v3, Landroid/graphics/Point;

    iget v6, v5, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    iget v5, v5, Landroid/graphics/PointF;->y:F

    float-to-int v5, v5

    invoke-direct {v3, v6, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 529
    new-instance v5, Landroid/graphics/Point;

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-direct {v5, v6, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 530
    iget v2, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Point;->offset(II)V

    .line 307
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, v5, Landroid/graphics/Point;->x:I

    if-gt v2, v3, :cond_10

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, v5, Landroid/graphics/Point;->x:I

    if-lt v2, v3, :cond_10

    iget p1, p1, Landroid/graphics/Rect;->top:I

    iget v2, v5, Landroid/graphics/Point;->y:I

    if-gt p1, v2, :cond_10

    if-eqz v13, :cond_f

    .line 310
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object p1

    invoke-virtual {p1, v13}, Landroid/graphics/Matrix;->setValues([F)V

    .line 313
    :cond_f
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copyWithout(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 314
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 315
    check-cast v4, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-direct {p0, v4}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    .line 316
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_10

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_10
    return v1

    .line 194
    :cond_11
    :goto_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v2, :cond_12

    .line 195
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v0, p1, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v0

    sget-object v5, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v5, p1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v5

    .line 508
    new-instance v6, Landroid/graphics/PointF;

    iget v7, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v7, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 509
    iget v0, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v0, v5}, Landroid/graphics/PointF;->offset(FF)V

    .line 195
    invoke-static {v6, v3}, Lcom/squareup/cardcustomizations/geometry/PointKt;->times(Landroid/graphics/PointF;F)Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_5

    .line 197
    :cond_12
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v0, p1, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v0

    .line 199
    :goto_5
    sget-object v3, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    iget-object v5, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v3, v5}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v3, :cond_13

    invoke-virtual {v3, v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stampAtOrNull(Landroid/graphics/PointF;)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v3

    goto :goto_6

    :cond_13
    move-object v3, v4

    :goto_6
    if-eqz v3, :cond_16

    const/4 v6, 0x0

    .line 201
    new-instance v7, Landroid/graphics/Matrix;

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-direct {v7, v5}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd

    const/4 v11, 0x0

    move-object v5, v3

    invoke-static/range {v5 .. v11}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->copy$default(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FIILjava/lang/Object;)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object v5

    .line 202
    new-instance v6, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    .line 203
    sget-object v7, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v7, p1, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v1

    .line 205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-le v7, v2, :cond_14

    sget-object v7, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    invoke-static {v7, p1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object v7

    goto :goto_7

    :cond_14
    move-object v7, v4

    .line 206
    :goto_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result p1

    if-le p1, v2, :cond_15

    goto :goto_8

    :cond_15
    move-object v0, v4

    .line 202
    :goto_8
    invoke-direct {v6, v1, v5, v7, v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;-><init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    invoke-direct {p0, v6}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    .line 208
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    invoke-virtual {v0, v3}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copyWithout(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copyWith(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_9

    .line 210
    :cond_16
    check-cast v4, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-direct {p0, v4}, Lcom/squareup/cardcustomizations/stampview/StampView;->setMovingStamp(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;)V

    .line 211
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :goto_9
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final setBitmapProvider(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmapProvider:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setBoundingBox(Landroid/graphics/Rect;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->boundingBox:Landroid/graphics/Rect;

    return-void
.end method

.method public final setDeleteStampArea(Landroid/graphics/Rect;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->deleteStampArea:Landroid/graphics/Rect;

    return-void
.end method

.method public final setStampAddedOrRemovedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 57
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setStampMovedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 56
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setStampMovingListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 55
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampMovingListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setStrokeColor(I)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    :cond_0
    return-void
.end method

.method public final setStrokeWidth(F)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 42
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    :cond_0
    return-void
.end method

.method public final stamps()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 150
    sget-object v0, Lcom/squareup/cardcustomizations/stampview/StampView;->Companion:Lcom/squareup/cardcustomizations/stampview/StampView$Companion;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->getStamps()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final undo()V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    .line 177
    :cond_0
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView;->redraw()V

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "undo() - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->snapshots:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " snapshots"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView;->stampAddedOrRemovedListener:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    :cond_1
    return-void
.end method
