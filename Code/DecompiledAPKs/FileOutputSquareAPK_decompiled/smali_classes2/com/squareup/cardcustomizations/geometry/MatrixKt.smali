.class public final Lcom/squareup/cardcustomizations/geometry/MatrixKt;
.super Ljava/lang/Object;
.source "Matrix.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u001e\u0010\u0003\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "getScale",
        "Landroid/graphics/PointF;",
        "Landroid/graphics/Matrix;",
        "updateScale",
        "",
        "scaleVector",
        "pivot",
        "card-customization_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# direct methods
.method public static final getScale(Landroid/graphics/Matrix;)Landroid/graphics/PointF;
    .locals 7

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 9
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 p0, 0x0

    .line 10
    aget v1, v0, p0

    aget v2, v0, p0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    float-to-double v1, v1

    aget v3, v0, p0

    aget p0, v0, p0

    mul-float v3, v3, p0

    const/4 p0, 0x1

    aget v4, v0, p0

    aget p0, v0, p0

    mul-float v4, v4, p0

    add-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    mul-double v1, v1, v3

    const/4 p0, 0x4

    .line 11
    aget v3, v0, p0

    aget v4, v0, p0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v3, v4

    float-to-double v3, v3

    const/4 v5, 0x3

    aget v6, v0, v5

    aget v5, v0, v5

    mul-float v6, v6, v5

    aget v5, v0, p0

    aget p0, v0, p0

    mul-float v5, v5, p0

    add-float/2addr v6, v5

    float-to-double v5, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    mul-double v3, v3, v5

    .line 12
    new-instance p0, Landroid/graphics/PointF;

    double-to-float v0, v1

    double-to-float v1, v3

    invoke-direct {p0, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object p0
.end method

.method public static final updateScale(Landroid/graphics/Matrix;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 12

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scaleVector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 17
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v1, 0x3

    .line 18
    aget v2, v0, v1

    float-to-double v2, v2

    const/4 v4, 0x4

    aget v5, v0, v4

    float-to-double v5, v5

    invoke-static {v2, v3, v5, v6}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 19
    iget v5, p1, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double v5, v5, v7

    double-to-float v5, v5

    const/4 v6, 0x0

    aput v5, v0, v6

    .line 20
    iget v5, p1, Landroid/graphics/PointF;->x:F

    neg-float v5, v5

    float-to-double v7, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    mul-double v7, v7, v9

    double-to-float v5, v7

    const/4 v7, 0x1

    aput v5, v0, v7

    .line 21
    iget v5, p1, Landroid/graphics/PointF;->y:F

    float-to-double v8, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double v8, v8, v10

    double-to-float v5, v8

    aput v5, v0, v1

    .line 22
    iget p1, p1, Landroid/graphics/PointF;->y:F

    float-to-double v8, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    mul-double v8, v8, v1

    double-to-float p1, v8

    aput p1, v0, v4

    if-eqz p2, :cond_0

    .line 25
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 26
    invoke-virtual {p0, p1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    const/4 v1, 0x2

    new-array v2, v1, [F

    .line 27
    iget v3, p2, Landroid/graphics/PointF;->x:F

    aput v3, v2, v6

    iget v3, p2, Landroid/graphics/PointF;->y:F

    aput v3, v2, v7

    .line 28
    invoke-virtual {p1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 29
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 30
    invoke-virtual {p0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 31
    aget p1, v0, v1

    iget v3, p2, Landroid/graphics/PointF;->x:F

    aget v4, v2, v6

    sub-float/2addr v3, v4

    add-float/2addr p1, v3

    aput p1, v0, v1

    const/4 p1, 0x5

    .line 32
    aget v1, v0, p1

    iget p2, p2, Landroid/graphics/PointF;->y:F

    aget v2, v2, v7

    sub-float/2addr p2, v2

    add-float/2addr v1, p2

    aput v1, v0, p1

    .line 33
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    :goto_0
    return-void
.end method

.method public static synthetic updateScale$default(Landroid/graphics/Matrix;Landroid/graphics/PointF;Landroid/graphics/PointF;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 15
    check-cast p2, Landroid/graphics/PointF;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/cardcustomizations/geometry/MatrixKt;->updateScale(Landroid/graphics/Matrix;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-void
.end method
