.class final Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealGlobalStateBuyerLanguageSelectionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/buyer/language/Exit;",
        "output",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;->this$0:Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    instance-of v0, p1, Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput$LanguageSelected;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;->this$0:Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;

    invoke-static {v0}, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->access$getBuyerLocaleOverride$p(Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-result-object v0

    check-cast p1, Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput$LanguageSelected;

    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput$LanguageSelected;->getLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->updateLocale(Ljava/util/Locale;)V

    .line 40
    :cond_0
    iget-object p1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;->this$0:Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;

    sget-object v0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1$1;->INSTANCE:Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;->invoke(Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
