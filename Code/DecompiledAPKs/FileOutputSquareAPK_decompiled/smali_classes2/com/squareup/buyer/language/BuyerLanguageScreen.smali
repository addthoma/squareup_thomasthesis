.class public final Lcom/squareup/buyer/language/BuyerLanguageScreen;
.super Ljava/lang/Object;
.source "BuyerLanguageScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008H\u00c6\u0003J\u000f\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000cH\u00c6\u0003JW\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00082\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000cH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000fR\u001d\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u000f\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/buyer/language/BuyerLanguageScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "englishDisplay",
        "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
        "canadianFrenchDisplay",
        "usSpanishDisplay",
        "japaneseDisplay",
        "onLocaleSelected",
        "Lkotlin/Function1;",
        "Ljava/util/Locale;",
        "",
        "goBack",
        "Lkotlin/Function0;",
        "(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getCanadianFrenchDisplay",
        "()Lcom/squareup/buyer/language/LocaleAndDisplayText;",
        "getEnglishDisplay",
        "getGoBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getJapaneseDisplay",
        "getOnLocaleSelected",
        "()Lkotlin/jvm/functions/Function1;",
        "getUsSpanishDisplay",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

.field private final englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

.field private final goBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

.field private final onLocaleSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/util/Locale;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/Locale;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "englishDisplay"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "canadianFrenchDisplay"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usSpanishDisplay"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "japaneseDisplay"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLocaleSelected"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "goBack"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iput-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iput-object p3, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iput-object p4, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iput-object p5, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    iput-object p6, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/buyer/language/BuyerLanguageScreen;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/buyer/language/BuyerLanguageScreen;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/buyer/language/BuyerLanguageScreen;->copy(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/buyer/language/BuyerLanguageScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final component2()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final component3()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final component4()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/util/Locale;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/buyer/language/BuyerLanguageScreen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/Locale;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/buyer/language/BuyerLanguageScreen;"
        }
    .end annotation

    const-string v0, "englishDisplay"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "canadianFrenchDisplay"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usSpanishDisplay"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "japaneseDisplay"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLocaleSelected"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "goBack"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/buyer/language/BuyerLanguageScreen;-><init>(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iget-object v1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iget-object v1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iget-object v1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    iget-object v1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanadianFrenchDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final getEnglishDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final getGoBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getJapaneseDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public final getOnLocaleSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/util/Locale;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getUsSpanishDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BuyerLanguageScreen(englishDisplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->englishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canadianFrenchDisplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->canadianFrenchDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", usSpanishDisplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->usSpanishDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", japaneseDisplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->japaneseDisplay:Lcom/squareup/buyer/language/LocaleAndDisplayText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onLocaleSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->onLocaleSelected:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", goBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreen;->goBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
