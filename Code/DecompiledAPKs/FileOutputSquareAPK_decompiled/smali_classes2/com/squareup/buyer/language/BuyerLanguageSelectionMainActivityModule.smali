.class public abstract Lcom/squareup/buyer/language/BuyerLanguageSelectionMainActivityModule;
.super Ljava/lang/Object;
.source "BuyerLanguageSelectionMainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindBuyerLanguageSelectionViewFactory(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionViewFactory;)Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindBuyerLanguageSelectionWorkflow(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindBuyerLanguageSelectionWorkflowRunner(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;)Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindBuyerLocaleEnglishSelector(Lcom/squareup/buyer/language/RealBuyerLocaleEnglishSelector;)Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindGlobalStateBuyerLanguageSelectionWorkflow(Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindLanguageSelectionIntoBuyer(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindLanguageSelectionIntoMainActivity(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
