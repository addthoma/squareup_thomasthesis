.class public final enum Lcom/squareup/card/ConnectV2CardBrandMapping;
.super Ljava/lang/Enum;
.source "ConnectCardBrandConverter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/card/ConnectV2CardBrandMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/card/ConnectV2CardBrandMapping;",
        "",
        "connectV2CardBrand",
        "Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
        "cardBrand",
        "Lcom/squareup/Card$Brand;",
        "(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V",
        "getCardBrand$proto_utilities_release",
        "()Lcom/squareup/Card$Brand;",
        "getConnectV2CardBrand$proto_utilities_release",
        "()Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
        "VISA",
        "AMEX",
        "MASTERCARD",
        "DISCOVER",
        "DISCOVER_DINERS",
        "JCB",
        "CHINA_UNION_PAY",
        "SQUARE_GIFT_CARD",
        "OTHER_BRAND",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum AMEX:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum CHINA_UNION_PAY:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum DISCOVER:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum JCB:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum MASTERCARD:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum OTHER_BRAND:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/card/ConnectV2CardBrandMapping;

.field public static final enum VISA:Lcom/squareup/card/ConnectV2CardBrandMapping;


# instance fields
.field private final cardBrand:Lcom/squareup/Card$Brand;

.field private final connectV2CardBrand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/card/ConnectV2CardBrandMapping;

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 19
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->VISA:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 20
    sget-object v3, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    const/4 v4, 0x0

    const-string v5, "VISA"

    .line 18
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->VISA:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 23
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 24
    sget-object v3, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    const/4 v4, 0x1

    const-string v5, "AMEX"

    .line 22
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->AMEX:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 27
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->MASTERCARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 28
    sget-object v3, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    const/4 v4, 0x2

    const-string v5, "MASTERCARD"

    .line 26
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->MASTERCARD:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 31
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 32
    sget-object v3, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    const/4 v4, 0x3

    const-string v5, "DISCOVER"

    .line 30
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->DISCOVER:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 35
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 36
    sget-object v3, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    const/4 v4, 0x4

    const-string v5, "DISCOVER_DINERS"

    .line 34
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->DISCOVER_DINERS:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 39
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->JCB:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 40
    sget-object v3, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    const/4 v4, 0x5

    const-string v5, "JCB"

    .line 38
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->JCB:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 43
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 44
    sget-object v3, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    const/4 v4, 0x6

    const-string v5, "CHINA_UNION_PAY"

    .line 42
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->CHINA_UNION_PAY:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 47
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 48
    sget-object v3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    const/4 v4, 0x7

    const-string v5, "SQUARE_GIFT_CARD"

    .line 46
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->SQUARE_GIFT_CARD:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/ConnectV2CardBrandMapping;

    .line 51
    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Card$Brand;->OTHER_BRAND:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    .line 52
    sget-object v3, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    const/16 v4, 0x8

    const-string v5, "OTHER_BRAND"

    .line 50
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/ConnectV2CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V

    sput-object v1, Lcom/squareup/card/ConnectV2CardBrandMapping;->OTHER_BRAND:Lcom/squareup/card/ConnectV2CardBrandMapping;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/card/ConnectV2CardBrandMapping;->$VALUES:[Lcom/squareup/card/ConnectV2CardBrandMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/connect/v2/resources/Card$Brand;Lcom/squareup/Card$Brand;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
            "Lcom/squareup/Card$Brand;",
            ")V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/card/ConnectV2CardBrandMapping;->connectV2CardBrand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    iput-object p4, p0, Lcom/squareup/card/ConnectV2CardBrandMapping;->cardBrand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/card/ConnectV2CardBrandMapping;
    .locals 1

    const-class v0, Lcom/squareup/card/ConnectV2CardBrandMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/card/ConnectV2CardBrandMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/card/ConnectV2CardBrandMapping;
    .locals 1

    sget-object v0, Lcom/squareup/card/ConnectV2CardBrandMapping;->$VALUES:[Lcom/squareup/card/ConnectV2CardBrandMapping;

    invoke-virtual {v0}, [Lcom/squareup/card/ConnectV2CardBrandMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/card/ConnectV2CardBrandMapping;

    return-object v0
.end method


# virtual methods
.method public final getCardBrand$proto_utilities_release()Lcom/squareup/Card$Brand;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/card/ConnectV2CardBrandMapping;->cardBrand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public final getConnectV2CardBrand$proto_utilities_release()Lcom/squareup/protos/connect/v2/resources/Card$Brand;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/card/ConnectV2CardBrandMapping;->connectV2CardBrand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object v0
.end method
