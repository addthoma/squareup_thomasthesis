.class public final Lcom/squareup/SquareDeviceTourSettings$NoTourSettings;
.super Lcom/squareup/SquareDeviceTourSettings;
.source "SquareDeviceTourSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/SquareDeviceTourSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoTourSettings"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0004H\u0016J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/SquareDeviceTourSettings$NoTourSettings;",
        "Lcom/squareup/SquareDeviceTourSettings;",
        "()V",
        "deviceTourViewed",
        "",
        "featureTourViewed",
        "notifyTourShowing",
        "shouldShowDeviceTour",
        "",
        "shouldShowFeatureTour",
        "square-device-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lcom/squareup/SquareDeviceTourSettings;-><init>()V

    return-void
.end method


# virtual methods
.method public deviceTourViewed()V
    .locals 0

    return-void
.end method

.method public featureTourViewed()V
    .locals 0

    return-void
.end method

.method public notifyTourShowing()V
    .locals 0

    return-void
.end method

.method public shouldShowDeviceTour()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowFeatureTour()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
