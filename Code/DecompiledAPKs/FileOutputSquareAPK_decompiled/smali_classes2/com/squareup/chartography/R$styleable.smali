.class public final Lcom/squareup/chartography/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/chartography/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final BarChartView:[I

.field public static final BarChartView_barSpacing:I = 0x0

.field public static final BarChartView_stepSpacing:I = 0x1

.field public static final ChartView:[I

.field public static final ChartView_axisColor:I = 0x0

.field public static final ChartView_axisStrokeWidth:I = 0x1

.field public static final ChartView_dataColors:I = 0x2

.field public static final ChartView_dataSelectedColors:I = 0x3

.field public static final ChartView_hideFirstYLabel:I = 0x4

.field public static final ChartView_textColor:I = 0x5

.field public static final ChartView_tickSize:I = 0x6

.field public static final ChartView_xLabelFrequency:I = 0x7

.field public static final ChartView_xLabelMinGap:I = 0x8

.field public static final ChartView_xLabelSize:I = 0x9

.field public static final ChartView_xLabelTickSpacing:I = 0xa

.field public static final ChartView_yLabelSize:I = 0xb

.field public static final ChartView_yLabelSpacing:I = 0xc

.field public static final LineChartView:[I

.field public static final LineChartView_dotSize:I = 0x0

.field public static final LineChartView_lineSize:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 50
    fill-array-data v1, :array_0

    sput-object v1, Lcom/squareup/chartography/R$styleable;->BarChartView:[I

    const/16 v1, 0xd

    new-array v1, v1, [I

    .line 53
    fill-array-data v1, :array_1

    sput-object v1, Lcom/squareup/chartography/R$styleable;->ChartView:[I

    new-array v0, v0, [I

    .line 67
    fill-array-data v0, :array_2

    sput-object v0, Lcom/squareup/chartography/R$styleable;->LineChartView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040055
        0x7f0403f6
    .end array-data

    :array_1
    .array-data 4
        0x7f040040
        0x7f040041
        0x7f040114
        0x7f040115
        0x7f0401b8
        0x7f040446
        0x7f04045a
        0x7f0404a4
        0x7f0404a5
        0x7f0404a6
        0x7f0404a7
        0x7f0404a9
        0x7f0404aa
    .end array-data

    :array_2
    .array-data 4
        0x7f040133
        0x7f040280
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
