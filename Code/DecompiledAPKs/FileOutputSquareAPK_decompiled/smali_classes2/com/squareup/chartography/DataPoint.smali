.class public abstract Lcom/squareup/chartography/DataPoint;
.super Ljava/lang/Object;
.source "DataPoint.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DomainT:",
        "Ljava/lang/Object;",
        "RangeT:",
        "Ljava/lang/Number;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDataPoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DataPoint.kt\ncom/squareup/chartography/DataPoint\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,49:1\n11416#2,2:50\n*E\n*S KotlinDebug\n*F\n+ 1 DataPoint.kt\ncom/squareup/chartography/DataPoint\n*L\n40#1,2:50\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0004\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\u0011\n\u0002\u0008\u0003\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0002B\r\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0006R\u0013\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\u0008\u0007\u0010\u0008R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/chartography/DataPoint;",
        "DomainT",
        "",
        "RangeT",
        "",
        "domain",
        "(Ljava/lang/Object;)V",
        "getDomain",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "extent",
        "Lkotlin/ranges/ClosedRange;",
        "",
        "getExtent",
        "()Lkotlin/ranges/ClosedRange;",
        "ranges",
        "",
        "getRanges",
        "()[Ljava/lang/Number;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final domain:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDomainT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDomainT;)V"
        }
    .end annotation

    const-string v0, "domain"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/chartography/DataPoint;->domain:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getDomain()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDomainT;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/chartography/DataPoint;->domain:Ljava/lang/Object;

    return-object v0
.end method

.method public final getExtent()Lkotlin/ranges/ClosedRange;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/ranges/ClosedRange<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Lcom/squareup/chartography/DataPoint;->getRanges()[Ljava/lang/Number;

    move-result-object v0

    .line 50
    array-length v1, v0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-wide v5, v2

    :goto_0
    if-ge v4, v1, :cond_0

    aget-object v7, v0, v4

    .line 41
    invoke-virtual {v7}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v7

    .line 42
    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 43
    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->max(DD)D

    move-result-wide v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 46
    :cond_0
    invoke-static {v2, v3, v5, v6}, Lkotlin/ranges/RangesKt;->rangeTo(DD)Lkotlin/ranges/ClosedFloatingPointRange;

    move-result-object v0

    check-cast v0, Lkotlin/ranges/ClosedRange;

    return-object v0
.end method

.method public abstract getRanges()[Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TRangeT;"
        }
    .end annotation
.end method
