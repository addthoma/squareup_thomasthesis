.class public final Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;
.super Ljava/lang/Object;
.source "AppBootstrapModule_ProvideStartUptimeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/AppBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/AppBootstrapModule;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;->module:Lcom/squareup/AppBootstrapModule;

    return-void
.end method

.method public static create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;-><init>(Lcom/squareup/AppBootstrapModule;)V

    return-object v0
.end method

.method public static provideStartUptime(Lcom/squareup/AppBootstrapModule;)J
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule;->provideStartUptime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public get()Ljava/lang/Long;
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;->module:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;->provideStartUptime(Lcom/squareup/AppBootstrapModule;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;->get()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
