.class public abstract Lcom/squareup/salesreport/SalesReportModule;
.super Ljava/lang/Object;
.source "SalesReportModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/salesreport/basic/BasicSalesReportModule;,
        Lcom/squareup/salesreport/customize/CustomizeSalesReportModule;,
        Lcom/squareup/salesreport/detailed/DetailedSalesReportModule;,
        Lcom/squareup/salesreport/export/ExportSalesReportModule;,
        Lcom/squareup/salesreport/InitialSalesReportStateProviderModule;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\'\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportModule;",
        "",
        "()V",
        "bindSalesReportViewFactory",
        "Lcom/squareup/salesreport/SalesReportViewFactory;",
        "viewFactory",
        "Lcom/squareup/salesreport/RealSalesReportViewFactory;",
        "bindSalesReportWorkflow",
        "Lcom/squareup/salesreport/SalesReportWorkflow;",
        "workflow",
        "Lcom/squareup/salesreport/RealSalesReportWorkflow;",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/salesreport/SalesReportModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/salesreport/SalesReportModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/SalesReportModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/salesreport/SalesReportModule;->Companion:Lcom/squareup/salesreport/SalesReportModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation runtime Lcom/squareup/salesreport/export/IncludeItemsInReport;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/salesreport/SalesReportModule;->Companion:Lcom/squareup/salesreport/SalesReportModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/salesreport/SalesReportModule$Companion;->provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindSalesReportViewFactory(Lcom/squareup/salesreport/RealSalesReportViewFactory;)Lcom/squareup/salesreport/SalesReportViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSalesReportWorkflow(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/SalesReportWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
