.class final Lcom/squareup/salesreport/FeeDetailDialogFactory$create$1$2;
.super Ljava/lang/Object;
.source "FeeDetailDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/FeeDetailDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/salesreport/FeeDetailDialogScreen;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/FeeDetailDialogScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/FeeDetailDialogFactory$create$1$2;->$screen:Lcom/squareup/salesreport/FeeDetailDialogScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "<anonymous parameter 0>"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object p1, p0, Lcom/squareup/salesreport/FeeDetailDialogFactory$create$1$2;->$screen:Lcom/squareup/salesreport/FeeDetailDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/salesreport/FeeDetailDialogScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/salesreport/FeeDetailDialogScreen$Event$Back;->INSTANCE:Lcom/squareup/salesreport/FeeDetailDialogScreen$Event$Back;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
