.class public abstract Lcom/squareup/salesreport/InitialSalesReportStateProviderModule;
.super Ljava/lang/Object;
.source "InitialSalesReportStateProviderModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008!\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a2\u0006\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/salesreport/InitialSalesReportStateProviderModule;",
        "",
        "()V",
        "bindRealBasicReportConfigPolicy",
        "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
        "realDefaultReportConfigProvider",
        "Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;",
        "bindRealBasicReportConfigPolicy$impl_wiring_release",
        "bindRealDefaultReportConfigProvider",
        "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
        "Lcom/squareup/salesreport/RealDefaultReportConfigProvider;",
        "bindRealDefaultReportConfigProvider$impl_wiring_release",
        "bindRealInitialSalesReportStateProvider",
        "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
        "realInitialSalesReportStateProvider",
        "Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;",
        "bindRealInitialSalesReportStateProvider$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindRealBasicReportConfigPolicy$impl_wiring_release(Lcom/squareup/salesreport/util/RealBasicReportConfigPolicy;)Lcom/squareup/salesreport/util/BasicReportConfigPolicy;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealDefaultReportConfigProvider$impl_wiring_release(Lcom/squareup/salesreport/RealDefaultReportConfigProvider;)Lcom/squareup/salesreport/DefaultReportConfigProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealInitialSalesReportStateProvider$impl_wiring_release(Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;)Lcom/squareup/salesreport/InitialSalesReportStateProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
