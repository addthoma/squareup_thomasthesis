.class public final Lcom/squareup/salesreport/SalesReportCoordinator$Factory;
.super Ljava/lang/Object;
.source "SalesReportCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0004\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u00ed\u0001\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\t\u0012\u000e\u0008\u0001\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\t\u0012\u000e\u0008\u0001\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\r0\t\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u000e\u0008\u0001\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\t\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'\u0012\u0006\u0010)\u001a\u00020*\u0012\u0006\u0010+\u001a\u00020,\u00a2\u0006\u0002\u0010-J*\u0010.\u001a\u00020/2\"\u00100\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u0008\u0012\u0004\u0012\u000203`501R\u000e\u0010+\u001a\u00020,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\r0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
        "",
        "device",
        "Lcom/squareup/util/Device;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "percentageChangeFormatter",
        "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
        "numberFormatter",
        "Lcom/squareup/text/Formatter;",
        "",
        "compactNumberFormatter",
        "moneyFormatter",
        "Lcom/squareup/protos/common/Money;",
        "compactMoneyFormatter",
        "compactShorterMoneyFormatter",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "nameOrTranslationTypeFormatter",
        "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
        "wholeNumberPercentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "res",
        "Lcom/squareup/util/Res;",
        "resources",
        "Landroid/content/res/Resources;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "salesReportAnalytics",
        "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "current24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "backButtonConfig",
        "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
        "(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V",
        "create",
        "Lcom/squareup/salesreport/SalesReportCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/SalesReportScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

.field private final compactMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final compactNumberFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

.field private final numberFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final resources:Landroid/content/res/Resources;

.field private final salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V
    .locals 16
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/CompactNumber;
        .end annotation
    .end param
    .param p6    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/AccountingFormat;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/CompactAccountingFormat;
        .end annotation
    .end param
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/CompactShorterAccountingFormat;
        .end annotation
    .end param
    .param p13    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/WholeNumberPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/time/Current24HourClockMode;",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "device"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageChangeFormatter"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormatter"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactNumberFormatter"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactMoneyFormatter"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactShorterMoneyFormatter"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOrTranslationTypeFormatter"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wholeNumberPercentageFormatter"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportAnalytics"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current24HourClockMode"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backButtonConfig"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object v3, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    iput-object v4, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->numberFormatter:Lcom/squareup/text/Formatter;

    iput-object v5, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactNumberFormatter:Lcom/squareup/text/Formatter;

    iput-object v6, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v7, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v8, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v9, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object v10, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object v11, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object v12, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iput-object v13, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    iput-object v14, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object v15, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->resources:Landroid/content/res/Resources;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->features:Lcom/squareup/settings/server/Features;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->localeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/salesreport/SalesReportCoordinator;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/salesreport/SalesReportCoordinator;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v4, p1

    const-string v1, "screens"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    new-instance v25, Lcom/squareup/salesreport/SalesReportCoordinator;

    move-object/from16 v1, v25

    .line 247
    iget-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->device:Lcom/squareup/util/Device;

    .line 248
    iget-object v3, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 250
    iget-object v5, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    .line 251
    iget-object v6, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->numberFormatter:Lcom/squareup/text/Formatter;

    .line 252
    iget-object v7, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactNumberFormatter:Lcom/squareup/text/Formatter;

    .line 253
    iget-object v8, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 254
    iget-object v9, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 255
    iget-object v10, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 256
    iget-object v11, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    .line 257
    iget-object v12, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 258
    iget-object v13, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 259
    iget-object v14, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    .line 260
    iget-object v15, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 p1, v1

    .line 261
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v16, v1

    .line 262
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->res:Lcom/squareup/util/Res;

    move-object/from16 v17, v1

    .line 263
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->resources:Landroid/content/res/Resources;

    move-object/from16 v18, v1

    .line 264
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v19, v1

    .line 265
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

    move-object/from16 v20, v1

    .line 266
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v21, v1

    .line 267
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->localeProvider:Ljavax/inject/Provider;

    move-object/from16 v22, v1

    .line 268
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

    move-object/from16 v23, v1

    .line 269
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;->backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    move-object/from16 v24, v1

    move-object/from16 v1, p1

    .line 246
    invoke-direct/range {v1 .. v24}, Lcom/squareup/salesreport/SalesReportCoordinator;-><init>(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V

    return-object v25
.end method
