.class public final Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;
.super Ljava/lang/Object;
.source "RealInitialSalesReportStateProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
            ">;)",
            "Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;Lcom/squareup/salesreport/util/BasicReportConfigPolicy;)Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;-><init>(Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;Lcom/squareup/salesreport/util/BasicReportConfigPolicy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    iget-object v1, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;

    iget-object v2, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/salesreport/util/BasicReportConfigPolicy;

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->newInstance(Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;Lcom/squareup/salesreport/util/BasicReportConfigPolicy;)Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider_Factory;->get()Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;

    move-result-object v0

    return-object v0
.end method
