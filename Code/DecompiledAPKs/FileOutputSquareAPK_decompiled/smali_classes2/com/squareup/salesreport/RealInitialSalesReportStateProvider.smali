.class public final Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;
.super Ljava/lang/Object;
.source "InitialSalesReportStateProvider.kt"

# interfaces
.implements Lcom/squareup/salesreport/InitialSalesReportStateProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;",
        "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
        "basicSalesReportConfigRepository",
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
        "detailedSalesReportConfigRepository",
        "Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;",
        "basicReportConfigPolicy",
        "Lcom/squareup/salesreport/util/BasicReportConfigPolicy;",
        "(Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;Lcom/squareup/salesreport/util/BasicReportConfigPolicy;)V",
        "earliestSelectableDate",
        "Lorg/threeten/bp/LocalDate;",
        "detailLevel",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "get",
        "Lcom/squareup/salesreport/SalesReportState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final basicReportConfigPolicy:Lcom/squareup/salesreport/util/BasicReportConfigPolicy;

.field private final basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

.field private final detailedSalesReportConfigRepository:Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;Lcom/squareup/salesreport/util/BasicReportConfigPolicy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "basicSalesReportConfigRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailedSalesReportConfigRepository"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "basicReportConfigPolicy"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    iput-object p2, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->detailedSalesReportConfigRepository:Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;

    iput-object p3, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->basicReportConfigPolicy:Lcom/squareup/salesreport/util/BasicReportConfigPolicy;

    return-void
.end method

.method private final earliestSelectableDate(Lcom/squareup/api/salesreport/DetailLevel;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->basicReportConfigPolicy:Lcom/squareup/salesreport/util/BasicReportConfigPolicy;

    invoke-interface {p1}, Lcom/squareup/salesreport/util/BasicReportConfigPolicy;->getEarliestSelectableReportDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_0
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Detailed;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public get(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/salesreport/SalesReportState;
    .locals 14

    const-string v0, "detailLevel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    invoke-interface {v0}, Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;->get()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/squareup/salesreport/RealInitialSalesReportStateProvider;->detailedSalesReportConfigRepository:Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;

    invoke-interface {v0}, Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;->get()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 40
    :goto_0
    new-instance v1, Lcom/squareup/salesreport/SalesReportState$InitialState;

    .line 42
    new-instance v13, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    .line 43
    invoke-static {p1}, Lcom/squareup/salesreport/util/DetailLevelsKt;->isOverviewEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    :goto_1
    move-object v3, p1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xfe

    const/4 v12, 0x0

    move-object v2, v13

    .line 42
    invoke-direct/range {v2 .. v12}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;-><init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 40
    invoke-direct {v1, v0, v13}, Lcom/squareup/salesreport/SalesReportState$InitialState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    check-cast v1, Lcom/squareup/salesreport/SalesReportState;

    return-object v1
.end method
