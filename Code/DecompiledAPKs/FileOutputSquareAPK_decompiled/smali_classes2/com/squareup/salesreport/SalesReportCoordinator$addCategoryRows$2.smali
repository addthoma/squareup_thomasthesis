.class final Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesReportCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;->addCategoryRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Landroid/view/View;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/salesreport/SalesReportScreen;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;->$screen:Lcom/squareup/salesreport/SalesReportScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 711
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;->$screen:Lcom/squareup/salesreport/SalesReportScreen;

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;-><init>(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
