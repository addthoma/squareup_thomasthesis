.class public final Lcom/squareup/salesreport/RealDefaultReportConfigProvider;
.super Ljava/lang/Object;
.source "DefaultReportConfigProvider.kt"

# interfaces
.implements Lcom/squareup/salesreport/DefaultReportConfigProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/salesreport/RealDefaultReportConfigProvider;",
        "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "thisDeviceOnlyFilteredLocalSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "(Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/LocalSetting;)V",
        "get",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "detailLevel",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .param p2    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/salesreport/settings/SalesReportThisDeviceOnlyFilteredSetting;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "thisDeviceOnlyFilteredLocalSetting"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/RealDefaultReportConfigProvider;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p2, p0, Lcom/squareup/salesreport/RealDefaultReportConfigProvider;->thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ReportConfig;
    .locals 11

    const-string v0, "detailLevel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/customreport/data/ReportConfig;

    .line 39
    iget-object v1, p0, Lcom/squareup/salesreport/RealDefaultReportConfigProvider;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 40
    iget-object v1, p0, Lcom/squareup/salesreport/RealDefaultReportConfigProvider;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 41
    sget-object v4, Lorg/threeten/bp/LocalTime;->MIN:Lorg/threeten/bp/LocalTime;

    const-string v1, "LocalTime.MIN"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object v5, Lorg/threeten/bp/LocalTime;->MAX:Lorg/threeten/bp/LocalTime;

    const-string v1, "LocalTime.MAX"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/salesreport/RealDefaultReportConfigProvider;->thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v6, "thisDeviceOnlyFilteredLocalSetting.get()"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 45
    sget-object v1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;

    move-object v8, v1

    check-cast v8, Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    .line 46
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    move-object v9, v1

    check-cast v9, Lcom/squareup/customreport/data/RangeSelection;

    .line 47
    invoke-static {p1}, Lcom/squareup/salesreport/util/DetailLevelsKt;->getDefaultComparisonRange(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v10

    const/4 v6, 0x1

    move-object v1, v0

    .line 38
    invoke-direct/range {v1 .. v10}, Lcom/squareup/customreport/data/ReportConfig;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)V

    return-object v0
.end method
