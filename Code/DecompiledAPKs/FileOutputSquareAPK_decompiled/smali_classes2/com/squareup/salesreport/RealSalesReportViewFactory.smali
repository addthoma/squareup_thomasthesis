.class public final Lcom/squareup/salesreport/RealSalesReportViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealSalesReportViewFactory.kt"

# interfaces
.implements Lcom/squareup/salesreport/SalesReportViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\'\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/salesreport/RealSalesReportViewFactory;",
        "Lcom/squareup/salesreport/SalesReportViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "salesReportCoordinatorFactory",
        "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
        "customizeReportAllFieldsCoordinatorFactory",
        "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;",
        "employeeSelectionCoordinatorFactory",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
        "emailReportCoordinatorFactory",
        "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
        "(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)V
    .locals 24
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const-string v4, "salesReportCoordinatorFactory"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "customizeReportAllFieldsCoordinatorFactory"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "employeeSelectionCoordinatorFactory"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "emailReportCoordinatorFactory"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v4, 0x8

    new-array v4, v4, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 27
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 28
    const-class v6, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v6, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 29
    sget-object v9, Lcom/squareup/salesreport/RealSalesReportViewFactory$1;->INSTANCE:Lcom/squareup/salesreport/RealSalesReportViewFactory$1;

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 27
    invoke-virtual {v5, v6, v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 31
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 32
    const-class v5, Lcom/squareup/salesreport/SalesReportScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 33
    sget v11, Lcom/squareup/salesreport/impl/R$layout;->sales_report_view:I

    .line 34
    new-instance v5, Lcom/squareup/workflow/ScreenHint;

    sget-object v18, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x1df

    const/16 v23, 0x0

    move-object v12, v5

    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    new-instance v6, Lcom/squareup/salesreport/RealSalesReportViewFactory$2;

    invoke-direct {v6, v0}, Lcom/squareup/salesreport/RealSalesReportViewFactory$2;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;)V

    move-object v14, v6

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/16 v15, 0x8

    .line 31
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    aput-object v0, v4, v7

    .line 37
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 38
    const-class v0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 39
    sget v11, Lcom/squareup/salesreport/impl/R$layout;->customize_report_view:I

    .line 40
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v18, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 41
    new-instance v5, Lcom/squareup/salesreport/RealSalesReportViewFactory$3;

    invoke-direct {v5, v1}, Lcom/squareup/salesreport/RealSalesReportViewFactory$3;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;)V

    move-object v14, v5

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/16 v15, 0x8

    .line 37
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v4, v1

    .line 43
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 44
    const-class v1, Lcom/squareup/salesreport/export/ExportReportDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 45
    sget-object v5, Lcom/squareup/salesreport/RealSalesReportViewFactory$4;->INSTANCE:Lcom/squareup/salesreport/RealSalesReportViewFactory$4;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 43
    invoke-virtual {v0, v1, v5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v4, v1

    .line 47
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 48
    const-class v0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 49
    sget v11, Lcom/squareup/salesreport/impl/R$layout;->employee_selection_view:I

    .line 50
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v18, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 51
    new-instance v1, Lcom/squareup/salesreport/RealSalesReportViewFactory$5;

    invoke-direct {v1, v2}, Lcom/squareup/salesreport/RealSalesReportViewFactory$5;-><init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;)V

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/16 v15, 0x8

    .line 47
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v4, v1

    .line 53
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 54
    const-class v0, Lcom/squareup/salesreport/export/email/EmailReportScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 55
    sget v11, Lcom/squareup/salesreport/impl/R$layout;->email_report_view:I

    .line 56
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v18, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 57
    new-instance v1, Lcom/squareup/salesreport/RealSalesReportViewFactory$6;

    invoke-direct {v1, v3}, Lcom/squareup/salesreport/RealSalesReportViewFactory$6;-><init>(Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)V

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/16 v15, 0x8

    .line 53
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x5

    aput-object v0, v4, v1

    .line 59
    sget-object v0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$Binding;->INSTANCE:Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$Binding;

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x6

    aput-object v0, v4, v1

    .line 60
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 61
    const-class v1, Lcom/squareup/salesreport/FeeDetailDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v8, v7, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/squareup/salesreport/RealSalesReportViewFactory$7;->INSTANCE:Lcom/squareup/salesreport/RealSalesReportViewFactory$7;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x7

    aput-object v0, v4, v1

    move-object/from16 v0, p0

    .line 26
    invoke-direct {v0, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
