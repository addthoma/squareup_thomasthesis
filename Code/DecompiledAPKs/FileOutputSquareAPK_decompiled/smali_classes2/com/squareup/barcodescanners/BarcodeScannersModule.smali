.class public abstract Lcom/squareup/barcodescanners/BarcodeScannersModule;
.super Ljava/lang/Object;
.source "BarcodeScannersModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideUsbBarcodeScanners(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/util/Res;)Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 22
    new-instance v6, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    const-string/jumbo v0, "usb_barcode_scanners"

    .line 23
    invoke-static {v0}, Lcom/squareup/thread/Threads;->backgroundThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Ljava/util/concurrent/Executor;Lcom/squareup/util/Res;)V

    return-object v6
.end method


# virtual methods
.method abstract bindUsbBarcodeScannerDiscovererIntoAppScope(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
