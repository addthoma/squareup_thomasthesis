.class public final Lcom/squareup/charts/piechart/Slice;
.super Ljava/lang/Object;
.source "Slice.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSlice.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Slice.kt\ncom/squareup/charts/piechart/Slice\n*L\n1#1,18:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/Slice;",
        "",
        "length",
        "",
        "thickness",
        "color",
        "Lcom/squareup/charts/piechart/SliceColor;",
        "(FFLcom/squareup/charts/piechart/SliceColor;)V",
        "getColor",
        "()Lcom/squareup/charts/piechart/SliceColor;",
        "getLength",
        "()F",
        "getThickness",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final color:Lcom/squareup/charts/piechart/SliceColor;

.field private final length:F

.field private final thickness:F


# direct methods
.method public constructor <init>(FFLcom/squareup/charts/piechart/SliceColor;)V
    .locals 2

    const-string v0, "color"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    iput p2, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    iput-object p3, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    .line 14
    iget p1, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    const/4 p2, 0x1

    const/high16 p3, 0x42c80000    # 100.0f

    const/4 v0, 0x0

    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    cmpg-float p1, p1, p3

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    .line 15
    iget p1, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    cmpg-float p1, p1, p3

    if-gtz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_2

    return-void

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Thickness ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p2, ") must be between 10%-100%."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 14
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Length ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p2, ") must be between 0%-100%."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public static synthetic copy$default(Lcom/squareup/charts/piechart/Slice;FFLcom/squareup/charts/piechart/SliceColor;ILjava/lang/Object;)Lcom/squareup/charts/piechart/Slice;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/charts/piechart/Slice;->copy(FFLcom/squareup/charts/piechart/SliceColor;)Lcom/squareup/charts/piechart/Slice;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    return v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    return v0
.end method

.method public final component3()Lcom/squareup/charts/piechart/SliceColor;
    .locals 1

    iget-object v0, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    return-object v0
.end method

.method public final copy(FFLcom/squareup/charts/piechart/SliceColor;)Lcom/squareup/charts/piechart/Slice;
    .locals 1

    const-string v0, "color"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/charts/piechart/Slice;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/charts/piechart/Slice;-><init>(FFLcom/squareup/charts/piechart/SliceColor;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/charts/piechart/Slice;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/charts/piechart/Slice;

    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    iget v1, p1, Lcom/squareup/charts/piechart/Slice;->length:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    iget v1, p1, Lcom/squareup/charts/piechart/Slice;->thickness:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    iget-object p1, p1, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getColor()Lcom/squareup/charts/piechart/SliceColor;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    return-object v0
.end method

.method public final getLength()F
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    return v0
.end method

.method public final getThickness()F
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    invoke-static {v0}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Slice(length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/charts/piechart/Slice;->length:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", thickness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/charts/piechart/Slice;->thickness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/charts/piechart/Slice;->color:Lcom/squareup/charts/piechart/SliceColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
