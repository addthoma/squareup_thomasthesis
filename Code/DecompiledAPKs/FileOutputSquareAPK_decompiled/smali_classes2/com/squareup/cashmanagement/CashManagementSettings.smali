.class public Lcom/squareup/cashmanagement/CashManagementSettings;
.super Ljava/lang/Object;
.source "CashManagementSettings.java"


# instance fields
.field private final autoEmailReportSetting:Lcom/squareup/cashmanagement/AutoEmailReportSetting;

.field private final autoPrintReportSetting:Lcom/squareup/cashmanagement/AutoPrintReportSetting;

.field private final cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final defaultStartingCash:Lcom/squareup/cashmanagement/DefaultStartingCashSetting;

.field private final emailRecipientSetting:Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashmanagement/CashManagementEnabledSetting;Lcom/squareup/cashmanagement/DefaultStartingCashSetting;Lcom/squareup/cashmanagement/AutoEmailReportSetting;Lcom/squareup/cashmanagement/AutoPrintReportSetting;Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 26
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    .line 27
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->defaultStartingCash:Lcom/squareup/cashmanagement/DefaultStartingCashSetting;

    .line 28
    iput-object p4, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoEmailReportSetting:Lcom/squareup/cashmanagement/AutoEmailReportSetting;

    .line 29
    iput-object p5, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoPrintReportSetting:Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    .line 30
    iput-object p6, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->emailRecipientSetting:Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;

    return-void
.end method


# virtual methods
.method public cashManagementEnabled()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementEnabledSetting;->cashManagementEnabled()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public enableCashManagementToggled(Z)V
    .locals 0

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setCashManagementEnabled(Z)V

    return-void
.end method

.method public getDefaultStartingCash()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->defaultStartingCash:Lcom/squareup/cashmanagement/DefaultStartingCashSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getEmailRecipient()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->emailRecipientSetting:Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isCashManagementAllowed()Z
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementEnabledSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    return v0
.end method

.method public isCashManagementEnabled()Z
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementEnabledSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isEmailReportEnabled()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoEmailReportSetting:Lcom/squareup/cashmanagement/AutoEmailReportSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/AutoEmailReportSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isPrintReportEnabled()Z
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoPrintReportSetting:Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public setCashManagementEnabled(Z)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabledSetting:Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashManagementEnabledSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setDefaultStartingCash(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->defaultStartingCash:Lcom/squareup/cashmanagement/DefaultStartingCashSetting;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setEmailRecipient(Ljava/lang/String;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->emailRecipientSetting:Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setEmailReportEnabled(Z)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoEmailReportSetting:Lcom/squareup/cashmanagement/AutoEmailReportSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/AutoEmailReportSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setPrintReportEnabled(Z)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings;->autoPrintReportSetting:Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method
