.class public final Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CheckoutResponse;",
        "Lcom/squareup/checkoutfe/CheckoutResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

.field public create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;"
        }
    .end annotation
.end field

.field public order:Lcom/squareup/orders/model/Order;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CheckoutResponse;
    .locals 7

    .line 165
    new-instance v6, Lcom/squareup/checkoutfe/CheckoutResponse;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    iget-object v3, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    iget-object v4, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutfe/CheckoutResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;Lcom/squareup/orders/model/Order;Lcom/squareup/checkoutfe/CheckoutClientDetails;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object v0

    return-object v0
.end method

.method public client_details(Lcom/squareup/checkoutfe/CheckoutClientDetails;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    return-object p0
.end method

.method public create_payment_response(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;)",
            "Lcom/squareup/checkoutfe/CheckoutResponse$Builder;"
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method
