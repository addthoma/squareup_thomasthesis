.class public final Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public loyalty_status:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 189
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    .line 190
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
    .locals 5

    .line 212
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;-><init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    move-result-object v0

    return-object v0
.end method

.method public coupon(Ljava/util/List;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;"
        }
    .end annotation

    .line 199
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 200
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    return-object p0
.end method

.method public loyalty_status(Ljava/util/List;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;)",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;"
        }
    .end annotation

    .line 205
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 206
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    return-object p0
.end method

.method public receipt_details(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    return-object p0
.end method
