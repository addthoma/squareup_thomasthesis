.class public final Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest;",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotency_key:Ljava/lang/String;

.field public order_id:Ljava/lang/String;

.field public order_version:Ljava/lang/Integer;

.field public payments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 129
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
    .locals 7

    .line 155
    new-instance v6, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_version:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method

.method public order_version(Ljava/lang/Integer;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public payments(Ljava/util/List;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
            ">;)",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;"
        }
    .end annotation

    .line 148
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 149
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    return-object p0
.end method
