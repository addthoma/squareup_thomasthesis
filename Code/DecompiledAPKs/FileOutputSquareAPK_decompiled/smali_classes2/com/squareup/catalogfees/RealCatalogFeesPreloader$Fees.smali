.class Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;
.super Ljava/lang/Object;
.source "RealCatalogFeesPreloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalogfees/RealCatalogFeesPreloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Fees"
.end annotation


# instance fields
.field final discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field final taxRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation
.end field

.field final taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;)V"
        }
    .end annotation

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->taxes:Ljava/util/List;

    .line 82
    iput-object p2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->taxRules:Ljava/util/List;

    .line 83
    iput-object p3, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->discounts:Ljava/util/List;

    return-void
.end method
