.class public final Lcom/squareup/android/xml/TagVisitor;
.super Ljava/lang/Object;
.source "XmlResourceParsing.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nXmlResourceParsing.kt\nKotlin\n*S Kotlin\n*F\n+ 1 XmlResourceParsing.kt\ncom/squareup/android/xml/TagVisitor\n*L\n1#1,214:1\n165#1,6:215\n192#1,7:221\n*E\n*S KotlinDebug\n*F\n+ 1 XmlResourceParsing.kt\ncom/squareup/android/xml/TagVisitor\n*L\n180#1,6:215\n207#1,7:221\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000m\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\t\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J#\u0010\u0014\u001a\u00020\u00152\u001b\u0010\u0016\u001a\u0017\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00150\u0017j\u0002`\u0019\u00a2\u0006\u0002\u0008\u001aJ.\u0010\u001b\u001a\u00020\u00152#\u0008\u0004\u0010\u0016\u001a\u001d\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00150\u0017j\u0008\u0012\u0004\u0012\u00020\u0015`\u001c\u00a2\u0006\u0002\u0008\u001aH\u0086\u0008J6\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u000e2#\u0008\u0004\u0010\u0016\u001a\u001d\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00150\u0017j\u0008\u0012\u0004\u0012\u00020\u0015`\u001c\u00a2\u0006\u0002\u0008\u001aH\u0086\u0008J9\u0010\u001e\u001a\u0002H\u001f\"\u0004\u0008\u0000\u0010\u001f2#\u0008\u0004\u0010\u0016\u001a\u001d\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u0002H\u001f0\u0017j\u0008\u0012\u0004\u0012\u0002H\u001f`\u001c\u00a2\u0006\u0002\u0008\u001aH\u0086\u0008\u00a2\u0006\u0002\u0010 J<\u0010\u001e\u001a\u00020\u0015\"\u0004\u0008\u0000\u0010\u001f2\u0006\u0010!\u001a\u00020\u000e2#\u0008\u0004\u0010\u0016\u001a\u001d\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u0002H\u001f0\u0017j\u0008\u0012\u0004\u0012\u0002H\u001f`\u001c\u00a2\u0006\u0002\u0008\u001aH\u0086\u0008JM\u0010\"\u001a\u00020\u00152\n\u0010#\u001a\u00060$R\u00020%2\u0008\u0008\u0001\u0010\u0004\u001a\u00020&2\u0008\u0008\u0003\u0010\'\u001a\u00020(2\u0008\u0008\u0003\u0010)\u001a\u00020(2\u001b\u0010\u0016\u001a\u0017\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\u00150\u0017j\u0002`+\u00a2\u0006\u0002\u0008\u001aJT\u0010,\u001a\u0002H\u001f\"\u0004\u0008\u0000\u0010\u001f2\n\u0010#\u001a\u00060$R\u00020%2\u0008\u0008\u0001\u0010\u0004\u001a\u00020&2\u0008\u0008\u0001\u0010\'\u001a\u00020(2\u0008\u0008\u0001\u0010)\u001a\u00020(2\u0017\u0010\u0016\u001a\u0013\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u0002H\u001f0\u0017\u00a2\u0006\u0002\u0008\u001a\u00a2\u0006\u0002\u0010.R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0019\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/android/xml/TagVisitor;",
        "",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V",
        "attributeSet",
        "attributeVisitor",
        "com/squareup/android/xml/TagVisitor$attributeVisitor$1",
        "Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;",
        "getAttrs",
        "()Landroid/util/AttributeSet;",
        "name",
        "",
        "kotlin.jvm.PlatformType",
        "getName",
        "()Ljava/lang/String;",
        "getParser",
        "()Lorg/xmlpull/v1/XmlPullParser;",
        "visitAttributes",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/android/xml/AttributesVisitor;",
        "Lcom/squareup/android/xml/AttributesVisitorBlock;",
        "Lkotlin/ExtensionFunctionType;",
        "visitChildren",
        "Lcom/squareup/android/xml/TagVisitorBlock;",
        "tagName",
        "visitRoot",
        "R",
        "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "rootTagName",
        "visitStyledAttributes",
        "theme",
        "Landroid/content/res/Resources$Theme;",
        "Landroid/content/res/Resources;",
        "",
        "defStyleAttr",
        "",
        "defStyleRes",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "Lcom/squareup/android/xml/StyledAttributesVisitorBlock;",
        "withStyledAttributes",
        "Landroid/content/res/TypedArray;",
        "(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "android-xml_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private attributeSet:Landroid/util/AttributeSet;

.field private final attributeVisitor:Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

.field private final attrs:Landroid/util/AttributeSet;

.field private final parser:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "parser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor;->parser:Lorg/xmlpull/v1/XmlPullParser;

    iput-object p2, p0, Lcom/squareup/android/xml/TagVisitor;->attrs:Landroid/util/AttributeSet;

    .line 100
    new-instance p1, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    invoke-direct {p1, p0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;-><init>(Lcom/squareup/android/xml/TagVisitor;)V

    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor;->attributeVisitor:Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    return-void
.end method

.method public static final synthetic access$getAttributeSet$p(Lcom/squareup/android/xml/TagVisitor;)Landroid/util/AttributeSet;
    .locals 1

    .line 96
    iget-object p0, p0, Lcom/squareup/android/xml/TagVisitor;->attributeSet:Landroid/util/AttributeSet;

    if-nez p0, :cond_0

    const-string v0, "attributeSet"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAttributeVisitor$p(Lcom/squareup/android/xml/TagVisitor;)Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/android/xml/TagVisitor;->attributeVisitor:Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    return-object p0
.end method

.method public static final synthetic access$setAttributeSet$p(Lcom/squareup/android/xml/TagVisitor;Landroid/util/AttributeSet;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor;->attributeSet:Landroid/util/AttributeSet;

    return-void
.end method

.method public static synthetic visitStyledAttributes$default(Lcom/squareup/android/xml/TagVisitor;Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    move v5, p4

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p5

    .line 148
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/android/xml/TagVisitor;->visitStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public final getAttrs()Landroid/util/AttributeSet;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->attrs:Landroid/util/AttributeSet;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->parser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getParser()Lorg/xmlpull/v1/XmlPullParser;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->parser:Lorg/xmlpull/v1/XmlPullParser;

    return-object v0
.end method

.method public final visitAttributes(Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/AttributesVisitor;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->parser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 117
    iget-object v2, p0, Lcom/squareup/android/xml/TagVisitor;->attributeVisitor:Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    invoke-virtual {v2, v1}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->setIndex(I)V

    .line 118
    iget-object v2, p0, Lcom/squareup/android/xml/TagVisitor;->attributeVisitor:Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final visitChildren(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "tagName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 225
    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/squareup/android/xml/TagVisitor;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextSiblingTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void
.end method

.method public final visitChildren(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 196
    :cond_0
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextSiblingTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void
.end method

.method public final visitRoot(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-nez v0, :cond_1

    .line 169
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 169
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No root element found"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 167
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not at beginning of visitRoot"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final visitRoot(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "+TR;>;)V"
        }
    .end annotation

    const-string v0, "rootTagName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-nez v0, :cond_2

    .line 219
    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    move-object v0, p0

    check-cast v0, Lcom/squareup/android/xml/TagVisitor;

    .line 181
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    .line 184
    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 182
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Root differs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " != "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 219
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No root element found"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 217
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Not at beginning of visitRoot"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final visitStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources$Theme;",
            "[III",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/StyledAttributesVisitor;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "theme"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;

    invoke-direct {v0, p0, p5}, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;-><init>(Lcom/squareup/android/xml/TagVisitor;Lkotlin/jvm/functions/Function1;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/android/xml/TagVisitor;->withStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    return-void
.end method

.method public final withStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/res/Resources$Theme;",
            "[III",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/res/TypedArray;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "theme"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    move-object v0, p0

    check-cast v0, Lcom/squareup/android/xml/TagVisitor;

    iget-object v0, v0, Lcom/squareup/android/xml/TagVisitor;->attributeSet:Landroid/util/AttributeSet;

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->parser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    const-string v1, "Xml.asAttributeSet(parser)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->attributeSet:Landroid/util/AttributeSet;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor;->attributeSet:Landroid/util/AttributeSet;

    if-nez v0, :cond_1

    const-string v1, "attributeSet"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "styled"

    .line 134
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p5, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method
