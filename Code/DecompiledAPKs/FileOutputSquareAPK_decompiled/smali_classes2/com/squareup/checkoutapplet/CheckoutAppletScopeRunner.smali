.class public final Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;
.super Ljava/lang/Object;
.source "CheckoutAppletScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/checkoutapplet/CheckoutAppletScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;",
        "Lmortar/Scoped;",
        "applet",
        "Lcom/squareup/checkoutapplet/CheckoutApplet;",
        "(Lcom/squareup/checkoutapplet/CheckoutApplet;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final applet:Lcom/squareup/checkoutapplet/CheckoutApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutapplet/CheckoutApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "applet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;->applet:Lcom/squareup/checkoutapplet/CheckoutApplet;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object p1, p0, Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;->applet:Lcom/squareup/checkoutapplet/CheckoutApplet;

    invoke-virtual {p1}, Lcom/squareup/checkoutapplet/CheckoutApplet;->select()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
