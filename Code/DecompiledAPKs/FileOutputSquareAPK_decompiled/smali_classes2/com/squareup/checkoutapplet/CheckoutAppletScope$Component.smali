.class public interface abstract Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;
.super Ljava/lang/Object;
.source "CheckoutAppletScope.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/checkoutapplet/CheckoutAppletScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutapplet/CheckoutAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;",
        "",
        "checkoutAppletScopeRunner",
        "Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;",
        "checkoutData",
        "Lcom/squareup/checkout/v2/data/transaction/TransactionData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract checkoutAppletScopeRunner()Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;
.end method

.method public abstract checkoutData()Lcom/squareup/checkout/v2/data/transaction/TransactionData;
.end method
