.class public interface abstract Lcom/squareup/PosAppComponent;
.super Ljava/lang/Object;
.source "PosAppComponent.java"

# interfaces
.implements Lcom/squareup/CommonAppComponent;
.implements Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService$Component;
.implements Lcom/squareup/loggedout/LoggedOutFeatureComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/PosAppComponent$Module;
    }
.end annotation


# virtual methods
.method public abstract loggedOutActivityComponent()Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;
.end method

.method public abstract paymentActivityComponent()Lcom/squareup/ui/PaymentActivity$Component;
.end method
