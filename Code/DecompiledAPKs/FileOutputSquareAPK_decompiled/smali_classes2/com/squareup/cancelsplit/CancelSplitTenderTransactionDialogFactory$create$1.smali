.class final Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1;
.super Ljava/lang/Object;
.source "CancelSplitTenderTransactionDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getPositiveButton()I

    move-result v1

    .line 39
    new-instance v2, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1$builder$1;

    invoke-direct {v2, p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1$builder$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 37
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 41
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getNegativeButton()I

    move-result v1

    .line 42
    new-instance v2, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1$builder$2;

    invoke-direct {v2, p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1$builder$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 40
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 43
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 44
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 45
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 46
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 48
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getAmountPaid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 51
    iget-object v2, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1;->$context:Landroid/content/Context;

    iget-object v3, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {v3}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getTitle()I

    move-result v3

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 52
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getAmountPaid()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v3, "amount"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 50
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    goto :goto_0

    .line 56
    :cond_0
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->getMessage()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 59
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
