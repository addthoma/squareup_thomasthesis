.class public final enum Lcom/squareup/CountryCode;
.super Ljava/lang/Enum;
.source "CountryCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/CountryCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/CountryCode;

.field public static final enum AE:Lcom/squareup/CountryCode;

.field public static final enum AG:Lcom/squareup/CountryCode;

.field public static final enum AI:Lcom/squareup/CountryCode;

.field public static final enum AL:Lcom/squareup/CountryCode;

.field public static final enum AM:Lcom/squareup/CountryCode;

.field public static final enum AO:Lcom/squareup/CountryCode;

.field public static final enum AR:Lcom/squareup/CountryCode;

.field public static final enum AT:Lcom/squareup/CountryCode;

.field public static final enum AU:Lcom/squareup/CountryCode;

.field public static final enum AW:Lcom/squareup/CountryCode;

.field public static final enum AZ:Lcom/squareup/CountryCode;

.field public static final enum BA:Lcom/squareup/CountryCode;

.field public static final enum BB:Lcom/squareup/CountryCode;

.field public static final enum BD:Lcom/squareup/CountryCode;

.field public static final enum BE:Lcom/squareup/CountryCode;

.field public static final enum BF:Lcom/squareup/CountryCode;

.field public static final enum BG:Lcom/squareup/CountryCode;

.field public static final enum BH:Lcom/squareup/CountryCode;

.field public static final enum BJ:Lcom/squareup/CountryCode;

.field public static final enum BM:Lcom/squareup/CountryCode;

.field public static final enum BN:Lcom/squareup/CountryCode;

.field public static final enum BO:Lcom/squareup/CountryCode;

.field public static final enum BR:Lcom/squareup/CountryCode;

.field public static final enum BS:Lcom/squareup/CountryCode;

.field public static final enum BT:Lcom/squareup/CountryCode;

.field public static final enum BW:Lcom/squareup/CountryCode;

.field public static final enum BY:Lcom/squareup/CountryCode;

.field public static final enum BZ:Lcom/squareup/CountryCode;

.field public static final enum CA:Lcom/squareup/CountryCode;

.field public static final enum CG:Lcom/squareup/CountryCode;

.field public static final enum CH:Lcom/squareup/CountryCode;

.field public static final enum CI:Lcom/squareup/CountryCode;

.field public static final enum CL:Lcom/squareup/CountryCode;

.field public static final enum CM:Lcom/squareup/CountryCode;

.field public static final enum CN:Lcom/squareup/CountryCode;

.field public static final enum CO:Lcom/squareup/CountryCode;

.field public static final enum CR:Lcom/squareup/CountryCode;

.field public static final enum CV:Lcom/squareup/CountryCode;

.field public static final enum CY:Lcom/squareup/CountryCode;

.field public static final enum CZ:Lcom/squareup/CountryCode;

.field public static final enum DE:Lcom/squareup/CountryCode;

.field public static final enum DK:Lcom/squareup/CountryCode;

.field public static final enum DM:Lcom/squareup/CountryCode;

.field public static final enum DO:Lcom/squareup/CountryCode;

.field public static final enum DZ:Lcom/squareup/CountryCode;

.field public static final enum EC:Lcom/squareup/CountryCode;

.field public static final enum EE:Lcom/squareup/CountryCode;

.field public static final enum EG:Lcom/squareup/CountryCode;

.field public static final enum ES:Lcom/squareup/CountryCode;

.field public static final enum FI:Lcom/squareup/CountryCode;

.field public static final enum FJ:Lcom/squareup/CountryCode;

.field public static final enum FM:Lcom/squareup/CountryCode;

.field public static final enum FR:Lcom/squareup/CountryCode;

.field public static final enum GA:Lcom/squareup/CountryCode;

.field public static final enum GB:Lcom/squareup/CountryCode;

.field public static final enum GD:Lcom/squareup/CountryCode;

.field public static final enum GH:Lcom/squareup/CountryCode;

.field public static final enum GM:Lcom/squareup/CountryCode;

.field public static final enum GR:Lcom/squareup/CountryCode;

.field public static final enum GT:Lcom/squareup/CountryCode;

.field public static final enum GU:Lcom/squareup/CountryCode;

.field public static final enum GW:Lcom/squareup/CountryCode;

.field public static final enum GY:Lcom/squareup/CountryCode;

.field public static final enum HK:Lcom/squareup/CountryCode;

.field public static final enum HN:Lcom/squareup/CountryCode;

.field public static final enum HR:Lcom/squareup/CountryCode;

.field public static final enum HT:Lcom/squareup/CountryCode;

.field public static final enum HU:Lcom/squareup/CountryCode;

.field public static final enum ID:Lcom/squareup/CountryCode;

.field public static final enum IE:Lcom/squareup/CountryCode;

.field public static final enum IL:Lcom/squareup/CountryCode;

.field public static final enum IN:Lcom/squareup/CountryCode;

.field public static final enum IS:Lcom/squareup/CountryCode;

.field public static final enum IT:Lcom/squareup/CountryCode;

.field public static final enum JM:Lcom/squareup/CountryCode;

.field public static final enum JO:Lcom/squareup/CountryCode;

.field public static final enum JP:Lcom/squareup/CountryCode;

.field public static final enum KE:Lcom/squareup/CountryCode;

.field public static final enum KG:Lcom/squareup/CountryCode;

.field public static final enum KH:Lcom/squareup/CountryCode;

.field public static final enum KN:Lcom/squareup/CountryCode;

.field public static final enum KR:Lcom/squareup/CountryCode;

.field public static final enum KW:Lcom/squareup/CountryCode;

.field public static final enum KY:Lcom/squareup/CountryCode;

.field public static final enum KZ:Lcom/squareup/CountryCode;

.field public static final enum LA:Lcom/squareup/CountryCode;

.field public static final enum LB:Lcom/squareup/CountryCode;

.field public static final enum LC:Lcom/squareup/CountryCode;

.field public static final enum LI:Lcom/squareup/CountryCode;

.field public static final enum LK:Lcom/squareup/CountryCode;

.field public static final enum LR:Lcom/squareup/CountryCode;

.field public static final enum LS:Lcom/squareup/CountryCode;

.field public static final enum LT:Lcom/squareup/CountryCode;

.field public static final enum LU:Lcom/squareup/CountryCode;

.field public static final enum LV:Lcom/squareup/CountryCode;

.field public static final enum LY:Lcom/squareup/CountryCode;

.field public static final enum MA:Lcom/squareup/CountryCode;

.field public static final enum MD:Lcom/squareup/CountryCode;

.field public static final enum MG:Lcom/squareup/CountryCode;

.field public static final enum MK:Lcom/squareup/CountryCode;

.field public static final enum ML:Lcom/squareup/CountryCode;

.field public static final enum MM:Lcom/squareup/CountryCode;

.field public static final enum MN:Lcom/squareup/CountryCode;

.field public static final enum MO:Lcom/squareup/CountryCode;

.field public static final enum MP:Lcom/squareup/CountryCode;

.field public static final enum MR:Lcom/squareup/CountryCode;

.field public static final enum MS:Lcom/squareup/CountryCode;

.field public static final enum MT:Lcom/squareup/CountryCode;

.field public static final enum MU:Lcom/squareup/CountryCode;

.field public static final enum MW:Lcom/squareup/CountryCode;

.field public static final enum MX:Lcom/squareup/CountryCode;

.field public static final enum MY:Lcom/squareup/CountryCode;

.field public static final enum MZ:Lcom/squareup/CountryCode;

.field public static final enum NA:Lcom/squareup/CountryCode;

.field public static final enum NE:Lcom/squareup/CountryCode;

.field public static final enum NG:Lcom/squareup/CountryCode;

.field public static final enum NI:Lcom/squareup/CountryCode;

.field public static final enum NL:Lcom/squareup/CountryCode;

.field public static final enum NO:Lcom/squareup/CountryCode;

.field public static final enum NP:Lcom/squareup/CountryCode;

.field public static final enum NZ:Lcom/squareup/CountryCode;

.field public static final enum OM:Lcom/squareup/CountryCode;

.field public static final enum PA:Lcom/squareup/CountryCode;

.field public static final enum PE:Lcom/squareup/CountryCode;

.field public static final enum PG:Lcom/squareup/CountryCode;

.field public static final enum PH:Lcom/squareup/CountryCode;

.field public static final enum PK:Lcom/squareup/CountryCode;

.field public static final enum PL:Lcom/squareup/CountryCode;

.field public static final enum PR:Lcom/squareup/CountryCode;

.field public static final enum PT:Lcom/squareup/CountryCode;

.field public static final enum PW:Lcom/squareup/CountryCode;

.field public static final enum PY:Lcom/squareup/CountryCode;

.field public static final enum QA:Lcom/squareup/CountryCode;

.field public static final enum RO:Lcom/squareup/CountryCode;

.field public static final enum RS:Lcom/squareup/CountryCode;

.field public static final enum RU:Lcom/squareup/CountryCode;

.field public static final enum RW:Lcom/squareup/CountryCode;

.field public static final enum SA:Lcom/squareup/CountryCode;

.field public static final enum SB:Lcom/squareup/CountryCode;

.field public static final enum SC:Lcom/squareup/CountryCode;

.field public static final enum SE:Lcom/squareup/CountryCode;

.field public static final enum SG:Lcom/squareup/CountryCode;

.field public static final enum SI:Lcom/squareup/CountryCode;

.field public static final enum SK:Lcom/squareup/CountryCode;

.field public static final enum SL:Lcom/squareup/CountryCode;

.field public static final enum SN:Lcom/squareup/CountryCode;

.field public static final enum SR:Lcom/squareup/CountryCode;

.field public static final enum ST:Lcom/squareup/CountryCode;

.field public static final enum SV:Lcom/squareup/CountryCode;

.field public static final enum SZ:Lcom/squareup/CountryCode;

.field public static final enum TC:Lcom/squareup/CountryCode;

.field public static final enum TD:Lcom/squareup/CountryCode;

.field public static final enum TG:Lcom/squareup/CountryCode;

.field public static final enum TH:Lcom/squareup/CountryCode;

.field public static final enum TJ:Lcom/squareup/CountryCode;

.field public static final enum TM:Lcom/squareup/CountryCode;

.field public static final enum TN:Lcom/squareup/CountryCode;

.field public static final enum TO:Lcom/squareup/CountryCode;

.field public static final enum TR:Lcom/squareup/CountryCode;

.field public static final enum TT:Lcom/squareup/CountryCode;

.field public static final enum TV:Lcom/squareup/CountryCode;

.field public static final enum TW:Lcom/squareup/CountryCode;

.field public static final enum TZ:Lcom/squareup/CountryCode;

.field public static final enum UA:Lcom/squareup/CountryCode;

.field public static final enum UG:Lcom/squareup/CountryCode;

.field public static final enum US:Lcom/squareup/CountryCode;

.field public static final enum UY:Lcom/squareup/CountryCode;

.field public static final enum UZ:Lcom/squareup/CountryCode;

.field public static final enum VC:Lcom/squareup/CountryCode;

.field public static final enum VE:Lcom/squareup/CountryCode;

.field public static final enum VG:Lcom/squareup/CountryCode;

.field public static final enum VI:Lcom/squareup/CountryCode;

.field public static final enum VN:Lcom/squareup/CountryCode;

.field public static final enum YE:Lcom/squareup/CountryCode;

.field public static final enum ZA:Lcom/squareup/CountryCode;

.field public static final enum ZM:Lcom/squareup/CountryCode;

.field public static final enum ZW:Lcom/squareup/CountryCode;


# instance fields
.field public final hasPayments:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v1, 0x0

    const-string v2, "AL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AL:Lcom/squareup/CountryCode;

    .line 11
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v2, 0x1

    const-string v3, "DZ"

    invoke-direct {v0, v3, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->DZ:Lcom/squareup/CountryCode;

    .line 13
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v3, 0x2

    const-string v4, "AO"

    invoke-direct {v0, v4, v3}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AO:Lcom/squareup/CountryCode;

    .line 15
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v4, 0x3

    const-string v5, "AI"

    invoke-direct {v0, v5, v4}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AI:Lcom/squareup/CountryCode;

    .line 17
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v5, 0x4

    const-string v6, "AG"

    invoke-direct {v0, v6, v5}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AG:Lcom/squareup/CountryCode;

    .line 19
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v6, 0x5

    const-string v7, "AR"

    invoke-direct {v0, v7, v6}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AR:Lcom/squareup/CountryCode;

    .line 21
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v7, 0x6

    const-string v8, "AM"

    invoke-direct {v0, v8, v7}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AM:Lcom/squareup/CountryCode;

    .line 23
    new-instance v0, Lcom/squareup/CountryCode;

    const/4 v8, 0x7

    const-string v9, "AW"

    invoke-direct {v0, v9, v8}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AW:Lcom/squareup/CountryCode;

    .line 25
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v9, 0x8

    const-string v10, "AU"

    invoke-direct {v0, v10, v9, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    .line 27
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v10, 0x9

    const-string v11, "AT"

    invoke-direct {v0, v11, v10}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AT:Lcom/squareup/CountryCode;

    .line 29
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v11, 0xa

    const-string v12, "AZ"

    invoke-direct {v0, v12, v11}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AZ:Lcom/squareup/CountryCode;

    .line 31
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v12, 0xb

    const-string v13, "BS"

    invoke-direct {v0, v13, v12}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BS:Lcom/squareup/CountryCode;

    .line 33
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v13, 0xc

    const-string v14, "BH"

    invoke-direct {v0, v14, v13}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BH:Lcom/squareup/CountryCode;

    .line 35
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v14, 0xd

    const-string v15, "BD"

    invoke-direct {v0, v15, v14}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BD:Lcom/squareup/CountryCode;

    .line 37
    new-instance v0, Lcom/squareup/CountryCode;

    const/16 v15, 0xe

    const-string v14, "BB"

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BB:Lcom/squareup/CountryCode;

    .line 39
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BY"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BY:Lcom/squareup/CountryCode;

    .line 41
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BE"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BE:Lcom/squareup/CountryCode;

    .line 43
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BZ"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BZ:Lcom/squareup/CountryCode;

    .line 45
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BJ"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BJ:Lcom/squareup/CountryCode;

    .line 47
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BM"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BM:Lcom/squareup/CountryCode;

    .line 49
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BT"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BT:Lcom/squareup/CountryCode;

    .line 51
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BO"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BO:Lcom/squareup/CountryCode;

    .line 53
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BA"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BA:Lcom/squareup/CountryCode;

    .line 55
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BW"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BW:Lcom/squareup/CountryCode;

    .line 57
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BR"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BR:Lcom/squareup/CountryCode;

    .line 59
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BN"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BN:Lcom/squareup/CountryCode;

    .line 61
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BG"

    const/16 v15, 0x1a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BG:Lcom/squareup/CountryCode;

    .line 63
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "BF"

    const/16 v15, 0x1b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->BF:Lcom/squareup/CountryCode;

    .line 65
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CV"

    const/16 v15, 0x1c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CV:Lcom/squareup/CountryCode;

    .line 67
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KH"

    const/16 v15, 0x1d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KH:Lcom/squareup/CountryCode;

    .line 69
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CM"

    const/16 v15, 0x1e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CM:Lcom/squareup/CountryCode;

    .line 71
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CA"

    const/16 v15, 0x1f

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    .line 73
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KY"

    const/16 v15, 0x20

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KY:Lcom/squareup/CountryCode;

    .line 75
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TD"

    const/16 v15, 0x21

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TD:Lcom/squareup/CountryCode;

    .line 77
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CL"

    const/16 v15, 0x22

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CL:Lcom/squareup/CountryCode;

    .line 79
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CN"

    const/16 v15, 0x23

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CN:Lcom/squareup/CountryCode;

    .line 81
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CO"

    const/16 v15, 0x24

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CO:Lcom/squareup/CountryCode;

    .line 83
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CG"

    const/16 v15, 0x25

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CG:Lcom/squareup/CountryCode;

    .line 85
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CR"

    const/16 v15, 0x26

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CR:Lcom/squareup/CountryCode;

    .line 87
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CI"

    const/16 v15, 0x27

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CI:Lcom/squareup/CountryCode;

    .line 89
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "HR"

    const/16 v15, 0x28

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->HR:Lcom/squareup/CountryCode;

    .line 91
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CY"

    const/16 v15, 0x29

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CY:Lcom/squareup/CountryCode;

    .line 93
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CZ"

    const/16 v15, 0x2a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CZ:Lcom/squareup/CountryCode;

    .line 95
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "DK"

    const/16 v15, 0x2b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->DK:Lcom/squareup/CountryCode;

    .line 97
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "DM"

    const/16 v15, 0x2c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->DM:Lcom/squareup/CountryCode;

    .line 99
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "DO"

    const/16 v15, 0x2d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->DO:Lcom/squareup/CountryCode;

    .line 101
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "EC"

    const/16 v15, 0x2e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->EC:Lcom/squareup/CountryCode;

    .line 103
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "EG"

    const/16 v15, 0x2f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->EG:Lcom/squareup/CountryCode;

    .line 105
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SV"

    const/16 v15, 0x30

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SV:Lcom/squareup/CountryCode;

    .line 107
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "EE"

    const/16 v15, 0x31

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->EE:Lcom/squareup/CountryCode;

    .line 109
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "FJ"

    const/16 v15, 0x32

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->FJ:Lcom/squareup/CountryCode;

    .line 111
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "FI"

    const/16 v15, 0x33

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->FI:Lcom/squareup/CountryCode;

    .line 113
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "FR"

    const/16 v15, 0x34

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    .line 115
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GA"

    const/16 v15, 0x35

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GA:Lcom/squareup/CountryCode;

    .line 117
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GM"

    const/16 v15, 0x36

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GM:Lcom/squareup/CountryCode;

    .line 119
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "DE"

    const/16 v15, 0x37

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->DE:Lcom/squareup/CountryCode;

    .line 121
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GH"

    const/16 v15, 0x38

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GH:Lcom/squareup/CountryCode;

    .line 123
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GR"

    const/16 v15, 0x39

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GR:Lcom/squareup/CountryCode;

    .line 125
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GD"

    const/16 v15, 0x3a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GD:Lcom/squareup/CountryCode;

    .line 127
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GU"

    const/16 v15, 0x3b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GU:Lcom/squareup/CountryCode;

    .line 129
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GT"

    const/16 v15, 0x3c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GT:Lcom/squareup/CountryCode;

    .line 131
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GW"

    const/16 v15, 0x3d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GW:Lcom/squareup/CountryCode;

    .line 133
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GY"

    const/16 v15, 0x3e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->GY:Lcom/squareup/CountryCode;

    .line 135
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "HT"

    const/16 v15, 0x3f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->HT:Lcom/squareup/CountryCode;

    .line 137
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "HN"

    const/16 v15, 0x40

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->HN:Lcom/squareup/CountryCode;

    .line 139
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "HK"

    const/16 v15, 0x41

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->HK:Lcom/squareup/CountryCode;

    .line 141
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "HU"

    const/16 v15, 0x42

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->HU:Lcom/squareup/CountryCode;

    .line 143
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "IS"

    const/16 v15, 0x43

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->IS:Lcom/squareup/CountryCode;

    .line 145
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "IN"

    const/16 v15, 0x44

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->IN:Lcom/squareup/CountryCode;

    .line 147
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ID"

    const/16 v15, 0x45

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ID:Lcom/squareup/CountryCode;

    .line 149
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "IE"

    const/16 v15, 0x46

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->IE:Lcom/squareup/CountryCode;

    .line 151
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "IL"

    const/16 v15, 0x47

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->IL:Lcom/squareup/CountryCode;

    .line 153
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "IT"

    const/16 v15, 0x48

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->IT:Lcom/squareup/CountryCode;

    .line 155
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "JM"

    const/16 v15, 0x49

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->JM:Lcom/squareup/CountryCode;

    .line 157
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "JP"

    const/16 v15, 0x4a

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    .line 159
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "JO"

    const/16 v15, 0x4b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->JO:Lcom/squareup/CountryCode;

    .line 161
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KZ"

    const/16 v15, 0x4c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KZ:Lcom/squareup/CountryCode;

    .line 163
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KE"

    const/16 v15, 0x4d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KE:Lcom/squareup/CountryCode;

    .line 165
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KW"

    const/16 v15, 0x4e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KW:Lcom/squareup/CountryCode;

    .line 167
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KG"

    const/16 v15, 0x4f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KG:Lcom/squareup/CountryCode;

    .line 169
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LA"

    const/16 v15, 0x50

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LA:Lcom/squareup/CountryCode;

    .line 171
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LV"

    const/16 v15, 0x51

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LV:Lcom/squareup/CountryCode;

    .line 173
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LB"

    const/16 v15, 0x52

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LB:Lcom/squareup/CountryCode;

    .line 175
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LS"

    const/16 v15, 0x53

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LS:Lcom/squareup/CountryCode;

    .line 177
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LR"

    const/16 v15, 0x54

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LR:Lcom/squareup/CountryCode;

    .line 179
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LY"

    const/16 v15, 0x55

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LY:Lcom/squareup/CountryCode;

    .line 181
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LI"

    const/16 v15, 0x56

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LI:Lcom/squareup/CountryCode;

    .line 183
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LT"

    const/16 v15, 0x57

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LT:Lcom/squareup/CountryCode;

    .line 185
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LU"

    const/16 v15, 0x58

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LU:Lcom/squareup/CountryCode;

    .line 187
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MO"

    const/16 v15, 0x59

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MO:Lcom/squareup/CountryCode;

    .line 189
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MK"

    const/16 v15, 0x5a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MK:Lcom/squareup/CountryCode;

    .line 191
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MG"

    const/16 v15, 0x5b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MG:Lcom/squareup/CountryCode;

    .line 193
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MW"

    const/16 v15, 0x5c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MW:Lcom/squareup/CountryCode;

    .line 195
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MY"

    const/16 v15, 0x5d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MY:Lcom/squareup/CountryCode;

    .line 197
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ML"

    const/16 v15, 0x5e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ML:Lcom/squareup/CountryCode;

    .line 199
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MT"

    const/16 v15, 0x5f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MT:Lcom/squareup/CountryCode;

    .line 201
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MR"

    const/16 v15, 0x60

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MR:Lcom/squareup/CountryCode;

    .line 203
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MU"

    const/16 v15, 0x61

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MU:Lcom/squareup/CountryCode;

    .line 205
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MX"

    const/16 v15, 0x62

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MX:Lcom/squareup/CountryCode;

    .line 207
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "FM"

    const/16 v15, 0x63

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->FM:Lcom/squareup/CountryCode;

    .line 209
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MD"

    const/16 v15, 0x64

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MD:Lcom/squareup/CountryCode;

    .line 211
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MN"

    const/16 v15, 0x65

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MN:Lcom/squareup/CountryCode;

    .line 213
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MS"

    const/16 v15, 0x66

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MS:Lcom/squareup/CountryCode;

    .line 215
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MA"

    const/16 v15, 0x67

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MA:Lcom/squareup/CountryCode;

    .line 217
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MZ"

    const/16 v15, 0x68

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MZ:Lcom/squareup/CountryCode;

    .line 219
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MM"

    const/16 v15, 0x69

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MM:Lcom/squareup/CountryCode;

    .line 221
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NA"

    const/16 v15, 0x6a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NA:Lcom/squareup/CountryCode;

    .line 223
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NP"

    const/16 v15, 0x6b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NP:Lcom/squareup/CountryCode;

    .line 225
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NL"

    const/16 v15, 0x6c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NL:Lcom/squareup/CountryCode;

    .line 227
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NZ"

    const/16 v15, 0x6d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NZ:Lcom/squareup/CountryCode;

    .line 229
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NI"

    const/16 v15, 0x6e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NI:Lcom/squareup/CountryCode;

    .line 231
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NE"

    const/16 v15, 0x6f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NE:Lcom/squareup/CountryCode;

    .line 233
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NG"

    const/16 v15, 0x70

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NG:Lcom/squareup/CountryCode;

    .line 235
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "MP"

    const/16 v15, 0x71

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->MP:Lcom/squareup/CountryCode;

    .line 237
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "NO"

    const/16 v15, 0x72

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->NO:Lcom/squareup/CountryCode;

    .line 239
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "OM"

    const/16 v15, 0x73

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->OM:Lcom/squareup/CountryCode;

    .line 241
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PK"

    const/16 v15, 0x74

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PK:Lcom/squareup/CountryCode;

    .line 243
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PW"

    const/16 v15, 0x75

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PW:Lcom/squareup/CountryCode;

    .line 245
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PA"

    const/16 v15, 0x76

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PA:Lcom/squareup/CountryCode;

    .line 247
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PG"

    const/16 v15, 0x77

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PG:Lcom/squareup/CountryCode;

    .line 249
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PY"

    const/16 v15, 0x78

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PY:Lcom/squareup/CountryCode;

    .line 251
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PE"

    const/16 v15, 0x79

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PE:Lcom/squareup/CountryCode;

    .line 253
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PH"

    const/16 v15, 0x7a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PH:Lcom/squareup/CountryCode;

    .line 255
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PL"

    const/16 v15, 0x7b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PL:Lcom/squareup/CountryCode;

    .line 257
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PT"

    const/16 v15, 0x7c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PT:Lcom/squareup/CountryCode;

    .line 259
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "PR"

    const/16 v15, 0x7d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->PR:Lcom/squareup/CountryCode;

    .line 261
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "QA"

    const/16 v15, 0x7e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->QA:Lcom/squareup/CountryCode;

    .line 263
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "RO"

    const/16 v15, 0x7f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->RO:Lcom/squareup/CountryCode;

    .line 265
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "RU"

    const/16 v15, 0x80

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->RU:Lcom/squareup/CountryCode;

    .line 267
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "RW"

    const/16 v15, 0x81

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->RW:Lcom/squareup/CountryCode;

    .line 269
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ST"

    const/16 v15, 0x82

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ST:Lcom/squareup/CountryCode;

    .line 271
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SA"

    const/16 v15, 0x83

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SA:Lcom/squareup/CountryCode;

    .line 273
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SN"

    const/16 v15, 0x84

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SN:Lcom/squareup/CountryCode;

    .line 275
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "RS"

    const/16 v15, 0x85

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->RS:Lcom/squareup/CountryCode;

    .line 277
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SC"

    const/16 v15, 0x86

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SC:Lcom/squareup/CountryCode;

    .line 279
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SL"

    const/16 v15, 0x87

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SL:Lcom/squareup/CountryCode;

    .line 281
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SG"

    const/16 v15, 0x88

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SG:Lcom/squareup/CountryCode;

    .line 283
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SK"

    const/16 v15, 0x89

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SK:Lcom/squareup/CountryCode;

    .line 285
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SI"

    const/16 v15, 0x8a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SI:Lcom/squareup/CountryCode;

    .line 287
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SB"

    const/16 v15, 0x8b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SB:Lcom/squareup/CountryCode;

    .line 289
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ZA"

    const/16 v15, 0x8c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ZA:Lcom/squareup/CountryCode;

    .line 291
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KR"

    const/16 v15, 0x8d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KR:Lcom/squareup/CountryCode;

    .line 293
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ES"

    const/16 v15, 0x8e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ES:Lcom/squareup/CountryCode;

    .line 295
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LK"

    const/16 v15, 0x8f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LK:Lcom/squareup/CountryCode;

    .line 297
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "KN"

    const/16 v15, 0x90

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->KN:Lcom/squareup/CountryCode;

    .line 299
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "LC"

    const/16 v15, 0x91

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->LC:Lcom/squareup/CountryCode;

    .line 301
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "VC"

    const/16 v15, 0x92

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->VC:Lcom/squareup/CountryCode;

    .line 303
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SR"

    const/16 v15, 0x93

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SR:Lcom/squareup/CountryCode;

    .line 305
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SZ"

    const/16 v15, 0x94

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SZ:Lcom/squareup/CountryCode;

    .line 307
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "SE"

    const/16 v15, 0x95

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->SE:Lcom/squareup/CountryCode;

    .line 309
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "CH"

    const/16 v15, 0x96

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->CH:Lcom/squareup/CountryCode;

    .line 311
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TW"

    const/16 v15, 0x97

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TW:Lcom/squareup/CountryCode;

    .line 313
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TJ"

    const/16 v15, 0x98

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TJ:Lcom/squareup/CountryCode;

    .line 315
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TZ"

    const/16 v15, 0x99

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TZ:Lcom/squareup/CountryCode;

    .line 317
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TH"

    const/16 v15, 0x9a

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TH:Lcom/squareup/CountryCode;

    .line 319
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TG"

    const/16 v15, 0x9b

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TG:Lcom/squareup/CountryCode;

    .line 321
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TO"

    const/16 v15, 0x9c

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TO:Lcom/squareup/CountryCode;

    .line 323
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TT"

    const/16 v15, 0x9d

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TT:Lcom/squareup/CountryCode;

    .line 325
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TN"

    const/16 v15, 0x9e

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TN:Lcom/squareup/CountryCode;

    .line 327
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TR"

    const/16 v15, 0x9f

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TR:Lcom/squareup/CountryCode;

    .line 329
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TM"

    const/16 v15, 0xa0

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TM:Lcom/squareup/CountryCode;

    .line 331
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TC"

    const/16 v15, 0xa1

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TC:Lcom/squareup/CountryCode;

    .line 333
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "TV"

    const/16 v15, 0xa2

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->TV:Lcom/squareup/CountryCode;

    .line 335
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "UG"

    const/16 v15, 0xa3

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->UG:Lcom/squareup/CountryCode;

    .line 337
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "UA"

    const/16 v15, 0xa4

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->UA:Lcom/squareup/CountryCode;

    .line 339
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "AE"

    const/16 v15, 0xa5

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->AE:Lcom/squareup/CountryCode;

    .line 341
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "GB"

    const/16 v15, 0xa6

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    .line 343
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "US"

    const/16 v15, 0xa7

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    .line 345
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "UY"

    const/16 v15, 0xa8

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->UY:Lcom/squareup/CountryCode;

    .line 347
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "UZ"

    const/16 v15, 0xa9

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->UZ:Lcom/squareup/CountryCode;

    .line 349
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "VE"

    const/16 v15, 0xaa

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->VE:Lcom/squareup/CountryCode;

    .line 351
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "VN"

    const/16 v15, 0xab

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->VN:Lcom/squareup/CountryCode;

    .line 353
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "VG"

    const/16 v15, 0xac

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->VG:Lcom/squareup/CountryCode;

    .line 355
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "VI"

    const/16 v15, 0xad

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->VI:Lcom/squareup/CountryCode;

    .line 357
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "YE"

    const/16 v15, 0xae

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->YE:Lcom/squareup/CountryCode;

    .line 359
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ZM"

    const/16 v15, 0xaf

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ZM:Lcom/squareup/CountryCode;

    .line 361
    new-instance v0, Lcom/squareup/CountryCode;

    const-string v14, "ZW"

    const/16 v15, 0xb0

    invoke-direct {v0, v14, v15}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/CountryCode;->ZW:Lcom/squareup/CountryCode;

    const/16 v0, 0xb1

    new-array v0, v0, [Lcom/squareup/CountryCode;

    .line 7
    sget-object v14, Lcom/squareup/CountryCode;->AL:Lcom/squareup/CountryCode;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/CountryCode;->DZ:Lcom/squareup/CountryCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->AO:Lcom/squareup/CountryCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/CountryCode;->AI:Lcom/squareup/CountryCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/CountryCode;->AG:Lcom/squareup/CountryCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/CountryCode;->AR:Lcom/squareup/CountryCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/CountryCode;->AM:Lcom/squareup/CountryCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/CountryCode;->AW:Lcom/squareup/CountryCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/CountryCode;->AT:Lcom/squareup/CountryCode;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/CountryCode;->AZ:Lcom/squareup/CountryCode;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/CountryCode;->BS:Lcom/squareup/CountryCode;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/CountryCode;->BH:Lcom/squareup/CountryCode;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/CountryCode;->BD:Lcom/squareup/CountryCode;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BB:Lcom/squareup/CountryCode;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BY:Lcom/squareup/CountryCode;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BE:Lcom/squareup/CountryCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BJ:Lcom/squareup/CountryCode;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BM:Lcom/squareup/CountryCode;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BT:Lcom/squareup/CountryCode;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BO:Lcom/squareup/CountryCode;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BA:Lcom/squareup/CountryCode;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BW:Lcom/squareup/CountryCode;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BR:Lcom/squareup/CountryCode;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BN:Lcom/squareup/CountryCode;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BG:Lcom/squareup/CountryCode;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->BF:Lcom/squareup/CountryCode;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CV:Lcom/squareup/CountryCode;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KH:Lcom/squareup/CountryCode;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CM:Lcom/squareup/CountryCode;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KY:Lcom/squareup/CountryCode;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TD:Lcom/squareup/CountryCode;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CL:Lcom/squareup/CountryCode;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CN:Lcom/squareup/CountryCode;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CO:Lcom/squareup/CountryCode;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CG:Lcom/squareup/CountryCode;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CR:Lcom/squareup/CountryCode;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CI:Lcom/squareup/CountryCode;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->HR:Lcom/squareup/CountryCode;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CY:Lcom/squareup/CountryCode;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->DK:Lcom/squareup/CountryCode;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->DM:Lcom/squareup/CountryCode;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->DO:Lcom/squareup/CountryCode;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->EC:Lcom/squareup/CountryCode;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->EG:Lcom/squareup/CountryCode;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SV:Lcom/squareup/CountryCode;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->EE:Lcom/squareup/CountryCode;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->FJ:Lcom/squareup/CountryCode;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->FI:Lcom/squareup/CountryCode;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GA:Lcom/squareup/CountryCode;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GM:Lcom/squareup/CountryCode;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->DE:Lcom/squareup/CountryCode;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GH:Lcom/squareup/CountryCode;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GR:Lcom/squareup/CountryCode;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GD:Lcom/squareup/CountryCode;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GU:Lcom/squareup/CountryCode;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GT:Lcom/squareup/CountryCode;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GW:Lcom/squareup/CountryCode;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GY:Lcom/squareup/CountryCode;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->HT:Lcom/squareup/CountryCode;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->HN:Lcom/squareup/CountryCode;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->HK:Lcom/squareup/CountryCode;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->HU:Lcom/squareup/CountryCode;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->IS:Lcom/squareup/CountryCode;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->IN:Lcom/squareup/CountryCode;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ID:Lcom/squareup/CountryCode;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->IE:Lcom/squareup/CountryCode;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->IL:Lcom/squareup/CountryCode;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->IT:Lcom/squareup/CountryCode;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->JM:Lcom/squareup/CountryCode;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->JO:Lcom/squareup/CountryCode;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KE:Lcom/squareup/CountryCode;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KW:Lcom/squareup/CountryCode;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KG:Lcom/squareup/CountryCode;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LA:Lcom/squareup/CountryCode;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LV:Lcom/squareup/CountryCode;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LB:Lcom/squareup/CountryCode;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LS:Lcom/squareup/CountryCode;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LR:Lcom/squareup/CountryCode;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LY:Lcom/squareup/CountryCode;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LI:Lcom/squareup/CountryCode;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LT:Lcom/squareup/CountryCode;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LU:Lcom/squareup/CountryCode;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MO:Lcom/squareup/CountryCode;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MK:Lcom/squareup/CountryCode;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MG:Lcom/squareup/CountryCode;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MW:Lcom/squareup/CountryCode;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MY:Lcom/squareup/CountryCode;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ML:Lcom/squareup/CountryCode;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MT:Lcom/squareup/CountryCode;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MR:Lcom/squareup/CountryCode;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MU:Lcom/squareup/CountryCode;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MX:Lcom/squareup/CountryCode;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->FM:Lcom/squareup/CountryCode;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MD:Lcom/squareup/CountryCode;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MN:Lcom/squareup/CountryCode;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MS:Lcom/squareup/CountryCode;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MA:Lcom/squareup/CountryCode;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MM:Lcom/squareup/CountryCode;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NA:Lcom/squareup/CountryCode;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NP:Lcom/squareup/CountryCode;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NL:Lcom/squareup/CountryCode;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NI:Lcom/squareup/CountryCode;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NE:Lcom/squareup/CountryCode;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NG:Lcom/squareup/CountryCode;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->MP:Lcom/squareup/CountryCode;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->NO:Lcom/squareup/CountryCode;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->OM:Lcom/squareup/CountryCode;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PK:Lcom/squareup/CountryCode;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PW:Lcom/squareup/CountryCode;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PA:Lcom/squareup/CountryCode;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PG:Lcom/squareup/CountryCode;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PY:Lcom/squareup/CountryCode;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PE:Lcom/squareup/CountryCode;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PH:Lcom/squareup/CountryCode;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PL:Lcom/squareup/CountryCode;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PT:Lcom/squareup/CountryCode;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->PR:Lcom/squareup/CountryCode;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->QA:Lcom/squareup/CountryCode;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->RO:Lcom/squareup/CountryCode;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->RU:Lcom/squareup/CountryCode;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->RW:Lcom/squareup/CountryCode;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ST:Lcom/squareup/CountryCode;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SA:Lcom/squareup/CountryCode;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SN:Lcom/squareup/CountryCode;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->RS:Lcom/squareup/CountryCode;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SC:Lcom/squareup/CountryCode;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SL:Lcom/squareup/CountryCode;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SG:Lcom/squareup/CountryCode;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SK:Lcom/squareup/CountryCode;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SI:Lcom/squareup/CountryCode;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SB:Lcom/squareup/CountryCode;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ZA:Lcom/squareup/CountryCode;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KR:Lcom/squareup/CountryCode;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ES:Lcom/squareup/CountryCode;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LK:Lcom/squareup/CountryCode;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->KN:Lcom/squareup/CountryCode;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->LC:Lcom/squareup/CountryCode;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->VC:Lcom/squareup/CountryCode;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SR:Lcom/squareup/CountryCode;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->SE:Lcom/squareup/CountryCode;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->CH:Lcom/squareup/CountryCode;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TW:Lcom/squareup/CountryCode;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TJ:Lcom/squareup/CountryCode;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TZ:Lcom/squareup/CountryCode;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TH:Lcom/squareup/CountryCode;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TG:Lcom/squareup/CountryCode;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TO:Lcom/squareup/CountryCode;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TT:Lcom/squareup/CountryCode;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TN:Lcom/squareup/CountryCode;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TR:Lcom/squareup/CountryCode;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TM:Lcom/squareup/CountryCode;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TC:Lcom/squareup/CountryCode;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->TV:Lcom/squareup/CountryCode;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->UG:Lcom/squareup/CountryCode;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->UA:Lcom/squareup/CountryCode;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->AE:Lcom/squareup/CountryCode;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->UY:Lcom/squareup/CountryCode;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->UZ:Lcom/squareup/CountryCode;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->VE:Lcom/squareup/CountryCode;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->VN:Lcom/squareup/CountryCode;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->VG:Lcom/squareup/CountryCode;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->VI:Lcom/squareup/CountryCode;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->YE:Lcom/squareup/CountryCode;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ZM:Lcom/squareup/CountryCode;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/CountryCode;->ZW:Lcom/squareup/CountryCode;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/CountryCode;->$VALUES:[Lcom/squareup/CountryCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 367
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/CountryCode;-><init>(Ljava/lang/String;IZ)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 371
    iput-boolean p3, p0, Lcom/squareup/CountryCode;->hasPayments:Z

    return-void
.end method

.method public static parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 379
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 383
    :cond_0
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/CountryCode;->valueOf(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    :cond_1
    :goto_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/CountryCode;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/CountryCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/CountryCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/CountryCode;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/CountryCode;->$VALUES:[Lcom/squareup/CountryCode;

    invoke-virtual {v0}, [Lcom/squareup/CountryCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/CountryCode;

    return-object v0
.end method
