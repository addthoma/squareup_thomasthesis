.class public Lcom/squareup/caller/ProgressDialogCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ProgressDialogCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/caller/ProgressDialogCoordinator$Provider;
    }
.end annotation


# static fields
.field private static final AUTO_DISMISS_TIMEOUT_MS:I = 0x7d0


# instance fields
.field private final autoDismiss:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final autoDismissSubscription:Lrx/subscriptions/CompositeSubscription;

.field private complete:Landroid/view/View;

.field private container:Landroid/view/View;

.field private message:Landroid/widget/TextView;

.field private final onContainerClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private progress:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    .line 87
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 84
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismissSubscription:Lrx/subscriptions/CompositeSubscription;

    .line 88
    new-instance v0, Lcom/squareup/util/RxWatchdog;

    invoke-direct {v0, p2, p3}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismiss:Lcom/squareup/util/RxWatchdog;

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->bindViews(Landroid/view/View;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->onContainerClicked:Lrx/Observable;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/caller/ProgressDialogCoordinator$1;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/caller/ProgressDialogCoordinator;-><init>(Landroid/view/View;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 127
    iput-object p1, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    .line 128
    sget v0, Lcom/squareup/widgets/pos/R$id;->progress:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->progress:Landroid/view/View;

    .line 129
    sget v0, Lcom/squareup/widgets/pos/R$id;->complete:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->complete:Landroid/view/View;

    .line 130
    sget v0, Lcom/squareup/noho/R$id;->message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->message:Landroid/widget/TextView;

    return-void
.end method

.method public static createDialog(Landroid/view/View;)Landroid/app/Dialog;
    .locals 3

    .line 47
    new-instance v0, Lcom/squareup/dialog/GlassDialog;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 49
    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 51
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    const/4 p0, 0x0

    .line 52
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 56
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    return-object v0
.end method

.method public static inflate(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .line 43
    sget v0, Lcom/squareup/widgets/pos/R$layout;->smoked_glass_dialog:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$scheduleAutoDismiss$0(Lrx/Subscription;)Lrx/Subscription;
    .locals 0

    return-object p0
.end method

.method private scheduleAutoDismiss()Lrx/Completable;
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismissSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->hasSubscriptions()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Auto-dismiss cannot be scheduled twice"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismiss:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const-wide/16 v2, 0x7d0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->onContainerClicked:Lrx/Observable;

    iget-object v1, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismiss:Lcom/squareup/util/RxWatchdog;

    .line 145
    invoke-virtual {v1}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    .line 144
    invoke-static {v0, v1}, Lrx/Observable;->amb(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lrx/Observable;->publish()Lrx/observables/ConnectableObservable;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lrx/observables/ConnectableObservable;->connect()Lrx/Subscription;

    move-result-object v1

    .line 151
    iget-object v2, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismissSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v2, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 153
    iget-object v2, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    new-instance v3, Lcom/squareup/caller/-$$Lambda$ProgressDialogCoordinator$oWLkPptCh8K42JfKlf75rQhqyg4;

    invoke-direct {v3, v1}, Lcom/squareup/caller/-$$Lambda$ProgressDialogCoordinator$oWLkPptCh8K42JfKlf75rQhqyg4;-><init>(Lrx/Subscription;)V

    invoke-static {v2, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 155
    invoke-virtual {v0}, Lrx/observables/ConnectableObservable;->toCompletable()Lrx/Completable;

    move-result-object v0

    return-object v0
.end method

.method private showMessage(Ljava/lang/String;)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->message:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Attached to the wrong view"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    return-void
.end method

.method public cancelAutoDismiss()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->autoDismissSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method public showComplete(I)Lrx/Completable;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 112
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showComplete(Ljava/lang/String;)Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method public showComplete(Ljava/lang/String;)Lrx/Completable;
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->progress:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->complete:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 118
    invoke-direct {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showMessage(Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Lcom/squareup/caller/ProgressDialogCoordinator;->scheduleAutoDismiss()Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method public showProgress(I)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->container:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 100
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showProgress(Ljava/lang/String;)V

    return-void
.end method

.method public showProgress(Ljava/lang/String;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->progress:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/caller/ProgressDialogCoordinator;->complete:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-direct {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showMessage(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/caller/ProgressDialogCoordinator;->cancelAutoDismiss()V

    return-void
.end method
