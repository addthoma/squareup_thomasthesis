.class public final Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;
.super Ljava/lang/Object;
.source "DefaultTenderOptionListsFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;",
            ">;)",
            "Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/tenderpayment/TenderSettingsFactory;Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;)Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;-><init>(Lcom/squareup/tenderpayment/TenderSettingsFactory;Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;
    .locals 5

    .line 41
    iget-object v0, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderSettingsFactory;

    iget-object v1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;

    iget-object v2, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;

    iget-object v3, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;

    iget-object v4, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->newInstance(Lcom/squareup/tenderpayment/TenderSettingsFactory;Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;)Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory_Factory;->get()Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;

    move-result-object v0

    return-object v0
.end method
