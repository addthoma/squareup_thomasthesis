.class public final Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;
.super Ljava/lang/Object;
.source "PaymentProcessDecider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\r\u001a\u00020\u000eH\u0002J\u0008\u0010\u000f\u001a\u00020\u000eH\u0002J\u0008\u0010\u0010\u001a\u00020\u000eH\u0002J\u0008\u0010\u0011\u001a\u00020\u000eH\u0002J\u0008\u0010\u0012\u001a\u00020\u000eH\u0002J\u0008\u0010\u0013\u001a\u00020\u000eH\u0002J\u0008\u0010\u0014\u001a\u00020\u000eH\u0002J\u0008\u0010\u0015\u001a\u00020\u000eH\u0002J\u0008\u0010\u0016\u001a\u00020\u000eH\u0002J\u0008\u0010\u0017\u001a\u00020\u000eH\u0002J\u0006\u0010\u0018\u001a\u00020\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;",
        "",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "skipReceiptScreenSettings",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V",
        "hasCustomer",
        "",
        "hasDiningOption",
        "hasTax",
        "hasTicketName",
        "isLoyaltyEnabled",
        "isOpenTicket",
        "isPreTaxTippingEnabled",
        "isReceiptEnabled",
        "isSingleCustomAmountLineItem",
        "isSplitTender",
        "shouldProcessPaymentUsingPaymentV2",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skipReceiptScreenSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iput-object p3, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p4, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p5, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final hasCustomer()Z
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    return v0
.end method

.method private final hasDiningOption()Z
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result v0

    return v0
.end method

.method private final hasTax()Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasInterestingTaxState()Z

    move-result v0

    return v0
.end method

.method private final hasTicketName()Z
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v0

    return v0
.end method

.method private final isLoyaltyEnabled()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final isOpenTicket()Z
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private final isPreTaxTippingEnabled()Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingTipPreTax()Z

    move-result v0

    return v0
.end method

.method private final isReceiptEnabled()Z
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->canSkipReceiptScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final isSingleCustomAmountLineItem()Z
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string v3, "transaction.order"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Cart;->getItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private final isSplitTender()Z
    .locals 5

    .line 67
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final shouldProcessPaymentUsingPaymentV2()Z
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isPreTaxTippingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isReceiptEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isLoyaltyEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isSingleCustomAmountLineItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isSplitTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->hasCustomer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->hasTax()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->hasDiningOption()Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->isOpenTicket()Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->hasTicketName()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
