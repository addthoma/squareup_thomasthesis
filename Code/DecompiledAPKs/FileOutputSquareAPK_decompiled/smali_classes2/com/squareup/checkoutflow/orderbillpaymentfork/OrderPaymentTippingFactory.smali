.class public final Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;
.super Ljava/lang/Object;
.source "OrderPaymentTippingFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderPaymentTippingFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderPaymentTippingFactory.kt\ncom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory\n*L\n1#1,46:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;",
        "",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/payment/Transaction;)V",
        "calculateTipping",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;",
        "paymentProcessingInput",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public final calculateTipping(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;
    .locals 5

    const-string v0, "paymentProcessingInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 20
    new-instance v1, Lcom/squareup/tipping/TippingCalculator;

    invoke-direct {v1, v0}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/settings/server/TipSettings;)V

    .line 21
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingTipPreTax()Z

    move-result v2

    if-nez v2, :cond_1

    .line 25
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 27
    iget-object v2, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    iget-object v3, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 33
    :goto_0
    invoke-virtual {v1, p1, v2}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v2

    .line 34
    invoke-virtual {v1, p1}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 35
    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    .line 37
    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result v0

    const-string v4, "tipOptions"

    .line 38
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "customTipMaxMoney"

    .line 39
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {v1, v3, v0, v2, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;-><init>(Lcom/squareup/protos/common/Money;ZLjava/util/List;Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    goto :goto_1

    .line 23
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "pre tax tipping not yet implemented"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 42
    :cond_2
    sget-object p1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipDisabled;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipDisabled;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    :goto_1
    return-object v1
.end method
