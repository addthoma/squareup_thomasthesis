.class public final Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealBuyerCheckoutTenderOptionFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;-><init>(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000*\u0001\u0000\u0008\n\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001JF\u0010\t\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\n\u001a\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00030\u000cH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;

    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender()Z

    move-result p1

    .line 48
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->access$getBuyerCheckoutV2Workflow$p(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 49
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutSettings;

    invoke-direct {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutSettings;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->startingState(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v3

    .line 50
    new-instance p1, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1$render$1;-><init>(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    .line 47
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    if-eqz p1, :cond_0

    .line 52
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method
