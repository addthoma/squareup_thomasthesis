.class public final Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;
.super Ljava/lang/Object;
.source "PayOtherLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/checkoutflow/payother/PayOtherScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\u001d\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002R\u0016\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/checkoutflow/payother/PayOtherScreen;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "disclaimerText",
        "Landroid/widget/TextView;",
        "helperText",
        "paymentNote",
        "Lcom/squareup/widgets/SelectableEditText;",
        "tenderButton",
        "getNoteText",
        "",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final disclaimerText:Landroid/widget/TextView;

.field private final helperText:Landroid/widget/TextView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentNote:Lcom/squareup/widgets/SelectableEditText;

.field private final tenderButton:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 30
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/checkoutflow/payother/impl/R$id;->payment_note:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->paymentNote:Lcom/squareup/widgets/SelectableEditText;

    .line 32
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 34
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/checkoutflow/payother/impl/R$id;->other_payment_type_helper_text:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->helperText:Landroid/widget/TextView;

    .line 35
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/checkoutflow/payother/impl/R$id;->other_tender_disclaimer:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->disclaimerText:Landroid/widget/TextView;

    .line 36
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/checkoutflow/payother/impl/R$id;->pay_other_record_payment_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->tenderButton:Landroid/view/View;

    .line 39
    iget-object p1, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->paymentNote:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$1;

    invoke-direct {p2, p0}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$1;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;)V

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/text/Formatter;)V

    return-void
.end method

.method public static final synthetic access$getNoteText(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;)Ljava/lang/String;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->getNoteText()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPaymentNote$p(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->paymentNote:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method private final getNoteText()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->paymentNote:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method private final updateActionBar(Lcom/squareup/checkoutflow/payother/PayOtherScreen;)V
    .locals 5

    .line 72
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 65
    iget-object v3, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherScreen;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 66
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherScreen;->getTenderTypeDisplayName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "%s %s"

    .line 64
    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 63
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 69
    new-instance v2, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$updateActionBar$1;

    invoke-direct {v2, p1}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 71
    sget v1, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/checkoutflow/payother/PayOtherScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$showRendering$1;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->updateActionBar(Lcom/squareup/checkoutflow/payother/PayOtherScreen;)V

    .line 52
    iget-object p2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->disclaimerText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherScreen;->getDisclaimerTextId()I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    .line 53
    iget-object p2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->helperText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherScreen;->getHelperTextId()I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    .line 55
    iget-object p2, p0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->tenderButton:Landroid/view/View;

    new-instance v0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$showRendering$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$showRendering$2;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;Lcom/squareup/checkoutflow/payother/PayOtherScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/checkoutflow/payother/PayOtherScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner;->showRendering(Lcom/squareup/checkoutflow/payother/PayOtherScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
