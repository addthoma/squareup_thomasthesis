.class public interface abstract Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;
.super Ljava/lang/Object;
.source "PayOtherTenderCompleter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
        "",
        "completeTender",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "type",
        "Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;",
        "note",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract completeTender(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderPaymentResult;
.end method
