.class public final Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TipInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipInputCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipInputCoordinator.kt\ncom/squareup/checkoutflow/core/tip/TipInputCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,195:1\n1103#2,7:196\n*E\n*S KotlinDebug\n*F\n+ 1 TipInputCoordinator.kt\ncom/squareup/checkoutflow/core/tip/TipInputCoordinator\n*L\n102#1,7:196\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001)B;\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0018\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0018\u0010\u001f\u001a\u00020 2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\"H\u0002J \u0010#\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J \u0010$\u001a\u00020\u00182\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020&2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0018\u0010(\u001a\u00020\u00182\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001eH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/core/tip/TipInputScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/money/MoneyLocaleHelper;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "confirmButton",
        "Lcom/squareup/noho/NohoButton;",
        "customTipScrubber",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "inputField",
        "Lcom/squareup/noho/NohoEditText;",
        "percentageHint",
        "Landroid/widget/TextView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "formatTipInput",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "getFormattedAmount",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "update",
        "updatePercentageHint",
        "tipAmount",
        "",
        "originalAmount",
        "updateTitleAndSubtitle",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private confirmButton:Lcom/squareup/noho/NohoButton;

.field private customTipScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

.field private inputField:Lcom/squareup/noho/NohoEditText;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private percentageHint:Landroid/widget/TextView;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/money/MoneyLocaleHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ")V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/money/MoneyLocaleHelper;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/money/MoneyLocaleHelper;)V

    return-void
.end method

.method public static final synthetic access$getInputField$p(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;)Lcom/squareup/noho/NohoEditText;
    .locals 1

    .line 36
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez p0, :cond_0

    const-string v0, "inputField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setInputField$p(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Landroid/view/View;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateTitleAndSubtitle(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->updateTitleAndSubtitle(Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 189
    sget v0, Lcom/squareup/checkoutflow/core/tip/impl/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 190
    sget v0, Lcom/squareup/checkout/R$id;->input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    .line 191
    sget v0, Lcom/squareup/checkout/R$id;->confirm_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    .line 192
    sget v0, Lcom/squareup/checkout/R$id;->percentage_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->percentageHint:Landroid/widget/TextView;

    return-void
.end method

.method private final formatTipInput(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;)V
    .locals 10

    .line 179
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->customTipScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    const-string v1, "inputField"

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez v3, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v3, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v2, v3, v0}, Lcom/squareup/money/MoneyLocaleHelper;->unconfigure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 180
    :cond_1
    new-instance v0, Lcom/squareup/money/MoneyScrubber;

    .line 181
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v6

    .line 182
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string p2, "screen.customTipMaxMoney.amount"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 183
    sget-object v9, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    move-object v4, v0

    .line 180
    invoke-direct/range {v4 .. v9}, Lcom/squareup/money/MoneyScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez p2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p2, Lcom/squareup/text/HasSelectableText;

    check-cast v0, Lcom/squareup/text/SelectableTextScrubber;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/money/MoneyLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->customTipScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    return-void
.end method

.method private final getFormattedAmount(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 0

    .line 171
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "localeOverrideFactory.mo\u2026yFormatter.format(amount)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Landroid/view/View;)V
    .locals 3

    .line 78
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 82
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$2;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p3, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 85
    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->updateTitleAndSubtitle(Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 87
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    const-string v0, "inputField"

    if-nez p3, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {p3, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->formatTipInput(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;)V

    .line 94
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez p3, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const-wide/16 v0, 0x0

    .line 96
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 94
    invoke-direct {p0, p1, v0}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->getFormattedAmount(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 100
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    const-string v0, "confirmButton"

    if-nez p3, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    sget v1, Lcom/squareup/configure/item/R$string;->add:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    .line 196
    new-instance p3, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {p3, p0, p2}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updatePercentageHint(JJLcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 153
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    long-to-double p1, p1

    long-to-double p3, p3

    div-double/2addr p1, p3

    invoke-virtual {v0, p1, p2}, Lcom/squareup/util/Percentage$Companion;->fromRate(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Math;->rint(D)D

    move-result-wide p1

    double-to-int p1, p1

    if-lez p1, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 156
    :goto_0
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->percentageHint:Landroid/widget/TextView;

    const-string p4, "percentageHint"

    if-nez p3, :cond_1

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p3, Landroid/view/View;

    invoke-static {p3, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p2, :cond_3

    .line 163
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->percentageHint:Landroid/widget/TextView;

    if-nez p2, :cond_2

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 159
    :cond_2
    invoke-virtual {p5}, Lcom/squareup/locale/LocaleOverrideFactory;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Lcom/squareup/checkoutflow/core/tip/impl/R$string;->buyer_display_tip_percentage_hint:I

    .line 158
    invoke-static {p3, p4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    const-string p4, "percent"

    .line 161
    invoke-virtual {p3, p4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 163
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private final updateTitleAndSubtitle(Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 12

    .line 115
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    const-string v1, "inputField"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_5

    .line 117
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 118
    iget-object v4, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v6, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez v6, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v6}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v4, v1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-wide/16 v6, 0x0

    if-eqz v1, :cond_3

    .line 119
    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    goto :goto_1

    :cond_3
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 120
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-string v4, "tipAmount"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v8, v10

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    invoke-static {v8, v9, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 121
    new-instance v8, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {p0, p2, v4}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->getFormattedAmount(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v8, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v4, v9, v6

    if-lez v4, :cond_4

    .line 123
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {p0, p2, v3}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->getFormattedAmount(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 124
    new-instance v4, Lcom/squareup/util/ViewString$TextString;

    .line 127
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 128
    sget v7, Lcom/squareup/checkout/R$string;->buyer_amount_tip_action_bar:I

    .line 126
    invoke-static {v6, v7}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    .line 130
    check-cast v3, Ljava/lang/CharSequence;

    const-string v7, "amount"

    invoke-virtual {v6, v7, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v6, "tip"

    .line 131
    invoke-virtual {v3, v6, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 124
    invoke-direct {v4, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 136
    :cond_4
    move-object v4, v3

    check-cast v4, Lcom/squareup/util/ViewString$TextString;

    :goto_2
    move-object v6, v4

    .line 138
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-string v0, "originalAmount"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    move-object v0, p0

    move-wide v1, v3

    move-wide v3, v9

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->updatePercentageHint(JJLcom/squareup/locale/LocaleOverrideFactory;)V

    goto :goto_3

    .line 140
    :cond_5
    new-instance v8, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->getFormattedAmount(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    .line 141
    move-object v6, v3

    check-cast v6, Lcom/squareup/util/ViewString$TextString;

    .line 144
    :goto_3
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v1, "buyerActionBar"

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v8, Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v8}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v6, Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v6}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->bindViews(Landroid/view/View;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "inputField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->post(Ljava/lang/Runnable;)Z

    .line 67
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$attach$2;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
