.class public interface abstract Lcom/squareup/checkoutflow/core/services/CheckoutFeService;
.super Ljava/lang/Object;
.source "CheckoutFeService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/services/CheckoutFeService$CheckoutStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0006J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
        "",
        "checkout",
        "Lcom/squareup/checkoutflow/core/services/CheckoutFeService$CheckoutStandardResponse;",
        "request",
        "Lcom/squareup/checkoutfe/CheckoutRequest;",
        "CheckoutStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract checkout(Lcom/squareup/checkoutfe/CheckoutRequest;)Lcom/squareup/checkoutflow/core/services/CheckoutFeService$CheckoutStandardResponse;
    .param p1    # Lcom/squareup/checkoutfe/CheckoutRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/checkoutfe/checkout"
    .end annotation
.end method
