.class public abstract Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowModule;
.super Ljava/lang/Object;
.source "CardProcessingWorkflowModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowModule;",
        "",
        "()V",
        "bindCardProcessingWorkflowViewFactory",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;",
        "viewFactory",
        "Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflowViewFactory;",
        "bindCardProcessingWorkflowWorkflow",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
        "workflow",
        "Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindCardProcessingWorkflowViewFactory(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflowViewFactory;)Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCardProcessingWorkflowWorkflow(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;)Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
