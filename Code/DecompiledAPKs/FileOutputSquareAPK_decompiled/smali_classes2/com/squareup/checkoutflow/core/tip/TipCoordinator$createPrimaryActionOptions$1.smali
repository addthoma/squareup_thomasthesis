.class final Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TipCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->createPrimaryActionOptions(Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $i:I

.field final synthetic $onEvent:Lkotlin/jvm/functions/Function1;

.field final synthetic $tipOptions:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/checkoutflow/core/tip/TipCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lkotlin/jvm/functions/Function1;Ljava/util/List;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->this$0:Lcom/squareup/checkoutflow/core/tip/TipCoordinator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$onEvent:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$tipOptions:Ljava/util/List;

    iput p4, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$i:I

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    .line 134
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->this$0:Lcom/squareup/checkoutflow/core/tip/TipCoordinator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->access$getBuyerActionContainer$p(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;)Lcom/squareup/ui/buyer/BuyerActionContainer;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$onEvent:Lkotlin/jvm/functions/Function1;

    .line 136
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;

    .line 137
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$tipOptions:Ljava/util/List;

    iget v3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$i:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v2, v2, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    const-string v3, "tipOptions[i].tip_money"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    sget-object v3, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v4, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$tipOptions:Ljava/util/List;

    iget v5, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;->$i:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v4, v4, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Lcom/squareup/util/Percentage$Companion;->fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;

    move-result-object v3

    .line 136
    invoke-direct {v1, v2, v3}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
