.class public final Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;
.super Ljava/lang/Object;
.source "RealAdditionalTenderOptions_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;",
            ">;)",
            "Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;)Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;-><init>(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;

    invoke-static {v0}, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;->newInstance(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;)Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions_Factory;->get()Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;

    move-result-object v0

    return-object v0
.end method
