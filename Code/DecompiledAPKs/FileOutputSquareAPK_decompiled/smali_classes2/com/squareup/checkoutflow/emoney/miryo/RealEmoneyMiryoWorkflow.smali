.class public final Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEmoneyMiryoWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyMiryoWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyMiryoWorkflow.kt\ncom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,188:1\n149#2,5:189\n149#2,5:194\n41#3:199\n56#3,2:200\n276#4:202\n*E\n*S KotlinDebug\n*F\n+ 1 RealEmoneyMiryoWorkflow.kt\ncom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow\n*L\n166#1,5:189\n184#1,5:194\n55#1:199\n55#1,2:200\n55#1:202\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJd\u0010\u0013\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00152\u001c\u0010\u0016\u001a\u0018\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\u0008j\u0008\u0012\u0004\u0012\u00020\u0017`\u00192\u0006\u0010\u001a\u001a\u00020\u0004H\u0002J8\u0010\u001b\u001a\u0018\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\u0008j\u0008\u0012\u0004\u0012\u00020\u0017`\u00192\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u001f\u001a\u00020 H\u0002J\u001a\u0010!\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u00032\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J \u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u0004H\u0016JN\u0010\'\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00042\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0015H\u0016J\u0010\u0010(\u001a\u00020#2\u0006\u0010\u001c\u001a\u00020\u0004H\u0016R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "tmnObservablesHelper",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "emoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "(Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V",
        "displayRequestWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "createDialogStack",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "body",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "cancelState",
        "getScreen",
        "state",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;",
        "input",
        "showingBalanceCheck",
        "",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayRequestWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tmnObservablesHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyIconFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 54
    invoke-interface {p1}, Lcom/squareup/tmn/TmnObservablesHelper;->getDisplayRequestObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 199
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 201
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 202
    const-class p2, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    .line 199
    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 201
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
            ">;",
            "Lcom/squareup/workflow/legacy/Screen;",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 176
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 177
    new-instance v1, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen;

    .line 178
    sget-object v2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$createDialogStack$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$createDialogStack$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 181
    new-instance v3, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$createDialogStack$2;

    invoke-direct {v3, p3}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$createDialogStack$2;-><init>(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 177
    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 195
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 196
    const-class p1, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    const-string p3, ""

    invoke-static {p1, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 197
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 195
    invoke-direct {v4, p1, v1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p2

    .line 176
    invoke-static/range {v0 .. v6}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final getScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 4

    .line 162
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    .line 164
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;-><init>(Lcom/squareup/protos/common/Money;Z)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    .line 165
    new-instance p3, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v2, "input.money.currency_code"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p3, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    .line 162
    invoke-direct {v0, p1, v1, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 190
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 191
    const-class p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 192
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 190
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method static synthetic getScreen$default(Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 160
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->getScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;
    .locals 2

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getShouldShowUnresolved()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    new-instance p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    const/4 p2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p1, p2, v1, v1, v0}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;-><init>(ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    goto :goto_0

    .line 63
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingResult;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingResult;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->initialState(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;
    .locals 2

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    instance-of v0, p3, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    if-eqz v0, :cond_0

    return-object p3

    .line 78
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getHasValidReaderConnected()Z

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getHasValidReaderConnected()Z

    move-result v1

    if-eq v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getHasValidReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getShouldShowUnresolved()Z

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getShouldShowUnresolved()Z

    move-result v0

    if-eq p1, v0, :cond_3

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getShouldShowUnresolved()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 80
    :cond_2
    new-instance p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    const/4 p2, 0x3

    const/4 p3, 0x0

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0, p2, p3}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;-><init>(ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    return-object p1

    :cond_3
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    check-cast p3, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->onPropsChanged(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->render(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 93
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getHasValidReaderConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;

    .line 95
    new-instance v2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$1;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$1;-><init>(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 94
    invoke-direct {v0, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    goto :goto_0

    .line 99
    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;->isFirstMiryo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasNotAttemptedMiryo;

    .line 101
    sget-object v2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$2;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 100
    invoke-direct {v0, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasNotAttemptedMiryo;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    goto :goto_0

    .line 106
    :cond_1
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;

    .line 107
    sget-object v2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$3;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 110
    sget-object v3, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$4;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$miryoInteractions$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 106
    invoke-direct {v0, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    .line 117
    :goto_0
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->getPreviousBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;)V

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 116
    invoke-direct {p0, v2, p1, v1}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->getScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 122
    check-cast p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;->getShowDialog()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;->isFirstMiryo()Z

    move-result p2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;-><init>(ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    invoke-direct {p0, p3, p1, v0}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 125
    :cond_2
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 128
    :cond_3
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;

    if-eqz v0, :cond_5

    .line 129
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    .line 130
    sget-object v2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$screenState$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$screenData$screenState$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 131
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 129
    invoke-direct {v0, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    .line 133
    move-object v5, v0

    check-cast v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    move-object v6, p1

    invoke-static/range {v4 .. v9}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->getScreen$default(Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 134
    check-cast p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;->getShowDialog()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 135
    new-instance p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;

    invoke-direct {p2, v1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;-><init>(Z)V

    check-cast p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 137
    :cond_4
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 140
    :cond_5
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingResult;

    if-eqz v0, :cond_6

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;

    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->getScreen$default(Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 141
    :cond_6
    instance-of p2, p2, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingBalance;

    if-eqz p2, :cond_7

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;

    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->getScreen$default(Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    .line 144
    :goto_1
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    const/4 v2, 0x0

    sget-object p2, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;

    move-object v3, p2

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-object p1

    .line 141
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->snapshotState(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
