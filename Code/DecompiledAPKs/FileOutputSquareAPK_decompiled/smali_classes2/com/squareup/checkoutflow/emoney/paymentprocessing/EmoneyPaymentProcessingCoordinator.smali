.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EmoneyPaymentProcessingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyPaymentProcessingCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyPaymentProcessingCoordinator.kt\ncom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator\n*L\n1#1,432:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001;BI\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0012\u0010#\u001a\u0004\u0018\u00010\n2\u0006\u0010$\u001a\u00020%H\u0002J\u0014\u0010&\u001a\u0004\u0018\u00010\'2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0002J*\u0010*\u001a\u00020\u001e2\u0006\u0010+\u001a\u00020,2\u0006\u0010(\u001a\u00020)2\u0008\u0008\u0001\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0002J\u001c\u00101\u001a\u00020\u001e2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001e03H\u0002J$\u00104\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001e03H\u0002J\u0010\u00105\u001a\u00020\u001e2\u0006\u00106\u001a\u00020\u0005H\u0002J\u0018\u00107\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u00106\u001a\u00020\u0005H\u0002J\u0014\u00108\u001a\u00020\u001e*\u00020\u00172\u0006\u00109\u001a\u00020:H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006<"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "emoneyDisplayRequestTextHelper",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;)V",
        "actionButton",
        "Lcom/squareup/noho/NohoButton;",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "messageView",
        "Lcom/squareup/widgets/MessageView;",
        "spinnerContainer",
        "Landroid/view/ViewGroup;",
        "spinnerGlyph",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "subtitleView",
        "Landroid/widget/TextView;",
        "titleView",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "disconnectBackHandling",
        "getBalanceFromScreenState",
        "screenState",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;",
        "getMessageFromError",
        "",
        "errorMessage",
        "Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;",
        "setUpErrorView",
        "transactionType",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;",
        "actionButtonTextId",
        "",
        "buttonAppearance",
        "Lcom/squareup/noho/NohoButtonType;",
        "setupActionButton",
        "callback",
        "Lkotlin/Function1;",
        "setupBackHandling",
        "updateDisplay",
        "data",
        "updateHandlers",
        "addCardReaderImage",
        "emoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionButton:Lcom/squareup/noho/NohoButton;

.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerContainer:Landroid/view/ViewGroup;

.field private spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private subtitleView:Landroid/widget/TextView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 71
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

    iput-object p4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$updateDisplay(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->updateDisplay(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V

    return-void
.end method

.method public static final synthetic access$updateHandlers(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->updateHandlers(Landroid/view/View;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V

    return-void
.end method

.method private final addCardReaderImage(Landroid/view/ViewGroup;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V
    .locals 3

    .line 334
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;->create(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 421
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 422
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026heckout.R.id.glyph_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    .line 423
    sget v0, Lcom/squareup/checkout/R$id;->glyph_subtitle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026kout.R.id.glyph_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->subtitleView:Landroid/widget/TextView;

    .line 424
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026ckout.R.id.glyph_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    .line 425
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026out.R.id.glyph_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    .line 427
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$layout;->auth_spinner_glyph:I

    const/4 v2, 0x0

    .line 426
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 429
    sget v0, Lcom/squareup/checkout/R$id;->payment_processing_primary_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(com.sq\u2026rocessing_primary_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    return-void

    .line 426
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.marin.widgets.MarinSpinnerGlyph"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final disconnectBackHandling(Landroid/view/View;)V
    .locals 1

    .line 414
    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$disconnectBackHandling$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$disconnectBackHandling$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 417
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p1, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideUpGlyph()V

    return-void
.end method

.method private final getBalanceFromScreenState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 339
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 340
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 341
    :cond_1
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getMessageFromError(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 379
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

    .line 380
    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;->getTmnUIMessage()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object p1

    .line 379
    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->getErrorMessageForTmnMessage(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 382
    :cond_1
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;->getGenericMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 383
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericMessage;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_something_went_wrong:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 384
    :cond_3
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericNetworkErrorMessage;

    if-eqz v0, :cond_4

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    .line 385
    sget v0, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 384
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 387
    :cond_4
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_failure_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "balance"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 389
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 390
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final setUpErrorView(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;ILcom/squareup/noho/NohoButtonType;)V
    .locals 3

    .line 351
    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$CheckBalance;

    if-eqz p1, :cond_0

    .line 352
    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_verify_balance_failed_title:I

    goto :goto_0

    .line 353
    :cond_0
    instance-of p1, p2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;

    if-eqz p1, :cond_1

    move-object p1, p2

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;->getFromMiryo()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 354
    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_error_miryo_title:I

    goto :goto_0

    .line 356
    :cond_1
    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_error_title:I

    .line 360
    :goto_0
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

    .line 361
    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;->getTmnUIMessage()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v1

    .line 360
    invoke-virtual {v0, v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->getErrorTitleForTmnMessage(Lcom/squareup/cardreader/lcr/CrsTmnMessage;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 365
    :goto_1
    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->getMessageFromError(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)Ljava/lang/String;

    move-result-object p2

    .line 367
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    const-string v1, "spinnerGlyph"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure()V

    .line 368
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_4

    const-string v2, "spinnerContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v2, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 369
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v0, :cond_6

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_7

    const-string v0, "messageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    const-string p2, "actionButton"

    if-nez p1, :cond_8

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 372
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_9

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_a

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    return-void
.end method

.method private final setupActionButton(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 395
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "actionButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupActionButton$1;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupActionButton$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 406
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupBackHandling$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupBackHandling$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 409
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v0, "buyerActionBar"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupBackHandling$2;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$setupBackHandling$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 410
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->showUpGlyph()V

    return-void
.end method

.method private final updateDisplay(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V
    .locals 12

    .line 161
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getState()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    move-result-object v0

    .line 163
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object v1

    .line 164
    instance-of v2, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Payment;

    const-string v3, "moneyFormatter.format(data.transactionType.money)"

    const-string v4, "balance"

    const-string v5, "brand"

    const-string v6, "buyerActionBar"

    if-eqz v2, :cond_3

    .line 165
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->getBalanceFromScreenState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 168
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    .line 169
    iget-object v7, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_title_with_balance:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-static {v7}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v7

    .line 170
    iget-object v8, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v9, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result v9

    invoke-interface {v8, v9}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v7, v5, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 171
    iget-object v7, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v7, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v5, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 168
    invoke-direct {v2, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 176
    :cond_0
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    .line 177
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_title:I

    invoke-interface {v1, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 178
    iget-object v7, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v8, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result v8

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 179
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 180
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 176
    invoke-direct {v2, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    .line 184
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_1

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 185
    :cond_1
    new-instance v5, Lcom/squareup/util/ViewString$TextString;

    iget-object v7, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object v8

    check-cast v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Payment;

    invoke-virtual {v8}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Payment;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v5, Lcom/squareup/util/ViewString;

    .line 184
    invoke-virtual {v1, v5}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 187
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_2

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Lcom/squareup/util/ViewString;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    goto/16 :goto_2

    .line 189
    :cond_3
    instance-of v2, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;

    if-eqz v2, :cond_7

    .line 190
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;->getShowingBalanceCheck()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 191
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_title_with_balance_check:I

    goto :goto_1

    .line 193
    :cond_4
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_title:I

    .line 196
    :goto_1
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v2, :cond_5

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 197
    :cond_5
    new-instance v7, Lcom/squareup/util/ViewString$TextString;

    iget-object v8, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object v9

    check-cast v9, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;

    invoke-virtual {v9}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Miryo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v7, Lcom/squareup/util/ViewString;

    .line 196
    invoke-virtual {v2, v7}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 200
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v2, :cond_6

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 201
    :cond_6
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    .line 202
    iget-object v6, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v6, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 203
    iget-object v6, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v7, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 204
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 205
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 201
    invoke-direct {v3, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 200
    invoke-virtual {v2, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    goto :goto_2

    .line 209
    :cond_7
    instance-of v1, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$CheckBalance;

    if-eqz v1, :cond_a

    .line 210
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_8

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 211
    :cond_8
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_check_balance:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 210
    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 213
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_9

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 214
    :cond_9
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v5, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result v5

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 213
    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    .line 219
    :cond_a
    :goto_2
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    const-string v2, "spinnerContainer"

    if-nez v1, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 220
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    const-string v3, "actionButton"

    if-nez v1, :cond_c

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 221
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->subtitleView:Landroid/widget/TextView;

    const-string v6, "subtitleView"

    if-nez v1, :cond_d

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    const-string v5, "messageView"

    if-nez v1, :cond_e

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 225
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;

    const/4 v8, 0x0

    const-string v9, "titleView"

    const-string v10, "spinnerGlyph"

    if-eqz v1, :cond_14

    .line 226
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_f

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->reset()V

    .line 227
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_10

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v0, :cond_11

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 229
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez p1, :cond_12

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_reader_not_ready_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_13

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {p1, v8}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 232
    :cond_14
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;

    if-eqz v1, :cond_1a

    .line 233
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_15

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure()V

    .line 234
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_16

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v0, :cond_17

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez p1, :cond_18

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_reader_disconnected_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_19

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_19
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_reader_disconnected_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 239
    :cond_1a
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    const-string v11, "card"

    if-eqz v1, :cond_1e

    .line 240
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_1b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1b
    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->getEmoneyIconFactory()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->addCardReaderImage(Landroid/view/ViewGroup;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v0, :cond_1c

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 242
    :cond_1c
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tap_card_prompt:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 243
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v3, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result p1

    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v11, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_1d

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1d
    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {p1, v8}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 249
    :cond_1e
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;

    if-eqz v1, :cond_22

    .line 250
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_1f

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1f
    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;->getEmoneyIconFactory()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->addCardReaderImage(Landroid/view/ViewGroup;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    .line 255
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v1, :cond_20

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 252
    :cond_20
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tap_again:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 253
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v4, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result p1

    invoke-interface {v3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v11, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 255
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_21

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_21
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->getMessageFromError(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 259
    :cond_22
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    if-eqz v1, :cond_32

    .line 260
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_23

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_23
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->reset()V

    .line 261
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_24

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_24
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v1, :cond_25

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_25
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 263
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez p1, :cond_26

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 264
    :cond_26
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;

    if-eqz v1, :cond_27

    goto :goto_3

    :cond_27
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;

    if-eqz v1, :cond_28

    :goto_3
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_processing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_4

    .line 265
    :cond_28
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Online;

    if-eqz v1, :cond_29

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_online_processing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_4

    .line 266
    :cond_29
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Authorizing;

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_authorizing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_4

    .line 267
    :cond_2a
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;

    if-eqz v1, :cond_2b

    move-object v1, v8

    goto :goto_4

    .line 268
    :cond_2b
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Cancelling;

    if-eqz v1, :cond_31

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_canceling:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 263
    :goto_4
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_2c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 272
    :cond_2c
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;

    if-eqz v1, :cond_2d

    goto :goto_5

    :cond_2d
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Authorizing;

    if-eqz v1, :cond_2e

    goto :goto_5

    :cond_2e
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;

    if-eqz v1, :cond_2f

    goto :goto_5

    :cond_2f
    instance-of v0, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Cancelling;

    if-eqz v0, :cond_30

    goto :goto_5

    .line 273
    :cond_30
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_processing_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    .line 271
    :goto_5
    invoke-virtual {p1, v8}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 268
    :cond_31
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 276
    :cond_32
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    if-eqz v1, :cond_3c

    .line 277
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_33

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_33
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    .line 278
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_34

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_34
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v1, :cond_35

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_35
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 280
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez p1, :cond_36

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_36
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_complete:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_37

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_37
    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;->getFromMiryo()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 282
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_complete_miryo_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    .line 281
    :cond_38
    invoke-virtual {p1, v8}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_39

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_39
    invoke-virtual {p1, v7}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 288
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_3a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3a
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_3b

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3b
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    goto/16 :goto_7

    .line 291
    :cond_3c
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;

    if-eqz v1, :cond_42

    .line 292
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_3d

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3d
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    .line 293
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_3e

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3e
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v1, :cond_3f

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3f
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez p1, :cond_40

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 295
    :cond_40
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_current_balance_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 296
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_41

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_41
    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {p1, v8}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 301
    :cond_42
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    if-eqz v1, :cond_43

    .line 302
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object p1

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v0

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_error_retry:I

    .line 303
    sget-object v2, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    .line 301
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setUpErrorView(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;ILcom/squareup/noho/NohoButtonType;)V

    goto/16 :goto_7

    .line 305
    :cond_43
    instance-of v1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    if-eqz v1, :cond_44

    .line 306
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getTransactionType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    move-result-object p1

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v0

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_payment_error_cancel:I

    .line 307
    sget-object v2, Lcom/squareup/noho/NohoButtonType;->DESTRUCTIVE:Lcom/squareup/noho/NohoButtonType;

    .line 305
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setUpErrorView(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;ILcom/squareup/noho/NohoButtonType;)V

    goto/16 :goto_7

    .line 309
    :cond_44
    instance-of p1, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;

    if-eqz p1, :cond_50

    .line 310
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->subtitleView:Landroid/widget/TextView;

    if-nez p1, :cond_45

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_45
    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;->getMiryoInteractions()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;

    if-eqz p1, :cond_46

    .line 312
    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_error_no_reader:I

    goto :goto_6

    .line 314
    :cond_46
    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_error:I

    .line 316
    :goto_6
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v1, :cond_47

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_47
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure()V

    .line 317
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_48

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_48
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez v2, :cond_49

    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_49
    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 318
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v1, :cond_4a

    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4a
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_unknown_error:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->subtitleView:Landroid/widget/TextView;

    if-nez v1, :cond_4b

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4b
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_error_subtitle:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_4c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 320
    :cond_4c
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 321
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;->getPreviousBalance()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v4, "previous_balance"

    invoke-virtual {p1, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 322
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 323
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_4d

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4d
    check-cast p1, Landroid/view/View;

    .line 325
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;->getMiryoInteractions()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;

    xor-int/lit8 v0, v0, 0x1

    .line 324
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrInvisible(Landroid/view/View;Z)V

    .line 327
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_4e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4e
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_verify_balance:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 328
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_4f

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4f
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    :cond_50
    :goto_7
    return-void
.end method

.method private final updateHandlers(Landroid/view/View;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;)V
    .locals 1

    .line 119
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->getState()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    move-result-object p2

    .line 120
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_1

    .line 121
    :cond_0
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_1

    .line 122
    :cond_1
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_1

    .line 123
    :cond_2
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;

    if-eqz v0, :cond_3

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_1

    .line 124
    :cond_3
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    if-eqz v0, :cond_5

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->disconnectBackHandling(Landroid/view/View;)V

    goto/16 :goto_1

    .line 125
    :cond_5
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    if-eqz v0, :cond_6

    .line 126
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;->getOnContinue()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupActionButton(Lkotlin/jvm/functions/Function1;)V

    .line 127
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->disconnectBackHandling(Landroid/view/View;)V

    goto :goto_1

    .line 129
    :cond_6
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    if-eqz v0, :cond_7

    .line 130
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;->getOnRetry()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupActionButton(Lkotlin/jvm/functions/Function1;)V

    .line 131
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 133
    :cond_7
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    if-eqz v0, :cond_8

    .line 134
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;->getOnCancel()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupActionButton(Lkotlin/jvm/functions/Function1;)V

    .line 135
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;->getOnCancel()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 137
    :cond_8
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;

    if-eqz v0, :cond_b

    .line 138
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError;->getMiryoInteractions()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions;

    move-result-object p2

    .line 139
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasNotAttemptedMiryo;

    if-eqz v0, :cond_9

    .line 140
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasNotAttemptedMiryo;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasNotAttemptedMiryo;->getOnRetry()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupActionButton(Lkotlin/jvm/functions/Function1;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->disconnectBackHandling(Landroid/view/View;)V

    goto :goto_1

    .line 143
    :cond_9
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;

    if-eqz v0, :cond_a

    .line 144
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$NoValidReaderConnected;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 146
    :cond_a
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;

    if-eqz v0, :cond_b

    .line 147
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;->getOnRetry()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupActionButton(Lkotlin/jvm/functions/Function1;)V

    .line 148
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$MiryoError$MiryoInteractions$HasAttemptedMiryo;->getOnBackPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->setupBackHandling(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    :cond_b
    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->bindViews(Landroid/view/View;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$2;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$2;

    check-cast v1, Lio/reactivex/functions/BiPredicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$attach$3;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.distinctUntilCha\u2026play(it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
