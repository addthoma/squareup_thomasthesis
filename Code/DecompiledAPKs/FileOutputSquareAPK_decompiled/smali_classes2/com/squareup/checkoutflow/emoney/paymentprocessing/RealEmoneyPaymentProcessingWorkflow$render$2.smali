.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->render(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "it",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

.field final synthetic $state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$NoMoneyMoved;

    const-string v1, "input.money.currency_code"

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getDanglingMiryo$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    .line 337
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$NoMoneyMoved;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$NoMoneyMoved;->getTmnEvent()Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 338
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 339
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    .line 340
    sget-object v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    move-object v4, v1

    check-cast v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;

    const/4 v2, 0x1

    invoke-direct {v1, v6, v2}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$CurrentBalanceMessage;-><init>(Lcom/squareup/protos/common/Money;Z)V

    move-object v5, v1

    check-cast v5, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v0

    .line 339
    invoke-direct/range {v3 .. v9}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string v1, "Miryo No money was moved"

    .line 338
    invoke-static {p1, v0, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_1

    .line 345
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$MoneyMoved;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$MoneyMoved;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$MoneyMoved;->getTmnEvent()Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 350
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 351
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v3

    sget-object v4, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v4, Lcom/squareup/tmn/Action;

    invoke-direct {v1, p1, v3, v2, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string p1, "Miryo money was moved"

    .line 350
    invoke-static {v0, v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 354
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$Cancel;

    if-eqz v0, :cond_3

    .line 355
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getDanglingMiryo$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    .line 356
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult$EmoneyAbort;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult$EmoneyAbort;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 358
    :cond_3
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$StartMiryoOnReader;

    if-eqz v0, :cond_4

    .line 359
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 360
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v3

    sget-object v4, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    check-cast v4, Lcom/squareup/tmn/Action;

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string v1, "Miryo Start Payment"

    .line 359
    invoke-static {p1, v0, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 364
    :cond_4
    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$Restart;

    if-eqz p1, :cond_5

    .line 365
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 366
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v3

    sget-object v4, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v4, Lcom/squareup/tmn/Action;

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string v1, "Miryo Restart"

    .line 365
    invoke-static {p1, v0, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;->invoke(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
