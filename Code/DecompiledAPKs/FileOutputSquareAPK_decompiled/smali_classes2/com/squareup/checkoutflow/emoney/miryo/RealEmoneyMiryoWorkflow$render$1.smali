.class final Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyMiryoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;->render(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
        "it",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState;",
            "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v0

    sget-object v1, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eq v0, v1, :cond_3

    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v3, 0x4

    if-eq v0, v3, :cond_1

    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    .line 150
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;

    invoke-direct {v0, v2, v2, v1, v4}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$Unresolved;-><init>(ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$Restart;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$Restart;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 149
    :cond_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$NoMoneyMoved;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$NoMoneyMoved;-><init>(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 148
    :cond_1
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$MoneyMoved;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoOutput$MoneyMoved;-><init>(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 147
    :cond_2
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$CheckingBalance;

    invoke-static {p1, v0, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 146
    :cond_3
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;

    invoke-direct {v0, v2}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoState$WaitingForTap;-><init>(Z)V

    invoke-static {p1, v0, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow$render$1;->invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
