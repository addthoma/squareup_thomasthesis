.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;
.super Ljava/lang/Object;
.source "EmoneyPaymentProcessingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ*\u0010\n\u001a\u00020\u000b2\"\u0010\u000c\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00020\u000f`\u00110\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "emoneyDisplayRequestTextHelper",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;)V",
        "create",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyDisplayRequestTextHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;

    .line 80
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->emoneyDisplayRequestTextHelper:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;

    iget-object v5, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;->res:Lcom/squareup/util/Res;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 79
    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
