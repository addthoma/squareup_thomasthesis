.class final Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;
.super Ljava/lang/Object;
.source "MiryoCancelDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;->this$0:Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, -0x1

    .line 33
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPrimaryButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 34
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_cancel_dialog_discard:I

    new-instance v2, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$1;-><init>(Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 45
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_cancel_dialog_keep:I

    new-instance v2, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 48
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_cancel_dialog_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 49
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_miryo_cancel_dialog_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$3;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1$3;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
