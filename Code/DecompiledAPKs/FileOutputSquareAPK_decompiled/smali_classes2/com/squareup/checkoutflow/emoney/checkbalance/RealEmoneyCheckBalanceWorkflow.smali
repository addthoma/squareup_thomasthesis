.class public final Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEmoneyCheckBalanceWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyCheckBalanceWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyCheckBalanceWorkflow.kt\ncom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,194:1\n149#2,5:195\n41#3:200\n56#3,2:201\n276#4:203\n*E\n*S KotlinDebug\n*F\n+ 1 RealEmoneyCheckBalanceWorkflow.kt\ncom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow\n*L\n127#1,5:195\n67#1:200\n67#1,2:201\n67#1:203\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012,\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00080\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ$\u0010\u0013\u001a\u00020\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00162\u0006\u0010\u0017\u001a\u00020\u0003H\u0002J\u001a\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J>\u0010\u001c\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00082\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u00042\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0016H\u0016J,\u0010\u001e\u001a\u00020\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00162\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u0004H\u0016R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "tmnWorkflow",
        "Lcom/squareup/tmn/TmnStarterWorkflow;",
        "tmnObservablesHelper",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "emoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "(Lcom/squareup/tmn/TmnStarterWorkflow;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V",
        "displayRequestWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "handleDisplayRequest",
        "",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "input",
        "initialState",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "renderTmnWorkflow",
        "action",
        "Lcom/squareup/tmn/Action;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayRequestWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

.field private final tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnStarterWorkflow;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tmnWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tmnObservablesHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyIconFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 66
    invoke-interface {p2}, Lcom/squareup/tmn/TmnObservablesHelper;->getDisplayRequestObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 200
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 202
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 203
    const-class p2, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 200
    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 202
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final handleDisplayRequest(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;",
            ")V"
        }
    .end annotation

    .line 139
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;-><init>(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/tmn/Action;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;",
            "Lcom/squareup/tmn/Action;",
            ")V"
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 179
    new-instance v0, Lcom/squareup/tmn/TmnInput;

    sget-object v4, Lcom/squareup/tmn/TmnTransactionType;->CheckBalance:Lcom/squareup/tmn/TmnTransactionType;

    sget-object v1, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getTmnBrandId(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v5

    const-wide/16 v6, 0x0

    move-object v3, v0

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/squareup/tmn/TmnInput;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V

    .line 180
    sget-object p2, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 177
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;
    .locals 0

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget-object p1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->initialState(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v0, Lcom/squareup/tmn/Action;

    .line 82
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    .line 84
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;

    if-eqz v2, :cond_0

    .line 85
    sget-object p2, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    move-object v0, p2

    check-cast v0, Lcom/squareup/tmn/Action;

    .line 86
    new-instance p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;

    .line 87
    sget-object v2, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 86
    invoke-direct {p2, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto/16 :goto_0

    .line 92
    :cond_0
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderDisconnected;

    if-eqz v2, :cond_1

    new-instance p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;

    .line 93
    sget-object v2, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$2;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 92
    invoke-direct {p2, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto/16 :goto_0

    .line 97
    :cond_1
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForTap;

    if-eqz v2, :cond_2

    new-instance p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    .line 98
    sget-object v2, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$3;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 101
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 97
    invoke-direct {p2, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 103
    :cond_2
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;

    if-eqz v2, :cond_3

    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;

    .line 104
    check-cast p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object p2

    .line 105
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 103
    invoke-direct {v2, p2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    move-object p2, v2

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 107
    :cond_3
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance$NoAuthOrReaderResponse;

    if-eqz v2, :cond_4

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$CheckingBalance;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 108
    :cond_4
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance$ReceivedRetryableErrorMessage;

    if-eqz v2, :cond_5

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 109
    :cond_5
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;

    if-eqz v2, :cond_6

    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 110
    sget-object v3, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$4;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 109
    invoke-direct {v2, p2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$BalanceComplete;-><init>(Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)V

    move-object p2, v2

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 114
    :cond_6
    instance-of v2, p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;

    if-eqz v2, :cond_7

    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    const/4 v3, 0x0

    .line 116
    check-cast p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object p2

    .line 117
    sget-object v4, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$5;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$5;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 120
    sget-object v5, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$6;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$render$screenData$6;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    .line 114
    invoke-direct {v2, v3, p2, v4, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object p2, v2

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 125
    :goto_0
    sget-object v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$CheckBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$CheckBalance;

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    .line 126
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v3

    .line 82
    invoke-direct {v1, p2, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 196
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 197
    const-class v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 198
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 196
    invoke-direct {p2, v2, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 129
    invoke-direct {p0, p3, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->handleDisplayRequest(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;)V

    .line 130
    invoke-direct {p0, p3, p1, v0}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/tmn/Action;)V

    return-object p2

    .line 114
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->render(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->snapshotState(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
