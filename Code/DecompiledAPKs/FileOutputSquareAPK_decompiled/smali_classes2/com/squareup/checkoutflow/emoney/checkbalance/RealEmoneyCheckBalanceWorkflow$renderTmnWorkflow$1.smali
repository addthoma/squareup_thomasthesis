.class final Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyCheckBalanceWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;Lcom/squareup/tmn/Action;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tmn/TmnOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyCheckBalanceWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyCheckBalanceWorkflow.kt\ncom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        "it",
        "Lcom/squareup/tmn/TmnOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/TmnOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$Failed$NetworkError;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;

    sget-object v3, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericNetworkErrorMessage;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericNetworkErrorMessage;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 183
    :cond_0
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$Failed$ActiveCardReaderDisconnected;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderDisconnected;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderDisconnected;

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 184
    :cond_1
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$Completed;

    if-eqz v0, :cond_2

    .line 186
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 188
    :cond_2
    instance-of p1, p1, Lcom/squareup/tmn/TmnOutput$CardReaderConnectionChanged;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 189
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unsupported result from peripheral layer"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/tmn/TmnOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$renderTmnWorkflow$1;->invoke(Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
