.class final Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->render(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        "output",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult$SelectBrand;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$SelectBrand;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult$SelectBrand;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult$SelectBrand;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$SelectBrand;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 54
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult$CheckBalance;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_1
    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult$Cancel;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$Cancel;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$Cancel;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;->invoke(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
