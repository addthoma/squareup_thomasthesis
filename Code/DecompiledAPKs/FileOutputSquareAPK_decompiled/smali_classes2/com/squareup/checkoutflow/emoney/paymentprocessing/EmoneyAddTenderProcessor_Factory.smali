.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;
.super Ljava/lang/Object;
.source "EmoneyAddTenderProcessor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;)",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/TenderInEdit;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor_Factory;->get()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    move-result-object v0

    return-object v0
.end method
