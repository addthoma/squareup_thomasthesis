.class public final Lcom/squareup/checkoutflow/installments/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final installments_enter_card_option:I = 0x7f0a0835

.field public static final installments_helper_text:I = 0x7f0a0836

.field public static final installments_link_option:I = 0x7f0a0837

.field public static final installments_link_options_subtitle:I = 0x7f0a0838

.field public static final installments_link_options_title:I = 0x7f0a0839

.field public static final installments_qr_container:I = 0x7f0a083a

.field public static final installments_qr_error_glyph:I = 0x7f0a083b

.field public static final installments_qr_hint:I = 0x7f0a083c

.field public static final installments_qr_image:I = 0x7f0a083d

.field public static final installments_qr_spinner:I = 0x7f0a083e

.field public static final installments_qr_title:I = 0x7f0a083f

.field public static final installments_scan_code_button:I = 0x7f0a0840

.field public static final installments_send_button:I = 0x7f0a0841

.field public static final installments_sms_hint:I = 0x7f0a0842

.field public static final installments_sms_input:I = 0x7f0a0843

.field public static final installments_sms_sent_hint:I = 0x7f0a0844

.field public static final installments_sms_sent_title:I = 0x7f0a0845

.field public static final installments_sms_title:I = 0x7f0a0846

.field public static final installments_text_link:I = 0x7f0a0847

.field public static final installments_text_me_button:I = 0x7f0a0848


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
