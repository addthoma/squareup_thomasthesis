.class final Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;
.super Lkotlin/jvm/internal/Lambda;
.source "RealInstallmentsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->render(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "+",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_LINK_INSTEAD:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 210
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
