.class public final Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstallmentsOptionsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\u001cB9\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0018\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0010\u001a\u00020\u0005H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "data",
        "enterCardOption",
        "Lcom/squareup/ui/account/view/LineRow;",
        "helperText",
        "Lcom/squareup/widgets/MessageView;",
        "linkOption",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private enterCardOption:Lcom/squareup/ui/account/view/LineRow;

.field private helperText:Lcom/squareup/widgets/MessageView;

.field private linkOption:Lcom/squareup/ui/account/view/LineRow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->res:Lcom/squareup/util/Res;

    .line 41
    sget-object p2, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 107
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 108
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_link_option:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_link_option)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    .line 109
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_enter_card_option:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026lments_enter_card_option)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->enterCardOption:Lcom/squareup/ui/account/view/LineRow;

    .line 110
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_helper_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.installments_helper_text)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V
    .locals 4

    .line 59
    new-instance v0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_options_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 65
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 66
    new-instance v2, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$2;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 67
    sget v2, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    const-string v1, "linkOption"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_options_link:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;->getOnLinkSelected()Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction;

    move-result-object v0

    .line 77
    instance-of v2, v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Enabled;

    if-eqz v2, :cond_4

    .line 78
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$3;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setEnabled(Z)V

    goto :goto_0

    .line 85
    :cond_4
    instance-of v0, v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Disabled;

    if-eqz v0, :cond_8

    .line 86
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$color;->installments_row_disabled:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    .line 87
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v2, Lcom/squareup/checkoutflow/installments/impl/R$color;->installments_row_disabled:I

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setValueColor(I)V

    .line 88
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->linkOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setEnabled(Z)V

    .line 92
    :cond_8
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->enterCardOption:Lcom/squareup/ui/account/view/LineRow;

    const-string v1, "enterCardOption"

    if-nez v0, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_options_enter_card_number:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->enterCardOption:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    new-instance v1, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$4;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$update$4;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    if-nez p2, :cond_b

    const-string v0, "helperText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 99
    :cond_b
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 100
    sget p1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_options_hint:I

    const-string v1, "square_support"

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 101
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_link_options_url:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 102
    sget v0, Lcom/squareup/checkout/R$string;->square_support:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->bindViews(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
