.class public Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule;
.super Ljava/lang/Object;
.source "InstallmentsServiceCommonModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideinstallmentsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    const-class v0, Lcom/squareup/checkoutflow/installments/InstallmentsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkoutflow/installments/InstallmentsService;

    return-object p0
.end method
