.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealInstallmentsTenderOptionFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000*\u0001\u0000\u0008\n\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001JF\u0010\t\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\n\u001a\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00030\u000cH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "render",
        "input",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;

    .line 64
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-static {v1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->access$getInstallmentsWorkflow$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;->mapWorkflowRenderTypeToAnyScreen(Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)Lcom/squareup/workflow/Workflow;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/StatelessWorkflowKt;->mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;

    move-result-object v3

    .line 71
    new-instance v4, Lcom/squareup/checkoutflow/installments/InstallmentsInput;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    invoke-direct {v4, v0, v1, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsInput;-><init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)V

    .line 72
    new-instance p1, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p2

    .line 62
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method
