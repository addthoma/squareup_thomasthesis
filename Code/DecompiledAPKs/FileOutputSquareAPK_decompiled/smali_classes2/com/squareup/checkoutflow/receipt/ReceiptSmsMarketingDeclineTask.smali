.class public final Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingTask.kt"

# interfaces
.implements Lcom/squareup/queue/LoggedInTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/LoggedInTask<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0002H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0008\u001a\u00020\t8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;",
        "Lcom/squareup/queue/LoggedInTask;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;",
        "subscriber",
        "Lcom/squareup/protos/postoffice/sms/Subscriber;",
        "invitationId",
        "",
        "(Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;)V",
        "receiptService",
        "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
        "getReceiptService",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptService;",
        "setReceiptService",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptService;)V",
        "execute",
        "",
        "callback",
        "Lcom/squareup/server/SquareCallback;",
        "Lcom/squareup/server/SimpleResponse;",
        "inject",
        "component",
        "secureCopyWithoutPIIForLogs",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final invitationId:Ljava/lang/String;

.field public transient receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;)V
    .locals 1

    const-string v0, "subscriber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invitationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->invitationId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

    if-nez v0, :cond_0

    const-string v1, "receiptService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 53
    :cond_0
    new-instance v1, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;-><init>()V

    .line 54
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->invitationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->invitation_id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;

    move-result-object v1

    .line 55
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;

    move-result-object v1

    const-string v2, "DeclineSubscriptionInvit\u2026ber)\n            .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptService;->declineSmsMarketingInvitation(Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;-><init>(Lcom/squareup/server/SquareCallback;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public final getReceiptService()Lcom/squareup/checkoutflow/receipt/ReceiptService;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

    if-nez v0, :cond_0

    const-string v1, "receiptService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public inject(Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-interface {p1, p0}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;->inject(Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->inject(Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public final setReceiptService(Lcom/squareup/checkoutflow/receipt/ReceiptService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

    return-void
.end method
