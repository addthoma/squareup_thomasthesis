.class public abstract Lcom/squareup/checkoutflow/receipt/ReceiptResult;
.super Ljava/lang/Object;
.source "ReceiptResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptExited;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionMade;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$BuyerLanguageClicked;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$UpdateCustomerClicked;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$AddCardClicked;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "",
        "()V",
        "AddCardClicked",
        "BuyerLanguageClicked",
        "ReceiptExited",
        "ReceiptSelection",
        "ReceiptSelectionMade",
        "ReceiptSelectionType",
        "UpdateCustomerClicked",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptExited;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionMade;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$BuyerLanguageClicked;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$UpdateCustomerClicked;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$AddCardClicked;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptResult;-><init>()V

    return-void
.end method
