.class public final Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;
.super Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
.source "ReceiptInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;,
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002#$B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0011\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "remainingAmountConfig",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "totalAmount",
        "remainingBalanceConfig",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;",
        "tipConfig",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)V",
        "getRemainingAmountConfig",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;",
        "getRemainingBalanceConfig",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;",
        "getTenderedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getTipConfig",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;",
        "getTotalAmount",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "RemainingBalanceConfig",
        "TipConfig",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final remainingAmountConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

.field private final remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

.field private final tenderedAmount:Lcom/squareup/protos/common/Money;

.field private final tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

.field private final totalAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)V
    .locals 1

    const-string v0, "remainingAmountConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remainingBalanceConfig"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipConfig"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingAmountConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tenderedAmount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->copy(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    return-object v0
.end method

.method public final component5()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;
    .locals 7

    const-string v0, "remainingAmountConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remainingBalanceConfig"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipConfig"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingAmountConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    return-object v0
.end method

.method public final getRemainingBalanceConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    return-object v0
.end method

.method public getTenderedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTipConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    return-object v0
.end method

.method public final getTotalAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentInfo(remainingAmountConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->totalAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", remainingBalanceConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->remainingBalanceConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->tipConfig:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
