.class final Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingTask.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $callback:Lcom/squareup/server/SquareCallback;


# direct methods
.method constructor <init>(Lcom/squareup/server/SquareCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;->$callback:Lcom/squareup/server/SquareCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationResponse;",
            ">;)V"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;->$callback:Lcom/squareup/server/SquareCallback;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingDeclineTask$execute$1;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
