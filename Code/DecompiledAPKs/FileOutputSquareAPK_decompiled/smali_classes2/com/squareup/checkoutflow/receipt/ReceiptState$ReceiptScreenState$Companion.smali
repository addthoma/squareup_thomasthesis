.class public final Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;
.super Ljava/lang/Object;
.source "ReceiptState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptState.kt\ncom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion\n*L\n1#1,95:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;",
        "",
        "()V",
        "getReceiptCompleteState",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;",
        "defaultEmail",
        "",
        "getReceiptCompleteState$impl_release",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getReceiptCompleteState$impl_release(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;
    .locals 3

    if-eqz p1, :cond_0

    .line 72
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    invoke-direct {v2, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    check-cast v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->NO_SMS_MARKETING:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-direct {v0, v1, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    goto :goto_0

    .line 73
    :cond_0
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->NO_SMS_MARKETING:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    :goto_0
    return-object v0
.end method
