.class final Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;
.super Lkotlin/jvm/internal/Lambda;
.source "ReceiptSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\u00080\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;->this$0:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 93
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;->this$0:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-static {v1, v0, p1, v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->access$update(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;Landroid/view/View;)V

    return-void
.end method
