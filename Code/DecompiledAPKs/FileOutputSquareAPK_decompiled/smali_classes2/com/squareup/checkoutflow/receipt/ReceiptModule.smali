.class public abstract Lcom/squareup/checkoutflow/receipt/ReceiptModule;
.super Ljava/lang/Object;
.source "ReceiptModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindReceiptIntoBuyer(Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideReceiptWorkflow(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
