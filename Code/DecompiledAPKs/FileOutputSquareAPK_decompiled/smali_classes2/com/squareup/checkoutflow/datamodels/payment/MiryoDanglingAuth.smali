.class public final Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;
.super Ljava/lang/Object;
.source "MiryoDanglingAuth.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\u0004H\u0016J\u0010\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u0007H\u0016J\u001a\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J<\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00072\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u0004H\u0016R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;",
        "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
        "inMiryo",
        "Lcom/squareup/persistent/AtomicSyncedValue;",
        "",
        "(Lcom/squareup/persistent/AtomicSyncedValue;)V",
        "danglingAuthBillId",
        "Lcom/squareup/protos/client/IdPair;",
        "getDanglingAuthBillId",
        "()Lcom/squareup/protos/client/IdPair;",
        "clearLastAuth",
        "",
        "hasDanglingAuth",
        "onFailedAuth",
        "billId",
        "voidLastAuth",
        "Lcom/squareup/protos/common/Money;",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "logReason",
        "",
        "writeLastAuth",
        "amountToReportOnAutoVoid",
        "",
        "paymentType",
        "Lcom/squareup/PaymentType;",
        "tenderIds",
        "",
        "hasCapturedTenders",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final inMiryo:Lcom/squareup/persistent/AtomicSyncedValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/persistent/AtomicSyncedValue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "inMiryo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;->inMiryo:Lcom/squareup/persistent/AtomicSyncedValue;

    return-void
.end method


# virtual methods
.method public clearLastAuth()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;->inMiryo:Lcom/squareup/persistent/AtomicSyncedValue;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/persistent/AtomicSyncedValue;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getDanglingAuthBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hasDanglingAuth()Z
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;->inMiryo:Lcom/squareup/persistent/AtomicSyncedValue;

    invoke-interface {v0}, Lcom/squareup/persistent/AtomicSyncedValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onFailedAuth(Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    const-string v0, "billId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "logReason"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;->clearLastAuth()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public writeLastAuth(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/PaymentType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;Z)V"
        }
    .end annotation

    .line 31
    iget-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;->inMiryo:Lcom/squareup/persistent/AtomicSyncedValue;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/persistent/AtomicSyncedValue;->set(Ljava/lang/Object;)V

    return-void
.end method
