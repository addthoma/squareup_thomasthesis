.class public final Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;
.super Ljava/lang/Object;
.source "BlockedByBuyerFacingDisplayDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;Landroid/content/Context;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)Landroid/app/AlertDialog;
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)Landroid/app/AlertDialog;
    .locals 2

    .line 39
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->getTitleText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->getBodyText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 43
    sget v1, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 46
    sget v1, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->getChargeText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    new-instance v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$1;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    sget v0, Lcom/squareup/checkoutflow/impl/R$string;->blocked_by_buyer_facing_display_cancel:I

    new-instance v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$2;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$2;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 54
    new-instance v0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$3;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$createDialog$3;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026cel() }\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 30
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory$create$1;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
