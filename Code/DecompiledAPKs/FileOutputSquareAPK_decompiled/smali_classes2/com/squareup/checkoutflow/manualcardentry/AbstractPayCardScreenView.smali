.class public abstract Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "AbstractPayCardScreenView.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnCardListener;
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private editor:Lcom/squareup/register/widgets/card/CardEditor;

.field private helperTextView:Lcom/squareup/widgets/MessageView;

.field final invalidCardPopup:Lcom/squareup/flowlegacy/WarningPopup;

.field final portrait:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->portrait:Z

    .line 38
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->invalidCardPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 145
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 146
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->card_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/CardEditor;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    .line 147
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->pay_card_screen_helper_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->helperTextView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public clearCard()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setPartialCard(Lcom/squareup/register/widgets/card/PartialCard;)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getCard()Lcom/squareup/Card;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    return-object v0
.end method

.method abstract getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter<",
            "*>;"
        }
    .end annotation
.end method

.method hasCard()Z
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->hasCard()Z

    move-result v0

    return v0
.end method

.method hideGlyphBasedOnHint(Z)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->hideGlyphBasedOnHint(Z)V

    return-void
.end method

.method hideSoftKeyboard()V
    .locals 0

    .line 133
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public initCardEditor(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method isCardBlank()Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getPartialCard()Lcom/squareup/register/widgets/card/PartialCard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PartialCard;->isBlank()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$requestInitialFocus$0$AbstractPayCardScreenView()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->requestFocus()Z

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 46
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->onAttachedToWindow()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->bindViews()V

    .line 48
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->showSoftKeyboard()V

    .line 49
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getStrategy()Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p0}, Lcom/squareup/register/widgets/card/CardEditor;->setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->invalidCardPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->invalidCardPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->ignoreFocusChange()V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->hideSoftKeyboard()V

    const/4 v0, 0x0

    return v0
.end method

.method public onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V

    return-void
.end method

.method public onCardInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->updateChargeButton(Z)V

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardInvalid(Lcom/squareup/Card$PanWarning;)V

    return-void
.end method

.method public onCardValid(Lcom/squareup/Card;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->updateChargeButton(Z)V

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardValid(Lcom/squareup/Card;)V

    return-void
.end method

.method public onChargeCard(Lcom/squareup/Card;)V
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->updateChargeButton(Z)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->hideSoftKeyboard()V

    .line 71
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onChargeCard(Lcom/squareup/Card;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->invalidCardPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->invalidCardPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 57
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;Z)Z
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onPanValid(Lcom/squareup/Card;Z)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 108
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getStrategy()Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$AbstractPayCardScreenView$rINyQ-BZBFxrIsPtTD1nrVIpt8s;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$AbstractPayCardScreenView$rINyQ-BZBFxrIsPtTD1nrVIpt8s;-><init>(Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setHelperText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->helperTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method abstract updateChargeButton(Z)V
.end method
