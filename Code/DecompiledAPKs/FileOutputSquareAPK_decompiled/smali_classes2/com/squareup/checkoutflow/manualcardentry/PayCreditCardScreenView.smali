.class public Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;
.super Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;
.source "PayCreditCardScreenView.java"


# instance fields
.field private panWarningView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private tenderButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-class p2, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;->inject(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 67
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->pay_card_screen_pan_warning:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->panWarningView:Lcom/squareup/widgets/MessageView;

    .line 68
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->pay_credit_card_tender_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->tenderButton:Landroid/view/View;

    return-void
.end method


# virtual methods
.method getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    return-object v0
.end method

.method public hidePanWarning()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->panWarningView:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$PayCreditCardScreenView(Landroid/view/View;)V
    .locals 0

    .line 37
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->onTenderButtonClicked()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 32
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onAttachedToWindow()V

    .line 33
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->bindViews()V

    .line 34
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->takeView(Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->tenderButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardScreenView$h0-T2eqaBGBUIASzlnJSdiGjKhY;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardScreenView$h0-T2eqaBGBUIASzlnJSdiGjKhY;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    .line 40
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->dropView(Ljava/lang/Object;)V

    .line 45
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onDetachedFromWindow()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->onWindowFocusChanged(Z)V

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public showPanWarning(I)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->panWarningView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 50
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->panWarningView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method updateChargeButton(Z)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->tenderButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
