.class public final Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;
.super Ljava/lang/Object;
.source "PayCreditCardScreenView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;->injectPresenter(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView_MembersInjector;->injectMembers(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;)V

    return-void
.end method
