.class public final Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;
.super Ljava/lang/Object;
.source "RealClientActionDispatcherAnalytics.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\rB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\u000c\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;",
        "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logHandled",
        "",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "deepLinkUrl",
        "",
        "logNoTranslatorFound",
        "logTranslatorFoundButLinkNotHandled",
        "DispatcherEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V
    .locals 2

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deepLinkUrl"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlHandled;

    invoke-direct {v1, p1, p2}, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlHandled;-><init>(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logNoTranslatorFound(Lcom/squareup/protos/client/ClientAction;)V
    .locals 2

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$NoTranslatorFound;

    invoke-direct {v1, p1}, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$NoTranslatorFound;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logTranslatorFoundButLinkNotHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V
    .locals 2

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deepLinkUrl"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlNotHandled;

    invoke-direct {v1, p1, p2}, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlNotHandled;-><init>(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
