.class public interface abstract Lcom/squareup/camerahelper/CameraHelper;
.super Ljava/lang/Object;
.source "CameraHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/camerahelper/CameraHelper$Listener;,
        Lcom/squareup/camerahelper/CameraHelper$View;
    }
.end annotation


# virtual methods
.method public abstract dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V
.end method

.method public abstract dropView(Lcom/squareup/camerahelper/CameraHelper$View;)V
.end method

.method public abstract isCameraOrGalleryAvailable()Z
.end method

.method public abstract isGalleryAvailable()Z
.end method

.method public abstract setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V
.end method

.method public abstract startCamera()V
.end method

.method public abstract startGallery()V
.end method

.method public abstract takeView(Lcom/squareup/camerahelper/CameraHelper$View;)V
.end method
