.class public final Lcom/squareup/blueprint/Blueprint;
.super Ljava/lang/Object;
.source "Blueprint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J.\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/blueprint/Blueprint;",
        "",
        "updateContext",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "(Lcom/squareup/blueprint/ViewsUpdateContext;)V",
        "update",
        "",
        "main",
        "Lcom/squareup/blueprint/Block;",
        "extendHorizontally",
        "",
        "extendVertically",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;


# direct methods
.method public constructor <init>(Lcom/squareup/blueprint/ViewsUpdateContext;)V
    .locals 1

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    return-void
.end method

.method public static synthetic update$default(Lcom/squareup/blueprint/Blueprint;Lcom/squareup/blueprint/Block;ZZILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    .line 47
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/Blueprint;->update(Lcom/squareup/blueprint/Block;ZZ)V

    return-void
.end method


# virtual methods
.method public final update(Lcom/squareup/blueprint/Block;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "Lkotlin/Unit;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "main"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    check-cast v0, Lcom/squareup/blueprint/UpdateContext;

    const/4 v1, 0x0

    .line 50
    invoke-virtual {p1, v0, v1, v1}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    .line 55
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 56
    new-instance v2, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v3, 0x4

    invoke-direct {v2, v1, v3, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 57
    new-instance v3, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v4, 0x6

    invoke-direct {v3, v1, v4, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 58
    new-instance v4, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v5, 0x7

    invoke-direct {v4, v1, v5, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    if-eqz p2, :cond_0

    .line 60
    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    .line 61
    :goto_0
    iget-object v5, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    check-cast v5, Lcom/squareup/blueprint/UpdateContext;

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p1, v5, v3, v1}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v1

    if-eqz p2, :cond_1

    .line 63
    iget-object p2, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    check-cast p2, Lcom/squareup/blueprint/UpdateContext;

    invoke-interface {v1, v4, p2}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    :cond_1
    if-eqz p3, :cond_2

    .line 66
    sget-object p2, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    goto :goto_1

    :cond_2
    sget-object p2, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    .line 67
    :goto_1
    iget-object v1, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    check-cast v1, Lcom/squareup/blueprint/UpdateContext;

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p1, v1, v0, p2}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    if-eqz p3, :cond_3

    .line 69
    iget-object p2, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    check-cast p2, Lcom/squareup/blueprint/UpdateContext;

    invoke-interface {p1, v2, p2}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 72
    :cond_3
    iget-object p1, p0, Lcom/squareup/blueprint/Blueprint;->updateContext:Lcom/squareup/blueprint/ViewsUpdateContext;

    invoke-virtual {p1}, Lcom/squareup/blueprint/ViewsUpdateContext;->applyConstraintSet()V

    return-void
.end method
