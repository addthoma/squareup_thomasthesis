.class public final Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShareSheetSelectedAppReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Component;,
        Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShareSheetSelectedAppReceiver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShareSheetSelectedAppReceiver.kt\ncom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,82:1\n52#2:83\n*E\n*S KotlinDebug\n*F\n+ 1 ShareSheetSelectedAppReceiver.kt\ncom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver\n*L\n32#1:83\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u000e\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u00020\u000eH\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;",
        "Landroid/content/BroadcastReceiver;",
        "()V",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "getAnalytics",
        "()Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "setAnalytics",
        "(Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V",
        "onReceive",
        "",
        "context",
        "Landroid/content/Context;",
        "intent",
        "Landroid/content/Intent;",
        "getShareContext",
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;",
        "Companion",
        "Component",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Companion;

.field public static final EXTRA_SHARED_CONTEXT:Ljava/lang/String; = "extra_shared_context"


# instance fields
.field public analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->Companion:Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private final getShareContext(Landroid/content/Intent;)Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;
    .locals 2

    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "extra_shared_context"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    if-eqz p1, :cond_1

    .line 68
    invoke-static {}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->values()[Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    aget-object v0, v0, p1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final getAnalytics()Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez v0, :cond_0

    const-string v1, "analytics"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "intent"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x16

    if-lt p1, v0, :cond_d

    .line 32
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "appContext()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const-class v0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Component;

    .line 33
    invoke-interface {p1, p0}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$Component;->inject(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)V

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "android.intent.extra.CHOSEN_COMPONENT"

    .line 35
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    instance-of v1, p1, Landroid/content/ComponentName;

    if-nez v1, :cond_1

    move-object p1, v0

    :cond_1
    check-cast p1, Landroid/content/ComponentName;

    if-eqz p1, :cond_d

    .line 36
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_d

    .line 38
    invoke-direct {p0, p2}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->getShareContext(Landroid/content/Intent;)Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-result-object p2

    if-nez p2, :cond_2

    goto/16 :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    const-string v1, "analytics"

    const-string v2, "activityName"

    if-eq p2, v0, :cond_b

    const/4 v0, 0x2

    if-eq p2, v0, :cond_9

    const/4 v0, 0x3

    if-eq p2, v0, :cond_7

    const/4 v0, 0x4

    if-eq p2, v0, :cond_5

    const/4 v0, 0x5

    if-eq p2, v0, :cond_3

    goto :goto_1

    .line 56
    :cond_3
    iget-object p2, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez p2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 57
    :cond_4
    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$ShareDonationLinkAppSelectedEvent;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$ShareDonationLinkAppSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    .line 56
    invoke-interface {p2, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_1

    .line 51
    :cond_5
    iget-object p2, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez p2, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 52
    :cond_6
    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$SharePayLinkAppSelectedEvent;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$SharePayLinkAppSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    .line 51
    invoke-interface {p2, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_1

    .line 46
    :cond_7
    iget-object p2, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez p2, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 47
    :cond_8
    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent$SharePayLinkAppSelectedEvent;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent$SharePayLinkAppSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    .line 46
    invoke-interface {p2, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_1

    .line 43
    :cond_9
    iget-object p2, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez p2, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent$ShareBuyLinkAppSelectedEvent;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent$ShareBuyLinkAppSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p2, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_1

    .line 40
    :cond_b
    iget-object p2, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    if-nez p2, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    new-instance v0, Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEvent$ShareCheckoutLinkAppSelectedEvent;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEvent$ShareCheckoutLinkAppSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p2, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    :cond_d
    :goto_1
    return-void
.end method

.method public final setAnalytics(Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-void
.end method
