.class public final Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

.field private appBootstrapModule:Lcom/squareup/AppBootstrapModule;

.field private cardReaderStoreModule:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

.field private cashDrawerModule:Lcom/squareup/cashdrawer/CashDrawerModule;

.field private deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

.field private globalBleModule:Lcom/squareup/cardreader/ble/GlobalBleModule;

.field private globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

.field private globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

.field private prod:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

.field private prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

.field private prodWithoutCardReaderFactory:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 7642
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accountServiceModule(Lcom/squareup/server/account/AccountServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7711
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public accountStatusServiceModule(Lcom/squareup/server/account/AccountStatusServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7721
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public activityComponentModule(Lcom/squareup/ActivityComponentModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7749
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public addressServiceCommonModule(Lcom/squareup/server/address/AddressServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7731
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public aesGcmKeyStoreEncryptorModule(Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8074
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public androidUtilModule(Lcom/squareup/android/util/AndroidUtilModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7758
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public anrChaperoneModule(Lcom/squareup/anrchaperone/AnrChaperoneModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 7763
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/anrchaperone/AnrChaperoneModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    return-object p0
.end method

.method public appBootstrapModule(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 7768
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/AppBootstrapModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    return-object p0
.end method

.method public balanceActivityServiceMainModule(Lcom/squareup/balance/activity/service/BalanceActivityServiceMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7674
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public bizbankServiceMainModule(Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7683
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public bluetoothModule(Lcom/squareup/blescan/BluetoothModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8205
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public build()Lcom/squareup/SposReleaseAppComponent;
    .locals 14

    .line 8255
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    if-nez v0, :cond_0

    .line 8256
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperoneModule;

    invoke-direct {v0}, Lcom/squareup/anrchaperone/AnrChaperoneModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    .line 8258
    :cond_0
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    const-class v1, Lcom/squareup/AppBootstrapModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8259
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cardReaderStoreModule:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    if-nez v0, :cond_1

    .line 8260
    new-instance v0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    invoke-direct {v0}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cardReaderStoreModule:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    .line 8262
    :cond_1
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    if-nez v0, :cond_2

    .line 8263
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule;

    invoke-direct {v0}, Lcom/squareup/settings/DeviceSettingsModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    .line 8265
    :cond_2
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cashDrawerModule:Lcom/squareup/cashdrawer/CashDrawerModule;

    if-nez v0, :cond_3

    .line 8266
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawerModule;

    invoke-direct {v0}, Lcom/squareup/cashdrawer/CashDrawerModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cashDrawerModule:Lcom/squareup/cashdrawer/CashDrawerModule;

    .line 8268
    :cond_3
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalBleModule:Lcom/squareup/cardreader/ble/GlobalBleModule;

    if-nez v0, :cond_4

    .line 8269
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalBleModule:Lcom/squareup/cardreader/ble/GlobalBleModule;

    .line 8271
    :cond_4
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    if-nez v0, :cond_5

    .line 8272
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule;

    invoke-direct {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    .line 8274
    :cond_5
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    if-nez v0, :cond_6

    .line 8275
    new-instance v0, Lcom/squareup/cardreader/GlobalHeadsetModule;

    invoke-direct {v0}, Lcom/squareup/cardreader/GlobalHeadsetModule;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 8277
    :cond_6
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    if-nez v0, :cond_7

    .line 8278
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    invoke-direct {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    .line 8280
    :cond_7
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prodWithoutCardReaderFactory:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    if-nez v0, :cond_8

    .line 8281
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    invoke-direct {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prodWithoutCardReaderFactory:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    .line 8283
    :cond_8
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    if-nez v0, :cond_9

    .line 8284
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;-><init>()V

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    .line 8286
    :cond_9
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cardReaderStoreModule:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    iget-object v5, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    iget-object v6, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cashDrawerModule:Lcom/squareup/cashdrawer/CashDrawerModule;

    iget-object v7, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalBleModule:Lcom/squareup/cardreader/ble/GlobalBleModule;

    iget-object v8, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    iget-object v9, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v10, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    iget-object v11, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prodWithoutCardReaderFactory:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    iget-object v12, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    const/4 v13, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v13}, Lcom/squareup/DaggerSposReleaseAppComponent;-><init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public capitalFlexLoanServiceModule(Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7693
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public cardReaderStoreModule(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 7773
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cardReaderStoreModule:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    return-object p0
.end method

.method public cashDrawerModule(Lcom/squareup/cashdrawer/CashDrawerModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8079
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cashdrawer/CashDrawerModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->cashDrawerModule:Lcom/squareup/cashdrawer/CashDrawerModule;

    return-object p0
.end method

.method public clockModule(Lcom/squareup/android/util/ClockModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7782
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public cogsServiceCommonModule(Lcom/squareup/cogs/CogsServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7740
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public commonTerminalCheckoutsServiceModule(Lcom/squareup/server/terminal/checkouts/CommonTerminalCheckoutsServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8027
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public connectServiceModule(Lcom/squareup/server/api/ConnectServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7886
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public crmServicesMainModule(Lcom/squareup/server/crm/CrmServicesMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7895
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public customReportServiceCommonModule(Lcom/squareup/customreport/data/service/CustomReportServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7905
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public depositScheduleServiceMainModule(Lcom/squareup/depositschedule/DepositScheduleServiceMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7915
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public deviceSettingsModule(Lcom/squareup/settings/DeviceSettingsModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 7787
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/settings/DeviceSettingsModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    return-object p0
.end method

.method public emoneyPaymentProcessingScreenModule(Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8137
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public experimentsModule(Lcom/squareup/experiments/ExperimentsModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7796
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public globalBleModule(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8196
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/GlobalBleModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalBleModule:Lcom/squareup/cardreader/ble/GlobalBleModule;

    return-object p0
.end method

.method public globalCardReaderModule(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8210
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/GlobalCardReaderModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    return-object p0
.end method

.method public globalHeadsetModule(Lcom/squareup/cardreader/GlobalHeadsetModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8215
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/GlobalHeadsetModule;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    return-object p0
.end method

.method public googlePayClientModule(Lcom/squareup/googlepay/client/GooglePayClientModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8146
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public gsonModule(Lcom/squareup/gson/GsonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7805
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public httpReleaseModule(Lcom/squareup/http/HttpReleaseModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7933
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public installationIdModule(Lcom/squareup/settings/InstallationIdModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7814
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public installmentsServiceCommonModule(Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8055
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public instantDepositsServiceModule(Lcom/squareup/instantdeposit/InstantDepositsServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7943
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public linkDebitCardServiceMainModule(Lcom/squareup/debitcard/LinkDebitCardServiceMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7953
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public loggedOut(Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOut;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7868
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public mainThreadModule(Lcom/squareup/thread/MainThreadModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7823
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public merchantProfileServiceModule(Lcom/squareup/merchantprofile/MerchantProfileServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7963
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public messagesServiceModule(Lcom/squareup/server/messages/MessagesServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8064
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public multipassServiceModule(Lcom/squareup/api/multipassauth/MultipassServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7972
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public noSpeModule(Lcom/squareup/cardreader/squid/common/NoSpeModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8155
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public noStatusBarAppModule(Lcom/squareup/statusbar/event/NoStatusBarAppModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8164
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public playServicesModule(Lcom/squareup/gms/common/PlayServicesModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8173
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public posFeaturesModule(Lcom/squareup/ui/main/PosFeaturesModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8088
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public precogServiceCommonModule(Lcom/squareup/server/precog/PrecogServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8045
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public prod(Lcom/squareup/account/AccountModule$Prod;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7702
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public prod(Lcom/squareup/blescan/BluetoothModule$Prod;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8240
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public prod(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8220
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    return-object p0
.end method

.method public prod(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8231
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    return-object p0
.end method

.method public prod(Lcom/squareup/server/analytics/EventStreamModule$Prod;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7924
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public prodWithoutCardReaderFactory(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0

    .line 8226
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;->prodWithoutCardReaderFactory:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    return-object p0
.end method

.method public receiptServiceCommonModule(Lcom/squareup/checkoutflow/receipt/ReceiptServiceCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8127
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public registerPushNotificationServiceModule(Lcom/squareup/pushmessages/register/RegisterPushNotificationServiceModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8098
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public releaseMinesweeperModule(Lcom/squareup/ms/ReleaseMinesweeperModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7981
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public relinkerLibraryLoaderModule(Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8250
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public resModule(Lcom/squareup/android/util/ResModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7832
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public restAdapterReleaseModule(Lcom/squareup/server/RestAdapterReleaseModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7999
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public retrofitModule(Lcom/squareup/server/RetrofitModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7841
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public rx1SchedulerModule(Lcom/squareup/thread/Rx1SchedulerModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7850
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public rx2SchedulerModule(Lcom/squareup/thread/Rx2SchedulerModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7859
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public secureSessionModule(Lcom/squareup/cardreader/SecureSessionModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8008
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public servicesReleaseModule(Lcom/squareup/api/ServicesReleaseModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8017
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public sharedFcmModule(Lcom/squareup/firebase/fcm/SharedFcmModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8182
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public sharedFirebaseModule(Lcom/squareup/firebase/common/SharedFirebaseModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8191
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public sharedMinesweeperModule(Lcom/squareup/ms/SharedMinesweeperModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7990
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public thumborProdModule(Lcom/squareup/ThumborProdModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8036
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public timeCommonModule(Lcom/squareup/time/TimeCommonModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8107
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public transfersServiceMainModule(Lcom/squareup/balance/core/server/transfers/TransfersServiceMainModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8117
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public uniqueModule(Lcom/squareup/jvm/util/UniqueModule;)Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7877
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
