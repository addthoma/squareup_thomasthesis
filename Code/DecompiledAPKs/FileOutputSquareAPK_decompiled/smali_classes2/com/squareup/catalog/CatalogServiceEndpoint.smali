.class public Lcom/squareup/catalog/CatalogServiceEndpoint;
.super Ljava/lang/Object;
.source "CatalogServiceEndpoint.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field private static final TEMPORARY_TOKEN_PREFIX:Ljava/lang/String; = "#"


# instance fields
.field private final activeUnits:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final catalogService:Lcom/squareup/server/catalog/CatalogService;

.field private final currentUnitToken:Ljava/lang/String;

.field private final saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 65
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 70
    iput-object p2, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->catalogService:Lcom/squareup/server/catalog/CatalogService;

    .line 71
    iput-object p3, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->analytics:Lcom/squareup/analytics/Analytics;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 74
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->activeUnits:Ljava/util/Set;

    .line 76
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantUnits()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 78
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/server/account/protos/MerchantUnit;

    .line 79
    iget-object p3, p2, Lcom/squareup/server/account/protos/MerchantUnit;->active:Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 80
    iget-object p3, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->activeUnits:Ljava/util/Set;

    iget-object p2, p2, Lcom/squareup/server/account/protos/MerchantUnit;->token:Ljava/lang/String;

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private checkBatchResponseForErrors(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;)Z
    .locals 3

    .line 377
    iget-object p1, p1, Lsquareup/items/merchant/BatchRequest;->request:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iget-object v0, p2, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 381
    iget-object p1, p2, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lsquareup/items/merchant/Response;

    .line 382
    iget-object v1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v1, v1, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    sget-object v2, Lsquareup/items/merchant/PutResponse$Status;->SUCCESS:Lsquareup/items/merchant/PutResponse$Status;

    if-eq v1, v2, :cond_0

    .line 383
    iget-object p1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object p1, p1, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    if-eqz p1, :cond_3

    iget-object p1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object p1, p1, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    .line 384
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    .line 390
    iget-object p1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object p1, p1, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lsquareup/items/merchant/InvalidCatalogObject;

    .line 391
    iget-object v1, p2, Lsquareup/items/merchant/InvalidCatalogObject;->catalog_object:Lsquareup/items/merchant/CatalogObject;

    iget-object v1, v1, Lsquareup/items/merchant/CatalogObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    sget-object v2, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

    if-ne v1, v2, :cond_1

    iget-object p2, p2, Lsquareup/items/merchant/InvalidCatalogObject;->reason:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_OBJECT_REFERENCE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    if-ne p2, v1, :cond_1

    return v0

    :cond_2
    const/4 p1, 0x1

    return p1

    .line 385
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Server returned error status without providing invalid objects."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    return v0

    .line 378
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Mismatched request and response sizes."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private clientIdFromTemporaryToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    .line 571
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createDeleteRequest(Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)Lsquareup/items/merchant/DeleteRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/catalog/CatalogObjectBuilder;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/DeleteRequest;"
        }
    .end annotation

    .line 490
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 492
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 493
    invoke-interface {p3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 494
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 495
    invoke-virtual {v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object v1

    iget-object v1, v1, Lsquareup/items/merchant/CatalogObject;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 499
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 503
    :cond_2
    new-instance p1, Lsquareup/items/merchant/DeleteRequest$Builder;

    invoke-direct {p1}, Lsquareup/items/merchant/DeleteRequest$Builder;-><init>()V

    .line 504
    invoke-virtual {p1, v0}, Lsquareup/items/merchant/DeleteRequest$Builder;->catalog_object_token(Ljava/util/List;)Lsquareup/items/merchant/DeleteRequest$Builder;

    move-result-object p1

    .line 505
    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteRequest$Builder;->build()Lsquareup/items/merchant/DeleteRequest;

    move-result-object p1

    return-object p1
.end method

.method private doSaveThenCogsSync(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;Ljava/util/Set;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/catalog/CatalogObjectBuilder;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/squareup/cogs/Cogs;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    move-object v9, p0

    move-object/from16 v10, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move/from16 v8, p9

    .line 277
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/catalog/CatalogServiceEndpoint;->buildSaveRequest(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;Ljava/util/Set;ZLjava/lang/String;Ljava/util/List;Z)Lsquareup/items/merchant/BatchRequest;

    move-result-object v0

    .line 283
    iget-object v1, v0, Lsquareup/items/merchant/BatchRequest;->request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v10, v0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->syncCogsWithCatalogService(Lcom/squareup/cogs/Cogs;Ljava/util/Map;)V

    return-void

    .line 288
    :cond_0
    iget-object v1, v9, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, v9, Lcom/squareup/catalog/CatalogServiceEndpoint;->catalogService:Lcom/squareup/server/catalog/CatalogService;

    invoke-interface {v2, v0}, Lcom/squareup/server/catalog/CatalogService;->sync(Lsquareup/items/merchant/BatchRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v2

    .line 289
    invoke-virtual {v2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$upDuEPI6rQ31aRjj2bWj7gdJw0Q;

    move-object v4, p2

    invoke-direct {v3, p0, v0, p2, v10}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$upDuEPI6rQ31aRjj2bWj7gdJw0Q;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;)V

    .line 290
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 288
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method static synthetic lambda$buildCreateItemAndVariationsRequest$15(Z)Z
    .locals 0

    return p0
.end method

.method static synthetic lambda$buildCreateItemAndVariationsRequest$16(Z)Z
    .locals 0

    return p0
.end method

.method static synthetic lambda$buildSaveRequest$14(Z)Z
    .locals 0

    return p0
.end method

.method private mapCreatedCatalogObjectClientIdsToMerchantCatalogReferences(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;Ljava/util/Set;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/BatchRequest;",
            "Lsquareup/items/merchant/BatchResponse;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;"
        }
    .end annotation

    .line 328
    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 333
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 335
    :goto_0
    iget-object v3, p2, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 336
    iget-object v3, p2, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/Response;

    .line 337
    iget-object v4, v3, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v4, v4, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    sget-object v5, Lsquareup/items/merchant/PutResponse$Status;->SUCCESS:Lsquareup/items/merchant/PutResponse$Status;

    if-ne v4, v5, :cond_3

    .line 338
    iget-object v4, p1, Lsquareup/items/merchant/BatchRequest;->request:Ljava/util/List;

    .line 339
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/Request;

    iget-object v4, v4, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    const-string v5, "put request"

    invoke-static {v4, v5}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/PutRequest;

    const/4 v5, 0x0

    .line 341
    :goto_1
    iget-object v6, v4, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 342
    iget-object v6, v4, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lsquareup/items/merchant/CatalogObject;

    .line 343
    iget-object v7, v3, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v7, v7, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lsquareup/items/merchant/CatalogObject;

    .line 344
    iget-object v8, v6, Lsquareup/items/merchant/CatalogObject;->token:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/squareup/catalog/CatalogServiceEndpoint;->tokenLooksLikeATemporaryToken(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    goto :goto_2

    .line 347
    :cond_1
    iget-object v6, v6, Lsquareup/items/merchant/CatalogObject;->token:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/squareup/catalog/CatalogServiceEndpoint;->clientIdFromTemporaryToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 348
    invoke-interface {p3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 349
    new-instance v8, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;

    invoke-direct {v8}, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;-><init>()V

    iget-object v7, v7, Lsquareup/items/merchant/CatalogObject;->token:Ljava/lang/String;

    .line 351
    invoke-virtual {v8, v7}, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->catalog_object_token(Ljava/lang/String;)Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;

    move-result-object v7

    iget-object v8, v3, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v8, v8, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    .line 352
    invoke-virtual {v7, v8}, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->version(Ljava/lang/Long;)Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;

    move-result-object v7

    .line 353
    invoke-virtual {v7}, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->build()Lcom/squareup/api/items/MerchantCatalogObjectReference;

    move-result-object v7

    .line 349
    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-object v0
.end method

.method private notifySaveFailure()V
    .locals 2

    .line 403
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {}, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->failure()Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 404
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method private notifySaveSuccess(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)V"
        }
    .end annotation

    .line 409
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 410
    invoke-static {p1}, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->success(Ljava/util/Map;)Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;

    move-result-object p1

    .line 409
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 412
    iget-object p1, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method private retrieveV3ItemAndItemVariationsThenSave(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Z",
            "Lcom/squareup/cogs/Cogs;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    move-object v10, p0

    .line 222
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/catalog/EditItemVariationsState;->getEditedAndDeletedIds()Ljava/util/List;

    move-result-object v2

    if-eqz p1, :cond_0

    .line 224
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v3, 0x0

    .line 230
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/squareup/catalog/CatalogServiceEndpoint;->doSaveThenCogsSync(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;Ljava/util/Set;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    return-void

    .line 235
    :cond_1
    iget-object v11, v10, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, v10, Lcom/squareup/catalog/CatalogServiceEndpoint;->catalogService:Lcom/squareup/server/catalog/CatalogService;

    iget-object v1, v10, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 236
    invoke-static {v2, v1}, Lcom/squareup/catalog/CatalogUtils;->createGetItemRequest(Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/GetRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/catalog/CatalogService;->get(Lsquareup/items/merchant/GetRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v12

    new-instance v13, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;

    move-object v0, v13

    move-object v1, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    .line 238
    invoke-virtual {v12, v13}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 235
    invoke-virtual {v11, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private syncCogsWithCatalogService(Lcom/squareup/cogs/Cogs;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)V"
        }
    .end annotation

    .line 363
    new-instance v0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$Q0esHQuux8TeHALR3jI9uV6trNg;

    invoke-direct {v0, p0, p2}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$Q0esHQuux8TeHALR3jI9uV6trNg;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Ljava/util/Map;)V

    const/4 p2, 0x0

    invoke-interface {p1, v0, p2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method private temporaryTokenForObject(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;
    .locals 2

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private tokenLooksLikeATemporaryToken(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 562
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_0

    const-string v1, "#"

    .line 563
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method buildCreateItemAndVariationsRequest(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)Lsquareup/items/merchant/BatchRequest;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)",
            "Lsquareup/items/merchant/BatchRequest;"
        }
    .end annotation

    .line 510
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 515
    invoke-direct {p0, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->temporaryTokenForObject(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;

    move-result-object v2

    .line 517
    iget-object v3, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v3

    .line 518
    new-instance v4, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$sbH2ahnDMbG8lTkVN0SWm44M-sw;

    invoke-direct {v4, v3}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$sbH2ahnDMbG8lTkVN0SWm44M-sw;-><init>(Z)V

    invoke-static {v4}, Lcom/squareup/catalog/ItemConverter;->get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;

    move-result-object v4

    .line 520
    new-instance v5, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$wdj5_EEau5Ad0rdRIF_IqF_lgDU;

    invoke-direct {v5, v3}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$wdj5_EEau5Ad0rdRIF_IqF_lgDU;-><init>(Z)V

    invoke-static {v5}, Lcom/squareup/catalog/ItemVariationConverter;->get(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)Lcom/squareup/catalog/ItemVariationConverter;

    move-result-object v3

    .line 524
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/Item;

    iget-object v5, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 525
    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    .line 524
    invoke-virtual {v4, p1, v2, v5, v6}, Lcom/squareup/catalog/ItemConverter;->createCatalogObject(Lcom/squareup/api/items/Item;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    .line 526
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 529
    invoke-direct {p0, p2}, Lcom/squareup/catalog/CatalogServiceEndpoint;->temporaryTokenForObject(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;

    move-result-object v4

    .line 531
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object p2

    check-cast p2, Lcom/squareup/api/items/ItemVariation;

    iget-object v5, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 532
    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 531
    invoke-virtual {v3, p2, v4, v5, v2}, Lcom/squareup/catalog/ItemVariationConverter;->createCatalogObject(Lcom/squareup/api/items/ItemVariation;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p2

    .line 530
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 536
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    .line 537
    new-instance p1, Lsquareup/items/merchant/Request$Builder;

    invoke-direct {p1}, Lsquareup/items/merchant/Request$Builder;-><init>()V

    new-instance p2, Lsquareup/items/merchant/PutRequest$Builder;

    invoke-direct {p2}, Lsquareup/items/merchant/PutRequest$Builder;-><init>()V

    .line 539
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object p2

    .line 540
    invoke-virtual {p2, v1}, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object(Ljava/util/List;)Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object p2

    .line 541
    invoke-virtual {p2}, Lsquareup/items/merchant/PutRequest$Builder;->build()Lsquareup/items/merchant/PutRequest;

    move-result-object p2

    .line 538
    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Request$Builder;->put_request(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/Request$Builder;

    move-result-object p1

    .line 542
    invoke-virtual {p1}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object p1

    .line 543
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    :cond_1
    new-instance p1, Lsquareup/items/merchant/BatchRequest$Builder;

    invoke-direct {p1}, Lsquareup/items/merchant/BatchRequest$Builder;-><init>()V

    .line 547
    invoke-virtual {p1, v0}, Lsquareup/items/merchant/BatchRequest$Builder;->request(Ljava/util/List;)Lsquareup/items/merchant/BatchRequest$Builder;

    move-result-object p1

    .line 548
    invoke-virtual {p1}, Lsquareup/items/merchant/BatchRequest$Builder;->build()Lsquareup/items/merchant/BatchRequest;

    move-result-object p1

    return-object p1
.end method

.method buildSaveRequest(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;Ljava/util/Set;ZLjava/lang/String;Ljava/util/List;Z)Lsquareup/items/merchant/BatchRequest;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/catalog/CatalogObjectBuilder;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lsquareup/items/merchant/BatchRequest;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    .line 425
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 427
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 428
    iget-object v6, v1, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 429
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 430
    iget-object v9, v1, Lcom/squareup/catalog/EditItemVariationsState;->converter:Lcom/squareup/catalog/CatalogObjectConverter;

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v10

    .line 431
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/catalog/CatalogObjectBuilder;

    if-eqz p5, :cond_1

    goto :goto_1

    :cond_1
    iget-object v8, v0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 430
    :goto_1
    invoke-interface {v9, v10, v7, v8}, Lcom/squareup/catalog/CatalogObjectConverter;->updateCatalogObject(Ljava/lang/Object;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x1

    if-nez p5, :cond_4

    if-eqz p8, :cond_3

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v9, 0x1

    .line 436
    :goto_3
    iget-object v10, v1, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 437
    invoke-direct {v0, v11}, Lcom/squareup/catalog/CatalogServiceEndpoint;->temporaryTokenForObject(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;

    move-result-object v12

    if-eqz v9, :cond_5

    move-object/from16 v13, p7

    goto :goto_5

    .line 438
    :cond_5
    iget-object v13, v0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 439
    invoke-static {v13}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    .line 440
    :goto_5
    iget-object v14, v1, Lcom/squareup/catalog/EditItemVariationsState;->converter:Lcom/squareup/catalog/CatalogObjectConverter;

    .line 441
    invoke-virtual {v11}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v11

    move-object/from16 v15, p6

    .line 440
    invoke-interface {v14, v11, v12, v13, v15}, Lcom/squareup/catalog/CatalogObjectConverter;->createCatalogObject(Ljava/lang/Object;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    if-nez p5, :cond_8

    if-eqz p8, :cond_7

    goto :goto_6

    :cond_7
    const/4 v7, 0x0

    :cond_8
    :goto_6
    if-nez v7, :cond_a

    .line 447
    iget-object v1, v1, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 448
    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 449
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/catalog/CatalogObjectBuilder;

    iget-object v7, v0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/squareup/catalog/CatalogUtils;->disableCatalogObject(Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 452
    :cond_a
    iget-object v6, v1, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_b

    .line 453
    iget-object v1, v1, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    .line 454
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/catalog/CatalogServiceEndpoint;->createDeleteRequest(Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)Lsquareup/items/merchant/DeleteRequest;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 457
    new-instance v6, Lsquareup/items/merchant/Request$Builder;

    invoke-direct {v6}, Lsquareup/items/merchant/Request$Builder;-><init>()V

    .line 458
    invoke-virtual {v6, v1}, Lsquareup/items/merchant/Request$Builder;->delete_request(Lsquareup/items/merchant/DeleteRequest;)Lsquareup/items/merchant/Request$Builder;

    move-result-object v1

    .line 459
    invoke-virtual {v1}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object v1

    .line 457
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    if-eqz p1, :cond_d

    .line 464
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 465
    iget-object v1, v0, Lcom/squareup/catalog/CatalogServiceEndpoint;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v1

    .line 466
    new-instance v3, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$zsofFvsXX1tImfk0lJh_0lxqrAs;

    invoke-direct {v3, v1}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$zsofFvsXX1tImfk0lJh_0lxqrAs;-><init>(Z)V

    invoke-static {v3}, Lcom/squareup/catalog/ItemConverter;->get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;

    move-result-object v1

    .line 468
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Item;

    .line 469
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/catalog/CatalogObjectBuilder;

    if-eqz p5, :cond_c

    goto :goto_8

    :cond_c
    iget-object v8, v0, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 468
    :goto_8
    invoke-virtual {v1, v3, v2, v8}, Lcom/squareup/catalog/ItemConverter;->updateCatalogObject(Lcom/squareup/api/items/Item;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    :cond_d
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    .line 474
    new-instance v1, Lsquareup/items/merchant/Request$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/Request$Builder;-><init>()V

    new-instance v2, Lsquareup/items/merchant/PutRequest$Builder;

    invoke-direct {v2}, Lsquareup/items/merchant/PutRequest$Builder;-><init>()V

    .line 476
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object v2

    .line 477
    invoke-virtual {v2, v5}, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object(Ljava/util/List;)Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object v2

    .line 478
    invoke-virtual {v2}, Lsquareup/items/merchant/PutRequest$Builder;->build()Lsquareup/items/merchant/PutRequest;

    move-result-object v2

    .line 475
    invoke-virtual {v1, v2}, Lsquareup/items/merchant/Request$Builder;->put_request(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/Request$Builder;

    move-result-object v1

    .line 479
    invoke-virtual {v1}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object v1

    .line 480
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    :cond_e
    new-instance v1, Lsquareup/items/merchant/BatchRequest$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/BatchRequest$Builder;-><init>()V

    .line 484
    invoke-virtual {v1, v4}, Lsquareup/items/merchant/BatchRequest$Builder;->request(Ljava/util/List;)Lsquareup/items/merchant/BatchRequest$Builder;

    move-result-object v1

    .line 485
    invoke-virtual {v1}, Lsquareup/items/merchant/BatchRequest$Builder;->build()Lsquareup/items/merchant/BatchRequest;

    move-result-object v1

    return-object v1
.end method

.method public createItemAndVariationsViaV3Endpoint(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;Lrx/functions/Action1;)Lrx/Subscription;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Lrx/functions/Action1<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;)",
            "Lrx/Subscription;"
        }
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p3}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p3

    .line 181
    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/CatalogServiceEndpoint;->buildCreateItemAndVariationsRequest(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)Lsquareup/items/merchant/BatchRequest;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->catalogService:Lcom/squareup/server/catalog/CatalogService;

    invoke-interface {v2, v0}, Lcom/squareup/server/catalog/CatalogService;->sync(Lsquareup/items/merchant/BatchRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v2

    .line 184
    invoke-virtual {v2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)V

    .line 185
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 183
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-object p3

    .line 176
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "V3 save is already in progress."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$createItemAndVariationsViaV3Endpoint$6$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$WNhSnxh22oW0_ZQTvEHTuS9eaLk;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$WNhSnxh22oW0_ZQTvEHTuS9eaLk;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)V

    new-instance p1, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$9r9defeZCXsTQJjXHHRFTJeDUuI;

    invoke-direct {p1, p0}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$9r9defeZCXsTQJjXHHRFTJeDUuI;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;)V

    invoke-virtual {p4, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$doSaveThenCogsSync$12$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 290
    new-instance v0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;)V

    new-instance p1, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$GNQ94Ro8yVctq6ZQnYB8w-GzSLA;

    invoke-direct {p1, p0}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$GNQ94Ro8yVctq6ZQnYB8w-GzSLA;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;)V

    invoke-virtual {p4, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$CatalogServiceEndpoint(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;ZLsquareup/items/merchant/GetResponse;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    iget-object p7, p7, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    .line 146
    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 151
    invoke-interface {p7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Lsquareup/items/merchant/CatalogObject;

    .line 152
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 153
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->activeUnits:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 154
    invoke-static {p7, v1}, Lcom/squareup/catalog/CatalogUtils;->isObjectEnabledAtLocation(Lsquareup/items/merchant/CatalogObject;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v7, p6

    .line 159
    invoke-direct/range {v0 .. v7}, Lcom/squareup/catalog/CatalogServiceEndpoint;->retrieveV3ItemAndItemVariationsThenSave(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    return-void
.end method

.method public synthetic lambda$null$1$CatalogServiceEndpoint(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void
.end method

.method public synthetic lambda$null$10$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;Lsquareup/items/merchant/BatchResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 292
    invoke-direct {p0, p1, p4}, Lcom/squareup/catalog/CatalogServiceEndpoint;->checkBatchResponseForErrors(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void

    .line 297
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 298
    iget-object p2, p2, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 303
    :cond_1
    invoke-direct {p0, p1, p4, v0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->mapCreatedCatalogObjectClientIdsToMerchantCatalogReferences(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 310
    invoke-direct {p0, p3, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->syncCogsWithCatalogService(Lcom/squareup/cogs/Cogs;Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$null$11$CatalogServiceEndpoint(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 314
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void
.end method

.method public synthetic lambda$null$2$CatalogServiceEndpoint(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    new-instance v8, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$5gugBzM6LJDoL4hWY77yQ2V9TDQ;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$5gugBzM6LJDoL4hWY77yQ2V9TDQ;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Z)V

    new-instance v0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$3BFno0-5vf2mKYf9HWq0MweTYVA;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$3BFno0-5vf2mKYf9HWq0MweTYVA;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;)V

    move-object/from16 v2, p7

    invoke-virtual {v2, v8, v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$4$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;Lsquareup/items/merchant/BatchResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 187
    invoke-direct {p0, p1, p4}, Lcom/squareup/catalog/CatalogServiceEndpoint;->checkBatchResponseForErrors(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void

    .line 192
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 193
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 195
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 200
    :cond_1
    invoke-direct {p0, p1, p4, v0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->mapCreatedCatalogObjectClientIdsToMerchantCatalogReferences(Lsquareup/items/merchant/BatchRequest;Lsquareup/items/merchant/BatchResponse;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 204
    invoke-direct {p0, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveSuccess(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$null$5$CatalogServiceEndpoint(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 207
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void
.end method

.method public synthetic lambda$null$7$CatalogServiceEndpoint(Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;ZLsquareup/items/merchant/GetResponse;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 241
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p9

    .line 242
    iget-object v0, v0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsquareup/items/merchant/CatalogObject;

    .line 243
    iget-object v2, v1, Lsquareup/items/merchant/CatalogObject;->cogs_id:Ljava/lang/String;

    new-instance v4, Lcom/squareup/catalog/CatalogObjectBuilder;

    invoke-direct {v4, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;-><init>(Lsquareup/items/merchant/CatalogObject;)V

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 249
    :cond_0
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 250
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 251
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 252
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    .line 256
    invoke-direct/range {v0 .. v9}, Lcom/squareup/catalog/CatalogServiceEndpoint;->doSaveThenCogsSync(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;Ljava/util/Set;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    return-void
.end method

.method public synthetic lambda$null$8$CatalogServiceEndpoint(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 261
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void
.end method

.method public synthetic lambda$retrieveV3ItemAndItemVariationsThenSave$9$CatalogServiceEndpoint(Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 238
    new-instance v10, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$sslBdeUBmeTLfwLjzHWQz8xt0OU;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$sslBdeUBmeTLfwLjzHWQz8xt0OU;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    new-instance v0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$TTd40RKLIDQwXZuU0ISHT4ESb6s;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$TTd40RKLIDQwXZuU0ISHT4ESb6s;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;)V

    move-object/from16 v2, p9

    invoke-virtual {v2, v10, v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$syncCogsWithCatalogService$13$CatalogServiceEndpoint(Ljava/util/Map;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 365
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 367
    iget-object p2, p2, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-nez p2, :cond_0

    .line 368
    invoke-direct {p0, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveSuccess(Ljava/util/Map;)V

    goto :goto_0

    .line 370
    :cond_0
    iget-object p1, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterErrorName;->ITEMS_APPLET_SYNC_FAILED_AFTER_V3_PUT:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 371
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$updateItemOptionsOnItemAndSaveVariations$3$CatalogServiceEndpoint(ZZLcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 12

    move-object v8, p0

    .line 122
    invoke-virtual/range {p8 .. p8}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-object/from16 v0, p8

    .line 124
    iget-object v0, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogServiceEndpoint;->notifySaveFailure()V

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move-object v3, p3

    .line 136
    iget-object v1, v3, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_1

    .line 140
    :cond_2
    iget-object v9, v8, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, v8, Lcom/squareup/catalog/CatalogServiceEndpoint;->catalogService:Lcom/squareup/server/catalog/CatalogService;

    .line 141
    invoke-static/range {p7 .. p7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, v8, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/catalog/CatalogUtils;->createGetItemRequest(Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/GetRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/catalog/CatalogService;->get(Lsquareup/items/merchant/GetRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v10

    new-instance v11, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;

    move-object v0, v11

    move-object v1, p0

    move-object/from16 v2, p4

    move-object v3, p3

    move v4, p1

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Z)V

    .line 143
    invoke-virtual {v10, v11}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 140
    invoke-virtual {v9, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_2

    .line 137
    :cond_3
    :goto_1
    iget-object v0, v8, Lcom/squareup/catalog/CatalogServiceEndpoint;->currentUnitToken:Ljava/lang/String;

    .line 138
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    move-object v0, p0

    move-object/from16 v1, p4

    move-object v2, p3

    move v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move v7, p2

    .line 137
    invoke-direct/range {v0 .. v7}, Lcom/squareup/catalog/CatalogServiceEndpoint;->retrieveV3ItemAndItemVariationsThenSave(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V

    :goto_2
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public saveItemVariations(Lcom/squareup/catalog/EditItemVariationsState;ZLrx/functions/Action1;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Z)Lrx/Subscription;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Z",
            "Lrx/functions/Action1<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;",
            "Lcom/squareup/cogs/Cogs;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Subscription;"
        }
    .end annotation

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    .line 99
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/catalog/CatalogServiceEndpoint;->updateItemOptionsOnItemAndSaveVariations(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLrx/functions/Action1;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Z)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public updateItemOptionsOnItemAndSaveVariations(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLrx/functions/Action1;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Z)Lrx/Subscription;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Z",
            "Lrx/functions/Action1<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;",
            "Lcom/squareup/cogs/Cogs;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Subscription;"
        }
    .end annotation

    move-object v9, p0

    .line 113
    iget-object v0, v9, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 117
    iget-object v0, v9, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveStateRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v10

    .line 120
    new-instance v11, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;

    move-object v0, v11

    move-object v1, p0

    move v2, p3

    move/from16 v3, p8

    move-object v4, p2

    move-object v5, p1

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;-><init>(Lcom/squareup/catalog/CatalogServiceEndpoint;ZZLcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    move-object/from16 v1, p5

    invoke-interface {v1, v11, v0}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-object v10

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "V3 save is already in progress."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
