.class final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->apply(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCatalogAppliedLocationCountFetcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCatalogAppliedLocationCountFetcher.kt\ncom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,154:1\n704#2:155\n777#2,2:156\n*E\n*S KotlinDebug\n*F\n+ 1 RealCatalogAppliedLocationCountFetcher.kt\ncom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1\n*L\n108#1:155\n108#1,2:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;",
        "catalogObject",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;


# direct methods
.method constructor <init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;
    .locals 5

    const-string v0, "catalogObject"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;

    iget-object v0, v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-static {v0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$getCurrentLocationToken$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->isAvailableAtLocation(Ljava/lang/String;)Z

    move-result v0

    .line 110
    iget-object v1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;

    iget-object v1, v1, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-static {v1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$getActiveLocations$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    check-cast v1, Ljava/lang/Iterable;

    .line 155
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 156
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/server/account/protos/MerchantUnit;

    .line 109
    iget-object v4, v4, Lcom/squareup/server/account/protos/MerchantUnit;->token:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->isAvailableAtLocation(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_1
    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    .line 111
    :goto_1
    new-instance v1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    invoke-direct {v1, v0, p1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;-><init>(ZI)V

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    invoke-virtual {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;->apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    move-result-object p1

    return-object p1
.end method
