.class final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->retrieveCatalogObject(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00010\u00010\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "kotlin.jvm.PlatformType",
        "result",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;

    invoke-direct {v0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;-><init>()V

    sput-object v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;->INSTANCE:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    return-object p1

    .line 139
    :cond_0
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1}, Ljava/lang/Exception;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;->apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    return-object p1
.end method
