.class public final synthetic Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final synthetic f$1:Lsquareup/items/merchant/BatchRequest;

.field private final synthetic f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private final synthetic f$3:Ljava/util/List;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iput-object p2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$1:Lsquareup/items/merchant/BatchRequest;

    iput-object p3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

    iput-object p4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$3:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$1:Lsquareup/items/merchant/BatchRequest;

    iget-object v2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$vm7hE84K4RXYOg1b3TD2CKNF9WE;->f$3:Ljava/util/List;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->lambda$createItemAndVariationsViaV3Endpoint$6$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
