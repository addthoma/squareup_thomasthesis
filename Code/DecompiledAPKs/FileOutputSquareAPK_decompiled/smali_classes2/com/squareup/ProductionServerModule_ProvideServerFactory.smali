.class public final Lcom/squareup/ProductionServerModule_ProvideServerFactory;
.super Ljava/lang/Object;
.source "ProductionServerModule_ProvideServerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/Server;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;",
            ">;"
        }
    .end annotation
.end field

.field private final urlRedirectSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->accountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->urlRedirectSettingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ProductionServerModule_ProvideServerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;)",
            "Lcom/squareup/ProductionServerModule_ProvideServerFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ProductionServerModule_ProvideServerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ProductionServerModule_ProvideServerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideServer(Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/Server;
    .locals 0

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/ProductionServerModule;->provideServer(Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/Server;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/http/Server;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/http/Server;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->accountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;

    iget-object v1, p0, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->urlRedirectSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/http/UrlRedirectSetting;

    invoke-static {v0, v1}, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->provideServer(Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/Server;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->get()Lcom/squareup/http/Server;

    move-result-object v0

    return-object v0
.end method
