.class Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;
.super Ljava/lang/Object;
.source "LegacyAccountStatusResponseCache.java"

# interfaces
.implements Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;


# instance fields
.field private final delegate:Lcom/squareup/account/LogInResponseCache;


# direct methods
.method constructor <init>(Lcom/squareup/account/LogInResponseCache;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->clearCache()V

    return-void
.end method

.method public get()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/squareup/accountstatus/QuietServerPreferences;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0, p1}, Lcom/squareup/account/LogInResponseCache;->init(Lcom/squareup/accountstatus/QuietServerPreferences;)V

    return-void
.end method

.method public onLoggedIn()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->onLoggedIn()V

    return-void
.end method

.method public put(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->getSessionToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/account/LogInResponseCache;->replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method public setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;->delegate:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0, p1}, Lcom/squareup/account/LogInResponseCache;->setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
