.class public final Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;
.super Ljava/lang/Object;
.source "AccountStatusModule_ProvideStatusFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;->serviceCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;)",
            "Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;

    invoke-direct {v0, p0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStatus(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/accountstatus/AccountStatusModule;->provideStatus(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;->serviceCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;->provideStatus(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method
