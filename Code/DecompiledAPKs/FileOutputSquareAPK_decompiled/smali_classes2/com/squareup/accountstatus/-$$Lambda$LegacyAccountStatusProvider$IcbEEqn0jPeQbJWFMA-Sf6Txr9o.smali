.class public final synthetic Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;

    invoke-direct {v0}, Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;-><init>()V

    sput-object v0, Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;->INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;

    check-cast p2, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {p1, p2}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->lambda$latest$0(Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
