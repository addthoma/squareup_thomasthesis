.class public final Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;
.super Ljava/lang/Object;
.source "AllTransactionsHistoryLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final billListServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final billsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/BillsList;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStatusCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/BillsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->busProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->resProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p3, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->billListServiceProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p4, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->billsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p5, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p6, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p7, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p8, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p9, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p10, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->tenderStatusCacheProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p11, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p12, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p13, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/BillsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;"
        }
    .end annotation

    .line 90
    new-instance v14, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lcom/squareup/payment/pending/PendingTransactionsStore;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/activity/AllTransactionsHistoryLoader;
    .locals 15

    .line 99
    new-instance v14, Lcom/squareup/activity/AllTransactionsHistoryLoader;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/activity/AllTransactionsHistoryLoader;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lcom/squareup/payment/pending/PendingTransactionsStore;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/activity/AllTransactionsHistoryLoader;
    .locals 14

    .line 78
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->billListServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->billsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/activity/BillsList;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->tenderStatusCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static/range {v1 .. v13}, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lcom/squareup/payment/pending/PendingTransactionsStore;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/activity/AllTransactionsHistoryLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader_Factory;->get()Lcom/squareup/activity/AllTransactionsHistoryLoader;

    move-result-object v0

    return-object v0
.end method
