.class public final Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;
.super Ljava/lang/Object;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemizedRefundAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemizedRefundAnalytics.kt\ncom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,176:1\n704#2:177\n777#2,2:178\n704#2:180\n777#2,2:181\n704#2:183\n777#2,2:184\n1550#2,3:186\n*E\n*S KotlinDebug\n*F\n+ 1 ItemizedRefundAnalytics.kt\ncom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion\n*L\n83#1:177\n83#1,2:178\n86#1:180\n86#1,2:181\n91#1:183\n91#1,2:184\n93#1,3:186\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;",
        "refundData",
        "Lcom/squareup/activity/refund/RefundData;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
    .locals 20
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "refundData"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/refund/RefundData;->getReturnItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 84
    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 177
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 178
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 84
    invoke-virtual {v9}, Lcom/squareup/activity/refund/ItemizationDetails;->isItemization()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    :cond_1
    check-cast v7, Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    if-ne v3, v6, :cond_2

    const/4 v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    .line 86
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/activity/refund/RefundData;->getReturnTips(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 87
    move-object v6, v3

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 180
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 181
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    move-object v10, v9

    check-cast v10, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 87
    invoke-virtual {v10}, Lcom/squareup/activity/refund/ItemizationDetails;->isTip()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v7, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 182
    :cond_4
    check-cast v7, Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    if-ne v3, v6, :cond_5

    const/4 v9, 0x1

    goto :goto_3

    :cond_5
    const/4 v9, 0x0

    .line 89
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getRefundReason()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    goto :goto_4

    :cond_6
    const-string v3, ""

    :goto_4
    move-object v10, v3

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 183
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 184
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/squareup/activity/refund/TenderDetails;

    .line 91
    invoke-virtual {v7}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v7

    iget-object v7, v7, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v7, v11, v13

    if-lez v7, :cond_8

    const/4 v7, 0x1

    goto :goto_6

    :cond_8
    const/4 v7, 0x0

    :goto_6
    if-eqz v7, :cond_7

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 185
    :cond_9
    check-cast v3, Ljava/util/List;

    move-object v11, v3

    check-cast v11, Ljava/lang/Iterable;

    const-string v0, ","

    .line 92
    move-object v12, v0

    check-cast v12, Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    sget-object v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;->INSTANCE:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;

    move-object/from16 v17, v0

    check-cast v17, Lkotlin/jvm/functions/Function1;

    const/16 v18, 0x1e

    const/16 v19, 0x0

    invoke-static/range {v11 .. v19}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 94
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_d

    check-cast v1, Ljava/lang/Iterable;

    .line 186
    instance-of v0, v1, Ljava/util/Collection;

    if-eqz v0, :cond_b

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    .line 187
    :cond_b
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 94
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {v1}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_d

    const/4 v12, 0x1

    goto :goto_8

    :cond_d
    const/4 v12, 0x0

    .line 82
    :goto_8
    new-instance v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;-><init>(ZZLjava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method
