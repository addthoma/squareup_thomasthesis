.class final Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;
.super Ljava/lang/Object;
.source "RestockOnItemizedRefundCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->populateRestockableItemList(Landroid/view/View;Lcom/squareup/activity/refund/RefundData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V",
        "com/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$2$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

.field final synthetic $data$inlined:Lcom/squareup/activity/refund/RefundData;

.field final synthetic $restockableSelectedItemIndices$inlined:Ljava/util/List;

.field final synthetic $row:Lcom/squareup/activity/refund/ItemizationRow;

.field final synthetic $rowFactory$inlined:Lcom/squareup/activity/refund/ItemizationRow$Factory;

.field final synthetic $selectedItemIndices$inlined:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/ItemizationRow;Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Lcom/squareup/activity/refund/RefundData;Ljava/util/List;Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/ItemizationRow;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$row:Lcom/squareup/activity/refund/ItemizationRow;

    iput-object p2, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iput-object p3, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$data$inlined:Lcom/squareup/activity/refund/RefundData;

    iput-object p4, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$selectedItemIndices$inlined:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$rowFactory$inlined:Lcom/squareup/activity/refund/ItemizationRow$Factory;

    iput-object p6, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

    iput-object p7, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$restockableSelectedItemIndices$inlined:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 1

    .line 133
    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$row:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationRow;->getChecked()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->getEventHandler()Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;->maybeShowDeprecatingInventoryApiScreen()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 136
    :cond_0
    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$row:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationRow;->toggle()V

    .line 137
    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 138
    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;->$restockableSelectedItemIndices$inlined:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->access$itemSelectionChanged(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Ljava/util/List;)V

    return-void
.end method
