.class public final Lcom/squareup/activity/refund/IssueRefundCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "IssueRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;,
        Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIssueRefundCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IssueRefundCoordinator.kt\ncom/squareup/activity/refund/IssueRefundCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,668:1\n704#2:669\n777#2,2:670\n1360#2:672\n1429#2,3:673\n1550#2,3:683\n1550#2,3:686\n1550#2,3:689\n1642#2:692\n1643#2:700\n1642#2,2:701\n1642#2,2:703\n1360#2:705\n1429#2,3:706\n1587#2,3:709\n1103#3,7:676\n1103#3,7:693\n*E\n*S KotlinDebug\n*F\n+ 1 IssueRefundCoordinator.kt\ncom/squareup/activity/refund/IssueRefundCoordinator\n*L\n191#1:669\n191#1,2:670\n202#1:672\n202#1,3:673\n252#1,3:683\n275#1,3:686\n287#1,3:689\n310#1:692\n310#1:700\n331#1,2:701\n372#1,2:703\n410#1:705\n410#1,3:706\n411#1,3:709\n226#1,7:676\n310#1,7:693\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0002LMBA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0002J\u0008\u0010.\u001a\u00020*H\u0002J\u0010\u0010/\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u00100\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u00101\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u00102\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u00103\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u00104\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0018\u00105\u001a\u0002062\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0002J\u0008\u0010;\u001a\u00020,H\u0002J\u0010\u0010<\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010=\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010>\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010?\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010@\u001a\u00020A2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010B\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010C\u001a\u00020*2\u0006\u0010D\u001a\u00020AH\u0002J\u0010\u0010E\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010F\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0018\u0010G\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u00062\u0006\u0010H\u001a\u00020AH\u0002J\u0010\u0010I\u001a\u00020J2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010K\u001a\u00020*2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"
    }
    d2 = {
        "Lcom/squareup/activity/refund/IssueRefundCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "errorsBarPresenter",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V",
        "actionBarView",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "amountRemaining",
        "Landroid/widget/TextView;",
        "cardsRequiredHelp",
        "Lcom/squareup/widgets/MessageView;",
        "cashDrawerWarningText",
        "feesHelp",
        "giftCardContainer",
        "Landroid/view/ViewGroup;",
        "giftCardHelperText",
        "giftCardPanEditor",
        "Lcom/squareup/register/widgets/card/PanEditor;",
        "giftCardRefundAmount",
        "Lcom/squareup/widgets/OnScreenRectangleEditText;",
        "giftCardToken",
        "Lcom/squareup/ui/TokenView;",
        "reasonTable",
        "refundGiftCardButton",
        "Landroid/widget/Button;",
        "refundGiftCardTitle",
        "refundLegal",
        "refundTaxHelp",
        "tenderTable",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "clearResolvedGiftCardView",
        "configureActionBar",
        "configureCardsRequired",
        "configureRefundReasons",
        "configureRefundToGiftCard",
        "configureTaxInformation",
        "configureTenders",
        "createReasonRow",
        "Landroid/widget/CheckedTextView;",
        "reasonOption",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "reason",
        "",
        "createTenderRow",
        "detach",
        "getRemainingTenderAmount",
        "hideRefundToGiftCardEditor",
        "initializeGiftCardPanEditor",
        "refundReady",
        "",
        "resetAndGoBack",
        "showRefundGiftCardButton",
        "shouldShowButton",
        "showRefundToGiftCardEditor",
        "showResolvedGiftCardview",
        "swapRefundAmount",
        "toOriginalTender",
        "taxesInformationLocation",
        "Lcom/squareup/activity/refund/TaxesInformation;",
        "updateView",
        "Factory",
        "IssueRefundEventHandler",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private amountRemaining:Landroid/widget/TextView;

.field private cardsRequiredHelp:Lcom/squareup/widgets/MessageView;

.field private cashDrawerWarningText:Lcom/squareup/widgets/MessageView;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

.field private feesHelp:Lcom/squareup/widgets/MessageView;

.field private giftCardContainer:Landroid/view/ViewGroup;

.field private giftCardHelperText:Lcom/squareup/widgets/MessageView;

.field private giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

.field private giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

.field private giftCardToken:Lcom/squareup/ui/TokenView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private reasonTable:Landroid/view/ViewGroup;

.field private refundGiftCardButton:Landroid/widget/Button;

.field private refundGiftCardTitle:Landroid/widget/TextView;

.field private refundLegal:Lcom/squareup/widgets/MessageView;

.field private refundTaxHelp:Lcom/squareup/widgets/MessageView;

.field private final res:Lcom/squareup/util/Res;

.field private tenderTable:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ")V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBarPresenter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->data:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    iput-object p4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method public static final synthetic access$clearResolvedGiftCardView(Lcom/squareup/activity/refund/IssueRefundCoordinator;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->clearResolvedGiftCardView()V

    return-void
.end method

.method public static final synthetic access$getEventHandler$p(Lcom/squareup/activity/refund/IssueRefundCoordinator;)Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    return-object p0
.end method

.method public static final synthetic access$hideRefundToGiftCardEditor(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->hideRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method public static final synthetic access$initializeGiftCardPanEditor(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->initializeGiftCardPanEditor(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method public static final synthetic access$resetAndGoBack(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->resetAndGoBack(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method public static final synthetic access$showRefundToGiftCardEditor(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->showRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->updateView(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 549
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 550
    sget v0, Lcom/squareup/activity/R$id;->refund_cards_required:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.refund_cards_required)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cardsRequiredHelp:Lcom/squareup/widgets/MessageView;

    .line 551
    sget v0, Lcom/squareup/activity/R$id;->refund_amount_remaining:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->amountRemaining:Landroid/widget/TextView;

    .line 552
    sget v0, Lcom/squareup/activity/R$id;->tenders:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    .line 553
    sget v0, Lcom/squareup/activity/R$id;->reasons:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->reasonTable:Landroid/view/ViewGroup;

    .line 554
    sget v0, Lcom/squareup/activity/R$id;->fees_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->feesHelp:Lcom/squareup/widgets/MessageView;

    .line 555
    sget v0, Lcom/squareup/activity/R$id;->refund_legal:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundLegal:Lcom/squareup/widgets/MessageView;

    .line 556
    sget v0, Lcom/squareup/activity/R$id;->refund_tax_information:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.refund_tax_information)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundTaxHelp:Lcom/squareup/widgets/MessageView;

    .line 557
    sget v0, Lcom/squareup/activity/R$id;->refund_to_gc_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardTitle:Landroid/widget/TextView;

    .line 558
    sget v0, Lcom/squareup/activity/R$id;->refund_to_gc_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    .line 559
    sget v0, Lcom/squareup/activity/R$id;->gift_card_number:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/PanEditor;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    .line 560
    sget v0, Lcom/squareup/activity/R$id;->card_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardContainer:Landroid/view/ViewGroup;

    .line 561
    sget v0, Lcom/squareup/activity/R$id;->gift_card_refund_amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/OnScreenRectangleEditText;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 562
    sget v0, Lcom/squareup/activity/R$id;->gift_card_brand_with_unmasked_digits:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/TokenView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    .line 563
    sget v0, Lcom/squareup/activity/R$id;->refund_to_gift_card_helper_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardHelperText:Lcom/squareup/widgets/MessageView;

    .line 564
    sget v0, Lcom/squareup/activity/R$id;->cash_drawer_warning:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cashDrawerWarningText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final clearResolvedGiftCardView()V
    .locals 4

    .line 541
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    const-string v1, "giftCardPanEditor"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    .line 542
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 543
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    const-string v1, "giftCardToken"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v2}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->clearCard(Z)V

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/activity/refund/RefundData;)V
    .locals 6

    .line 164
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 165
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v1

    const-string v2, "amount"

    if-eqz v1, :cond_0

    .line 167
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 168
    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/activity/R$string;->refund_amount:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 169
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 170
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 166
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/activity/R$string;->refund_amount:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 175
    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 173
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 185
    :goto_0
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_1

    const-string v2, "actionBarView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBarView.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 180
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 181
    new-instance v2, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureActionBar$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureActionBar$1;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 182
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/activity/R$string;->refund:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundReady(Lcom/squareup/activity/refund/RefundData;)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 184
    new-instance v0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureActionBar$2;

    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    invoke-direct {v0, v2}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureActionBar$2;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final configureCardsRequired(Lcom/squareup/activity/refund/RefundData;)V
    .locals 6

    .line 189
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTendersWithResidualMoney()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 669
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 670
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 191
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->requiresCardAuthorization()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 671
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 193
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cardsRequiredHelp:Lcom/squareup/widgets/MessageView;

    const-string v1, "cardsRequiredHelp"

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 194
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v4

    if-eqz p1, :cond_6

    .line 198
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/utilities/R$string;->list_pattern_long_two_separator:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 199
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 200
    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/utilities/R$string;->list_pattern_long_final_separator:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 197
    invoke-static {p1, v2, v3}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 202
    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    .line 672
    new-instance v3, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v2, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 673
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 674
    check-cast v5, Lcom/squareup/activity/refund/TenderDetails;

    .line 202
    invoke-virtual {v5}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 675
    :cond_3
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 202
    invoke-virtual {p1, v3}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 205
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cardsRequiredHelp:Lcom/squareup/widgets/MessageView;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_5

    .line 209
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->cards_required_to_be_present_plural:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "required_cards"

    .line 210
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_2

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->cards_required_to_be_present_singular:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "required_card"

    .line 207
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 208
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 205
    :goto_2
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    return-void
.end method

.method private final configureRefundReasons(Lcom/squareup/activity/refund/RefundData;)V
    .locals 13

    .line 364
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->reasonTable:Landroid/view/ViewGroup;

    const-string v1, "reasonTable"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 366
    sget-object v2, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->RETURNED_GOODS:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 367
    sget-object v2, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ACCIDENTAL_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v4, 0x1

    aput-object v2, v0, v4

    const/4 v2, 0x2

    .line 368
    sget-object v5, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->CANCELED_ORDER:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v5, v0, v2

    const/4 v2, 0x3

    .line 369
    sget-object v5, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->FRAUDULENT_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v5, v0, v2

    const/4 v2, 0x4

    .line 370
    sget-object v5, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v5, v0, v2

    .line 365
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 372
    check-cast v0, Ljava/lang/Iterable;

    .line 703
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 374
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getOtherReason()Ljava/lang/String;

    move-result-object v2

    .line 375
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    .line 373
    invoke-static {v8, v2, v5, v3}, Lcom/squareup/util/RefundReasonsHelperKt;->getReasonName(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object v7

    .line 378
    invoke-direct {p0, v8, v7}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->createReasonRow(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;)Landroid/widget/CheckedTextView;

    move-result-object v6

    .line 379
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-result-object v2

    if-ne v8, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v6, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 381
    move-object v2, v6

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v11

    new-instance v12, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;

    move-object v5, v12

    move-object v9, p0

    move-object v10, p1

    invoke-direct/range {v5 .. v10}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;-><init>(Landroid/widget/CheckedTextView;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V

    check-cast v12, Lrx/functions/Action1;

    invoke-virtual {v11, v12}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 386
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->reasonTable:Landroid/view/ViewGroup;

    if-nez v5, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private final configureRefundToGiftCard(Lcom/squareup/activity/refund/RefundData;)V
    .locals 4

    .line 217
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getShouldShowEnabledGiftCardButton()Z

    move-result v0

    const-string v1, "refundGiftCardTitle"

    const-string v2, "refundGiftCardButton"

    const/4 v3, 0x0

    if-eqz v0, :cond_7

    .line 218
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardTitle:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v1, "giftCardContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 220
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    const-string v1, "giftCardPanEditor"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    check-cast v3, Lcom/squareup/register/widgets/card/OnPanListener;

    invoke-virtual {v0, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setOnPanListener(Lcom/squareup/register/widgets/card/OnPanListener;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v1, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    .line 676
    new-instance v1, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundToGiftCard$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundToGiftCard$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 230
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getShouldShowDisabledGiftCardButton()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 232
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardTitle:Landroid/widget/TextView;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    if-nez v0, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    if-nez v0, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 235
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardHelperText:Lcom/squareup/widgets/MessageView;

    const-string v1, "giftCardHelperText"

    if-nez v0, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    sget v2, Lcom/squareup/activity/R$string;->refund_gift_card_split_tender_hint:I

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 236
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardHelperText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v0, v3}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 239
    :cond_d
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getHasResolvedGiftCard()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 240
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->showResolvedGiftCardview(Lcom/squareup/activity/refund/RefundData;)V

    .line 241
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    goto :goto_1

    .line 242
    :cond_e
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getHasInvalidGiftCard()Z

    move-result p1

    if-eqz p1, :cond_f

    .line 243
    invoke-direct {p0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->clearResolvedGiftCardView()V

    .line 244
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refund_gift_card_credit_card_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    :goto_1
    return-void
.end method

.method private final configureTaxInformation(Lcom/squareup/activity/refund/RefundData;)V
    .locals 3

    .line 259
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->taxesInformationLocation(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/TaxesInformation;

    move-result-object p1

    sget-object v0, Lcom/squareup/activity/refund/IssueRefundCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/activity/refund/TaxesInformation;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const-string v0, "refundTaxHelp"

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    .line 269
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundTaxHelp:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 261
    :cond_1
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundTaxHelp:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 267
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundTaxHelp:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 262
    :cond_3
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundTaxHelp:Lcom/squareup/widgets/MessageView;

    if-nez v2, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 263
    sget v0, Lcom/squareup/activity/R$string;->refund_tax_help:I

    const-string v2, "learn_more"

    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 264
    sget v1, Lcom/squareup/activity/R$string;->refund_help_text_url:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 265
    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final configureTenders(Lcom/squareup/activity/refund/RefundData;)V
    .locals 14

    .line 275
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 686
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 687
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/TenderDetails;

    .line 275
    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetails;->getEditable()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 276
    :goto_0
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->amountRemaining:Landroid/widget/TextView;

    const-string v4, "amountRemaining"

    if-nez v1, :cond_3

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    const-string v1, "amount"

    if-eqz v0, :cond_5

    .line 283
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->amountRemaining:Landroid/widget/TextView;

    if-nez v0, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 279
    :cond_4
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/strings/R$string;->split_tender_amount_remaining:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 278
    invoke-static {v4}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 281
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->getRemainingTenderAmount(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 282
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 283
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v0, 0x8

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_11

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 689
    instance-of v5, v4, Ljava/util/Collection;

    if-eqz v5, :cond_7

    move-object v5, v4

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    .line 690
    :cond_7
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/activity/refund/TenderDetails;

    .line 287
    invoke-virtual {v5}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v5

    sget-object v8, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v5, v8, :cond_9

    const/4 v5, 0x1

    goto :goto_1

    :cond_9
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_8

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_11

    .line 293
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundLegal:Lcom/squareup/widgets/MessageView;

    const-string v5, "refundLegal"

    if-nez v4, :cond_a

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 288
    :cond_a
    new-instance v8, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v9, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundLegal:Lcom/squareup/widgets/MessageView;

    if-nez v9, :cond_b

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v9}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v8, v5}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 289
    sget v5, Lcom/squareup/activity/R$string;->refund_legal:I

    const-string v9, "learn_more"

    invoke-virtual {v8, v5, v9}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v5

    .line 290
    sget v8, Lcom/squareup/activity/R$string;->refund_help_text_url:I

    invoke-virtual {v5, v8}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v5

    .line 291
    sget v8, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {v5, v8}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v5

    .line 292
    invoke-virtual {v5}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 293
    invoke-virtual {v5}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v4

    xor-int/2addr v4, v2

    const-string v5, "feesHelp"

    if-eqz v4, :cond_d

    .line 299
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->feesHelp:Lcom/squareup/widgets/MessageView;

    if-nez v4, :cond_c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/activity/R$string;->refund_fees:I

    invoke-interface {v5, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 300
    :cond_d
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v4, v8, v6

    if-lez v4, :cond_f

    .line 302
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->feesHelp:Lcom/squareup/widgets/MessageView;

    if-nez v4, :cond_e

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/activity/R$string;->refund_taxes_and_fees:I

    invoke-interface {v5, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 304
    :cond_f
    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->feesHelp:Lcom/squareup/widgets/MessageView;

    if-nez v4, :cond_10

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {v4, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 308
    :cond_11
    :goto_3
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCashDrawerMoneyAfterRefund()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result v4

    .line 309
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    const-string v8, "tenderTable"

    if-nez v5, :cond_12

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    const-string v9, "null cannot be cast to non-null type com.squareup.activity.refund.TenderDetailRowHolder"

    if-nez v5, :cond_17

    .line 310
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 692
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/activity/refund/TenderDetails;

    .line 311
    invoke-direct {p0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->createTenderRow()Landroid/view/View;

    move-result-object v10

    .line 312
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_16

    check-cast v11, Lcom/squareup/activity/refund/TenderDetailRowHolder;

    .line 314
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRemainingRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v12

    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->taxesInformationLocation(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/TaxesInformation;

    move-result-object v13

    .line 313
    invoke-virtual {v11, v5, v12, v13, v4}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->bind(Lcom/squareup/activity/refund/TenderDetails;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/TaxesInformation;Z)V

    .line 323
    invoke-virtual {v5}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object v11

    iget-object v11, v11, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v11, :cond_13

    goto :goto_5

    :cond_13
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    cmp-long v13, v11, v6

    if-nez v13, :cond_14

    invoke-virtual {p1, v5}, Lcom/squareup/activity/refund/RefundData;->tenderHasBeenAtLeastPartiallyRefunded(Lcom/squareup/activity/refund/TenderDetails;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 325
    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 693
    :cond_14
    :goto_5
    new-instance v5, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureTenders$$inlined$forEach$lambda$1;

    invoke-direct {v5, p0, p1, v4}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureTenders$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;Z)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    if-nez v5, :cond_15

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {v5, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_4

    .line 312
    :cond_16
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v9}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 331
    :cond_17
    new-instance v5, Lkotlin/ranges/IntRange;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    sub-int/2addr v6, v2

    invoke-direct {v5, v3, v6}, Lkotlin/ranges/IntRange;-><init>(II)V

    check-cast v5, Ljava/lang/Iterable;

    .line 701
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1a

    move-object v5, v2

    check-cast v5, Lkotlin/collections/IntIterator;

    invoke-virtual {v5}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v5

    .line 332
    iget-object v6, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    if-nez v6, :cond_18

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    const-string v7, "row"

    .line 333
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_19

    check-cast v6, Lcom/squareup/activity/refund/TenderDetailRowHolder;

    .line 335
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/activity/refund/TenderDetails;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRemainingRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v7

    .line 336
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->taxesInformationLocation(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/TaxesInformation;

    move-result-object v10

    .line 334
    invoke-virtual {v6, v5, v7, v10, v4}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->bind(Lcom/squareup/activity/refund/TenderDetails;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/TaxesInformation;Z)V

    goto :goto_6

    .line 333
    :cond_19
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v9}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 342
    :cond_1a
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getShouldShiftTendersToGiftCard()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 343
    invoke-direct {p0, p1, v3}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->swapRefundAmount(Lcom/squareup/activity/refund/RefundData;Z)V

    :cond_1b
    const-string v2, "cashDrawerWarningText"

    if-eqz v4, :cond_1e

    .line 347
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cashDrawerWarningText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1c
    invoke-virtual {v0, v3}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cashDrawerWarningText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1d

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 348
    :cond_1d
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/activity/R$string;->cash_drawer_warning_text:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 349
    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCashDrawerMoneyAfterRefund()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v3, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 350
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 351
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 353
    :cond_1e
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->cashDrawerWarningText:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_1f

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1f
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_7
    return-void
.end method

.method private final createReasonRow(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;)Landroid/widget/CheckedTextView;
    .locals 3

    .line 395
    sget v0, Lcom/squareup/widgets/pos/R$layout;->single_choice_list_item:I

    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->reasonTable:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v2, "reasonTable"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 394
    :cond_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 397
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    const/4 v1, 0x0

    .line 400
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setBackgroundResource(I)V

    .line 401
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1, p2, v2, v1}, Lcom/squareup/util/RefundReasonsHelperKt;->getReasonName(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    sget-object p2, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-ne p1, p2, :cond_1

    .line 403
    sget p1, Lcom/squareup/activity/R$string;->refund_reason_other_hint:I

    invoke-virtual {v0, p1}, Landroid/widget/CheckedTextView;->setHint(I)V

    :cond_1
    return-object v0
.end method

.method private final createTenderRow()Landroid/view/View;
    .locals 5

    .line 358
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_refund_tender_row:I

    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v2, "tenderTable"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 359
    new-instance v1, Lcom/squareup/activity/refund/TenderDetailRowHolder;

    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/squareup/activity/refund/TenderDetailRowHolder;-><init>(Landroid/view/ViewGroup;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 360
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRemainingTenderAmount(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 409
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 705
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 706
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 707
    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 410
    invoke-virtual {v3}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 708
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 411
    new-instance v1, Lcom/squareup/protos/common/Money;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-direct {v1, v3, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 710
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 411
    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_1

    .line 409
    :cond_1
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v0, "MoneyMath.subtract(data.\u2026e), MoneyMath::sum)\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final hideRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V
    .locals 3

    .line 439
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getShouldShowRefundGiftCard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    invoke-direct {p0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->clearResolvedGiftCardView()V

    const/4 v0, 0x1

    .line 441
    invoke-direct {p0, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->showRefundGiftCardButton(Z)V

    .line 442
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v2, "giftCardContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 443
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->swapRefundAmount(Lcom/squareup/activity/refund/RefundData;Z)V

    .line 444
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->clearCard(Z)V

    .line 445
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    :cond_1
    return-void
.end method

.method private final initializeGiftCardPanEditor(Lcom/squareup/activity/refund/RefundData;)V
    .locals 3

    .line 468
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    if-nez v0, :cond_0

    const-string v1, "giftCardPanEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    const-string v1, "giftCardRefundAmount"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setEnabled(Z)V

    .line 470
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 471
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->clearCard(Z)V

    return-void
.end method

.method private final refundReady(Lcom/squareup/activity/refund/RefundData;)Z
    .locals 1

    .line 516
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isReadyForRefund()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getShouldShowRefundGiftCard()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getHasResolvedGiftCard()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final resetAndGoBack(Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 521
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->hideRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V

    .line 522
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    invoke-interface {p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->onBackPressed()V

    return-void
.end method

.method private final showRefundGiftCardButton(Z)V
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->refundGiftCardButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "refundGiftCardButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 457
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v1, "giftCardContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final showRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V
    .locals 1

    const/4 v0, 0x0

    .line 424
    invoke-direct {p0, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->showRefundGiftCardButton(Z)V

    .line 427
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->swapRefundAmount(Lcom/squareup/activity/refund/RefundData;Z)V

    return-void
.end method

.method private final showResolvedGiftCardview(Lcom/squareup/activity/refund/RefundData;)V
    .locals 4

    .line 526
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->showRefundToGiftCardEditor(Lcom/squareup/activity/refund/RefundData;)V

    .line 527
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardPanEditor:Lcom/squareup/register/widgets/card/PanEditor;

    if-nez v0, :cond_0

    const-string v1, "giftCardPanEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 528
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    const-string v1, "giftCardToken"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 530
    :cond_1
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 531
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 529
    invoke-static {v2, v3, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 528
    invoke-virtual {v0, p1}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    .line 534
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    .line 535
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/ui/TokenView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x10e0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 534
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 537
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method private final swapRefundAmount(Lcom/squareup/activity/refund/RefundData;Z)V
    .locals 3

    .line 486
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "tenderTable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 487
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lcom/squareup/activity/refund/TenderDetailRowHolder;

    if-eqz v0, :cond_5

    .line 488
    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getFirstTenderRefundAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 489
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "giftCardRefundAmount"

    if-eqz p2, :cond_3

    .line 491
    iget-object p2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    if-nez p2, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 492
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getTextWatcher()Lcom/squareup/text/EmptyTextWatcher;

    move-result-object p2

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/OnScreenRectangleEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 493
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 494
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getTextWatcher()Lcom/squareup/text/EmptyTextWatcher;

    move-result-object p2

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/OnScreenRectangleEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_1

    .line 496
    :cond_3
    iget-object p2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardRefundAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    if-nez p2, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p2, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 497
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p2

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getTextWatcher()Lcom/squareup/text/EmptyTextWatcher;

    move-result-object v1

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {p2, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 498
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private final taxesInformationLocation(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/TaxesInformation;
    .locals 3

    .line 249
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isTaxableItemSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    sget-object p1, Lcom/squareup/activity/refund/TaxesInformation;->NONE:Lcom/squareup/activity/refund/TaxesInformation;

    return-object p1

    .line 252
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTendersWithResidualMoney()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 683
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 684
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/TenderDetails;

    .line 252
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getEditable()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :cond_3
    :goto_0
    if-ne v2, v1, :cond_4

    .line 253
    sget-object p1, Lcom/squareup/activity/refund/TaxesInformation;->ON_TENDERS:Lcom/squareup/activity/refund/TaxesInformation;

    goto :goto_1

    :cond_4
    if-nez v2, :cond_5

    .line 254
    sget-object p1, Lcom/squareup/activity/refund/TaxesInformation;->UNDER_TENDERS:Lcom/squareup/activity/refund/TaxesInformation;

    :goto_1
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final updateView(Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 155
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureActionBar(Lcom/squareup/activity/refund/RefundData;)V

    .line 156
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureCardsRequired(Lcom/squareup/activity/refund/RefundData;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureRefundToGiftCard(Lcom/squareup/activity/refund/RefundData;)V

    .line 158
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureTenders(Lcom/squareup/activity/refund/RefundData;)V

    .line 159
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureRefundReasons(Lcom/squareup/activity/refund/RefundData;)V

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureTaxInformation(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 133
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->bindViews(Landroid/view/View;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->data:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 136
    new-instance v1, Lcom/squareup/activity/refund/IssueRefundCoordinator$attach$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/activity/refund/IssueRefundCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/IssueRefundCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.distinctUntilChange\u2026 .subscribe(::updateView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez p1, :cond_0

    const-string v0, "giftCardToken"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/squareup/activity/refund/IssueRefundCoordinator$attach$2;

    invoke-direct {v0, v2}, Lcom/squareup/activity/refund/IssueRefundCoordinator$attach$2;-><init>(Lcom/squareup/activity/refund/IssueRefundCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v1, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$com_squareup_ui_TokenView_Listener$0;

    invoke-direct {v1, v0}, Lcom/squareup/activity/refund/IssueRefundCoordinator$sam$com_squareup_ui_TokenView_Listener$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/ui/TokenView$Listener;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/TokenView;->setListener(Lcom/squareup/ui/TokenView$Listener;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->giftCardToken:Lcom/squareup/ui/TokenView;

    if-nez v0, :cond_0

    const-string v1, "giftCardToken"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setListener(Lcom/squareup/ui/TokenView$Listener;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->tenderTable:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v1, "tenderTable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 150
    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator;->reasonTable:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    const-string v1, "reasonTable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 151
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method
