.class public interface abstract Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;
.super Ljava/lang/Object;
.source "RestockOnItemizedRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EventHandler"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0016\u0010\u0006\u001a\u00020\u00052\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&J\u0008\u0010\n\u001a\u00020\u0005H&J\u0008\u0010\u000b\u001a\u00020\u0005H&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
        "",
        "maybeShowDeprecatingInventoryApiScreen",
        "",
        "onBackPressed",
        "",
        "onItemSelectionForRestockChanged",
        "selection",
        "",
        "",
        "onNextPressedOnRestockOnItemizedRefundScreen",
        "onSkipRestockPressed",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract maybeShowDeprecatingInventoryApiScreen()Z
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onItemSelectionForRestockChanged(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onNextPressedOnRestockOnItemizedRefundScreen()V
.end method

.method public abstract onSkipRestockPressed()V
.end method
