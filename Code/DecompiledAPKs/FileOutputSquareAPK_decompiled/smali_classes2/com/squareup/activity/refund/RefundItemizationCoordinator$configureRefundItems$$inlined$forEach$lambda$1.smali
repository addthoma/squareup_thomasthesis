.class final Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;
.super Ljava/lang/Object;
.source "RefundItemizationCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RefundItemizationCoordinator;->configureRefundItems(Lcom/squareup/activity/refund/RefundData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V",
        "com/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

.field final synthetic $data$inlined:Lcom/squareup/activity/refund/RefundData;

.field final synthetic $row:Lcom/squareup/activity/refund/ItemizationRow;

.field final synthetic $rowFactory$inlined:Lcom/squareup/activity/refund/ItemizationRow$Factory;

.field final synthetic this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/ItemizationRow;Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/RefundData;Lcom/squareup/activity/refund/ItemizationRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$row:Lcom/squareup/activity/refund/ItemizationRow;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$rowFactory$inlined:Lcom/squareup/activity/refund/ItemizationRow$Factory;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$data$inlined:Lcom/squareup/activity/refund/RefundData;

    iput-object p5, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 50
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 1

    .line 207
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$row:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationRow;->toggle()V

    .line 208
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->$allItemsRow$inlined:Lcom/squareup/activity/refund/ItemizationRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 209
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$itemSelectionChanged(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)V

    return-void
.end method
