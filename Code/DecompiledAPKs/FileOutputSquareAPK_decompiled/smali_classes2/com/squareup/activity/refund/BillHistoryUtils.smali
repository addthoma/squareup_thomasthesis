.class public final Lcom/squareup/activity/refund/BillHistoryUtils;
.super Ljava/lang/Object;
.source "BillHistories.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistories.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistories.kt\ncom/squareup/activity/refund/BillHistoryUtils\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,35:1\n1412#2,9:36\n1642#2,2:45\n1421#2:47\n1265#2,12:48\n1412#2,9:60\n1642#2,2:69\n1421#2:71\n1265#2,12:72\n1360#2:84\n1429#2,3:85\n250#2,2:88\n*E\n*S KotlinDebug\n*F\n+ 1 BillHistories.kt\ncom/squareup/activity/refund/BillHistoryUtils\n*L\n15#1,9:36\n15#1,2:45\n15#1:47\n15#1,12:48\n15#1,9:60\n15#1,2:69\n15#1:71\n15#1,12:72\n15#1:84\n15#1,3:85\n33#1,2:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003\u001a\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u0005*\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "getAllRelatedItemizations",
        "",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "getSourceBillIdForTender",
        "Lcom/squareup/protos/client/IdPair;",
        "tender",
        "Lcom/squareup/protos/client/bills/Tender;",
        "activity_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getAllRelatedItemizations(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getAllRelatedItemizations"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 16
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 18
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v1

    const-string v2, "allRelatedBills"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 45
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 44
    check-cast v4, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 19
    invoke-virtual {v4}, Lcom/squareup/server/payment/RelatedBillHistory;->getPurchaseLineItems()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 44
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 47
    :cond_2
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 55
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 56
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 20
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 57
    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_2

    .line 59
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 23
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 60
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 69
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_4
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 68
    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 24
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getReturnedLineItems()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 68
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 71
    :cond_5
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 25
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->flatten(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 79
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 80
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 26
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 81
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_4

    .line 83
    :cond_6
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 84
    new-instance p0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 85
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 86
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 27
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-interface {p0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 87
    :cond_7
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/util/Collection;

    .line 22
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 15
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final getSourceBillIdForTender(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/IdPair;
    .locals 4

    const-string v0, "$this$getSourceBillIdForTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object p0

    const-string v0, "allRelatedBills"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 88
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 33
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 89
    :goto_1
    check-cast v0, Lcom/squareup/server/payment/RelatedBillHistory;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    :cond_3
    return-object v1
.end method
