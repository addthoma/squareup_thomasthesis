.class final Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;
.super Ljava/lang/Object;
.source "RestockOnItemizedRefundCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/activity/refund/RefundData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/activity/refund/RefundData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iput-object p2, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/activity/refund/RefundData;)V
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iget-object v1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->$view:Landroid/view/View;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->access$populateRestockableItemList(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;Lcom/squareup/activity/refund/RefundData;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->access$configureActionBar(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;->accept(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method
