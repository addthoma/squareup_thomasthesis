.class public Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
.super Ljava/lang/Object;
.source "IssueReceiptScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private emailReceiptEnabled:Z

.field private iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private iconGlyphVisible:Z

.field private printReceiptEnabled:Z

.field private smsReceiptEnabled:Z

.field private subtitleVisible:Z

.field private titleText:I

.field private titleVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget-object v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 54
    iget v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleText:I

    iput v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleText:I

    .line 55
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyphVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyphVisible:Z

    .line 56
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleVisible:Z

    .line 57
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->subtitleVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->subtitleVisible:Z

    .line 58
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->emailReceiptEnabled:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->emailReceiptEnabled:Z

    .line 59
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->smsReceiptEnabled:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->smsReceiptEnabled:Z

    .line 60
    iget-boolean p1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->printReceiptEnabled:Z

    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->printReceiptEnabled:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;
    .locals 10

    .line 104
    new-instance v9, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;

    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget v2, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleText:I

    iget-boolean v3, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyphVisible:Z

    iget-boolean v4, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleVisible:Z

    iget-boolean v5, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->subtitleVisible:Z

    iget-boolean v6, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->emailReceiptEnabled:Z

    iget-boolean v7, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->smsReceiptEnabled:Z

    iget-boolean v8, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->printReceiptEnabled:Z

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZZZZZZ)V

    return-object v9
.end method

.method public setEmailReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 89
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->emailReceiptEnabled:Z

    return-object p0
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public setIconGlyphVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 74
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->iconGlyphVisible:Z

    return-object p0
.end method

.method public setPrintReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 99
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->printReceiptEnabled:Z

    return-object p0
.end method

.method public setSmsReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 94
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->smsReceiptEnabled:Z

    return-object p0
.end method

.method public setSubtitleVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 84
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->subtitleVisible:Z

    return-object p0
.end method

.method public setTitleText(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 69
    iput p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleText:I

    return-object p0
.end method

.method public setTitleVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 0

    .line 79
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->titleVisible:Z

    return-object p0
.end method
