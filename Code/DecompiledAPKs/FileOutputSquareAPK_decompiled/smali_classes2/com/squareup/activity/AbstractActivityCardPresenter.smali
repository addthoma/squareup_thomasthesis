.class public abstract Lcom/squareup/activity/AbstractActivityCardPresenter;
.super Lmortar/ViewPresenter;
.source "AbstractActivityCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lmortar/ViewPresenter<",
        "TV;>;"
    }
.end annotation


# instance fields
.field protected final runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/activity/AbstractActivityCardPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    return-void
.end method


# virtual methods
.method public createDefaultActionBarConfigBuilder()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 3

    .line 70
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/activity/AbstractActivityCardPresenter;->getActionBarText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 72
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$66NCVymmsOOM6v4vlFW4nNoOQoQ;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$66NCVymmsOOM6v4vlFW4nNoOQoQ;-><init>(Lcom/squareup/activity/AbstractActivityCardPresenter;)V

    .line 73
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getActionBarText()Ljava/lang/String;
.end method

.method public synthetic lambda$null$0$AbstractActivityCardPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    iget-object p1, p0, Lcom/squareup/activity/AbstractActivityCardPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {p1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->goBackBecauseBillIdChanged()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$AbstractActivityCardPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/activity/AbstractActivityCardPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->onBillIdChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractActivityCardPresenter$G2bXrlGx03wHKUx3cLj7sQT21_g;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AbstractActivityCardPresenter$G2bXrlGx03wHKUx3cLj7sQT21_g;-><init>(Lcom/squareup/activity/AbstractActivityCardPresenter;)V

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/activity/AbstractActivityCardPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->goBack()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/activity/AbstractActivityCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 58
    new-instance v0, Lcom/squareup/activity/-$$Lambda$AbstractActivityCardPresenter$d_U3p0i6VjAAdthpBFnu4o4tmrw;

    invoke-direct {v0, p0}, Lcom/squareup/activity/-$$Lambda$AbstractActivityCardPresenter$d_U3p0i6VjAAdthpBFnu4o4tmrw;-><init>(Lcom/squareup/activity/AbstractActivityCardPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
