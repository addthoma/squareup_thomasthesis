.class public final Lcom/squareup/activity/BillsList_Factory;
.super Ljava/lang/Object;
.source "BillsList_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/activity/BillsList;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/activity/BillsList_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/activity/BillsList_Factory;->currentBillProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/activity/BillsList_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;)",
            "Lcom/squareup/activity/BillsList_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/activity/BillsList_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/activity/BillsList_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/activity/CurrentBill;)Lcom/squareup/activity/BillsList;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/activity/BillsList;

    invoke-direct {v0, p0, p1}, Lcom/squareup/activity/BillsList;-><init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/activity/CurrentBill;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/activity/BillsList;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/activity/BillsList_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, p0, Lcom/squareup/activity/BillsList_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/CurrentBill;

    invoke-static {v0, v1}, Lcom/squareup/activity/BillsList_Factory;->newInstance(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/activity/CurrentBill;)Lcom/squareup/activity/BillsList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/activity/BillsList_Factory;->get()Lcom/squareup/activity/BillsList;

    move-result-object v0

    return-object v0
.end method
