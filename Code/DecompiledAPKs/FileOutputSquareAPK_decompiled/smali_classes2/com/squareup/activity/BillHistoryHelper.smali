.class public final Lcom/squareup/activity/BillHistoryHelper;
.super Ljava/lang/Object;
.source "BillHistoryHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static asCaptureBuilder(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 10

    .line 30
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 31
    new-instance v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->billId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    .line 34
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    new-instance v1, Ljava/util/Date;

    .line 35
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    .line 36
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getCardBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardSuffix(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    move-result-object v0

    .line 31
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v9, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-static {p1}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    .line 42
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v0

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 43
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getItemizations()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/billhistory/Bills;->createBillNote(Ljava/util/List;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, v9

    move-object v1, p0

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    return-object v9
.end method

.method public static asCaptureBuilder(Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-static {v0, p0, p1, p2}, Lcom/squareup/activity/BillHistoryHelper;->asCaptureBuilder(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    return-object p0
.end method
