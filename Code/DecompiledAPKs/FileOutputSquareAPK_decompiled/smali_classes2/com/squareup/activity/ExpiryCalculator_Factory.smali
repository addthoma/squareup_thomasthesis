.class public final Lcom/squareup/activity/ExpiryCalculator_Factory;
.super Ljava/lang/Object;
.source "ExpiryCalculator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/activity/ExpiryCalculator;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/activity/ExpiryCalculator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/activity/ExpiryCalculator_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/activity/ExpiryCalculator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/activity/ExpiryCalculator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/util/Clock;)Lcom/squareup/activity/ExpiryCalculator;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/activity/ExpiryCalculator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/activity/ExpiryCalculator;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/activity/ExpiryCalculator;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v2, p0, Lcom/squareup/activity/ExpiryCalculator_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2}, Lcom/squareup/activity/ExpiryCalculator_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/util/Clock;)Lcom/squareup/activity/ExpiryCalculator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/activity/ExpiryCalculator_Factory;->get()Lcom/squareup/activity/ExpiryCalculator;

    move-result-object v0

    return-object v0
.end method
