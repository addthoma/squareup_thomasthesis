.class public final Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;
.super Lcom/mattprecious/telescope/Lens;
.source "VeryLargeTelescopeLayout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/bugreport/VeryLargeTelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/bugreport/VeryLargeTelescopeLayout$1",
        "Lcom/mattprecious/telescope/Lens;",
        "onCapture",
        "",
        "screenshot",
        "Ljava/io/File;",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $config:Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;

.field final synthetic this$0:Lcom/squareup/bugreport/VeryLargeTelescopeLayout;


# direct methods
.method constructor <init>(Lcom/squareup/bugreport/VeryLargeTelescopeLayout;Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;",
            ")V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;->this$0:Lcom/squareup/bugreport/VeryLargeTelescopeLayout;

    iput-object p2, p0, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;->$config:Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;

    invoke-direct {p0}, Lcom/mattprecious/telescope/Lens;-><init>()V

    return-void
.end method


# virtual methods
.method public onCapture(Ljava/io/File;)V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;->$config:Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;

    iget-object v1, p0, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;->this$0:Lcom/squareup/bugreport/VeryLargeTelescopeLayout;

    invoke-static {v1}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;->onCapture(Landroid/app/Activity;Ljava/io/File;)V

    return-void
.end method
