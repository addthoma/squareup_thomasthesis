.class final Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;
.super Ljava/lang/Object;
.source "RealBrandAudioPlayer.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/brandaudio/RealBrandAudioPlayer;->initialize(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBrandAudioPlayer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBrandAudioPlayer.kt\ncom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $turnUpAudioSettings:Z

.field final synthetic this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;


# direct methods
.method constructor <init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    iput-boolean p2, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;->$turnUpAudioSettings:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 29
    iget-boolean v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;->$turnUpAudioSettings:Z

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    invoke-static {v0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getBrandAudioSettingsController$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/BrandAudioSettingsController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/brandaudio/BrandAudioSettingsController;->turnUpAudioSettings()V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    .line 32
    invoke-static {v0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getMediaPlayerFactory$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/MediaPlayerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/brandaudio/MediaPlayerFactory;->create()Lcom/squareup/brandaudio/AudioPlayer;

    move-result-object v1

    const/4 v2, 0x4

    .line 34
    invoke-interface {v1, v2}, Lcom/squareup/brandaudio/AudioPlayer;->setAudioStreamType(I)V

    invoke-static {v0, v1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$setPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Lcom/squareup/brandaudio/AudioPlayer;)V

    return-void
.end method
