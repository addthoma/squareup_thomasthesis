.class public final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AccessiblePinTutorialStepCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessiblePinTutorialStepCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,128:1\n1360#2:129\n1429#2,3:130\n*E\n*S KotlinDebug\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator\n*L\n63#1:129\n63#1,3:130\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001%B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010#\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020\u0012H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u00020\u00128BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "cancelButton",
        "Landroid/widget/ImageView;",
        "crossFadeViews",
        "Lcom/squareup/accessibility/pin/impl/CrossFadeViews;",
        "currentPage",
        "Lcom/squareup/accessibility/pin/impl/TutorialPageResources;",
        "currentPageIndex",
        "",
        "getCurrentPageIndex",
        "()I",
        "pageIndicator",
        "Lcom/squareup/marin/widgets/MarinPageIndicator;",
        "pagerAdapter",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;",
        "tutorialDone",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "viewPager",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "selectPage",
        "position",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private cancelButton:Landroid/widget/ImageView;

.field private crossFadeViews:Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

.field private currentPage:Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

.field private pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

.field private final pagerAdapter:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialDone:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 47
    new-instance p1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;

    iget-object p2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {p2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;-><init>(Lcom/squareup/util/Res;)V

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pagerAdapter:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;

    .line 48
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->tutorialDone:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 55
    sget-object p1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {p1}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->currentPage:Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    return-void
.end method

.method public static final synthetic access$getCancelButton$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->cancelButton:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "cancelButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCrossFadeViews$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Lcom/squareup/accessibility/pin/impl/CrossFadeViews;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->crossFadeViews:Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    if-nez p0, :cond_0

    const-string v0, "crossFadeViews"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCurrentPageIndex$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)I
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->getCurrentPageIndex()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getTutorialDone$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->tutorialDone:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$selectPage(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->selectPage(I)V

    return-void
.end method

.method public static final synthetic access$setCancelButton$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->cancelButton:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setCrossFadeViews$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;Lcom/squareup/accessibility/pin/impl/CrossFadeViews;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->crossFadeViews:Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/accessibility/pin/impl/R$id;->cancel_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->cancelButton:Landroid/widget/ImageView;

    .line 120
    sget v0, Lcom/squareup/accessibility/pin/impl/R$id;->page_indicator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinPageIndicator;

    iput-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    .line 121
    sget v0, Lcom/squareup/accessibility/pin/impl/R$id;->view_pager:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    iput-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    .line 122
    sget v0, Lcom/squareup/accessibility/pin/impl/R$id;->paged_tutorial_views:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->crossFadeViews:Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    return-void
.end method

.method private final getCurrentPageIndex()I
    .locals 2

    .line 126
    sget-object v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->currentPage:Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private final selectPage(I)V
    .locals 3

    .line 113
    sget-object v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    iput-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->currentPage:Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 114
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    const-string/jumbo v1, "viewPager"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->requestFocus()Z

    .line 115
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-virtual {v1, p1, v2}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->textForAccessibility(ILcom/squareup/buyer/language/BuyerLocaleOverride;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->bindViews(Landroid/view/View;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->crossFadeViews:Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    if-nez v0, :cond_0

    const-string v1, "crossFadeViews"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 129
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 130
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 131
    check-cast v3, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 63
    invoke-virtual {v3}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->getScreenImageDrawableId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 63
    invoke-virtual {v0, v2}, Lcom/squareup/accessibility/pin/impl/CrossFadeViews;->assemble(Ljava/util/List;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    const-string/jumbo v1, "viewPager"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pagerAdapter:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialPagerAdapter;

    check-cast v2, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {v0, v2}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v2, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->setOffscreenPageLimit(I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    const-string v2, "pageIndicator"

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->viewPager:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;

    if-nez v3, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v3, Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-direct {p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->getCurrentPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setCurrentItem(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V

    check-cast v1, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 99
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 100
    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v2, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$4;->INSTANCE:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$4;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "screens.map { wrappedScr\u2026edScreen.unwrapV2Screen }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->tutorialDone:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v2, Lio/reactivex/Observable;

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
