.class public Lcom/squareup/accessibility/AccessibilityEnabledEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "AccessibilityEnabledEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "mobile_accessibility"

.field private static final DEFAULT_FONT_SCALE:F = 1.0f

.field private static final TALKBACK_SERVICE:Ljava/lang/String; = "talkbackservice"


# instance fields
.field private mobile_accessibility_captions_enabled:Z

.field private mobile_accessibility_color_inversion_enabled:Z

.field private mobile_accessibility_enabled_accessibility_services:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mobile_accessibility_large_font_enabled:Z

.field private mobile_accessibility_speak_passwords_enabled:Z

.field private mobile_accessibility_talkback_enabled:Z

.field private mobile_accessibility_touch_exploration_enabled:Z

.field private final transient resolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "mobile_accessibility"

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->resolver:Landroid/content/ContentResolver;

    const-string v0, "accessibility"

    .line 54
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->extractValuesFrom(Landroid/view/accessibility/AccessibilityManager;)V

    const-string v0, "captioning"

    .line 58
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/accessibility/CaptioningManager;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_captions_enabled:Z

    .line 62
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt p1, v2, :cond_1

    const-string p1, "accessibility_display_inversion_enabled"

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->isSecureSettingEnabled(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_color_inversion_enabled:Z

    const-string p1, "font_scale"

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->valueOfSystemSetting(Ljava/lang/String;)F

    move-result p1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float p1, p1, v2

    if-lez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_large_font_enabled:Z

    const-string p1, "speak_password"

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->isSecureSettingEnabled(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_speak_passwords_enabled:Z

    return-void
.end method

.method private extractValuesFrom(Landroid/view/accessibility/AccessibilityManager;)V
    .locals 3

    .line 70
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_enabled_accessibility_services:Ljava/util/Set;

    if-eqz p1, :cond_3

    .line 72
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, -0x1

    .line 79
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 82
    invoke-direct {p0, v1}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->nameOf(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;

    move-result-object v1

    .line 83
    iget-object v2, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_enabled_accessibility_services:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v2, "talkbackservice"

    .line 84
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 85
    iput-boolean v1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_talkback_enabled:Z

    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_touch_exploration_enabled:Z

    return-void

    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 73
    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_talkback_enabled:Z

    .line 74
    iput-boolean p1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->mobile_accessibility_touch_exploration_enabled:Z

    return-void
.end method

.method private isSecureSettingEnabled(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    .line 94
    :try_start_0
    iget-object v1, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->resolver:Landroid/content/ContentResolver;

    invoke-static {v1, p1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :catch_0
    :cond_0
    return v0
.end method

.method private nameOf(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 109
    invoke-virtual {p1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    if-eqz p1, :cond_1

    .line 110
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    :cond_1
    if-eqz v0, :cond_2

    .line 111
    iget-object p1, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-string p1, ""

    :goto_1
    return-object p1
.end method

.method private valueOfSystemSetting(Ljava/lang/String;)F
    .locals 1

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilityEnabledEvent;->resolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F

    move-result p1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/high16 p1, -0x40800000    # -1.0f

    return p1
.end method
