.class public final Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;
.super Ljava/lang/Object;
.source "RealCustomerCheckoutSettings.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/CustomerCheckoutSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0006H\u0016J\u0010\u0010\u000e\u001a\u00020\u000c2\u0006\u0010\u0007\u001a\u00020\u0006H\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;",
        "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "defaultCustomerCheckoutEnabled",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "skipCartScreenEnabled",
        "(Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "isDefaultCustomerCheckoutEnabled",
        "isSkipCartScreenEnabled",
        "setDefaultCustomerCheckoutEnabled",
        "",
        "defaultBuyerCheckoutEnabled",
        "setSkipCartScreenEnabled",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultCustomerCheckoutEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final skipCartScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/buyercheckout/DefaultCustomerCheckoutEnabled;
        .end annotation
    .end param
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/buyercheckout/SkipCartScreenEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCustomerCheckoutEnabled"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skipCartScreenEnabled"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->defaultCustomerCheckoutEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p3, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->skipCartScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method


# virtual methods
.method public isDefaultCustomerCheckoutEnabled()Z
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->defaultCustomerCheckoutEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "defaultCustomerCheckoutEnabled.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSkipCartScreenEnabled()Z
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->skipCartScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "skipCartScreenEnabled.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setDefaultCustomerCheckoutEnabled(Z)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->defaultCustomerCheckoutEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setSkipCartScreenEnabled(Z)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealCustomerCheckoutSettings;->skipCartScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method
