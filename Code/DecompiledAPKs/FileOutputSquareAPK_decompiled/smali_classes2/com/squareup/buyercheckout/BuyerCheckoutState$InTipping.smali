.class public final Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;
.super Lcom/squareup/buyercheckout/BuyerCheckoutState;
.source "BuyerCheckoutState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InTipping"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0087\u0008\u0018\u00002\u00020\u0001Bg\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012X\u0008\u0002\u0010\u0004\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u00080\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0005j&\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u0008j\u0006\u0012\u0002\u0008\u0003`\u000c`\u000b\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003JY\u0010\u0013\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u00080\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0005j&\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u0008j\u0006\u0012\u0002\u0008\u0003`\u000c`\u000bH\u00c6\u0003Jm\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032X\u0008\u0002\u0010\u0004\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u00080\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0005j&\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u0008j\u0006\u0012\u0002\u0008\u0003`\u000c`\u000bH\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fRa\u0010\u0004\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0007\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u00080\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0005j&\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\t0\u0008j\u0006\u0012\u0002\u0008\u0003`\u000c`\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "data",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;",
        "tipWorkflowHandle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V",
        "getData",
        "()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;",
        "getTipWorkflowHandle",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

.field private final tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;-",
            "Lkotlin/Unit;",
            "+",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipWorkflowHandle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    iput-object p2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 58
    sget-object p2, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;->Companion:Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;

    const/4 p3, 0x1

    const/4 p4, 0x0

    invoke-static {p2, p4, p3, p4}, Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;->legacyHandle$default(Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILjava/lang/Object;)Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->copy(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public final copy(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;-",
            "Lkotlin/Unit;",
            "+",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;)",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipWorkflowHandle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-direct {v0, p1, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    iget-object v1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    return-object v0
.end method

.method public final getTipWorkflowHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InTipping(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->data:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipWorkflowHandle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->tipWorkflowHandle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
