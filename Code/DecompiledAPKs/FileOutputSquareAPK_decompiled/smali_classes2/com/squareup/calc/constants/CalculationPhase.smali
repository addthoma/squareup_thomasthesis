.class public enum Lcom/squareup/calc/constants/CalculationPhase;
.super Ljava/lang/Enum;
.source "CalculationPhase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/calc/constants/CalculationPhase;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum SWEDISH_ROUNDING_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

.field public static final enum TIP_PHASE:Lcom/squareup/calc/constants/CalculationPhase;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 10
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT_PERCENTAGE_PHASE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 11
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v2, 0x1

    const-string v3, "DISCOUNT_AMOUNT_PHASE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 13
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v3, 0x2

    const-string v4, "SURCHARGE_PHASE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 15
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase$1;

    const/4 v4, 0x3

    const-string v5, "FEE_SUBTOTAL_PHASE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/calc/constants/CalculationPhase$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 21
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase$2;

    const/4 v5, 0x4

    const-string v6, "FEE_TOTAL_PHASE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/calc/constants/CalculationPhase$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 27
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v6, 0x5

    const-string v7, "SURCHARGE_TOTAL_PHASE"

    invoke-direct {v0, v7, v6}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 29
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v7, 0x6

    const-string v8, "TIP_PHASE"

    invoke-direct {v0, v8, v7}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->TIP_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 30
    new-instance v0, Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v8, 0x7

    const-string v9, "SWEDISH_ROUNDING_PHASE"

    invoke-direct {v0, v9, v8}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/calc/constants/CalculationPhase;

    .line 9
    sget-object v9, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->TIP_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/calc/constants/CalculationPhase;->$VALUES:[Lcom/squareup/calc/constants/CalculationPhase;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/calc/constants/CalculationPhase$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2}, Lcom/squareup/calc/constants/CalculationPhase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/calc/constants/CalculationPhase;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/calc/constants/CalculationPhase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/calc/constants/CalculationPhase;

    return-object p0
.end method

.method public static values()[Lcom/squareup/calc/constants/CalculationPhase;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->$VALUES:[Lcom/squareup/calc/constants/CalculationPhase;

    invoke-virtual {v0}, [Lcom/squareup/calc/constants/CalculationPhase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0
.end method


# virtual methods
.method public isFee()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
