.class public interface abstract Lcom/squareup/AppDelegate;
.super Ljava/lang/Object;
.source "AppDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/AppDelegate$Locator;
    }
.end annotation


# virtual methods
.method public abstract getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract getLoggedInMortarScope()Lmortar/MortarScope;
.end method

.method public abstract getOhSnapLogger()Lcom/squareup/log/OhSnapLogger;
.end method

.method public abstract getProcessStartupUptime()J
.end method

.method public abstract isLoggedIn()Z
.end method
