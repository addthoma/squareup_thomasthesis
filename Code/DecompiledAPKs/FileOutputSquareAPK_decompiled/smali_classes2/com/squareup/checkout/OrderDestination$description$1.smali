.class final Lcom/squareup/checkout/OrderDestination$description$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderDestination.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/OrderDestination;->description(Lcom/squareup/util/Res;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "seat",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkout/OrderDestination$description$1;->$res:Lcom/squareup/util/Res;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/OrderDestination$description$1;->invoke(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)Ljava/lang/String;
    .locals 2

    const-string v0, "seat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination$description$1;->$res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->seating_seat_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 52
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "seat_number"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
