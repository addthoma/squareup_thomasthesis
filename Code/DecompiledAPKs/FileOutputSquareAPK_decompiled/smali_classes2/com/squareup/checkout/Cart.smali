.class public final Lcom/squareup/checkout/Cart;
.super Ljava/lang/Object;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Cart$Builder;
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final deletedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/checkout/Cart$Builder;)V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/squareup/checkout/Cart$Builder;->access$000(Lcom/squareup/checkout/Cart$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/squareup/checkout/Cart$Builder;->access$100(Lcom/squareup/checkout/Cart$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/Cart;->deletedItems:Ljava/util/List;

    .line 42
    invoke-static {p1}, Lcom/squareup/checkout/Cart$Builder;->access$200(Lcom/squareup/checkout/Cart$Builder;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkout/Cart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/checkout/Cart$1;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/checkout/Cart;-><init>(Lcom/squareup/checkout/Cart$Builder;)V

    return-void
.end method


# virtual methods
.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/checkout/Cart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public getItem(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    return-object p1
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toBuilder()Lcom/squareup/checkout/Cart$Builder;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/checkout/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/Cart$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/checkout/Cart;->items:Ljava/util/List;

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Cart$Builder;->addItems(Ljava/lang/Iterable;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/Cart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 78
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Cart$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object v0

    return-object v0
.end method
