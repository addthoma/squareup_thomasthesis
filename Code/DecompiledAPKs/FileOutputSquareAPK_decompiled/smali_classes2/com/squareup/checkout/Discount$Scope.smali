.class public final enum Lcom/squareup/checkout/Discount$Scope;
.super Ljava/lang/Enum;
.source "Discount.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/checkout/Discount$Scope;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkout/Discount$Scope;",
        "",
        "protoScope",
        "Lcom/squareup/protos/client/bills/ApplicationScope;",
        "applyPerQuantity",
        "",
        "atItemScope",
        "(Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;ZZ)V",
        "ITEMIZATION",
        "ITEMIZATION_PER_QUANTITY",
        "CART",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/checkout/Discount$Scope;

.field public static final enum CART:Lcom/squareup/checkout/Discount$Scope;

.field public static final enum ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

.field public static final enum ITEMIZATION_PER_QUANTITY:Lcom/squareup/checkout/Discount$Scope;


# instance fields
.field public final applyPerQuantity:Z

.field public final atItemScope:Z

.field public final protoScope:Lcom/squareup/protos/client/bills/ApplicationScope;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/checkout/Discount$Scope;

    new-instance v7, Lcom/squareup/checkout/Discount$Scope;

    .line 131
    sget-object v4, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    const-string v2, "ITEMIZATION"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkout/Discount$Scope;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;ZZ)V

    sput-object v7, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/checkout/Discount$Scope;

    .line 134
    sget-object v11, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL_PER_QUANTITY:Lcom/squareup/protos/client/bills/ApplicationScope;

    const-string v9, "ITEMIZATION_PER_QUANTITY"

    const/4 v10, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/squareup/checkout/Discount$Scope;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;ZZ)V

    sput-object v1, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION_PER_QUANTITY:Lcom/squareup/checkout/Discount$Scope;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/checkout/Discount$Scope;

    .line 137
    sget-object v6, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    const-string v4, "CART"

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkout/Discount$Scope;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;ZZ)V

    sput-object v1, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/checkout/Discount$Scope;->$VALUES:[Lcom/squareup/checkout/Discount$Scope;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/ApplicationScope;",
            "ZZ)V"
        }
    .end annotation

    .line 122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/checkout/Discount$Scope;->protoScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    iput-boolean p4, p0, Lcom/squareup/checkout/Discount$Scope;->applyPerQuantity:Z

    iput-boolean p5, p0, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Scope;
    .locals 1

    const-class v0, Lcom/squareup/checkout/Discount$Scope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Discount$Scope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/checkout/Discount$Scope;
    .locals 1

    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->$VALUES:[Lcom/squareup/checkout/Discount$Scope;

    invoke-virtual {v0}, [Lcom/squareup/checkout/Discount$Scope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/checkout/Discount$Scope;

    return-object v0
.end method
