.class final Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutAppletWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->handleCartData(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "-",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cartData:Lcom/squareup/checkout/v2/data/transaction/model/CartData;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;->$cartData:Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "-",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;->$cartData:Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    invoke-virtual {v0}, Lcom/squareup/checkout/v2/data/transaction/model/CartData;->getItemCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    .line 98
    iget-object v0, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;->$cartData:Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    invoke-virtual {v0}, Lcom/squareup/checkout/v2/data/transaction/model/CartData;->getItems()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const-string v0, ", "

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    sget-object v0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1$1;->INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1$1;

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    const-string v0, "Cart item count = %d: %s"

    .line 96
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
