.class public final Lcom/squareup/checkout/v2/data/transaction/model/CartDataKt;
.super Ljava/lang/Object;
.source "CartData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "toCartData",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "",
        "Lcom/squareup/checkout/v2/data/transaction/model/ItemData;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toCartData(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/v2/data/transaction/model/ItemData;",
            ">;)",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;"
        }
    .end annotation

    const-string v0, "$this$toCartData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/squareup/checkout/v2/data/transaction/model/CartData;-><init>(Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
