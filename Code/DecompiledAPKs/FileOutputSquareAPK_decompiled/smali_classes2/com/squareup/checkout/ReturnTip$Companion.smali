.class public final Lcom/squareup/checkout/ReturnTip$Companion;
.super Ljava/lang/Object;
.source "ReturnTip.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/ReturnTip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnTip.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnTip.kt\ncom/squareup/checkout/ReturnTip$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,54:1\n1360#2:55\n1429#2,3:56\n*E\n*S KotlinDebug\n*F\n+ 1 ReturnTip.kt\ncom/squareup/checkout/ReturnTip$Companion\n*L\n37#1:55\n37#1,3:56\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkout/ReturnTip$Companion;",
        "",
        "()V",
        "fromReturnTipLineItem",
        "Lcom/squareup/checkout/ReturnTip;",
        "lineItem",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
        "fromReturnTipLineItems",
        "",
        "lineItems",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/checkout/ReturnTip$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromReturnTipLineItem(Lcom/squareup/protos/client/bills/ReturnTipLineItem;)Lcom/squareup/checkout/ReturnTip;
    .locals 5

    const-string v0, "lineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/checkout/ReturnTip;

    .line 45
    iget-object v1, p1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem;->tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v2, "tip_line_item.tip_line_item_id_pair"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v2, p1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->source_tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 47
    iget-object v3, p1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const-string v4, "tip_line_item.amounts.applied_money"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object p1, p1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    const-string v4, "tip_line_item.tender_server_token"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {v0, v1, v3, p1, v2}, Lcom/squareup/checkout/ReturnTip;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    return-object v0
.end method

.method public final fromReturnTipLineItems(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;"
        }
    .end annotation

    const-string v0, "lineItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    check-cast p1, Ljava/lang/Iterable;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 56
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 57
    check-cast v1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-object v2, p0

    check-cast v2, Lcom/squareup/checkout/ReturnTip$Companion;

    .line 37
    invoke-virtual {v2, v1}, Lcom/squareup/checkout/ReturnTip$Companion;->fromReturnTipLineItem(Lcom/squareup/protos/client/bills/ReturnTipLineItem;)Lcom/squareup/checkout/ReturnTip;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
