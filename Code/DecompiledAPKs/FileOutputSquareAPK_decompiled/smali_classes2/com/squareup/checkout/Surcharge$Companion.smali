.class public final Lcom/squareup/checkout/Surcharge$Companion;
.super Ljava/lang/Object;
.source "Surcharge.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Surcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSurcharge.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Surcharge.kt\ncom/squareup/checkout/Surcharge$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,201:1\n704#2:202\n777#2,2:203\n1360#2:205\n1429#2,3:206\n1099#2,2:209\n1127#2,4:211\n704#2:215\n777#2,2:216\n250#2,2:218\n*E\n*S KotlinDebug\n*F\n+ 1 Surcharge.kt\ncom/squareup/checkout/Surcharge$Companion\n*L\n185#1:202\n185#1,2:203\n186#1:205\n186#1,3:206\n187#1,2:209\n187#1,4:211\n194#1:215\n194#1,2:216\n198#1,2:218\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\"\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00040\u00072\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\nH\u0007J\u0018\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\nH\u0007J$\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00040\n2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkout/Surcharge$Companion;",
        "",
        "()V",
        "fromProto",
        "Lcom/squareup/checkout/Surcharge;",
        "surchargeProto",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "",
        "",
        "surchargeProtos",
        "",
        "getAutoGratuity",
        "Lcom/squareup/checkout/Surcharge$AutoGratuity;",
        "surcharges",
        "withPhase",
        "phase",
        "Lcom/squareup/calc/constants/CalculationPhase;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromProto(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/checkout/Surcharge;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "surchargeProto"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    sget-object v1, Lcom/squareup/api/items/Surcharge$Type;->AUTO_GRATUITY:Lcom/squareup/api/items/Surcharge$Type;

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 179
    new-instance v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    invoke-direct {v0, p1, v3, v2, v3}, Lcom/squareup/checkout/Surcharge$AutoGratuity;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    goto :goto_0

    .line 181
    :cond_0
    new-instance v0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    invoke-direct {v0, p1, v3, v2, v3}, Lcom/squareup/checkout/Surcharge$CustomSurcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    :goto_0
    return-object v0
.end method

.method public final fromProto(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "surchargeProtos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    check-cast p1, Ljava/lang/Iterable;

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 203
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 185
    iget-object v2, v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 205
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 206
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 207
    check-cast v2, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 186
    sget-object v3, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    invoke-virtual {v3, v2}, Lcom/squareup/checkout/Surcharge$Companion;->fromProto(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/checkout/Surcharge;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 208
    :cond_3
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 209
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 210
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 211
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 212
    move-object v2, v0

    check-cast v2, Lcom/squareup/checkout/Surcharge;

    .line 187
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    return-object v1
.end method

.method public final getAutoGratuity(Ljava/util/List;)Lcom/squareup/checkout/Surcharge$AutoGratuity;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/Surcharge;",
            ">;)",
            "Lcom/squareup/checkout/Surcharge$AutoGratuity;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "surcharges"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    check-cast p1, Ljava/lang/Iterable;

    .line 218
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/checkout/Surcharge;

    .line 198
    instance-of v2, v2, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 219
    :goto_0
    instance-of p1, v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    if-nez p1, :cond_2

    move-object v0, v1

    :cond_2
    check-cast v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    return-object v0
.end method

.method public final withPhase(Ljava/util/List;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/Surcharge;",
            ">;",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "surcharges"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phase"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/calc/constants/CalculationPhase;

    .line 190
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 191
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 190
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 192
    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    check-cast p1, Ljava/lang/Iterable;

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 216
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/checkout/Surcharge;

    .line 194
    invoke-virtual {v4}, Lcom/squareup/checkout/Surcharge;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v4

    if-ne v4, p2, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_2
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 193
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " was not one of the supported calculation phases."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 192
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
