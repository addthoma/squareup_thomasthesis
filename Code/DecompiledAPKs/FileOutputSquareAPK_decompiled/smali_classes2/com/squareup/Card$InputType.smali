.class public abstract enum Lcom/squareup/Card$InputType;
.super Ljava/lang/Enum;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "InputType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/Card$InputType$InputTypeHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/Card$InputType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/Card$InputType;

.field public static final enum A10_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

.field public static final enum GEN2_TRACK2:Lcom/squareup/Card$InputType;

.field public static final enum MANUAL:Lcom/squareup/Card$InputType;

.field public static final enum O1_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

.field public static final enum R4_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

.field public static final enum R6_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

.field public static final enum T2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

.field public static final enum X2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 283
    new-instance v0, Lcom/squareup/Card$InputType$1;

    const/4 v1, 0x0

    const-string v2, "MANUAL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/Card$InputType$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 290
    new-instance v0, Lcom/squareup/Card$InputType$2;

    const/4 v2, 0x1

    const-string v3, "GEN2_TRACK2"

    invoke-direct {v0, v3, v2}, Lcom/squareup/Card$InputType$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->GEN2_TRACK2:Lcom/squareup/Card$InputType;

    .line 297
    new-instance v0, Lcom/squareup/Card$InputType$3;

    const/4 v3, 0x2

    const-string v4, "O1_ENCRYPTED_TRACK"

    invoke-direct {v0, v4, v3}, Lcom/squareup/Card$InputType$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->O1_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    .line 304
    new-instance v0, Lcom/squareup/Card$InputType$4;

    const/4 v4, 0x3

    const-string v5, "R4_ENCRYPTED_TRACK"

    invoke-direct {v0, v5, v4}, Lcom/squareup/Card$InputType$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->R4_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    .line 311
    new-instance v0, Lcom/squareup/Card$InputType$5;

    const/4 v5, 0x4

    const-string v6, "R6_ENCRYPTED_TRACK"

    invoke-direct {v0, v6, v5}, Lcom/squareup/Card$InputType$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->R6_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    .line 318
    new-instance v0, Lcom/squareup/Card$InputType$6;

    const/4 v6, 0x5

    const-string v7, "A10_ENCRYPTED_TRACK"

    invoke-direct {v0, v7, v6}, Lcom/squareup/Card$InputType$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->A10_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    .line 325
    new-instance v0, Lcom/squareup/Card$InputType$7;

    const/4 v7, 0x6

    const-string v8, "X2_ENCRYPTED_TRACK"

    invoke-direct {v0, v8, v7}, Lcom/squareup/Card$InputType$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->X2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    .line 332
    new-instance v0, Lcom/squareup/Card$InputType$8;

    const/4 v8, 0x7

    const-string v9, "T2_ENCRYPTED_TRACK"

    invoke-direct {v0, v9, v8}, Lcom/squareup/Card$InputType$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$InputType;->T2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/Card$InputType;

    .line 281
    sget-object v9, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/Card$InputType;->GEN2_TRACK2:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$InputType;->O1_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/Card$InputType;->R4_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/Card$InputType;->R6_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/Card$InputType;->A10_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/Card$InputType;->X2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/Card$InputType;->T2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/Card$InputType;->$VALUES:[Lcom/squareup/Card$InputType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 281
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/Card$1;)V
    .locals 0

    .line 281
    invoke-direct {p0, p1, p2}, Lcom/squareup/Card$InputType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/Card$InputType;
    .locals 1

    .line 281
    const-class v0, Lcom/squareup/Card$InputType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/Card$InputType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/Card$InputType;
    .locals 1

    .line 281
    sget-object v0, Lcom/squareup/Card$InputType;->$VALUES:[Lcom/squareup/Card$InputType;

    invoke-virtual {v0}, [Lcom/squareup/Card$InputType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/Card$InputType;

    return-object v0
.end method


# virtual methods
.method abstract accept(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/Card$InputType$InputTypeHandler<",
            "TE;>;)TE;"
        }
    .end annotation
.end method
