.class public final Lcom/squareup/account/PersistentAccountService;
.super Ljava/lang/Object;
.source "PersistentAccountService.kt"

# interfaces
.implements Lcom/squareup/account/accountservice/AppAccountCache;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u001a\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00160\u00152\u0006\u0010\u0018\u001a\u00020\u0019J\u0006\u0010\u001a\u001a\u00020\u0011J\u0016\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u001c\u001a\u00020\u001dJ\u0006\u0010\u001e\u001a\u00020\u0011J\u001a\u0010\u001f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\u00160\u00152\u0006\u0010!\u001a\u00020\rJ\u000c\u0010\"\u001a\u00020#*\u00020\u0013H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u000c\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/account/PersistentAccountService;",
        "Lcom/squareup/account/accountservice/AppAccountCache;",
        "service",
        "Ldagger/Lazy;",
        "Lcom/squareup/server/account/AccountService;",
        "cache",
        "Lcom/squareup/account/LogInResponseCache;",
        "delegate",
        "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
        "singleSignOn",
        "Lcom/squareup/singlesignon/SingleSignOn;",
        "(Ldagger/Lazy;Lcom/squareup/account/LogInResponseCache;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/singlesignon/SingleSignOn;)V",
        "sessionToken",
        "",
        "getSessionToken",
        "()Ljava/lang/String;",
        "clearCache",
        "",
        "reason",
        "Lcom/squareup/account/accountservice/ClearCacheReason;",
        "create",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/CreateResponse;",
        "request",
        "Lcom/squareup/server/account/CreateBody;",
        "init",
        "onLoginResponseReceived",
        "response",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "refreshAfterProfileUpdate",
        "reportOutOfBandReader",
        "Lcom/squareup/server/SimpleResponse;",
        "postHack",
        "toClearSessionReason",
        "Lcom/squareup/singlesignon/ClearSessionReason;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cache:Lcom/squareup/account/LogInResponseCache;

.field private final delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

.field private final service:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/server/account/AccountService;",
            ">;"
        }
    .end annotation
.end field

.field private final singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/account/LogInResponseCache;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/singlesignon/SingleSignOn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/server/account/AccountService;",
            ">;",
            "Lcom/squareup/account/LogInResponseCache;",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cache"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "delegate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "singleSignOn"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/account/PersistentAccountService;->service:Ldagger/Lazy;

    iput-object p2, p0, Lcom/squareup/account/PersistentAccountService;->cache:Lcom/squareup/account/LogInResponseCache;

    iput-object p3, p0, Lcom/squareup/account/PersistentAccountService;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iput-object p4, p0, Lcom/squareup/account/PersistentAccountService;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    return-void
.end method

.method private final toClearSessionReason(Lcom/squareup/account/accountservice/ClearCacheReason;)Lcom/squareup/singlesignon/ClearSessionReason;
    .locals 1

    .line 85
    instance-of v0, p1, Lcom/squareup/account/accountservice/ClearCacheReason$SwitchToDeviceCodeSession;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/singlesignon/ClearSessionReason$SwitchToDeviceCodeSession;

    check-cast p1, Lcom/squareup/account/accountservice/ClearCacheReason$SwitchToDeviceCodeSession;

    invoke-virtual {p1}, Lcom/squareup/account/accountservice/ClearCacheReason$SwitchToDeviceCodeSession;->getDeviceCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/singlesignon/ClearSessionReason$SwitchToDeviceCodeSession;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/singlesignon/ClearSessionReason;

    goto :goto_0

    .line 86
    :cond_0
    instance-of p1, p1, Lcom/squareup/account/accountservice/ClearCacheReason$Default;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;->INSTANCE:Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;

    move-object v0, p1

    check-cast v0, Lcom/squareup/singlesignon/ClearSessionReason;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public clearCache(Lcom/squareup/account/accountservice/ClearCacheReason;)V
    .locals 1

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->cache:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->clearCache()V

    .line 79
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->clearCache()V

    .line 80
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    invoke-direct {p0, p1}, Lcom/squareup/account/PersistentAccountService;->toClearSessionReason(Lcom/squareup/account/accountservice/ClearCacheReason;)Lcom/squareup/singlesignon/ClearSessionReason;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/singlesignon/SingleSignOn;->clearSessionToken(Lcom/squareup/singlesignon/ClearSessionReason;)V

    return-void
.end method

.method public final create(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/CreateBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/CreateResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->service:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/AccountService;

    .line 58
    invoke-interface {v0, p1}, Lcom/squareup/server/account/AccountService;->create(Lcom/squareup/server/account/CreateBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 60
    sget-object v0, Lcom/squareup/account/PersistentAccountService$create$1;->INSTANCE:Lcom/squareup/account/PersistentAccountService$create$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "service.get()\n        .c\u2026opulateDefaultValue() } }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getSessionToken()Ljava/lang/String;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->cache:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0}, Lcom/squareup/account/LogInResponseCache;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cache.sessionToken"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final init()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->cache:Lcom/squareup/account/LogInResponseCache;

    iget-object v1, p0, Lcom/squareup/account/PersistentAccountService;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    check-cast v1, Lcom/squareup/accountstatus/QuietServerPreferences;

    invoke-interface {v0, v1}, Lcom/squareup/account/LogInResponseCache;->init(Lcom/squareup/accountstatus/QuietServerPreferences;)V

    return-void
.end method

.method public final onLoginResponseReceived(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 3

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 96
    invoke-static {p0, p1, v1, p1}, Lcom/squareup/account/accountservice/AppAccountCache$DefaultImpls;->clearCache$default(Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/account/accountservice/ClearCacheReason;ILjava/lang/Object;)V

    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->cache:Lcom/squareup/account/LogInResponseCache;

    invoke-interface {v0, p1}, Lcom/squareup/account/LogInResponseCache;->replaceCache(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    invoke-interface {v0, p1}, Lcom/squareup/singlesignon/SingleSignOn;->setSessionToken(Ljava/lang/String;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/account/PersistentAccountService;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {p1, p2}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->onLoginResponseReceived(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method public final refreshAfterProfileUpdate()V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    new-instance v1, Lcom/squareup/server/ErrorLoggingCallback;

    const-string v2, "Profile Updated"

    invoke-direct {v1, v2}, Lcom/squareup/server/ErrorLoggingCallback;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, v1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->status(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public final reportOutOfBandReader(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "postHack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService;->service:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/AccountService;

    .line 65
    invoke-interface {v0, p1}, Lcom/squareup/server/account/AccountService;->reportOutOfBandReader(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
