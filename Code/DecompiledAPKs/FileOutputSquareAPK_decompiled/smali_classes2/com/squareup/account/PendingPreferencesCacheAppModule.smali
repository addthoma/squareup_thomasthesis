.class public abstract Lcom/squareup/account/PendingPreferencesCacheAppModule;
.super Ljava/lang/Object;
.source "PendingPreferencesCacheAppModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideLogInResponseCache(Lcom/squareup/account/PendingPreferencesCache;)Lcom/squareup/account/LogInResponseCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
