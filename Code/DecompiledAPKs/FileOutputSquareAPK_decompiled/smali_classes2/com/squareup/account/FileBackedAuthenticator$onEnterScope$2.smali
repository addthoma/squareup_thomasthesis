.class final Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;
.super Ljava/lang/Object;
.source "FileBackedAuthenticator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/account/FileBackedAuthenticator;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a,\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/SimpleResponse;",
        "kotlin.jvm.PlatformType",
        "logOutData",
        "Lcom/squareup/account/FileBackedAuthenticator$LogOutData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/account/FileBackedAuthenticator;


# direct methods
.method constructor <init>(Lcom/squareup/account/FileBackedAuthenticator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;->this$0:Lcom/squareup/account/FileBackedAuthenticator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/account/FileBackedAuthenticator$LogOutData;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/FileBackedAuthenticator$LogOutData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "logOutData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;->this$0:Lcom/squareup/account/FileBackedAuthenticator;

    invoke-static {v0}, Lcom/squareup/account/FileBackedAuthenticator;->access$getOnLoggingOut$p(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;->this$0:Lcom/squareup/account/FileBackedAuthenticator;

    invoke-static {v0}, Lcom/squareup/account/FileBackedAuthenticator;->access$getLogoutService$p(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/squareup/server/account/LogoutService;

    move-result-object v0

    .line 73
    invoke-virtual {p1}, Lcom/squareup/account/FileBackedAuthenticator$LogOutData;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/squareup/server/account/LogoutService;->logOut(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2$result$1;->INSTANCE:Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2$result$1;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "logoutService\n          \u2026ailure(\"Remote Logout\") }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;->this$0:Lcom/squareup/account/FileBackedAuthenticator;

    invoke-virtual {p1}, Lcom/squareup/account/FileBackedAuthenticator$LogOutData;->getReason()Lcom/squareup/account/LogOutReason;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/account/FileBackedAuthenticator;->access$logOutLocally(Lcom/squareup/account/FileBackedAuthenticator;Lcom/squareup/account/LogOutReason;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/account/FileBackedAuthenticator$LogOutData;

    invoke-virtual {p0, p1}, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;->apply(Lcom/squareup/account/FileBackedAuthenticator$LogOutData;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
