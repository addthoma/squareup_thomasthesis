.class public abstract Lcom/squareup/account/LogOutReason;
.super Ljava/lang/Object;
.source "LogOutReason.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/LogOutReason$Normal;,
        Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;,
        Lcom/squareup/account/LogOutReason$SessionExpired;,
        Lcom/squareup/account/LogOutReason$SwitchToDeviceCodeSession;,
        Lcom/squareup/account/LogOutReason$Legacy;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0007\u0008\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0005\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/account/LogOutReason;",
        "",
        "analyticsName",
        "",
        "(Ljava/lang/String;)V",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "Legacy",
        "Normal",
        "SessionExpired",
        "SwitchToDeviceCodeSession",
        "UserPasswordIncorrect",
        "Lcom/squareup/account/LogOutReason$Normal;",
        "Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;",
        "Lcom/squareup/account/LogOutReason$SessionExpired;",
        "Lcom/squareup/account/LogOutReason$SwitchToDeviceCodeSession;",
        "Lcom/squareup/account/LogOutReason$Legacy;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analyticsName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/account/LogOutReason;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/account/LogOutReason;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/account/LogOutReason;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
