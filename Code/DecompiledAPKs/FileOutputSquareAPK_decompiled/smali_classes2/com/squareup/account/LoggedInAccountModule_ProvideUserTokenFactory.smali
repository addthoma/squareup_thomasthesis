.class public final Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;
.super Ljava/lang/Object;
.source "LoggedInAccountModule_ProvideUserTokenFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;->userTokenProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;

    invoke-direct {v0, p0}, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/account/LoggedInAccountModule;->provideUserToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/account/LoggedInAccountModule_ProvideUserTokenFactory;->provideUserToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
