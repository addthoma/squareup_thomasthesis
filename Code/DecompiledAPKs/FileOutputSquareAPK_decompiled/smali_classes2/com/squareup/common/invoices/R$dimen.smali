.class public final Lcom/squareup/common/invoices/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final bulk_settle_button_badge_center_offset:I = 0x7f07008b

.field public static final cart_header_height:I = 0x7f0700b5

.field public static final checkout_jumbo_button_height:I = 0x7f0700c9

.field public static final checkout_tablet_row_height:I = 0x7f0700ca

.field public static final color_picker_height:I = 0x7f0700cc

.field public static final edit_entry_label_height:I = 0x7f070145

.field public static final edit_entry_label_width:I = 0x7f070147

.field public static final edit_stroke_width:I = 0x7f070149

.field public static final grid_label_text_size:I = 0x7f070184

.field public static final grid_placeholder_text_max_size:I = 0x7f070185

.field public static final home_item_badge_horizontal_spacing:I = 0x7f070199

.field public static final home_item_badge_vertical_spacing:I = 0x7f07019a

.field public static final invoice_date_picker_cell_gap:I = 0x7f0701cc

.field public static final invoice_date_picker_side_padding:I = 0x7f0701cd

.field public static final invoice_timeline_left_pad:I = 0x7f0701d4

.field public static final sample_image_tile_height:I = 0x7f0704a1

.field public static final sample_text_tile_height:I = 0x7f0704a2

.field public static final sample_text_tile_width:I = 0x7f0704a3

.field public static final sample_tile_container_height:I = 0x7f0704a4

.field public static final sample_tile_gap:I = 0x7f0704a5

.field public static final subsection_header_bottom_padding:I = 0x7f070504

.field public static final subsection_header_top_padding:I = 0x7f070505


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
