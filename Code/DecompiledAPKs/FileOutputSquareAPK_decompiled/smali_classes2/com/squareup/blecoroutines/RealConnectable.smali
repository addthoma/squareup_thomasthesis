.class public final Lcom/squareup/blecoroutines/RealConnectable;
.super Ljava/lang/Object;
.source "RealConnectable.kt"

# interfaces
.implements Lcom/squareup/blecoroutines/Connectable;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J!\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J\u0019\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u000fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/RealConnectable;",
        "Lcom/squareup/blecoroutines/Connectable;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "tmnTimings",
        "Lcom/squareup/tmn/TmnTimings;",
        "(Lkotlinx/coroutines/CoroutineDispatcher;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/tmn/TmnTimings;)V",
        "bluetoothGatt",
        "Landroid/bluetooth/BluetoothGatt;",
        "callbackBroker",
        "Lcom/squareup/blecoroutines/GattCallbackBroker;",
        "onDisconnection",
        "Lkotlinx/coroutines/CompletableDeferred;",
        "",
        "connectGatt",
        "Lcom/squareup/blecoroutines/Connection;",
        "context",
        "Landroid/content/Context;",
        "autoConnect",
        "",
        "(Landroid/content/Context;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "disconnectWithTimeout",
        "timeoutMillis",
        "",
        "(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "readRssi",
        "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private final callbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

.field private final device:Landroid/bluetooth/BluetoothDevice;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final onDisconnection:Lkotlinx/coroutines/CompletableDeferred;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/CompletableDeferred<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final tmnTimings:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/CoroutineDispatcher;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/tmn/TmnTimings;)V
    .locals 1

    const-string v0, "mainDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tmnTimings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object p2, p0, Lcom/squareup/blecoroutines/RealConnectable;->device:Landroid/bluetooth/BluetoothDevice;

    iput-object p3, p0, Lcom/squareup/blecoroutines/RealConnectable;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 21
    invoke-static {p1, p2, p1}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    .line 22
    new-instance p1, Lcom/squareup/blecoroutines/GattCallbackBroker;

    iget-object p2, p0, Lcom/squareup/blecoroutines/RealConnectable;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p2

    const-string p3, "device.address"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p0, Lcom/squareup/blecoroutines/RealConnectable;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    invoke-direct {p1, p2, p3}, Lcom/squareup/blecoroutines/GattCallbackBroker;-><init>(Ljava/lang/String;Lkotlinx/coroutines/CompletableDeferred;)V

    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable;->callbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    return-void
.end method

.method public static final synthetic access$getBluetoothGatt$p(Lcom/squareup/blecoroutines/RealConnectable;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-object p0
.end method

.method public static final synthetic access$getCallbackBroker$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/blecoroutines/GattCallbackBroker;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable;->callbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    return-object p0
.end method

.method public static final synthetic access$getDevice$p(Lcom/squareup/blecoroutines/RealConnectable;)Landroid/bluetooth/BluetoothDevice;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable;->device:Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method

.method public static final synthetic access$getOnDisconnection$p(Lcom/squareup/blecoroutines/RealConnectable;)Lkotlinx/coroutines/CompletableDeferred;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    return-object p0
.end method

.method public static final synthetic access$getTmnTimings$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/tmn/TmnTimings;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    return-object p0
.end method

.method public static final synthetic access$setBluetoothGatt$p(Lcom/squareup/blecoroutines/RealConnectable;Landroid/bluetooth/BluetoothGatt;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-void
.end method


# virtual methods
.method public connectGatt(Landroid/content/Context;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/blecoroutines/Connection;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;-><init>(Lcom/squareup/blecoroutines/RealConnectable;Landroid/content/Context;ZLkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1, p3}, Lkotlinx/coroutines/BuildersKt;->withContext(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public disconnectWithTimeout(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/blecoroutines/RealConnectable$disconnectWithTimeout$2;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/squareup/blecoroutines/RealConnectable$disconnectWithTimeout$2;-><init>(Lcom/squareup/blecoroutines/RealConnectable;JLkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1, p3}, Lkotlinx/coroutines/BuildersKt;->withContext(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public readRssi(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/blecoroutines/RealConnectable$readRssi$2;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/blecoroutines/RealConnectable$readRssi$2;-><init>(Lcom/squareup/blecoroutines/RealConnectable;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->withContext(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
