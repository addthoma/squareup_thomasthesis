.class final Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "RealConnectable.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlin/Unit;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/blecoroutines/RealConnection;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/blecoroutines/RealConnection;",
        "it",
        "",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/blecoroutines/RealConnectable$connectGatt$2$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $gatt$inlined:Landroid/bluetooth/BluetoothGatt;

.field label:I

.field private p$0:Lkotlin/Unit;

.field final synthetic this$0:Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;Landroid/bluetooth/BluetoothGatt;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->this$0:Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iput-object p3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->$gatt$inlined:Landroid/bluetooth/BluetoothGatt;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;

    iget-object v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->this$0:Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->$gatt$inlined:Landroid/bluetooth/BluetoothGatt;

    invoke-direct {v0, p2, v1, v2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;Landroid/bluetooth/BluetoothGatt;)V

    check-cast p1, Lkotlin/Unit;

    iput-object p1, v0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->p$0:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 39
    iget v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->p$0:Lkotlin/Unit;

    .line 40
    new-instance p1, Lcom/squareup/blecoroutines/RealConnection;

    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->$gatt$inlined:Landroid/bluetooth/BluetoothGatt;

    iget-object v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->this$0:Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iget-object v1, v1, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v1}, Lcom/squareup/blecoroutines/RealConnectable;->access$getCallbackBroker$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/blecoroutines/GattCallbackBroker;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;->this$0:Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iget-object v2, v2, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v2}, Lcom/squareup/blecoroutines/RealConnectable;->access$getTmnTimings$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/blecoroutines/RealConnection;-><init>(Landroid/bluetooth/BluetoothGatt;Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
