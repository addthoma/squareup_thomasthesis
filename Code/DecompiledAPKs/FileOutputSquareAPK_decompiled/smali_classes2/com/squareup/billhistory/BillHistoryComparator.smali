.class public final Lcom/squareup/billhistory/BillHistoryComparator;
.super Ljava/lang/Object;
.source "BillHistoryComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/billhistory/model/BillHistory;",
        ">;"
    }
.end annotation


# static fields
.field public static final COMPARATOR:Lcom/squareup/billhistory/BillHistoryComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/billhistory/BillHistoryComparator;

    invoke-direct {v0}, Lcom/squareup/billhistory/BillHistoryComparator;-><init>()V

    sput-object v0, Lcom/squareup/billhistory/BillHistoryComparator;->COMPARATOR:Lcom/squareup/billhistory/BillHistoryComparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isPending(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 31
    iget-boolean v0, p1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public compare(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/BillHistory;)I
    .locals 3

    const/4 v0, 0x0

    if-ne p1, p2, :cond_0

    return v0

    .line 18
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/BillHistoryComparator;->isPending(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v1

    invoke-direct {p0, p2}, Lcom/squareup/billhistory/BillHistoryComparator;->isPending(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v2

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/BillHistoryComparator;->isPending(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 22
    iget-object p2, p2, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-virtual {p2, p1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    :cond_3
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    check-cast p2, Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/BillHistoryComparator;->compare(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/BillHistory;)I

    move-result p1

    return p1
.end method
