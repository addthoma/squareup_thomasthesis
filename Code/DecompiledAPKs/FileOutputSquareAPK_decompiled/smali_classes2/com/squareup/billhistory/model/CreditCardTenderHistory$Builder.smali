.class public Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "CreditCardTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/CreditCardTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/CreditCardTenderHistory;",
        "Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private applicationId:Ljava/lang/String;

.field private applicationPreferredName:Ljava/lang/String;

.field private authorizationCode:Ljava/lang/String;

.field private brand:Lcom/squareup/Card$Brand;

.field private buyerName:Ljava/lang/String;

.field private buyerSelectedAccountName:Ljava/lang/String;

.field private cardSuffix:Ljava/lang/String;

.field private cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field private entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field private felicaMaskedCardNumber:Ljava/lang/String;

.field private felicaTerminalId:Ljava/lang/String;

.field private tip:Lcom/squareup/protos/common/Money;

.field private tipPercentage:Lcom/squareup/util/Percentage;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/Card$Brand;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->brand:Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationPreferredName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerSelectedAccountName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaMaskedCardNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaTerminalId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardSuffix:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/util/Percentage;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->authorizationCode:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public applicationId(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationId:Ljava/lang/String;

    return-object p0
.end method

.method public applicationPreferredName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationPreferredName:Ljava/lang/String;

    return-object p0
.end method

.method public authorizationCode(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->authorizationCode:Ljava/lang/String;

    return-object p0
.end method

.method public brand(Lcom/squareup/Card$Brand;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->brand:Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/billhistory/model/CreditCardTenderHistory;
    .locals 2

    .line 135
    new-instance v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;-><init>(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;Lcom/squareup/billhistory/model/CreditCardTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public buyerName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerName:Ljava/lang/String;

    return-object p0
.end method

.method public buyerSelectedAccountName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerSelectedAccountName:Ljava/lang/String;

    return-object p0
.end method

.method public cardSuffix(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardSuffix:Ljava/lang/String;

    return-object p0
.end method

.method public cardholderVerificationMethod(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0
.end method

.method public entryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public felicaBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method public felicaMaskedCardNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaMaskedCardNumber:Ljava/lang/String;

    return-object p0
.end method

.method public felicaTerminalId(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaTerminalId:Ljava/lang/String;

    return-object p0
.end method

.method public from(Lcom/squareup/billhistory/model/CreditCardTenderHistory;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 1

    .line 46
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->brand:Lcom/squareup/Card$Brand;

    .line 47
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 48
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaMaskedCardNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaMaskedCardNumber:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaTerminalId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaTerminalId:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardSuffix:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardSuffix:Ljava/lang/String;

    .line 51
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerName:Ljava/lang/String;

    .line 52
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    .line 53
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tipPercentage:Lcom/squareup/util/Percentage;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    .line 54
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->authorizationCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->authorizationCode:Ljava/lang/String;

    .line 55
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationPreferredName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationPreferredName:Ljava/lang/String;

    .line 56
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerSelectedAccountName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerSelectedAccountName:Ljava/lang/String;

    .line 57
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationId:Ljava/lang/String;

    .line 58
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 59
    iget-object v0, p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    return-object p1
.end method

.method public bridge synthetic from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/CreditCardTenderHistory;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public bridge synthetic tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object p0
.end method

.method public bridge synthetic tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method
