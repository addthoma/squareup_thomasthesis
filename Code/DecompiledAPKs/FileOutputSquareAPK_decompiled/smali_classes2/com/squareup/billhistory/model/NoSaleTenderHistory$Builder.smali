.class public Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "NoSaleTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/NoSaleTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/NoSaleTenderHistory;",
        "Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
    .locals 5

    .line 24
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    return-object p1

    .line 25
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    .line 26
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " amount must be $0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public build()Lcom/squareup/billhistory/model/NoSaleTenderHistory;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory;-><init>(Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;Lcom/squareup/billhistory/model/NoSaleTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
    .locals 2

    const-wide/16 v0, 0x0

    .line 32
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    return-object p0
.end method

.method public from(Lcom/squareup/billhistory/model/NoSaleTenderHistory;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
    .locals 0

    .line 20
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    return-object p1
.end method

.method public bridge synthetic from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/NoSaleTenderHistory;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method
