.class public Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;
.super Ljava/lang/Object;
.source "BillHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/BillHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemizationsNote"
.end annotation


# static fields
.field public static final EMPTY:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

.field private static final EMPTY_NOTE:Ljava/lang/String; = ""


# instance fields
.field private final components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;",
            ">;"
        }
    .end annotation
.end field

.field private localizedNote:Ljava/lang/String;

.field private final note:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1150
    new-instance v0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;-><init>()V

    sput-object v0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->EMPTY:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 1167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->components:Ljava/util/List;

    const-string v0, ""

    .line 1169
    iput-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->note:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;",
            ">;)V"
        }
    .end annotation

    .line 1159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160
    iput-object p2, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->components:Ljava/util/List;

    .line 1162
    new-instance v0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$AENjN41ywR52_KRTnRW8rq1FkFY;

    invoke-direct {v0, p1}, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$AENjN41ywR52_KRTnRW8rq1FkFY;-><init>(Lcom/squareup/util/Res;)V

    .line 1164
    invoke-static {p1, v0, p2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->createCombinedNote(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->note:Ljava/lang/String;

    return-void
.end method

.method private static createCombinedNote(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1238
    sget v0, Lcom/squareup/utilities/R$string;->list_pattern_long_two_separator:I

    .line 1239
    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    .line 1240
    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/utilities/R$string;->list_pattern_long_final_separator:I

    .line 1241
    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 1238
    invoke-static {v0, v1, p0}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    .line 1244
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 1245
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;

    iget-boolean v1, v1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isCustomAmount:Z

    if-eqz v1, :cond_1

    .line 1246
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;

    iget-object v1, v1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->notes:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1247
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->itemName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    goto :goto_1

    .line 1248
    :cond_1
    :goto_0
    invoke-virtual {p0, p2, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method private static createComponentNote(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;
    .locals 5

    .line 1258
    invoke-static {p0, p1}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->toNameNotesOrDefaultName(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;

    move-result-object v0

    .line 1261
    iget-object v1, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantity:Ljava/math/BigDecimal;

    .line 1262
    iget-boolean v2, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isUnitPriced:Z

    const-string v3, "quantity"

    const-string v4, "name"

    if-eqz v2, :cond_0

    .line 1263
    iget v2, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantityPrecision:I

    .line 1264
    invoke-static {v1, v2}, Lcom/squareup/quantity/ItemQuantities;->applyQuantityPrecision(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1265
    sget v2, Lcom/squareup/common/strings/R$string;->payment_note_item_name_quantity_unit:I

    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1266
    invoke-virtual {p0, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1267
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->unitAbbreviation:Ljava/lang/String;

    const-string v0, "unit"

    .line 1268
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1269
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 1270
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 1271
    :cond_0
    sget-object p1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v1, p1}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1272
    sget p1, Lcom/squareup/common/strings/R$string;->payment_note_item_name_quantity:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1273
    invoke-virtual {p0, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1274
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1275
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 1276
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    return-object v0
.end method

.method private static createLocalizedComponentNote(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;
    .locals 3

    .line 1288
    invoke-static {p0, p2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->toNameNotesOrDefaultName(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;

    move-result-object v0

    .line 1291
    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantity:Ljava/math/BigDecimal;

    .line 1292
    iget-boolean v2, p2, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isUnitPriced:Z

    if-nez v2, :cond_1

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v1, v2}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    return-object v0

    .line 1293
    :cond_1
    :goto_0
    iget v2, p2, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantityPrecision:I

    .line 1294
    invoke-virtual {p1, v1, v2}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->unitAbbreviation:Ljava/lang/String;

    .line 1295
    invoke-virtual {p1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 1296
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 1297
    sget p2, Lcom/squareup/common/strings/R$string;->payment_note_item_name_quantity:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "name"

    .line 1298
    invoke-virtual {p0, p2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "quantity"

    .line 1299
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 1300
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 1301
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$localizedNote$1(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/CharSequence;
    .locals 0

    .line 1224
    invoke-static {p0, p1, p2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->createLocalizedComponentNote(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/CharSequence;
    .locals 0

    .line 1163
    invoke-static {p0, p1}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->createComponentNote(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static toNameNotesOrDefaultName(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/String;
    .locals 3

    .line 1315
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->itemName:Ljava/lang/String;

    .line 1316
    iget-object v1, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->notes:Ljava/lang/String;

    .line 1317
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean p1, p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isCustomAmount:Z

    if-eqz p1, :cond_1

    :cond_0
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    move-object v0, v1

    .line 1320
    :cond_1
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, ""

    .line 1322
    invoke-static {p0, p1}, Lcom/squareup/payment/OrderVariationNames;->getVariableDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public getNote()Ljava/lang/String;
    .locals 2

    .line 1204
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->note:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public localizedNote(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/String;
    .locals 1

    .line 1218
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->note:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 1222
    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->localizedNote:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1223
    new-instance v0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;

    invoke-direct {v0, p1, p2}, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;-><init>(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 1225
    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->components:Ljava/util/List;

    invoke-static {p1, v0, p2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->createCombinedNote(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->localizedNote:Ljava/lang/String;

    .line 1228
    :cond_1
    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->localizedNote:Ljava/lang/String;

    return-object p1
.end method
