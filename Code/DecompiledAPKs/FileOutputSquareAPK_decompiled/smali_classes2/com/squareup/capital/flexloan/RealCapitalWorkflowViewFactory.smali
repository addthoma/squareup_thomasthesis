.class public final Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealCapitalWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 13
    sget-object v1, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 14
    sget-object v1, Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 15
    sget-object v1, Lcom/squareup/capital/flexloan/offer/CapitalOfferScreenLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/offer/CapitalOfferScreenLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 16
    sget-object v1, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 17
    sget-object v1, Lcom/squareup/capital/flexloan/error/CapitalErrorLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/error/CapitalErrorLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 18
    sget-object v1, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$Binding;->INSTANCE:Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$Binding;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
