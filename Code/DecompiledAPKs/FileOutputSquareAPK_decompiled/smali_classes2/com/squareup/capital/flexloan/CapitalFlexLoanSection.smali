.class public abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;
.super Lcom/squareup/applet/AppletSection;
.source "CapitalFlexLoanSection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\t\u001a\u00020\nH&R\u0012\u0010\u0005\u001a\u00020\u0006X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "Lcom/squareup/applet/AppletSection;",
        "sectionAccess",
        "Lcom/squareup/applet/SectionAccess;",
        "(Lcom/squareup/applet/SectionAccess;)V",
        "appletSectionListEntry",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "getAppletSectionListEntry",
        "()Lcom/squareup/applet/AppletSectionsListEntry;",
        "logSectionClick",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/applet/SectionAccess;)V
    .locals 1

    const-string v0, "sectionAccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public abstract getAppletSectionListEntry()Lcom/squareup/applet/AppletSectionsListEntry;
.end method

.method public abstract logSectionClick()V
.end method
