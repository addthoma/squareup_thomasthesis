.class public final Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;
.super Ljava/lang/Object;
.source "CapitalPlanLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Binding;,
        Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCapitalPlanLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CapitalPlanLayoutRunner.kt\ncom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,187:1\n1103#2,7:188\n*E\n*S KotlinDebug\n*F\n+ 1 CapitalPlanLayoutRunner.kt\ncom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner\n*L\n77#1,7:188\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 42\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u000234B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010 \u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u001e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010\'\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020\u0002H\u0002J\u0012\u0010)\u001a\u00020*2\u0008\u0008\u0001\u0010+\u001a\u00020*H\u0002J\u001a\u0010,\u001a\u00020\u001d2\u0006\u0010-\u001a\u00020\u000c2\u0008\u0008\u0001\u0010+\u001a\u00020*H\u0002J\u0018\u0010.\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020\u00022\u0006\u0010/\u001a\u000200H\u0016J\u0012\u00101\u001a\u0002022\u0008\u0008\u0001\u0010+\u001a\u00020*H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\n \u0014*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n \u0014*\u0004\u0018\u00010\u00160\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "currentPlan",
        "Lcom/squareup/noho/NohoLabel;",
        "outstandingAmountValue",
        "outstandingBulletImage",
        "Landroid/widget/ImageView;",
        "paymentAmountValue",
        "paymentBulletImage",
        "paymentMainText",
        "paymentRow",
        "Landroid/widget/LinearLayout;",
        "paymentSubText",
        "percentagePaid",
        "kotlin.jvm.PlatformType",
        "pieChart",
        "Lcom/squareup/charts/piechart/PieChart;",
        "planBulletImage",
        "showBackButton",
        "",
        "viewInSquareButton",
        "Lcom/squareup/noho/NohoButton;",
        "buildOutstandingAmount",
        "",
        "planScreenData",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;",
        "buildPaymentAmount",
        "buildPieChartSlices",
        "",
        "Lcom/squareup/charts/piechart/Slice;",
        "thickness",
        "",
        "buildTotalPaidRow",
        "configureActionBar",
        "rendering",
        "getColor",
        "",
        "color",
        "setImageColor",
        "imageContainer",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "solidSliceColor",
        "Lcom/squareup/charts/piechart/SliceColor;",
        "Binding",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Companion;

.field private static final GAP_THICKNESS:F = 0.5f

.field private static final SLICE_THICKNESS:F = 15.0f


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final currentPlan:Lcom/squareup/noho/NohoLabel;

.field private final outstandingAmountValue:Lcom/squareup/noho/NohoLabel;

.field private final outstandingBulletImage:Landroid/widget/ImageView;

.field private final paymentAmountValue:Lcom/squareup/noho/NohoLabel;

.field private final paymentBulletImage:Landroid/widget/ImageView;

.field private final paymentMainText:Lcom/squareup/noho/NohoLabel;

.field private final paymentRow:Landroid/widget/LinearLayout;

.field private final paymentSubText:Lcom/squareup/noho/NohoLabel;

.field private final percentagePaid:Lcom/squareup/noho/NohoLabel;

.field private final pieChart:Lcom/squareup/charts/piechart/PieChart;

.field private final planBulletImage:Landroid/widget/ImageView;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;

.field private final viewInSquareButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->Companion:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    .line 39
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 40
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->showBackButton:Z

    .line 41
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_action_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->viewInSquareButton:Lcom/squareup/noho/NohoButton;

    .line 42
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->pie_chart:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/charts/piechart/PieChart;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->pieChart:Lcom/squareup/charts/piechart/PieChart;

    .line 43
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->percentage_paid:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->percentagePaid:Lcom/squareup/noho/NohoLabel;

    .line 46
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->current_plan_value_image:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->planBulletImage:Landroid/widget/ImageView;

    .line 47
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->current_plan_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->currentPlan:Lcom/squareup/noho/NohoLabel;

    .line 50
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_payment_amount_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentRow:Landroid/widget/LinearLayout;

    .line 51
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_payment_amount_image:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentBulletImage:Landroid/widget/ImageView;

    .line 52
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_payment_amount_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentMainText:Lcom/squareup/noho/NohoLabel;

    .line 53
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_payment_amount_sub_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentSubText:Lcom/squareup/noho/NohoLabel;

    .line 54
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_payment_amount_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentAmountValue:Lcom/squareup/noho/NohoLabel;

    .line 57
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_amount_outstanding_image:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->outstandingBulletImage:Landroid/widget/ImageView;

    .line 58
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->plan_amount_outstanding_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->outstandingAmountValue:Lcom/squareup/noho/NohoLabel;

    return-void
.end method

.method public static final synthetic access$buildPieChartSlices(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;F)Ljava/util/List;
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->buildPieChartSlices(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;F)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$solidSliceColor(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;I)Lcom/squareup/charts/piechart/SliceColor;
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->solidSliceColor(I)Lcom/squareup/charts/piechart/SliceColor;

    move-result-object p0

    return-object p0
.end method

.method private final buildOutstandingAmount(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->outstandingAmountValue:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getOutstandingAmountText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->outstandingBulletImage:Landroid/widget/ImageView;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_outstanding_due_bullet:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->setImageColor(Landroid/widget/ImageView;I)V

    return-void
.end method

.method private final buildPaymentAmount(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V
    .locals 3

    .line 103
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getDisplayAmountDue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueDateColor()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->getColor(I)I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentMainText:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentSubText:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueDateText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentSubText:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 110
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentAmountValue:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueAmountText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentAmountValue:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentBulletImage:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueBulletColor()I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->setImageColor(Landroid/widget/ImageView;I)V

    goto :goto_0

    .line 115
    :cond_0
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->paymentRow:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method private final buildPieChartSlices(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;F)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;",
            "F)",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;"
        }
    .end annotation

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 148
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/charts/piechart/Slice;

    .line 149
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPercentPaid()F

    move-result v3

    .line 150
    sget v4, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_total_paid_bullet:I

    invoke-direct {p0, v4}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->solidSliceColor(I)Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v4

    .line 148
    invoke-direct {v2, v3, p2, v4}, Lcom/squareup/charts/piechart/Slice;-><init>(FFLcom/squareup/charts/piechart/SliceColor;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getDisplayAmountDue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    new-instance v2, Lcom/squareup/charts/piechart/Slice;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPercentDue()F

    move-result v3

    .line 157
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPaymentDueBulletColor()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->solidSliceColor(I)Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v4

    .line 155
    invoke-direct {v2, v3, p2, v4}, Lcom/squareup/charts/piechart/Slice;-><init>(FFLcom/squareup/charts/piechart/SliceColor;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v2, Lcom/squareup/charts/piechart/Slice;

    .line 162
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPercentOutstanding()F

    move-result p1

    .line 163
    sget v3, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_outstanding_due_bullet:I

    invoke-direct {p0, v3}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->solidSliceColor(I)Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v3

    .line 161
    invoke-direct {v2, p1, p2, v3}, Lcom/squareup/charts/piechart/Slice;-><init>(FFLcom/squareup/charts/piechart/SliceColor;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method private final buildTotalPaidRow(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->currentPlan:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getTotalPaid()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->planBulletImage:Landroid/widget/ImageView;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_total_paid_bullet:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->setImageColor(Landroid/widget/ImageView;I)V

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V
    .locals 4

    .line 141
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 134
    iget-boolean v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->showBackButton:Z

    if-eqz v2, :cond_0

    .line 135
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 137
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 140
    :goto_0
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital:I

    invoke-direct {p1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final getColor(I)I
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method private final setImageColor(Landroid/widget/ImageView;I)V
    .locals 2

    .line 128
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {p0, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->getColor(I)I

    move-result p2

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p2, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    check-cast v0, Landroid/graphics/ColorFilter;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method

.method private final solidSliceColor(I)Lcom/squareup/charts/piechart/SliceColor;
    .locals 1

    .line 172
    new-instance v0, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->getColor(I)I

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/charts/piechart/SliceColor$SolidColor;-><init>(I)V

    check-cast v0, Lcom/squareup/charts/piechart/SliceColor;

    return-object v0
.end method


# virtual methods
.method public showRendering(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->configureActionBar(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V

    .line 65
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$1;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 68
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->buildTotalPaidRow(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->buildPaymentAmount(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->buildOutstandingAmount(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)V

    .line 76
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->viewInSquareButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getButtonLinkText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->viewInSquareButton:Lcom/squareup/noho/NohoButton;

    check-cast p2, Landroid/view/View;

    .line 188
    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->percentagePaid:Lcom/squareup/noho/NohoLabel;

    const-string v0, "percentagePaid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->getPercentPaidText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->pieChart:Lcom/squareup/charts/piechart/PieChart;

    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/charts/piechart/PieChart;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->showRendering(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
