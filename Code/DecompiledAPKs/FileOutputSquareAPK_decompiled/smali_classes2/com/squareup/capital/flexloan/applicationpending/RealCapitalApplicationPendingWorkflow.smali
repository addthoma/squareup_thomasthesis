.class public final Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealCapitalApplicationPendingWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;",
        ">;",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalApplicationPendingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalApplicationPendingWorkflow.kt\ncom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,25:1\n179#2,3:26\n199#2,4:29\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalApplicationPendingWorkflow.kt\ncom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow\n*L\n16#1,3:26\n16#1,4:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J)\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00032\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u000cH\u0016\u00a2\u0006\u0002\u0010\rR\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;",
        "()V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final finish:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 29
    new-instance v0, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow$$special$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow$$special$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    .line 28
    iput-object v0, p0, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance p1, Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;

    new-instance v0, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow$render$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;

    move-result-object p1

    return-object p1
.end method
