.class public final Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "CapitalFlexLoanSectionAccess.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "capitalFlexLoanFeatureSupport",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;)V",
        "determineVisibility",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final capitalFlexLoanFeatureSupport:Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanFeatureSupport"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->capitalFlexLoanFeatureSupport:Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAPITAL_CAN_MANAGE_FLEX_OFFER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->capitalFlexLoanFeatureSupport:Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;->getSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isOwnerLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
