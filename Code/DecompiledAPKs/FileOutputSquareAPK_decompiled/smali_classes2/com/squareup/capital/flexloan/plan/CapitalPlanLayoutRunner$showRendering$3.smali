.class final Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;
.super Lkotlin/jvm/internal/Lambda;
.source "CapitalPlanLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->showRendering(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/charts/piechart/PieChart$Update;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCapitalPlanLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CapitalPlanLayoutRunner.kt\ncom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3\n*L\n1#1,187:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/charts/piechart/PieChart$Update;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

.field final synthetic this$0:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->this$0:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->$rendering:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/charts/piechart/PieChart$Update;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->invoke(Lcom/squareup/charts/piechart/PieChart$Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/charts/piechart/PieChart$Update;)V
    .locals 7

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart$Update;->getBackgroundSlice()Lcom/squareup/charts/piechart/Slice;

    move-result-object v1

    .line 83
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->this$0:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$color;->capital_hero_content_color:I

    invoke-static {v0, v2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->access$solidSliceColor(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;I)Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    .line 82
    invoke-static/range {v1 .. v6}, Lcom/squareup/charts/piechart/Slice;->copy$default(Lcom/squareup/charts/piechart/Slice;FFLcom/squareup/charts/piechart/SliceColor;ILjava/lang/Object;)Lcom/squareup/charts/piechart/Slice;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/charts/piechart/PieChart$Update;->setBackgroundSlice(Lcom/squareup/charts/piechart/Slice;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->this$0:Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;

    .line 87
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner$showRendering$3;->$rendering:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

    invoke-virtual {v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;->getPlanScreenData()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object v1

    const/high16 v2, 0x41700000    # 15.0f

    .line 86
    invoke-static {v0, v1, v2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;->access$buildPieChartSlices(Lcom/squareup/capital/flexloan/plan/CapitalPlanLayoutRunner;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;F)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/charts/piechart/PieChart$Update;->setSlices(Ljava/util/List;)V

    .line 91
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart$Update;->getRenderer()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;

    if-eqz v0, :cond_2

    .line 92
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart$Update;->getSlices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x1

    if-le p1, v1, :cond_1

    const/high16 p1, 0x3f000000    # 0.5f

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->setGap(F)V

    :cond_2
    return-void
.end method
