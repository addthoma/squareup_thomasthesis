.class public Lcom/squareup/autocapture/AutoCaptureJob;
.super Lcom/squareup/backgroundjob/ExecutorBackgroundJob;
.source "AutoCaptureJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;,
        Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;
    }
.end annotation


# static fields
.field public static final QUALIFIED_NAME:Ljava/lang/String;

.field public static final QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

.field private static final QUICK_DEBUG_NOTIFICATION_TEXT_FORMAT_STRING:Ljava/lang/String; = "Quick auto-capture job scheduled to be run in %d min"

.field private static final QUICK_DEBUG_NOTIFICATION_TITLE:Ljava/lang/String; = "Debug: Quick Auto-Capture"

.field public static final SLOW_AUTO_CAPTURE_TAG:Ljava/lang/String;

.field private static final SLOW_DEBUG_NOTIFICATION_TEXT_FORMAT_STRING:Ljava/lang/String; = "Slow auto-capture job scheduled to be run in ~%d min"

.field private static final SLOW_DEBUG_NOTIFICATION_TITLE:Ljava/lang/String; = "Debug: Slow Auto-Capture"

.field static final UNIQUE_KEY:Ljava/lang/String;

.field private static final WAKE_LOCK_TAG:Ljava/lang/String;


# instance fields
.field private final appDelegate:Lcom/squareup/AppDelegate;

.field private final autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

.field private final autoVoid:Lcom/squareup/payment/AutoVoid;

.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private final lazyTransaction:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    const-class v0, Lcom/squareup/autocapture/AutoCaptureJob;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->QUALIFIED_NAME:Ljava/lang/String;

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":quick.auto.capture.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":slow.auto.capture.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->SLOW_AUTO_CAPTURE_TAG:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":wake.lock.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":unique.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->UNIQUE_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/payment/AutoVoid;Ldagger/Lazy;Lcom/squareup/AppDelegate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/payment/AutoVoid;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Lcom/squareup/AppDelegate;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;)V

    .line 59
    iput-object p3, p0, Lcom/squareup/autocapture/AutoCaptureJob;->bus:Lcom/squareup/badbus/BadEventSink;

    .line 60
    iput-object p4, p0, Lcom/squareup/autocapture/AutoCaptureJob;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 61
    iput-object p6, p0, Lcom/squareup/autocapture/AutoCaptureJob;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    .line 62
    iput-object p7, p0, Lcom/squareup/autocapture/AutoCaptureJob;->autoVoid:Lcom/squareup/payment/AutoVoid;

    .line 63
    iput-object p8, p0, Lcom/squareup/autocapture/AutoCaptureJob;->lazyTransaction:Ldagger/Lazy;

    .line 64
    iput-object p9, p0, Lcom/squareup/autocapture/AutoCaptureJob;->appDelegate:Lcom/squareup/AppDelegate;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/autocapture/AutoCaptureJob;)Ljava/lang/String;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureJob;->logName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private logName()Ljava/lang/String;
    .locals 1

    .line 161
    invoke-static {p0}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static quickAutoCaptureRequest(JLjava/lang/String;)Lcom/evernote/android/job/JobRequest;
    .locals 3

    .line 75
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 76
    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->UNIQUE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    new-instance p2, Lcom/evernote/android/job/JobRequest$Builder;

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

    invoke-direct {p2, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2, p0, p1}, Lcom/evernote/android/job/JobRequest$Builder;->setExact(J)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    const/4 v1, 0x1

    .line 89
    invoke-virtual {p2, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 90
    invoke-virtual {p2, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 91
    invoke-virtual {p2}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    .line 93
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 94
    invoke-virtual {v2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v1, p1

    const-string p0, "Quick auto-capture job scheduled to be run in %d min"

    .line 93
    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 95
    sget-object p1, Lcom/squareup/autocapture/AutoCaptureJob;->QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    const-string v0, "Debug: Quick Auto-Capture"

    invoke-static {p2, p1, v0, p0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;ILjava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method public static slowAutoCaptureRequest(JLjava/lang/String;)Lcom/evernote/android/job/JobRequest;
    .locals 4

    .line 106
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 107
    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->UNIQUE_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance p2, Lcom/evernote/android/job/JobRequest$Builder;

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureJob;->SLOW_AUTO_CAPTURE_TAG:Ljava/lang/String;

    invoke-direct {p2, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 115
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, p0

    invoke-virtual {p2, p0, p1, v1, v2}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 116
    invoke-virtual {p2, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 117
    invoke-virtual {p2}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    .line 119
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 120
    invoke-virtual {v2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v1, p1

    const-string p0, "Slow auto-capture job scheduled to be run in ~%d min"

    .line 119
    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "Debug: Slow Auto-Capture"

    .line 121
    invoke-static {p2, p1, p0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 8

    .line 127
    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object p1

    sget-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->UNIQUE_KEY:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 129
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob;->appDelegate:Lcom/squareup/AppDelegate;

    invoke-interface {v0}, Lcom/squareup/AppDelegate;->isLoggedIn()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;-><init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureJob$1;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 131
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureJob;->logName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const-string v7, "%s received %s"

    invoke-static {v7, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v0}, Lcom/squareup/autocapture/AutoCaptureControl;->stop()V

    .line 139
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob;->lazyTransaction:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    .line 140
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    if-nez v2, :cond_1

    .line 142
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-array v2, v3, [Ljava/lang/Object;

    .line 143
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureJob;->logName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasLostPayment()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v2, v5

    const-string v0, "%s no payment, cart.hasNoPayment? %s"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    invoke-interface {p1, v1, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 149
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob;->autoVoid:Lcom/squareup/payment/AutoVoid;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Null payment in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/payment/AutoVoid;->voidDanglingAuthAfterCrash(Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getUniqueClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureJob;->logName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object p1, v2, v5

    const-string p1, "%s capturing %s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 152
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/autocapture/PerformAutoCapture;

    invoke-direct {v0}, Lcom/squareup/autocapture/PerformAutoCapture;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    :cond_2
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;

    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getUniqueClientId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;-><init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureJob$1;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 157
    :goto_0
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
