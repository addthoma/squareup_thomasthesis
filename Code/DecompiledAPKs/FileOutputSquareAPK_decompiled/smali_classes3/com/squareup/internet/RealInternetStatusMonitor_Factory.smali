.class public final Lcom/squareup/internet/RealInternetStatusMonitor_Factory;
.super Ljava/lang/Object;
.source "RealInternetStatusMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/internet/RealInternetStatusMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final managerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->managerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/internet/RealInternetStatusMonitor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/internet/RealInternetStatusMonitor_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/net/ConnectivityManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/internet/RealInternetStatusMonitor;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/internet/RealInternetStatusMonitor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/internet/RealInternetStatusMonitor;-><init>(Landroid/net/ConnectivityManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/internet/RealInternetStatusMonitor;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->managerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2}, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->newInstance(Landroid/net/ConnectivityManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/internet/RealInternetStatusMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->get()Lcom/squareup/internet/RealInternetStatusMonitor;

    move-result-object v0

    return-object v0
.end method
