.class public final Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J3\u0010\u0003\u001a\r\u0012\t\u0012\u00070\u0005\u00a2\u0006\u0002\u0008\u00060\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;",
        "",
        "()V",
        "provideConnectedPeripheralsLoggingProviders",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "connectedCardReadersLoggingProvider",
        "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
        "cashDrawersLoggingProvider",
        "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
        "connectedPrintersLoggingProvider",
        "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
        "connectedBarcodeScannersLoggingProvider",
        "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "connectedCardReadersLoggingProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawersLoggingProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectedPrintersLoggingProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectedBarcodeScannersLoggingProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    .line 17
    check-cast p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    check-cast p2, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 18
    check-cast p3, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    const/4 p1, 0x2

    aput-object p3, v0, p1

    check-cast p4, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    const/4 p1, 0x3

    aput-object p4, v0, p1

    .line 16
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method
