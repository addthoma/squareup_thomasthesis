.class public final Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;
.super Ljava/lang/Object;
.source "ConnectedBarcodeScannersLoggingProvider.kt"

# interfaces
.implements Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectedBarcodeScannersLoggingProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectedBarcodeScannersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,27:1\n1360#2:28\n1429#2,3:29\n*E\n*S KotlinDebug\n*F\n+ 1 ConnectedBarcodeScannersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider\n*L\n12#1:28\n12#1,3:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "(Lcom/squareup/barcodescanners/BarcodeScannerTracker;)V",
        "getBarcodeScannerTracker",
        "()Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "getConnectedPeripherals",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
        "getConnectionType",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralType;",
        "connectionType",
        "",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;


# direct methods
.method public constructor <init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "barcodeScannerTracker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    return-void
.end method

.method private final getConnectionType(Ljava/lang/String;)Lcom/squareup/connectedperipherals/ConnectedPeripheralType;
    .locals 2

    .line 21
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x14964

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "USB"

    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->USB:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    goto :goto_1

    .line 23
    :cond_1
    :goto_0
    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->UNKNOWN:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public final getBarcodeScannerTracker()Lcom/squareup/barcodescanners/BarcodeScannerTracker;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    return-object v0
.end method

.method public getConnectedPeripherals()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->getAvailableBarcodeScanners()Ljava/util/List;

    move-result-object v0

    const-string v1, "barcodeScannerTracker.availableBarcodeScanners"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 29
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 30
    check-cast v2, Lcom/squareup/barcodescanners/BarcodeScanner;

    .line 13
    new-instance v9, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;

    const-string v3, "it"

    .line 14
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/barcodescanners/BarcodeScanner;->getSerialNumber()Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lcom/squareup/connectedperipherals/DeviceType;->BarcodeScanner:Lcom/squareup/connectedperipherals/DeviceType;

    invoke-virtual {v3}, Lcom/squareup/connectedperipherals/DeviceType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, Lcom/squareup/barcodescanners/BarcodeScanner;->getModel()Ljava/lang/String;

    move-result-object v6

    .line 15
    invoke-interface {v2}, Lcom/squareup/barcodescanners/BarcodeScanner;->getConnectionType()Ljava/lang/String;

    move-result-object v3

    const-string v7, "it.connectionType"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;->getConnectionType(Ljava/lang/String;)Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    move-result-object v7

    invoke-interface {v2}, Lcom/squareup/barcodescanners/BarcodeScanner;->getManufacturer()Ljava/lang/String;

    move-result-object v8

    move-object v3, v9

    .line 13
    invoke-direct/range {v3 .. v8}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/connectedperipherals/ConnectedPeripheralType;Ljava/lang/String;)V

    .line 16
    invoke-interface {v1, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
