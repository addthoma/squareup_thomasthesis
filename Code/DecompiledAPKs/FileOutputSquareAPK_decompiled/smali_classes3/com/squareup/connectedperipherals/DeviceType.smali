.class public final enum Lcom/squareup/connectedperipherals/DeviceType;
.super Ljava/lang/Enum;
.source "ConnectedPeripheralData.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/connectedperipherals/DeviceType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/DeviceType;",
        "",
        "(Ljava/lang/String;I)V",
        "BarcodeScanner",
        "CardReader",
        "CashDrawer",
        "Printer",
        "UnsupportedDevice",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/connectedperipherals/DeviceType;

.field public static final enum BarcodeScanner:Lcom/squareup/connectedperipherals/DeviceType;

.field public static final enum CardReader:Lcom/squareup/connectedperipherals/DeviceType;

.field public static final enum CashDrawer:Lcom/squareup/connectedperipherals/DeviceType;

.field public static final enum Printer:Lcom/squareup/connectedperipherals/DeviceType;

.field public static final enum UnsupportedDevice:Lcom/squareup/connectedperipherals/DeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/connectedperipherals/DeviceType;

    new-instance v1, Lcom/squareup/connectedperipherals/DeviceType;

    const/4 v2, 0x0

    const-string v3, "BarcodeScanner"

    invoke-direct {v1, v3, v2}, Lcom/squareup/connectedperipherals/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/connectedperipherals/DeviceType;->BarcodeScanner:Lcom/squareup/connectedperipherals/DeviceType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/connectedperipherals/DeviceType;

    const/4 v2, 0x1

    const-string v3, "CardReader"

    invoke-direct {v1, v3, v2}, Lcom/squareup/connectedperipherals/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/connectedperipherals/DeviceType;->CardReader:Lcom/squareup/connectedperipherals/DeviceType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/connectedperipherals/DeviceType;

    const/4 v2, 0x2

    const-string v3, "CashDrawer"

    invoke-direct {v1, v3, v2}, Lcom/squareup/connectedperipherals/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/connectedperipherals/DeviceType;->CashDrawer:Lcom/squareup/connectedperipherals/DeviceType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/connectedperipherals/DeviceType;

    const/4 v2, 0x3

    const-string v3, "Printer"

    invoke-direct {v1, v3, v2}, Lcom/squareup/connectedperipherals/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/connectedperipherals/DeviceType;->Printer:Lcom/squareup/connectedperipherals/DeviceType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/connectedperipherals/DeviceType;

    const/4 v2, 0x4

    const-string v3, "UnsupportedDevice"

    invoke-direct {v1, v3, v2}, Lcom/squareup/connectedperipherals/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/connectedperipherals/DeviceType;->UnsupportedDevice:Lcom/squareup/connectedperipherals/DeviceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/connectedperipherals/DeviceType;->$VALUES:[Lcom/squareup/connectedperipherals/DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/connectedperipherals/DeviceType;
    .locals 1

    const-class v0, Lcom/squareup/connectedperipherals/DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/connectedperipherals/DeviceType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/connectedperipherals/DeviceType;
    .locals 1

    sget-object v0, Lcom/squareup/connectedperipherals/DeviceType;->$VALUES:[Lcom/squareup/connectedperipherals/DeviceType;

    invoke-virtual {v0}, [Lcom/squareup/connectedperipherals/DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/connectedperipherals/DeviceType;

    return-object v0
.end method
