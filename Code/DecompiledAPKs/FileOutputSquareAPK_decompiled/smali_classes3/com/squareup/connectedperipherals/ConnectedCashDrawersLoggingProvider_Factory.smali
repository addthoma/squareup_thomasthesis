.class public final Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;
.super Ljava/lang/Object;
.source "ConnectedCashDrawersLoggingProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;)",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cashdrawer/CashDrawerTracker;)Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;

    invoke-direct {v0, p0}, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-static {v0}, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;->newInstance(Lcom/squareup/cashdrawer/CashDrawerTracker;)Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider_Factory;->get()Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;

    move-result-object v0

    return-object v0
.end method
