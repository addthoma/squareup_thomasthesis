.class public final enum Lcom/squareup/eventstream/DroppedEventCounter$DropType;
.super Ljava/lang/Enum;
.source "DroppedEventCounter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/DroppedEventCounter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DropType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
        "",
        "key",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "QUEUE_FAILURE",
        "QUEUE_CREATION_FAILURE",
        "MAX_ITEMS",
        "MAX_BYTES",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/eventstream/DroppedEventCounter$DropType;

.field public static final enum MAX_BYTES:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

.field public static final enum MAX_ITEMS:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

.field public static final enum QUEUE_CREATION_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

.field public static final enum QUEUE_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;


# instance fields
.field public final key:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    new-instance v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    const-string v2, "QUEUE_FAILURE"

    const/4 v3, 0x0

    .line 24
    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/eventstream/DroppedEventCounter$DropType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    const-string v2, "QUEUE_CREATION_FAILURE"

    const/4 v3, 0x1

    .line 29
    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/eventstream/DroppedEventCounter$DropType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_CREATION_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    const-string v2, "MAX_ITEMS"

    const/4 v3, 0x2

    .line 33
    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/eventstream/DroppedEventCounter$DropType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_ITEMS:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    const-string v2, "MAX_BYTES"

    const/4 v3, 0x3

    .line 37
    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/eventstream/DroppedEventCounter$DropType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_BYTES:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->$VALUES:[Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/eventstream/DroppedEventCounter$DropType;
    .locals 1

    const-class v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/eventstream/DroppedEventCounter$DropType;
    .locals 1

    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->$VALUES:[Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-virtual {v0}, [Lcom/squareup/eventstream/DroppedEventCounter$DropType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    return-object v0
.end method
