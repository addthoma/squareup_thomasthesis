.class final Lcom/squareup/eventstream/v1/WireEventConverter;
.super Ljava/lang/Object;
.source "WireEventConverter.java"

# interfaces
.implements Lcom/squareup/tape/FileObjectQueue$Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/FileObjectQueue$Converter<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public from([B)Lcom/squareup/protos/eventstream/v1/Event;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/eventstream/v1/Event;

    return-object p1
.end method

.method public bridge synthetic from([B)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v1/WireEventConverter;->from([B)Lcom/squareup/protos/eventstream/v1/Event;

    move-result-object p1

    return-object p1
.end method

.method public toStream(Lcom/squareup/protos/eventstream/v1/Event;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public bridge synthetic toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Event;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/eventstream/v1/WireEventConverter;->toStream(Lcom/squareup/protos/eventstream/v1/Event;Ljava/io/OutputStream;)V

    return-void
.end method
