.class Lcom/squareup/eventstream/v1/DroppedEventsV1Event;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "DroppedEventsV1Event.java"


# instance fields
.field private final batch_size:J

.field private final max_bytes_count:J

.field private final max_items_count:J

.field private final queue_creation_failure_count:J

.field private final queue_failure_count:J


# direct methods
.method constructor <init>(Ljava/util/Map;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .line 17
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Dropped Events"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;->queue_failure_count:J

    .line 19
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_CREATION_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    .line 20
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;->queue_creation_failure_count:J

    .line 21
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_ITEMS:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;->max_items_count:J

    .line 22
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_BYTES:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;->max_bytes_count:J

    int-to-long p1, p2

    .line 23
    iput-wide p1, p0, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;->batch_size:J

    return-void
.end method
