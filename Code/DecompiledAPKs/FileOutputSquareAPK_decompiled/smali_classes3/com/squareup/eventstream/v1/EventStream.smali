.class public final Lcom/squareup/eventstream/v1/EventStream;
.super Lcom/squareup/eventstream/BaseEventstream;
.source "EventStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/v1/EventStream$NoRawData;,
        Lcom/squareup/eventstream/v1/EventStream$CurrentState;,
        Lcom/squareup/eventstream/v1/EventStream$Builder;,
        Lcom/squareup/eventstream/v1/EventStream$Name;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/eventstream/BaseEventstream<",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        "Lcom/squareup/eventstream/v1/EventStream$CurrentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENTSTREAM_QUEUE_FILENAME:Ljava/lang/String; = "eventstream.queue"


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            "Lcom/squareup/eventstream/v1/EventStreamEvent;",
            "Lcom/squareup/eventstream/v1/EventStream$CurrentState;",
            ">;",
            "Lcom/squareup/eventstream/EventStore<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            ">;)V"
        }
    .end annotation

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/eventstream/BaseEventstream;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V

    return-void
.end method


# virtual methods
.method public log(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 1

    .line 99
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$NoRawData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/eventstream/v1/EventStream$NoRawData;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/eventstream/v1/EventStream;->log(Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 103
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$NoRawData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/eventstream/v1/EventStream$NoRawData;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;)V

    invoke-virtual {p0, v0}, Lcom/squareup/eventstream/v1/EventStream;->log(Ljava/lang/Object;)V

    return-void
.end method
