.class public Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
.super Ljava/lang/Object;
.source "ConnectionCatalog.java"


# instance fields
.field private connection_ip_address:Ljava/lang/String;

.field private connection_network_generation:Ljava/lang/String;

.field private connection_network_operator:Ljava/lang/String;

.field private connection_network_type:Ljava/lang/String;

.field private connection_user_agent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setIpAddress(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->connection_ip_address:Ljava/lang/String;

    return-object p0
.end method

.method public setNetworkGeneration(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->connection_network_generation:Ljava/lang/String;

    return-object p0
.end method

.method public setNetworkOperator(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->connection_network_operator:Ljava/lang/String;

    return-object p0
.end method

.method public setNetworkType(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->connection_network_type:Ljava/lang/String;

    return-object p0
.end method

.method public setUserAgent(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->connection_user_agent:Ljava/lang/String;

    return-object p0
.end method
