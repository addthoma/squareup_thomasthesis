.class public interface abstract Lcom/squareup/eventstream/EventStreamLog;
.super Ljava/lang/Object;
.source "EventStreamLog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/EventStreamLog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cJ-\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0016\u0010\u0006\u001a\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u00010\u00010\u0007\"\u0004\u0018\u00010\u0001H&\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&\u0082\u0002\u0007\n\u0005\u0008\u0091F0\u0001\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventStreamLog;",
        "",
        "log",
        "",
        "message",
        "",
        "args",
        "",
        "(Ljava/lang/String;[Ljava/lang/Object;)V",
        "report",
        "throwable",
        "",
        "Companion",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/eventstream/EventStreamLog$Companion;

.field public static final NONE:Lcom/squareup/eventstream/EventStreamLog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/eventstream/EventStreamLog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/EventStreamLog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/EventStreamLog;->Companion:Lcom/squareup/eventstream/EventStreamLog$Companion;

    .line 19
    new-instance v0, Lcom/squareup/eventstream/EventStreamLog$Companion$NONE$1;

    invoke-direct {v0}, Lcom/squareup/eventstream/EventStreamLog$Companion$NONE$1;-><init>()V

    check-cast v0, Lcom/squareup/eventstream/EventStreamLog;

    sput-object v0, Lcom/squareup/eventstream/EventStreamLog;->NONE:Lcom/squareup/eventstream/EventStreamLog;

    return-void
.end method


# virtual methods
.method public varargs abstract log(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public abstract report(Ljava/lang/Throwable;)V
.end method
