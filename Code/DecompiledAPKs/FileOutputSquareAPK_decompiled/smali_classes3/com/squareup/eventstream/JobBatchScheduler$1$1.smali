.class public final Lcom/squareup/eventstream/JobBatchScheduler$1$1;
.super Lcom/evernote/android/job/Job;
.source "JobBatchScheduler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/JobBatchScheduler$1;->create(Ljava/lang/String;)Lcom/squareup/eventstream/JobBatchScheduler$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0014\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/eventstream/JobBatchScheduler$1$1",
        "Lcom/evernote/android/job/Job;",
        "onRunJob",
        "Lcom/evernote/android/job/Job$Result;",
        "params",
        "Lcom/evernote/android/job/Job$Params;",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/eventstream/JobBatchScheduler$1;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/JobBatchScheduler$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler$1$1;->this$0:Lcom/squareup/eventstream/JobBatchScheduler$1;

    invoke-direct {p0}, Lcom/evernote/android/job/Job;-><init>()V

    return-void
.end method


# virtual methods
.method protected onRunJob(Lcom/evernote/android/job/Job$Params;)Lcom/evernote/android/job/Job$Result;
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler$1$1;->this$0:Lcom/squareup/eventstream/JobBatchScheduler$1;

    iget-object p1, p1, Lcom/squareup/eventstream/JobBatchScheduler$1;->$uploadBatcher:Lcom/squareup/eventstream/UploadBatcher;

    invoke-virtual {p1}, Lcom/squareup/eventstream/UploadBatcher;->upload()Lcom/evernote/android/job/Job$Result;

    move-result-object p1

    return-object p1
.end method
