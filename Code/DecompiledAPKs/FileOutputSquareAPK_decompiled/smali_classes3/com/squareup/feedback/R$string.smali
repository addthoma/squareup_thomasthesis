.class public final Lcom/squareup/feedback/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feedback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final feedback_and_more:I = 0x7f120ab7

.field public static final feedback_error:I = 0x7f120ab8

.field public static final feedback_success:I = 0x7f120ab9

.field public static final have_feedback:I = 0x7f120b4d

.field public static final no_thanks:I = 0x7f121091

.field public static final play_store_link:I = 0x7f12143d

.field public static final play_store_package_name_for_review:I = 0x7f12143e

.field public static final rate_on_play_store_prompt:I = 0x7f121539

.field public static final rate_on_the_play_store:I = 0x7f12153a

.field public static final submit_feedback:I = 0x7f1218cf

.field public static final what_do_you_think_of_app:I = 0x7f121bdf


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
