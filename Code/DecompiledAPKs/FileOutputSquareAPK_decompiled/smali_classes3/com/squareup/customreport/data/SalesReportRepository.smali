.class public interface abstract Lcom/squareup/customreport/data/SalesReportRepository;
.super Ljava/lang/Object;
.source "SalesReportRepository.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/SalesReportRepository$Result;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0008J\u001c\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/customreport/data/SalesReportRepository;",
        "",
        "salesReport",
        "Lio/reactivex/Single;",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesReport;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "Result",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract salesReport(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesReport;",
            ">;>;"
        }
    .end annotation
.end method
