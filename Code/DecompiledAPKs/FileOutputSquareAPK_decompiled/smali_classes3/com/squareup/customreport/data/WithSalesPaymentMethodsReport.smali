.class public final Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;
.super Lcom/squareup/customreport/data/SalesPaymentMethodsReport;
.source "SalesPaymentMethodsReport.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B/\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J;\u0010\u0014\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\u0019\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0016H\u00d6\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000b\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "paymentMethods",
        "",
        "Lcom/squareup/customreport/data/SalesPaymentMethod;",
        "fees",
        "Lcom/squareup/protos/common/Money;",
        "totalCollectedMoney",
        "netTotalMoney",
        "(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V",
        "getFees",
        "()Lcom/squareup/protos/common/Money;",
        "getNetTotalMoney",
        "getPaymentMethods",
        "()Ljava/util/List;",
        "getTotalCollectedMoney",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final fees:Lcom/squareup/protos/common/Money;

.field private final netTotalMoney:Lcom/squareup/protos/common/Money;

.field private final paymentMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/SalesPaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private final totalCollectedMoney:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/SalesPaymentMethod;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    const-string v0, "paymentMethods"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalCollectedMoney"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, v0}, Lcom/squareup/customreport/data/SalesPaymentMethodsReport;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->copy(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/SalesPaymentMethod;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/SalesPaymentMethod;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;"
        }
    .end annotation

    const-string v0, "paymentMethods"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalCollectedMoney"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;-><init>(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFees()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getNetTotalMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getPaymentMethods()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/SalesPaymentMethod;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    return-object v0
.end method

.method public final getTotalCollectedMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WithSalesPaymentMethodsReport(paymentMethods="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalCollectedMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", netTotalMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->paymentMethods:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/customreport/data/SalesPaymentMethod;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->fees:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->totalCollectedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->netTotalMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
