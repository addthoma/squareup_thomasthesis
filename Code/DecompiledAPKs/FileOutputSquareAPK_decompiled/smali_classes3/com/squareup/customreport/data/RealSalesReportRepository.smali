.class public final Lcom/squareup/customreport/data/RealSalesReportRepository;
.super Ljava/lang/Object;
.source "RealSalesReportRepository.kt"

# interfaces
.implements Lcom/squareup/customreport/data/SalesReportRepository;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSalesReportRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSalesReportRepository.kt\ncom/squareup/customreport/data/RealSalesReportRepository\n*L\n1#1,394:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001c\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J$\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u00120\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u001c\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00190\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010\u001a\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u001c\u0010\u001c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010\u001e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u00120\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0008\u0010\u001f\u001a\u00020 H\u0002J\u001c\u0010!\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\"0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010#\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020$0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/customreport/data/RealSalesReportRepository;",
        "Lcom/squareup/customreport/data/SalesReportRepository;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "remoteStore",
        "Lcom/squareup/customreport/data/service/RemoteCustomReportStore;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/time/CurrentTime;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/RemoteCustomReportStore;Lcom/squareup/settings/server/Features;)V",
        "chartedSales",
        "Lio/reactivex/Single;",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "customReportResponse",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "salesReportType",
        "Lcom/squareup/customreport/data/SalesReportType;",
        "discounts",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "paymentMethods",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "salesReport",
        "Lcom/squareup/customreport/data/SalesReport;",
        "salesSummary",
        "Lcom/squareup/customreport/data/SalesSummaryReport;",
        "salesSummaryResponse",
        "seeMeasurementUnit",
        "",
        "topCategories",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "topItems",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final remoteStore:Lcom/squareup/customreport/data/service/RemoteCustomReportStore;


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/RemoteCustomReportStore;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTimeZone"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remoteStore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p2, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    iput-object p3, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->remoteStore:Lcom/squareup/customreport/data/service/RemoteCustomReportStore;

    iput-object p4, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getCurrentTime$p(Lcom/squareup/customreport/data/RealSalesReportRepository;)Lcom/squareup/time/CurrentTime;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTime:Lcom/squareup/time/CurrentTime;

    return-object p0
.end method

.method public static final synthetic access$getCurrentTimeZone$p(Lcom/squareup/customreport/data/RealSalesReportRepository;)Lcom/squareup/time/CurrentTimeZone;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    return-object p0
.end method

.method private final chartedSales(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesChartReport;",
            ">;>;"
        }
    .end annotation

    .line 186
    invoke-static {p1}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->access$getChartReportType$p(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object v0

    .line 187
    move-object v1, v0

    check-cast v1, Lcom/squareup/customreport/data/SalesReportType;

    invoke-direct {p0, p1, v1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object v2

    .line 188
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    if-eqz v3, :cond_0

    .line 190
    new-instance p1, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$1;

    invoke-direct {p1, p0, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$1;-><init>(Lcom/squareup/customreport/data/RealSalesReportRepository;Lcom/squareup/customreport/data/SalesReportType$Charted;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {v2, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "chartedSales\n          .\u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 206
    :cond_0
    invoke-static {p1}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonConfig(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    .line 207
    invoke-direct {p0, p1, v1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    .line 208
    sget-object v1, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    .line 209
    check-cast v2, Lio/reactivex/SingleSource;

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 210
    new-instance v1, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;-><init>(Lcom/squareup/customreport/data/RealSalesReportRepository;Lcom/squareup/customreport/data/SalesReportType$Charted;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Singles\n        .zip(cha\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/customreport/data/SalesReportType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;"
        }
    .end annotation

    .line 293
    iget-object v0, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 294
    invoke-static {p1}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/threeten/bp/ZonedDateTime;->of(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v4

    .line 295
    invoke-static {p1}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getEndDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/threeten/bp/ZonedDateTime;->of(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v5

    .line 296
    iget-object v2, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->remoteStore:Lcom/squareup/customreport/data/service/RemoteCustomReportStore;

    const-string/jumbo v0, "zonedStartDateTime"

    .line 298
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zonedEndDateTime"

    .line 299
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result v6

    .line 301
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object v7

    move-object v3, p2

    .line 296
    invoke-interface/range {v2 .. v7}, Lcom/squareup/customreport/data/service/RemoteCustomReportStore;->customReport(Lcom/squareup/customreport/data/SalesReportType;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final discounts(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesDiscountsReport;",
            ">;>;"
        }
    .end annotation

    .line 261
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Discounts;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Discounts;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    .line 262
    sget-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$discounts$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$discounts$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "customReportResponse(rep\u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final paymentMethods(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
            ">;>;"
        }
    .end annotation

    .line 273
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$PaymentMethods;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$PaymentMethods;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    .line 274
    sget-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$paymentMethods$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$paymentMethods$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "customReportResponse(rep\u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final salesSummary(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesSummaryReport;",
            ">;>;"
        }
    .end annotation

    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->salesSummaryResponse(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    .line 129
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    if-eqz v1, :cond_0

    .line 131
    sget-object p1, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "salesSummary\n          .\u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    const/4 v2, 0x0

    .line 144
    iget-object v1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    const/4 v4, 0x0

    .line 145
    iget-object v1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->localTime()Lorg/threeten/bp/LocalTime;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1f5

    const/4 v12, 0x0

    move-object v1, p1

    .line 143
    invoke-static/range {v1 .. v12}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    .line 147
    invoke-static {p1}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonConfig(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->salesSummaryResponse(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object p1

    .line 149
    sget-object v1, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    .line 150
    check-cast v0, Lio/reactivex/SingleSource;

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 151
    sget-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$2;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Singles\n        .zip(sal\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final salesSummaryResponse(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;"
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Aggregate;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Aggregate;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final seeMeasurementUnit()Z
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/squareup/customreport/data/RealSalesReportRepository;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_SEE_MEASUREMENT_UNIT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final topCategories(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
            ">;>;"
        }
    .end annotation

    .line 229
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    .line 230
    sget-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$topCategories$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$topCategories$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "customReportResponse(rep\u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final topItems(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesTopItemsReport;",
            ">;>;"
        }
    .end annotation

    .line 242
    invoke-direct {p0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->seeMeasurementUnit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$ItemWithMeasurementUnitSales;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$ItemWithMeasurementUnitSales;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    goto :goto_0

    .line 245
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$ItemSales;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$ItemSales;

    check-cast v0, Lcom/squareup/customreport/data/SalesReportType;

    .line 241
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/customreport/data/RealSalesReportRepository;->customReportResponse(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/SalesReportType;)Lio/reactivex/Single;

    move-result-object p1

    .line 248
    sget-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$topItems$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$topItems$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "customReportResponse(\n  \u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public salesReport(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesReport;",
            ">;>;"
        }
    .end annotation

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object v1, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->salesSummary(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/reactivex/SingleSource;

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->chartedSales(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/SingleSource;

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->discounts(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/SingleSource;

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->topCategories(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/SingleSource;

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->topItems(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/SingleSource;

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->paymentMethods(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object p1

    move-object v7, p1

    check-cast v7, Lio/reactivex/SingleSource;

    .line 69
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 77
    new-instance v0, Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;

    invoke-direct {v0, p0}, Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;-><init>(Lcom/squareup/customreport/data/RealSalesReportRepository;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Singles\n        .zip(\n  \u2026d\")\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
