.class final Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;
.super Ljava/lang/Object;
.source "RealSalesReportRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/customreport/data/RealSalesReportRepository;->salesReport(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00a3\u0001\u0010\u0003\u001a\u009e\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \u0006*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0008 \u0006*\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \u0006*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\n \u0006*\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u000b \u0006*\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00010\u00010\u0004H\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesReport;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Sextuple;",
        "Lcom/squareup/customreport/data/SalesSummaryReport;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;


# direct methods
.method constructor <init>(Lcom/squareup/customreport/data/RealSalesReportRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;->this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/tuple/Sextuple;)Lcom/squareup/customreport/data/SalesReportRepository$Result;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Sextuple<",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesSummaryReport;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesChartReport;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesDiscountsReport;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesTopItemsReport;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
            ">;>;)",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesReport;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component3()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component4()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component5()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/util/tuple/Sextuple;->component6()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    .line 84
    instance-of v6, v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 85
    instance-of v6, v2, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 86
    instance-of v6, v3, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 87
    instance-of v6, v4, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 88
    instance-of v6, v5, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 89
    instance-of v6, v1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    if-eqz v6, :cond_2

    .line 90
    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/customreport/data/SalesSummaryReport;

    .line 91
    instance-of v6, v0, Lcom/squareup/customreport/data/NoSalesSummaryReport;

    if-eqz v6, :cond_0

    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    sget-object v1, Lcom/squareup/customreport/data/NoSalesReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesReport;

    invoke-direct {v0, v1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :cond_0
    instance-of v6, v0, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v6, :cond_1

    new-instance v6, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    .line 93
    new-instance v15, Lcom/squareup/customreport/data/WithSalesReport;

    move-object/from16 v14, p0

    .line 94
    iget-object v7, v14, Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;->this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;

    invoke-static {v7}, Lcom/squareup/customreport/data/RealSalesReportRepository;->access$getCurrentTime$p(Lcom/squareup/customreport/data/RealSalesReportRepository;)Lcom/squareup/time/CurrentTime;

    move-result-object v7

    invoke-interface {v7}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v8

    .line 95
    move-object v9, v0

    check-cast v9, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    .line 96
    check-cast v2, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/customreport/data/SalesChartReport;

    .line 97
    check-cast v3, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/customreport/data/SalesDiscountsReport;

    .line 98
    check-cast v4, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    .line 99
    check-cast v5, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v5}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/customreport/data/SalesTopItemsReport;

    .line 100
    check-cast v1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    move-object v7, v15

    move-object v14, v0

    .line 93
    invoke-direct/range {v7 .. v14}, Lcom/squareup/customreport/data/WithSalesReport;-><init>(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)V

    .line 92
    invoke-direct {v6, v15}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;-><init>(Ljava/lang/Object;)V

    move-object v0, v6

    .line 90
    :goto_0
    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    goto :goto_1

    .line 92
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 105
    :cond_2
    instance-of v6, v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v6, :cond_3

    goto :goto_1

    .line 106
    :cond_3
    instance-of v0, v2, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v0, :cond_4

    move-object v0, v2

    goto :goto_1

    .line 107
    :cond_4
    instance-of v0, v3, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v0, :cond_5

    move-object v0, v3

    goto :goto_1

    .line 108
    :cond_5
    instance-of v0, v4, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v0, :cond_6

    move-object v0, v4

    goto :goto_1

    .line 109
    :cond_6
    instance-of v0, v5, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v0, :cond_7

    move-object v0, v5

    goto :goto_1

    .line 110
    :cond_7
    instance-of v0, v1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz v0, :cond_8

    move-object v0, v1

    goto :goto_1

    .line 111
    :cond_8
    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    :goto_1
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/util/tuple/Sextuple;

    invoke-virtual {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository$salesReport$1;->apply(Lcom/squareup/util/tuple/Sextuple;)Lcom/squareup/customreport/data/SalesReportRepository$Result;

    move-result-object p1

    return-object p1
.end method
