.class public final Lcom/squareup/customreport/data/SalesDiscount;
.super Ljava/lang/Object;
.source "SalesDiscount.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/SalesDiscount$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesDiscount.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesDiscount.kt\ncom/squareup/customreport/data/SalesDiscount\n*L\n1#1,28:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/customreport/data/SalesDiscount;",
        "Landroid/os/Parcelable;",
        "name",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
        "discountMoney",
        "Lcom/squareup/protos/common/Money;",
        "count",
        "",
        "(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)V",
        "getCount",
        "()J",
        "getDiscountMoney",
        "()Lcom/squareup/protos/common/Money;",
        "getName",
        "()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final count:J

.field private final discountMoney:Lcom/squareup/protos/common/Money;

.field private final name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/SalesDiscount$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/SalesDiscount$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/SalesDiscount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountMoney"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object p2, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    iput-wide p3, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    .line 23
    iget-wide p1, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    const-wide/16 p3, 0x0

    cmp-long v0, p1, p3

    if-ltz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Count value should not be negative"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/SalesDiscount;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;JILjava/lang/Object;)Lcom/squareup/customreport/data/SalesDiscount;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-wide p3, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/customreport/data/SalesDiscount;->copy(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)Lcom/squareup/customreport/data/SalesDiscount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    return-wide v0
.end method

.method public final copy(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)Lcom/squareup/customreport/data/SalesDiscount;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountMoney"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/SalesDiscount;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/customreport/data/SalesDiscount;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/SalesDiscount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/SalesDiscount;

    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v1, p1, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    iget-wide v2, p1, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()J
    .locals 2

    .line 20
    iget-wide v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    return-wide v0
.end method

.method public final getDiscountMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SalesDiscount(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discountMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/SalesDiscount;->name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/SalesDiscount;->discountMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-wide v0, p0, Lcom/squareup/customreport/data/SalesDiscount;->count:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
