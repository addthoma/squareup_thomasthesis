.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->updateCheckoutLinkScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 PayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner\n*L\n1#1,1322:1\n132#2,4:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;->$screenData$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;->$screenData$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getOnClickCheckoutLinkUrl()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    .line 1324
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->access$getView$p(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;->$screenData$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getCheckoutLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
