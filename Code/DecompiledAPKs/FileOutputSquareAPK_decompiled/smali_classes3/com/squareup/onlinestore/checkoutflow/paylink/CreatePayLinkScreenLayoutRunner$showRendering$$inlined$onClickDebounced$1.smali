.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 CreatePayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner\n*L\n1#1,1322:1\n67#2,15:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->$rendering$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1324
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->access$getPayLinkName$p(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ljava/lang/CharSequence;

    .line 1323
    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1324
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 1325
    :goto_0
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    .line 1326
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_get_link_name_is_required:I

    invoke-static {p1, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->access$showNameError(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;I)V

    goto :goto_3

    .line 1330
    :cond_3
    sget-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v0, p1}, Lcom/squareup/onlinestore/common/PayLinkUtil;->nameExceedsCharCountLimit(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1331
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_get_link_name_over_255:I

    invoke-static {p1, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->access$showNameError(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;I)V

    goto :goto_3

    .line 1335
    :cond_4
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->access$getPayLinkName$p(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 1336
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;->$rendering$inlined:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getOnClickGetLink()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    return-void
.end method
