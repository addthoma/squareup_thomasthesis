.class public final Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOnlineCheckoutBuyLinkWorkflow.kt"

# interfaces
.implements Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action;,
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult;,
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnlineCheckoutBuyLinkWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnlineCheckoutBuyLinkWorkflow.kt\ncom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,284:1\n32#2,12:285\n149#3,5:297\n85#4:302\n240#5:303\n276#6:304\n*E\n*S KotlinDebug\n*F\n+ 1 RealOnlineCheckoutBuyLinkWorkflow.kt\ncom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow\n*L\n94#1,12:285\n168#1,5:297\n254#1:302\n254#1:303\n254#1:304\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00bb\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\"\u0018\u0000 C2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0003BCDB]\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u00a2\u0006\u0002\u0010 J\u0018\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u0012\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010(\u001a\u00020)H\u0007J\u0016\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-2\u0006\u0010/\u001a\u00020)H\u0002J\u0010\u00100\u001a\u00020)2\u0006\u00101\u001a\u00020\u0015H\u0002J\u0010\u00102\u001a\u00020)2\u0006\u00103\u001a\u00020)H\u0002J\u0010\u00104\u001a\u00020\u00032\u0006\u00105\u001a\u000206H\u0002J\u001a\u00107\u001a\u00020\u00032\u0006\u00108\u001a\u00020\u00022\u0008\u00109\u001a\u0004\u0018\u00010:H\u0016J\u0018\u0010;\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002JN\u0010<\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u00108\u001a\u00020\u00022\u0006\u0010=\u001a\u00020\u00032\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040>H\u0016J\u0010\u0010?\u001a\u00020:2\u0006\u0010=\u001a\u00020\u0003H\u0016J\u0014\u0010@\u001a\u000206*\u00020A2\u0006\u0010/\u001a\u00020)H\u0002R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010#R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006E"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "server",
        "Lcom/squareup/http/Server;",
        "checkoutLinkShareSheet",
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
        "clipboard",
        "Lcom/squareup/util/Clipboard;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "qrCodeGenerator",
        "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/util/Clipboard;Lcom/squareup/text/Formatter;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;)V",
        "logViewEventWorker",
        "com/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;",
        "copyCheckoutLinkUrlToClipboard",
        "",
        "context",
        "Landroid/content/Context;",
        "checkoutLinkUrl",
        "",
        "createCheckoutLinkQrCode",
        "Landroid/graphics/Bitmap;",
        "enableCheckoutLinksWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult;",
        "merchantCatalogObjectToken",
        "getActionBarTitle",
        "amount",
        "getCheckoutLinkUrl",
        "itemId",
        "getInitialState",
        "isEcomAvailable",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "openShareSheet",
        "render",
        "state",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "enableEcomAvailable",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "Action",
        "Companion",
        "EnableCheckoutLinksWorkerResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ANALYTICS_WORKER_KEY:Ljava/lang/String; = "analytics-worker-key"

.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Companion;

.field public static final ENABLE_CHECKOUTLINKS_WORKER_KEY:Ljava/lang/String; = "enable-checkoutlinks-worker-key"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

.field private final clipboard:Lcom/squareup/util/Clipboard;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

.field private final res:Lcom/squareup/util/Res;

.field private final server:Lcom/squareup/http/Server;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->Companion:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/util/Clipboard;Lcom/squareup/text/Formatter;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/http/Server;",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            "Lcom/squareup/util/Clipboard;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/ToastFactory;",
            "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinkShareSheet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qrCodeGenerator"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinksRepository"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->server:Lcom/squareup/http/Server;

    iput-object p3, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    iput-object p4, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->clipboard:Lcom/squareup/util/Clipboard;

    iput-object p5, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p7, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    iput-object p8, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    iput-object p9, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p10, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    .line 85
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;

    return-void
.end method

.method public static final synthetic access$copyCheckoutLinkUrlToClipboard(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->copyCheckoutLinkUrlToClipboard(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$enableEcomAvailable(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;)Z
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->enableEcomAvailable(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object p0
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)Lcom/squareup/cogs/Cogs;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    return-object p0
.end method

.method public static final synthetic access$openShareSheet(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->openShareSheet(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private final copyCheckoutLinkUrlToClipboard(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 194
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->BUY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->clipboard:Lcom/squareup/util/Clipboard;

    .line 196
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_clipboard_label:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 197
    check-cast p2, Ljava/lang/CharSequence;

    .line 195
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/util/Clipboard;->copyPlainText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_copied_toast_message:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 201
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private final enableCheckoutLinksWorker(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult;",
            ">;"
        }
    .end annotation

    .line 226
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 227
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 253
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$2;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026 anything goes wrong */ }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 303
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 304
    const-class v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final enableEcomAvailable(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;)Z
    .locals 2

    .line 257
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-interface {p1, v0, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItem;

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    .line 258
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-direct {v1, p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V

    .line 259
    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setEcomAvailable(Z)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    move-result-object p2

    .line 260
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object p2

    .line 261
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {p1, p2, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getActionBarTitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2

    .line 173
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_checkout_link:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "money"

    .line 173
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_buylink_url:I

    goto :goto_0

    .line 181
    :cond_0
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_buylink_url_staging:I

    .line 184
    :goto_0
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 185
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 186
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 187
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getInitialState(Z)Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;
    .locals 0

    if-eqz p1, :cond_0

    .line 220
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$CheckoutLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$CheckoutLinkState;

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    goto :goto_0

    .line 222
    :cond_0
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$LoadingState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$LoadingState;

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    :goto_0
    return-object p1
.end method

.method private final openShareSheet(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .line 208
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->SHARE_BUY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 209
    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    .line 211
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_share_sheet_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 212
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_share_sheet_subject:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 214
    sget-object v7, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->ONLINE_CHECKOUT_BUY_LINK_SCREEN:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-object v3, p1

    move-object v6, p2

    .line 209
    invoke-interface/range {v2 .. v7}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;->openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V

    return-void
.end method


# virtual methods
.method public final createCheckoutLinkQrCode(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const-string v0, "checkoutLinkUrl"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$dimen;->online_checkout_qr_code_size:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    .line 267
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    invoke-interface {v1, p1, v0}, Lcom/squareup/qrcodegenerator/QrCodeGenerator;->generate(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public initialState(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 285
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 292
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 293
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 294
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 296
    :cond_3
    check-cast v2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 94
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;->isEcomAvailable()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->getInitialState(Z)Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->initialState(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->render(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 129
    instance-of v1, p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$LoadingState;

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->enableCheckoutLinksWorker(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    .line 133
    sget-object p2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$1;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    const-string v1, "enable-checkoutlinks-worker-key"

    .line 130
    invoke-interface {p3, p1, v1, p2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 137
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;

    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$2;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    goto :goto_0

    .line 139
    :cond_0
    instance-of v1, p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$CheckoutLinkState;

    if-eqz v1, :cond_1

    .line 140
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$logViewEventWorker$1;

    check-cast p2, Lcom/squareup/workflow/Worker;

    const-string v1, "analytics-worker-key"

    invoke-static {p3, p2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;->getCheckoutLinkItemId()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 142
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;

    .line 143
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->getActionBarTitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v2

    .line 145
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$3;

    move-object p3, p0

    check-cast p3, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    invoke-direct {p1, p3}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$3;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function2;

    .line 146
    invoke-virtual {p0, v3}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->createCheckoutLinkQrCode(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 147
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$4;

    invoke-direct {p1, p3}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$4;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function2;

    .line 148
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$5;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$5;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    move-object v1, p2

    .line 142
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    goto :goto_0

    .line 154
    :cond_1
    instance-of p1, p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$FeatureNotAvailableState;

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;

    .line 155
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$6;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$6;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 154
    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    goto :goto_0

    .line 157
    :cond_2
    instance-of p1, p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$ErrorState;

    if-eqz p1, :cond_3

    .line 158
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;

    .line 159
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$7;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$7;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 160
    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$8;

    invoke-direct {p3, p0, v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$render$screenData$8;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 158
    invoke-direct {p1, p2, p3}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    .line 168
    :goto_0
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 298
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 299
    const-class p3, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 300
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 298
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 169
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 158
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->snapshotState(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
