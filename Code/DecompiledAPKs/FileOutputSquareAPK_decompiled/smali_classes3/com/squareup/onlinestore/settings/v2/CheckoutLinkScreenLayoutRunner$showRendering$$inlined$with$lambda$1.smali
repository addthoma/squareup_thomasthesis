.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 CheckoutLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner\n*L\n1#1,1322:1\n93#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering$inlined:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;->$rendering$inlined:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;->$rendering$inlined:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getOnClickCheckoutLinkUrl()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->access$getView$p(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
