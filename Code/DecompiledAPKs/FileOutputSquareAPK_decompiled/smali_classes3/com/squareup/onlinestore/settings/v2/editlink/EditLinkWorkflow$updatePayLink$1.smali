.class final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;
.super Ljava/lang/Object;
.source "EditLinkWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->updatePayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
        "kotlin.jvm.PlatformType",
        "tokenResult",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->$editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "tokenResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object v0

    .line 147
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;->getJsonWebToken()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->$editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    invoke-static {v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->access$toPayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/onlinestore/repository/PayLink;

    move-result-object v1

    .line 146
    invoke-interface {v0, p1, v1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->updatePayLink(Ljava/lang/String;Lcom/squareup/onlinestore/repository/PayLink;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 149
    :cond_0
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(UpdatePayLinkResult.OtherError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
