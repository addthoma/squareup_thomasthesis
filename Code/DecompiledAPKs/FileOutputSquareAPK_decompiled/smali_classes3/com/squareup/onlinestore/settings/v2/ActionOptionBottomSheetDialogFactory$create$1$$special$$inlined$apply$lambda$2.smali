.class public final Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 ActionOptionBottomSheetDialogFactory.kt\ncom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1\n*L\n1#1,1322:1\n53#2,3:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$3"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dialog$inlined:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

.field final synthetic $screenData$inlined:Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;->$screenData$inlined:Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;->$dialog$inlined:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;->$dialog$inlined:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->dismiss()V

    .line 1324
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;->$screenData$inlined:Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;->getOnClickUpdateEnabledStatus()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
