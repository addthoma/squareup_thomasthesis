.class final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->invoke(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "-",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->$it:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->$state:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object v0

    .line 101
    instance-of v1, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SAVE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v1, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->$state:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->component2()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 104
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult$Success;

    new-instance v4, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    iget-object v5, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->$it:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

    check-cast v5, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$Success;

    invoke-virtual {v5}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$Success;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1, v0, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v4, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    invoke-direct {v3, v4}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult$Success;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    invoke-virtual {p1, v3}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_0
    instance-of v0, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SAVE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v1, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 109
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult$Success;

    .line 110
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->$it:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$Success;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$Success;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    iget-object v4, v4, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->$state:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    check-cast v4, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;

    invoke-virtual {v4}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object v4

    check-cast v4, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    invoke-virtual {v4}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    .line 109
    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult$Success;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    .line 108
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
