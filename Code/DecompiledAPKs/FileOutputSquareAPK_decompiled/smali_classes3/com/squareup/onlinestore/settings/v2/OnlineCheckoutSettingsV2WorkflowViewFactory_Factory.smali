.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2WorkflowViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->newInstance(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflowViewFactory;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory_Factory;->get()Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method
