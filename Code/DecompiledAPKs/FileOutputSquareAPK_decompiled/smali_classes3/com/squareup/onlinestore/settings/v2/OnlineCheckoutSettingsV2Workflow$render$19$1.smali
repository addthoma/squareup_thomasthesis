.class final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnlineCheckoutSettingsV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->invoke(Z)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $success:Z

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iput-boolean p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->$success:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->$success:Z

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v1, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v2, v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getNewEnabledState()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->access$logActivation(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)V

    .line 366
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$LoadCheckoutLinkListState;

    .line 367
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v2, v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v3, v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getNewEnabledState()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;-><init>(Ljava/lang/String;Z)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;

    .line 366
    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$LoadCheckoutLinkListState;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;)V

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    goto :goto_0

    .line 370
    :cond_0
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;

    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$UpdateEnabledError;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v2, v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getNewEnabledState()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    iget-object v3, v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$UpdateEnabledError;-><init>(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;)V

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    .line 364
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
