.class public final Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;
.super Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;
.source "CheckoutLinksRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Success"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J-\u0010\u0013\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00082\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
        "payLinks",
        "",
        "Lcom/squareup/onlinestore/repository/PayLink;",
        "currentPage",
        "",
        "hasMore",
        "",
        "(Ljava/util/List;IZ)V",
        "getCurrentPage",
        "()I",
        "getHasMore",
        "()Z",
        "getPayLinks",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentPage:I

.field private final hasMore:Z

.field private final payLinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ">;IZ)V"
        }
    .end annotation

    const-string v0, "payLinks"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    iput p2, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    iput-boolean p3, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;Ljava/util/List;IZILjava/lang/Object;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->copy(Ljava/util/List;IZ)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    return v0
.end method

.method public final copy(Ljava/util/List;IZ)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ">;IZ)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;"
        }
    .end annotation

    const-string v0, "payLinks"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;-><init>(Ljava/util/List;IZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    iget-object v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    iget v1, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    iget-boolean p1, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentPage()I
    .locals 1

    .line 100
    iget v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    return v0
.end method

.method public final getHasMore()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    return v0
.end method

.method public final getPayLinks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Success(payLinks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->payLinks:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->currentPage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", hasMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->hasMore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
