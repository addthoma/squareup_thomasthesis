.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantExists()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/MaybeSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
        "kotlin.jvm.PlatformType",
        "pasetoToken",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "pasetoToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-static {v0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->access$getWeeblySquareSyncService$p(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;

    iget-object v2, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-static {v2}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->access$getMerchantToken$p(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->merchantExists(Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 308
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 309
    invoke-virtual {p1}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;->apply(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method
