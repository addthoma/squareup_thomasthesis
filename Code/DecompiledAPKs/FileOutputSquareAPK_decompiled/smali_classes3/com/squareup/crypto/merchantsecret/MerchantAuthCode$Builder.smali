.class public final Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantAuthCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;",
        "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public keygen_timestamp:Ljava/lang/Long;

.field public master_key_id:Ljava/lang/Integer;

.field public payload_mac:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
    .locals 5

    .line 155
    new-instance v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->master_key_id:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->keygen_timestamp:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->payload_mac:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->build()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    move-result-object v0

    return-object v0
.end method

.method public keygen_timestamp(Ljava/lang/Long;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->keygen_timestamp:Ljava/lang/Long;

    return-object p0
.end method

.method public master_key_id(Ljava/lang/Integer;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->master_key_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public payload_mac(Lokio/ByteString;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->payload_mac:Lokio/ByteString;

    return-object p0
.end method
