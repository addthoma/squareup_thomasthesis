.class public final Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory;
.super Ljava/lang/Object;
.source "ReaderMessageBar_NoMessageBar_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory$InstanceHolder;->access$000()Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;

    invoke-direct {v0}, Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory;->newInstance()Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/messagebar/api/ReaderMessageBar_NoMessageBar_Factory;->get()Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;

    move-result-object v0

    return-object v0
.end method
