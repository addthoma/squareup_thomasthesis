.class final enum Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;
.super Ljava/lang/Enum;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BarAnimationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field public static final enum CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field public static final enum CYCLE_OUT_TO_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field public static final enum HIDE_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field public static final enum IGNORE_BAR:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 692
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v1, 0x0

    const-string v2, "IGNORE_BAR"

    invoke-direct {v0, v2, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->IGNORE_BAR:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 693
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v2, 0x1

    const-string v3, "HIDE_IMMEDIATELY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->HIDE_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 694
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v3, 0x2

    const-string v4, "CYCLE_IN_IF_GONE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 695
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v4, 0x3

    const-string v5, "CYCLE_OUT_TO_TOP"

    invoke-direct {v0, v5, v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_OUT_TO_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 691
    sget-object v5, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->IGNORE_BAR:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->HIDE_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_OUT_TO_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->$VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 691
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;
    .locals 1

    .line 691
    const-class v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;
    .locals 1

    .line 691
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->$VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v0}, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-object v0
.end method
