.class public interface abstract Lcom/squareup/ms/MinesweeperTicket;
.super Ljava/lang/Object;
.source "MinesweeperTicket.java"


# virtual methods
.method public abstract getFreshTicket()[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public abstract getTicket()Lcom/squareup/protos/client/flipper/SealedTicket;
.end method

.method public abstract setTicket(Lcom/squareup/protos/client/flipper/SealedTicket;)V
.end method
