.class public interface abstract Lcom/squareup/ms/Minesweeper;
.super Ljava/lang/Object;
.source "Minesweeper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ms/Minesweeper$MinesweeperLogger;,
        Lcom/squareup/ms/Minesweeper$DataListener;
    }
.end annotation


# virtual methods
.method public abstract addStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract passDataToMinesweeper([B)V
.end method

.method public abstract removeStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
.end method

.method public abstract updateTicketAsync()V
.end method
