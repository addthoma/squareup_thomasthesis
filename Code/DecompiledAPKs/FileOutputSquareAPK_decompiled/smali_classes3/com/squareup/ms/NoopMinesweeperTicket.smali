.class public final Lcom/squareup/ms/NoopMinesweeperTicket;
.super Ljava/lang/Object;
.source "NoopMinesweeperTicket.kt"

# interfaces
.implements Lcom/squareup/ms/MinesweeperTicket;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ms/NoopMinesweeperTicket;",
        "Lcom/squareup/ms/MinesweeperTicket;",
        "()V",
        "getFreshTicket",
        "",
        "getTicket",
        "Lcom/squareup/protos/client/flipper/SealedTicket;",
        "setTicket",
        "",
        "newTicket",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ms/NoopMinesweeperTicket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/ms/NoopMinesweeperTicket;

    invoke-direct {v0}, Lcom/squareup/ms/NoopMinesweeperTicket;-><init>()V

    sput-object v0, Lcom/squareup/ms/NoopMinesweeperTicket;->INSTANCE:Lcom/squareup/ms/NoopMinesweeperTicket;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFreshTicket()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTicket()Lcom/squareup/protos/client/flipper/SealedTicket;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setTicket(Lcom/squareup/protos/client/flipper/SealedTicket;)V
    .locals 1

    const-string v0, "newTicket"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
