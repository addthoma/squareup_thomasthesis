.class public interface abstract Lcom/squareup/ms/MinesweeperService;
.super Ljava/lang/Object;
.source "MinesweeperService.java"


# virtual methods
.method public abstract sendMinesweeperFrame(Lcom/squareup/protos/client/flipper/GetTicketRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/flipper/GetTicketRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/flipper/GetTicketRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/flipper/GetTicketResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/flipper/ticket"
    .end annotation
.end method
