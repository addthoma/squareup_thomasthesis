.class public final Lcom/squareup/fonts/FontUtilsKt;
.super Ljava/lang/Object;
.source "FontUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "configureOptimalTextFlags",
        "",
        "Landroid/graphics/Paint;",
        "fonts-utils_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final configureOptimalTextFlags(Landroid/graphics/Paint;)V
    .locals 1

    const-string v0, "$this$configureOptimalTextFlags"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x1

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setFlags(I)V

    return-void
.end method
