.class public final Lcom/squareup/invoices/PartialTransactionsKt;
.super Ljava/lang/Object;
.source "PartialTransactions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPartialTransactions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PartialTransactions.kt\ncom/squareup/invoices/PartialTransactionsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,198:1\n704#2:199\n777#2,2:200\n1360#2:202\n1429#2,3:203\n1866#2,7:206\n704#2:213\n777#2,2:214\n1360#2:216\n1429#2,3:217\n1866#2,7:220\n9338#3:227\n9671#3,3:228\n*E\n*S KotlinDebug\n*F\n+ 1 PartialTransactions.kt\ncom/squareup/invoices/PartialTransactionsKt\n*L\n129#1:199\n129#1,2:200\n130#1:202\n130#1,3:203\n131#1,7:206\n153#1:213\n153#1,2:214\n154#1:216\n154#1,3:217\n155#1,7:220\n188#1:227\n188#1,3:228\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u0002\u001a\u001c\u0010\u0004\u001a\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0006\u0010\t\u001a\u00020\n\u001a\u001c\u0010\u000b\u001a\u00020\u00052\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00072\u0006\u0010\t\u001a\u00020\n\u001a>\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002\u001a6\u0010\u0019\u001a\n \u001b*\u0004\u0018\u00010\u001a0\u001a2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u001d\u001a\u00020\nH\u0002\u001a\u0012\u0010\u001e\u001a\u00020\u001f2\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u0003\u001a2\u0010 \u001a\u00020!2\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00072\u0006\u0010\"\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\n\u001a\u0012\u0010#\u001a\u00020\u001a*\u00020\r2\u0006\u0010\u001d\u001a\u00020\n\u001a\u0012\u0010#\u001a\u00020\u001a*\u00020\u00082\u0006\u0010\u001d\u001a\u00020\n\u001a\u001a\u0010$\u001a\u00020\u000f*\u00020\r2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u0016\u001a\"\u0010$\u001a\u00020\u000f*\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0015\u001a\u00020\u0016\u001a\u000c\u0010\'\u001a\u00020!*\u00020\u0011H\u0002\u00a8\u0006("
    }
    d2 = {
        "brandFromString",
        "Lcom/squareup/Card$Brand;",
        "brand",
        "",
        "getPaidAmount",
        "Lcom/squareup/protos/common/Money;",
        "payments",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getRefundedAmount",
        "refunds",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
        "getStatusTextPhrase",
        "",
        "tenderType",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
        "lastFour",
        "date",
        "Ljava/util/Date;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "kotlin.jvm.PlatformType",
        "amount",
        "defaultCurrencyCode",
        "humanBrandNameRes",
        "",
        "isPartiallyPaid",
        "",
        "invoiceAmount",
        "getGlyph",
        "getStatusText",
        "locale",
        "Ljava/util/Locale;",
        "isCardOrCoF",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final brandFromString(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 5

    if-eqz p0, :cond_1

    .line 188
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 228
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    .line 188
    invoke-virtual {v4}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 230
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 188
    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-static {p0}, Lcom/squareup/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p0

    goto :goto_1

    .line 191
    :cond_1
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :goto_1
    return-object p0
.end method

.method public static final getGlyph(Lcom/squareup/protos/client/invoice/InvoiceRefund;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    const-string v0, "$this$getGlyph"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCurrencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/invoices/PartialTransactionsKt;->glyph(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    const-string p1, "glyph(tender_type, brand\u2026ney, defaultCurrencyCode)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getGlyph(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    const-string v0, "$this$getGlyph"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCurrencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/invoices/PartialTransactionsKt;->glyph(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    const-string p1, "glyph(tender_type, brand\u2026unt, defaultCurrencyCode)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getPaidAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "payments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 124
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 126
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 128
    :cond_0
    check-cast p0, Ljava/lang/Iterable;

    .line 199
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 200
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    .line 129
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 202
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 203
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 204
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    .line 130
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    move-object v1, p1

    :goto_3
    invoke-static {v2, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 205
    :cond_5
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 206
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 207
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 208
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 209
    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 210
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 131
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_4

    :cond_6
    const-string p0, "payments\n      .filter {\u2026neyMath.sum(acc, money) }"

    .line 212
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/common/Money;

    return-object p1

    .line 207
    :cond_7
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Empty collection can\'t be reduced."

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getRefundedAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "refunds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 148
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 150
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 152
    :cond_0
    check-cast p0, Ljava/lang/Iterable;

    .line 213
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 214
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    .line 153
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 215
    :cond_3
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 216
    new-instance p0, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p0, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 217
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 218
    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    .line 154
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    invoke-interface {p0, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 219
    :cond_4
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 220
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 221
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 222
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 223
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 155
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_3

    :cond_5
    const-string p0, "refunds\n      .filter { \u2026neyMath.sum(acc, money) }"

    .line 226
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/common/Money;

    return-object p1

    .line 221
    :cond_6
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Empty collection can\'t be reduced."

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getStatusText(Lcom/squareup/protos/client/invoice/InvoiceRefund;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 7

    const-string v0, "$this$getStatusText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->tryParseIso8601Date(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object v4

    const-string p0, "ProtoDates.tryParseIso8601Date(refunded_at)"

    invoke-static {v4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p2

    move-object v6, p1

    .line 72
    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/PartialTransactionsKt;->getStatusTextPhrase(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final getStatusText(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 7

    const-string v0, "$this$getStatusText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {p0, p2}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v4

    const-string p0, "ProtoTimes.asDate(tendered_at, locale)"

    invoke-static {v4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p3

    move-object v6, p1

    .line 65
    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/PartialTransactionsKt;->getStatusTextPhrase(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private static final getStatusTextPhrase(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p0, :cond_2

    .line 84
    invoke-static {p0}, Lcom/squareup/invoices/PartialTransactionsKt;->isCardOrCoF(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    goto :goto_1

    .line 87
    :cond_0
    sget-object v0, Lcom/squareup/invoices/PartialTransactionsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 101
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 102
    :pswitch_0
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_other_payment_status:I

    .line 101
    invoke-interface {p4, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_2

    .line 98
    :pswitch_1
    sget p1, Lcom/squareup/common/invoices/R$string;->partial_payments_cash_check_payment_status:I

    invoke-interface {p4, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p2, "payment_type"

    .line 98
    invoke-virtual {p1, p2, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_2

    :pswitch_2
    if-eqz p2, :cond_1

    .line 91
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_card_payment_status:I

    invoke-interface {p4, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 92
    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "last_four"

    invoke-virtual {p0, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_0

    .line 94
    :cond_1
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_card_payment_status_no_last_four:I

    invoke-interface {p4, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 96
    :goto_0
    invoke-static {p1}, Lcom/squareup/invoices/PartialTransactionsKt;->humanBrandNameRes(Ljava/lang/String;)I

    move-result p1

    invoke-interface {p4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "brand"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_2

    .line 85
    :cond_2
    :goto_1
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_other_payment_status:I

    invoke-interface {p4, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 106
    :goto_2
    invoke-virtual {p5, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "date"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 107
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "phrase.put(\"date\", dateF\u2026at(date))\n      .format()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static final glyph(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    if-nez p0, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    sget-object v0, Lcom/squareup/invoices/PartialTransactionsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 52
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :goto_0
    :pswitch_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_3

    .line 50
    :pswitch_1
    invoke-static {p1}, Lcom/squareup/invoices/PartialTransactionsKt;->brandFromString(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p0

    if-eqz p2, :cond_1

    iget-object p1, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, p3

    .line 49
    :goto_1
    invoke-static {p0, p1}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    goto :goto_3

    :pswitch_2
    if-eqz p2, :cond_2

    .line 48
    iget-object p0, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz p0, :cond_2

    goto :goto_2

    :cond_2
    move-object p0, p3

    :goto_2
    invoke-static {p0}, Lcom/squareup/util/ProtoGlyphs;->cash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    :goto_3
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static final humanBrandNameRes(Ljava/lang/String;)I
    .locals 0

    .line 185
    invoke-static {p0}, Lcom/squareup/invoices/PartialTransactionsKt;->brandFromString(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p0

    iget p0, p0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    return p0
.end method

.method private static final isCardOrCoF(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Z
    .locals 1

    .line 197
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CARD:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->INSTRUMENT:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isPartiallyPaid(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")Z"
        }
    .end annotation

    const-string v0, "payments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refunds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCurrencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-static {p0, p3}, Lcom/squareup/invoices/PartialTransactionsKt;->getPaidAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 174
    invoke-static {p0}, Lcom/squareup/money/MoneyMath;->greaterThanZero(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 176
    :cond_0
    invoke-static {p1, p3}, Lcom/squareup/invoices/PartialTransactionsKt;->getRefundedAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 177
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 179
    invoke-static {p0}, Lcom/squareup/money/MoneyMath;->greaterThanZero(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {p2, p0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
