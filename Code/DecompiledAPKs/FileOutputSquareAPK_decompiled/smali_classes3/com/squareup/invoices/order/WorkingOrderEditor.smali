.class public final Lcom/squareup/invoices/order/WorkingOrderEditor;
.super Ljava/lang/Object;
.source "WorkingOrderEditor.kt"

# interfaces
.implements Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/invoices/order/WorkingOrderEditor$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/order/WorkingOrderEditor$SharedScope;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001)B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u000e\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0008\u001a\u00020\tJ\u0012\u0010\u0019\u001a\u00020\u00122\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0012H\u0016J\u0006\u0010\u001d\u001a\u00020\u0012J\u000e\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 J\u0016\u0010!\u001a\u00020\u00122\u0006\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u0017J\u000e\u0010$\u001a\u00020\u00122\u0006\u0010%\u001a\u00020&J\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\t0(R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u00020\t8VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u000c\u0010\rR\u001c\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\t0\t0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
        "Lmortar/Scoped;",
        "workingOrderLogger",
        "Lcom/squareup/invoices/order/WorkingOrderLogger;",
        "(Lcom/squareup/invoices/order/WorkingOrderLogger;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "order",
        "Lcom/squareup/payment/Order;",
        "order$annotations",
        "()V",
        "getOrder",
        "()Lcom/squareup/payment/Order;",
        "orderRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "kotlin.jvm.PlatformType",
        "addDiscountToCart",
        "",
        "discount",
        "Lcom/squareup/checkout/Discount;",
        "addItemToCart",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "init",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "popUninterestingItem",
        "removeItemFromCart",
        "indexToRemove",
        "",
        "replaceItemInCart",
        "indexToReplace",
        "orderItem",
        "setCustomer",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "workingOrder",
        "Lio/reactivex/Observable;",
        "SharedScope",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation
.end field

.field private final workingOrderLogger:Lcom/squareup/invoices/order/WorkingOrderLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/order/WorkingOrderLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "workingOrderLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrderLogger:Lcom/squareup/invoices/order/WorkingOrderLogger;

    .line 35
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<Order>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getOrderRelay$p(Lcom/squareup/invoices/order/WorkingOrderEditor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static synthetic order$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use workingOrder() instead."
    .end annotation

    return-void
.end method


# virtual methods
.method public addDiscountToCart(Lcom/squareup/checkout/Discount;)V
    .locals 2

    const-string v0, "discount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrderLogger:Lcom/squareup/invoices/order/WorkingOrderLogger;

    invoke-interface {v0}, Lcom/squareup/invoices/order/WorkingOrderLogger;->logAddDiscount()V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;-><init>(Lcom/squareup/checkout/Discount;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public addItemToCart(Lcom/squareup/checkout/CartItem;)V
    .locals 2

    const-string v0, "cartItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrderLogger:Lcom/squareup/invoices/order/WorkingOrderLogger;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "No Item ID"

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderLogger;->logAddItem(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$addItemToCart$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$addItemToCart$1;-><init>(Lcom/squareup/checkout/CartItem;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Order;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WorkingOrderEditor has not been initialized. Call init()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final init(Lcom/squareup/payment/Order;)V
    .locals 2

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "order.onCustomerChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$init$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$init$1;-><init>(Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/payment/Order;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "order.onCustomerChanged(\u2026y.accept(order)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public final popUninterestingItem()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/order/WorkingOrderEditor$popUninterestingItem$1;->INSTANCE:Lcom/squareup/invoices/order/WorkingOrderEditor$popUninterestingItem$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final removeItemFromCart(I)V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$removeItemFromCart$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$removeItemFromCart$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final replaceItemInCart(ILcom/squareup/checkout/CartItem;)V
    .locals 2

    const-string v0, "orderItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v0, v1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrderLogger:Lcom/squareup/invoices/order/WorkingOrderLogger;

    invoke-interface {v0}, Lcom/squareup/invoices/order/WorkingOrderLogger;->logAddOrEditCustomAmount()V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$replaceItemInCart$1;

    invoke-direct {v1, p1, p2}, Lcom/squareup/invoices/order/WorkingOrderEditor$replaceItemInCart$1;-><init>(ILcom/squareup/checkout/CartItem;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final setCustomer(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/order/WorkingOrderEditor$setCustomer$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$setCustomer$1;-><init>(Lcom/squareup/protos/client/rolodex/Contact;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditorKt;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final workingOrder()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor;->orderRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
