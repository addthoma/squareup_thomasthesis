.class public final Lcom/squareup/invoices/PaymentRequestsKt;
.super Ljava/lang/Object;
.source "PaymentRequests.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequests.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequests.kt\ncom/squareup/invoices/PaymentRequestsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,489:1\n704#2:490\n777#2,2:491\n1360#2:493\n1429#2,3:494\n1866#2,7:497\n1642#2,2:504\n704#2:506\n777#2,2:507\n250#2,2:509\n310#2,7:511\n1550#2,3:518\n1847#2,3:521\n310#2,7:524\n1529#2,3:531\n1360#2:534\n1429#2,3:535\n1370#2:538\n1401#2,4:539\n1360#2:543\n1429#2,3:544\n1370#2:547\n1401#2,4:548\n1370#2:552\n1401#2,4:553\n1360#2:557\n1429#2,3:558\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequests.kt\ncom/squareup/invoices/PaymentRequestsKt\n*L\n192#1:490\n192#1,2:491\n193#1:493\n193#1,3:494\n194#1,7:497\n214#1,2:504\n235#1:506\n235#1,2:507\n317#1,2:509\n355#1,7:511\n366#1,3:518\n402#1,3:521\n409#1,7:524\n420#1,3:531\n423#1:534\n423#1,3:535\n425#1:538\n425#1,4:539\n437#1:543\n437#1,3:544\n439#1:547\n439#1,4:548\n446#1:552\n446#1,4:553\n451#1:557\n451#1,3:558\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0001H\u0002\u001a\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002\u001a\u0018\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002\u001a&\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\n\u001a\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0015\u001a\u0012\u0010\u0017\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0001\u001a,\u0010\u0019\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u001a0\u0015*\u0008\u0012\u0004\u0012\u00020\u00030\u00152\u0006\u0010\u001b\u001a\u00020\u0001H\u0002\u001a.\u0010\u001c\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u001a\u0018\u00010\u0015*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00152\u0006\u0010\u0004\u001a\u00020\u0001\u001a\u0018\u0010\u001d\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u00152\u0006\u0010\u0004\u001a\u00020\u0001\u001a\u0012\u0010\u001e\u001a\u00020\u0011*\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u0011\u001a\u0018\u0010 \u001a\u00020!*\u0008\u0012\u0004\u0012\u00020\u00030\u00152\u0006\u0010\"\u001a\u00020!\u001a\u001c\u0010#\u001a\u0004\u0018\u00010\u0001*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00152\u0006\u0010\u0004\u001a\u00020\u0001\u001a\u001c\u0010$\u001a\u0004\u0018\u00010%*\u00020\u00032\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020%\u0018\u00010\u0015\u001a\u0016\u0010\'\u001a\u0004\u0018\u00010\u0001*\u00020\u00032\u0008\u0010(\u001a\u0004\u0018\u00010)\u001a\u0014\u0010*\u001a\u0004\u0018\u00010\u0003*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0015\u001a3\u0010+\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030,*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00152\u0012\u0010-\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020/0.\"\u00020/\u00a2\u0006\u0002\u00100\u001a\u0012\u00101\u001a\u00020\r*\u00020\u00032\u0006\u0010\t\u001a\u00020\n\u001a\u0012\u00102\u001a\u00020\u0003*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0015\u001a\u0012\u00103\u001a\u000204*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0015\u001a\n\u00105\u001a\u000204*\u00020\u0003\u001a\n\u00106\u001a\u000204*\u00020\u0003\u001a\n\u00107\u001a\u000204*\u00020\u0003\u001a\n\u00108\u001a\u000204*\u00020\u0003\u001a\u001e\u00109\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0015*\u0008\u0012\u0004\u0012\u00020\u00030\u00152\u0006\u0010\t\u001a\u00020\n\u001a\u0016\u0010:\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0015*\u0008\u0012\u0004\u0012\u00020\u00030\u0015\u001a\u001a\u0010;\u001a\u00020<*\u00020<2\u0006\u0010\u001f\u001a\u00020\u00112\u0006\u0010=\u001a\u00020\u0011\u00a8\u0006>"
    }
    d2 = {
        "calculatePercentageAmount",
        "Lcom/squareup/protos/common/Money;",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceAmount",
        "dueDaysAgo",
        "Lcom/squareup/phrase/Phrase;",
        "days",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dueWithinDays",
        "formatDueDate",
        "",
        "dateType",
        "Lcom/squareup/invoices/DateType;",
        "dueDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "sortedPaymentRequests",
        "",
        "paymentRequests",
        "calculateAmount",
        "totalMoney",
        "calculateAmountPercentageInstallments",
        "Lkotlin/Pair;",
        "balanceAmount",
        "calculateAmounts",
        "calculateBalanceAmount",
        "calculateDueDate",
        "firstSentDate",
        "calculateInstallmentNumber",
        "",
        "index",
        "calculateRemainderAmount",
        "findPaymentRequestState",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "paymentRequestStates",
        "getCompletedAmount",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails$Invoice;",
        "getDeposit",
        "getIndexedPaymentRequestByTypes",
        "Lkotlin/collections/IndexedValue;",
        "amountTypes",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
        "(Ljava/util/List;[Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lkotlin/collections/IndexedValue;",
        "getPaymentRequestType",
        "getRemainderPaymentRequest",
        "hasDepositRequest",
        "",
        "isDepositRequest",
        "isFixedAmountType",
        "isInstallment",
        "isPercentageType",
        "labels",
        "sorted",
        "updateDueDate",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "newDueDate",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "$this$calculateAmount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalMoney"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v0, :cond_3

    sget-object v1, Lcom/squareup/invoices/PaymentRequestsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 p1, 0x5

    if-eq v0, p1, :cond_0

    goto :goto_1

    .line 159
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Can\'t calculate the amount for a REMAINDER; use calculateRemainderAmount instead."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 156
    :cond_1
    invoke-static {p0, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculatePercentageAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    goto :goto_0

    .line 155
    :cond_2
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    const-string p1, "fixed_amount"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0

    .line 162
    :cond_3
    :goto_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown Amount Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private static final calculateAmountPercentageInstallments(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation

    .line 420
    check-cast p0, Ljava/lang/Iterable;

    .line 531
    instance-of v0, p0, Ljava/util/Collection;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 532
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 420
    iget-object v3, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    const/4 v2, 0x0

    :cond_3
    :goto_1
    if-eqz v2, :cond_7

    .line 534
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 535
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 536
    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 423
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v5, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 537
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 424
    invoke-static {v0, p1}, Lcom/squareup/invoices/PaymentRequestPercentageCalculatorsKt;->calculatePercentageAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    .line 538
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 540
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_5

    .line 541
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_5
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 427
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_3

    .line 542
    :cond_6
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 421
    :cond_7
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Must have only percentage installments"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation

    const-string v0, "invoiceAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 211
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 214
    move-object v1, p0

    check-cast v1, Ljava/lang/Iterable;

    .line 504
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v3, p1

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 215
    iget-object v6, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v6, :cond_4

    sget-object v7, Lcom/squareup/invoices/PaymentRequestsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v6}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v6

    aget v6, v7, v6

    if-eq v6, v5, :cond_3

    const/4 v5, 0x2

    if-eq v6, v5, :cond_2

    const/4 v5, 0x3

    if-eq v6, v5, :cond_1

    const/4 v5, 0x4

    if-eq v6, v5, :cond_1

    const/4 v4, 0x5

    goto :goto_0

    .line 219
    :cond_1
    invoke-static {v4, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 220
    invoke-static {v3, v5}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const-string v6, "MoneyMath.subtract(remainingInvoiceAmount, amount)"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_2
    iget-object v5, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v4, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_3
    invoke-static {v4, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Payment request must have amount type"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 234
    :cond_5
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result p0

    if-eq p1, p0, :cond_9

    .line 506
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    check-cast p0, Ljava/util/Collection;

    .line 507
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 235
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v2, v4, :cond_7

    const/4 v2, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_6

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 508
    :cond_8
    check-cast p0, Ljava/util/List;

    .line 236
    invoke-static {p0, v3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmountPercentageInstallments(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p0

    .line 237
    check-cast p0, Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    return-object v0
.end method

.method public static final calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "$this$calculateBalanceAmount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    check-cast p0, Ljava/lang/Iterable;

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    if-eqz v0, :cond_2

    .line 172
    invoke-static {v0, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 173
    invoke-static {p1, p0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    if-eqz p0, :cond_2

    goto :goto_1

    :cond_2
    move-object p0, p1

    :goto_1
    return-object p0
.end method

.method public static final calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 7

    const-string v0, "$this$calculateDueDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, -0x80000000

    int-to-long v0, v0

    const v2, 0x7fffffff

    int-to-long v2, v2

    .line 258
    iget-object v4, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    const-string v5, "relative_due_on"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-gtz v6, :cond_0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 263
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int p0, v0

    invoke-static {p1, p0}, Lcom/squareup/util/ProtoDates;->addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    const-string p1, "addDays(firstSentDate, relative_due_on.toInt())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 259
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot calculate due date: relative_due_on not within int bounds. Value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 259
    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final calculateInstallmentNumber(Ljava/util/List;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;I)I"
        }
    .end annotation

    const-string v0, "$this$calculateInstallmentNumber"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 402
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 521
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 522
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 402
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_6

    .line 405
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 525
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 526
    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 409
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, -0x1

    :goto_2
    sub-int/2addr p1, v2

    add-int/2addr p1, v3

    return p1

    .line 406
    :cond_5
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " must refer to the index of an installment."

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 405
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 402
    :cond_6
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "To calculate payment number, list must contains installments."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final calculatePercentageAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 3

    .line 486
    new-instance v0, Ljava/math/BigDecimal;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    const-string v1, "paymentRequest.percentage_amount"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    sget-object p0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    .line 485
    invoke-static {p1, v0, p0}, Lcom/squareup/money/MoneyMath;->multiply(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-wide/16 v0, 0x64

    .line 487
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    sget-object v0, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    .line 482
    invoke-static {p0, p1, v0}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string p1, "MoneyMath.divide(\n    //\u2026valueOf(100), HALF_DOWN\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final calculateRemainderAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "invoiceAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p0, :cond_0

    const/4 p1, 0x0

    goto/16 :goto_4

    .line 189
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string p1, "invoiceAmount.currency_code"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto/16 :goto_4

    .line 191
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    goto/16 :goto_4

    .line 192
    :cond_2
    check-cast p0, Ljava/lang/Iterable;

    .line 490
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 491
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 192
    iget-object v3, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v3, v4, :cond_4

    const/4 v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 492
    :cond_5
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 493
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 494
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 495
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 193
    invoke-static {v1, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 496
    :cond_6
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 497
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 498
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 499
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 500
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 501
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 194
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "MoneyMath.sum(acc, amount)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 195
    :cond_7
    check-cast v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_4
    return-object p1

    .line 498
    :cond_8
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Empty collection can\'t be reduced."

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final dueDaysAgo(JLcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;
    .locals 3

    const-wide/16 v0, 0x1

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    .line 460
    sget v0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_within_past_date_singular:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    goto :goto_0

    .line 462
    :cond_0
    sget v0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_within_past_date_plural:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 464
    :goto_0
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "days"

    invoke-virtual {p2, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "if (days == 1L) {\n    re\u2026(\"days\", days.toString())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final dueWithinDays(JLcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;
    .locals 3

    const-wide/16 v0, 0x1

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    .line 472
    sget v0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_within_future_date_singular:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    goto :goto_0

    .line 474
    :cond_0
    sget v0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_within_future_date_plural:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 476
    :goto_0
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "days"

    invoke-virtual {p2, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "if (days == 1L) {\n    re\u2026(\"days\", days.toString())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final findPaymentRequestState(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;"
        }
    .end annotation

    const-string v0, "$this$findPaymentRequestState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 317
    check-cast p1, Ljava/lang/Iterable;

    .line 509
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    .line 317
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->token:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 510
    :cond_1
    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    :cond_2
    return-object v0
.end method

.method public static final formatDueDate(Lcom/squareup/invoices/DateType;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 4

    const-string v0, "dateType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    instance-of v0, p0, Lcom/squareup/invoices/DateType$DateOnly;

    const-string v1, "due_date"

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_date:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 87
    invoke-static {p1, p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "res.phrase(R.string.part\u2026atter))\n        .format()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 90
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/DateType$DaysUntil;

    if-eqz v0, :cond_5

    .line 91
    check-cast p0, Lcom/squareup/invoices/DateType$DaysUntil;

    invoke-virtual {p0}, Lcom/squareup/invoices/DateType$DaysUntil;->getClock()Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lcom/squareup/invoices/DateType$DaysUntil;->getSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    .line 96
    invoke-static {v0, p1}, Lcom/squareup/util/YearMonthDaysKt;->isSameDayAs(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_today:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "today"

    .line 99
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/util/YearMonthDaysKt;->isAfter(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0, p1}, Lcom/squareup/util/YearMonthDaysKt;->isSameDayAs(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_due_upon_receipt:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_0

    .line 103
    :cond_2
    invoke-static {p1, v0}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 104
    invoke-static {v0, p1}, Lcom/squareup/util/YearMonthDaysKt;->minus(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 105
    invoke-static {v2, v3, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->dueDaysAgo(JLcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_0

    .line 109
    :cond_3
    invoke-static {p0, v0}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 110
    invoke-static {p1, v0}, Lcom/squareup/util/YearMonthDaysKt;->minus(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 111
    invoke-static {v2, v3, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->dueWithinDays(JLcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    goto :goto_0

    .line 116
    :cond_4
    invoke-static {p1, p0}, Lcom/squareup/util/YearMonthDaysKt;->minus(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v2

    .line 117
    invoke-static {v2, v3, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->dueWithinDays(JLcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 121
    :goto_0
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 122
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "phrase\n          .put(\"d\u2026te)))\n          .format()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final getCompletedAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/DisplayDetails$Invoice;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "$this$getCompletedAmount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 389
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 388
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->findPaymentRequestState(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    :cond_1
    return-object v0
.end method

.method public static final getDeposit(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 336
    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getIndexedPaymentRequestByTypes(Ljava/util/List;[Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lkotlin/collections/IndexedValue;

    move-result-object p0

    invoke-virtual {p0}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object p0
.end method

.method public static final varargs getIndexedPaymentRequestByTypes(Ljava/util/List;[Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lkotlin/collections/IndexedValue;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;[",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            ")",
            "Lkotlin/collections/IndexedValue<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "amountTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_3

    .line 349
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 512
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 513
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 355
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-static {p1, v2}, Lkotlin/collections/ArraysKt;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    .line 357
    :goto_1
    new-instance p1, Lkotlin/collections/IndexedValue;

    if-gez v0, :cond_2

    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    :goto_2
    invoke-direct {p1, v0, p0}, Lkotlin/collections/IndexedValue;-><init>(ILjava/lang/Object;)V

    return-object p1

    .line 350
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Didn\'t have any PaymentRequest; this should never happen."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "$this$getPaymentRequestType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/invoices/PaymentRequestsKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_1

    .line 288
    :goto_0
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_unknown:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    goto :goto_1

    .line 286
    :cond_1
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_installment:I

    .line 285
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    goto :goto_1

    .line 283
    :cond_2
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_deposit:I

    .line 282
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    goto :goto_1

    .line 281
    :cond_3
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_balance:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    :goto_1
    return-object p0
.end method

.method public static final getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 327
    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getIndexedPaymentRequestByTypes(Ljava/util/List;[Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lkotlin/collections/IndexedValue;

    move-result-object p0

    invoke-virtual {p0}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Didn\'t have a REMAINDER type PaymentRequest."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final hasDepositRequest(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_4

    .line 366
    check-cast p0, Ljava/lang/Iterable;

    .line 518
    instance-of v2, p0, Ljava/util/Collection;

    if-eqz v2, :cond_0

    move-object v2, p0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 519
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 366
    iget-object v3, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v3, v4, :cond_3

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v3, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v2, v3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_4
    :goto_2
    return v1
.end method

.method public static final isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z
    .locals 2

    const-string v0, "$this$isDepositRequest"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isFixedAmountType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z
    .locals 2

    const-string v0, "$this$isFixedAmountType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z
    .locals 2

    const-string v0, "$this$isInstallment"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isPercentageType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z
    .locals 2

    const-string v0, "$this$isPercentageType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final labels(Ljava/util/List;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$labels"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 434
    sget-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v0

    .line 436
    sget-object v1, Lcom/squareup/invoices/PaymentRequestsKt$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/16 v2, 0xa

    if-eq v0, v1, :cond_9

    const/4 v1, 0x2

    if-eq v0, v1, :cond_9

    const/4 v1, 0x3

    const-string v3, "number"

    const/4 v4, 0x0

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 p1, 0x5

    if-ne v0, p1, :cond_1

    .line 451
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid configuration type: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p0, Ljava/lang/Iterable;

    .line 557
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 558
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 559
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 451
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 560
    :cond_0
    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 451
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 446
    :cond_2
    check-cast p0, Ljava/lang/Iterable;

    .line 552
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 554
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v4, 0x1

    if-gez v4, :cond_3

    .line 555
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 447
    sget v1, Lcom/squareup/common/invoices/R$string;->payment_request_number:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 448
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 449
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v4, v2

    goto :goto_1

    .line 556
    :cond_4
    check-cast v0, Ljava/util/List;

    goto :goto_5

    .line 439
    :cond_5
    check-cast p0, Ljava/lang/Iterable;

    .line 547
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 549
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v4, 0x1

    if-gez v4, :cond_6

    .line 550
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_6
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 440
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {v1, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_3

    .line 441
    :cond_7
    sget v1, Lcom/squareup/common/invoices/R$string;->payment_request_number:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 442
    invoke-virtual {v1, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 443
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v4, v2

    goto :goto_2

    .line 551
    :cond_8
    check-cast v0, Ljava/util/List;

    goto :goto_5

    .line 437
    :cond_9
    check-cast p0, Ljava/lang/Iterable;

    .line 543
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 544
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 545
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 437
    invoke-static {v1, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 546
    :cond_a
    check-cast v0, Ljava/util/List;

    :goto_5
    return-object v0
.end method

.method public static final sorted(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$sorted"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-static {p0}, Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    check-cast p0, Ljava/lang/Iterable;

    sget-object v0, Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;->INSTANCE:Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final updateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;
    .locals 1

    const-string v0, "$this$updateDueDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newDueDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-static {p1, p2}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object p0

    const-string p1, "relative_due_on(countDay\u2026rstSentDate, newDueDate))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
