.class public final Lcom/squareup/invoices/InvoiceTimelineCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InvoiceTimelineCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u000e\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoiceTimelineCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;",
        "invoiceTimelineViewFactory",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "content",
        "Landroid/widget/LinearLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "actionBarTitle",
        "",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private content:Landroid/widget/LinearLayout;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

.field private final runner:Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTimelineViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->runner:Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    iput-object p3, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public static final synthetic access$getContent$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Landroid/widget/LinearLayout;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->content:Landroid/widget/LinearLayout;

    if-nez p0, :cond_0

    const-string v0, "content"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getInvoiceTimelineViewFactory$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->runner:Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$setContent$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->content:Landroid/widget/LinearLayout;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/invoices/InvoiceTimelineCoordinator;Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->spinnerData(Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 65
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 66
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_detail_timeline_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->content:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final spinnerData(Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 53
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;->isBusy()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->runner:Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;->timelineScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$1;-><init>(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/invoices/InvoiceTimelineCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/InvoiceTimelineCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTransform(::spinnerData)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;-><init>(Lcom/squareup/invoices/InvoiceTimelineCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.timelineScreenDat\u2026  )\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public final configureActionBar(Ljava/lang/String;)V
    .locals 3

    const-string v0, "actionBarTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 58
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 59
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 60
    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/InvoiceTimelineCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
