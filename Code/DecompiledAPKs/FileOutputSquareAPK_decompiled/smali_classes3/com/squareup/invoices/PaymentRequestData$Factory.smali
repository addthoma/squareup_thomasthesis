.class public final Lcom/squareup/invoices/PaymentRequestData$Factory;
.super Ljava/lang/Object;
.source "PaymentRequestData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/PaymentRequestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestData.kt\ncom/squareup/invoices/PaymentRequestData$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,135:1\n1360#2:136\n1429#2,3:137\n1370#2:140\n1401#2,4:141\n1360#2:145\n1429#2,3:146\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequestData.kt\ncom/squareup/invoices/PaymentRequestData$Factory\n*L\n55#1:136\n55#1,3:137\n58#1:140\n58#1,4:141\n115#1:145\n115#1,3:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u001a\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0014J:\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00142\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00142\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00142\u0006\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestData$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "displayStateFactory",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;Lcom/squareup/util/Clock;)V",
        "createForDetails",
        "Lcom/squareup/invoices/PaymentRequestData;",
        "label",
        "",
        "paymentRequestReadOnlyInfo",
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
        "",
        "paymentRequestReadOnlyInfoList",
        "createForEditing",
        "paymentRequests",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "paymentRequestStates",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "invoiceAmount",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final displayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;Lcom/squareup/util/Clock;)V
    .locals 1
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;",
            "Lcom/squareup/util/Clock;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayStateFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->displayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

    iput-object p5, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final createForDetails(Ljava/lang/CharSequence;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;)Lcom/squareup/invoices/PaymentRequestData;
    .locals 10

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->displayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;->fromPaymentRequestReadOnlyInfo(Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;)Lcom/squareup/invoices/PaymentRequestDisplayState;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/PaymentRequestDisplayState;->getDisplayString(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 72
    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState;->getColorId()I

    move-result v9

    .line 75
    sget-object v0, Lcom/squareup/invoices/DateType$DateOnly;->INSTANCE:Lcom/squareup/invoices/DateType$DateOnly;

    check-cast v0, Lcom/squareup/invoices/DateType;

    .line 76
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->dateFormat:Ljava/text/DateFormat;

    .line 78
    iget-object v3, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    .line 74
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/PaymentRequestsKt;->formatDueDate(Lcom/squareup/invoices/DateType;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 80
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getRequestedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 82
    new-instance p2, Lcom/squareup/invoices/PaymentRequestData;

    const-string v0, "formattedAmount"

    .line 83
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    move-object v3, p2

    move-object v4, p1

    .line 82
    invoke-direct/range {v3 .. v9}, Lcom/squareup/invoices/PaymentRequestData;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)V

    return-object p2
.end method


# virtual methods
.method public final createForDetails(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestData;",
            ">;"
        }
    .end annotation

    const-string v0, "paymentRequestReadOnlyInfoList"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    check-cast p1, Ljava/lang/Iterable;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 137
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 138
    check-cast v3, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;

    .line 55
    invoke-virtual {v3}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 56
    iget-object v2, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v2}, Lcom/squareup/invoices/PaymentRequestsKt;->labels(Ljava/util/List;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v0

    .line 140
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    const/4 v1, 0x0

    .line 142
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    if-gez v1, :cond_1

    .line 143
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v3, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;

    .line 59
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 60
    invoke-direct {p0, v1, v3}, Lcom/squareup/invoices/PaymentRequestData$Factory;->createForDetails(Ljava/lang/CharSequence;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;)Lcom/squareup/invoices/PaymentRequestData;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v1, v4

    goto :goto_1

    .line 144
    :cond_2
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method public final createForEditing(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestData;",
            ">;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 108
    invoke-static {v0, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->findPaymentRequestState(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    move-result-object p2

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 110
    iget-object p2, p3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v2, "invoiceAmount.currency_code"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 112
    :goto_0
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->greaterThanZero(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    const/4 v0, 0x1

    xor-int/2addr p2, v0

    .line 114
    invoke-static {p1, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 115
    check-cast p1, Ljava/lang/Iterable;

    .line 145
    new-instance p3, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p3, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p3, Ljava/util/Collection;

    .line 146
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 147
    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 118
    iget-object v3, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v3, v4, :cond_2

    if-eqz p2, :cond_1

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    const/4 v9, 0x0

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v9, 0x1

    .line 120
    :goto_3
    invoke-static {v2, p4}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 121
    iget-object v4, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v4}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 123
    new-instance v2, Lcom/squareup/invoices/DateType$DaysUntil;

    iget-object v4, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-direct {v2, p4, v4}, Lcom/squareup/invoices/DateType$DaysUntil;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)V

    check-cast v2, Lcom/squareup/invoices/DateType;

    .line 125
    iget-object v4, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->dateFormat:Ljava/text/DateFormat;

    .line 126
    iget-object v6, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->res:Lcom/squareup/util/Res;

    .line 122
    invoke-static {v2, v3, v4, v6}, Lcom/squareup/invoices/PaymentRequestsKt;->formatDueDate(Lcom/squareup/invoices/DateType;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 128
    iget-object v2, p0, Lcom/squareup/invoices/PaymentRequestData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 130
    new-instance v1, Lcom/squareup/invoices/PaymentRequestData;

    const-string v2, "formattedAmount"

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, ""

    move-object v8, v2

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v10, -0x1

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/squareup/invoices/PaymentRequestData;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)V

    invoke-interface {p3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 148
    :cond_3
    check-cast p3, Ljava/util/List;

    return-object p3

    .line 114
    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
