.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;
.super Ljava/lang/Object;
.source "ConfirmRemovePaymentRequestDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0003J)\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "data",
        "",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
        "",
        "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V",
        "getData",
        "()Ljava/lang/String;",
        "getEventHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "Event",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final data:Ljava/lang/String;

.field private final eventHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Companion;

    .line 28
    const-class v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->copy(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConfirmRemovePaymentRequestDialogScreen(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", eventHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
