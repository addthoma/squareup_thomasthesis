.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditPaymentRequestCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestCoordinator.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,380:1\n1103#2,7:381\n1103#2,7:388\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestCoordinator.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator\n*L\n159#1,7:381\n188#1,7:388\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 O2\u00020\u0001:\u0002OPBU\u0008\u0002\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u000e\u0008\u0001\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\n\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0016J\u0010\u00103\u001a\u0002002\u0006\u00101\u001a\u000202H\u0002J$\u00104\u001a\u0002002\u0006\u00105\u001a\u0002062\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020008H\u0002J\u0010\u0010:\u001a\u0002002\u0006\u00101\u001a\u000202H\u0016J\u0008\u0010;\u001a\u00020<H\u0002J\u0010\u0010=\u001a\u00020,2\u0006\u0010>\u001a\u00020?H\u0002J\u0008\u0010@\u001a\u000200H\u0002J\u001c\u0010A\u001a\u0002002\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020008H\u0002J\u0018\u0010B\u001a\u0002002\u0006\u0010C\u001a\u00020?2\u0006\u0010D\u001a\u00020?H\u0002J\u0018\u0010E\u001a\u0002002\u0006\u0010F\u001a\u00020\u00042\u0006\u00101\u001a\u000202H\u0002J$\u0010G\u001a\u0002002\u0006\u0010H\u001a\u00020?2\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020008H\u0002J\u0010\u0010I\u001a\u0002002\u0006\u0010>\u001a\u00020?H\u0002J\u000c\u0010J\u001a\u00020?*\u00020KH\u0002J\u0014\u0010L\u001a\u000200*\u00020*2\u0006\u0010M\u001a\u00020,H\u0002J\u0014\u0010N\u001a\u000200*\u00020*2\u0006\u0010M\u001a\u00020,H\u0002R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010+\u001a\n -*\u0004\u0018\u00010,0,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010.\u001a\n -*\u0004\u0018\u00010,0,X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Q"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "moneyFormatter",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "amountTypeRadioGroup",
        "Landroid/widget/RadioGroup;",
        "balanceAmountText",
        "Lcom/squareup/ui/account/view/LineRow;",
        "dueDate",
        "fixedAmountButton",
        "Landroid/widget/RadioButton;",
        "fixedAmountText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "fixedAmountTextWatcher",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "invoiceAmount",
        "moneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "percentageAmountTextWatcher",
        "percentageText",
        "percentageTextScrubber",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "removeRequestButton",
        "Landroid/widget/Button;",
        "summaryText",
        "Landroid/widget/TextView;",
        "zeroAmountHint",
        "",
        "kotlin.jvm.PlatformType",
        "zeroPercentageHint",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;",
        "detach",
        "getAllowedFixedAmountChars",
        "",
        "getFormattedMonetaryAmount",
        "forPercentage",
        "",
        "removeListeners",
        "setListeners",
        "swapInputs",
        "shouldShowPercentage",
        "showKeyboard",
        "update",
        "screen",
        "updateAmount",
        "isPercentage",
        "updateSummaryText",
        "isRemovable",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "setTextIfEmpty",
        "newText",
        "setTextIfNotEqual",
        "Companion",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ALLOWED_FIXED_AMOUNT_CHARS:Ljava/lang/String; = "0123456789.,"

.field public static final ALLOWED_PERCENTAGE_CHARS:Ljava/lang/String; = "0123456789%\u00a0"

.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MAX_PERCENTAGE_AMOUNT:J = 0x64L


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private amountTypeRadioGroup:Landroid/widget/RadioGroup;

.field private balanceAmountText:Lcom/squareup/ui/account/view/LineRow;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final dateFormat:Ljava/text/DateFormat;

.field private dueDate:Lcom/squareup/ui/account/view/LineRow;

.field private fixedAmountButton:Landroid/widget/RadioButton;

.field private fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

.field private fixedAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

.field private invoiceAmount:Lcom/squareup/protos/common/Money;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private percentageAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private percentageText:Lcom/squareup/widgets/SelectableEditText;

.field private percentageTextScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private removeRequestButton:Landroid/widget/Button;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;",
            ">;"
        }
    .end annotation
.end field

.field private summaryText:Landroid/widget/TextView;

.field private final zeroAmountHint:Ljava/lang/CharSequence;

.field private final zeroPercentageHint:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Companion;

    return-void
.end method

.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V
    .locals 0
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 78
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->dateFormat:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->res:Lcom/squareup/util/Res;

    .line 97
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    sget-object p2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->zeroPercentageHint:Ljava/lang/CharSequence;

    .line 98
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 p3, 0x0

    invoke-static {p3, p4, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->zeroAmountHint:Ljava/lang/CharSequence;

    .line 100
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    iget-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p3, p4, p6}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-direct {p1, p2, p5, p3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 69
    invoke-direct/range {p0 .. p7}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;Landroid/view/View;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->update(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;ZLkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->updateAmount(ZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 127
    sget v0, Lcom/squareup/features/invoices/R$id;->request_type_radio_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->amountTypeRadioGroup:Landroid/widget/RadioGroup;

    .line 128
    sget v0, Lcom/squareup/features/invoices/R$id;->percentage_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    .line 129
    sget v0, Lcom/squareup/features/invoices/R$id;->fixed_amount_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountButton:Landroid/widget/RadioButton;

    .line 130
    sget v0, Lcom/squareup/features/invoices/R$id;->fixed_amount_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    .line 131
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_request_summary_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->summaryText:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/squareup/features/invoices/R$id;->due_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->dueDate:Lcom/squareup/ui/account/view/LineRow;

    .line 133
    sget v0, Lcom/squareup/features/invoices/R$id;->balance_amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->balanceAmountText:Lcom/squareup/ui/account/view/LineRow;

    .line 134
    sget v0, Lcom/squareup/features/invoices/R$id;->remove_payment_request:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->removeRequestButton:Landroid/widget/Button;

    .line 135
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountButton:Landroid/widget/RadioButton;

    if-nez v0, :cond_0

    const-string v1, "fixedAmountButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 139
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    .line 140
    new-instance v1, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v2, "view.resources"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-direct {v1, p1, v2}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;-><init>(Landroid/content/res/Resources;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    check-cast v1, Lcom/squareup/text/SelectableTextScrubber;

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_1

    const-string v2, "percentageText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Lcom/squareup/text/HasSelectableText;

    .line 139
    invoke-direct {v0, v1, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageTextScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 197
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 200
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 201
    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v3, v4, :cond_1

    .line 202
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->edit_balance:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 203
    :cond_1
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->request_deposit:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 201
    :goto_0
    check-cast v3, Ljava/lang/CharSequence;

    .line 199
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 205
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 206
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 207
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$configureActionBar$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$configureActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 211
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final getAllowedFixedAmountChars()Ljava/lang/String;
    .locals 2

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0123456789.,"

    .line 217
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySubunitSymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder()\n        \u2026    }\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getFormattedMonetaryAmount(Z)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p1, :cond_3

    .line 234
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_0

    const-string v0, "percentageText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {p1, v0}, Lcom/squareup/util/Numbers;->parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->invoiceAmount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_2

    const-string v1, "invoiceAmount"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v1, "invoiceAmount.amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 234
    invoke-virtual {p1, v0, v1}, Lcom/squareup/util/Percentage;->percentOf(J)J

    move-result-wide v0

    .line 237
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 239
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_4

    const-string v1, "fixedAmountText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_5

    goto :goto_0

    :cond_5
    const-wide/16 v0, 0x0

    .line 240
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 239
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(monetaryAmount)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final isRemovable(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;)Z
    .locals 1

    .line 372
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getRemovalInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final removeListeners()V
    .locals 5

    .line 352
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "fixedAmountText"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->removeScrubbingTextWatcher(Lcom/squareup/text/HasSelectableText;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "percentageText"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageTextScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    if-nez v3, :cond_2

    const-string v4, "percentageTextScrubber"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v3, Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final setListeners(Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->amountTypeRadioGroup:Landroid/widget/RadioGroup;

    if-nez v0, :cond_0

    const-string v1, "amountTypeRadioGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->dueDate:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_1

    const-string v1, "dueDate"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    .line 381
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "fixedAmountText"

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Lcom/squareup/text/HasSelectableText;

    sget-object v3, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v1, v3}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->getAllowedFixedAmountChars()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    check-cast v1, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "percentageText"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const-string v3, "0123456789%\u00a0"

    invoke-static {v3}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v3

    check-cast v3, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageTextScrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    if-nez v3, :cond_6

    const-string v4, "percentageTextScrubber"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v3, Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    .line 179
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 181
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    .line 186
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageAmountTextWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->removeRequestButton:Landroid/widget/Button;

    if-nez v0, :cond_9

    const-string v1, "removeRequestButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/view/View;

    .line 388
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$$inlined$onClickDebounced$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setTextIfEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 3

    .line 359
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 360
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method private final setTextIfNotEqual(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 2

    .line 366
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private final swapInputs(ZZ)V
    .locals 4

    const-string v0, "fixedAmountText"

    const-string v1, "percentageText"

    if-eqz p1, :cond_3

    .line 316
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableEditText;->clearFocus()V

    .line 317
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_7

    .line 319
    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 320
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$swapInputs$1$1;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$swapInputs$1$1;-><init>(Lcom/squareup/widgets/SelectableEditText;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {p2, v3}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 324
    :cond_3
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableEditText;->clearFocus()V

    .line 325
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    if-eqz p2, :cond_7

    .line 327
    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 328
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$swapInputs$2$1;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$swapInputs$2$1;-><init>(Lcom/squareup/widgets/SelectableEditText;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {p2, v3}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    .line 332
    :cond_7
    :goto_0
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p2, Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 333
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p2, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final update(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;Landroid/view/View;)V
    .locals 10

    .line 250
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->removeListeners()V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;->getEditPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v0

    .line 253
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;->getEventHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 255
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$update$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$update$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 257
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object p2

    .line 258
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    .line 259
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->configureActionBar(Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/functions/Function1;)V

    .line 261
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->invoiceAmount:Lcom/squareup/protos/common/Money;

    .line 262
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 263
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->removeRequestButton:Landroid/widget/Button;

    if-nez v2, :cond_0

    const-string v3, "removeRequestButton"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->isRemovable(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 264
    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->isRemovable(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;)Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    .line 267
    instance-of v4, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    const-string v5, "percentageText"

    const-string v6, "fixedAmountText"

    const-string v7, "amountTypeRadioGroup"

    if-eqz v4, :cond_b

    .line 269
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isFixedAmountType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    const-wide/16 v8, 0x0

    if-eqz p2, :cond_5

    .line 270
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->amountTypeRadioGroup:Landroid/widget/RadioGroup;

    if-nez p2, :cond_1

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v3, Lcom/squareup/features/invoices/R$id;->fixed_amount_button:I

    invoke-virtual {p2, v3}, Landroid/widget/RadioGroup;->check(I)V

    .line 271
    iget-object p2, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_3

    iget-object p2, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long p2, v3, v8

    if-lez p2, :cond_3

    .line 272
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_2

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "moneyFormatter.format(paymentRequest.fixed_amount)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setTextIfNotEqual(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 274
    :cond_3
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_4

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->zeroAmountHint:Ljava/lang/CharSequence;

    const-string/jumbo v4, "zeroAmountHint"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setTextIfEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :goto_0
    const/4 p2, 0x0

    .line 276
    invoke-direct {p0, p2, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->swapInputs(ZZ)V

    goto/16 :goto_2

    .line 278
    :cond_5
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isPercentageType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 279
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->amountTypeRadioGroup:Landroid/widget/RadioGroup;

    if-nez p2, :cond_6

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v4, Lcom/squareup/features/invoices/R$id;->percentage_button:I

    invoke-virtual {p2, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 280
    iget-object p2, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    if-eqz p2, :cond_8

    iget-object p2, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long p2, v6, v8

    if-lez p2, :cond_8

    .line 281
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_7

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p2, Landroid/widget/TextView;

    .line 282
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 283
    sget-object v5, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v6, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v7, v6

    invoke-virtual {v5, v7}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v5

    .line 282
    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "percentageFormatter.form\u2026nt())\n                  )"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0, p2, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setTextIfNotEqual(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 287
    :cond_8
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->zeroPercentageHint:Ljava/lang/CharSequence;

    const-string/jumbo v5, "zeroPercentageHint"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setTextIfEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 289
    :goto_1
    invoke-direct {p0, v3, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->swapInputs(ZZ)V

    goto :goto_2

    .line 291
    :cond_a
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 292
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " PaymentRequests aren\'t supported."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 291
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 296
    :cond_b
    instance-of v2, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v2, :cond_12

    .line 297
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->amountTypeRadioGroup:Landroid/widget/RadioGroup;

    if-nez v2, :cond_c

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 298
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->summaryText:Landroid/widget/TextView;

    if-nez v2, :cond_d

    const-string v3, "summaryText"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 299
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->balanceAmountText:Lcom/squareup/ui/account/view/LineRow;

    const-string v3, "balanceAmountText"

    if-nez v2, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 300
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->balanceAmountText:Lcom/squareup/ui/account/view/LineRow;

    if-nez v2, :cond_f

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getBalanceAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v3, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v2, p2}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 301
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_10

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 302
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_11

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 305
    :cond_12
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    invoke-static {v1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 306
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->dueDate:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_13

    const-string v2, "dueDate"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p2}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 307
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isPercentageType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->updateSummaryText(Z)V

    .line 308
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setListeners(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateAmount(ZLkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 341
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->percentageText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_0

    const-string v1, "percentageText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/Percentage;->percentOf(J)J

    move-result-wide v0

    .line 344
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$PercentageChanged;

    invoke-direct {v2, v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$PercentageChanged;-><init>(J)V

    invoke-interface {p2, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 346
    :cond_2
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$FixedAmountChanged;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v1, Lcom/squareup/money/MoneyExtractor;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->fixedAmountText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_3

    const-string v3, "fixedAmountText"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyExtractorKt;->requireMoney(Lcom/squareup/money/MoneyExtractor;Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$FixedAmountChanged;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->updateSummaryText(Z)V

    return-void
.end method

.method private final updateSummaryText(Z)V
    .locals 4

    .line 228
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->summaryText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "summaryText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->payment_request_summary:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 226
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->getFormattedMonetaryAmount(Z)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "request_amount"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 227
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->invoiceAmount:Lcom/squareup/protos/common/Money;

    if-nez v2, :cond_1

    const-string v3, "invoiceAmount"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "invoice_amount"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 119
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->bindViews(Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->screens:Lio/reactivex/Observable;

    .line 122
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscri\u2026-> update(screen, view) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    .line 147
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->removeListeners()V

    return-void
.end method
