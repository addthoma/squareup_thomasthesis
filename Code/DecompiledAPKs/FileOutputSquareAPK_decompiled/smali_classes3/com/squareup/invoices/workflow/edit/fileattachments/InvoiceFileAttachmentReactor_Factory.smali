.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final attachmentFileViewerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/AttachmentFileViewer;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final fileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final filePickerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/filepicker/FilePicker;",
            ">;"
        }
    .end annotation
.end field

.field private final fileViewerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;"
        }
    .end annotation
.end field

.field private final imageCompressorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;"
        }
    .end annotation
.end field

.field private final imageDownloaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;"
        }
    .end annotation
.end field

.field private final imageUploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageUploader;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceImageChooserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceImageChooser;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceImageChooser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/filepicker/FilePicker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/AttachmentFileViewer;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->invoiceImageChooserProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->cameraHelperProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageCompressorProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->fileHelperProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageUploaderProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageDownloaderProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->filePickerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->fileViewerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->resProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->attachmentFileViewerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceImageChooser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageCompressor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/filepicker/FilePicker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/AttachmentFileViewer;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/invoices/image/InvoiceImageChooser;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/filepicker/FilePicker;Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/util/ToastFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;
    .locals 13

    .line 91
    new-instance v12, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;-><init>(Lcom/squareup/invoices/image/InvoiceImageChooser;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/filepicker/FilePicker;Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/util/ToastFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/AttachmentFileViewer;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->invoiceImageChooserProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/image/InvoiceImageChooser;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->cameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/camerahelper/CameraHelper;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageCompressorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/image/ImageCompressor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->fileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageUploaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/image/ImageUploader;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->imageDownloaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/image/ImageDownloader;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->filePickerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/filepicker/FilePicker;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->fileViewerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/invoices/image/FileViewer;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->attachmentFileViewerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/invoices/image/AttachmentFileViewer;

    invoke-static/range {v1 .. v11}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->newInstance(Lcom/squareup/invoices/image/InvoiceImageChooser;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/filepicker/FilePicker;Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/util/ToastFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor_Factory;->get()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    move-result-object v0

    return-object v0
.end method
