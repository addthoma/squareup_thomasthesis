.class public final Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "RealEditInvoiceWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/SimpleWorkflowRunner<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00aa\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004:\u0001@B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0016\u0010\u0010\u001a\u00020\r2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0010\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0017\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016JL\u0010\u001c\u001a\u00020\r2\u0006\u0010\u001d\u001a\u00020\u00132\u0012\u0010\u001e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\u00120\u001f2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001f2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\u0013H\u0016J\u0018\u0010\'\u001a\u00020\r2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0016J\u0010\u0010,\u001a\u00020\r2\u0006\u0010-\u001a\u00020.H\u0016J\u0010\u0010/\u001a\u00020\r2\u0006\u00100\u001a\u000201H\u0016J\u0018\u00102\u001a\u00020\r2\u0006\u0010-\u001a\u00020.2\u0006\u00103\u001a\u00020\u0016H\u0016J\u0018\u00104\u001a\u00020\r2\u0006\u00105\u001a\u00020\u00132\u0006\u00106\u001a\u00020\u0016H\u0016J\u0010\u00107\u001a\u00020\r2\u0006\u00108\u001a\u000209H\u0016J \u0010:\u001a\u00020\r2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020<2\u0006\u0010>\u001a\u00020?H\u0016\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "editReactor",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;",
        "editViewFactory",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;)V",
        "startAddPaymentSchedule",
        "",
        "editPaymentScheduleInput",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "startAdditionalRecipients",
        "recipients",
        "",
        "",
        "startAutomaticPayments",
        "allowAutoPayments",
        "",
        "startDateChooser",
        "chooseDateInfo",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "type",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateType;",
        "startDeliveryMethod",
        "title",
        "instruments",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "loadingInstruments",
        "currentPaymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "shareLinkMessage",
        "",
        "currentInstrumentToken",
        "startEditingPaymentRequest",
        "editPaymentRequestInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "index",
        "",
        "startEstimateDetails",
        "detailsInfo",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "startInvoiceAttachment",
        "startAttachmentInfo",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;",
        "startInvoiceDetails",
        "disableId",
        "startInvoiceMessage",
        "message",
        "isDefaultMessageEnabled",
        "startRecurring",
        "recurrenceInfo",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "startReminders",
        "remindersListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "defaultRemindersListInfo",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "EditInvoiceWorkflowStarter",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editReactor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    const-class v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "EditInvoiceWorkflowRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 41
    move-object v4, p3

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 42
    new-instance p1, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;)V

    move-object v5, p1

    check-cast v5, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 38
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public startAddPaymentSchedule(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V
    .locals 1

    const-string v0, "editPaymentScheduleInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 142
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentSchedule;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentSchedule;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startAdditionalRecipients(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "recipients"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 124
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAdditionalRecipients;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAdditionalRecipients;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startAutomaticPayments(Z)V
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 129
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAutomaticPayments;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAutomaticPayments;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startDateChooser(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V
    .locals 1

    const-string v0, "chooseDateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 78
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startDeliveryMethod(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instruments"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loadingInstruments"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentPaymentMethod"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shareLinkMessage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 55
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V
    .locals 1

    const-string v0, "editPaymentRequestInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 137
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startEstimateDetails(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;)V
    .locals 1

    const-string v0, "detailsInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 106
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEstimateDetails;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEstimateDetails;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V
    .locals 1

    const-string v0, "startAttachmentInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 91
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAttachment;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAttachment;-><init>(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startInvoiceDetails(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V
    .locals 1

    const-string v0, "detailsInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 99
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartInvoiceDetails;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartInvoiceDetails;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startInvoiceMessage(Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 86
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartMessage;

    if-eqz p2, :cond_0

    new-instance p2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    const-string v1, ""

    invoke-direct {p2, v1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;

    :goto_0
    check-cast p2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartMessage;-><init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startRecurring(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V
    .locals 1

    const-string v0, "recurrenceInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 70
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartRecurring;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartRecurring;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public startReminders(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V
    .locals 9

    const-string v0, "remindersListInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultRemindersListInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->ensureWorkflow()V

    .line 116
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAutoReminders;

    .line 117
    new-instance v8, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 116
    invoke-direct {v0, v8}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartAutoReminders;-><init>(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;)V

    .line 115
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
