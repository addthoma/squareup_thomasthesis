.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;
.super Ljava/lang/Object;
.source "AutomaticReminderErrorDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \u001b2\u00020\u0001:\u0002\u001b\u001cB)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J3\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "",
        "body",
        "positiveButton",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;",
        "negativeButton",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V",
        "getBody",
        "()Ljava/lang/String;",
        "getNegativeButton",
        "()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;",
        "getPositiveButton",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "DialogButton",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final body:Ljava/lang/String;

.field private final negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

.field private final positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;

    .line 27
    const-class v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "positiveButton"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 18
    check-cast p4, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    return-object v0
.end method

.method public final component4()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "positiveButton"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBody()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    return-object v0
.end method

.method public final getNegativeButton()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    return-object v0
.end method

.method public final getPositiveButton()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AutomaticReminderErrorDialogScreen(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", positiveButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->positiveButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", negativeButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->negativeButton:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
