.class final Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;
.super Ljava/lang/Object;
.source "RecurringRepeatEveryCoordinator.kt"

# interfaces
.implements Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->setUp(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/widgets/CheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "previousCheckedId",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 1

    if-eq p2, p3, :cond_1

    .line 66
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;

    invoke-static {v0, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->access$unitForViewId(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;I)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p3, p2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V

    invoke-interface {p1, p3}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
