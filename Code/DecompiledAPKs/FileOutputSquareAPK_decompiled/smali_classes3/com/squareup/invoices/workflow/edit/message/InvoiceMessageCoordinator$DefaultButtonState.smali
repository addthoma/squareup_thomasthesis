.class final enum Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;
.super Ljava/lang/Enum;
.source "InvoiceMessageCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DefaultButtonState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\u0008\u0082\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;",
        "",
        "stringId",
        "",
        "enabled",
        "",
        "(Ljava/lang/String;IIZ)V",
        "getEnabled",
        "()Z",
        "getStringId",
        "()I",
        "SAVING",
        "MESSAGE_SET_AS_DEFAULT",
        "SET_MESSAGE_AS_DEFAULT",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

.field public static final enum MESSAGE_SET_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

.field public static final enum SAVING:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

.field public static final enum SET_MESSAGE_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;


# instance fields
.field private final enabled:Z

.field private final stringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    .line 127
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_set_default_message_saving:I

    const/4 v3, 0x0

    const-string v4, "SAVING"

    invoke-direct {v1, v4, v3, v2, v3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->SAVING:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    .line 128
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_set_default_message_set:I

    const/4 v4, 0x1

    const-string v5, "MESSAGE_SET_AS_DEFAULT"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->MESSAGE_SET_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    .line 129
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_set_default_message:I

    const/4 v3, 0x2

    const-string v5, "SET_MESSAGE_AS_DEFAULT"

    invoke-direct {v1, v5, v3, v2, v4}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->SET_MESSAGE_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->$VALUES:[Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->stringId:I

    iput-boolean p4, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->enabled:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;
    .locals 1

    const-class v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->$VALUES:[Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    invoke-virtual {v0}, [Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    return-object v0
.end method


# virtual methods
.method public final getEnabled()Z
    .locals 1

    .line 126
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->enabled:Z

    return v0
.end method

.method public final getStringId()I
    .locals 1

    .line 126
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->stringId:I

    return v0
.end method
