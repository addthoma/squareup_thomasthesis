.class public final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;
.super Ljava/lang/Object;
.source "EditRecurringReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000e2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001\u000eB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J:\u0010\u0006\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00080\u00072\u0006\u0010\t\u001a\u00020\u00022\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;",
        "()V",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;->Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public launch(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            "Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/Reactor$DefaultImpls;->launch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;->launch(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
            "Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    if-eqz p3, :cond_0

    new-instance p3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    invoke-direct {p3, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_0
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    if-eqz p3, :cond_1

    new-instance p3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;

    invoke-direct {p3, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_1
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    if-eqz p3, :cond_2

    new-instance p3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;

    invoke-direct {p3, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$3;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 135
    :cond_2
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    if-eqz p3, :cond_3

    new-instance p3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$4;

    invoke-direct {p3, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$4;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;->onReact(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
