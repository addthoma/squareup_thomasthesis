.class public final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "InvoiceMessageCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2;->$workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2;->$workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
