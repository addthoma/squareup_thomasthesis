.class final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;
.super Ljava/lang/Object;
.source "ChooseDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDateCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDateCoordinator.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,255:1\n310#2,7:256\n310#2,7:263\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDateCoordinator.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion\n*L\n230#1,7:256\n237#1,7:263\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0000\u0008\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J/\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\tH\u0002\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u000c\u001a\u00020\t*\u00020\t2\u0006\u0010\r\u001a\u00020\u000eH\u0082\u0002\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;",
        "",
        "()V",
        "getSelectedOptionIndexFromDate",
        "",
        "dateOptions",
        "",
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "startDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "selectedDate",
        "(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/Integer;",
        "plus",
        "days",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 210
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getSelectedOptionIndexFromDate(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/Integer;
    .locals 0

    .line 210
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;->getSelectedOptionIndexFromDate(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$plus(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;Lcom/squareup/protos/common/time/YearMonthDay;J)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 0

    .line 210
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;->plus(Lcom/squareup/protos/common/time/YearMonthDay;J)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method private final getSelectedOptionIndexFromDate(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/Integer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    if-nez p3, :cond_3

    .line 223
    move-object p2, p1

    check-cast p2, Ljava/lang/Iterable;

    .line 224
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object v1, p3

    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    instance-of v1, v1, Lcom/squareup/invoices/workflow/edit/DateOption$Never;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    move-object p3, v0

    :goto_0
    check-cast p3, Lcom/squareup/invoices/workflow/edit/DateOption;

    if-eqz p3, :cond_2

    .line 225
    invoke-interface {p1, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_2
    return-object v0

    .line 228
    :cond_3
    invoke-static {p2, p3}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v0

    .line 257
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, -0x1

    if-eqz v4, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 258
    check-cast v4, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 232
    instance-of v6, v4, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    if-eqz v6, :cond_4

    check-cast v4, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;->getRelativeDays()J

    move-result-wide v6

    cmp-long v4, v6, v0

    if-nez v4, :cond_5

    const/4 v4, 0x1

    goto :goto_2

    .line 233
    :cond_4
    instance-of v6, v4, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    if-eqz v6, :cond_5

    check-cast v4, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;->getDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    invoke-static {v4, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_6

    goto :goto_3

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_7
    const/4 v3, -0x1

    :goto_3
    if-gez v3, :cond_a

    .line 264
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 265
    check-cast p2, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 239
    instance-of p2, p2, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    if-eqz p2, :cond_8

    goto :goto_5

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    const/4 v2, -0x1

    .line 269
    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_6

    .line 240
    :cond_a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_6
    return-object p1
.end method

.method private final plus(Lcom/squareup/protos/common/time/YearMonthDay;J)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "$this$plus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    long-to-int p3, p2

    .line 251
    invoke-static {p1, p3}, Lcom/squareup/util/ProtoDates;->addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string p2, "addDays(this, days.toInt())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
