.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditInvoiceDetailsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceDetailsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceDetailsScreen.kt\ncom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator\n*L\n1#1,179:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001 B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0008\u0010\u0015\u001a\u00020\u0016H\u0002J\u0018\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0006H\u0002J.\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u001d2\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00110\u001fH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
        "(Lcom/squareup/register/widgets/GlassSpinner;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "idEditText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "messageEditText",
        "saveMessageAsDefaultToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "titleEditText",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "savePressedEvent",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
        "update",
        "data",
        "updateActionBar",
        "resources",
        "Landroid/content/res/Resources;",
        "actionBarTitleRes",
        "",
        "eventHandler",
        "Lkotlin/Function1;",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private idEditText:Lcom/squareup/widgets/SelectableEditText;

.field private messageEditText:Lcom/squareup/widgets/SelectableEditText;

.field private saveMessageAsDefaultToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
            ">;"
        }
    .end annotation
.end field

.field private titleEditText:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method public constructor <init>(Lcom/squareup/register/widgets/GlassSpinner;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "glassSpinner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$savePressedEvent(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->savePressedEvent()Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 171
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_details_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.invoice_details_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->titleEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 172
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_details_id:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.invoice_details_id)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->idEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 173
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_details_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.invoice_details_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->messageEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 174
    sget v0, Lcom/squareup/features/invoices/R$id;->save_message_as_default_toggle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026essage_as_default_toggle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->saveMessageAsDefaultToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 175
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string/jumbo v0, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method private final savePressedEvent()Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;
    .locals 6

    .line 163
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->titleEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_0

    const-string/jumbo v1, "titleEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->idEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v1, :cond_1

    const-string v2, "idEditText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 165
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->messageEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_2

    const-string v3, "messageEditText"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    .line 167
    new-instance v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->saveMessageAsDefaultToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v4, :cond_3

    const-string v5, "saveMessageAsDefaultToggle"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event$SavePressed;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;

    return-object v3
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;)V
    .locals 2

    .line 131
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getEventHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$update$1;

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$update$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v1, "view.resources"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getActionBarTitleRes()I

    move-result v1

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->updateActionBar(Landroid/content/res/Resources;ILkotlin/jvm/functions/Function1;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->titleEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_0

    const-string/jumbo v0, "titleEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->idEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_1

    const-string v0, "idEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 139
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getId()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getDisableId()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 142
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->messageEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_2

    const-string v0, "messageEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->saveMessageAsDefaultToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    const-string v0, "saveMessageAsDefaultToggle"

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getShowSaveMessageAsDefaultToggle()Z

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 144
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getShowSaveMessageAsDefaultToggle()Z

    move-result p1

    if-nez p1, :cond_5

    .line 145
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->saveMessageAsDefaultToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :cond_5
    return-void
.end method

.method private final updateActionBar(Landroid/content/res/Resources;ILkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 154
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 155
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 156
    new-instance v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$updateActionBar$1;

    invoke-direct {v1, p3}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$updateActionBar$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p2, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 157
    sget v1, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 158
    new-instance p2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$updateActionBar$2;

    invoke-direct {p2, p0, p3}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$updateActionBar$2;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->bindViews(Landroid/view/View;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->screens:Lio/reactivex/Observable;

    .line 114
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 118
    new-instance v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$2;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026pdate(view, it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
