.class final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentRequestV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,281:1\n1360#2:282\n1429#2,3:283\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12\n*L\n246#1:282\n246#1,3:283\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;",
        "output",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;
    .locals 2

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->getReminders()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 282
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 283
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 284
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    if-eqz v1, :cond_0

    .line 246
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.InvoiceReminder.Instance"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 285
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 246
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;-><init>(Ljava/util/List;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;->invoke(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;

    move-result-object p1

    return-object p1
.end method
