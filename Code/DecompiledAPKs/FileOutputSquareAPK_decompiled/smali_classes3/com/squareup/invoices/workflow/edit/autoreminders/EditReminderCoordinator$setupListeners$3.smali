.class final Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3;
.super Ljava/lang/Object;
.source "EditReminderScreen.kt"

# interfaces
.implements Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->setupListeners(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditReminderScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditReminderScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3\n*L\n1#1,169:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/widgets/CheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3;->$data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 126
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3;->$data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getOffsetTypeChanged()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p3, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;

    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;->getOPTION_ID_MAP()Ljava/util/Map;

    move-result-object p3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    if-eqz p2, :cond_0

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unknown option id."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
