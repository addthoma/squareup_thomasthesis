.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/image/CompressionResult;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;",
        "it",
        "Lcom/squareup/invoices/image/CompressionResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/image/CompressionResult;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/image/CompressionResult;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    instance-of v0, p1, Lcom/squareup/invoices/image/CompressionResult$Failed;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;-><init>(Landroid/net/Uri;)V

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;

    goto :goto_0

    .line 221
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/image/CompressionResult$Success;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    check-cast p1, Lcom/squareup/invoices/image/CompressionResult$Success;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/CompressionResult$Success;->getFile()Ljava/io/File;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;-><init>(Ljava/io/File;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;

    .line 225
    :goto_0
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 226
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/jpeg"

    .line 225
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    new-instance v1, Lcom/squareup/workflow/legacy/EnterState;

    .line 230
    new-instance v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    .line 231
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 233
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;-><init>(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    .line 234
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v4

    .line 230
    invoke-direct {v2, p1, v0, v3, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    .line 229
    invoke-direct {v1, v2}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v1

    .line 221
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/invoices/image/CompressionResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;->invoke(Lcom/squareup/invoices/image/CompressionResult;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
