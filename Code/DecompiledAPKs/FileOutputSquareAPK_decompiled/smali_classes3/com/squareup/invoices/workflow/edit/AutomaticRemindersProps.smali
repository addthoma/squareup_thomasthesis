.class public final Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;
.super Ljava/lang/Object;
.source "AutomaticRemindersProps.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0008H\u00c6\u0003J1\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00082\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000b\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "",
        "remindersListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "defaultList",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "isPaymentRequestInSchedule",
        "",
        "(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)V",
        "getDefaultList",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "()Z",
        "getReminderSettings",
        "()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "getRemindersListInfo",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

.field private final isPaymentRequestInSchedule:Z

.field private final reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

.field private final remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)V
    .locals 1

    const-string v0, "remindersListInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iput-boolean p4, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 9
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->copy(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final component2()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;
    .locals 1

    const-string v0, "remindersListInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDefaultList()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object v0
.end method

.method public final getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPaymentRequestInSchedule()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AutomaticRemindersProps(remindersListInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->defaultList:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reminderSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isPaymentRequestInSchedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
