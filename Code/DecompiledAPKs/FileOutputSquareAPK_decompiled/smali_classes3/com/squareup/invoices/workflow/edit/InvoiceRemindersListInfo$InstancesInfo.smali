.class public final Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;
.super Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
.source "InvoiceRemindersListInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstancesInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008J\u000f\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0006H\u00c6\u0003J-\u0010\u0011\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0006H\u00c6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0013H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000c\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "instances",
        "",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "paymentRequestDueDate",
        "(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V",
        "getInstances",
        "()Ljava/util/List;",
        "getInvoiceFirstSentDate",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getPaymentRequestDueDate",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final instances:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo$Creator;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo$Creator;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")V"
        }
    .end annotation

    const-string v0, "instances"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->copy(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;"
        }
    .end annotation

    const-string v0, "instances"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInstances()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    return-object v0
.end method

.method public final getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getPaymentRequestDueDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InstancesInfo(instances="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceFirstSentDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentRequestDueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->instances:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->invoiceFirstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->paymentRequestDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
