.class public final enum Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;
.super Ljava/lang/Enum;
.source "EditReminderScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OffsetType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u0000 \u00062\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
        "",
        "(Ljava/lang/String;I)V",
        "BEFORE",
        "ON",
        "AFTER",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

.field public static final enum AFTER:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

.field public static final enum BEFORE:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

.field public static final Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;

.field public static final enum ON:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

.field private static final OPTION_ID_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    const/4 v3, 0x0

    const-string v4, "BEFORE"

    invoke-direct {v2, v4, v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->BEFORE:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    aput-object v2, v1, v3

    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    const/4 v4, 0x1

    const-string v5, "ON"

    invoke-direct {v2, v5, v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->ON:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    aput-object v2, v1, v4

    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    const/4 v5, 0x2

    const-string v6, "AFTER"

    invoke-direct {v2, v6, v5}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->AFTER:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    aput-object v2, v1, v5

    sput-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->$VALUES:[Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;

    new-array v0, v0, [Lkotlin/Pair;

    .line 49
    sget v1, Lcom/squareup/features/invoices/R$id;->option_before_date:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->BEFORE:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v3

    .line 50
    sget v1, Lcom/squareup/features/invoices/R$id;->option_on_date:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->ON:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v4

    .line 51
    sget v1, Lcom/squareup/features/invoices/R$id;->option_after_date:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->AFTER:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v5

    .line 48
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->OPTION_ID_MAP:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static final synthetic access$getOPTION_ID_MAP$cp()Ljava/util/Map;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->OPTION_ID_MAP:Ljava/util/Map;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;
    .locals 1

    const-class v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->$VALUES:[Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-virtual {v0}, [Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    return-object v0
.end method
