.class public final Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;
.super Ljava/lang/Object;
.source "RealEditInvoiceWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditInvoiceWorkflowStarter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowStarter<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J`\u0010\u0007\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b`\u000e0\t\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0008j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "editReactor",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;",
        "(Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;)V",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editReactor:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;)V
    .locals 1

    const-string v0, "editReactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;->editReactor:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
            ">;"
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter;->editReactor:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;

    .line 151
    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;->Companion:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;

    invoke-virtual {v2, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;->fromSnapshot$invoices_hairball_release(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->launch(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 153
    new-instance v1, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter$start$1;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v0, v2}, Lcom/squareup/invoices/workflow/edit/RealEditInvoiceWorkflowRunner$EditInvoiceWorkflowStarter$start$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
