.class final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;
.super Lkotlin/jvm/internal/Lambda;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 82
    new-instance v8, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getTitle()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstruments()Ljava/util/List;

    move-result-object v3

    .line 84
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v6

    .line 85
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v7

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, v8

    .line 82
    invoke-direct/range {v0 .. v7}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/util/List;IZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 81
    invoke-direct {p1, v8}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$3;->invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
