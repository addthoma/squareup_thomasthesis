.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "it",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/filepicker/FilePickerResult;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/filepicker/FilePickerResult;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    instance-of v0, p1, Lcom/squareup/filepicker/FilePickerResult$Cancelled;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Canceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Canceled;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 194
    :cond_0
    instance-of v0, p1, Lcom/squareup/filepicker/FilePickerResult$Selected;

    if-eqz v0, :cond_1

    .line 197
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 198
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;->getTitle()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pdf"

    const-string v3, "application/pdf"

    .line 197
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    new-instance v1, Lcom/squareup/workflow/legacy/EnterState;

    .line 201
    new-instance v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    .line 202
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    check-cast p1, Lcom/squareup/filepicker/FilePickerResult$Selected;

    invoke-virtual {p1}, Lcom/squareup/filepicker/FilePickerResult$Selected;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;-><init>(Landroid/net/Uri;)V

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 204
    new-instance p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;->getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    move-result-object v4

    invoke-direct {p1, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Upload;-><init>(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    .line 205
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v4

    .line 201
    invoke-direct {v2, v3, v0, p1, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    .line 200
    invoke-direct {v1, v2}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/filepicker/FilePickerResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;->invoke(Lcom/squareup/filepicker/FilePickerResult;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
