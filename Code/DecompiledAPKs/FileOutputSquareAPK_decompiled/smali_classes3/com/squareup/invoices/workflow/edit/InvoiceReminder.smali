.class public abstract Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
.super Ljava/lang/Object;
.source "InvoiceReminder.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;,
        Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\nR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006R\u0012\u0010\t\u001a\u00020\nX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u0082\u0001\u0002\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "Landroid/os/Parcelable;",
        "()V",
        "configToken",
        "",
        "getConfigToken",
        "()Ljava/lang/String;",
        "customMessage",
        "getCustomMessage",
        "relativeDays",
        "",
        "getRelativeDays",
        "()I",
        "updateCustomMessage",
        "message",
        "updateRelativeDays",
        "days",
        "Config",
        "Instance",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getConfigToken()Ljava/lang/String;
.end method

.method public abstract getCustomMessage()Ljava/lang/String;
.end method

.method public abstract getRelativeDays()I
.end method

.method public final updateCustomMessage(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
    .locals 10

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;Ljava/lang/String;ILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    goto :goto_0

    .line 54
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final updateRelativeDays(I)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
    .locals 10

    .line 46
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move v3, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;Ljava/lang/String;ILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    goto :goto_0

    .line 47
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3d

    const/4 v9, 0x0

    move v3, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
