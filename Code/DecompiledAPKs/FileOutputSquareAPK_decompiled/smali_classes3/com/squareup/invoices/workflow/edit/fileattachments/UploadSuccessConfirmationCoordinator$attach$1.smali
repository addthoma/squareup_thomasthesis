.class final Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;
.super Ljava/lang/Object;
.source "UploadSuccessConfirmationCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lkotlin/Unit;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0004 \u0006*\u0016\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003j\u0004\u0018\u0001`\u00050\u0003j\u0002`\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)V"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;->access$update(Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator$attach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
