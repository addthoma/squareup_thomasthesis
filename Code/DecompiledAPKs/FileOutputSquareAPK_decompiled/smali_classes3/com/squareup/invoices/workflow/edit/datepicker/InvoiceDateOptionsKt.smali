.class public final Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptionsKt;
.super Ljava/lang/Object;
.source "InvoiceDateOptions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "getRelativeDaysToEndOfMonth",
        "",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getRelativeDaysToEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)J
    .locals 3

    const-string v0, "$this$getRelativeDaysToEndOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x5

    .line 174
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 175
    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v0

    return-wide v0
.end method
