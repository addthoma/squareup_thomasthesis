.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestV2Screen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestV2Screen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestV2Screen.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,181:1\n1577#2,4:182\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestV2Screen.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory\n*L\n158#1,4:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ&\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0015H\u0002J\\\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u00192\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bJ\u0010\u0010!\u001a\u00020\"2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0018\u0010#\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010$\u001a\u00020\"H\u0002J\u0010\u0010%\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0014\u0010&\u001a\u00020\u000f*\u00020\u00112\u0006\u0010\'\u001a\u00020(H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V",
        "actionBarTitle",
        "",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "index",
        "",
        "paymentRequests",
        "",
        "from",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;",
        "props",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;",
        "cancelClicked",
        "Lkotlin/Function0;",
        "",
        "saveClicked",
        "removePaymentClicked",
        "dueDateClicked",
        "reminderRowClicked",
        "showRemovePaymentRequestButton",
        "",
        "subtitle",
        "editingInstallment",
        "title",
        "dueString",
        "firstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .param p4    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final actionBarTitle(Lcom/squareup/protos/client/invoice/PaymentRequest;ILjava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 141
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz p1, :cond_4

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 145
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->edit_payment_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 146
    invoke-static {p3, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateInstallmentNumber(Ljava/util/List;I)I

    move-result p2

    const-string p3, "number"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 143
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->edit_balance_title:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 142
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->edit_deposit_title:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1

    .line 150
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Payment request must have amount type."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 5

    .line 163
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 164
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_request_due_upon_receipt_no_due_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 166
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_request_due_in_no_due_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 167
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "number_of_days"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_1
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    const-string p2, "ProtoDates.calendarFromY\u2026teDueDate(firstSentDate))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 173
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final showRemovePaymentRequestButton(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)Z
    .locals 5

    .line 155
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object v0

    .line 156
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result p1

    .line 158
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    .line 182
    instance-of v2, v1, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    .line 184
    :cond_0
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 158
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v1, 0x2

    if-le v2, v1, :cond_3

    .line 159
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v3, 0x1

    :cond_3
    return v3
.end method

.method private final subtitle(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Z)Ljava/lang/String;
    .locals 4

    .line 113
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result v0

    .line 114
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object p1

    .line 118
    sget-object v2, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-static {p1, v1}, Lcom/squareup/invoices/PaymentRequestPercentageCalculatorsKt;->calculatePercentages(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    const-string v2, "percentage"

    if-eqz p2, :cond_0

    .line 121
    invoke-static {p1, v1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 122
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->percentage_of_invoice_balance:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 123
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string/jumbo v0, "total_balance_amount"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 126
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->percentage_of_invoice_total:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 129
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 130
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "invoice_total_money"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final title(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)Ljava/lang/String;
    .locals 2

    .line 102
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result p1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Pair;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final from(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p7

    const-string v4, "paymentRequest"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "props"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "cancelClicked"

    move-object/from16 v12, p3

    invoke-static {v12, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "saveClicked"

    move-object/from16 v13, p4

    invoke-static {v13, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "removePaymentClicked"

    move-object/from16 v14, p5

    invoke-static {v14, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "dueDateClicked"

    move-object/from16 v15, p6

    invoke-static {v15, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "reminderRowClicked"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object v5

    invoke-direct {v0, v1, v4, v5}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->actionBarTitle(Lcom/squareup/protos/client/invoice/PaymentRequest;ILjava/util/List;)Ljava/lang/String;

    move-result-object v6

    .line 72
    invoke-direct {v0, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->title(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)Ljava/lang/String;

    move-result-object v7

    .line 73
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    invoke-direct {v0, v2, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->subtitle(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Z)Ljava/lang/String;

    move-result-object v8

    .line 74
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getFirstSentAt()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v9

    .line 75
    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 76
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 77
    :goto_0
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Show;

    if-nez v1, :cond_1

    .line 78
    iget-object v1, v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->res:Lcom/squareup/util/Res;

    .line 79
    sget v5, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_off:I

    .line 78
    invoke-interface {v1, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 80
    :cond_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 77
    :goto_1
    invoke-direct {v4, v1, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Show;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow;

    move-object v11, v4

    goto :goto_2

    .line 84
    :cond_2
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Hide;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Hide;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow;

    move-object v11, v1

    .line 87
    :goto_2
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;

    .line 92
    invoke-direct {v0, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->showRemovePaymentRequestButton(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)Z

    move-result v10

    move-object v5, v1

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p6

    .line 87
    invoke-direct/range {v5 .. v15}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v1
.end method
