.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    move-object v2, v1

    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;->getTitle()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1b

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->copy$default(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;ILjava/lang/Object;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;->invoke(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent$AttachmentTitleUpdated;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
