.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealEditDetailsViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/details/EditDetailsViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditDetailsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "editInvoiceDetailsScreenFactory",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;",
        "(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)V
    .locals 20
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "editInvoiceDetailsScreenFactory"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 12
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    sget-object v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 14
    sget v4, Lcom/squareup/features/invoices/R$layout;->edit_invoice_v2_invoice_detail_view:I

    .line 15
    new-instance v5, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory$1;

    invoke-direct {v5, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory$1;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)V

    move-object v7, v5

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 16
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v14, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1df

    const/16 v19, 0x0

    move-object v8, v0

    invoke-direct/range {v8 .. v19}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v6, 0x0

    const/16 v8, 0x8

    move-object v5, v0

    .line 12
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 19
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x1

    aput-object v0, v1, v2

    move-object/from16 v0, p0

    .line 11
    invoke-direct {v0, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
