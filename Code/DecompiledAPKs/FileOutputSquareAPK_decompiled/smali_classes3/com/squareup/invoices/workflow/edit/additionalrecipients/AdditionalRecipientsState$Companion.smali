.class public final Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;
.super Ljava/lang/Object;
.source "AdditionalRecipientsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAdditionalRecipientsState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AdditionalRecipientsState.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,31:1\n180#2:32\n165#2:33\n*E\n*S KotlinDebug\n*F\n+ 1 AdditionalRecipientsState.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion\n*L\n25#1:32\n25#1:33\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;",
        "",
        "()V",
        "restoreSnapshot",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;
    .locals 4

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 32
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 33
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 26
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    .line 26
    new-instance p1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    return-object p1
.end method
