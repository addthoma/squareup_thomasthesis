.class final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentRequestV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getFirstSentAt()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 263
    :cond_0
    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$NoDateSelected;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$NoDateSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;->invoke(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
