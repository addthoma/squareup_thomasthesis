.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2;
.super Ljava/lang/Object;
.source "ValidationErrorDialog.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;)Landroid/app/AlertDialog;
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;->getErrorInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;->getErrorInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;->getMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 43
    sget v1, Lcom/squareup/common/strings/R$string;->confirm:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogFactory$create$2;->apply(Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
