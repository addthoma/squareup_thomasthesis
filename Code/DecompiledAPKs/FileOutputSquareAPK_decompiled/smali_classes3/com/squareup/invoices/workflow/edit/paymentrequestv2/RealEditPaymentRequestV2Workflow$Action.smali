.class public abstract Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;
.super Ljava/lang/Object;
.source "EditPaymentRequestV2Workflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$CancelClicked;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$RemovePaymentRequestClicked;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$SaveClicked;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateClicked;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$ReminderClicked;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$EditReminders;,
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,281:1\n132#2,3:282\n132#2,3:285\n1360#3:288\n1429#3,3:289\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action\n*L\n145#1,3:282\n167#1,3:285\n167#1:288\n167#1,3:289\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\t\u0007\u0008\t\n\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\t\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "CancelClicked",
        "DueDateCanceled",
        "DueDateClicked",
        "EditReminders",
        "ReminderClicked",
        "RemovePaymentRequestClicked",
        "SaveClicked",
        "UpdateDueDate",
        "UpdateReminders",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$CancelClicked;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$RemovePaymentRequestClicked;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$SaveClicked;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateClicked;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$ReminderClicked;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$EditReminders;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$CancelClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$CancelClicked;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Canceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Canceled;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;

    return-object p1

    .line 134
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$RemovePaymentRequestClicked;

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Removed;

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$RemovePaymentRequestClicked;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$RemovePaymentRequestClicked;->getIndex()I

    move-result v0

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Removed;-><init>(I)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;

    return-object p1

    .line 136
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$SaveClicked;

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Saved;

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$SaveClicked;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$SaveClicked;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Saved;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;

    return-object p1

    .line 138
    :cond_2
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateClicked;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentrequestv2.EditPaymentRequestV2State.Viewing"

    if-eqz v0, :cond_4

    .line 139
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    .line 140
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 139
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 143
    :cond_4
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    const-string v2, "null cannot be cast to non-null type B"

    const-string v3, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentrequestv2.EditPaymentRequestV2State.EditingDueDate"

    if-eqz v0, :cond_7

    .line 144
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;

    .line 145
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    .line 283
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_5

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 146
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->getFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->getDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsKt;->updateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v0

    const-string v1, "oldState.paymentRequest.\u2026ate, dueDate)\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 145
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 283
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 144
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 150
    :cond_7
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$DueDateCanceled;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 151
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;

    .line 152
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 151
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 155
    :cond_9
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$ReminderClicked;

    if-eqz v0, :cond_b

    .line 156
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    .line 157
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 156
    :cond_a
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 160
    :cond_b
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$EditReminders;

    if-eqz v0, :cond_d

    .line 161
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;

    .line 162
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$EditReminders;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$EditReminders;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 161
    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentrequestv2.EditPaymentRequestV2State.FetchingReminderSettings"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 165
    :cond_d
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;

    if-eqz v0, :cond_11

    .line 166
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_10

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;

    .line 167
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    .line 286
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_f

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 168
    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateReminders;->getReminders()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 288
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 289
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 290
    check-cast v4, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    .line 169
    invoke-static {v4}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291
    :cond_e
    check-cast v3, Ljava/util/List;

    iput-object v3, v1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    .line 287
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v0

    .line 167
    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 172
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    const-string/jumbo v2, "updatedPaymentRequest"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 286
    :cond_f
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 166
    :cond_10
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentrequestv2.EditPaymentRequestV2State.EditingReminders"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_11
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
