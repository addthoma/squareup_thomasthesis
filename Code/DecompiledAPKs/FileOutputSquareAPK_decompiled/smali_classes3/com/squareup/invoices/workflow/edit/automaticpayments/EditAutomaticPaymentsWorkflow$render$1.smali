.class final Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditAutomaticPaymentsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;->render(ZLcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;",
        "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
        "event",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$render$1;->$state:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event$TogglePressed;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event$TogglePressed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event$TogglePressed;->getAllowAutoPayments()Z

    move-result p1

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;-><init>(Z)V

    const/4 p1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 50
    :cond_0
    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event$BackPressed;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$render$1;->$state:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->getAllowAutoPayments()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$render$1;->invoke(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
