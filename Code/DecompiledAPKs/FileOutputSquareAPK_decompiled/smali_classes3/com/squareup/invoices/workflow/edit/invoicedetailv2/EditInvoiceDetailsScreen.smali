.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;,
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0018\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 +2\u00020\u0001:\u0002+,BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c6\u0003J\t\u0010#\u001a\u00020\u000bH\u00c6\u0003J\u0015\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH\u00c6\u0003Je\u0010%\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00072\u0008\u0008\u0003\u0010\n\u001a\u00020\u000b2\u0014\u0008\u0002\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH\u00c6\u0001J\u0013\u0010&\u001a\u00020\u00072\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u00d6\u0003J\t\u0010)\u001a\u00020\u000bH\u00d6\u0001J\t\u0010*\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u001d\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0014R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0018\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "",
        "id",
        "message",
        "showProgress",
        "",
        "disableId",
        "showSaveMessageAsDefaultToggle",
        "actionBarTitleRes",
        "",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)V",
        "getActionBarTitleRes",
        "()I",
        "getDisableId",
        "()Z",
        "getEventHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "getId",
        "()Ljava/lang/String;",
        "getMessage",
        "getShowProgress",
        "getShowSaveMessageAsDefaultToggle",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "Companion",
        "Event",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final actionBarTitleRes:I

.field private final disableId:Z

.field private final eventHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final id:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field private final showProgress:Z

.field private final showSaveMessageAsDefaultToggle:Z

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    .line 86
    const-class v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZI",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    iput-boolean p5, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    iput-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    iput p7, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    iput-object p8, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    return v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZI",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;"
        }
    .end annotation

    const-string/jumbo v0, "title"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-object v1, v0

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    iget v1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarTitleRes()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    return v0
.end method

.method public final getDisableId()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    return v0
.end method

.method public final getEventHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowProgress()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    return v0
.end method

.method public final getShowSaveMessageAsDefaultToggle()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditInvoiceDetailsScreen(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", showProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showProgress:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", disableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->disableId:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSaveMessageAsDefaultToggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->showSaveMessageAsDefaultToggle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", actionBarTitleRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->actionBarTitleRes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", eventHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
