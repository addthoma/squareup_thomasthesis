.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecurringEndsDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001 B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u001e\u0010\u0016\u001a\u00020\u00172\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0010H\u0002J&\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u00052\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00192\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateScreen;",
        "locale",
        "Ljava/util/Locale;",
        "(Lio/reactivex/Observable;Ljava/util/Locale;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "datePicker",
        "Lcom/squareup/timessquare/CalendarPickerView;",
        "datePickerStartDate",
        "Ljava/util/Date;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "res",
        "Landroid/content/res/Resources;",
        "getMaxDateExclusive",
        "fromDate",
        "update",
        "recurrenceInfo",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private datePicker:Lcom/squareup/timessquare/CalendarPickerView;

.field private datePickerStartDate:Ljava/util/Date;

.field private final locale:Ljava/util/Locale;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Ljava/util/Locale;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->locale:Ljava/util/Locale;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Ljava/util/Locale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;-><init>(Lio/reactivex/Observable;Ljava/util/Locale;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->update(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 104
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 106
    sget v0, Lcom/squareup/features/invoices/R$id;->ends_date_calendar_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/timessquare/CalendarPickerView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    return-void
.end method

.method private final getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config;"
        }
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 97
    sget v1, Lcom/squareup/features/invoices/R$string;->end:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    const/4 v0, 0x1

    .line 98
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 99
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$getActionBarConfig$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string p2, "Builder()\n        .setUp\u2026essed) }\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getMaxDateExclusive(Ljava/util/Date;)Ljava/util/Date;
    .locals 2

    .line 110
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 111
    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    const/4 p1, 0x1

    const/4 v1, 0x2

    .line 112
    invoke-virtual {v0, p1, v1}, Ljava/util/GregorianCalendar;->add(II)V

    const/4 v1, 0x5

    .line 113
    invoke-virtual {v0, v1, p1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 114
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object p1

    const-string v0, "maxDate.time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final update(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 66
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePickerStartDate:Ljava/util/Date;

    const/4 v2, 0x1

    const-string v3, "datePicker"

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getStartDate()Ljava/util/Date;

    move-result-object v1

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePickerStartDate:Ljava/util/Date;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 69
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getStartDate()Ljava/util/Date;

    move-result-object v1

    .line 70
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v4, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->getMaxDateExclusive(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v1, v5, v6}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    .line 71
    iput-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePickerStartDate:Ljava/util/Date;

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v4, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1;

    invoke-direct {v4, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    invoke-virtual {v1, v4}, Lcom/squareup/timessquare/CalendarPickerView;->setOnDateSelectedListener(Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;)V

    .line 82
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_4

    const-string v4, "actionBar"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const-string v4, "resources"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v0}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 84
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$2;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 86
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object p1

    .line 87
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz p2, :cond_7

    .line 88
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez p2, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1, v2}, Lcom/squareup/timessquare/CalendarPickerView;->selectDate(Ljava/util/Date;Z)Z

    :cond_7
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->bindViews(Landroid/view/View;)V

    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    const-string v3, "datePicker"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v4, Lcom/squareup/features/invoices/R$drawable;->invoice_calendar_divider:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/CalendarPickerView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 54
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v4, Lcom/squareup/marin/R$dimen;->marin_gap_ultra_large:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/squareup/timessquare/CalendarPickerView;->setDividerHeight(I)V

    .line 55
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setTitleTypeface(Landroid/graphics/Typeface;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->datePicker:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/timessquare/CalendarPickerView;->setDateTypeface(Landroid/graphics/Typeface;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
