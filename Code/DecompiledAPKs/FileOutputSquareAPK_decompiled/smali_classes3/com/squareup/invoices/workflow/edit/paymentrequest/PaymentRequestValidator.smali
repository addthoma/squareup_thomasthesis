.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;
.super Ljava/lang/Object;
.source "PaymentRequestValidator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0016B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002J$\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V",
        "getFormattedZeroValue",
        "",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "validate",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
        "paymentRequestType",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "invoiceAmount",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "ValidationResult",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 1
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private final getFormattedZeroValue(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/CharSequence;
    .locals 4

    .line 105
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isFixedAmountType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    const-wide/16 v1, 0x0

    .line 106
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "paymentRequest.fixed_amount.currency_code"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 105
    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(\n \u2026ount.currency_code)\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isPercentageType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->percentageFormatter:Lcom/squareup/text/Formatter;

    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "percentageFormatter.format(Percentage.ZERO)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    .line 109
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get a zero amount for a PaymentRequest of type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 109
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final validate(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "paymentRequestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v0, :cond_1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p2

    invoke-static {p2, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 52
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getDepositDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 53
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    .line 54
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invalid_balance_date_title:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 55
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invalid_balance_date_message:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 53
    invoke-direct {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    goto :goto_0

    .line 58
    :cond_0
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    .line 51
    :goto_0
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(\n           \u2026d\n            }\n        )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 62
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz v0, :cond_6

    .line 63
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 64
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getCompletedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 65
    invoke-static {v0, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 66
    invoke-static {v0, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    .line 67
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getOverlappingDateValidation()Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    move-result-object p1

    if-eqz v1, :cond_2

    .line 70
    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    .line 72
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invalid_deposit_amount_title:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 73
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->deposit_amount_less_than_completed:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "completed_amount"

    invoke-virtual {p3, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 75
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    .line 71
    invoke-direct {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    goto/16 :goto_1

    .line 78
    :cond_2
    invoke-static {v2, p2}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 79
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    .line 80
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invalid_deposit_amount_title:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->deposit_amount_larger_than_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "invoice_amount"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 83
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 79
    invoke-direct {p1, p3, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    goto :goto_1

    .line 85
    :cond_3
    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-eqz p2, :cond_4

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    .line 86
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invalid_deposit_amount_title:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 87
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->deposit_amount_greater_than_zero:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 88
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->getFormattedZeroValue(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "zero_amount"

    .line 87
    invoke-virtual {p3, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 89
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    .line 85
    invoke-direct {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    goto :goto_1

    .line 91
    :cond_4
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    if-eqz p2, :cond_5

    .line 92
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;->getEndDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/util/YearMonthDaysKt;->compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p1

    if-ltz p1, :cond_5

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    .line 93
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invalid_balance_date_title:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 94
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invalid_balance_date_message:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 92
    invoke-direct {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    goto :goto_1

    .line 96
    :cond_5
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    .line 99
    :goto_1
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(result)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
