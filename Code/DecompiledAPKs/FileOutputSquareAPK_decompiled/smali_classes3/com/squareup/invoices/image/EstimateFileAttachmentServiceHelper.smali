.class public interface abstract Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;
.super Ljava/lang/Object;
.source "EstimateFileAttachmentServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000fJ$\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H&J0\u0010\t\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;",
        "",
        "downloadFile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lokhttp3/ResponseBody;",
        "estimateToken",
        "",
        "attachmentToken",
        "uploadFile",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "file",
        "Ljava/io/File;",
        "mediaType",
        "Lokhttp3/MediaType;",
        "NoEstimateFileAttachmentServiceHelper",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Lokhttp3/MediaType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;>;"
        }
    .end annotation
.end method
