.class final Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;
.super Ljava/lang/Object;
.source "ImageDownloader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageDownloader;->toDownloadResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/lang/Throwable;",
        "Lcom/squareup/invoices/image/FileDownloadResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/image/FileDownloadResult$Failed;",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/image/ImageDownloader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageDownloader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;->this$0:Lcom/squareup/invoices/image/ImageDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Throwable;)Lcom/squareup/invoices/image/FileDownloadResult$Failed;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance p1, Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;->this$0:Lcom/squareup/invoices/image/ImageDownloader;

    invoke-static {v0}, Lcom/squareup/invoices/image/ImageDownloader;->access$getRes$p(Lcom/squareup/invoices/image/ImageDownloader;)Lcom/squareup/util/Res;

    move-result-object v0

    .line 61
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_image_download_failed:I

    .line 60
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 59
    invoke-direct {p1, v0, v1}, Lcom/squareup/invoices/image/FileDownloadResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;->apply(Ljava/lang/Throwable;)Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    move-result-object p1

    return-object p1
.end method
