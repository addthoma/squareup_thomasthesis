.class public final Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "InvoiceSetupPaymentEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "()V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;

    invoke-direct {v0}, Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;-><init>()V

    sput-object v0, Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;->INSTANCE:Lcom/squareup/invoices/analytics/SetupPaymentsSendAnywayTapped;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 20
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Invoices: Prompt IDV: Tap Send Anyway"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
