.class public final enum Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
.super Ljava/lang/Enum;
.source "InvoiceRecordPaymentTenderType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "",
        "displayString",
        "",
        "tenderType",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
        "(Ljava/lang/String;IILcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)V",
        "getDisplayString",
        "()I",
        "getTenderType",
        "()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
        "CASH",
        "CHECK",
        "OTHER",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

.field public static final enum CASH:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

.field public static final enum CHECK:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

.field public static final enum OTHER:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;


# instance fields
.field private final displayString:I

.field private final tenderType:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    new-instance v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    .line 14
    sget v2, Lcom/squareup/common/invoices/R$string;->record_payment_tender_type_cash:I

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v4, 0x0

    const-string v5, "CASH"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)V

    sput-object v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->CASH:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    .line 15
    sget v2, Lcom/squareup/common/invoices/R$string;->record_payment_tender_type_check:I

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CHECK:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v4, 0x1

    const-string v5, "CHECK"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)V

    sput-object v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->CHECK:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    .line 16
    sget v2, Lcom/squareup/common/invoices/R$string;->record_payment_tender_type_other:I

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->OTHER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v4, 0x2

    const-string v5, "OTHER"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)V

    sput-object v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->OTHER:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->$VALUES:[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
            ")V"
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->displayString:I

    iput-object p4, p0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->tenderType:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    const-class v0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->$VALUES:[Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-virtual {v0}, [Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object v0
.end method


# virtual methods
.method public final getDisplayString()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->displayString:I

    return v0
.end method

.method public final getTenderType()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->tenderType:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object v0
.end method
