.class public final Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;
.super Ljava/lang/Object;
.source "InvoiceCreationContext.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceCreationContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceCreationContext.kt\ncom/squareup/invoices/edit/contexts/InvoiceCreationContextKt\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,138:1\n132#2,3:139\n1360#3:142\n1429#3,3:143\n1360#3:146\n1429#3,3:147\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceCreationContext.kt\ncom/squareup/invoices/edit/contexts/InvoiceCreationContextKt\n*L\n93#1,3:139\n93#1:142\n93#1,3:143\n135#1:146\n135#1,3:147\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a0\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00012\u0010\u0008\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008\u001a\u0018\u0010\n\u001a\u00020\u00012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u0016\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u001a\u0018\u0010\n\u001a\u00020\u0001*\u00020\u00112\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u001a\u0012\u0010\u000b\u001a\u00020\u000c*\u00020\u00122\u0006\u0010\r\u001a\u00020\u000e\u00a8\u0006\u0013"
    }
    d2 = {
        "constructDefaultDepositPaymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "amountType",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "remainderPaymentRequest",
        "defaultReminderConfigs",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "constructDefaultPaymentRequest",
        "constructNewTemplate",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "creationContext",
        "Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;",
        "defaultMessage",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final constructDefaultDepositPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;"
        }
    .end annotation

    const-string v0, "amountType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remainderPaymentRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 88
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " can\'t be used for a deposit payment requests."

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 90
    :cond_1
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p0

    if-eqz p0, :cond_6

    move-object p1, p0

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 94
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    :goto_1
    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 95
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_2

    :cond_3
    const-wide/16 v0, 0x0

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    if-eqz p3, :cond_5

    .line 97
    check-cast p3, Ljava/lang/Iterable;

    .line 142
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p3, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 143
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 144
    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 97
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 145
    :cond_4
    check-cast p2, Ljava/util/List;

    .line 98
    iput-object p2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    .line 141
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p0

    const-string p1, "constructDefaultPaymentR\u2026stances\n        }\n      }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object p0

    .line 140
    :cond_6
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type B"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic constructDefaultDepositPaymentRequest$default(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 0

    and-int/lit8 p4, p4, 0x8

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 85
    check-cast p3, Ljava/util/List;

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultDepositPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    return-object p0
.end method

.method public static final constructDefaultPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 6

    const-string v0, "amountType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    .line 108
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 110
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 111
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 113
    sget-object v4, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const/4 v3, 0x3

    if-eq v4, v3, :cond_1

    const/4 v3, 0x4

    if-eq v4, v3, :cond_1

    const/4 p1, 0x5

    if-ne v4, p1, :cond_0

    goto :goto_0

    .line 119
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " is not supported for creating a default PaymentRequest."

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 119
    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 115
    :cond_1
    invoke-static {v1, v2, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    goto :goto_0

    .line 114
    :cond_2
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 124
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    const-string p1, "Builder()\n      .amount_\u2026 }\n      }\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final constructDefaultPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;"
        }
    .end annotation

    const-string v0, "$this$constructDefaultPaymentRequest"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultReminderConfigs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    .line 131
    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 134
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v1, p0

    goto :goto_2

    :cond_2
    const-wide/16 v1, 0x0

    :goto_2
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object p0

    .line 135
    check-cast p1, Ljava/lang/Iterable;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 147
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 148
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 135
    invoke-static {v1}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 149
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 135
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object p0

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    const-string p1, "Builder()\n      .amount_\u2026tance() })\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic constructDefaultPaymentRequest$default(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;ILjava/lang/Object;)Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 104
    sget-object p0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    return-object p0
.end method

.method public static final constructNewTemplate(Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 3

    const-string v0, "creationContext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;-><init>()V

    .line 39
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 41
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 42
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrentTime()Lcom/squareup/time/CurrentTime;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->getEmptyCartProto(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    const/4 v1, 0x1

    .line 48
    invoke-static {v2, p0, v1, v2}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultPaymentRequest$default(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;ILjava/lang/Object;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    .line 47
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 46
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    const-string p1, "Invoice.Builder()\n      \u2026_reminders_enabled(false)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final constructNewTemplate(Lcom/squareup/protos/client/invoice/InvoiceDefaults;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 5

    const-string v0, "$this$constructNewTemplate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creationContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    const-string v1, "payment_request_defaults"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "payment_request_defaults.first()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    const-string v2, "automatic_reminder_config"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 66
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;-><init>()V

    .line 68
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v3}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrentTime()Lcom/squareup/time/CurrentTime;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    const-string v4, "relative_send_on"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/squareup/util/ProtoDates;->addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->getEmptyCartProto(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    const/4 v1, 0x0

    .line 75
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    .line 77
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    .line 78
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    const-string p1, "Invoice.Builder()\n      \u2026_request(paymentRequests)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
