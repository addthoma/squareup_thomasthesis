.class public Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;
.super Lmortar/ViewPresenter;
.source "InvoiceConfirmationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLOSE_CARD_DELAY_MS:I = 0x7d0


# instance fields
.field private action:Lcom/squareup/invoices/edit/InvoiceAction;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final backRunnable:Ljava/lang/Runnable;

.field private final cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private errorDescription:Ljava/lang/CharSequence;

.field private errorTitle:Ljava/lang/CharSequence;

.field errored:Z

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private invoice:Lcom/squareup/protos/client/invoice/Invoice;

.field private final invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private isSuccess:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

.field private subscription:Lrx/Subscription;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errored:Z

    .line 57
    new-instance v0, Lcom/squareup/invoices/edit/confirmation/-$$Lambda$InvoiceConfirmationPresenter$lecIwzutRmepMT6WbSN0Nl9v5N0;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/confirmation/-$$Lambda$InvoiceConfirmationPresenter$lecIwzutRmepMT6WbSN0Nl9v5N0;-><init>(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;)V

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->backRunnable:Ljava/lang/Runnable;

    .line 64
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->subscription:Lrx/Subscription;

    .line 74
    iput-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->flow:Lflow/Flow;

    .line 75
    iput-object p2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 76
    iput-object p3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 77
    iput-object p4, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    .line 78
    iput-object p5, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 79
    iput-object p6, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 80
    iput-object p7, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    .line 81
    iput-object p8, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 82
    iput-object p9, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    .line 83
    iput-object p10, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    .line 84
    iput-object p11, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    return-void
.end method

.method private populateChargedView()V
    .locals 5

    .line 243
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 244
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getSelectedInstrumentIfAny()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v1

    .line 245
    iget-boolean v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 246
    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v2}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 247
    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoices()V

    .line 248
    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_charged:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v3, v3, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 249
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "brand"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v1, v1, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    const-string v3, "pan"

    .line 250
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 251
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 252
    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_charged_receipt_sent:I

    .line 253
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    const-string v4, "buyer_name"

    .line 254
    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    const-string v4, "email"

    .line 255
    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 256
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 257
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 261
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateDeleteDraftView()V
    .locals 3

    .line 278
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 279
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_deleted:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 284
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateDeleteSeriesView()V
    .locals 3

    .line 288
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 289
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_deleted_series:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 294
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populatePutInvoiceView()V
    .locals 4

    .line 318
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 319
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_3

    .line 320
    sget-object v1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter$1;->$SwitchMap$com$squareup$invoices$edit$InvoiceAction:[I

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/InvoiceAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    const/4 v2, 0x4

    const-string v3, ""

    if-eq v1, v2, :cond_1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_saved_recurring:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 322
    :cond_1
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 323
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoicesQuietly()V

    .line 324
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_saved:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 328
    :cond_2
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoicesQuietly()V

    .line 329
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_resent:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    const-string v3, "recipient"

    .line 330
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 331
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    .line 329
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 338
    :cond_3
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 340
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateScheduleRecurringView()V
    .locals 3

    .line 298
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 299
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_scheduled_recurring_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 304
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateScheduleView()V
    .locals 3

    .line 265
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 266
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 268
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoicesQuietly()V

    .line 269
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_scheduled:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 274
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateSendView()V
    .locals 4

    .line 229
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 230
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 232
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoices()V

    .line 233
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_sent:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    const-string v3, "recipient"

    .line 234
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 235
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    .line 233
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 239
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateShareLinkView()V
    .locals 5

    .line 200
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 201
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_1

    .line 202
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->readyToFinishInvoicesQuietly()V

    .line 207
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne v1, v2, :cond_0

    .line 208
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->setHasInvoicesTrue()V

    .line 209
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_created:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_created_subtitle:I

    .line 211
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    const-string v4, "email"

    .line 212
    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 213
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_updated:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    .line 219
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showShareLinkHelper()V

    .line 220
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 221
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceUrlHelper;->canShareInvoiceUrl()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showShareLinkButtons(Z)V

    goto :goto_1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 225
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateUpdateRecurringView()V
    .locals 3

    .line 308
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    .line 309
    iget-boolean v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_updated_recurring_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 312
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 314
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->showGlyph()V

    return-void
.end method

.method private populateViews()V
    .locals 2

    .line 165
    sget-object v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter$1;->$SwitchMap$com$squareup$invoices$edit$InvoiceAction:[I

    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/InvoiceAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateChargedView()V

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateUpdateRecurringView()V

    goto :goto_0

    .line 188
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateScheduleRecurringView()V

    goto :goto_0

    .line 195
    :pswitch_3
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateShareLinkView()V

    goto :goto_0

    .line 179
    :pswitch_4
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateDeleteSeriesView()V

    goto :goto_0

    .line 176
    :pswitch_5
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateDeleteDraftView()V

    goto :goto_0

    .line 185
    :pswitch_6
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populatePutInvoiceView()V

    goto :goto_0

    .line 173
    :pswitch_7
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateScheduleView()V

    goto :goto_0

    .line 170
    :pswitch_8
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateSendView()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method finish()V
    .locals 1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errored:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->cancelInvoiceConfirmation()V

    :goto_0
    return-void
.end method

.method public getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 115
    sget-object v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter$1;->$SwitchMap$com$squareup$invoices$edit$InvoiceAction:[I

    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/InvoiceAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 143
    new-instance v0, Ljava/lang/EnumConstantNotPresentException;

    const-class v1, Lcom/squareup/invoices/edit/InvoiceAction;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/InvoiceAction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/EnumConstantNotPresentException;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    throw v0

    .line 141
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_charged_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 139
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_updated_recurring_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 137
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_scheduled_recurring_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 135
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_save_recurring_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 133
    :pswitch_4
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->update_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 131
    :pswitch_5
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->create_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 129
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_preview_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 127
    :pswitch_7
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_delete_series_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 125
    :pswitch_8
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_delete_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 123
    :pswitch_9
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_save_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 121
    :pswitch_a
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_resend_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 119
    :pswitch_b
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_scheduled_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 117
    :pswitch_c
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_send_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic lambda$new$0$InvoiceConfirmationPresenter()V
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->finish()V

    :cond_0
    return-void
.end method

.method public onCopyLinkClicked()V
    .locals 4

    .line 351
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_COPY_LINK:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 352
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    sget-object v2, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/InvoiceUrlHelper;->copyUrlToClipboard(Ljava/lang/String;Lcom/squareup/url/UrlType;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_link_copied:I

    .line 355
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 354
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 88
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 89
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    .line 90
    iget-object v0, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 91
    iget-object v0, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->errorTitle:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorTitle:Ljava/lang/CharSequence;

    .line 92
    iget-object v0, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->errorDescription:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errorDescription:Ljava/lang/CharSequence;

    .line 93
    iget-boolean p1, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->isResponseSuccess:Z

    iput-boolean p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->isSuccess:Z

    .line 94
    iget-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->backRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->subscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 98
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 100
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/edit/confirmation/-$$Lambda$k3dXVwEoFL_3LcXmfYTGIlwLfG0;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/confirmation/-$$Lambda$k3dXVwEoFL_3LcXmfYTGIlwLfG0;-><init>(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;)V

    .line 102
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->populateViews()V

    return-void
.end method

.method onMoreOptionsClicked()V
    .locals 4

    .line 344
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SHARE_LINK:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 345
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 346
    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    sget-object v3, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/url/InvoiceShareUrlLauncher;->shareUrl(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/url/UrlType;)V

    return-void
.end method

.method startFinishTimer()V
    .locals 4

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-eq v0, v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->backRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
