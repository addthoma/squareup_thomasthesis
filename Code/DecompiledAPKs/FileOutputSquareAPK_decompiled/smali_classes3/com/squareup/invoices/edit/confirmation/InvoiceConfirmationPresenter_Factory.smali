.class public final Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;
.super Ljava/lang/Object;
.source "InvoiceConfirmationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p4, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p5, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p7, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p8, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->cacheProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p9, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p10, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p11, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;)",
            "Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;
    .locals 13

    .line 93
    new-instance v12, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;-><init>(Lflow/Flow;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;
    .locals 12

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-static/range {v1 .. v11}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter_Factory;->get()Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    move-result-object v0

    return-object v0
.end method
