.class final Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;
.super Ljava/lang/Object;
.source "EditInvoiceGateway.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "editFlowV2On",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $inCheckoutFlow:Z

.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceGateway;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    iput-boolean p2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;->$inCheckoutFlow:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "editFlowV2On"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;->$inCheckoutFlow:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    invoke-static {v0}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->access$getFeatures$p(Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lcom/squareup/settings/server/Features;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_V1_DEPRECATION_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->enabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Single;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1$1;-><init>(Ljava/lang/Boolean;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 107
    :cond_0
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$1;->apply(Ljava/lang/Boolean;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
