.class public final Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;
.super Ljava/lang/Object;
.source "InvoicesPlaceholderProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;-><init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {v0}, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;->newInstance(Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider_Factory;->get()Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;

    move-result-object v0

    return-object v0
.end method
