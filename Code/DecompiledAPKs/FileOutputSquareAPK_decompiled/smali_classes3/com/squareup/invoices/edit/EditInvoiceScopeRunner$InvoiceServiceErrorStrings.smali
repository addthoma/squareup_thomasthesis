.class Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InvoiceServiceErrorStrings"
.end annotation


# instance fields
.field final errorDescription:Ljava/lang/CharSequence;

.field final errorTitle:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 2

    .line 2066
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->this$0:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067
    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->access$000(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->error_default:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 2071
    iget-object v0, p2, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    .line 2072
    iget-object p1, p2, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    goto :goto_0

    .line 2073
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->access$100(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->isOffline()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 2074
    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->access$000(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/util/Res;

    move-result-object p2

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_connection_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2075
    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->access$000(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/util/Res;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/invoices/edit/InvoiceAction;->getConnectionError(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    .line 2078
    :goto_0
    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    .line 2079
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    return-void
.end method
