.class public Lcom/squareup/invoices/edit/EditInvoicePresenter;
.super Lmortar/ViewPresenter;
.source "EditInvoicePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/edit/EditInvoiceView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p3

    .line 143
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v2, p1

    .line 144
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    move-object v2, p2

    .line 145
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 146
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    move-object v2, p4

    .line 147
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    move-object v2, p5

    .line 148
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v2, p6

    .line 149
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dateFormat:Ljava/text/DateFormat;

    move-object v2, p7

    .line 150
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->formatter:Lcom/squareup/text/Formatter;

    move-object v2, p8

    .line 151
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    move-object v2, p9

    .line 152
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object v2, p10

    .line 153
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->flow:Lflow/Flow;

    move-object v2, p11

    .line 154
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    move-object v2, p12

    .line 155
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v2, p13

    .line 156
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v2, p14

    .line 157
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    move-object/from16 v2, p15

    .line 158
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v2, p16

    .line 159
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-object/from16 v2, p17

    .line 160
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    move-object/from16 v2, p18

    .line 161
    iput-object v2, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    const/4 v2, 0x1

    .line 163
    invoke-virtual {p3, v2}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/EditInvoicePresenter;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
    .locals 0

    .line 105
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    return-object p0
.end method

.method private displayPaymentMethod(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 2

    .line 462
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 463
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getSelectedInstrumentIfAny()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v1

    .line 462
    invoke-static {p2, v0, v1}, Lcom/squareup/invoices/edit/PaymentMethodUtility;->getPaymentMethodString(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/util/Res;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Ljava/lang/String;

    move-result-object p2

    .line 461
    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->setPaymentMethodValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private getDueDateValueAndSetIfNecessary()Ljava/lang/CharSequence;
    .locals 10

    .line 798
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 799
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 800
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 801
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 802
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 801
    invoke-static {v2, v0, v3}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 800
    invoke-static {v1, v2}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 803
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v3, v1}, Lcom/squareup/protos/common/time/YearMonthDay;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 805
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v3, v1}, Lcom/squareup/invoices/InvoiceDatesKt;->isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldUpdateDates()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 806
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 807
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 808
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 807
    invoke-static {v2, v0, v3}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 809
    invoke-static {v2}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 806
    invoke-static {v1, v0, v2}, Lcom/squareup/invoices/Invoices;->updateDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 810
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_upon_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 812
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 814
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoiceDatesKt;->isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 815
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    :cond_2
    move-object v4, v0

    const/4 v5, 0x0

    .line 820
    iget-object v6, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    .line 821
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dateFormat:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 822
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    xor-int/lit8 v9, v0, 0x1

    .line 820
    invoke-static/range {v3 .. v9}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValue(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 804
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_upon_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHeaderText()Ljava/lang/String;
    .locals 2

    .line 761
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isNewInvoice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_new_recurring_series:I

    .line 763
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_new_invoice:I

    .line 764
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 766
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingSeries()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_recurring_series:I

    .line 767
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_invoice:I

    .line 768
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method private getSendDateValueAndSetIfNecessary()Ljava/lang/CharSequence;
    .locals 3

    .line 827
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 828
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v1, v2}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 829
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldUpdateDates()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 830
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 833
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingSingleInvoice()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 834
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 835
    invoke-virtual {v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    sget-object v2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne v1, v2, :cond_1

    .line 836
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dateFormat:Ljava/text/DateFormat;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 839
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 840
    :cond_2
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v1, v2}, Lcom/squareup/invoices/InvoiceDatesKt;->isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 841
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldUpdateDates()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 842
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 843
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 845
    :cond_3
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dateFormat:Ljava/text/DateFormat;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initialize(Lcom/squareup/invoices/edit/EditInvoiceView;)V
    .locals 3

    .line 325
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 328
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCustomerInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 333
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->displayPaymentMethod(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 336
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->initializeDateRows(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)V

    .line 339
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowRecurringViews()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 340
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNotDraftSingleInvoice()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 341
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 339
    :goto_0
    invoke-virtual {p1, v2}, Lcom/squareup/invoices/edit/EditInvoiceView;->showFrequencyRow(Z)V

    .line 344
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 345
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setTitle(Ljava/lang/CharSequence;)V

    .line 349
    :cond_2
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 350
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setInvoiceId(Ljava/lang/CharSequence;)V

    .line 354
    :cond_3
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v1, v2, :cond_4

    .line 355
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideAdditionalEmailRows()V

    .line 357
    :cond_4
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    .line 358
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 359
    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->restoreAdditionalEmailRows(Ljava/util/List;)V

    .line 363
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showViewMessageRow()V

    .line 364
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 367
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->setBuyerToggles(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 369
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowCustomerTextRows()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 370
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->showCustomerTextBoxes(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 373
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addInvoiceTextWatchers()V

    .line 376
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result p1

    if-nez p1, :cond_9

    const/4 p1, 0x0

    .line 378
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v1, :cond_7

    .line 379
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 380
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    .line 383
    :cond_7
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 384
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->populatePaymentScheduleSection(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    goto :goto_1

    .line 386
    :cond_8
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->populateDepositRequestSection(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    :cond_9
    :goto_1
    return-void
.end method

.method private initializeDateRows(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)V
    .locals 2

    if-eqz p3, :cond_1

    .line 471
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSeries()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateLabelToNextInvoice()V

    goto :goto_0

    .line 474
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateLabelToStart()V

    .line 476
    :goto_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getSendDateValueAndSetIfNecessary()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateValue(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 478
    :cond_1
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_2

    .line 479
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideSendDateRow()V

    .line 481
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    goto :goto_2

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_3

    .line 484
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateLabelToCharge()V

    goto :goto_1

    .line 486
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateLabelToSend()V

    .line 488
    :goto_1
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getSendDateValueAndSetIfNecessary()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setSendDateValue(Ljava/lang/CharSequence;)V

    :goto_2
    if-eqz p3, :cond_4

    .line 494
    iget-object p3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_each_invoice_due:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/invoices/edit/EditInvoiceView;->setDueDateTitle(Ljava/lang/CharSequence;)V

    .line 497
    :cond_4
    invoke-direct {p0, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldHideDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 498
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideDueDateRow()V

    goto :goto_3

    .line 500
    :cond_5
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getDueDateValueAndSetIfNecessary()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->setDueDateValue(Ljava/lang/CharSequence;)V

    :goto_3
    return-void
.end method

.method private isAddEditCustomerAvailable()Z
    .locals 1

    .line 559
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAllowed()Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/lang/Boolean;)V
    .locals 1

    .line 228
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 229
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setEnabled(Z)V

    .line 230
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showProgress(Z)V

    return-void
.end method

.method private populateDepositRequestSection(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 4

    .line 305
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 308
    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 312
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    return-void

    .line 316
    :cond_1
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 317
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 318
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    invoke-static {p2, p1, v3}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 320
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/edit/EditInvoiceView;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    invoke-virtual {v3, v0, v1, v2, p1}, Lcom/squareup/invoices/PaymentRequestData$Factory;->createForEditing(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showPaymentRequestList(Ljava/util/List;)V

    return-void
.end method

.method private populateErrorBars(Ljava/lang/String;)V
    .locals 2

    const-string v0, ""

    if-eqz p1, :cond_0

    .line 774
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 776
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    :goto_0
    return-void
.end method

.method private populatePaymentScheduleSection(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 3

    .line 286
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 288
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    return-void

    .line 292
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 293
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 294
    invoke-static {v2}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    invoke-static {p2, p1, v2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 296
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    .line 297
    invoke-interface {p2, v0, v1, p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;->fromPaymentRequests(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    .line 300
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showPaymentSchedule(Ljava/util/List;)V

    return-void
.end method

.method private presentCart(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 5

    .line 534
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceView;

    .line 535
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 536
    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->buildCartEntries(Lcom/squareup/protos/client/bills/Cart;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;

    move-result-object p1

    .line 538
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    .line 539
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 541
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/cart/CartEntryView;

    .line 543
    invoke-virtual {v0, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    .line 544
    new-instance v4, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;

    invoke-direct {v4, p0, v2}, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;I)V

    invoke-virtual {v3, v4}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 551
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getSubTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    .line 552
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getDiscountViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRows(Ljava/util/List;)V

    .line 553
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTaxViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRows(Ljava/util/List;)V

    .line 554
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTipView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    .line 555
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    return-void
.end method

.method private setButtons(Lcom/squareup/invoices/edit/EditInvoiceView;)V
    .locals 2

    .line 434
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowRequestDeposit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showRequestDepositButton()V

    .line 438
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowAddPaymentSchedule()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showAddPaymentScheduleButton()V

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->newOrEditingDraft()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isDuplicateInvoice()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 446
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideDraftButton()V

    const/4 v0, 0x0

    .line 447
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->showPreviewButton(Z)V

    goto :goto_1

    .line 443
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PREVIEW:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->showPreviewButton(Z)V

    .line 444
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_save_as_draft:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setDraftText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private setBuyerToggles(Lcom/squareup/invoices/edit/EditInvoiceView;)V
    .locals 7

    .line 392
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 394
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    .line 395
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    if-eq v2, v3, :cond_0

    .line 396
    invoke-virtual {p1, v5, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateShippingAddressRow(ILjava/lang/Boolean;)V

    goto :goto_0

    .line 398
    :cond_0
    invoke-virtual {p1, v4, v6}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateShippingAddressRow(ILjava/lang/Boolean;)V

    .line 402
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportsInvoiceTipping()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v1, v2, :cond_1

    .line 403
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->isTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z

    move-result v1

    invoke-virtual {p1, v5, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateTippableRow(IZ)V

    goto :goto_1

    .line 405
    :cond_1
    invoke-virtual {p1, v4, v5}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateTippableRow(IZ)V

    .line 409
    :goto_1
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 410
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v0, v2, :cond_2

    .line 411
    invoke-virtual {p1, v5, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateBuyerCofRow(ILjava/lang/Boolean;)V

    goto :goto_2

    .line 413
    :cond_2
    invoke-virtual {p1, v4, v6}, Lcom/squareup/invoices/edit/EditInvoiceView;->updateBuyerCofRow(ILjava/lang/Boolean;)V

    :goto_2
    return-void
.end method

.method private setFileAttachmentsList(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/edit/EditInvoiceView;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)V"
        }
    .end annotation

    .line 419
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->clearFileAttachmentContainer()V

    .line 420
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->numberOfFilesWithinLimit()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAttachmentButton(Z)V

    .line 422
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideFileAttachmentTitle()V

    goto :goto_1

    .line 425
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showFileAttachmentTitle()V

    .line 427
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 428
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalFileAttachmentRowView(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private setRecurrenceHelper(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)V
    .locals 2

    .line 452
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 453
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p2, v1, v0}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->getRecurringPeriodLongText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    .line 457
    :goto_0
    invoke-virtual {p1, p2, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setRecurringPeriodHelper(ILjava/lang/CharSequence;)V

    return-void
.end method

.method private shouldHideDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z
    .locals 3

    .line 579
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v0, v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    .line 580
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    .line 581
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method private shouldShowAddPaymentSchedule()Z
    .locals 2

    .line 868
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 869
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 870
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 871
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 872
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isCardOnFile()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private shouldShowCustomerTextRows()Z
    .locals 1

    .line 563
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->isAddEditCustomerAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 564
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shouldShowRecurringViews()Z
    .locals 2

    .line 855
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->inTender()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shouldShowRequestDeposit()Z
    .locals 2

    .line 859
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    .line 860
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 861
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 862
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->hasDepositRequest(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 863
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 864
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isCardOnFile()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shouldUpdateDates()Z
    .locals 3

    .line 781
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->newOrEditingDraft()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 785
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingSingleInvoice()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 786
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 787
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 788
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 787
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 789
    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    if-ne v0, v2, :cond_2

    :cond_1
    return v1

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private showCustomerTextBoxes(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 2

    .line 568
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceView;

    .line 569
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideCustomerRow()V

    .line 570
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->showCustomerEmailRow()V

    .line 571
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->showCustomerNameRow()V

    .line 572
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_0

    .line 573
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCustomerInfoTextRows(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private showReminderRow()Lcom/squareup/util/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 511
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 512
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isCardOnFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 513
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    .line 519
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 520
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    goto :goto_3

    .line 521
    :cond_2
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 522
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    goto :goto_3

    .line 524
    :cond_4
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    goto :goto_3

    .line 527
    :cond_5
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    :goto_3
    return-object v0
.end method

.method private updateActionBar()V
    .locals 4

    .line 733
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->inTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 735
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_amount:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->formatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 736
    invoke-virtual {v3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getTransaction()Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 737
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 739
    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 740
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getHeaderText()Ljava/lang/String;

    move-result-object v1

    .line 743
    :goto_0
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 744
    invoke-virtual {v2, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$otDUlvbvc8RyY1HXNUHtaPuWPuY;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$otDUlvbvc8RyY1HXNUHtaPuWPuY;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;)V

    .line 745
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 746
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getSendButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$88XtOg1e0SncZtoElL219znbedM;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$88XtOg1e0SncZtoElL219znbedM;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;)V

    .line 747
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 749
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method addItemButtonClicked()V
    .locals 3

    .line 625
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method draftButtonClicked()V
    .locals 2

    .line 690
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 691
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onSaveDraftClicked()V

    return-void
.end method

.method public goToEditFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 1

    .line 585
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startUpdatingFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    return-void
.end method

.method inTender()Z
    .locals 2

    .line 850
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope;->getType()Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_TENDER:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$null$1$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 202
    :goto_0
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p1, p3, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->setFrequencyLabel(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/text/DateFormat;)V

    .line 203
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    .line 204
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isAutomaticPaymentsEnabled()Z

    move-result v4

    if-eqz v3, :cond_2

    if-eqz v2, :cond_1

    .line 207
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v5

    sget-object v6, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    .line 206
    :goto_1
    invoke-virtual {p1, v5, v4}, Lcom/squareup/invoices/edit/EditInvoiceView;->setAutomaticPaymentViews(ZZ)V

    .line 210
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->setRecurrenceHelper(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)V

    .line 211
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1, v2, p2, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->changeCofHelper(ZLjava/lang/Boolean;Z)V

    return-void
.end method

.method public synthetic lambda$null$3$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/util/List;)V
    .locals 0

    .line 222
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->setFileAttachmentsList(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$7$EditInvoicePresenter(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;Lkotlin/Unit;)V
    .locals 1

    .line 238
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 240
    invoke-static {p2, p1}, Lcom/squareup/invoices/Invoices;->setPayerFromContact(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result p4

    if-eqz p4, :cond_3

    .line 242
    iget-object p4, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p4

    if-eqz p4, :cond_0

    .line 243
    iget-object p4, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {p4, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setContactId(Ljava/lang/String;)V

    .line 245
    :cond_0
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getDisplayNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p4

    .line 246
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    .line 247
    invoke-virtual {p3, p4, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCustomerInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 250
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object p4, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 251
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->resetCofDeliveryMethod()V

    .line 252
    invoke-direct {p0, p3, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->displayPaymentMethod(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 253
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result p1

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->initializeDateRows(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)V

    .line 254
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->updateActionBar()V

    .line 255
    invoke-direct {p0, p3}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->setBuyerToggles(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 257
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/common/invoices/R$string;->invoice_detail_customer:I

    .line 258
    invoke-interface {p1, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p4, 0x0

    .line 257
    invoke-virtual {p3, p1, p4}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCustomerInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 260
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 263
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-static {p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isPayerEmpty(Lcom/squareup/protos/client/invoice/InvoiceContact;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-interface {p1, p2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->customerOnInvoice(Z)V

    return-void
.end method

.method public synthetic lambda$null$9$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;Ljava/util/List;)V
    .locals 0

    .line 269
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->displayPaymentMethod(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$EditInvoicePresenter(Lcom/squareup/protos/client/invoice/Invoice$Builder;Landroid/view/View;II)V
    .locals 0

    .line 175
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->presentCart(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    return-void
.end method

.method public synthetic lambda$onLoad$10$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lrx/Subscription;
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInstruments()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$LDiqtKSg6lYOwe8ENx-VrnSvQcY;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$LDiqtKSg6lYOwe8ENx-VrnSvQcY;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 269
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lrx/Subscription;
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$hUOFuVBnkoHaJgt-SSzqe_BMqGM;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$hUOFuVBnkoHaJgt-SSzqe_BMqGM;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 200
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;)Lrx/Subscription;
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getFileMetadataList()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$FJGm7tVyNTtclxK1ByCfiJ9i06k;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$FJGm7tVyNTtclxK1ByCfiJ9i06k;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 222
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$EditInvoicePresenter(Lcom/squareup/invoices/edit/EditInvoiceView;)Lrx/Subscription;
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isBusy()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$b49Ob5Dh_zSZYJhiYlyIWK9qG8w;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$b49Ob5Dh_zSZYJhiYlyIWK9qG8w;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 227
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$EditInvoicePresenter(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;)Lrx/Subscription;
    .locals 2

    .line 236
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$qypoiEoM4rNb4aHzXQ3QxqDdB8o;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$qypoiEoM4rNb4aHzXQ3QxqDdB8o;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 237
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onAddPaymentScheduleClicked()V
    .locals 2

    .line 633
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddPaymentScheduleClicked;->INSTANCE:Lcom/squareup/invoices/analytics/AddPaymentScheduleClicked;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 634
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToAddPaymentSchedule()V

    return-void
.end method

.method onAttachFileClicked()V
    .locals 1

    .line 646
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startCreatingFileAttachment()V

    return-void
.end method

.method onAutomaticPaymentRowClicked()V
    .locals 1

    .line 661
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToAutomaticPaymentScreen()V

    return-void
.end method

.method onAutomaticRemindersRowClicked()V
    .locals 1

    .line 665
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToAutomaticRemindersScreen()V

    .line 666
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->exitAutoRemindersTutorial()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 589
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->hasWorkingInvoiceChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->displayDiscardChangesDialog()V

    const/4 v0, 0x0

    return v0

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->discardInvoice()V

    const/4 v0, 0x1

    return v0
.end method

.method onBuyerCofChanged(Z)V
    .locals 4

    .line 654
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->updateBuyerCofEnabled(Z)V

    .line 655
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceView;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 656
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 657
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isAutomaticPaymentsEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 655
    :goto_0
    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/invoices/edit/EditInvoiceView;->changeCofHelper(ZLjava/lang/Boolean;Z)V

    return-void
.end method

.method public onCustomerClicked()V
    .locals 9

    .line 700
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 702
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasCustomer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->flow:Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/squareup/ui/crm/flow/CrmScope;->forInvoiceLikeInEditViewCustomer(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Order;Z)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 705
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 709
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->flow:Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    invoke-static {v2, v0, v3}, Lcom/squareup/ui/crm/flow/CrmScope;->forInvoiceCreateFromDraftContact(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Order;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 714
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->EDIT_INVOICE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    sget v4, Lcom/squareup/crmchoosecustomer/R$string;->crm_add_customer_to_invoice_title:I

    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_FIRST_LAST_EMAIL:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    sget-object v6, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 713
    invoke-static/range {v2 .. v8}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method onDeliveryMethodClicked()V
    .locals 1

    .line 642
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startEditingDeliveryMethod()V

    return-void
.end method

.method onDueDateClicked()V
    .locals 1

    .line 674
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToChooseDueDate()V

    return-void
.end method

.method onEditPaymentScheduleClicked()V
    .locals 1

    .line 638
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditPaymentSchedule()V

    return-void
.end method

.method onFrequencyClicked()V
    .locals 1

    .line 670
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startEditingFrequency()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 167
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown EditInvoiceScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/EditInvoiceView;

    .line 171
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 173
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->updateActionBar()V

    .line 174
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->resetLineItems()V

    .line 175
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$7hRXajSZ6NzEQtOni8ZhVNJccLM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$7hRXajSZ6NzEQtOni8ZhVNJccLM;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 177
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    invoke-interface {v1, v2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->itemOnInvoice(Z)V

    .line 178
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->newOrEditingDraft()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->isDraftInvoice(Z)V

    .line 180
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v2, Lcom/squareup/comms/protos/common/TenderType;->INVOICE:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {v1, v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    .line 182
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->initialize(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->setButtons(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 185
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->disableCustomerRow()V

    .line 187
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingEmailedInvoice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideSendDateRow()V

    .line 189
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->disableDeliveryRow()V

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingInvoiceInRecurringSeries()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingSeries()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 194
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->disableInvoiceId()V

    .line 197
    :cond_2
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowRecurringViews()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 198
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$nJ3CWMYWssi1fqM2bUFswbrRpE0;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$nJ3CWMYWssi1fqM2bUFswbrRpE0;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 217
    :cond_3
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 218
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showFileAttachmentButton()V

    .line 219
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showFileAttachmentDivider()V

    .line 220
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$sZixGUDyS2wxrR3lBsQqda35tHQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$sZixGUDyS2wxrR3lBsQqda35tHQ;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 225
    :cond_4
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$SnniD0ycpT7_9x457aiH9XJc-Mo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$SnniD0ycpT7_9x457aiH9XJc-Mo;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 233
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->isAddEditCustomerAvailable()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 234
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 235
    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;

    invoke-direct {v2, p0, v1, v0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 267
    :cond_5
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$JNb0Vw6XlwN5r9kHLTzHZ-XYHS0;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$JNb0Vw6XlwN5r9kHLTzHZ-XYHS0;-><init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 271
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->hasValidationError()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 272
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInvoiceValidationErrorIfAny()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->populateErrorBars(Ljava/lang/String;)V

    .line 275
    :cond_6
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->showReminderRow()Lcom/squareup/util/Optional;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 277
    invoke-virtual {p1, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->showAutomaticReminderRow(Z)V

    .line 278
    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->setAutomaticReminderValue(Z)V

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    .line 280
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->showAutomaticReminderRow(Z)V

    :goto_0
    return-void
.end method

.method public onPaymentRequestClicked(I)V
    .locals 1

    .line 727
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onPaymentRequestClicked(I)V

    return-void
.end method

.method onRequestDepositClicked()V
    .locals 1

    .line 629
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToRequestDeposit()V

    return-void
.end method

.method onSendDateClicked()V
    .locals 1

    .line 678
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToChooseSendDate()V

    return-void
.end method

.method public onSendInvoice()V
    .locals 1

    .line 753
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInvoiceValidationErrorIfAny()Ljava/lang/String;

    move-result-object v0

    .line 754
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->populateErrorBars(Ljava/lang/String;)V

    if-nez v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onSendClicked()V

    :cond_0
    return-void
.end method

.method onShippingAddressChanged(Z)V
    .locals 1

    .line 650
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->updateShippingAddressEnabled(Z)V

    return-void
.end method

.method public onTippableRowCheckedChanged(Z)V
    .locals 1

    .line 686
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/invoices/Invoices;->setTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method onViewMessageClicked()V
    .locals 1

    .line 682
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startUpdatingInvoiceMessage()V

    return-void
.end method

.method previewButtonClicked()V
    .locals 2

    .line 695
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_PREVIEW:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 696
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->preview()V

    return-void
.end method

.method textChanged()V
    .locals 4

    .line 599
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 605
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_1

    .line 606
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceContact;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v1

    goto :goto_0

    .line 608
    :cond_1
    new-instance v1, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;-><init>()V

    .line 611
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/edit/EditInvoiceView;

    .line 612
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->shouldShowCustomerTextRows()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 613
    iget-object v3, v2, Lcom/squareup/invoices/edit/EditInvoiceView;->customerNameRow:Lcom/squareup/widgets/SelectableEditText;

    .line 614
    invoke-virtual {v2, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v1

    iget-object v3, v2, Lcom/squareup/invoices/edit/EditInvoiceView;->customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

    .line 615
    invoke-virtual {v2, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_email(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v1

    const/4 v3, 0x0

    .line 616
    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;

    move-result-object v1

    .line 617
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object v1

    .line 613
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 619
    :cond_2
    iget-object v1, v2, Lcom/squareup/invoices/edit/EditInvoiceView;->title:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v2, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 620
    iget-object v1, v2, Lcom/squareup/invoices/edit/EditInvoiceView;->invoiceId:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v2, v1}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 621
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/EditInvoiceView;->getAdditionalEmailsTrimmed()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method
