.class public final enum Lcom/squareup/invoices/edit/InvoiceAction;
.super Ljava/lang/Enum;
.source "InvoiceAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/edit/InvoiceAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum CHARGE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum PREVIEW:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum RESEND:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum SCHEDULE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum SEND:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum UPDATE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

.field public static final enum UPLOAD:Lcom/squareup/invoices/edit/InvoiceAction;


# instance fields
.field private connectionErrorString:I

.field private isRecurring:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 8
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_send:I

    const/4 v2, 0x0

    const-string v3, "SEND"

    invoke-direct {v0, v3, v2, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SEND:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 10
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_schedule:I

    const/4 v3, 0x1

    const-string v4, "SCHEDULE"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 12
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_charge:I

    const/4 v4, 0x2

    const-string v5, "CHARGE"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->CHARGE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 14
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_create:I

    const/4 v5, 0x3

    const-string v6, "CREATE"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 16
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_resend:I

    const/4 v6, 0x4

    const-string v7, "RESEND"

    invoke-direct {v0, v7, v6, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->RESEND:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 18
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_update:I

    const/4 v7, 0x5

    const-string v8, "UPDATE"

    invoke-direct {v0, v8, v7, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 20
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_save:I

    const/4 v8, 0x6

    const-string v9, "SAVE"

    invoke-direct {v0, v9, v8, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 22
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_delete:I

    const/4 v9, 0x7

    const-string v10, "DELETE"

    invoke-direct {v0, v10, v9, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 24
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_preview:I

    const/16 v10, 0x8

    const-string v11, "PREVIEW"

    invoke-direct {v0, v11, v10, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->PREVIEW:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 26
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_upload:I

    const/16 v11, 0x9

    const-string v12, "UPLOAD"

    invoke-direct {v0, v12, v11, v1, v2}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPLOAD:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 28
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_save_recurring_draft:I

    const/16 v12, 0xa

    const-string v13, "SAVE_RECURRING"

    invoke-direct {v0, v13, v12, v1, v3}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 30
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_schedule_recurring:I

    const/16 v13, 0xb

    const-string v14, "SCHEDULE_RECURRING"

    invoke-direct {v0, v14, v13, v1, v3}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 32
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_delete_series:I

    const/16 v14, 0xc

    const-string v15, "DELETE_SERIES"

    invoke-direct {v0, v15, v14, v1, v3}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 34
    new-instance v0, Lcom/squareup/invoices/edit/InvoiceAction;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_update_series:I

    const/16 v15, 0xd

    const-string v14, "UPDATE_SERIES"

    invoke-direct {v0, v14, v15, v1, v3}, Lcom/squareup/invoices/edit/InvoiceAction;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/invoices/edit/InvoiceAction;

    .line 7
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SEND:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->CHARGE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->RESEND:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->PREVIEW:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->UPLOAD:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->$VALUES:[Lcom/squareup/invoices/edit/InvoiceAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/squareup/invoices/edit/InvoiceAction;->connectionErrorString:I

    .line 41
    iput-boolean p4, p0, Lcom/squareup/invoices/edit/InvoiceAction;->isRecurring:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/edit/InvoiceAction;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/edit/InvoiceAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/edit/InvoiceAction;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->$VALUES:[Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {v0}, [Lcom/squareup/invoices/edit/InvoiceAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/edit/InvoiceAction;

    return-object v0
.end method


# virtual methods
.method public getConnectionError(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 3

    .line 53
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/InvoiceAction;->isRecurring:Z

    const-string v1, "action"

    if-eqz v0, :cond_0

    .line 54
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_invoice_service_series_subtitle:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/InvoiceAction;->getConnectionErrorId()I

    move-result v2

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 57
    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPLOAD:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne p0, v0, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/InvoiceAction;->getConnectionErrorId()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 60
    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_connection_error_invoice_service_subtitle:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/InvoiceAction;->getConnectionErrorId()I

    move-result v2

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getConnectionErrorId()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/invoices/edit/InvoiceAction;->connectionErrorString:I

    return v0
.end method

.method public isRecurring()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/InvoiceAction;->isRecurring:Z

    return v0
.end method
