.class public final Lcom/squareup/invoices/InvoiceContactFormatter;
.super Ljava/lang/Object;
.source "InvoiceContactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceContactFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceContactFormatter.kt\ncom/squareup/invoices/InvoiceContactFormatter\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u001a\u001e\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u000c\u0010\u0008\u001a\u00020\t*\u00020\nH\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "formatForDisplay",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "context",
        "Landroid/content/Context;",
        "nameClickableState",
        "Lcom/squareup/invoices/NameClickableState;",
        "showClickable",
        "toAddress",
        "Lcom/squareup/address/Address;",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final formatForDisplay(Lcom/squareup/protos/client/invoice/InvoiceContact;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;
    .locals 10

    const-string v0, "nameClickableState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "\n"

    if-eqz p0, :cond_3

    .line 49
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    .line 52
    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-eqz v5, :cond_2

    check-cast v5, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    :cond_1
    invoke-static {v5, v2, p2}, Lcom/squareup/invoices/InvoiceContactFormatter;->showClickable(Ljava/lang/CharSequence;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;

    move-result-object v2

    :cond_2
    aput-object v2, v3, v4

    const/4 p1, 0x1

    .line 53
    iget-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_company_name:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    aput-object p2, v3, p1

    const/4 p1, 0x2

    .line 54
    iget-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    aput-object p2, v3, p1

    const/4 p1, 0x3

    .line 55
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_phone_number:Ljava/lang/String;

    check-cast p0, Ljava/lang/CharSequence;

    aput-object p0, v3, p1

    .line 51
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 57
    invoke-static {v1}, Lcom/squareup/address/AddressFormatter;->formatForShippingDisplay(Lcom/squareup/address/Address;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    check-cast p0, Ljava/lang/Iterable;

    .line 58
    invoke-static {p0}, Lcom/squareup/util/CollectionsKt;->filterNotNullOrBlank(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    move-object v1, p0

    check-cast v1, Ljava/lang/Iterable;

    .line 59
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/util/AndroidCollectionsKt;->joinToCharSequence$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0

    .line 60
    :cond_3
    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public static synthetic formatForDisplay$default(Lcom/squareup/protos/client/invoice/InvoiceContact;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;ILjava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 45
    check-cast p1, Landroid/content/Context;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 46
    sget-object p2, Lcom/squareup/invoices/NameClickableState$Unclickable;->INSTANCE:Lcom/squareup/invoices/NameClickableState$Unclickable;

    check-cast p2, Lcom/squareup/invoices/NameClickableState;

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/squareup/invoices/InvoiceContactFormatter;->formatForDisplay(Lcom/squareup/protos/client/invoice/InvoiceContact;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private static final showClickable(Ljava/lang/CharSequence;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;
    .locals 4

    .line 69
    instance-of v0, p2, Lcom/squareup/invoices/NameClickableState$Clickable;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 71
    :cond_0
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    new-instance v1, Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, p2, p1, v2}, Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;-><init>(Lcom/squareup/invoices/NameClickableState;Landroid/content/Context;I)V

    check-cast v1, Lcom/squareup/ui/LinkSpan;

    .line 72
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object p0

    const-string p1, "LinkSpan.Builder(context\u2026g())\n      .asSpannable()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/CharSequence;

    :cond_1
    :goto_0
    return-object p0
.end method

.method private static final toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;
    .locals 0

    .line 63
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p0

    return-object p0
.end method
