.class public final Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;
.super Ljava/lang/Object;
.source "InvoiceReminderUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceReminderUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceReminderUtils.kt\ncom/squareup/invoices/util/InvoiceReminderUtilsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,137:1\n1462#2,8:138\n1360#2:146\n1429#2,3:147\n1360#2:150\n1429#2,2:151\n1431#2:156\n132#3,3:153\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceReminderUtils.kt\ncom/squareup/invoices/util/InvoiceReminderUtilsKt\n*L\n61#1,8:138\n90#1:146\n90#1,3:147\n131#1:150\n131#1,2:151\n131#1:156\n131#1,3:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u0010\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0001\u001a\u0010\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0001\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\u0008\u001a\u000c\u0010\t\u001a\u00020\n*\u00020\nH\u0002\u001a\u0010\u0010\u000b\u001a\u00020\u000c*\u0008\u0012\u0004\u0012\u00020\u00080\r\u001a\u0012\u0010\u000e\u001a\u00020\u000f*\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0011\u001a\u0016\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\r0\u0013*\u00020\u0014\u001a\"\u0010\u0015\u001a\u00020\n*\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u0019\u001a\u00020\u000c\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "NEW_REMINDER_RELATIVE_DAYS",
        "",
        "newReminderConfigProto",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "days",
        "newReminderInstanceProto",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "buildUponFixedMessage",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "clearInstances",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "containsDuplicateDays",
        "",
        "",
        "getReminderText",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "reminderDefaults",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "sanitizeReminders",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "isRecurring",
        "isCardOnFile",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final NEW_REMINDER_RELATIVE_DAYS:I = 0x1


# direct methods
.method public static final buildUponFixedMessage(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
    .locals 1

    const-string v0, "$this$buildUponFixedMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getCustomMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 71
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->updateCustomMessage(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private static final clearInstances(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 5

    .line 132
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 150
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 151
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 152
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 133
    check-cast v2, Lcom/squareup/wire/Message;

    .line 154
    invoke-virtual {v2}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 133
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    iput-object v4, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    .line 155
    invoke-virtual {v2}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 133
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 156
    :cond_1
    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    return-object p0
.end method

.method public static final containsDuplicateDays(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$containsDuplicateDays"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 138
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 139
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 141
    move-object v4, v3

    check-cast v4, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    .line 61
    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 142
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 143
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_1
    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-eq v0, p0, :cond_2

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static final getReminderText(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 6

    const-string v0, "$this$getReminderText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v0

    if-nez v0, :cond_0

    .line 35
    sget p0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_on_date:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v0

    if-gez v0, :cond_1

    .line 38
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_before_due_date:I

    goto :goto_0

    .line 40
    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_after_due_date:I

    .line 42
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x1

    const-string v3, "null cannot be cast to non-null type java.lang.String"

    const-string v4, "(this as java.lang.String).toLowerCase(locale)"

    const-string v5, "Locale.getDefault()"

    if-le v1, v2, :cond_3

    .line 43
    sget v1, Lcom/squareup/invoicesappletapi/R$string;->plural_days:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    const-string v2, "count"

    invoke-virtual {v1, v2, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 47
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_2

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 49
    :cond_3
    sget p0, Lcom/squareup/invoicesappletapi/R$string;->one_day:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 50
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_4

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    :goto_1
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 54
    check-cast p0, Ljava/lang/CharSequence;

    const-string v0, "day_count"

    invoke-virtual {p1, v0, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 50
    :cond_4
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final newReminderConfigProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;-><init>()V

    .line 21
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object p0

    const-string v0, ""

    .line 22
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object p0

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object p0

    const-string v0, "InvoiceReminderConfig.Bu\u2026essage(\"\")\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic newReminderConfigProto$default(IILjava/lang/Object;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
    .locals 0

    const/4 p2, 0x1

    and-int/2addr p1, p2

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    .line 19
    :cond_0
    invoke-static {p0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->newReminderConfigProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object p0

    return-object p0
.end method

.method public static final newReminderInstanceProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;-><init>()V

    .line 28
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object p0

    const-string v0, ""

    .line 29
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object p0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p0

    const-string v0, "InvoiceReminderInstance.\u2026essage(\"\")\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic newReminderInstanceProto$default(IILjava/lang/Object;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 0

    const/4 p2, 0x1

    and-int/2addr p1, p2

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    .line 26
    :cond_0
    invoke-static {p0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->newReminderInstanceProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p0

    return-object p0
.end method

.method public static final reminderDefaults(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$reminderDefaults"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-interface {p0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->invoiceDefaultsList()Lio/reactivex/Observable;

    move-result-object p0

    .line 124
    sget-object v0, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt$reminderDefaults$1;->INSTANCE:Lcom/squareup/invoices/util/InvoiceReminderUtilsKt$reminderDefaults$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "invoiceDefaultsList()\n  \u2026c_reminder_config\n      }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final sanitizeReminders(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 2

    const-string v0, "$this$sanitizeReminders"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    .line 94
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    if-eqz p3, :cond_0

    .line 98
    invoke-static {p0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->clearInstances(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string p2, "payment_request"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 146
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 147
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 148
    check-cast p3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 101
    iget-object p3, p3, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string v1, "it.reminders"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Ljava/util/Collection;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_1
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 102
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->sumOfInt(Ljava/lang/Iterable;)I

    move-result p1

    if-lez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 104
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    goto :goto_2

    .line 109
    :cond_3
    invoke-static {p0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->clearInstances(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    if-eqz p3, :cond_4

    .line 113
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 116
    :cond_4
    iget-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    const-string p2, "automatic_reminder_config"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    :goto_2
    return-object p0
.end method
