.class public Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;
.super Ljava/lang/Object;
.source "DismissedNewInvoiceFeatureTips.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;
    }
.end annotation


# instance fields
.field private final tipsDismissedSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->tipsDismissedSetting:Lcom/squareup/settings/LocalSetting;

    .line 12
    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method isDismissed(Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;)Z
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->tipsDismissedSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {p1}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method setDismissed(Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;)V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->tipsDismissedSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 19
    invoke-virtual {p1}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 20
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->tipsDismissedSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method
