.class final enum Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;
.super Ljava/lang/Enum;
.source "DismissedNewInvoiceFeatureTips.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "NewInvoiceFeatureTip"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

.field public static final enum AUTOMATIC_REMINDER_INVOICE_FEATURE:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 28
    new-instance v0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    const/4 v1, 0x0

    const-string v2, "AUTOMATIC_REMINDER_INVOICE_FEATURE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->AUTOMATIC_REMINDER_INVOICE_FEATURE:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    .line 27
    sget-object v2, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->AUTOMATIC_REMINDER_INVOICE_FEATURE:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->$VALUES:[Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->$VALUES:[Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    invoke-virtual {v0}, [Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    return-object v0
.end method
