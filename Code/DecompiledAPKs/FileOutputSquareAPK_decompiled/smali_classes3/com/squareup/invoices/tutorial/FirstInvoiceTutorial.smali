.class public Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;
.super Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;
.source "FirstInvoiceTutorial.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final helpBadge:Lcom/squareup/ui/help/HelpBadge;

.field private invoiceHasCustomer:Z

.field private invoiceHasItem:Z

.field private invoiceIsDraft:Z

.field private invoicesAppletActive:Z

.field private final invoicesAppletRunner:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private isCreatingCustomAmount:Z

.field private final presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;


# direct methods
.method public constructor <init>(Lcom/squareup/register/tutorial/TutorialPresenter;Ldagger/Lazy;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0}, Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;-><init>()V

    .line 70
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->NOT_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 77
    iput-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    .line 78
    iput-object p2, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    .line 79
    iput-object p3, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 80
    iput-object p4, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    .line 81
    iput-object p5, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->features:Lcom/squareup/settings/server/Features;

    .line 82
    iput-object p6, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    .line 83
    iput-object p7, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;

    .line 84
    iput-object p8, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method private canShowTutorialContent()Z
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasActiveTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletActive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 237
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletActive:Z

    if-eqz v0, :cond_7

    .line 238
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_create:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    return-void

    .line 241
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    if-eqz v0, :cond_1

    .line 242
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->triggerEditInvoiceScreenBanner()V

    return-void

    .line 244
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    if-eqz v0, :cond_2

    .line 245
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_choose_customer:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    return-void

    .line 247
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;

    if-eqz v0, :cond_3

    .line 248
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_customer_info:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    return-void

    .line 250
    :cond_3
    instance-of v0, p1, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    if-eqz v0, :cond_4

    .line 251
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_item:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    return-void

    .line 253
    :cond_4
    instance-of v0, p1, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;

    if-eqz v0, :cond_5

    return-void

    .line 255
    :cond_5
    instance-of p1, p1, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    if-eqz p1, :cond_6

    .line 257
    iget-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->isCreatingCustomAmount:Z

    if-eqz p1, :cond_6

    return-void

    .line 261
    :cond_6
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    goto :goto_0

    .line 263
    :cond_7
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    :goto_0
    return-void
.end method

.method private runTutorial()V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->refresh()V

    .line 232
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 233
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->getInstanceOfHistoryScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private triggerEditInvoiceScreenBanner()V
    .locals 3

    .line 295
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 296
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceHasCustomer:Z

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_customer:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 298
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceHasItem:Z

    if-nez v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_choose_item:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 300
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceIsDraft:Z

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_preview:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    :cond_3
    :goto_0
    return-void
.end method

.method private tutorialPreconditionsSatisfied()Z
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->STOPPED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->NOT_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 290
    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->hasInvoices()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 291
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public customerOnInvoice(Z)V
    .locals 0

    .line 174
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceHasCustomer:Z

    .line 175
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->triggerEditInvoiceScreenBanner()V

    return-void
.end method

.method public exitQuietly()V
    .locals 1

    .line 168
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->STOPPED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 169
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 170
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->endTutorial()V

    return-void
.end method

.method public forceStart()V
    .locals 1

    .line 163
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->MANUALLY_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 164
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->start()V

    return-void
.end method

.method public handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    .line 138
    check-cast p1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;

    invoke-virtual {p1, p2, p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;->handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;)V

    return-void
.end method

.method public isDraftInvoice(Z)V
    .locals 0

    .line 184
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceIsDraft:Z

    .line 185
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->triggerEditInvoiceScreenBanner()V

    return-void
.end method

.method public isTriggered()Z
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->shouldActivateTutorial()Z

    move-result v0

    return v0
.end method

.method public itemOnInvoice(Z)V
    .locals 0

    .line 179
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoiceHasItem:Z

    .line 180
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->triggerEditInvoiceScreenBanner()V

    return-void
.end method

.method public onContactUpdated(Z)V
    .locals 2

    .line 189
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 191
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_customer_info:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 193
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_customer_info_save:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onCustomAmountUpdated(Z)V
    .locals 2

    .line 199
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->isCreatingCustomAmount:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 201
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_item_info_save:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 203
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_add_item_info:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onRequestExitTutorial()V
    .locals 4

    .line 129
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    sget-object v2, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_QUIETLY:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_SEND:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->in([Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;

    invoke-direct {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v2, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v2}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/register/tutorial/TutorialPresenter;->prompt(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    :cond_0
    return-void
.end method

.method public onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 92
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$1;->$SwitchMap$com$squareup$invoices$tutorial$FirstInvoiceTutorial$State:[I

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    invoke-virtual {v1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 123
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown tutorial state for first invoice: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 110
    :pswitch_0
    instance-of v0, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    if-eqz v0, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 113
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 114
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fi_end_done:I

    invoke-direct {v0, v1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EndTutorialPrompt;-><init>(I)V

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 116
    invoke-interface {v1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v1

    .line 114
    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->prompt(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    goto :goto_0

    .line 103
    :pswitch_1
    instance-of v0, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    if-eqz v0, :cond_1

    .line 104
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->exitQuietly()V

    goto :goto_0

    .line 100
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 95
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 96
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->runTutorial()V

    :cond_2
    :goto_0
    :pswitch_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onShowingThanks()V
    .locals 0

    return-void
.end method

.method public readyToFinish()V
    .locals 1

    .line 209
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_SEND:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    :cond_0
    return-void
.end method

.method public readyToFinishQuietly()V
    .locals 1

    .line 215
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_QUIETLY:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    iput-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialState:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    :cond_0
    return-void
.end method

.method public setInvoicesAppletActive(Z)V
    .locals 0

    .line 155
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletActive:Z

    return-void
.end method

.method public setIsCreatingCustomAmount(Z)V
    .locals 0

    .line 159
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->isCreatingCustomAmount:Z

    return-void
.end method

.method public shouldActivateTutorial()Z
    .locals 1

    .line 268
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->invoicesAppletActive:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->tutorialPreconditionsSatisfied()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public startAndActivateTutorial()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasActiveTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->replaceTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->maybeLookForNewTutorial()V

    :goto_0
    return-void
.end method

.method public updatePreviewBanner(Ljava/lang/String;)V
    .locals 2

    .line 221
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->canShowTutorialContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->first_invoice_tutorial_preview_confirm:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "action"

    .line 223
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 224
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 225
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
