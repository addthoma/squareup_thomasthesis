.class final Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoicesAppletConfirmationScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TT;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "T",
        "r",
        "invoke",
        "(Ljava/lang/Object;)Lcom/squareup/receiving/FailureMessage$Parts;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/receiving/FailureMessage$Parts;"
        }
    .end annotation

    .line 235
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-static {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->access$convertToStatus(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Ljava/lang/Object;)Lcom/squareup/protos/client/Status;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;->invoke(Ljava/lang/Object;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
