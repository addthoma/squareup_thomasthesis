.class public Lcom/squareup/invoices/ui/RealInvoicesApplet;
.super Lcom/squareup/invoicesappletapi/InvoicesApplet;
.source "RealInvoicesApplet.java"


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final container:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/invoicesappletapi/InvoicesApplet;-><init>(Ldagger/Lazy;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->container:Ldagger/Lazy;

    .line 47
    iput-object p2, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->device:Lcom/squareup/util/Device;

    .line 48
    iput-object p3, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 49
    iput-object p4, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->features:Lcom/squareup/settings/server/Features;

    .line 50
    iput-object p5, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    return-void
.end method

.method private createInvoiceHistoryToDetailWrapper()Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 109
    new-instance v0, Lcom/squareup/invoices/ui/RealInvoicesApplet$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/RealInvoicesApplet$1;-><init>(Lcom/squareup/invoices/ui/RealInvoicesApplet;)V

    return-object v0
.end method


# virtual methods
.method public createInvoiceHistoryToSingleInvoiceDetail(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 0

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/RealInvoicesApplet;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 57
    sget-object p2, Lcom/squareup/invoices/ui/InvoiceDetailScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 58
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "Invoices"

    return-object v0
.end method

.method public getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    .line 87
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Lcom/squareup/container/ContainerTreeKey;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;

    aput-object v1, p1, v0

    const/4 v0, 0x1

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    aput-object v1, p1, v0

    .line 88
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 96
    sget-object v0, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 78
    sget v0, Lcom/squareup/features/invoices/R$string;->titlecase_invoices:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$visibility$0$RealInvoicesApplet(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 103
    iget-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_APPLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v0, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    .line 104
    invoke-interface {p1, v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldDisplayFeature(Lcom/squareup/permissions/Permission;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 103
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public returnAfterInvoicePayment()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-interface {v0, p0, v1}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public returnAfterInvoicePaymentCanceled()V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/RealInvoicesApplet;->createInvoiceHistoryToDetailWrapper()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v1

    sget-object v2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public showFullInvoiceDetailFromReadOnly()V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/RealInvoicesApplet;->createInvoiceHistoryToDetailWrapper()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v1

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 102
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$RealInvoicesApplet$fW6PxRT6jAJibxFGsw6kSnDSSAQ;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$RealInvoicesApplet$fW6PxRT6jAJibxFGsw6kSnDSSAQ;-><init>(Lcom/squareup/invoices/ui/RealInvoicesApplet;)V

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
