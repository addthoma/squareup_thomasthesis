.class Lcom/squareup/invoices/ui/InvoiceDetailView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InvoiceDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceDetailView;->createButtonView(Lcom/squareup/features/invoices/widgets/SectionElement;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

.field final synthetic val$buttonData:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$2;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$2;->val$buttonData:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 151
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$2;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$2;->val$buttonData:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getEvent()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V

    return-void
.end method
