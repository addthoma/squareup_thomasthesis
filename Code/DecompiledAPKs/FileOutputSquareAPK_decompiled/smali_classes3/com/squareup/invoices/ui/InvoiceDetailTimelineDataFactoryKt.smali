.class public final Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactoryKt;
.super Ljava/lang/Object;
.source "invoiceDetailTimelineDataFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "timelineBackgroundColor",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final timelineBackgroundColor(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)I
    .locals 1

    const-string v0, "$this$timelineBackgroundColor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {p0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object p0

    const-string v0, "InvoiceDisplayState.forI\u2026splayState(display_state)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/invoices/InvoiceDisplayState;->getDisplayColor()I

    move-result p0

    .line 39
    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    if-ne p0, v0, :cond_0

    .line 40
    sget p0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    :cond_0
    return p0
.end method
