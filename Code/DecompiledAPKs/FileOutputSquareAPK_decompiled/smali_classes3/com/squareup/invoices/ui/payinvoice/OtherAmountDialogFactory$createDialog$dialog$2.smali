.class final Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;
.super Ljava/lang/Object;
.source "OtherAmountDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $editor:Lcom/squareup/widgets/SelectableEditText;

.field final synthetic $screen:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

.field final synthetic this$0:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->this$0:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->$editor:Lcom/squareup/widgets/SelectableEditText;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->$screen:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 89
    iget-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->$editor:Lcom/squareup/widgets/SelectableEditText;

    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    .line 90
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->$screen:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;->getOnSave()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->this$0:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;

    invoke-static {v0}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->access$getPriceLocaleHelper$p(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    iget-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;->this$0:Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;

    invoke-static {p1}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->access$getCurrencyCode$p(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
