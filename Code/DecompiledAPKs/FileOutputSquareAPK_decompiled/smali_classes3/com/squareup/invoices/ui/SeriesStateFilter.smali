.class public final enum Lcom/squareup/invoices/ui/SeriesStateFilter;
.super Ljava/lang/Enum;
.source "SeriesStateFilter.java"

# interfaces
.implements Lcom/squareup/invoices/ui/GenericListFilter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/ui/SeriesStateFilter;",
        ">;",
        "Lcom/squareup/invoices/ui/GenericListFilter;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/ui/SeriesStateFilter;

.field public static final enum ACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

.field public static final enum DRAFT_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

.field public static final enum INACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;


# instance fields
.field private final shortTitleId:I

.field private final stateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

.field private final titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 19
    new-instance v6, Lcom/squareup/invoices/ui/SeriesStateFilter;

    sget v3, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_active:I

    sget v4, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_active_short:I

    sget-object v5, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->ACTIVE:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const-string v1, "ACTIVE_SERIES"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/SeriesStateFilter;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V

    sput-object v6, Lcom/squareup/invoices/ui/SeriesStateFilter;->ACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    .line 22
    new-instance v0, Lcom/squareup/invoices/ui/SeriesStateFilter;

    sget v10, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_inactive:I

    sget v11, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_inactive_short:I

    sget-object v12, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->ENDED:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const-string v8, "INACTIVE_SERIES"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/invoices/ui/SeriesStateFilter;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/SeriesStateFilter;->INACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    .line 25
    new-instance v0, Lcom/squareup/invoices/ui/SeriesStateFilter;

    sget v4, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_draft:I

    sget v5, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_draft_short:I

    sget-object v6, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->DRAFT:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const-string v2, "DRAFT_SERIES"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/ui/SeriesStateFilter;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/SeriesStateFilter;->DRAFT_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/invoices/ui/SeriesStateFilter;

    .line 18
    sget-object v1, Lcom/squareup/invoices/ui/SeriesStateFilter;->ACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/ui/SeriesStateFilter;->INACTIVE_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/ui/SeriesStateFilter;->DRAFT_SERIES:Lcom/squareup/invoices/ui/SeriesStateFilter;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/ui/SeriesStateFilter;->$VALUES:[Lcom/squareup/invoices/ui/SeriesStateFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;",
            ")V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->titleId:I

    .line 36
    iput p4, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->shortTitleId:I

    .line 37
    iput-object p5, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->stateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    return-void
.end method

.method public static getVisibleFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-static {}, Lcom/squareup/invoices/ui/SeriesStateFilter;->values()[Lcom/squareup/invoices/ui/SeriesStateFilter;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/ui/SeriesStateFilter;
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/invoices/ui/SeriesStateFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/ui/SeriesStateFilter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/ui/SeriesStateFilter;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/invoices/ui/SeriesStateFilter;->$VALUES:[Lcom/squareup/invoices/ui/SeriesStateFilter;

    invoke-virtual {v0}, [Lcom/squareup/invoices/ui/SeriesStateFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/ui/SeriesStateFilter;

    return-object v0
.end method


# virtual methods
.method public formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->titleId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLabel()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->shortTitleId:I

    return v0
.end method

.method public getSearchBarHint()I
    .locals 2

    .line 49
    sget-object v0, Lcom/squareup/invoices/ui/SeriesStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$SeriesStateFilter:[I

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/SeriesStateFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 57
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_active_series:I

    return v0

    .line 55
    :cond_0
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_draft_series:I

    return v0

    .line 53
    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_inactive_series:I

    return v0

    .line 51
    :cond_2
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_active_series:I

    return v0
.end method

.method public getStateFilter()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->stateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    return-object v0
.end method

.method public getTitle()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/invoices/ui/SeriesStateFilter;->titleId:I

    return v0
.end method

.method public isRecurringListFilter()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isStateFilter()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
