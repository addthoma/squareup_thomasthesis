.class public Lcom/squareup/invoices/ui/InvoiceLoader$Input;
.super Ljava/lang/Object;
.source "InvoiceLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Input"
.end annotation


# instance fields
.field public final query:Ljava/lang/String;

.field public final stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 120
    :cond_1
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;

    .line 121
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    .line 122
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 131
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Input{query=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' stateFilter = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
