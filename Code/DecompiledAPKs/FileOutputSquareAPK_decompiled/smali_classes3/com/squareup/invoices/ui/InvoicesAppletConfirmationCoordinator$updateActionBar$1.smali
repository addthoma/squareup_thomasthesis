.class final Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;
.super Ljava/lang/Object;
.source "InvoicesAppletConfirmationCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->updateActionBar(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

.field final synthetic $success:Z


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;->$runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    iput-boolean p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;->$success:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;->$runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;->$success:Z

    invoke-interface {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;->goBackFromConfirmationScreen(Z)V

    return-void
.end method
