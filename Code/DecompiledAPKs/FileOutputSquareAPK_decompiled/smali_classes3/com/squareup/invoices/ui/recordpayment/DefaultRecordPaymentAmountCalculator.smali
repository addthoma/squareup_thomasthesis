.class public final Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;
.super Ljava/lang/Object;
.source "DefaultRecordPaymentAmountCalculator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDefaultRecordPaymentAmountCalculator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DefaultRecordPaymentAmountCalculator.kt\ncom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,62:1\n1360#2:63\n1429#2,3:64\n*E\n*S KotlinDebug\n*F\n+ 1 DefaultRecordPaymentAmountCalculator.kt\ncom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator\n*L\n30#1:63\n30#1,3:64\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
        "",
        "paymentRequestDisplayStateFactory",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;",
        "paymentRequestReadOnlyInfoFactory",
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
        "(Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;)V",
        "computeForInvoice",
        "Lcom/squareup/protos/common/Money;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final paymentRequestDisplayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

.field private final paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRequestDisplayStateFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestReadOnlyInfoFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->paymentRequestDisplayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

    return-void
.end method


# virtual methods
.method public final computeForInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;
    .locals 4

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;->fromInvoiceDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object v0

    .line 30
    check-cast v0, Ljava/lang/Iterable;

    .line 63
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 64
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 65
    check-cast v2, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;

    .line 30
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->paymentRequestDisplayStateFactory:Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;

    invoke-virtual {v3, v2}, Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;->fromPaymentRequestReadOnlyInfo(Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;)Lcom/squareup/invoices/PaymentRequestDisplayState;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 31
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v0, "currencyCode"

    .line 32
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt;->initialRecordPaymentAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method
