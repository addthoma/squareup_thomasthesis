.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;
.super Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;
.source "InvoicePaymentAmountState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OtherAmountDialog"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u001a\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
        "amountTypes",
        "",
        "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
        "previousIndex",
        "",
        "(Ljava/util/List;I)V",
        "getAmountTypes",
        "()Ljava/util/List;",
        "getPreviousIndex",
        "()I",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;"
        }
    .end annotation
.end field

.field private final previousIndex:I


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "amountTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->amountTypes:Ljava/util/List;

    iput p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->copy(Ljava/util/List;I)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    return v0
.end method

.method public final copy(Ljava/util/List;I)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;I)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;"
        }
    .end annotation

    const-string v0, "amountTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;-><init>(Ljava/util/List;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    iget p1, p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAmountTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->amountTypes:Ljava/util/List;

    return-object v0
.end method

.method public final getPreviousIndex()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OtherAmountDialog(amountTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", previousIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->previousIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
