.class public final Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;
.super Ljava/lang/Object;
.source "InvoiceDetailPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceDetailPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final buttonDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEntryViewsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultCurrencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final partialTransactionDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PartialTransactionData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentRequestDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentRequestReadOnlyInfoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final withYearFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final withoutYearFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PartialTransactionData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 74
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 75
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 76
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 77
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 78
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->withYearFormatProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 79
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 80
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 81
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 82
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->withoutYearFormatProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 83
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->defaultCurrencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 84
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->partialTransactionDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 85
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->paymentRequestReadOnlyInfoFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 86
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 87
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->buttonDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 88
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 89
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PartialTransactionData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 110
    new-instance v17, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lflow/Flow;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/PartialTransactionData$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;)Lcom/squareup/invoices/ui/InvoiceDetailPresenter;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/server/Features;",
            "Lflow/Flow;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/invoices/PartialTransactionData$Factory;",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ")",
            "Lcom/squareup/invoices/ui/InvoiceDetailPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 123
    new-instance v17, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;-><init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lflow/Flow;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/PartialTransactionData$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceDetailPresenter;
    .locals 18

    move-object/from16 v0, p0

    .line 94
    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/cart/CartEntryViewsFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->withYearFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->withoutYearFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->defaultCurrencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->partialTransactionDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/invoices/PartialTransactionData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->paymentRequestReadOnlyInfoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoices/PaymentRequestData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->buttonDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    invoke-static/range {v2 .. v17}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lflow/Flow;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/PartialTransactionData$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;)Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter_Factory;->get()Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    move-result-object v0

    return-object v0
.end method
