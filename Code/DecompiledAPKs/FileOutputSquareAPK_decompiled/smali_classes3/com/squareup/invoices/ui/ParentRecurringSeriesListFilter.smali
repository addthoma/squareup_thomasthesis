.class public Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;
.super Ljava/lang/Object;
.source "ParentRecurringSeriesListFilter.java"

# interfaces
.implements Lcom/squareup/invoices/ui/GenericListFilter;


# instance fields
.field private final seriesNumber:Ljava/lang/String;

.field private final seriesToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p2, p0, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->seriesNumber:Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->seriesToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->seriesNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "number"

    .line 31
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLabel()I
    .locals 1

    .line 22
    sget v0, Lcom/squareup/common/invoices/R$string;->series_list_title:I

    return v0
.end method

.method public getSearchBarHint()I
    .locals 1

    .line 26
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_in_series:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    .line 18
    sget v0, Lcom/squareup/common/invoices/R$string;->series_list_title:I

    return v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->seriesToken:Ljava/lang/String;

    return-object v0
.end method

.method public isRecurringListFilter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isStateFilter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
