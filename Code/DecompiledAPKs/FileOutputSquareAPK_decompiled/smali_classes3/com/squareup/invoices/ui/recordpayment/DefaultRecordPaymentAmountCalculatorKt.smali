.class public final Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt;
.super Ljava/lang/Object;
.source "DefaultRecordPaymentAmountCalculator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDefaultRecordPaymentAmountCalculator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DefaultRecordPaymentAmountCalculator.kt\ncom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,62:1\n415#2,2:63\n1078#2,3:65\n250#3,2:68\n*E\n*S KotlinDebug\n*F\n+ 1 DefaultRecordPaymentAmountCalculator.kt\ncom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt\n*L\n47#1,2:63\n48#1,3:65\n53#1,2:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "initialRecordPaymentAmount",
        "Lcom/squareup/protos/common/Money;",
        "displayStateList",
        "",
        "Lcom/squareup/invoices/PaymentRequestDisplayState;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final initialRecordPaymentAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/PaymentRequestDisplayState;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "displayStateList"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 64
    sget-object v1, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt$initialRecordPaymentAmount$$inlined$filterIsInstance$1;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculatorKt$initialRecordPaymentAmount$$inlined$filterIsInstance$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 48
    new-instance v1, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v1, v4, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 66
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;

    .line 49
    invoke-virtual {v4}, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;->getOverdueAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v4, "MoneyMath.sum(acc, overdue.overdueAmount)"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/PaymentRequestDisplayState;

    .line 53
    instance-of v5, v4, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;

    if-nez v5, :cond_3

    instance-of v4, v4, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_1

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    :goto_3
    check-cast v0, Lcom/squareup/invoices/PaymentRequestDisplayState;

    .line 55
    instance-of p0, v0, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;

    if-eqz p0, :cond_5

    check-cast v0, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;->getUnpaidAmount()Lcom/squareup/protos/common/Money;

    move-result-object p0

    goto :goto_4

    .line 56
    :cond_5
    instance-of p0, v0, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    if-eqz p0, :cond_6

    check-cast v0, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;->getRemainingAmount()Lcom/squareup/protos/common/Money;

    move-result-object p0

    goto :goto_4

    .line 57
    :cond_6
    new-instance p0, Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 60
    :goto_4
    invoke-static {v1, p0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string p1, "MoneyMath.sum(overdueAmount, unpaidAmount)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 64
    :cond_7
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlin.sequences.Sequence<R>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
