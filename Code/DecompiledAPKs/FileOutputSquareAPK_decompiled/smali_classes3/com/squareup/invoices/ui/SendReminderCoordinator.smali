.class public Lcom/squareup/invoices/ui/SendReminderCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SendReminderCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private reminderMessageConfirmationMessage:Lcom/squareup/widgets/MessageView;

.field private reminderMessageEditText:Lcom/squareup/widgets/SelectableEditText;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 30
    iput-object p2, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 2

    .line 72
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 73
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/ui/SendReminderCoordinator;->getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 75
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_reminder_custom_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->reminderMessageEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 76
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_reminder_confirmation_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->reminderMessageConfirmationMessage:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private formatConfirmationMessage()Ljava/lang/CharSequence;
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_two_separator:I

    .line 46
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    .line 47
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/utilities/R$string;->list_pattern_long_final_separator:I

    .line 48
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-static {v0, v1, v2}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object v0

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    iget-object v2, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v2, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 53
    iget-object v2, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_send_reminder_confirmation_message:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 55
    invoke-virtual {v0, v1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "emails"

    invoke-virtual {v2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 60
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 61
    iget-object v1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$XZFb249rSmFgZ4O4ui7RcGqigTc;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/ui/-$$Lambda$XZFb249rSmFgZ4O4ui7RcGqigTc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_send_reminder_title:I

    .line 63
    invoke-interface {p1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 62
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 64
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_send_reminder_button:I

    .line 66
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$SendReminderCoordinator$5JSr0GM_xC5MA0Y6RMpURBtlVmU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$SendReminderCoordinator$5JSr0GM_xC5MA0Y6RMpURBtlVmU;-><init>(Lcom/squareup/invoices/ui/SendReminderCoordinator;)V

    .line 67
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 68
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$5JSr0GM_xC5MA0Y6RMpURBtlVmU(Lcom/squareup/invoices/ui/SendReminderCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/SendReminderCoordinator;->onSendClicked()V

    return-void
.end method

.method private onSendClicked()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->reminderMessageEditText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->sendReminder(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/SendReminderCoordinator;->bindViews(Landroid/view/View;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/invoices/ui/SendReminderCoordinator;->reminderMessageConfirmationMessage:Lcom/squareup/widgets/MessageView;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/SendReminderCoordinator;->formatConfirmationMessage()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
