.class public final Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;
.super Ljava/lang/Object;
.source "FloatingHeaderBodyRows.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFloatingHeaderBodyRows.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FloatingHeaderBodyRows.kt\ncom/squareup/invoices/ui/FloatingHeaderBodyRowsKt\n*L\n1#1,36:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u001c\n\u0000\u001a\'\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0016\u0010\u0003\u001a\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u00010\u00050\u0004\"\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0010\u0010\u0003\u001a\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "setBodyAndVisibility",
        "",
        "Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;",
        "lines",
        "",
        "",
        "(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V",
        "",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;Ljava/lang/Iterable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$setBodyAndVisibility"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 20
    invoke-static {p1}, Lcom/squareup/util/CollectionsKt;->filterNotNullOrBlank(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    const-string p1, "\n"

    .line 21
    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/squareup/util/AndroidCollectionsKt;->joinToCharSequence$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 23
    :goto_0
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_3

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setBodyText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method public static final varargs setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "$this$setBodyAndVisibility"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lines"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p1}, Lkotlin/collections/ArraysKt;->asIterable([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;Ljava/lang/Iterable;)V

    return-void
.end method
