.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "NullViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V",
        "image",
        "Landroid/widget/ImageView;",
        "message",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "Landroid/widget/TextView;",
        "bind",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final image:Landroid/widget/ImageView;

.field private final message:Lcom/squareup/widgets/MessageView;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 698
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 699
    sget p1, Lcom/squareup/features/invoices/R$id;->null_state_image:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->image:Landroid/widget/ImageView;

    .line 700
    sget p1, Lcom/squareup/features/invoices/R$id;->null_state_title:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->title:Landroid/widget/TextView;

    .line 701
    sget p1, Lcom/squareup/features/invoices/R$id;->null_state_message:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public final bind()V
    .locals 2

    .line 704
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->image:Landroid/widget/ImageView;

    sget v1, Lcom/squareup/features/invoices/R$drawable;->no_invoices:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 705
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasNeverCreatedInvoice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->title:Landroid/widget/TextView;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_no_invoice_null_state_title:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 707
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->message:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_no_invoice_null_state_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    goto :goto_0

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->title:Landroid/widget/TextView;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_no_invoices_for_filter_title:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 710
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->message:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_no_invoices_for_filter_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    :goto_0
    return-void
.end method
