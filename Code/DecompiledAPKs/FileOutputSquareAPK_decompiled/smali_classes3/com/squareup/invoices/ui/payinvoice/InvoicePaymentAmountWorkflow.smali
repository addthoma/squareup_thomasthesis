.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "InvoicePaymentAmountWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaymentAmountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,289:1\n149#2,5:290\n149#2,5:295\n149#2,5:300\n149#2,5:305\n149#2,5:310\n1360#3:315\n1429#3,3:316\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow\n*L\n182#1,5:290\n200#1,5:295\n209#1,5:300\n234#1,5:305\n241#1,5:310\n248#1:315\n248#1,3:316\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\'B-\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u001a\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00022\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016JN\u0010\u0019\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00032\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0003H\u0016J\u0014\u0010\u001e\u001a\u00020\u001f*\u00020 2\u0006\u0010!\u001a\u00020\u001fH\u0002J\u000c\u0010\"\u001a\u00020\u001f*\u00020\u000eH\u0002J\u0018\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$*\u0008\u0012\u0004\u0012\u00020&0$H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "defaultRecordPaymentAmount",
        "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;Lcom/squareup/protos/common/CurrencyCode;)V",
        "zeroMoney",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "actionBarTitle",
        "",
        "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;",
        "default",
        "format",
        "toOptionRows",
        "",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;",
        "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
        "Action",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultRecordPaymentAmount:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final zeroMoney:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultRecordPaymentAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->defaultRecordPaymentAmount:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    const-wide/16 p1, 0x0

    .line 53
    invoke-static {p1, p2, p4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->zeroMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method private final actionBarTitle(Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 267
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method private final format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 272
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final toOptionRows(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;",
            ">;"
        }
    .end annotation

    .line 248
    check-cast p1, Ljava/lang/Iterable;

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 316
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 317
    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 250
    instance-of v2, v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;

    .line 251
    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_amount_option_next_payment:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 252
    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-direct {v2, v3, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 254
    :cond_0
    instance-of v2, v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;

    if-eqz v2, :cond_1

    new-instance v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;

    .line 255
    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_amount_option_remaining_amount:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 256
    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-direct {v2, v3, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 258
    :cond_1
    instance-of v2, v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    if-eqz v2, :cond_3

    new-instance v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;

    .line 259
    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_amount_option_other_amount:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 258
    :goto_1
    invoke-direct {v2, v3, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :goto_2
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 318
    :cond_4
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;
    .locals 5

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;->getInvoice()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    .line 143
    new-instance p2, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;

    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->defaultRecordPaymentAmount:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->computeForInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 144
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->totalRemaining(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 145
    new-instance p1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    const/4 v1, 0x0

    invoke-direct {p1, v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 149
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$NextPayment;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$RemainingAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_0

    new-array p2, v2, [Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 150
    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    aput-object v0, p2, v4

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    aput-object p1, p2, v3

    .line 149
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 152
    check-cast p2, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    aput-object p2, v1, v4

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    aput-object v0, v1, v3

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    aput-object p1, v1, v2

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 155
    :goto_0
    new-instance p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    invoke-direct {p2, p1, v4}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;-><init>(Ljava/util/List;I)V

    check-cast p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->initialState(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;

    check-cast p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->render(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
            "-",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 166
    instance-of v0, p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    const-string v1, "Cannot be displaying empty other option"

    const-string v2, ""

    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object p1

    move-object v0, p2

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getSelectedIndex()I

    move-result v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 168
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 172
    new-instance p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    .line 174
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->toOptionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 175
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getSelectedIndex()I

    move-result v6

    .line 176
    new-instance p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$1;

    invoke-direct {p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 177
    new-instance p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$2;

    invoke-direct {p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 178
    new-instance p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$3;

    invoke-direct {p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v3, p1

    .line 172
    invoke-direct/range {v3 .. v9}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;-><init>(Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 291
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 292
    const-class p3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 293
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 291
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 183
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 186
    :cond_1
    instance-of v0, p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;

    if-eqz v0, :cond_3

    .line 187
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object p1

    move-object v0, p2

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;->getSelectedIndex()I

    move-result v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 188
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 192
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 193
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    .line 195
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->toOptionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 196
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;->getSelectedIndex()I

    move-result v6

    .line 197
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$4;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$4;

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 198
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$5;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$5;

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 199
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$6;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$6;

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v3, v1

    .line 193
    invoke-direct/range {v3 .. v9}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;-><init>(Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 296
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 297
    const-class v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 298
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 296
    invoke-direct {p2, v0, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 202
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoiceAmountErrorDialogScreen;

    .line 203
    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->zero_amount_error:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->zero_amount_error_body:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 205
    iget-object v4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->zeroMoney:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string/jumbo v5, "zero_money"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 206
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 207
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 208
    new-instance v4, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$7;

    invoke-direct {v4, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$7;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 202
    invoke-direct {v0, v1, v3, v4}, Lcom/squareup/invoices/ui/payinvoice/InvoiceAmountErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 301
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 302
    const-class v1, Lcom/squareup/invoices/ui/payinvoice/InvoiceAmountErrorDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 303
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 301
    invoke-direct {p3, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 192
    invoke-virtual {p1, p2, p3}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 210
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 188
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 213
    :cond_3
    instance-of v0, p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    if-eqz v0, :cond_6

    .line 214
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt;->access$getOtherAmountAndIndex(Ljava/util/List;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 216
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    move-object v3, p2

    check-cast v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getPreviousIndex()I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 221
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 220
    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->actionBarTitle(Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 226
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 227
    new-instance v10, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    .line 229
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->toOptionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 231
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$8;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$8;

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 232
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$9;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$9;

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 233
    sget-object p2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$10;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$10;

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v3, v10

    .line 227
    invoke-direct/range {v3 .. v9}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;-><init>(Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v10, Lcom/squareup/workflow/legacy/V2Screen;

    .line 306
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 307
    const-class v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 308
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 306
    invoke-direct {p2, v3, v10, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 236
    new-instance v3, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

    .line 237
    iget-object v4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->zeroMoney:Lcom/squareup/protos/common/Money;

    :goto_0
    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;->getInvoice()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->totalRemaining(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 239
    new-instance v4, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$11;

    invoke-direct {v4, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$11;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 240
    new-instance v5, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$12;

    invoke-direct {v5, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$render$12;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 236
    invoke-direct {v3, v1, p1, v4, v5}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 311
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 312
    const-class p3, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 313
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 311
    invoke-direct {p1, p3, v3, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 226
    invoke-virtual {v0, p2, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 242
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    .line 221
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Viewing other amount dialog - previous selection must have non-null amount."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 242
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;->snapshotState(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
