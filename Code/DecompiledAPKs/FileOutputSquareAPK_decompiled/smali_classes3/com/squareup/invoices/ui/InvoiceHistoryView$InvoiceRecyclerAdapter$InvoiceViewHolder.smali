.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "InvoiceViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V",
        "row",
        "Lcom/squareup/ui/account/view/SmartLineRow;",
        "getRow",
        "()Lcom/squareup/ui/account/view/SmartLineRow;",
        "bind",
        "",
        "position",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final row:Lcom/squareup/ui/account/view/SmartLineRow;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 775
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 776
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method public final bind(I)V
    .locals 5

    .line 779
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getRow(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->getInvoice$invoices_hairball_release()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    .line 781
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 784
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    .line 785
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {v3}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getCustomerTitle(Lcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 786
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 787
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 788
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 789
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 791
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 792
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 793
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 800
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    const-string v1, "displayState"

    if-eqz v0, :cond_1

    .line 802
    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    .line 801
    invoke-static {v0}, Lcom/squareup/invoices/ui/SeriesDisplayState;->forSeriesDisplayState(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)Lcom/squareup/invoices/ui/SeriesDisplayState;

    move-result-object v0

    .line 804
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/SeriesDisplayState;->getTitle()I

    move-result v1

    .line 805
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/SeriesDisplayState;->getDisplayColor()I

    move-result v0

    goto :goto_0

    .line 807
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_2

    .line 809
    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 808
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 811
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v1

    .line 812
    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getDisplayColor()I

    move-result v0

    .line 815
    :goto_0
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleText(I)V

    .line 816
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleColor(I)V

    .line 817
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleVisible(Z)V

    .line 819
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    return-void

    .line 812
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 776
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    return-object v0
.end method
