.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecordPaymentConfirmationCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0018\u0010\u0013\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "runner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;",
        "(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "glyphMessageView",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "isSuccessful",
        "",
        "populateFromScreenData",
        "screenData",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;Landroid/view/View;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->populateFromScreenData(Landroid/view/View;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 60
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 61
    sget v0, Lcom/squareup/features/invoices/R$id;->confirmation_glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method private final configureActionBar(Z)V
    .locals 6

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 53
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->record_payment:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v3, 0x1

    .line 54
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 55
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;Z)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final populateFromScreenData(Landroid/view/View;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;)V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "glyphMessageView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 44
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$populateFromScreenData$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$populateFromScreenData$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;->isSuccessful()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->configureActionBar(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;->recordPaymentConfirmationScreenData()Lrx/Observable;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.recordPaymentConf\u2026nData(view, it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
