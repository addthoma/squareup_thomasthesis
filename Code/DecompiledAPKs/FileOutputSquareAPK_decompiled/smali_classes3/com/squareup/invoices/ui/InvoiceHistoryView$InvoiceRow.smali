.class final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;
.super Ljava/lang/Object;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InvoiceRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;",
        "",
        "invoice",
        "Lcom/squareup/invoices/DisplayDetails;",
        "(Lcom/squareup/invoices/DisplayDetails;)V",
        "headerDate",
        "Ljava/util/Date;",
        "getHeaderDate$invoices_hairball_release",
        "()Ljava/util/Date;",
        "getInvoice$invoices_hairball_release",
        "()Lcom/squareup/invoices/DisplayDetails;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final headerDate:Ljava/util/Date;

.field private final invoice:Lcom/squareup/invoices/DisplayDetails;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/DisplayDetails;)V
    .locals 1

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->invoice:Lcom/squareup/invoices/DisplayDetails;

    .line 379
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->invoice:Lcom/squareup/invoices/DisplayDetails;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getSortDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->Iso8601ToNormalizedDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    const-string v0, "Times.Iso8601ToNormalize\u2026ice.sortDate.date_string)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->headerDate:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public final getHeaderDate$invoices_hairball_release()Ljava/util/Date;
    .locals 1

    .line 379
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->headerDate:Ljava/util/Date;

    return-object v0
.end method

.method public final getInvoice$invoices_hairball_release()Lcom/squareup/invoices/DisplayDetails;
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->invoice:Lcom/squareup/invoices/DisplayDetails;

    return-object v0
.end method
