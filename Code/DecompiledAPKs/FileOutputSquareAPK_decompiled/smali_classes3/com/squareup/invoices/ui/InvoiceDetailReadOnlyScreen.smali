.class public Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InvoiceDetailReadOnlyScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;,
        Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final invoiceId:Ljava/lang/String;

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyScreen$4lKNAwTrmtI-nAR2AlI_3oB9oFQ;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyScreen$4lKNAwTrmtI-nAR2AlI_3oB9oFQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 28
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->invoiceId:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;
    .locals 2

    .line 61
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 62
    const-class v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 63
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;-><init>(Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->invoiceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method getInvoiceId()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->invoiceId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->invoiceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 67
    sget v0, Lcom/squareup/common/invoices/R$layout;->invoice_read_only_detail_view:I

    return v0
.end method
