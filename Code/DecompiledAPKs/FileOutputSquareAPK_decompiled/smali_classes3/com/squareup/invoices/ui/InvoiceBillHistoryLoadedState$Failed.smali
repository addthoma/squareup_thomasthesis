.class public final Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;
.super Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;
.source "InvoiceBillLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;",
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
        "failureTitle",
        "",
        "failureDescription",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getFailureDescription",
        "()Ljava/lang/String;",
        "getFailureTitle",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureDescription:Ljava/lang/String;

.field private final failureTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "failureTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureDescription"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 151
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->failureTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->failureDescription:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getFailureDescription()Ljava/lang/String;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->failureDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getFailureTitle()Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->failureTitle:Ljava/lang/String;

    return-object v0
.end method
