.class public final Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;
.super Ljava/lang/Object;
.source "CancelInvoiceCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;-><init>(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->newInstance(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator_Factory;->get()Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    move-result-object v0

    return-object v0
.end method
