.class public abstract Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$AddPaymentModule;
.super Ljava/lang/Object;
.source "RecordPaymentScope.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AddPaymentModule"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\nJ\u0015\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$AddPaymentModule;",
        "",
        "()V",
        "provideRecordPaymentConfirmationScreenRunner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;",
        "scopeRunner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;",
        "provideRecordPaymentConfirmationScreenRunner$invoices_hairball_release",
        "provideRecordPaymentMethodScreenRunner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;",
        "provideRecordPaymentMethodScreenRunner$invoices_hairball_release",
        "provideRecordPaymentScreenRunner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;",
        "provideRecordPaymentScreenRunner$invoices_hairball_release",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideRecordPaymentConfirmationScreenRunner$invoices_hairball_release(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideRecordPaymentMethodScreenRunner$invoices_hairball_release(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideRecordPaymentScreenRunner$invoices_hairball_release(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
