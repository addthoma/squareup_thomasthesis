.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultRecordPaymentAmountCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final recordPaymentConfirmationScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final recordPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->invoicesAppletScopeRunnerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->recordPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->recordPaymentConfirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->defaultRecordPaymentAmountCalculatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;)",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;"
        }
    .end annotation

    .line 64
    new-instance v8, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lflow/Flow;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;
    .locals 9

    .line 73
    new-instance v8, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lflow/Flow;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->invoicesAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->recordPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->recordPaymentConfirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->defaultRecordPaymentAmountCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    invoke-static/range {v1 .. v7}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->newInstance(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lflow/Flow;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner_Factory;->get()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    move-result-object v0

    return-object v0
.end method
