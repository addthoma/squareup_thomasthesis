.class public Lcom/squareup/invoices/ui/SendReminderConfirmationDialog$Factory;
.super Ljava/lang/Object;
.source "SendReminderConfirmationDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 36
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/4 p1, 0x0

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->sendReminder(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 39
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    .line 24
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;->scopeRunner()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-result-object v0

    .line 27
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_reminder_confirmation_body:I

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 28
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    const-string v3, "email"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 30
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 32
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_reminder_confirmation_title:I

    .line 33
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/utilities/R$string;->send:I

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$SendReminderConfirmationDialog$Factory$z1oAkJRA_KOm-di0RlPCOB8zmeQ;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/ui/-$$Lambda$SendReminderConfirmationDialog$Factory$z1oAkJRA_KOm-di0RlPCOB8zmeQ;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 35
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$SendReminderConfirmationDialog$Factory$D-qqoZgVDYh0vqr86099T_Q6nf8;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$SendReminderConfirmationDialog$Factory$D-qqoZgVDYh0vqr86099T_Q6nf8;

    .line 38
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 41
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 42
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
