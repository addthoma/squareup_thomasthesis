.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;
.super Ljava/lang/Object;
.source "InvoicePaymentStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;-><init>(Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->newInstance(Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter_Factory;->get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    move-result-object v0

    return-object v0
.end method
