.class public abstract Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;
.super Lmortar/ViewPresenter;
.source "AbstractInvoiceDetailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

.field protected final clock:Lcom/squareup/util/Clock;

.field protected final features:Lcom/squareup/settings/server/Features;

.field protected final flow:Lflow/Flow;

.field protected final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;

.field protected final withYearFormat:Ljava/text/DateFormat;

.field protected final withoutYearFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    .line 85
    iput-object p2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 86
    iput-object p3, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->flow:Lflow/Flow;

    .line 87
    iput-object p4, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 88
    iput-object p5, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withoutYearFormat:Ljava/text/DateFormat;

    .line 89
    iput-object p6, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    .line 90
    iput-object p7, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    .line 91
    iput-object p8, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private getDueDateValue(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;
    .locals 8

    .line 256
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v1}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 258
    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 259
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 261
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->isSentInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    .line 262
    iget-object v5, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 263
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 262
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValueWithDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 263
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 266
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->isSendInFutureInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    .line 267
    iget-object v5, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 268
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    move-object v3, v1

    .line 267
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValueWithDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 268
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 272
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/util/Clock;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v4, 0x1

    .line 273
    iget-object v5, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 274
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 273
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValueWithDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 274
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 v4, 0x1

    .line 276
    iget-object v5, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 277
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    move-object v3, v1

    .line 276
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValueWithDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 277
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 373
    iget-object p3, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p3, "date"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    return-object p3
.end method

.method private getSendDateValue(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;
    .locals 3

    .line 237
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 238
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 240
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->isSentInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getISODateString(Lcom/squareup/protos/client/ISO8601Date;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 244
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->isSendInFutureInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-static {v1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/util/Clock;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 252
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getStatusString(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;
    .locals 3

    .line 313
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 314
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v1

    .line 315
    sget-object v2, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter$1;->$SwitchMap$com$squareup$invoices$InvoiceDisplayState:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    const-string p1, ""

    return-object p1

    .line 362
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 364
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 360
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 346
    :pswitch_3
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 347
    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_scheduled:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 349
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 341
    :pswitch_4
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 342
    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_recurring:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 344
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 341
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 326
    :pswitch_5
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 327
    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getISODateString(Lcom/squareup/protos/client/ISO8601Date;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_refunded:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 329
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 317
    :pswitch_6
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 318
    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getISODateString(Lcom/squareup/protos/client/ISO8601Date;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_canceled:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 320
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 322
    :pswitch_7
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    .line 323
    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getISODateString(Lcom/squareup/protos/client/ISO8601Date;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_paid:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 324
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 322
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_8
    if-eqz v1, :cond_0

    .line 332
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    .line 333
    invoke-static {p1, v0}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_overdue:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 335
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 332
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 337
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_9
    if-eqz v1, :cond_1

    .line 352
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->clock:Lcom/squareup/util/Clock;

    .line 353
    invoke-static {p1, v0}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_status_unpaid:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 355
    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 352
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getFormattedStatusString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 357
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initActionBar()V
    .locals 4

    .line 184
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    const/4 v1, 0x1

    .line 185
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/ui/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 186
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 187
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private initializeDates(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 4

    .line 212
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    .line 213
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    .line 215
    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v2, :cond_0

    .line 216
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->hideSentRow()V

    goto :goto_0

    .line 218
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getSendDateValue(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setInvoiceDateRowValue(Ljava/lang/String;)V

    .line 221
    :goto_0
    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v2, :cond_2

    .line 222
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v2}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->isSendDateImmediatelyOrFuture(Lcom/squareup/invoices/InvoiceDisplayState;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_charge:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setInvoiceDateRowTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 225
    :cond_1
    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_charged:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setInvoiceDateRowTitle(Ljava/lang/String;)V

    .line 229
    :cond_2
    :goto_1
    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v0, v2, :cond_4

    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    .line 232
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getDueDateValue(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setDueRowValue(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    :goto_2
    const/4 p1, 0x0

    .line 230
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setDueRowVisibility(Z)V

    :goto_3
    return-void
.end method

.method private isSendDateImmediatelyOrFuture(Lcom/squareup/invoices/InvoiceDisplayState;)Z
    .locals 1

    .line 302
    sget-object v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter$1;->$SwitchMap$com$squareup$invoices$InvoiceDisplayState:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isSendInFutureInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z
    .locals 1

    .line 293
    sget-object v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter$1;->$SwitchMap$com$squareup$invoices$InvoiceDisplayState:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private isSentInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)Z
    .locals 2

    .line 281
    sget-object v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter$1;->$SwitchMap$com$squareup$invoices$InvoiceDisplayState:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    return v0
.end method


# virtual methods
.method public createActionBarConfigForInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 191
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_detail_title:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getDetailTitle(ILcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 192
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 193
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 194
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    return-object v0
.end method

.method protected getDetailTitle(ILcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/CharSequence;
    .locals 1

    .line 201
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    goto :goto_0

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    const-string v0, "number"

    .line 205
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 206
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->initActionBar()V

    return-void
.end method

.method protected populateItems(Lcom/squareup/protos/client/bills/Cart;)V
    .locals 2

    .line 99
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->clearLineItems()V

    .line 102
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceCartUtils;->isEmpty(Lcom/squareup/protos/client/bills/Cart;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    .line 103
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setLineItemVisibility(Z)V

    goto :goto_0

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->cartEntryViewsFactory:Lcom/squareup/ui/cart/CartEntryViewsFactory;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/cart/CartEntryViewsFactory;->buildCartEntries(Lcom/squareup/protos/client/bills/Cart;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRows(Ljava/util/List;)V

    .line 108
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getSubTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getDiscountViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRows(Ljava/util/List;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTaxViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRows(Ljava/util/List;)V

    .line 111
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTipView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryViews;->getTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    const/4 p1, 0x1

    .line 114
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setLineItemVisibility(Z)V

    :goto_0
    return-void
.end method

.method public updateInvoiceDisplayDetailsSections(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 7

    .line 119
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    .line 121
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v1

    .line 124
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    const-string v3, ""

    if-eqz v2, :cond_0

    .line 125
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/SeriesDetails;->recurrence_rule_text:Lcom/squareup/protos/client/invoice/RecurrenceRuleText;

    if-eqz v2, :cond_0

    .line 128
    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/RecurrenceRuleText;->newBuilder()Lcom/squareup/protos/client/invoice/RecurrenceRuleText$Builder;

    move-result-object v2

    .line 129
    iget-object v3, v2, Lcom/squareup/protos/client/invoice/RecurrenceRuleText$Builder;->repeat_clause:Ljava/lang/String;

    .line 130
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/RecurrenceRuleText$Builder;->end_clause:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, v3

    .line 133
    :goto_0
    sget-object v4, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-ne v1, v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v0, v4, v3, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setSingleInvoiceInRecurringSeries(ZLjava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->getDisplayColor()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getStatusString(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/String;

    move-result-object v3

    .line 136
    invoke-virtual {v0, v2, v3}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setInvoiceDisplayStateText(ILjava/lang/String;)V

    .line 139
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->initializeDates(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 141
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 142
    invoke-static {v2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    .line 144
    :goto_2
    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->isUnpaid()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->isSentOrShared()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    .line 143
    :goto_3
    invoke-virtual {v0, v5}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setShareLinkTextVisibility(Z)V

    .line 146
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    if-eqz p1, :cond_4

    .line 147
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ShippingAddress;->formatted_address:Ljava/lang/String;

    .line 148
    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 149
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ShippingAddress;->formatted_address:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/ShippingAddress;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->showShippingAddress(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 151
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->hideShippingAddress()V

    :goto_4
    return-void
.end method

.method public updateSharedInvoiceTemplateSections(Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 4

    .line 162
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;

    .line 163
    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_invoice_total:I

    .line 164
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 165
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/NameClickableState$Unclickable;->INSTANCE:Lcom/squareup/invoices/NameClickableState$Unclickable;

    invoke-static {v1, v2, v3}, Lcom/squareup/invoices/InvoiceContactFormatter;->formatForDisplay(Lcom/squareup/protos/client/invoice/InvoiceContact;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setBillToRow(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setTitleRow(Ljava/lang/String;)V

    .line 167
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setIdRow(Ljava/lang/String;)V

    .line 170
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setAdditionalRecipients(Ljava/util/List;)V

    .line 171
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->setInvoiceMessage(Ljava/lang/String;)V

    .line 173
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    .line 174
    iget-object v2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_FILE_ATTACHMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 176
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 177
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v1, v2, :cond_1

    .line 178
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->showFileAttachmentHeader()V

    .line 179
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->showFileAttachmentRows(Ljava/util/List;)V

    :cond_1
    return-void
.end method
