.class public final Lcom/squareup/invoices/ui/AddPaymentCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AddPaymentCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0008\u0010\u0010\u001a\u00020\u000cH\u0002J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/AddPaymentCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "runner",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;",
        "(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "paymentTypeContainer",
        "Landroid/widget/LinearLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "createSectionView",
        "Lcom/squareup/invoices/ui/InvoicePaymentTypeView;",
        "sectionData",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;",
        "context",
        "Landroid/content/Context;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private paymentTypeContainer:Landroid/widget/LinearLayout;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;

    return-void
.end method

.method public static final synthetic access$createSectionView(Lcom/squareup/invoices/ui/AddPaymentCoordinator;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;Landroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->createSectionView(Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;Landroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPaymentTypeContainer$p(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)Landroid/widget/LinearLayout;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->paymentTypeContainer:Landroid/widget/LinearLayout;

    if-nez p0, :cond_0

    const-string v0, "paymentTypeContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$setPaymentTypeContainer$p(Lcom/squareup/invoices/ui/AddPaymentCoordinator;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->paymentTypeContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 78
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 79
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->paymentTypeContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final configureActionBar()V
    .locals 6

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 71
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->payment_type:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v3, 0x1

    .line 72
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 73
    new-instance v3, Lcom/squareup/invoices/ui/AddPaymentCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 74
    iget-object v3, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final createSectionView(Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;Landroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;
    .locals 9

    .line 46
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getButtonData()Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;->getActionType()Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/AddPaymentCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 54
    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$3;-><init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 51
    :cond_1
    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$2;-><init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    goto :goto_0

    .line 48
    :cond_2
    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$createSectionView$action$1;-><init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    :goto_0
    move-object v6, v0

    .line 58
    sget-object v1, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->Factory:Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    .line 60
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    .line 61
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getHelperText()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    .line 62
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getButtonData()Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;->getButtonText()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    .line 64
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->getDrawableId()I

    move-result v7

    move-object v8, p2

    .line 58
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;->createView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;ILandroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->bindViews(Landroid/view/View;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->configureActionBar()V

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;->addPaymentScreenData()Lrx/Observable;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;Landroid/view/View;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.addPaymentScreenD\u2026t))\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
