.class public final Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;
.super Ljava/lang/Object;
.source "InvoiceKeypadRunner.kt"

# interfaces
.implements Lcom/squareup/orderentry/KeypadEntryScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$SharedScope;,
        Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001:\u0002\u001e\u001fB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0015H\u0002J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0017J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0016\u0010\u0018\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000cJ\u000e\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010J\u0016\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001cJ\u0008\u0010\u001d\u001a\u00020\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
        "Lcom/squareup/orderentry/KeypadEntryScreen$Runner;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V",
        "events",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;",
        "kotlin.jvm.PlatformType",
        "keypadInfo",
        "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;",
        "workingDiscount",
        "Lcom/squareup/configure/item/WorkingDiscount;",
        "workingItem",
        "Lcom/squareup/configure/item/WorkingItem;",
        "clear",
        "",
        "closeKeypadEntryCard",
        "commit",
        "",
        "discountApplicationIdEnabled",
        "Lio/reactivex/Observable;",
        "setupToEditDiscount",
        "setupToEditItem",
        "setupToEditItemVariablePrice",
        "buttonText",
        "",
        "updateWithKeypadResult",
        "Event",
        "SharedScope",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final events:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

.field private workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

.field private workingItem:Lcom/squareup/configure/item/WorkingItem;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->features:Lcom/squareup/settings/server/Features;

    .line 49
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Event>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method private final clear()V
    .locals 2

    const/4 v0, 0x0

    .line 122
    move-object v1, v0

    check-cast v1, Lcom/squareup/configure/item/WorkingItem;

    iput-object v1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 123
    move-object v1, v0

    check-cast v1, Lcom/squareup/configure/item/WorkingDiscount;

    iput-object v1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 124
    check-cast v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    iput-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-void
.end method

.method private final discountApplicationIdEnabled()Z
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final updateWithKeypadResult()V
    .locals 4

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 98
    iget-object v2, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    if-nez v1, :cond_5

    .line 103
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v1

    if-eqz v1, :cond_4

    sget-object v3, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    if-nez v2, :cond_1

    .line 105
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    .line 104
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, v2, Lcom/squareup/configure/item/WorkingDiscount;->amountMoney:Lcom/squareup/protos/common/Money;

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$DiscountAmountEntered;

    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->discountApplicationIdEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/squareup/configure/item/WorkingDiscount;->finish(Z)Lcom/squareup/checkout/Discount;

    move-result-object v2

    const-string/jumbo v3, "workingDiscount.finish(d\u2026ntApplicationIdEnabled())"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$DiscountAmountEntered;-><init>(Lcom/squareup/checkout/Discount;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    .line 106
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected Keypad Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 112
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    .line 113
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v2, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$ItemPriceEntered;

    invoke-virtual {v1}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    const-string/jumbo v3, "workingItem.finish()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$ItemPriceEntered;-><init>(Lcom/squareup/checkout/CartItem;)V

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 116
    :goto_1
    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->clear()V

    return-void
.end method


# virtual methods
.method public closeKeypadEntryCard(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$CloseKeypad;->INSTANCE:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$CloseKeypad;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->updateWithKeypadResult()V

    :goto_0
    return-void
.end method

.method public final events()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public final setupToEditDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;)V
    .locals 1

    const-string/jumbo v0, "workingDiscount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keypadInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->clear()V

    .line 91
    iput-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 92
    iput-object p2, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-void
.end method

.method public final setupToEditItem(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    const-string/jumbo v0, "workingItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->clear()V

    .line 83
    iput-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method

.method public final setupToEditItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "workingItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->clear()V

    const/4 v0, 0x0

    .line 72
    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 75
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    .line 76
    invoke-virtual {p1}, Lcom/squareup/configure/item/WorkingItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    .line 77
    invoke-static {p1}, Lcom/squareup/configure/item/WorkingItemUtilsKt;->getUnitAbbreviation(Lcom/squareup/configure/item/WorkingItem;)Ljava/lang/String;

    move-result-object p1

    .line 75
    invoke-direct {v0, v1, v2, p2, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-void
.end method
