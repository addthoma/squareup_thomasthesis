.class public final Lcom/squareup/invoices/PaymentRequestsConfig$Companion;
.super Ljava/lang/Object;
.source "PaymentRequestsConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/PaymentRequestsConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestsConfig$Companion;",
        "",
        "()V",
        "fromPaymentRequests",
        "Lcom/squareup/invoices/PaymentRequestsConfig;",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/invoices/PaymentRequestsConfig;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->FULL:Lcom/squareup/invoices/PaymentRequestsConfig;

    goto :goto_0

    .line 29
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isDepositRemainder(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->DEPOSIT_REMAINDER:Lcom/squareup/invoices/PaymentRequestsConfig;

    goto :goto_0

    .line 30
    :cond_1
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isDepositInstallment(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->DEPOSIT_INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

    goto :goto_0

    .line 31
    :cond_2
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isInstallments(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

    goto :goto_0

    .line 32
    :cond_3
    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->INVALID:Lcom/squareup/invoices/PaymentRequestsConfig;

    :goto_0
    return-object p1
.end method
