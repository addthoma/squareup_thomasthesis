.class public final Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;
.super Ljava/lang/Object;
.source "Comparisons.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/PartialTransactionData$Factory;->create(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nComparisons.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Comparisons.kt\nkotlin/comparisons/ComparisonsKt__ComparisonsKt$compareByDescending$1\n+ 2 PartialTransactionData.kt\ncom/squareup/invoices/PartialTransactionData$Factory\n*L\n1#1,319:1\n66#2,5:320\n66#2,5:325\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0007\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u000e\u0010\u0003\u001a\n \u0004*\u0004\u0018\u0001H\u0002H\u00022\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u0001H\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "a",
        "kotlin.jvm.PlatformType",
        "b",
        "compare",
        "(Ljava/lang/Object;Ljava/lang/Object;)I",
        "kotlin/comparisons/ComparisonsKt__ComparisonsKt$compareByDescending$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/PartialTransactionData$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/PartialTransactionData$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;->this$0:Lcom/squareup/invoices/PartialTransactionData$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .line 121
    check-cast p2, Lcom/squareup/wire/Message;

    .line 320
    instance-of v0, p2, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    const-string v1, "null cannot be cast to non-null type com.squareup.protos.client.invoice.InvoiceTenderDetails"

    if-eqz v0, :cond_0

    .line 321
    check-cast p2, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p2}, Lcom/squareup/util/ProtoDates;->tryParseIso8601Date(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object p2

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_3

    .line 323
    check-cast p2, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;->this$0:Lcom/squareup/invoices/PartialTransactionData$Factory;

    invoke-static {v0}, Lcom/squareup/invoices/PartialTransactionData$Factory;->access$getLocale$p(Lcom/squareup/invoices/PartialTransactionData$Factory;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    .line 324
    :goto_0
    check-cast p2, Ljava/lang/Comparable;

    check-cast p1, Lcom/squareup/wire/Message;

    .line 325
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    if-eqz v0, :cond_1

    .line 326
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->tryParseIso8601Date(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object p1

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 328
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;->this$0:Lcom/squareup/invoices/PartialTransactionData$Factory;

    invoke-static {v0}, Lcom/squareup/invoices/PartialTransactionData$Factory;->access$getLocale$p(Lcom/squareup/invoices/PartialTransactionData$Factory;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 329
    :goto_1
    check-cast p1, Ljava/lang/Comparable;

    invoke-static {p2, p1}, Lkotlin/comparisons/ComparisonsKt;->compareValues(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    return p1

    .line 328
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 323
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
