.class public final enum Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;
.super Ljava/lang/Enum;
.source "EditInvoice1ScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0012\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;",
        "",
        "(Ljava/lang/String;I)V",
        "ADD_CUSTOMER_BUTTON_CLICKED",
        "VIEW_CUSTOMER_ROW_CLICKED",
        "ADD_LINE_ITEM_CLICKED",
        "LINE_ITEM_CLICKED",
        "FREQUENCY_ROW_CLICKED",
        "REQUEST_SHIPPING_ADDRESS_TOGGLED",
        "REQUEST_A_TIP_TOGGLED",
        "INVOICE_METHOD_CLICKED",
        "AUTO_PAYMENTS_CLICKED",
        "AUTO_PAYMENTS_TOGGLED",
        "SEND_ROW_CLICKED",
        "DUE_ROW_CLICKED",
        "REQUEST_DEPOSIT_CLICKED",
        "ADD_PAYMENT_SCHEDULE_CLICKED",
        "PAYMENT_REQUEST_CLICKED",
        "EDIT_PAYMENT_SCHEDULE_CLICKED",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum ADD_CUSTOMER_BUTTON_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum ADD_LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum ADD_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum AUTO_PAYMENTS_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum AUTO_PAYMENTS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum DUE_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum INVOICE_METHOD_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum PAYMENT_REQUEST_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum REQUEST_A_TIP_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum REQUEST_DEPOSIT_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum REQUEST_SHIPPING_ADDRESS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum SEND_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

.field public static final enum VIEW_CUSTOMER_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x0

    const-string v3, "ADD_CUSTOMER_BUTTON_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_CUSTOMER_BUTTON_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x1

    const-string v3, "VIEW_CUSTOMER_ROW_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->VIEW_CUSTOMER_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x2

    const-string v3, "ADD_LINE_ITEM_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x3

    const-string v3, "LINE_ITEM_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x4

    const-string v3, "FREQUENCY_ROW_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x5

    const-string v3, "REQUEST_SHIPPING_ADDRESS_TOGGLED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_SHIPPING_ADDRESS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x6

    const-string v3, "REQUEST_A_TIP_TOGGLED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_A_TIP_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v2, 0x7

    const-string v3, "INVOICE_METHOD_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->INVOICE_METHOD_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0x8

    const-string v3, "AUTO_PAYMENTS_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0x9

    const-string v3, "AUTO_PAYMENTS_TOGGLED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xa

    const-string v3, "SEND_ROW_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->SEND_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xb

    const-string v3, "DUE_ROW_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->DUE_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xc

    const-string v3, "REQUEST_DEPOSIT_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_DEPOSIT_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xd

    const-string v3, "ADD_PAYMENT_SCHEDULE_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xe

    const-string v3, "PAYMENT_REQUEST_CLICKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->PAYMENT_REQUEST_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const-string v2, "EDIT_PAYMENT_SCHEDULE_CLICKED"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->$VALUES:[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;
    .locals 1

    const-class v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->$VALUES:[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v0}, [Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    return-object v0
.end method
