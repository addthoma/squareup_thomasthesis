.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1;->apply(Lkotlin/Triple;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a.\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00030\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/tuple/Quartet;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $busy:Ljava/lang/Boolean;

.field final synthetic $invoice:Lcom/squareup/protos/client/invoice/Invoice;

.field final synthetic $rule:Lcom/squareup/util/Optional;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Optional;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$rule:Lcom/squareup/util/Optional;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$busy:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/Optional;)Lcom/squareup/util/tuple/Quartet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;)",
            "Lcom/squareup/util/tuple/Quartet<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    new-instance v0, Lcom/squareup/util/tuple/Quartet;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$rule:Lcom/squareup/util/Optional;

    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->$busy:Ljava/lang/Boolean;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/util/tuple/Quartet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1$1;->apply(Lcom/squareup/util/Optional;)Lcom/squareup/util/tuple/Quartet;

    move-result-object p1

    return-object p1
.end method
