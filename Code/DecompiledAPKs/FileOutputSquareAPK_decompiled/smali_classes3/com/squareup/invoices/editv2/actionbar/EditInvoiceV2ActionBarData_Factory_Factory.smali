.class public final Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceV2ActionBarData_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData_Factory_Factory;->get()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    move-result-object v0

    return-object v0
.end method
