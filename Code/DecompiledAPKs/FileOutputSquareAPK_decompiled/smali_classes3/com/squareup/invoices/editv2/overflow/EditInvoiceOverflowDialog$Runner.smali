.class public interface abstract Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;
.super Ljava/lang/Object;
.source "EditInvoiceOverflowDialog.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "goBackFromOverflowDialog",
        "",
        "overflowDialogScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract goBackFromOverflowDialog()V
.end method

.method public abstract overflowDialogScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
            ">;"
        }
    .end annotation
.end method
