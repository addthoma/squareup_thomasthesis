.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->addPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkingInvoiceEditor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkingInvoiceEditor.kt\ncom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,357:1\n132#2,3:358\n*E\n*S KotlinDebug\n*F\n+ 1 WorkingInvoiceEditor.kt\ncom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1\n*L\n267#1,3:358\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;->invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 8

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "payment_request"

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 264
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v3, 0x0

    .line 266
    iget-object v4, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    aput-object v4, v1, v3

    .line 267
    check-cast v0, Lcom/squareup/wire/Message;

    .line 359
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 268
    iget-object v4, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$Companion()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;

    const-wide/16 v6, 0x1e

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    .line 360
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    aput-object v0, v1, v2

    .line 265
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    goto :goto_0

    .line 359
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 271
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->sorted(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    :goto_0
    return-void
.end method
