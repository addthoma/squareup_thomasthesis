.class final Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;
.super Ljava/lang/Object;
.source "EditInvoice1Coordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;)V
    .locals 5

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getShowOverflowMenuButton()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->$view:Landroid/view/View;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$updateActionBar(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    .line 60
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getUseBigHeader()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$updateHeader(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;ZLjava/lang/CharSequence;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getBottomButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$updateBottomButton(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;->getInvoiceSectionDataList()Ljava/util/List;

    move-result-object p1

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$getSectionsContainer$p(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 66
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$getInvoiceSectionContainerViewFactory$p(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;

    .line 67
    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$getRunner$p(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/EventHandler;

    .line 68
    iget-object v3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;->access$getFeatures$p(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;)Lcom/squareup/settings/server/Features;

    move-result-object v3

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    .line 64
    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt;->setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;Lcom/squareup/features/invoices/widgets/EventHandler;Z)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator$attach$3;->accept(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;)V

    return-void
.end method
