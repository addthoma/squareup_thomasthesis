.class public final Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;
.super Ljava/lang/Object;
.source "EditInvoice2ScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoice2ScreenData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoice2ScreenData.kt\ncom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory\n*L\n1#1,446:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u00002\u00020\u0001BI\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010\u0015\u001a\u00020\u0014H\u0002J \u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\u0016\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\u0016\u0010\u001f\u001a\u00020 2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\"0\u001bH\u0002J\u0018\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020&H\u0002J\"\u0010(\u001a\u0004\u0018\u00010\u00172\u0006\u0010%\u001a\u00020&2\u0006\u0010)\u001a\u00020&2\u0006\u0010\'\u001a\u00020&H\u0002J*\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010%\u001a\u00020&2\u0006\u0010,\u001a\u00020&2\u0006\u0010)\u001a\u00020&2\u0006\u0010\'\u001a\u00020&H\u0002J*\u0010-\u001a\u00020 2\u0006\u0010.\u001a\u00020/2\u0006\u0010%\u001a\u00020&2\u0008\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u000203H\u0002J0\u00104\u001a\u0002052\u0006\u0010.\u001a\u00020/2\u0006\u0010%\u001a\u00020&2\u0006\u00102\u001a\u0002032\u0006\u00106\u001a\u00020&2\u0008\u00100\u001a\u0004\u0018\u000101J\u0010\u00107\u001a\u00020\u001e2\u0006\u00108\u001a\u00020/H\u0002J\"\u00109\u001a\u00020:2\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u000203H\u0002J\u0018\u0010;\u001a\u00020 2\u0006\u0010%\u001a\u00020&2\u0006\u0010<\u001a\u00020\u001cH\u0002J\u001a\u0010=\u001a\u0004\u0018\u00010:2\u0006\u0010.\u001a\u00020/2\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010=\u001a\u00020:2\u0006\u0010>\u001a\u00020?H\u0002J,\u0010@\u001a\u0004\u0018\u00010:2\u0006\u00102\u001a\u0002032\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010%\u001a\u00020&2\u0008\u0010A\u001a\u0004\u0018\u00010BH\u0002J \u0010C\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u00102\u001a\u000203H\u0002J\u0010\u0010D\u001a\u00020&2\u0006\u00102\u001a\u000203H\u0002J \u0010E\u001a\u00020&2\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u00102\u001a\u000203H\u0002J\u0010\u0010F\u001a\u0004\u0018\u00010 *\u0004\u0018\u00010 H\u0002J\u0010\u0010F\u001a\u0004\u0018\u00010\u0017*\u0004\u0018\u00010\u0017H\u0002J\u000c\u0010G\u001a\u00020&*\u000203H\u0002J\u000e\u0010H\u001a\u00020&*\u0004\u0018\u00010BH\u0002J\u0014\u0010I\u001a\n J*\u0004\u0018\u00010\u001c0\u001c*\u00020BH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006K"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "actionBarDataFactory",
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
        "invoiceUrlHelper",
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "bottomButtonTextFactory",
        "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
        "buyerCofLearnMoreMessageFactory",
        "Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "(Lcom/squareup/util/Res;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;Lcom/squareup/util/Clock;Ljava/text/DateFormat;)V",
        "addAdditionalRecipientsButton",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "addAttachmentButton",
        "additionalRecipients",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "paymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "recipients",
        "",
        "",
        "additionalRecipientsRow",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "attachmentSection",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "attachments",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "buyerCofHelperText",
        "",
        "isRecurring",
        "",
        "enabledByDefault",
        "buyerCofHelperTextRow",
        "shouldShowBuyerCofRow",
        "buyerCofRow",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "buyerCofEnabled",
        "communicationSection",
        "workingInvoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "instrumentSummary",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "create",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        "isBusy",
        "invoiceDetailsRow",
        "invoice",
        "methodRow",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "previewSection",
        "serverToken",
        "reminderRow",
        "reminderCount",
        "",
        "sendRow",
        "scheduledAt",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "sendRowLabel",
        "shouldDisableMethodRow",
        "shouldHideSendRow",
        "hideIfV2",
        "isEditingScheduledInvoice",
        "isToday",
        "toDateString",
        "kotlin.jvm.PlatformType",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

.field private final bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

.field private final buyerCofLearnMoreMessageFactory:Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;

.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;Lcom/squareup/util/Clock;Ljava/text/DateFormat;)V
    .locals 1
    .param p8    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarDataFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUrlHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomButtonTextFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCofLearnMoreMessageFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    iput-object p4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    iput-object p6, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofLearnMoreMessageFactory:Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;

    iput-object p7, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    iput-object p8, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method private final addAdditionalRecipientsButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 4

    .line 312
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 313
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->add_additional_recipients:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 314
    sget-object v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADDITIONAL_RECIPIENTS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 315
    sget v3, Lcom/squareup/features/invoices/R$drawable;->add_recipient_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 312
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final addAttachmentButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 4

    .line 330
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 331
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->add_attachment:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADD_ATTACHMENT_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 332
    sget v3, Lcom/squareup/features/invoices/widgets/R$drawable;->add_attachment_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 330
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final additionalRecipients(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/SectionElement;"
        }
    .end annotation

    .line 305
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 306
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->addAdditionalRecipientsButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_0

    .line 307
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->additionalRecipientsRow(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    :goto_0
    return-object p1
.end method

.method private final additionalRecipientsRow(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;"
        }
    .end annotation

    .line 322
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    .line 323
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->additional_recipients:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 324
    move-object v2, p1

    check-cast v2, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 325
    sget-object v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADDITIONAL_RECIPIENTS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 322
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    return-object v0
.end method

.method private final attachmentSection(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;"
        }
    .end annotation

    .line 175
    new-instance v7, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 176
    new-instance v0, Lcom/squareup/features/invoices/widgets/Header$Show;

    .line 177
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->uppercase_file_attachments:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 180
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;

    sget-object v3, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ATTACHMENT_ROW_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-direct {v2, p1, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;-><init>(Ljava/util/List;Ljava/lang/Object;)V

    check-cast v2, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 p1, 0x0

    aput-object v2, v0, p1

    invoke-direct {p0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->addAttachmentButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v2, 0x1

    aput-object p1, v0, v2

    .line 179
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    .line 175
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final buyerCofHelperText(ZZ)Ljava/lang/CharSequence;
    .locals 0

    if-eqz p1, :cond_0

    .line 296
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofLearnMoreMessageFactory:Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;->getLearnMoreMessage(Z)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 297
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_save_cof_helper:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    :goto_0
    return-object p1
.end method

.method private final buyerCofHelperTextRow(ZZZ)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 0

    if-eqz p2, :cond_0

    .line 288
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;

    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofHelperText(ZZ)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;-><init>(Ljava/lang/CharSequence;)V

    check-cast p2, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private final buyerCofRow(ZZZZ)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
    .locals 7

    if-eqz p3, :cond_1

    if-eqz p4, :cond_0

    .line 269
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    .line 270
    iget-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_allow_buyer_save_cof:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 271
    invoke-direct {p0, p1, p4}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofHelperText(ZZ)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 269
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 274
    :cond_0
    new-instance p3, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    .line 275
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_allow_buyer_save_cof:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 276
    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->BUYER_COF_TOGGLED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-direct {p0, p1, p4}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofHelperText(ZZ)Ljava/lang/CharSequence;

    move-result-object p1

    .line 274
    invoke-direct {p3, v0, p2, v1, p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V

    move-object p2, p3

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private final communicationSection(Lcom/squareup/protos/client/invoice/Invoice;ZLcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p4

    .line 121
    invoke-direct/range {p0 .. p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->invoiceDetailsRow(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    move-result-object v4

    .line 123
    invoke-direct/range {p0 .. p2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->reminderRow(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v5

    .line 127
    iget-object v6, v1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    const-string/jumbo v7, "workingInvoice.buyer_entered_instrument_enabled"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 128
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v7

    sget-object v8, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-eq v7, v8, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 129
    :goto_0
    iget-object v8, v1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    const-string/jumbo v11, "workingInvoice.buyer_ent\u2026tic_charge_enroll_enabled"

    invoke-static {v8, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 125
    invoke-direct {v0, v2, v6, v7, v8}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofRow(ZZZZ)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    move-result-object v6

    .line 132
    iget-object v7, v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v7, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_1

    move-object v7, v8

    goto :goto_2

    .line 137
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v7

    sget-object v12, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq v7, v12, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    .line 138
    :goto_1
    iget-object v12, v1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    invoke-static {v12, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 135
    invoke-direct {v0, v2, v7, v11}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->buyerCofHelperTextRow(ZZZ)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v7

    .line 143
    :goto_2
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v11

    iget-object v12, v1, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    const-string/jumbo v13, "workingInvoice.additional_recipient_email"

    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-direct {v0, v11, v12}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->additionalRecipients(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v11

    .line 145
    iget-object v12, v1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    if-eqz v12, :cond_3

    .line 146
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->addAttachmentButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object v8

    .line 150
    :cond_3
    iget-object v12, v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v12, v13}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v12

    const/4 v13, 0x4

    const/4 v14, 0x3

    const/4 v15, 0x2

    if-eqz v12, :cond_4

    new-array v4, v13, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 152
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v7

    move-object/from16 v8, p3

    invoke-direct {v0, v7, v8, v3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->methodRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v7

    check-cast v7, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v7, v4, v10

    .line 153
    check-cast v6, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v6, v4, v9

    .line 154
    invoke-static/range {p1 .. p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v6

    .line 155
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 154
    invoke-direct {v0, v3, v6, v2, v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->sendRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v1, v4, v15

    .line 156
    check-cast v5, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v5, v4, v14

    .line 151
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x6

    new-array v1, v1, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 160
    check-cast v4, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v4, v1, v10

    .line 161
    check-cast v5, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v5, v1, v9

    .line 162
    check-cast v6, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v6, v1, v15

    aput-object v7, v1, v14

    aput-object v11, v1, v13

    const/4 v2, 0x5

    .line 165
    check-cast v8, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v8, v1, v2

    .line 159
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_3
    move-object v4, v1

    .line 168
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 169
    new-instance v2, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object v3, v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/features/invoices/R$string;->uppercase_communication:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v3, v2

    check-cast v3, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-object v2, v1

    .line 168
    invoke-direct/range {v2 .. v8}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v1
.end method

.method private final hideIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 2

    .line 430
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method private final hideIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method private final invoiceDetailsRow(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;
    .locals 5

    .line 203
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    .line 205
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 206
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->no_invoice_title:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 208
    :cond_2
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    .line 212
    :goto_2
    iget-object v4, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_3

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    const-string p1, ""

    goto :goto_3

    .line 215
    :cond_5
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    .line 218
    :goto_3
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    .line 219
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " (#"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "invoiceDescription"

    .line 220
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 221
    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->EDIT_INVOICE_DETAILS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 218
    invoke-direct {v2, v0, p1, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    return-object v2
.end method

.method private final isEditingScheduledInvoice(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 438
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p1

    return p1
.end method

.method private final methodRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 3

    .line 406
    invoke-direct {p0, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->shouldDisableMethodRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p3

    .line 407
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 408
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_method:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 409
    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v2, p2}, Lcom/squareup/invoices/edit/PaymentMethodUtility;->getPaymentMethodString(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/util/Res;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Ljava/lang/String;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 410
    sget-object p2, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->INVOICE_METHOD_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    :goto_0
    sget p3, Lcom/squareup/features/invoices/R$drawable;->share_row_icon:I

    .line 407
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object v0
.end method

.method private final previewSection(ZLjava/lang/String;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 7

    if-eqz p1, :cond_0

    .line 189
    sget-object p1, Lcom/squareup/url/UrlType;->RECURRING_SERIES:Lcom/squareup/url/UrlType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/invoices/InvoiceUrlHelper;->invoiceUrl(Ljava/lang/String;Lcom/squareup/url/UrlType;)Ljava/lang/String;

    move-result-object p1

    .line 193
    new-instance p2, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 194
    new-instance v0, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->uppercase_preview:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header;

    .line 196
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/preview"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;-><init>(Ljava/lang/String;)V

    .line 195
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p2

    .line 193
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method

.method private final reminderRow(I)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 4

    .line 251
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 252
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez p1, :cond_0

    .line 254
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_off:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 255
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    .line 256
    :goto_0
    sget-object v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->REMINDERS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 257
    sget v3, Lcom/squareup/features/invoices/R$drawable;->reminders_row_icon:I

    .line 251
    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object v0
.end method

.method private final reminderRow(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const-string v1, "automatic_reminder_config"

    if-eqz v0, :cond_2

    if-eqz p2, :cond_0

    .line 233
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->reminderRow(I)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object p1

    goto :goto_0

    .line 239
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v0, "payment_request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 240
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string p2, "payment_request.first().reminders"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    .line 239
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->reminderRow(I)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 245
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->reminderRow(I)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final sendRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 2

    .line 342
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->shouldHideSendRow(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 346
    :cond_0
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->sendRowLabel(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/lang/String;

    move-result-object p2

    .line 352
    invoke-direct {p0, p4}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->isEditingScheduledInvoice(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 353
    sget p3, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    .line 352
    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    .line 355
    invoke-direct {p0, p4}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->toDateString(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v1

    .line 358
    :cond_2
    :goto_0
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    sget-object p3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->SEND_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget p4, Lcom/squareup/features/invoices/R$drawable;->send_row_icon:I

    invoke-direct {p1, p2, v1, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object p1
.end method

.method private final sendRowLabel(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_1

    .line 381
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSeries()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 382
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_next_invoice:I

    goto :goto_0

    .line 384
    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_start:I

    goto :goto_0

    .line 389
    :cond_1
    sget-object p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_3

    const/4 p2, 0x2

    if-eq p1, p2, :cond_2

    .line 394
    sget p1, Lcom/squareup/utilities/R$string;->send:I

    goto :goto_0

    .line 391
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Send row should be hidden for Share link."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 390
    :cond_3
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof_action:I

    .line 377
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final shouldDisableMethodRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 417
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final shouldHideSendRow(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 0

    if-nez p1, :cond_0

    .line 368
    sget-object p1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p2, p1, :cond_2

    .line 367
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 368
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    sget-object p2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, p2, :cond_3

    :cond_2
    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final toDateString(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 1

    .line 443
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/Invoice;ZLcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;
    .locals 9

    const-string/jumbo v0, "workingInvoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-direct {p0, p1, p2, p5, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->communicationSection(Lcom/squareup/protos/client/invoice/Invoice;ZLcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p5

    .line 91
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_1

    .line 92
    :cond_1
    :goto_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string/jumbo v2, "workingInvoice.id_pair.server_id"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->previewSection(ZLjava/lang/String;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object v0

    .line 94
    :goto_1
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 95
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-direct {p0, v2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->attachmentSection(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object v1

    .line 98
    :cond_3
    :goto_2
    new-instance v8, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    .line 99
    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    .line 100
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 99
    invoke-virtual {v2, v3, p3, p2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;->create(Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v3

    .line 102
    iget-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    const/4 v2, 0x0

    invoke-virtual {p2, p1, p3, v2}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->create(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result p1

    const/4 p2, 0x1

    if-nez p1, :cond_5

    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v6, 0x1

    :goto_4
    const/4 p1, 0x3

    new-array p1, p1, [Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    aput-object p5, p1, v2

    .line 108
    invoke-direct {p0, v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;->hideIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p3

    aput-object p3, p1, p2

    const/4 p2, 0x2

    aput-object v0, p1, p2

    .line 106
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 110
    invoke-static {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionDataKt;->addDividers(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v2, v8

    move v7, p4

    .line 98
    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;-><init>(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-object v8
.end method
