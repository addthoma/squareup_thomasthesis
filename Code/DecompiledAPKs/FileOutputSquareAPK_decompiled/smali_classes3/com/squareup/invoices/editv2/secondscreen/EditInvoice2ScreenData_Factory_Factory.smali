.class public final Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;
.super Ljava/lang/Object;
.source "EditInvoice2ScreenData_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final bottomButtonTextFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCofLearnMoreMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->actionBarDataFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->bottomButtonTextFactoryProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->buyerCofLearnMoreMessageFactoryProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;Lcom/squareup/util/Clock;Ljava/text/DateFormat;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;
    .locals 10

    .line 75
    new-instance v9, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;Lcom/squareup/util/Clock;Ljava/text/DateFormat;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->actionBarDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->bottomButtonTextFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->buyerCofLearnMoreMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/text/DateFormat;

    invoke-static/range {v1 .. v8}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Lcom/squareup/invoices/edit/BuyerCofLearnMoreMessageFactory;Lcom/squareup/util/Clock;Ljava/text/DateFormat;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData_Factory_Factory;->get()Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method
