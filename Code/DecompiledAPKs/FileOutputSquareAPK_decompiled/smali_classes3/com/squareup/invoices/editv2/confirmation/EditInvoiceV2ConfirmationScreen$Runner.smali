.class public interface abstract Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u0006H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
        "",
        "confirmationScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        "copyLink",
        "",
        "goBackFromConfirmationScreen",
        "success",
        "",
        "moreOptionsPressed",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract confirmationScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract copyLink()V
.end method

.method public abstract goBackFromConfirmationScreen(Z)V
.end method

.method public abstract moreOptionsPressed()V
.end method
