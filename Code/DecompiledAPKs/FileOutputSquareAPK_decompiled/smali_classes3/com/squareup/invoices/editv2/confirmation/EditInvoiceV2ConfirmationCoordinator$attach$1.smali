.class final Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfirmationCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$getGlyphMessageView$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v1

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$set(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Lcom/squareup/marin/widgets/MarinGlyphMessage;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getActionBarHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$getRunner$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getSuccess()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$updateActionBar(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Ljava/lang/String;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Z)V

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1$1;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getDisplayShareLinkButtons()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$setShareLinkSection(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Z)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;->accept(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V

    return-void
.end method
