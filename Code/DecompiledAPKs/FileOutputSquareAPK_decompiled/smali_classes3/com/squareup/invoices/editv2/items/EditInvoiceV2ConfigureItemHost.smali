.class public final Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfigureItemHost.kt"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemHost;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J.\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016JH\u0010\u0014\u001aB\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00160\u0016\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00180\u0018 \u0017* \u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00160\u0016\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00180\u0018\u0018\u00010\u00190\u0015H\u0016J,\u0010\u001a\u001a&\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001c0\u001c \u0017*\u0012\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001c0\u001c\u0018\u00010\u001d0\u001bH\u0016J,\u0010\u001e\u001a&\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001f0\u001f \u0017*\u0012\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001f0\u001f\u0018\u00010\u001d0\u001bH\u0016J\u0010\u0010 \u001a\n \u0017*\u0004\u0018\u00010!0!H\u0016J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020!0\u001dH\u0016J\u0010\u0010#\u001a\n \u0017*\u0004\u0018\u00010$0$H\u0016J\u0018\u0010%\u001a\n \u0017*\u0004\u0018\u00010\n0\n2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010&\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\'\u001a\u00020(H\u0016J\u0008\u0010)\u001a\u00020(H\u0016J\u0010\u0010*\u001a\u00020(2\u0006\u0010+\u001a\u00020(H\u0016J\u001c\u0010,\u001a\u00020\u00082\u0008\u0010-\u001a\u0004\u0018\u00010.2\u0008\u0010/\u001a\u0004\u0018\u000100H\u0016J\u001a\u00101\u001a\u00020(2\u0008\u0010/\u001a\u0004\u0018\u0001002\u0006\u0010+\u001a\u00020(H\u0016J\u0010\u00102\u001a\u00020(2\u0006\u00103\u001a\u000200H\u0016J\u0008\u00104\u001a\u00020\u0008H\u0016J\u001c\u00105\u001a\u00020\u00082\u0008\u0010-\u001a\u0004\u0018\u00010.2\u0008\u0010/\u001a\u0004\u0018\u000100H\u0016J\u0008\u00106\u001a\u00020\u0008H\u0016J\u0008\u00107\u001a\u00020\u0008H\u0016J\u0010\u00108\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u00109\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\nH\u0016J\u0010\u0010:\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J$\u0010;\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010<\u001a\u0004\u0018\u00010\u00162\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;",
        "Lcom/squareup/configure/item/ConfigureItemHost;",
        "scopeRunner",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
        "orderEditor",
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/order/WorkingOrderEditor;)V",
        "commitCartItem",
        "",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "compItem",
        "",
        "indexToConfigure",
        "",
        "catalogDiscount",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "orderItem",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "getAddedCouponsAndCartScopeDiscounts",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/checkout/Discount;",
        "",
        "getAvailableTaxRules",
        "",
        "Lcom/squareup/payment/OrderTaxRule;",
        "",
        "getAvailableTaxes",
        "Lcom/squareup/checkout/Tax;",
        "getCurrentDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "getDiningOptions",
        "getGiftCardTotal",
        "Lcom/squareup/protos/common/Money;",
        "getOrderItem",
        "getOrderItemCopy",
        "hasTicket",
        "",
        "isVoidCompAllowed",
        "onCancelSelected",
        "isWorkingItem",
        "onCommit",
        "viewName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "state",
        "Lcom/squareup/configure/item/ConfigureItemState;",
        "onCommitSuccess",
        "onDeleteSelected",
        "configureItemState",
        "onSelectedVariationClicked",
        "onStartVisualTransition",
        "onVariablePriceButtonClicked",
        "onVariationCheckChanged",
        "removeItem",
        "replaceItem",
        "uncompItem",
        "voidItem",
        "reason",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

.field private final scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/order/WorkingOrderEditor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEditor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    return-void
.end method


# virtual methods
.method public commitCartItem(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    const-string v0, "cartItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    return-void
.end method

.method public compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Ljava/lang/Void;
    .locals 0

    .line 96
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Invoices should not comp items."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 14
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Ljava/lang/Void;

    return-void
.end method

.method public getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxRules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxesAsList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getDiningOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 22
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getGiftCardTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getOrderItem(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    return-object p1
.end method

.method public getOrderItemCopy(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 83
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 84
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    const-string v0, "getOrderItem(indexToConf\u2026ldUpon()\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public hasTicket()Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasTicket()Z

    move-result v0

    return v0
.end method

.method public isVoidCompAllowed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCancelSelected(Z)Z
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->cancelConfigureItemCard(Z)V

    const/4 p1, 0x1

    return p1
.end method

.method public onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 0

    return-void
.end method

.method public onCommitSuccess(Lcom/squareup/configure/item/ConfigureItemState;Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDeleteSelected(Lcom/squareup/configure/item/ConfigureItemState;)Z
    .locals 1

    const-string v0, "configureItemState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->delete()V

    const/4 p1, 0x1

    return p1
.end method

.method public onSelectedVariationClicked()V
    .locals 0

    return-void
.end method

.method public onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 0

    return-void
.end method

.method public onVariablePriceButtonClicked()V
    .locals 0

    return-void
.end method

.method public onVariationCheckChanged()V
    .locals 0

    return-void
.end method

.method public removeItem(I)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->removeItemFromCart(I)V

    .line 48
    iget-object p1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    return-void
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)V
    .locals 1

    const-string v0, "orderItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/order/WorkingOrderEditor;->replaceItemInCart(ILcom/squareup/checkout/CartItem;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    return-void
.end method

.method public uncompItem(I)Ljava/lang/Void;
    .locals 1

    .line 99
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Invoices should not uncomp items."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic uncompItem(I)V
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->uncompItem(I)Ljava/lang/Void;

    return-void
.end method

.method public voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)Ljava/lang/Void;
    .locals 0

    .line 105
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Invoices should not void items."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 14
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;->voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)Ljava/lang/Void;

    return-void
.end method
