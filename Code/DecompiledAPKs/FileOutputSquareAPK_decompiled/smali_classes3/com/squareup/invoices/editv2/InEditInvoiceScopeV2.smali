.class public abstract Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "EditInvoiceScopeV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0003R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "editInvoiceScopeV2",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V",
        "getEditInvoiceScopeV2",
        "()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "getParentKey",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V
    .locals 1

    const-string v0, "editInvoiceScopeV2"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    return-void
.end method


# virtual methods
.method public final getEditInvoiceScopeV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    return-object v0
.end method

.method public final getParentKey()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->getParentKey()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v0

    return-object v0
.end method
