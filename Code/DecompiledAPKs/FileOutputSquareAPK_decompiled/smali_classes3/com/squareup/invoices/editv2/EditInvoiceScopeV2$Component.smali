.class public interface abstract Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;
.super Ljava/lang/Object;
.source "EditInvoiceScopeV2.kt"

# interfaces
.implements Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;
.implements Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$ParentComponent;
.implements Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;
.implements Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/edit/EditCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;
.implements Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;
.implements Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ParentComponent;
.implements Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;
.implements Lcom/squareup/orderentry/KeypadEntryScreen$ParentComponent;
.implements Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;
.implements Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation runtime Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/invoices/order/WorkingOrderEditor$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$EditInvoiceScopeV2Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u00072\u00020\u00082\u00020\t2\u00020\n2\u00020\u000b2\u00020\u000c2\u00020\rJ\u0008\u0010\u000e\u001a\u00020\u000fH&J\u0008\u0010\u0010\u001a\u00020\u0011H&J\u0008\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$ParentComponent;",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;",
        "Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;",
        "Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;",
        "Lcom/squareup/ui/crm/edit/EditCustomerScope$ParentComponent;",
        "Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;",
        "Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ParentComponent;",
        "Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;",
        "Lcom/squareup/orderentry/KeypadEntryScreen$ParentComponent;",
        "Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;",
        "editInvoiceScopeRunnerV2",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
        "workingInvoiceEditor",
        "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
        "workingOrderEditor",
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract editInvoiceScopeRunnerV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;
.end method

.method public abstract workingInvoiceEditor()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;
.end method

.method public abstract workingOrderEditor()Lcom/squareup/invoices/order/WorkingOrderEditor;
.end method
