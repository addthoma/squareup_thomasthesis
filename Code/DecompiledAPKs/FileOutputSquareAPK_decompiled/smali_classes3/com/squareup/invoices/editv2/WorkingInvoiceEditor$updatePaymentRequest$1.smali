.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updatePaymentRequest(ILcom/squareup/protos/client/invoice/PaymentRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $index:I

.field final synthetic $paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    iput p2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;->$index:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;->invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 3

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    iget v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;->$index:I

    iget-object v2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;->$paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string v1, "payment_request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->sorted(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    return-void
.end method
