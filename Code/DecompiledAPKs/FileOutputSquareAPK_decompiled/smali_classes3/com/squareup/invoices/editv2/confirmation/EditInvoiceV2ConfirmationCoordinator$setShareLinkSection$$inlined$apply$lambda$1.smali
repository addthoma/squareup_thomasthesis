.class public final Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->setShareLinkSection(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 EditInvoiceV2ConfirmationCoordinator.kt\ncom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator\n*L\n1#1,1322:1\n59#2,6:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $visible$inlined:Z

.field final synthetic this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    iput-boolean p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;->$visible$inlined:Z

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 1323
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$getRunner$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;->copyLink()V

    .line 1324
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;->this$0:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->access$getHudToaster$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/hudtoaster/HudToaster;

    move-result-object v0

    .line 1325
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 1326
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_link_copied:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, ""

    check-cast v2, Ljava/lang/CharSequence;

    .line 1324
    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method
