.class final Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;
.super Ljava/lang/Object;
.source "RealInvoiceUnitCache.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/RealInvoiceUnitCache;->forceGetSettings()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;->this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;)",
            "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;"
        }
    .end annotation

    .line 157
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;->this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;

    invoke-static {v0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->access$getFailureMessageFactory$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    .line 159
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_update_settings_failure_title:I

    .line 160
    sget-object v2, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3$failureMessage$1;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 158
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 161
    new-instance v0, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Failed;

    invoke-direct {v0, p1}, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Failed;-><init>(Lcom/squareup/receiving/FailureMessage;)V

    check-cast v0, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;

    goto :goto_0

    .line 163
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Success;->INSTANCE:Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Success;

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;

    move-result-object p1

    return-object p1
.end method
