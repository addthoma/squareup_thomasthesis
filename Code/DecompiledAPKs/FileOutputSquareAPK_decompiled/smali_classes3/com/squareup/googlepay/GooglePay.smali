.class public interface abstract Lcom/squareup/googlepay/GooglePay;
.super Ljava/lang/Object;
.source "GooglePay.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/googlepay/GooglePay$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008H&J0\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/googlepay/GooglePay;",
        "",
        "activeWalletId",
        "Lio/reactivex/Single;",
        "",
        "createOnFailure",
        "",
        "onConnected",
        "Lio/reactivex/Observable;",
        "pushTokenize",
        "Lcom/squareup/googlepay/GooglePayResponse;",
        "opaqueCard",
        "",
        "lastFour",
        "displayName",
        "address",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "stableHardwareId",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract activeWalletId(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onConnected()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract pushTokenize([BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/googlepay/GooglePayAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/googlepay/GooglePayResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract stableHardwareId()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
