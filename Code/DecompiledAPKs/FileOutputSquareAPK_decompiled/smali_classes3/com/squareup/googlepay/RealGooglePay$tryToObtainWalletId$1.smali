.class final Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay;->tryToObtainWalletId()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealGooglePay.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealGooglePay.kt\ncom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1\n*L\n1#1,237:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/googlepay/RealGooglePay;


# direct methods
.method constructor <init>(Lcom/squareup/googlepay/RealGooglePay;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/google/android/gms/tapandpay/TapAndPay;->TapAndPay:Lcom/google/android/gms/tapandpay/TapAndPay;

    .line 183
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    invoke-static {v1}, Lcom/squareup/googlepay/RealGooglePay;->access$getApiClient$p(Lcom/squareup/googlepay/RealGooglePay;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/tapandpay/TapAndPay;->getActiveWalletId(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 184
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$1$1;-><init>(Lcom/google/android/gms/common/api/PendingResult;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 185
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;

    invoke-direct {v1, p1}, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lcom/google/android/gms/common/api/ResultCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    return-void
.end method
