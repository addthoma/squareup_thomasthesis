.class final Lcom/squareup/googlepay/RealGooglePay$createWallet$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealGooglePay.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay;->createWallet(Landroid/app/Activity;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/googlepay/RealGooglePay$createWallet$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$createWallet$1;

    invoke-direct {v0}, Lcom/squareup/googlepay/RealGooglePay$createWallet$1;-><init>()V

    sput-object v0, Lcom/squareup/googlepay/RealGooglePay$createWallet$1;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$createWallet$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/ui/ActivityResultHandler$IntentResult;

    invoke-virtual {p0, p1}, Lcom/squareup/googlepay/RealGooglePay$createWallet$1;->invoke(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Z
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getRequestCode()I

    move-result v0

    invoke-static {}, Lcom/squareup/googlepay/RealGooglePay;->access$Companion()Lcom/squareup/googlepay/RealGooglePay$Companion;

    const/4 v1, 0x1

    const/16 v2, 0x12d

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getResultCode()I

    move-result p1

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
