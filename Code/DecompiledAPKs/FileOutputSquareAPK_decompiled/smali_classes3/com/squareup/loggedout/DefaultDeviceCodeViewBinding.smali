.class public final Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding;
.super Ljava/lang/Object;
.source "DefaultDeviceCodeViewBinding.kt"

# interfaces
.implements Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R2\u0010\u0003\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00060\u0005\u0012\u0004\u0012\u00020\t0\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding;",
        "Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;",
        "()V",
        "coordinatorFactory",
        "Lkotlin/Function1;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "Lcom/squareup/coordinators/Coordinator;",
        "getCoordinatorFactory",
        "()Lkotlin/jvm/functions/Function1;",
        "layoutId",
        "",
        "getLayoutId",
        "()I",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCoordinatorFactory()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;"
        }
    .end annotation

    .line 20
    sget-object v0, Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding$coordinatorFactory$1;->INSTANCE:Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding$coordinatorFactory$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 18
    sget v0, Lcom/squareup/common/authenticatorviews/R$layout;->device_code_login_view:I

    return v0
.end method
