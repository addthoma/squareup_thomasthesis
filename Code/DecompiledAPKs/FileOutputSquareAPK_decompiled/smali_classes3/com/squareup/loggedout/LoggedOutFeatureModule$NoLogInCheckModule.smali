.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule$NoLogInCheckModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "NoLogInCheckModule"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;


# direct methods
.method public constructor <init>(Lcom/squareup/loggedout/LoggedOutFeatureModule;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutFeatureModule$NoLogInCheckModule;->this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindsCreateAccountFinalLogInCheck(Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;)Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsFinalLogInCheck(Lcom/squareup/ui/loggedout/NoFinalLogInCheck;)Lcom/squareup/ui/loggedout/FinalLogInCheck;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
