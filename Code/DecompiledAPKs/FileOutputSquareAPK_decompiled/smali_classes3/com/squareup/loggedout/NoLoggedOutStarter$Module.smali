.class public abstract Lcom/squareup/loggedout/NoLoggedOutStarter$Module;
.super Ljava/lang/Object;
.source "NoLoggedOutStarter.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/NoLoggedOutStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideLoggedOutStarter(Lcom/squareup/loggedout/NoLoggedOutStarter;)Lcom/squareup/loggedout/LoggedOutStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
