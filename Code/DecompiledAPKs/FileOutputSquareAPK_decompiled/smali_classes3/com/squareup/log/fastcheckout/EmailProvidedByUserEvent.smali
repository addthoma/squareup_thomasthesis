.class public Lcom/squareup/log/fastcheckout/EmailProvidedByUserEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "EmailProvidedByUserEvent.java"


# instance fields
.field private final _payment_token:Ljava/lang/String;

.field private final transaction_amount:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_RECEIPT_VIEW_SELECT_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 13
    iput-wide p1, p0, Lcom/squareup/log/fastcheckout/EmailProvidedByUserEvent;->transaction_amount:J

    .line 14
    iput-object p3, p0, Lcom/squareup/log/fastcheckout/EmailProvidedByUserEvent;->_payment_token:Ljava/lang/String;

    return-void
.end method
