.class public Lcom/squareup/log/fastcheckout/SkipSigToggleEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SkipSigToggleEvent.java"


# instance fields
.field private final signature_toggle_enabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 12
    iput-boolean p1, p0, Lcom/squareup/log/fastcheckout/SkipSigToggleEvent;->signature_toggle_enabled:Z

    return-void
.end method
