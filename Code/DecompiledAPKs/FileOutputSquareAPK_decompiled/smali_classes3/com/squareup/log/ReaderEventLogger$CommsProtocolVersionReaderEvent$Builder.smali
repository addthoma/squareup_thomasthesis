.class public Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public commsVersionResult:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

.field public readerProtocolVersion:Ljava/lang/String;

.field public registerProtocolVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1380
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    const/4 v0, 0x0

    .line 1381
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->readerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    .line 1382
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->registerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    .line 1383
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->commsVersionResult(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    .line 1384
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->sessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1375
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;
    .locals 1

    .line 1403
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;)V

    return-object v0
.end method

.method public commsVersionResult(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;
    .locals 0

    .line 1398
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->commsVersionResult:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    return-object p0
.end method

.method public readerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;
    .locals 0

    .line 1388
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->readerProtocolVersion:Ljava/lang/String;

    return-object p0
.end method

.method public registerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;
    .locals 0

    .line 1393
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->registerProtocolVersion:Ljava/lang/String;

    return-object p0
.end method
