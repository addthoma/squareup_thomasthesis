.class public Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private uxHintString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1284
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    .line 1285
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_REVOKED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    const/4 v0, 0x0

    .line 1286
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->title(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    .line 1287
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->description(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    .line 1288
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->uxHintString(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 1279
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 1279
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 1279
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->uxHintString:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1279
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;
    .locals 1

    .line 1307
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)V

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;
    .locals 0

    .line 1297
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;
    .locals 0

    .line 1292
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public uxHintString(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;
    .locals 0

    .line 1302
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->uxHintString:Ljava/lang/String;

    return-object p0
.end method
