.class public final Lcom/squareup/log/OhSnapBusBoy_Factory;
.super Ljava/lang/Object;
.source "OhSnapBusBoy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/OhSnapBusBoy;",
        ">;"
    }
.end annotation


# instance fields
.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/log/OhSnapBusBoy_Factory;->loggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/log/OhSnapBusBoy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)",
            "Lcom/squareup/log/OhSnapBusBoy_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/log/OhSnapBusBoy_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/log/OhSnapBusBoy_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/log/OhSnapBusBoy;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/log/OhSnapBusBoy;

    invoke-direct {v0, p0}, Lcom/squareup/log/OhSnapBusBoy;-><init>(Lcom/squareup/log/OhSnapLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/OhSnapBusBoy;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/log/OhSnapBusBoy_Factory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {v0}, Lcom/squareup/log/OhSnapBusBoy_Factory;->newInstance(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/log/OhSnapBusBoy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/log/OhSnapBusBoy_Factory;->get()Lcom/squareup/log/OhSnapBusBoy;

    move-result-object v0

    return-object v0
.end method
