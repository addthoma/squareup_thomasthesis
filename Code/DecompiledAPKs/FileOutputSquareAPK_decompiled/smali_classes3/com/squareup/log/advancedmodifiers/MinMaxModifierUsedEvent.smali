.class public Lcom/squareup/log/advancedmodifiers/MinMaxModifierUsedEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "MinMaxModifierUsedEvent.java"


# instance fields
.field final number_of_items_with_max:I

.field final number_of_items_with_min:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_MIN_MAX_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 15
    iput p1, p0, Lcom/squareup/log/advancedmodifiers/MinMaxModifierUsedEvent;->number_of_items_with_min:I

    .line 16
    iput p2, p0, Lcom/squareup/log/advancedmodifiers/MinMaxModifierUsedEvent;->number_of_items_with_max:I

    return-void
.end method
