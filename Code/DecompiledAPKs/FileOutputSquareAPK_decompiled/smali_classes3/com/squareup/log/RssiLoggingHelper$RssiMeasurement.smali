.class Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;
.super Ljava/lang/Object;
.source "RssiLoggingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/RssiLoggingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RssiMeasurement"
.end annotation


# instance fields
.field private final rssi:I

.field final synthetic this$0:Lcom/squareup/log/RssiLoggingHelper;

.field private final timestamp:J


# direct methods
.method constructor <init>(Lcom/squareup/log/RssiLoggingHelper;I)V
    .locals 2

    .line 253
    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->this$0:Lcom/squareup/log/RssiLoggingHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    invoke-static {p1}, Lcom/squareup/log/RssiLoggingHelper;->access$100(Lcom/squareup/log/RssiLoggingHelper;)Lcom/squareup/util/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->timestamp:J

    .line 255
    iput p2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->rssi:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I
    .locals 0

    .line 249
    iget p0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->rssi:I

    return p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->this$0:Lcom/squareup/log/RssiLoggingHelper;

    invoke-static {v1}, Lcom/squareup/log/RssiLoggingHelper;->access$200(Lcom/squareup/log/RssiLoggingHelper;)Ljava/text/SimpleDateFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->this$0:Lcom/squareup/log/RssiLoggingHelper;

    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper;->access$100(Lcom/squareup/log/RssiLoggingHelper;)Lcom/squareup/util/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->timestamp:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->rssi:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
