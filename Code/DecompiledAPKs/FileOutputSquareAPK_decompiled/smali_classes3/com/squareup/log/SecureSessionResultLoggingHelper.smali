.class public Lcom/squareup/log/SecureSessionResultLoggingHelper;
.super Ljava/lang/Object;
.source "SecureSessionResultLoggingHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDescription(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)Ljava/lang/String;
    .locals 3

    .line 14
    sget-object v0, Lcom/squareup/log/SecureSessionResultLoggingHelper$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureSessionResult:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 42
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "Unknown secure session result type: %s"

    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_0
    const-string p0, "Secure Session Maximum Number of Readers Connected"

    return-object p0

    :pswitch_1
    const-string p0, "Secure Session Module Generic Error"

    return-object p0

    :pswitch_2
    const-string p0, "Secure Session Denied By Server"

    return-object p0

    :pswitch_3
    const-string p0, "Secure Session No Reader"

    return-object p0

    :pswitch_4
    const-string p0, "Secure Session Generic Error"

    return-object p0

    :pswitch_5
    const-string p0, "Secure Session Call Unexpected"

    return-object p0

    :pswitch_6
    const-string p0, "Secure Session Error"

    return-object p0

    :pswitch_7
    const-string p0, "Secure Session Already Terminated"

    return-object p0

    :pswitch_8
    const-string p0, "Secure Session Not Terminated"

    return-object p0

    :pswitch_9
    const-string p0, "Secure Session Already Initialized"

    return-object p0

    :pswitch_a
    const-string p0, "Secure Session Not Initialized"

    return-object p0

    :pswitch_b
    const-string p0, "Secure Session Invalid Parameter"

    return-object p0

    :pswitch_c
    const-string p0, "Secure Session Success"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getValueForCardReader(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 1

    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/squareup/log/SecureSessionResultLoggingHelper;->getDescription(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
