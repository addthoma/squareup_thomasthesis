.class public Lcom/squareup/log/ReaderEventLogger;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"

# interfaces
.implements Lcom/squareup/cardreader/ReaderEventLogger;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;,
        Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;,
        Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;,
        Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;,
        Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;,
        Lcom/squareup/log/ReaderEventLogger$AudioEvent;,
        Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;
    }
.end annotation


# static fields
.field private static final NUM_RSSI_SAMPLES_FOR_CONNECTION_EVENTS:I = 0x64

.field private static final RSSI_LOGGING_INTERVAL_MILLIS:J = 0x1b7740L

.field private static final RSSI_MAX:I = 0x0

.field private static final RSSI_MIN:I = -0x7f


# instance fields
.field private final aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderOracle:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

.field private final internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final readerEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/squareup/log/ReaderEvent$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

.field protected final readerStateMap:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;",
            ">;"
        }
    .end annotation
.end field

.field private final rssiIntermittentLoggingRunnable:Ljava/lang/Runnable;

.field private final rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/util/Clock;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/internet/InternetStatusMonitor;Lcom/squareup/log/AclConnectionLoggingHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/log/ReaderSessionIds;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            "Lcom/squareup/log/AclConnectionLoggingHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p5, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    .line 158
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 159
    iput-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 160
    iput-object p6, p0, Lcom/squareup/log/ReaderEventLogger;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 161
    new-instance p1, Lcom/squareup/util/DateTimeFactory;

    invoke-direct {p1}, Lcom/squareup/util/DateTimeFactory;-><init>()V

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    .line 162
    iput-object p3, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 163
    iput-object p4, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 164
    iget-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 165
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    .line 166
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->readerStateMap:Ljava/util/concurrent/ConcurrentMap;

    .line 167
    new-instance p1, Lcom/squareup/log/RssiLoggingHelper;

    invoke-direct {p1, p5}, Lcom/squareup/log/RssiLoggingHelper;-><init>(Lcom/squareup/util/Clock;)V

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    .line 168
    new-instance p1, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$zJY_oimj17WOqdkRpYobr_GX-Ls;

    invoke-direct {p1, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$zJY_oimj17WOqdkRpYobr_GX-Ls;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->rssiIntermittentLoggingRunnable:Ljava/lang/Runnable;

    .line 169
    iput-object p7, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 170
    iput-object p8, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderOracle:Ljavax/inject/Provider;

    .line 171
    iput-object p11, p0, Lcom/squareup/log/ReaderEventLogger;->aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

    .line 172
    iput-object p9, p0, Lcom/squareup/log/ReaderEventLogger;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 173
    iput-object p10, p0, Lcom/squareup/log/ReaderEventLogger;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    .line 175
    iget-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->rssiIntermittentLoggingRunnable:Ljava/lang/Runnable;

    const-wide/32 p2, 0x1b7740

    invoke-interface {p6, p1, p2, p3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private addLastConnectionAttemptData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/dipper/events/DipperEvent;)V
    .locals 5

    .line 552
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->readerStateMap:Ljava/util/concurrent/ConcurrentMap;

    .line 553
    invoke-virtual {p2}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    if-eqz v0, :cond_0

    .line 555
    iget-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    .line 556
    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getTimeOfLastConnectionAttempt()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    .line 555
    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastConnectionAttempt(J)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    goto :goto_0

    .line 558
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Current Reader State shouldn\'t be null here: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private addReaderData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 4

    if-nez p2, :cond_0

    return-void

    .line 535
    :cond_0
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerState(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 536
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->firmwareVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->firmwareVersion(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 537
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-direct {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 539
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 540
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    .line 541
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iget-object v2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 540
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastSuccessfulConnection(Ljava/lang/Long;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 544
    :cond_1
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_2

    .line 545
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryInfo()Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->batteryInfo(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 546
    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getBleRate()Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleRate(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    :cond_2
    return-void
.end method

.method private addRssiData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Ljava/lang/String;)V
    .locals 2

    .line 518
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/log/RssiLoggingHelper;->hasMeasurements(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/log/RssiLoggingHelper;->getStatistics(Ljava/lang/String;)Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;

    move-result-object v0

    .line 524
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiStatistics(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    const/16 v1, 0x64

    .line 525
    invoke-virtual {v0, p2, v1}, Lcom/squareup/log/RssiLoggingHelper;->formatMeasurements(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiSamples(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    return-void
.end method

.method private bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 2

    .line 1520
    invoke-direct {p0, p3}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object v0

    .line 1521
    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;-><init>()V

    .line 1522
    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 1523
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerName(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 1524
    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->macAddress(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 1525
    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getPreviousState()Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/dipper/events/BleConnectionState;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->previousBleState(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 1526
    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getCurrentState()Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/dipper/events/BleConnectionState;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->currentBleState(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 1527
    invoke-static {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->access$300(Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method private buildPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    .locals 1

    .line 878
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;-><init>()V

    .line 879
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentTimings(Lcom/squareup/cardreader/PaymentTimings;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 880
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->cardPresenceRequired(Ljava/lang/Boolean;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    if-eqz p3, :cond_0

    .line 883
    iget-object p2, p3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->clientTenderId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    .line 884
    iget-object p2, p3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->serverTenderId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    .line 887
    :cond_0
    iget-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderSessionIds;->hasPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 888
    iget-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 889
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/log/ReaderSessionIds;->getPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;

    move-result-object p1

    .line 888
    invoke-virtual {v0, p1}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentSessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    :cond_1
    return-object v0
.end method

.method private cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;
    .locals 0

    if-eqz p1, :cond_0

    .line 700
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private cardReaderIdOrNull(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/cardreader/CardReaderId;
    .locals 0

    if-eqz p1, :cond_0

    .line 704
    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public static commsVersionToString(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const-string p0, "Unknown"

    return-object p0

    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1080
    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object p0, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    aput-object p0, v0, v1

    const-string p0, "%d.%d.%d"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private createDisconnectionEvent(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 9

    .line 483
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object v0

    .line 484
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    .line 485
    invoke-interface {v1}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getTimeOfLastCommunicationFromReader()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 486
    invoke-direct {p0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->millisToMessage(J)Ljava/lang/String;

    move-result-object v3

    .line 487
    sget-object v4, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 488
    invoke-virtual {p0, v4, p1}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    div-long/2addr v1, v5

    long-to-int v2, v1

    int-to-long v1, v2

    .line 489
    invoke-virtual {v4, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastLCRCommunication(J)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 490
    invoke-virtual {v1, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hoursMinutesAndSecondsSinceLastLCRCommunicationString(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 492
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/cardreader/BluetoothUtils;->bondedDevicesCount(Ljava/lang/String;)I

    move-result v2

    .line 493
    iget-object v3, p0, Lcom/squareup/log/ReaderEventLogger;->aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

    .line 494
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/log/AclConnectionLoggingHelper;->connectedDevicesCount(Ljava/lang/String;)I

    move-result v3

    .line 495
    sget-object v4, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 496
    invoke-virtual {v4}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v4

    .line 497
    invoke-virtual {v4, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bondedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v2

    .line 498
    invoke-virtual {v2, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/log/ReaderEventLogger;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    .line 499
    invoke-interface {v3}, Lcom/squareup/internet/InternetStatusMonitor;->getNetworkConnection()Lcom/squareup/internet/InternetConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->internetConnection(Lcom/squareup/internet/InternetConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/log/ReaderEventLogger;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 500
    invoke-interface {v3}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bluetoothEnabled(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 502
    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getTimeSinceLastSuccessfulConnection()J

    move-result-wide v2

    const-wide/16 v7, -0x1

    cmp-long v4, v2, v7

    if-eqz v4, :cond_0

    .line 503
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    .line 504
    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getTimeSinceLastSuccessfulConnection()J

    move-result-wide v7

    sub-long/2addr v2, v7

    div-long/2addr v2, v5

    .line 506
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastSuccessfulConnection(Ljava/lang/Long;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 508
    :cond_0
    invoke-virtual {v1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 509
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/log/ReaderEventLogger;->addRssiData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Ljava/lang/String;)V

    return-object v1
.end method

.method private flushAndCompleteEventsForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 11

    .line 989
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->toString()Ljava/lang/String;

    move-result-object v0

    .line 990
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    .line 991
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v2

    .line 992
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getChargeCycleCount()I

    move-result v3

    .line 993
    iget-object v4, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/log/ReaderSessionIds;->getSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;

    move-result-object v4

    .line 997
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_0

    .line 998
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    move-result-object v5

    iget-object v6, v5, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->inCommsRateValue:Ljava/lang/String;

    .line 999
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->outCommsRateValue:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v5, v6

    .line 1002
    :goto_0
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1004
    :goto_1
    iget-object v8, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {v8}, Ljava/util/Queue;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1005
    iget-object v8, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {v8}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/log/ReaderEvent$Builder;

    .line 1007
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v9

    iget-object v10, v8, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v9, v10}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1008
    invoke-interface {v7, v8}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1012
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/squareup/log/ReaderEvent$Builder;->setReaderTypePrefix(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 1013
    iput-object v0, v8, Lcom/squareup/log/ReaderEvent$Builder;->connectionType:Ljava/lang/String;

    .line 1014
    iput-object v1, v8, Lcom/squareup/log/ReaderEvent$Builder;->firmwareVersion:Ljava/lang/String;

    .line 1015
    iput-object v2, v8, Lcom/squareup/log/ReaderEvent$Builder;->hardwareSerialNumber:Ljava/lang/String;

    .line 1016
    iput v3, v8, Lcom/squareup/log/ReaderEvent$Builder;->chargeCycleCount:I

    .line 1017
    iput-object v4, v8, Lcom/squareup/log/ReaderEvent$Builder;->sessionId:Ljava/lang/String;

    if-eqz v6, :cond_2

    if-eqz v5, :cond_2

    .line 1019
    iput-object v6, v8, Lcom/squareup/log/ReaderEvent$Builder;->inCommsRate:Ljava/lang/String;

    .line 1020
    iput-object v5, v8, Lcom/squareup/log/ReaderEvent$Builder;->outCommsRate:Ljava/lang/String;

    .line 1023
    :cond_2
    invoke-virtual {v8}, Lcom/squareup/log/ReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEvent;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    const-string v10, "To EventStream: %s"

    .line 1024
    invoke-static {v10, v9}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1025
    iget-object v9, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v9, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 1028
    :cond_3
    iget-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {p1, v7}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private flushPartialEventsForCardReader(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 4

    .line 967
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v0, p1}, Lcom/squareup/log/ReaderSessionIds;->getSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;

    move-result-object v0

    .line 969
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 971
    :goto_0
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 972
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/ReaderEvent$Builder;

    .line 973
    iget-object v3, v2, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p1, v3}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 974
    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 978
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v2, v3}, Lcom/squareup/log/ReaderEvent$Builder;->setReaderTypePrefix(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 979
    iput-object v0, v2, Lcom/squareup/log/ReaderEvent$Builder;->sessionId:Ljava/lang/String;

    .line 980
    iget-object v3, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v2}, Lcom/squareup/log/ReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEvent;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 983
    :cond_1
    iget-object p1, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    invoke-interface {p1, v1}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private getCurrentReaderState(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;
    .locals 0

    .line 1469
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object p1

    return-object p1
.end method

.method private getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;
    .locals 2

    .line 1482
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->readerStateMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    if-nez v0, :cond_0

    .line 1484
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 1485
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->readerStateMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    if-nez p1, :cond_1

    return-object v0

    :cond_0
    move-object p1, v0

    :cond_1
    return-object p1
.end method

.method public static synthetic lambda$1Gsp83aZayRYo2ENcK81sG2fHpQ(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logAlreadyConnected(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V

    return-void
.end method

.method public static synthetic lambda$NPOBYgKDH0UMHdUdeGjKf-V-bSY(Lcom/squareup/log/ReaderEventLogger;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->updateReaderStateMap(Ljava/util/Collection;)V

    return-void
.end method

.method public static synthetic lambda$OrljRTCVEU_xd-apFq0oz5C8GbI(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionSuccess(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V

    return-void
.end method

.method public static synthetic lambda$RaVB5LC86YHM-2FSJpuWSEQoQT4(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/dipper/events/DipperEvent$RssiReceived;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->onRemoteRssiReceived(Lcom/squareup/dipper/events/DipperEvent$RssiReceived;)V

    return-void
.end method

.method public static synthetic lambda$ZlfUrB11Ur72UM5J1pvk1hk7mgQ(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionFailure(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V

    return-void
.end method

.method static synthetic lambda$initialize$0(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;
    .locals 2

    .line 208
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;-><init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;Lcom/squareup/log/ReaderEventLogger$1;)V

    return-object v0
.end method

.method static synthetic lambda$initialize$1(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;
    .locals 2

    .line 213
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;-><init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;Lcom/squareup/log/ReaderEventLogger$1;)V

    return-object v0
.end method

.method static synthetic lambda$initialize$2(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;
    .locals 2

    .line 218
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;-><init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;Lcom/squareup/log/ReaderEventLogger$1;)V

    return-object v0
.end method

.method public static synthetic lambda$orrSLMJZGBonKXE9vkSutc5NtQY(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/dipper/events/AclEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->onAclEvent(Lcom/squareup/dipper/events/AclEvent;)V

    return-void
.end method

.method public static synthetic lambda$zJY_oimj17WOqdkRpYobr_GX-Ls(Lcom/squareup/log/ReaderEventLogger;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/log/ReaderEventLogger;->logAndFlushWirelessRssiSamples()V

    return-void
.end method

.method private logAlreadyConnected(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 5

    .line 388
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$100(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/dipper/events/DipperEvent;

    move-result-object v0

    .line 390
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->BLE_ALREADY_CONNECTED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v3}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 391
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "%s: %s %s"

    .line 390
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 393
    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_ALREADY_CONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 394
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 395
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 394
    invoke-direct {p0, v2, v3, v0}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v0

    .line 396
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v0

    .line 398
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$200(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->addReaderData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 400
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method private logAndFlushWirelessRssiSamples()V
    .locals 8

    .line 940
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 941
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    .line 942
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 943
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 944
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-virtual {v2, v1}, Lcom/squareup/log/RssiLoggingHelper;->hasMeasurements(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 945
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-virtual {v2, v1}, Lcom/squareup/log/RssiLoggingHelper;->getStatistics(Ljava/lang/String;)Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;

    move-result-object v2

    .line 946
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 948
    invoke-virtual {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMin()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMax()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMean()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 949
    invoke-virtual {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getVariance()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getStdDev()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "RSSI samples: min %d, max %d, mean %f, var %f, sdev %f."

    .line 947
    invoke-static {v3, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 950
    new-instance v4, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    invoke-direct {v4}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;-><init>()V

    sget-object v5, Lcom/squareup/analytics/ReaderEventName;->WIRELESS_RSSI_SAMPLES:Lcom/squareup/analytics/ReaderEventName;

    .line 952
    invoke-virtual {v4, v5}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v4

    .line 953
    invoke-virtual {v4, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->macAddress(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v4

    .line 954
    invoke-virtual {v4, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v3

    .line 955
    invoke-virtual {v3, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiStatistics(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    .line 956
    invoke-virtual {v3, v1}, Lcom/squareup/log/RssiLoggingHelper;->formatMeasurements(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiSamples(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 957
    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object v1

    .line 958
    invoke-direct {p0, v1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    goto/16 :goto_0

    .line 963
    :cond_1
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->rssiIntermittentLoggingRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0x1b7740

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V
    .locals 3

    .line 604
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 605
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    iget-object v2, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->unlocalizedDescription:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logBleConnectionFailure(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState<",
            "Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;",
            ">;)V"
        }
    .end annotation

    .line 352
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$100(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/dipper/events/DipperEvent;

    move-result-object v0

    check-cast v0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    .line 354
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SILENT_RETRY:Lcom/squareup/analytics/ReaderEventName;

    .line 355
    :goto_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 356
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getErrorType()Lcom/squareup/dipper/events/BleErrorType;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 357
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "%s: %s: %s %s"

    .line 355
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 360
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 361
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    .line 360
    invoke-direct {p0, v1, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 362
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 363
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getAttemptNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionAttemptNumber(Ljava/lang/Integer;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 365
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->addRssiData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Ljava/lang/String;)V

    .line 366
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$200(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/log/ReaderEventLogger;->addReaderData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 367
    invoke-direct {p0, v1, v0}, Lcom/squareup/log/ReaderEventLogger;->addLastConnectionAttemptData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/dipper/events/DipperEvent;)V

    .line 369
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDisconnectionEvent()Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 371
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getDisconnectStatus()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectStatus(Ljava/lang/Integer;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 373
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/BleError;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    goto :goto_1

    .line 375
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 376
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionError;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 380
    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method private logBleConnectionSuccess(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)V
    .locals 5

    .line 324
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$100(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/dipper/events/DipperEvent;

    move-result-object v0

    .line 326
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v3}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 327
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "%s: %s %s"

    .line 326
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 329
    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 330
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 331
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    .line 330
    invoke-direct {p0, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v2

    .line 332
    invoke-virtual {v2, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v1

    .line 334
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$200(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->addReaderData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 335
    invoke-direct {p0, v1, v0}, Lcom/squareup/log/ReaderEventLogger;->addLastConnectionAttemptData(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;Lcom/squareup/dipper/events/DipperEvent;)V

    .line 338
    invoke-virtual {v0}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object v0

    .line 339
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setTimeSinceLastSuccessfulConnection(J)V

    .line 340
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->access$200(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 342
    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method private logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V
    .locals 2

    .line 923
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 924
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->readerEventQueue:Ljava/util/Queue;

    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    .line 925
    invoke-virtual {v1}, Lcom/squareup/util/DateTimeFactory;->now()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestampIfNotSet(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 926
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 924
    invoke-interface {v0, p2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 928
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSystemInfoAcquired()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 929
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->flushAndCompleteEventsForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method private logReaderEventDirect(Lcom/squareup/log/ReaderEvent$Builder;)V
    .locals 2

    .line 1037
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    .line 1038
    invoke-virtual {v0}, Lcom/squareup/util/DateTimeFactory;->now()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 1039
    invoke-virtual {p1}, Lcom/squareup/log/ReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEvent;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "To EventStream: %s"

    .line 1040
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1041
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private millisToMessage(J)Ljava/lang/String;
    .locals 5

    const-wide/32 v0, 0x5265c00

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const-string p1, "more than 24h"

    return-object p1

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    .line 1066
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "impossibly "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v0, "UTC"

    .line 1069
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 1070
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 1071
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "HH:mm:ss"

    invoke-direct {v2, v4, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1072
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1073
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1074
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private onAclEvent(Lcom/squareup/dipper/events/AclEvent;)V
    .locals 8

    .line 234
    instance-of v0, p1, Lcom/squareup/dipper/events/AclConnectedEvent;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "%s: %s %s"

    if-eqz v0, :cond_1

    .line 235
    check-cast p1, Lcom/squareup/dipper/events/AclConnectedEvent;

    .line 236
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclConnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/squareup/log/AclConnectionLoggingHelper;->addConnectedDevices(Lcom/squareup/cardreader/WirelessConnection;)V

    .line 239
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclConnectedEvent;->getFromAppInitialization()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_APP_INITIALIZATION:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    .line 242
    :cond_0
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_BROADCAST:Lcom/squareup/analytics/ReaderEventName;

    .line 245
    :goto_0
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v3

    .line 246
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclConnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    .line 247
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclConnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 245
    invoke-static {v6, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclConnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 251
    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 253
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 254
    :cond_1
    instance-of v0, p1, Lcom/squareup/dipper/events/AclDisconnectedEvent;

    if-eqz v0, :cond_2

    .line 255
    check-cast p1, Lcom/squareup/dipper/events/AclDisconnectedEvent;

    .line 256
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclDisconnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v6

    .line 256
    invoke-virtual {v0, v6}, Lcom/squareup/log/AclConnectionLoggingHelper;->removeConnectedDevices(Lcom/squareup/cardreader/WirelessConnection;)V

    .line 259
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v6, Lcom/squareup/analytics/ReaderEventName;->ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v6}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v3

    .line 260
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclDisconnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    .line 261
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclDisconnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 259
    invoke-static {v0, v5, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 263
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 264
    invoke-virtual {p1}, Lcom/squareup/dipper/events/AclDisconnectedEvent;->getWirelessConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 265
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 267
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 268
    :cond_2
    instance-of v0, p1, Lcom/squareup/dipper/events/AclErrorEvent;

    if-eqz v0, :cond_3

    .line 269
    sget-object p1, Lcom/squareup/analytics/ReaderEventName;->ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    const-string v0, ""

    .line 270
    invoke-direct {p0, p1, v0, v0}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 272
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_1
    return-void

    .line 274
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unspported AclEvent type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onRemoteRssiReceived(Lcom/squareup/dipper/events/DipperEvent$RssiReceived;)V
    .locals 2

    .line 1102
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;->getRssi()I

    move-result v0

    const/16 v1, -0x7f

    if-le v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;->getRssi()I

    move-result v0

    if-ltz v0, :cond_0

    goto :goto_0

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    .line 1106
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;->getRssi()I

    move-result p1

    .line 1105
    invoke-virtual {v0, v1, p1}, Lcom/squareup/log/RssiLoggingHelper;->addMeasurement(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private prettyHashCode(I)Ljava/lang/String;
    .locals 2

    .line 1045
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private updateCurrentReaderState(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 2

    .line 1500
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object p1

    .line 1501
    invoke-virtual {p1}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->getCurrentState()Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setPreviousState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 1502
    invoke-virtual {p1, p2}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setCurrentState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 1503
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setTimeOfLastCommunicationFromReader(J)V

    .line 1506
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne p2, v0, :cond_0

    .line 1507
    iget-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setTimeOfLastConnectionAttempt(J)V

    :cond_0
    return-void
.end method

.method private updateReaderStateMap(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .line 563
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 564
    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 568
    :cond_0
    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 569
    invoke-static {v1}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->access$300(Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    if-nez v2, :cond_1

    goto :goto_1

    .line 573
    :cond_1
    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->access$302(Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public addToOhSnapLog(Ljava/lang/String;)V
    .locals 2

    .line 895
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public addToOhSnapLog(Ljava/lang/String;I)V
    .locals 5

    .line 903
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {p0, p2}, Lcom/squareup/log/ReaderEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v3, p2

    const-string p1, "%s (%s)"

    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 899
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .line 907
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    aput-object p2, v3, p1

    .line 908
    invoke-direct {p0, p3}, Lcom/squareup/log/ReaderEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v3, p2

    const-string p1, "%s: %s (%s)"

    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 907
    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method protected bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 1

    .line 1514
    invoke-interface {p2}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1515
    invoke-interface {p2}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p2

    .line 1514
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public initialize()V
    .locals 4

    .line 203
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderOracle:Ljavax/inject/Provider;

    .line 204
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v1

    .line 203
    invoke-static {v0, v1}, Lrx/Observable;->concat(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;

    .line 207
    invoke-virtual {v1, v2}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$VKNUHDJ8aC30Y613IMDRWD0lECk;->INSTANCE:Lcom/squareup/log/-$$Lambda$ReaderEventLogger$VKNUHDJ8aC30Y613IMDRWD0lECk;

    .line 208
    invoke-virtual {v1, v0, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$OrljRTCVEU_xd-apFq0oz5C8GbI;

    invoke-direct {v2, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$OrljRTCVEU_xd-apFq0oz5C8GbI;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 209
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 211
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    .line 212
    invoke-virtual {v1, v2}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$0Zd4q5OyxdZ73x4KIKhXyvybGuw;->INSTANCE:Lcom/squareup/log/-$$Lambda$ReaderEventLogger$0Zd4q5OyxdZ73x4KIKhXyvybGuw;

    .line 213
    invoke-virtual {v1, v0, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$ZlfUrB11Ur72UM5J1pvk1hk7mgQ;

    invoke-direct {v2, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$ZlfUrB11Ur72UM5J1pvk1hk7mgQ;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 214
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 216
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;

    .line 217
    invoke-virtual {v1, v2}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$ieEnxdCrJ-v87r1Xlq0uESqKY6Q;->INSTANCE:Lcom/squareup/log/-$$Lambda$ReaderEventLogger$ieEnxdCrJ-v87r1Xlq0uESqKY6Q;

    .line 218
    invoke-virtual {v1, v0, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$1Gsp83aZayRYo2ENcK81sG2fHpQ;

    invoke-direct {v2, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$1Gsp83aZayRYo2ENcK81sG2fHpQ;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 219
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 221
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;

    .line 222
    invoke-virtual {v1, v2}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$RaVB5LC86YHM-2FSJpuWSEQoQT4;

    invoke-direct {v2, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$RaVB5LC86YHM-2FSJpuWSEQoQT4;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 223
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 225
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->cardReaderDataEvents()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/dipper/events/CardReaderDataEventFilters;->INSTANCE:Lcom/squareup/dipper/events/CardReaderDataEventFilters;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/log/-$$Lambda$0kFZ0jpbNmvk-TfYEQEn8uvG5NQ;

    invoke-direct {v3, v2}, Lcom/squareup/log/-$$Lambda$0kFZ0jpbNmvk-TfYEQEn8uvG5NQ;-><init>(Lcom/squareup/dipper/events/CardReaderDataEventFilters;)V

    .line 226
    invoke-virtual {v1, v3}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$orrSLMJZGBonKXE9vkSutc5NtQY;

    invoke-direct {v2, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$orrSLMJZGBonKXE9vkSutc5NtQY;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    .line 227
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 230
    new-instance v1, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$NPOBYgKDH0UMHdUdeGjKf-V-bSY;

    invoke-direct {v1, p0}, Lcom/squareup/log/-$$Lambda$ReaderEventLogger$NPOBYgKDH0UMHdUdeGjKf-V-bSY;-><init>(Lcom/squareup/log/ReaderEventLogger;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
    .locals 1

    .line 620
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logBatteryHud(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 819
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_HUD:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    .line 820
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 819
    invoke-virtual {p0, v0, v1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;-><init>()V

    .line 822
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->percentage(I)Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_HUD:Lcom/squareup/analytics/ReaderEventName;

    iget-object v1, v1, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 823
    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object v0

    .line 821
    invoke-direct {p0, p1, v0}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logBatteryInfo(ILcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 610
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_LEVEL:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p2}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    .line 611
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 610
    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;I)V

    .line 613
    new-instance p1, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    invoke-direct {p1}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;-><init>()V

    .line 614
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->percentage(I)Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    move-result-object p1

    .line 615
    invoke-static {p2}, Lcom/squareup/cardreader/BatteryLevel;->isBatteryLow(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_LOW_BATTERY:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_LEVEL:Lcom/squareup/analytics/ReaderEventName;

    :goto_0
    iget-object v0, v0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 613
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logBleConnectionAction(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 4

    .line 432
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_RECEIVED_ACTION:Lcom/squareup/analytics/ReaderEventName;

    .line 433
    invoke-virtual {v2}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {p4}, Lcom/squareup/cardreader/ble/BleAction;->describe()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    aput-object p3, v1, v2

    iget p3, p1, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const-string p3, "%s \"%s\" in state %s on id: %d"

    .line 432
    invoke-static {v0, p3, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 435
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_RECEIVED_ACTION:Lcom/squareup/analytics/ReaderEventName;

    .line 436
    invoke-virtual {p0, v0, p2}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v0

    .line 437
    invoke-interface {p4}, Lcom/squareup/cardreader/ble/BleAction;->describe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->receivedBleAction(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object v0

    .line 438
    invoke-virtual {v0, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p3

    .line 439
    invoke-virtual {p3, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p3

    .line 440
    invoke-virtual {p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p3

    .line 441
    invoke-direct {p0, p3}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    .line 446
    instance-of p3, p4, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    if-eqz p3, :cond_0

    .line 448
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->createDisconnectionEvent(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 449
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    :cond_0
    return-void
.end method

.method public logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V
    .locals 4

    .line 302
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_ENQUEUED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v2}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 303
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const-string v2, "%s: %s %s %s"

    .line 302
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 304
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/BluetoothUtils;->bondedDevicesCount(Ljava/lang/String;)I

    move-result v1

    .line 305
    iget-object v2, p0, Lcom/squareup/log/ReaderEventLogger;->aclConnectionLoggingHelper:Lcom/squareup/log/AclConnectionLoggingHelper;

    .line 306
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/log/AclConnectionLoggingHelper;->connectedDevicesCount(Ljava/lang/String;)I

    move-result v2

    .line 308
    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_ENQUEUED:Lcom/squareup/analytics/ReaderEventName;

    .line 309
    invoke-virtual {p0, v3, p1}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 310
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 311
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleConnectType(Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 312
    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bondedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 313
    invoke-virtual {p1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 314
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    .line 315
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method public logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
    .locals 4

    .line 585
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    .line 586
    invoke-virtual {v2}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 p3, 0x2

    aput-object p4, v1, p3

    iget p3, p1, Lcom/squareup/cardreader/CardReaderId;->id:I

    .line 587
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const-string p3, "%s: %s -> %s on id: %d"

    .line 586
    invoke-static {v0, p3, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 591
    invoke-direct {p0, p2, p4}, Lcom/squareup/log/ReaderEventLogger;->updateCurrentReaderState(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 593
    sget-object p4, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    .line 594
    invoke-virtual {p0, p4, p2}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p2

    .line 595
    invoke-virtual {p2, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p2

    .line 596
    invoke-virtual {p2, p5}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->additionalContext(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p2

    .line 597
    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 598
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    .line 600
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method public logBleDisconnectedEvent(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;)V
    .locals 0

    .line 460
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->createDisconnectionEvent(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 461
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getDisconnectStatus()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectStatus(Ljava/lang/Integer;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 462
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getErrorBeforeConnection()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->errorBeforeConnection(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 464
    invoke-virtual {p1, p4}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectRequestedBeforeDisconnection(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 465
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getLoggingStateAtDisconnection()Lcom/squareup/cardreader/ble/R12State;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 467
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getLoggingStateAtDisconnection()Lcom/squareup/cardreader/ble/R12State;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/R12State;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->currentBleState(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 469
    :cond_0
    invoke-virtual {p5}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;->getShouldReconnect()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->willReconnect(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 470
    invoke-virtual {p5}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;->getShouldReconnect()Z

    move-result p2

    if-nez p2, :cond_1

    .line 471
    invoke-virtual {p5}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;->getReason()Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->noReconnectReason(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 473
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 474
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/blecoroutines/BleError;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    goto :goto_0

    .line 475
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 476
    invoke-virtual {p3}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/ConnectionError;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    .line 478
    :cond_3
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method public logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 4

    .line 408
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FORCE_UNPAIR:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v2}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 409
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "%s: %s %s"

    .line 408
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 411
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FORCE_UNPAIR:Lcom/squareup/analytics/ReaderEventName;

    .line 412
    invoke-virtual {p0, v1, p1}, Lcom/squareup/log/ReaderEventLogger;->bleEventBuilder(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 413
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;

    move-result-object p1

    .line 414
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    move-result-object p1

    .line 416
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBleConnectionEvent(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;)V

    return-void
.end method

.method public logBluetoothStatusChanged(Ljava/lang/String;ZZ)V
    .locals 4

    .line 280
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string v3, "Bluetooth state has changed to: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;-><init>(Ljava/lang/String;ZZLcom/squareup/log/ReaderEventLogger$1;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logCirqueTamperStatus(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/bills/CardData$ReaderType;I)V
    .locals 2

    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CIRQUE_SECURITY_STATUS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v1, p2}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 807
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    .line 808
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->setReaderTypePrefix(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CIRQUE_SECURITY_STATUS:Lcom/squareup/analytics/ReaderEventName;

    iget-object v1, v1, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 809
    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 812
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    goto :goto_0

    .line 814
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEventDirect(Lcom/squareup/log/ReaderEvent$Builder;)V

    :goto_0
    return-void
.end method

.method public logCommsRateUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 679
    sget-object v0, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_RATE_UPDATED:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    .line 680
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->toString()Ljava/lang/String;

    move-result-object p1

    .line 679
    invoke-virtual {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public logCommsVersionAcquired(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 665
    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 666
    invoke-static {p4}, Lcom/squareup/log/ReaderEventLogger;->commsVersionToString(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p3}, Lcom/squareup/log/ReaderEventLogger;->commsVersionToString(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "[%s] Reader: %s, Register: %s"

    .line 665
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 668
    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_VERSION_ACQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-virtual {v1, p1}, Lcom/squareup/cardreader/CardReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;-><init>()V

    .line 670
    invoke-static {p4}, Lcom/squareup/log/ReaderEventLogger;->commsVersionToString(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->readerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    move-result-object p4

    .line 671
    invoke-static {p3}, Lcom/squareup/log/ReaderEventLogger;->commsVersionToString(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->registerProtocolVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    move-result-object p3

    .line 672
    invoke-virtual {p3, p2}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->commsVersionResult(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/log/ReaderEventLogger;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 673
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/log/ReaderSessionIds;->getSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->sessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 674
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    sget-object p3, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_VERSION_ACQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    iget-object p3, p3, Lcom/squareup/cardreader/CardReaderEventName;->value:Ljava/lang/String;

    .line 675
    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 669
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logCoreDumpResult(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 2

    if-eqz p2, :cond_0

    .line 685
    sget-object p2, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_FOUND:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 686
    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 687
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    .line 688
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object v0

    iget-object p2, p2, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 689
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 687
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logEvent(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 1

    .line 709
    invoke-interface {p3, p2}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;I)V

    .line 710
    new-instance p1, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    invoke-direct {p1}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;-><init>()V

    .line 711
    invoke-direct {p0, p2}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 712
    invoke-interface {p3}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValue()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 710
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 835
    invoke-virtual {p2, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 836
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 837
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEvent$Builder;->paymentTimings(Lcom/squareup/cardreader/PaymentTimings;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 838
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Ljava/lang/String;)V
    .locals 1

    .line 828
    invoke-virtual {p2, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    new-instance p3, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {p3}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 830
    invoke-virtual {p3, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 829
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 2

    .line 693
    invoke-interface {p2, p1}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 694
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    .line 695
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object v0

    .line 696
    invoke-interface {p2}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 694
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 6

    .line 733
    iget-object v0, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    const-string v1, "Unknown"

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    .line 734
    :goto_0
    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_LOG:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v2, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " @ "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->timestamp:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    iget-object v0, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 739
    :goto_1
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;-><init>()V

    .line 740
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_LOG:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->overrideValue:Ljava/lang/String;

    .line 741
    iget-wide v1, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->timestamp:J

    iput-wide v1, v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->timestamp:J

    .line 742
    iget-object v1, p0, Lcom/squareup/log/ReaderEventLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    iget-wide v2, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->timestamp:J

    invoke-virtual {v1, v2, v3}, Lcom/squareup/util/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->overrideTimestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 743
    iget v1, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->event:I

    iput v1, v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->event:I

    .line 744
    iget v1, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->source:I

    iput v1, v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->source:I

    .line 745
    iget-object p2, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    iput-object p2, v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->message:Ljava/lang/String;

    .line 747
    invoke-direct {p0, p1, v0}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logFirmwareAssetVersionInfo(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 634
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;-><init>()V

    .line 635
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareAssetVersionInfos()[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    move-result-object v1

    .line 634
    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->firmwareComponentVersions([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->FIRMWARE_ASSET_VERSIONS:Lcom/squareup/cardreader/CardReaderEventName;

    iget-object v1, v1, Lcom/squareup/cardreader/CardReaderEventName;->value:Ljava/lang/String;

    .line 636
    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object v0

    .line 634
    invoke-direct {p0, p1, v0}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 2

    if-nez p3, :cond_0

    return-void

    .line 764
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE_REASON:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v1, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 765
    invoke-static {p3}, Lcom/squareup/log/FirmwareUpdateResultLoggingHelper;->getDescription(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 764
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 766
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;-><init>()V

    .line 767
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->firmwareUpdateSessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE_REASON:Lcom/squareup/analytics/ReaderEventName;

    .line 769
    invoke-virtual {v1}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Lcom/squareup/log/FirmwareUpdateResultLoggingHelper;->getDescription(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 768
    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 766
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 1

    .line 752
    invoke-interface {p3, p1}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 753
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;-><init>()V

    .line 754
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->firmwareUpdateSessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;

    move-result-object p2

    .line 755
    invoke-interface {p3}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValue()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 753
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V
    .locals 4

    .line 285
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLE GATT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->describe()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    iget-object v2, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    iget-object v2, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->characteristicUuid:Ljava/util/UUID;

    .line 288
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->characteristic(Ljava/util/UUID;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->descriptorUuid:Ljava/util/UUID;

    .line 289
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->descriptor(Ljava/util/UUID;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    move-result-object v1

    iget v2, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->newState:I

    .line 290
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->newState(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    move-result-object v1

    iget v2, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->mtu:I

    .line 291
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->mtu(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    move-result-object v1

    iget p1, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->status:I

    .line 292
    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->gattStatus(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;

    move-result-object p1

    .line 293
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;

    move-result-object p1

    .line 287
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    .line 860
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/log/ReaderEventLogger;->buildPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    move-result-object p3

    .line 861
    iget-object p4, p2, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {p3, p4}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 862
    invoke-virtual {p2, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object p2

    iget-object p4, p3, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentSessionId:Ljava/lang/String;

    invoke-virtual {p0, p2, p4}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    invoke-direct {p0, p1, p3}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    .line 869
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/log/ReaderEventLogger;->buildPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;

    move-result-object p3

    .line 870
    invoke-virtual {p3, p2}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 871
    iget-object p4, p3, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentSessionId:Ljava/lang/String;

    invoke-virtual {p0, p2, p4}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    invoke-direct {p0, p1, p3}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 2

    .line 647
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    new-instance v1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logReaderError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 1

    .line 651
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;-><init>()V

    .line 652
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;->error(Lcom/squareup/cardreader/lcr/CrsReaderError;)Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;

    move-result-object v0

    .line 653
    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrsReaderError;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 651
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 1

    .line 717
    invoke-static {p2, p1}, Lcom/squareup/log/SecureSessionResultLoggingHelper;->getValueForCardReader(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 718
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    .line 719
    invoke-static {p2}, Lcom/squareup/log/SecureSessionResultLoggingHelper;->getDescription(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 718
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logSecureSessionRevoked(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 1

    .line 724
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_REVOKED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    .line 725
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;-><init>()V

    .line 726
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->title(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    move-result-object p2

    .line 727
    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->description(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    move-result-object p2

    .line 728
    invoke-virtual {p4}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->name()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->uxHintString(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;

    move-result-object p2

    .line 725
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logSerialNumberReceived(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)V
    .locals 0

    .line 658
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->getCurrentReaderState(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;

    move-result-object p1

    .line 659
    invoke-virtual {p1, p2}, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->setHardwareSerialNumber(Ljava/lang/String;)V

    return-void
.end method

.method public logSystemCapabilities(ZLjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    .line 625
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string p1, "supported"

    goto :goto_0

    :cond_0
    const-string p1, "not supported"

    :goto_0
    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string p1, "Reader system capabilities: %s"

    invoke-static {v2, p1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 628
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 629
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v3, [Ljava/lang/Object;

    aput-object p2, v4, v5

    const-string p2, "Reader supports capability: %s"

    invoke-static {v2, p2, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, v1, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public logTamperData(ILcom/squareup/cardreader/CardReaderInfo;[B)V
    .locals 1

    .line 775
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_DATA:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p2}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;I)V

    .line 776
    new-instance p1, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {p1}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_TAMPER:Lcom/squareup/analytics/ReaderEventName;

    iget-object v0, v0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 777
    invoke-virtual {p1, v0}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 778
    invoke-virtual {p1, p3}, Lcom/squareup/log/ReaderEvent$Builder;->overrideRawBytes([B)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 776
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logTamperResult(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 2

    .line 784
    sget-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    invoke-virtual {p3}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 792
    sget-object p3, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FOUND:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    .line 796
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unidentified tamper status: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 789
    :cond_1
    sget-object p3, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FLAGGED:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    .line 786
    :cond_2
    sget-object p3, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 798
    :goto_0
    invoke-virtual {p3, p2}, Lcom/squareup/analytics/ReaderEventName;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;I)V

    .line 799
    new-instance p1, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {p1}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    iget-object p3, p3, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 800
    invoke-virtual {p1, p3}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 799
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/CountryCode;Lcom/squareup/CountryCode;)V
    .locals 1

    .line 641
    new-instance v0, Lcom/squareup/log/FirmwareTmsEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/FirmwareTmsEvent$Builder;-><init>()V

    .line 642
    invoke-virtual {v0, p2}, Lcom/squareup/log/FirmwareTmsEvent$Builder;->setTmsCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/log/FirmwareTmsEvent$Builder;

    move-result-object p2

    .line 643
    invoke-virtual {p2, p3}, Lcom/squareup/log/FirmwareTmsEvent$Builder;->setUserCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/log/FirmwareTmsEvent$Builder;

    move-result-object p2

    .line 641
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logReaderEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/log/ReaderEvent$Builder;)V

    return-void
.end method

.method public logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V
    .locals 1

    .line 842
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_0

    .line 843
    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    goto :goto_0

    .line 847
    :cond_0
    new-instance v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    .line 848
    invoke-virtual {v0, p2}, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    .line 849
    invoke-virtual {p2, v0}, Lcom/squareup/log/ReaderEvent$Builder;->hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    .line 850
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/log/ReaderEvent$Builder;->setReaderTypePrefix(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 851
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger;->cardReaderIdOrNull(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    .line 852
    invoke-virtual {p1}, Lcom/squareup/log/ReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEvent;

    move-result-object p1

    .line 853
    iget-object p2, p0, Lcom/squareup/log/ReaderEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 1092
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/log/ReaderEventLogger;->flushPartialEventsForCardReader(Lcom/squareup/cardreader/CardReaderId;)V

    .line 1094
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger;->rssiLoggingHelper:Lcom/squareup/log/RssiLoggingHelper;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/log/RssiLoggingHelper;->remove(Ljava/lang/String;)V

    return-void
.end method
