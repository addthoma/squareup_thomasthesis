.class public final Lcom/squareup/log/FirmwareTmsEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "FirmwareTmsEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/FirmwareTmsEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\u000c\u001a\u00020\u00002\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0004R\"\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\"\u0010\u0008\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/log/FirmwareTmsEvent$Builder;",
        "Lcom/squareup/log/ReaderEvent$Builder;",
        "()V",
        "<set-?>",
        "Lcom/squareup/CountryCode;",
        "tmsCountryCode",
        "getTmsCountryCode$cardreader_release",
        "()Lcom/squareup/CountryCode;",
        "userCountryCode",
        "getUserCountryCode$cardreader_release",
        "buildReaderEvent",
        "Lcom/squareup/log/FirmwareTmsEvent;",
        "setTmsCountryCode",
        "setUserCountryCode",
        "cardreader_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private tmsCountryCode:Lcom/squareup/CountryCode;

.field private userCountryCode:Lcom/squareup/CountryCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public buildReaderEvent()Lcom/squareup/log/FirmwareTmsEvent;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/log/FirmwareTmsEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/FirmwareTmsEvent;-><init>(Lcom/squareup/log/FirmwareTmsEvent$Builder;)V

    return-object v0
.end method

.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/log/FirmwareTmsEvent$Builder;->buildReaderEvent()Lcom/squareup/log/FirmwareTmsEvent;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderEvent;

    return-object v0
.end method

.method public final getTmsCountryCode$cardreader_release()Lcom/squareup/CountryCode;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/log/FirmwareTmsEvent$Builder;->tmsCountryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getUserCountryCode$cardreader_release()Lcom/squareup/CountryCode;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/log/FirmwareTmsEvent$Builder;->userCountryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final setTmsCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/log/FirmwareTmsEvent$Builder;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/log/FirmwareTmsEvent$Builder;->tmsCountryCode:Lcom/squareup/CountryCode;

    return-object p0
.end method

.method public final setUserCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/log/FirmwareTmsEvent$Builder;
    .locals 1

    const-string/jumbo v0, "userCountryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/log/FirmwareTmsEvent$Builder;->userCountryCode:Lcom/squareup/CountryCode;

    return-object p0
.end method
