.class public interface abstract Lcom/squareup/log/OhSnapLoggable;
.super Ljava/lang/Object;
.source "OhSnapLoggable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;,
        Lcom/squareup/log/OhSnapLoggable$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0004J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/log/OhSnapLoggable;",
        "",
        "getOhSnapMessage",
        "",
        "JavaOhSnapLoggable",
        "crash-reporting_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getOhSnapMessage()Ljava/lang/String;
.end method
