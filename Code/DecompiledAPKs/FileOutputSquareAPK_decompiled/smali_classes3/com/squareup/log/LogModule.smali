.class public abstract Lcom/squareup/log/LogModule;
.super Ljava/lang/Object;
.source "LogModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/LogModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMortarScopeHierarchy()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 21
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Mortar Hierarchy: null"

    goto :goto_0

    .line 23
    :cond_0
    invoke-static {v0}, Lmortar/MortarScopeDevHelper;->scopeHierarchyToString(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
