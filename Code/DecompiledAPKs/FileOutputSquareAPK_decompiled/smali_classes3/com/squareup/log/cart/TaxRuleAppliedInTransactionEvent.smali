.class public Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "TaxRuleAppliedInTransactionEvent.java"


# instance fields
.field public final any_rule_overridden_by_user:Z

.field public final entire_cart:Z

.field public final number_of_tax_rules:I


# direct methods
.method public constructor <init>(IZZ)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->TAX_RULE_APPLIED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 17
    iput p1, p0, Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;->number_of_tax_rules:I

    .line 18
    iput-boolean p2, p0, Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;->entire_cart:Z

    .line 19
    iput-boolean p3, p0, Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;->any_rule_overridden_by_user:Z

    return-void
.end method
