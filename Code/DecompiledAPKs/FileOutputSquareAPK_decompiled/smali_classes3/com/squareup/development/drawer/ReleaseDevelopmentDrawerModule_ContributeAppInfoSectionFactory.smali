.class public final Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory;
.super Ljava/lang/Object;
.source "ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static contributeAppInfoSection()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
            ">;"
        }
    .end annotation

    .line 27
    invoke-static {}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;->contributeAppInfoSection()Ljava/util/Set;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public static create()Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory$InstanceHolder;->access$000()Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
            ">;"
        }
    .end annotation

    .line 19
    invoke-static {}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule_ContributeAppInfoSectionFactory;->contributeAppInfoSection()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
