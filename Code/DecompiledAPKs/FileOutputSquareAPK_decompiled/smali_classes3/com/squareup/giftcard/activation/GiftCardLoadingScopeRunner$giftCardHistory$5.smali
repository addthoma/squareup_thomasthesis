.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/GiftCard;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 525
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/squareup/protos/client/giftcards/GiftCard$State;->ACTIVE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-eq v0, v1, :cond_1

    .line 526
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->goToAddValueScreen()V

    goto :goto_3

    .line 530
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    sget-object v1, Lcom/squareup/protos/client/giftcards/GiftCard$CardType;->THIRD_PARTY:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCardLoadingScope$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 533
    :cond_2
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$isAddValueEnabledForHistory$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_2

    .line 531
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$isAddValueEnabledForHistory$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 535
    :goto_2
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getOnFetchGiftCardHistory$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :goto_3
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;->call(Lcom/squareup/protos/client/giftcards/GiftCard;)V

    return-void
.end method
