.class public final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "EMPTY_CARD",
        "Lcom/squareup/Card;",
        "getEMPTY_CARD",
        "()Lcom/squareup/Card;",
        "ERRORS_BAR_TAG",
        "",
        "ERRORS_BAR_TAG$annotations",
        "()V",
        "giftcard-activation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY_CARD:Lcom/squareup/Card;

.field public static final ERRORS_BAR_TAG:Ljava/lang/String; = "GiftCardLookupCoordinator"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    .line 35
    invoke-virtual {v0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v0

    const-string v1, "com.squareup.Card.Builder()\n    .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->EMPTY_CARD:Lcom/squareup/Card;

    return-void
.end method

.method public static synthetic ERRORS_BAR_TAG$annotations()V
    .locals 0

    return-void
.end method

.method public static final getEMPTY_CARD()Lcom/squareup/Card;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->EMPTY_CARD:Lcom/squareup/Card;

    return-object v0
.end method
