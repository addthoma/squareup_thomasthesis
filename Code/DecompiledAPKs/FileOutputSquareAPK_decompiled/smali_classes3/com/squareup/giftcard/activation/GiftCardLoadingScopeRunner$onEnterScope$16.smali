.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;)V
    .locals 8

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getTransaction$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCardItemizer$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardItemizer;

    move-result-object v2

    .line 339
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;->getEGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getIdPair$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/protos/client/IdPair;

    move-result-object v6

    const-string p1, "idPair"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x1

    .line 338
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/giftcard/activation/GiftCardItemizer;->createGiftCardItemization(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;Lcom/squareup/protos/client/IdPair;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 337
    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    .line 342
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getFlow$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lflow/Flow;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$goBackPastChooseTypeScreen(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lflow/Flow;)V

    goto :goto_0

    .line 344
    :cond_0
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getFlow$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lflow/Flow;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$goBackUntilChooseTypeScreen(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lflow/Flow;)V

    :goto_0
    return-void
.end method
