.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/Card;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/Card;)V
    .locals 4

    .line 237
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getChooseTypeBusy$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 238
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getAutoAdvanceOnSwipe$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/util/RxWatchdog;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {p1, v0, v2, v3, v1}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;->call(Lcom/squareup/Card;)V

    return-void
.end method
