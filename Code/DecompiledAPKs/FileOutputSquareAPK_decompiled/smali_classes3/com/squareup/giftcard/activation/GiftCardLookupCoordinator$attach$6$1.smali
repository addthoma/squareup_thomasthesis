.class final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/Card;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "swipedCard",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/Card;)V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/GiftCards;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    const-string v1, "GiftCardLookupCoordinator"

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 162
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object v0

    const-string v1, "swipedCard"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->setCard(Lcom/squareup/Card;)V

    goto :goto_0

    .line 164
    :cond_0
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    .line 166
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v2, Lcom/squareup/giftcard/activation/R$string;->gift_card_unsupported_message:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object p1

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->setCard(Lcom/squareup/Card;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6$1;->call(Lcom/squareup/Card;)V

    return-void
.end method
