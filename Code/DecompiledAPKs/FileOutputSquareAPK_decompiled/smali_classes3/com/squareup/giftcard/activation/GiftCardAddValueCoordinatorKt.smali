.class public final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinatorKt;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "MAX_PRESET_STRING_LENGTH",
        "",
        "getActivityType",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
        "state",
        "Lcom/squareup/protos/client/giftcards/GiftCard$State;",
        "giftcard-activation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MAX_PRESET_STRING_LENGTH:I = 0x6


# direct methods
.method public static final synthetic access$getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinatorKt;->getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    move-result-object p0

    return-object p0
.end method

.method private static final getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 1

    .line 254
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard$State;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 257
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    goto :goto_0

    .line 259
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected gift card state!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 255
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    :goto_0
    return-object p0
.end method
