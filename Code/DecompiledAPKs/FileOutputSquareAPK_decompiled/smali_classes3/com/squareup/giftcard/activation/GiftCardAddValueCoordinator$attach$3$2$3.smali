.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $customAmount:Lrx/observables/ConnectableObservable;

.field final synthetic $giftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;Lrx/observables/ConnectableObservable;Lcom/squareup/protos/client/giftcards/GiftCard;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->$customAmount:Lrx/observables/ConnectableObservable;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->$giftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customAmountButton:Lcom/squareup/marketfont/MarketButton;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->$customAmount:Lrx/observables/ConnectableObservable;

    check-cast v1, Lrx/Observable;

    sget-object v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$1;

    check-cast v2, Lrx/functions/Func2;

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 190
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "customAmountButton.debou\u2026                        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
