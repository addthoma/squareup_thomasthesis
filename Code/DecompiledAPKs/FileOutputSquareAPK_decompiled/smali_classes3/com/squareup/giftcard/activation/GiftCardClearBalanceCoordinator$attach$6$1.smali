.class final Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6$1;
.super Ljava/lang/Object;
.source "GiftCardClearBalanceCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "visible",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;->$progressBar:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/view/View;

    const-string/jumbo v1, "visible"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6$1;->call(Ljava/lang/Boolean;)V

    return-void
.end method
