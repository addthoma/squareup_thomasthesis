.class final Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;
.super Ljava/lang/Object;
.source "GiftCardClearBalanceCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 79
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    const-string v1, "response.status.success"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;->finishClearBalanceAndCloseActivationFlow()V

    goto :goto_2

    :cond_0
    if-eqz p1, :cond_1

    .line 82
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 83
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    goto :goto_1

    .line 85
    :cond_2
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/giftcard/activation/R$string;->gift_card_clear_balance_failure:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 88
    :goto_1
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7$1;->call(Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;)V

    return-void
.end method
