.class public interface abstract Lcom/squareup/connectivity/ConnectivityMonitor;
.super Ljava/lang/Object;
.source "ConnectivityMonitor.java"


# virtual methods
.method public abstract internetState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isConnected()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
