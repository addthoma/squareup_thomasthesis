.class public final Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;
.super Ljava/lang/Object;
.source "RealConnectivityMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/connectivity/RealConnectivityMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->connectivityManagerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;)",
            "Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Landroid/net/ConnectivityManager;Lcom/squareup/internet/InternetStatusMonitor;)Lcom/squareup/connectivity/RealConnectivityMonitor;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/connectivity/RealConnectivityMonitor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/connectivity/RealConnectivityMonitor;-><init>(Landroid/app/Application;Landroid/net/ConnectivityManager;Lcom/squareup/internet/InternetStatusMonitor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/connectivity/RealConnectivityMonitor;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->connectivityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {v0, v1, v2}, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->newInstance(Landroid/app/Application;Landroid/net/ConnectivityManager;Lcom/squareup/internet/InternetStatusMonitor;)Lcom/squareup/connectivity/RealConnectivityMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->get()Lcom/squareup/connectivity/RealConnectivityMonitor;

    move-result-object v0

    return-object v0
.end method
