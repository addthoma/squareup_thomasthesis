.class public final Lcom/squareup/http/useragent/UserAgentProvider;
.super Ljava/lang/Object;
.source "UserAgentProvider.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUserAgentProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UserAgentProvider.kt\ncom/squareup/http/useragent/UserAgentProvider\n*L\n1#1,37:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u00020\u0001BO\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fR\u000e\u0010\u000c\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00038FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/http/useragent/UserAgentProvider;",
        "",
        "userAgentId",
        "",
        "versionName",
        "productVersionName",
        "readerSdkBucketBinIdProvider",
        "Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;",
        "environmentDiscovery",
        "Lcom/squareup/http/useragent/EnvironmentDiscovery;",
        "locale",
        "Ljava/util/Locale;",
        "deviceInformation",
        "posBuild",
        "Lcom/squareup/util/PosBuild;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;Lcom/squareup/http/useragent/EnvironmentDiscovery;Ljava/util/Locale;Ljava/lang/String;Lcom/squareup/util/PosBuild;)V",
        "userAgentString",
        "getUserAgentString",
        "()Ljava/lang/String;",
        "userAgentString$delegate",
        "Lkotlin/Lazy;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deviceInformation:Ljava/lang/String;

.field private final environmentDiscovery:Lcom/squareup/http/useragent/EnvironmentDiscovery;

.field private final locale:Ljava/util/Locale;

.field private final productVersionName:Ljava/lang/String;

.field private final readerSdkBucketBinIdProvider:Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;

.field private final userAgentId:Ljava/lang/String;

.field private final userAgentString$delegate:Lkotlin/Lazy;

.field private final versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;Lcom/squareup/http/useragent/EnvironmentDiscovery;Ljava/util/Locale;Ljava/lang/String;Lcom/squareup/util/PosBuild;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/http/useragent/UserAgentId;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/util/PosSdkVersionName;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/util/ProductVersionName;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/http/DeviceInformation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "userAgentId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "versionName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "productVersionName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerSdkBucketBinIdProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "environmentDiscovery"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceInformation"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "posBuild"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/http/useragent/UserAgentProvider;->userAgentId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/http/useragent/UserAgentProvider;->versionName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/http/useragent/UserAgentProvider;->productVersionName:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/http/useragent/UserAgentProvider;->readerSdkBucketBinIdProvider:Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;

    iput-object p5, p0, Lcom/squareup/http/useragent/UserAgentProvider;->environmentDiscovery:Lcom/squareup/http/useragent/EnvironmentDiscovery;

    iput-object p6, p0, Lcom/squareup/http/useragent/UserAgentProvider;->locale:Ljava/util/Locale;

    iput-object p7, p0, Lcom/squareup/http/useragent/UserAgentProvider;->deviceInformation:Ljava/lang/String;

    .line 25
    new-instance p1, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;

    invoke-direct {p1, p0, p8}, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;-><init>(Lcom/squareup/http/useragent/UserAgentProvider;Lcom/squareup/util/PosBuild;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/http/useragent/UserAgentProvider;->userAgentString$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getDeviceInformation$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->deviceInformation:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getEnvironmentDiscovery$p(Lcom/squareup/http/useragent/UserAgentProvider;)Lcom/squareup/http/useragent/EnvironmentDiscovery;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->environmentDiscovery:Lcom/squareup/http/useragent/EnvironmentDiscovery;

    return-object p0
.end method

.method public static final synthetic access$getLocale$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/util/Locale;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method public static final synthetic access$getProductVersionName$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->productVersionName:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getReaderSdkBucketBinIdProvider$p(Lcom/squareup/http/useragent/UserAgentProvider;)Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->readerSdkBucketBinIdProvider:Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;

    return-object p0
.end method

.method public static final synthetic access$getUserAgentId$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->userAgentId:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getVersionName$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->versionName:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final getUserAgentString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/http/useragent/UserAgentProvider;->userAgentString$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
