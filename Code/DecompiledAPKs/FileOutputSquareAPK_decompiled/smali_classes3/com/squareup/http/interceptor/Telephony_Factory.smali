.class public final Lcom/squareup/http/interceptor/Telephony_Factory;
.super Ljava/lang/Object;
.source "Telephony_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/Telephony;",
        ">;"
    }
.end annotation


# instance fields
.field private final telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/http/interceptor/Telephony_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/Telephony_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;)",
            "Lcom/squareup/http/interceptor/Telephony_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/http/interceptor/Telephony_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/Telephony_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/telephony/TelephonyManager;)Lcom/squareup/http/interceptor/Telephony;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/http/interceptor/Telephony;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/Telephony;-><init>(Landroid/telephony/TelephonyManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/Telephony;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/http/interceptor/Telephony_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-static {v0}, Lcom/squareup/http/interceptor/Telephony_Factory;->newInstance(Landroid/telephony/TelephonyManager;)Lcom/squareup/http/interceptor/Telephony;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/Telephony_Factory;->get()Lcom/squareup/http/interceptor/Telephony;

    move-result-object v0

    return-object v0
.end method
