.class public final Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;
.super Ljava/lang/Object;
.source "SpeleoIdGenerator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/SpeleoIdGenerator;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory$InstanceHolder;->access$000()Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/http/interceptor/SpeleoIdGenerator;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    invoke-direct {v0}, Lcom/squareup/http/interceptor/SpeleoIdGenerator;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/SpeleoIdGenerator;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;->newInstance()Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;->get()Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    move-result-object v0

    return-object v0
.end method
