.class public Lcom/squareup/http/interceptor/RegisterHttpInterceptor;
.super Ljava/lang/Object;
.source "RegisterHttpInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

.field private final telephony:Lcom/squareup/http/interceptor/Telephony;


# direct methods
.method public constructor <init>(Lcom/squareup/http/interceptor/RealSquareHeaders;Lcom/squareup/http/interceptor/Telephony;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    .line 27
    iput-object p2, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->telephony:Lcom/squareup/http/interceptor/Telephony;

    return-void
.end method

.method private static setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 65
    invoke-virtual {p0, p1, p2}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    :cond_0
    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 31
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    .line 35
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    if-lt v1, v2, :cond_0

    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    iget-object v1, v1, Lcom/squareup/http/interceptor/RealSquareHeaders;->deviceId:Lcom/squareup/settings/DeviceIdProvider;

    invoke-interface {v1}, Lcom/squareup/settings/DeviceIdProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 37
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "X-Device-ID"

    .line 40
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/RealSquareHeaders;->createMutableHeaderList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Header;

    .line 45
    iget-object v3, v2, Lcom/squareup/protos/common/Header;->name:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/protos/common/Header;->value:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    goto :goto_0

    .line 49
    :cond_2
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/RealSquareHeaders;->getEncodedSim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-SIM"

    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 50
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/RealSquareHeaders;->sanitizeNetworkOperator()Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-Network-Operator"

    invoke-static {v0, v2, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->telephony:Lcom/squareup/http/interceptor/Telephony;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/Telephony;->getNetworkType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-Network-Type"

    invoke-static {v0, v2, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    iget-object v1, v1, Lcom/squareup/http/interceptor/RealSquareHeaders;->connectivityProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-static {v1}, Lcom/squareup/util/ConnectivityKt;->provideNetworkConnectivity(Landroid/net/ConnectivityManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-Connectivity"

    .line 53
    invoke-static {v0, v2, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/RealSquareHeaders;->sanitizeDeviceName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-Device-Name"

    invoke-static {v0, v2, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->headers:Lcom/squareup/http/interceptor/RealSquareHeaders;

    invoke-virtual {v1}, Lcom/squareup/http/interceptor/RealSquareHeaders;->getCellLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "X-Cell-Location"

    invoke-static {v0, v2, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;->setIfNotNull(Lokhttp3/Request$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object p1

    return-object p1

    .line 38
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Device ID is required."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
