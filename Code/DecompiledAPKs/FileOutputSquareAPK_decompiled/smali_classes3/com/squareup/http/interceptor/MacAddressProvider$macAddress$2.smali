.class final Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;
.super Lkotlin/jvm/internal/Lambda;
.source "MacAddressProvider.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/http/interceptor/MacAddressProvider;-><init>(Landroid/net/wifi/WifiManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/http/interceptor/MacAddressProvider;


# direct methods
.method constructor <init>(Lcom/squareup/http/interceptor/MacAddressProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;->this$0:Lcom/squareup/http/interceptor/MacAddressProvider;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;->invoke()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/lang/String;
    .locals 1

    .line 16
    :try_start_0
    iget-object v0, p0, Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;->this$0:Lcom/squareup/http/interceptor/MacAddressProvider;

    invoke-static {v0}, Lcom/squareup/http/interceptor/MacAddressProvider;->access$getWifiManager$p(Lcom/squareup/http/interceptor/MacAddressProvider;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    const-string/jumbo v0, "unavailable"

    :goto_0
    return-object v0
.end method
