.class public final Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;
.super Ljava/lang/Object;
.source "RegisterHttpInterceptor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/RegisterHttpInterceptor;",
        ">;"
    }
.end annotation


# instance fields
.field private final headersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/RealSquareHeaders;",
            ">;"
        }
    .end annotation
.end field

.field private final telephonyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/RealSquareHeaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->headersProvider:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->telephonyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/RealSquareHeaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;)",
            "Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/http/interceptor/RealSquareHeaders;Ljava/lang/Object;)Lcom/squareup/http/interceptor/RegisterHttpInterceptor;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;

    check-cast p1, Lcom/squareup/http/interceptor/Telephony;

    invoke-direct {v0, p0, p1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor;-><init>(Lcom/squareup/http/interceptor/RealSquareHeaders;Lcom/squareup/http/interceptor/Telephony;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/RegisterHttpInterceptor;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->headersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/interceptor/RealSquareHeaders;

    iget-object v1, p0, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->telephonyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->newInstance(Lcom/squareup/http/interceptor/RealSquareHeaders;Ljava/lang/Object;)Lcom/squareup/http/interceptor/RegisterHttpInterceptor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->get()Lcom/squareup/http/interceptor/RegisterHttpInterceptor;

    move-result-object v0

    return-object v0
.end method
