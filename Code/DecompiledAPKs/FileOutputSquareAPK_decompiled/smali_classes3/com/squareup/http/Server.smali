.class public Lcom/squareup/http/Server;
.super Ljava/lang/Object;
.source "Server.java"


# static fields
.field private static final DEFAULT_NAME:Ljava/lang/String; = "default"


# instance fields
.field private final name:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "default"

    .line 10
    invoke-direct {p0, p1, v0}, Lcom/squareup/http/Server;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/http/Server;->url:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/squareup/http/Server;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/http/Server;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/http/Server;->url:Ljava/lang/String;

    return-object v0
.end method

.method public isProduction()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
