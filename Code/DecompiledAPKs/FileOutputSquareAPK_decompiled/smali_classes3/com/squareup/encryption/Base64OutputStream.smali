.class public Lcom/squareup/encryption/Base64OutputStream;
.super Ljava/io/FilterOutputStream;
.source "Base64OutputStream.java"


# static fields
.field private static EMPTY:[B


# instance fields
.field private bpos:I

.field private buffer:[B

.field private final flags:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 26
    sput-object v0, Lcom/squareup/encryption/Base64OutputStream;->EMPTY:[B

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    .line 23
    iput-object p1, p0, Lcom/squareup/encryption/Base64OutputStream;->buffer:[B

    const/4 p1, 0x0

    .line 24
    iput p1, p0, Lcom/squareup/encryption/Base64OutputStream;->bpos:I

    .line 40
    iput p2, p0, Lcom/squareup/encryption/Base64OutputStream;->flags:I

    return-void
.end method

.method private flushBuffer()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 48
    iget v0, p0, Lcom/squareup/encryption/Base64OutputStream;->bpos:I

    if-lez v0, :cond_0

    .line 49
    iget-object v1, p0, Lcom/squareup/encryption/Base64OutputStream;->buffer:[B

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/encryption/Base64OutputStream;->internalWrite([BII)V

    .line 50
    iput v2, p0, Lcom/squareup/encryption/Base64OutputStream;->bpos:I

    :cond_0
    return-void
.end method

.method private internalWrite([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    iget v0, p0, Lcom/squareup/encryption/Base64OutputStream;->flags:I

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/util/Base64;->encode([BIII)[B

    move-result-object p1

    .line 95
    iget-object p2, p0, Lcom/squareup/encryption/Base64OutputStream;->out:Ljava/io/OutputStream;

    array-length p3, p1

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0, p3}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method private writeFinal([BII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-gtz p3, :cond_0

    return-void

    .line 60
    :cond_0
    invoke-direct {p0}, Lcom/squareup/encryption/Base64OutputStream;->flushBuffer()V

    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/encryption/Base64OutputStream;->internalWrite([BII)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 67
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/encryption/Base64OutputStream;->flushBuffer()V

    .line 68
    sget-object v0, Lcom/squareup/encryption/Base64OutputStream;->EMPTY:[B

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/squareup/encryption/Base64OutputStream;->internalWrite([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 74
    :goto_0
    :try_start_1
    iget v1, p0, Lcom/squareup/encryption/Base64OutputStream;->flags:I

    and-int/lit8 v1, v1, 0x10

    if-nez v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/squareup/encryption/Base64OutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/squareup/encryption/Base64OutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    if-eqz v0, :cond_1

    move-object v0, v1

    :cond_1
    :goto_1
    if-nez v0, :cond_2

    return-void

    .line 86
    :cond_2
    throw v0
.end method

.method public writeFinal([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    array-length v0, p1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([BII)V

    return-void
.end method
