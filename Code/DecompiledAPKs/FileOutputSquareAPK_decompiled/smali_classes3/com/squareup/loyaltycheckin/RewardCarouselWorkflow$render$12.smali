.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->render(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "+",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    invoke-static {p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getGoBackToRewardCarouselAction$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
