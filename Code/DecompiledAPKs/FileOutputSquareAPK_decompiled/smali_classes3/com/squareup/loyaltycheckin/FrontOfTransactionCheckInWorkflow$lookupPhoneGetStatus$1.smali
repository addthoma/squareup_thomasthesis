.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->lookupPhoneGetStatus(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "it",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contact:Lcom/squareup/protos/client/rolodex/Contact;

.field final synthetic $state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0, p1, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$checkedInDataOf(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$showWelcomeScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 238
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    invoke-static {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getShowNewLoyaltyAccount$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 239
    :cond_1
    sget-object v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;

    invoke-static {p1, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$showErrorLookingUpPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;->apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
