.class public abstract Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "CheckInAnalyticEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$CheckIn;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$SeeRewards;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$CancelPhoneEntry;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorEnteringPhone;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewNewAccount;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewRewardCarousel;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$RedeemReward;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorRedeemingReward;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ExitRewardCarousel;,
        Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewCartDiff;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\n\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u0082\u0001\n\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "type",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "path",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V",
        "CancelPhoneEntry",
        "CheckIn",
        "ErrorEnteringPhone",
        "ErrorRedeemingReward",
        "ExitRewardCarousel",
        "RedeemReward",
        "SeeRewards",
        "ViewCartDiff",
        "ViewNewAccount",
        "ViewRewardCarousel",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$CheckIn;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$SeeRewards;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$CancelPhoneEntry;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorEnteringPhone;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewNewAccount;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewRewardCarousel;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$RedeemReward;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorRedeemingReward;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ExitRewardCarousel;",
        "Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewCartDiff;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 2

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loyalty X2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
