.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;
.super Ljava/lang/Object;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->redeemReward(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0015\n\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0001\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "com/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "apply",
        "(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

.field final synthetic $tier:Lcom/squareup/loyaltycheckin/Tier;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/Tier;Lcom/squareup/loyaltycheckin/RewardCarouselProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->$tier:Lcom/squareup/loyaltycheckin/Tier;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->$props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;

    move-result-object p1

    return-object p1
.end method
