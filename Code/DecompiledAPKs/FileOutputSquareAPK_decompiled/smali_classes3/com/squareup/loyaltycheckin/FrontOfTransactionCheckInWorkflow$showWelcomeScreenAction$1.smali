.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showWelcomeScreenAction(Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Mutator<",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        ">;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/loyaltycheckin/CheckedInData;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/CheckedInData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;->$data:Lcom/squareup/loyaltycheckin/CheckedInData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;->$data:Lcom/squareup/loyaltycheckin/CheckedInData;

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;-><init>(Lcom/squareup/loyaltycheckin/CheckedInData;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    .line 263
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;->$data:Lcom/squareup/loyaltycheckin/CheckedInData;

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/CheckedInData;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;-><init>(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ContactCheckedIn;

    move-result-object p1

    return-object p1
.end method
