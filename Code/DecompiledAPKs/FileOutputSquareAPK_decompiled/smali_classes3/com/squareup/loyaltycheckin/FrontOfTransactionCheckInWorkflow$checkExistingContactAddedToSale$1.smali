.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->checkExistingContactAddedToSale(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "it",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    invoke-static {v0, p1, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$checkedInDataOf(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 198
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    invoke-static {p1, v2, v1, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showEnteringPhone$default(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 199
    :cond_1
    sget-object v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;->apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
