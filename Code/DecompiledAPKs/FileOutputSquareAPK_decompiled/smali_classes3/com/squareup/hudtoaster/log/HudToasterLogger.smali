.class public final Lcom/squareup/hudtoaster/log/HudToasterLogger;
.super Ljava/lang/Object;
.source "HudToasterLogger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/hudtoaster/log/HudToasterLogger$HudToasterEvent;,
        Lcom/squareup/hudtoaster/log/HudToasterLogger$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u0000 \u00132\u00020\u0001:\u0002\u0013\u0014B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u000e\u0010\u0008\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\u0006J\u000e\u0010\u000c\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nJ!\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000f2\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0002\u00a2\u0006\u0002\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/hudtoaster/log/HudToasterLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logCancelNullToast",
        "",
        "logCancelNullToastRef",
        "logCancelToast",
        "toast",
        "Landroid/widget/Toast;",
        "logDisplayToastFailed",
        "logDisplayToastSuccess",
        "logEvent",
        "eventValue",
        "",
        "toastIdentifier",
        "",
        "(Ljava/lang/String;Ljava/lang/Integer;)V",
        "Companion",
        "HudToasterEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/hudtoaster/log/HudToasterLogger$Companion;

.field private static final TOAST_CANCELED:Ljava/lang/String; = "Toast Canceled"

.field private static final TOAST_CANCELED_NULL_TOAST:Ljava/lang/String; = "Toast Canceled Null Toast"

.field private static final TOAST_CANCELED_NULL_TOAST_REF:Ljava/lang/String; = "Toast Canceled Null Toast Ref"

.field private static final TOAST_START_FAILED:Ljava/lang/String; = "Toast Start Failed"

.field private static final TOAST_START_SUCCESS:Ljava/lang/String; = "Toast Start Success"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/hudtoaster/log/HudToasterLogger$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/hudtoaster/log/HudToasterLogger$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/hudtoaster/log/HudToasterLogger;->Companion:Lcom/squareup/hudtoaster/log/HudToasterLogger$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/hudtoaster/log/HudToasterLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final logEvent(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/hudtoaster/log/HudToasterLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 52
    new-instance v1, Lcom/squareup/hudtoaster/log/HudToasterLogger$HudToasterEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/hudtoaster/log/HudToasterLogger$HudToasterEvent;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 51
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method static synthetic logEvent$default(Lcom/squareup/hudtoaster/log/HudToasterLogger;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 49
    check-cast p2, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public final logCancelNullToast()V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "Toast Canceled Null Toast"

    const/4 v2, 0x2

    .line 40
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent$default(Lcom/squareup/hudtoaster/log/HudToasterLogger;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final logCancelNullToastRef()V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "Toast Canceled Null Toast Ref"

    const/4 v2, 0x2

    .line 44
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent$default(Lcom/squareup/hudtoaster/log/HudToasterLogger;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final logCancelToast(Landroid/widget/Toast;)V
    .locals 1

    const-string/jumbo v0, "toast"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Landroid/widget/Toast;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "Toast Canceled"

    .line 33
    invoke-direct {p0, v0, p1}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public final logDisplayToastFailed()V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "Toast Start Failed"

    const/4 v2, 0x2

    .line 29
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent$default(Lcom/squareup/hudtoaster/log/HudToasterLogger;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final logDisplayToastSuccess(Landroid/widget/Toast;)V
    .locals 1

    const-string/jumbo v0, "toast"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Landroid/widget/Toast;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "Toast Start Success"

    .line 22
    invoke-direct {p0, v0, p1}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logEvent(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method
