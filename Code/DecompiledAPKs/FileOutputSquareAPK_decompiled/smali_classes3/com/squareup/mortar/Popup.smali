.class public interface abstract Lcom/squareup/mortar/Popup;
.super Ljava/lang/Object;
.source "Popup.java"

# interfaces
.implements Lcom/squareup/mortar/HasContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/mortar/HasContext;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract dismiss()V
.end method

.method public abstract isShowing()Z
.end method

.method public abstract show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "TD;TR;>;)V"
        }
    .end annotation
.end method
