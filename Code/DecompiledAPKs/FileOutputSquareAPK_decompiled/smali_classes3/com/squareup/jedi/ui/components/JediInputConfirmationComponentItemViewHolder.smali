.class public Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediInputConfirmationComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 15
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_input_confirmation_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 21
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->label:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 22
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;->label()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->text:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;->text()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
