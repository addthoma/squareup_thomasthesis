.class public final enum Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
.super Ljava/lang/Enum;
.source "JediButtonBaseComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

.field public static final enum PRIMARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

.field public static final enum SECONDARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

.field public static final enum TERTIARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 14
    new-instance v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    const/4 v1, 0x0

    const-string v2, "PRIMARY"

    const-string v3, "primary"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->PRIMARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    .line 15
    new-instance v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    const/4 v2, 0x1

    const-string v3, "SECONDARY"

    const-string v4, "secondary"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->SECONDARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    .line 16
    new-instance v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    const/4 v3, 0x2

    const-string v4, "TERTIARY"

    const-string v5, "tertiary"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->TERTIARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    .line 13
    sget-object v4, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->PRIMARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->SECONDARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->TERTIARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->value:Ljava/lang/String;

    return-void
.end method

.method static fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
    .locals 5

    .line 26
    invoke-static {}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->values()[Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 27
    iget-object v4, v3, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    invoke-virtual {v0}, [Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    return-object v0
.end method
