.class public final Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "JediPanelView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/ui/JediPanelView;->maybeShowSearchBar(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jedi/ui/JediPanelView;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/ui/JediPanelView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 176
    iput-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->clearFocus()V

    .line 180
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    const-string v0, "jediSearchBar.editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getSearchClearRelay$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
