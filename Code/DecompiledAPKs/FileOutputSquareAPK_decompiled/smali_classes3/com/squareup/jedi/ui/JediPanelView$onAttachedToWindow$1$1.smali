.class final Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;
.super Ljava/lang/Object;
.source "JediPanelView.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 107
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v1, v1, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {v1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    instance-of v2, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    invoke-static {v0, v1, v2}, Lcom/squareup/jedi/ui/JediPanelView;->access$fadeInOrOut(Lcom/squareup/jedi/ui/JediPanelView;Landroid/view/View;Z)V

    .line 110
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v1, v1, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {v1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediProgressBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Landroid/widget/ProgressBar;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    instance-of v3, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;

    invoke-static {v0, v1, v3}, Lcom/squareup/jedi/ui/JediPanelView;->access$fadeInOrOut(Lcom/squareup/jedi/ui/JediPanelView;Landroid/view/View;Z)V

    .line 111
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v1, v1, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {v1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediErrorView$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/EmptyView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    instance-of v3, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    invoke-static {v0, v1, v3}, Lcom/squareup/jedi/ui/JediPanelView;->access$fadeInOrOut(Lcom/squareup/jedi/ui/JediPanelView;Landroid/view/View;Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v1, v1, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {v1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getComponentsRecyclerView$p(Lcom/squareup/jedi/ui/JediPanelView;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/squareup/jedi/ui/JediPanelView;->access$fadeInOrOut(Lcom/squareup/jedi/ui/JediPanelView;Landroid/view/View;Z)V

    .line 114
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    const-string v1, "screenData"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$maybeShowSuccess(Lcom/squareup/jedi/ui/JediPanelView;Lcom/squareup/jedi/JediHelpScreenData;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {v0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$maybeShowError(Lcom/squareup/jedi/ui/JediPanelView;Lcom/squareup/jedi/JediHelpScreenData;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 68
    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1$1;->accept(Lcom/squareup/jedi/JediHelpScreenData;)V

    return-void
.end method
