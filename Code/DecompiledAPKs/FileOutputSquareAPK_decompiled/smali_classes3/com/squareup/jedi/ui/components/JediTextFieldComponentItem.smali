.class public Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediTextFieldComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method


# virtual methods
.method hasError()Z
    .locals 2

    const/4 v0, 0x0

    .line 68
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "error_state"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 52
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public minLength()I
    .locals 3

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$rGLruuLUyn8KVWMknvn8vgDl5YI;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$rGLruuLUyn8KVWMknvn8vgDl5YI;

    const-string v2, "min_length"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method multiline()Z
    .locals 2

    const/4 v0, 0x0

    .line 64
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "multiline"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method placeholder()Ljava/lang/String;
    .locals 1

    const-string v0, "placeholder"

    .line 56
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public required()Z
    .locals 2

    const/4 v0, 0x0

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "required"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method text()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    .line 48
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method type()Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;
    .locals 3

    .line 60
    sget-object v0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;->TEXT:Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$op2bBx6dc_kW4hGxLqrWbo6HAVU;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$op2bBx6dc_kW4hGxLqrWbo6HAVU;

    const-string/jumbo v2, "type"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    return-object v0
.end method
