.class public abstract Lcom/squareup/jedi/JediWorkflowState;
.super Ljava/lang/Object;
.source "JediWorkflowState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediWorkflowState$UninitializedState;,
        Lcom/squareup/jedi/JediWorkflowState$InLoadingState;,
        Lcom/squareup/jedi/JediWorkflowState$InShowingState;,
        Lcom/squareup/jedi/JediWorkflowState$InSearchingState;,
        Lcom/squareup/jedi/JediWorkflowState$InErrorState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0005\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Landroid/os/Parcelable;",
        "()V",
        "currentScreenData",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "getCurrentScreenData",
        "()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "screenDataStack",
        "",
        "getScreenDataStack",
        "()Ljava/util/List;",
        "InErrorState",
        "InLoadingState",
        "InSearchingState",
        "InShowingState",
        "UninitializedState",
        "Lcom/squareup/jedi/JediWorkflowState$UninitializedState;",
        "Lcom/squareup/jedi/JediWorkflowState$InLoadingState;",
        "Lcom/squareup/jedi/JediWorkflowState$InShowingState;",
        "Lcom/squareup/jedi/JediWorkflowState$InSearchingState;",
        "Lcom/squareup/jedi/JediWorkflowState$InErrorState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/jedi/JediWorkflowState;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCurrentScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-object v0
.end method

.method public abstract getScreenDataStack()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
            ">;"
        }
    .end annotation
.end method
