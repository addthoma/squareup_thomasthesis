.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;
.super Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.source "SelectSingleOptionValueForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TapOptionValue"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue\n*L\n1#1,211:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J#\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;",
        "value",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "valuesUsedByExistingVariations",
        "",
        "(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V",
        "getValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "getValuesUsedByExistingVariations",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

.field private final valuesUsedByExistingVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valuesUsedByExistingVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 179
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    if-eqz v0, :cond_2

    .line 183
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    .line 184
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->gotoDuplicateVariationState(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 187
    :cond_0
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 183
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.addsinglevariation.SelectSingleOptionValueForVariationState.SelectOptionValueState"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 182
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valuesUsedByExistingVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final getValuesUsedByExistingVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TapOptionValue(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->value:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", valuesUsedByExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;->valuesUsedByExistingVariations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
