.class final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesForVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->render(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;",
        "it",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$NoOp;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 83
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p1

    .line 85
    :goto_0
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    invoke-direct {v0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    return-object v0

    .line 83
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;->invoke(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    move-result-object p1

    return-object p1
.end method
