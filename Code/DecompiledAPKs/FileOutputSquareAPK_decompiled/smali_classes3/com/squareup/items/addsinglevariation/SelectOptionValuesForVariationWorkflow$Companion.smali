.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;
.super Ljava/lang/Object;
.source "SelectOptionValuesForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,322:1\n1360#2:323\n1429#2,3:324\n1642#2:327\n1642#2,2:328\n1643#2:330\n704#2:331\n777#2,2:332\n704#2:334\n777#2,2:335\n1529#2,3:337\n704#2:340\n777#2,2:341\n1550#2,3:343\n1360#2:346\n1429#2,3:347\n704#2:350\n777#2,2:351\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion\n*L\n153#1:323\n153#1,3:324\n155#1:327\n155#1,2:328\n155#1:330\n166#1:331\n166#1,2:332\n185#1:334\n185#1,2:335\n203#1,3:337\n211#1:340\n211#1,2:341\n222#1,3:343\n233#1:346\n233#1,3:347\n236#1:350\n236#1,2:351\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0008\u0008\u0008\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0002J(\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\n2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010J.\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\n2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006J8\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00080\n2\u0006\u0010\u0014\u001a\u00020\u000e2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0002J$\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006H\u0002J*\u0010\u0017\u001a\u00020\u00042\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\n2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006H\u0002\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;",
        "",
        "()V",
        "doesValueSelectionsMatchAnyValueCombination",
        "",
        "selectedValuesByOptionIds",
        "",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "optionValueCombinations",
        "",
        "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
        "findAllAvailableItemOptionValueCombinationsWithUsedOptionValues",
        "options",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "usedOptionValueCombinations",
        "",
        "findNewOptionValues",
        "assignedOptions",
        "findValueSelectionsMatchingValueCombinations",
        "option",
        "isOptionValueCombinationSelected",
        "optionValueCombination",
        "shouldEnableCreateCustomVariationButton",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$doesValueSelectionsMatchAnyValueCombination(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Ljava/util/Map;Ljava/util/List;)Z
    .locals 0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->doesValueSelectionsMatchAnyValueCombination(Ljava/util/Map;Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$findValueSelectionsMatchingValueCombinations(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/Map;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->findValueSelectionsMatchingValueCombinations(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/Map;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldEnableCreateCustomVariationButton(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Ljava/util/List;Ljava/util/Map;)Z
    .locals 0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->shouldEnableCreateCustomVariationButton(Ljava/util/List;Ljava/util/Map;)Z

    move-result p0

    return p0
.end method

.method private final doesValueSelectionsMatchAnyValueCombination(Ljava/util/Map;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;)Z"
        }
    .end annotation

    .line 222
    check-cast p2, Ljava/lang/Iterable;

    .line 343
    instance-of v0, p2, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 344
    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 223
    sget-object v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    invoke-direct {v2, v0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->isOptionValueCombinationSelected(Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method

.method private final findValueSelectionsMatchingValueCombinations(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/Map;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 211
    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 340
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 341
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 212
    invoke-static {p2}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 213
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    invoke-direct {v2, v3, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->doesValueSelectionsMatchAnyValueCombination(Ljava/util/Map;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 342
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final isOptionValueCombinationSelected(Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;Ljava/util/Map;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)Z"
        }
    .end annotation

    .line 192
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;->getValues()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 193
    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    return v1
.end method

.method private final shouldEnableCreateCustomVariationButton(Ljava/util/List;Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)Z"
        }
    .end annotation

    .line 203
    check-cast p1, Ljava/lang/Iterable;

    .line 337
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 338
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 204
    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method


# virtual methods
.method public final findAllAvailableItemOptionValueCombinationsWithUsedOptionValues(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;"
        }
    .end annotation

    const-string v0, "options"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usedOptionValueCombinations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 323
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 324
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 325
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 153
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v3, Ljava/util/Set;

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 154
    invoke-static {v1}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    .line 155
    move-object v1, p2

    check-cast v1, Ljava/lang/Iterable;

    .line 327
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 156
    invoke-virtual {v2}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;->getValues()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 328
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 157
    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v4, Ljava/util/Set;

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 161
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 164
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-ltz v2, :cond_a

    .line 165
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 166
    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 331
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 332
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 167
    invoke-virtual {v6}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    check-cast v7, Ljava/util/Set;

    invoke-virtual {v6}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 333
    :cond_6
    check-cast v4, Ljava/util/List;

    .line 170
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/List;

    .line 171
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 172
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 173
    new-instance v6, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v6, v5}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;-><init>(Ljava/util/List;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 175
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 176
    new-instance v8, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 177
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    check-cast v9, Ljava/util/Collection;

    invoke-virtual {v7}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;->getValues()Ljava/util/List;

    move-result-object v7

    check-cast v7, Ljava/lang/Iterable;

    invoke-static {v9, v7}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v7

    .line 176
    invoke-direct {v8, v7}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;-><init>(Ljava/util/List;)V

    .line 179
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    add-int/lit8 v2, v2, -0x1

    move-object v1, v3

    goto/16 :goto_2

    .line 185
    :cond_a
    check-cast v1, Ljava/lang/Iterable;

    .line 334
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 335
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_b
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 185
    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_b

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 336
    :cond_c
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method public final findNewOptionValues(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    const-string v0, "assignedOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedValuesByOptionIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 231
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 232
    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 346
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 347
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 348
    check-cast v4, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 233
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 349
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 234
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    .line 232
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 236
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 350
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 351
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 237
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast v3, Ljava/util/Set;

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 352
    :cond_4
    check-cast p2, Ljava/util/List;

    return-object p2
.end method
