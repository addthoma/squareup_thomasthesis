.class public abstract Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsModule;
.super Ljava/lang/Object;
.source "AddSingleVariationWithOptionsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsModule;",
        "",
        "()V",
        "bindViewFactory",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;",
        "viewFactory",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsViewFactory;",
        "bindWorkflow",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;",
        "feature",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;",
        "bindWorkflowRunner",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;",
        "runner",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindViewFactory(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsViewFactory;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindWorkflow(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindWorkflowRunner(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
