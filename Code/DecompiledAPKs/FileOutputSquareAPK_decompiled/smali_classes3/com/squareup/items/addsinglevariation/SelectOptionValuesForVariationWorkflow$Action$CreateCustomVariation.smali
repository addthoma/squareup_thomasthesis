.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;
.super Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;
.source "SelectOptionValuesForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateCustomVariation"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,322:1\n1360#2:323\n1429#2,3:324\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation\n*L\n288#1:323\n288#1,3:324\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005H\u00c6\u0003J3\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u001a\u001a\u00020\u001b*\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001cH\u0016R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;",
        "itemCogsId",
        "",
        "assignedOptions",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "usedOptionValueCombinations",
        "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
        "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V",
        "getAssignedOptions",
        "()Ljava/util/List;",
        "getItemCogsId",
        "()Ljava/lang/String;",
        "getUsedOptionValueCombinations",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignedOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final itemCogsId:Ljava/lang/String;

.field private final usedOptionValueCombinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;)V"
        }
    .end annotation

    const-string v0, "itemCogsId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignedOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usedOptionValueCombinations"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 278
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->copy(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    sget-object v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    .line 282
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v1

    .line 283
    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    .line 281
    invoke-static {v0, v1, v2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->access$doesValueSelectionsMatchAnyValueCombination(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Ljava/util/Map;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$DuplicateVariation;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$DuplicateVariation;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 323
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 324
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 325
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 290
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 292
    new-instance v0, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;-><init>(Ljava/util/List;)V

    .line 293
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;->buildNewVariation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    .line 294
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    .line 295
    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    .line 296
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v3

    .line 294
    invoke-virtual {v1, v2, v3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->findNewOptionValues(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .line 298
    new-instance v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    invoke-direct {v2, v0, v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;"
        }
    .end annotation

    const-string v0, "itemCogsId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignedOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usedOptionValueCombinations"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignedOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation

    .line 276
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    return-object v0
.end method

.method public final getItemCogsId()Ljava/lang/String;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    return-object v0
.end method

.method public final getUsedOptionValueCombinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 277
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateCustomVariation(itemCogsId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->itemCogsId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", assignedOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->assignedOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", usedOptionValueCombinations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;->usedOptionValueCombinations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
