.class final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesForVariationLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$OptionWithSelectedValueRow;",
        "Lcom/squareup/noho/NohoRow;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1\n+ 2 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,217:1\n64#2,2:218\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1\n*L\n69#1,2:218\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$OptionWithSelectedValueRow;",
        "Lcom/squareup/noho/NohoRow;",
        "context",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;

    invoke-direct {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;-><init>()V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$OptionWithSelectedValueRow;",
            "Lcom/squareup/noho/NohoRow;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/noho/NohoRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 68
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    sget-object v1, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 218
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1$$special$$inlined$bind$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1$$special$$inlined$bind$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
