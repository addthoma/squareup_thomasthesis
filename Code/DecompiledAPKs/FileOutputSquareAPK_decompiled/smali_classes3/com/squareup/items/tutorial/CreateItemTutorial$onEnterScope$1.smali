.class final Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;
.super Ljava/lang/Object;
.source "CreateItemTutorial.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/tutorial/CreateItemTutorial;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/tutorial/CreateItemTutorial;


# direct methods
.method constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorial;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;)V
    .locals 2

    if-nez p1, :cond_0

    goto :goto_0

    .line 87
    :cond_0
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorial$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 89
    :cond_1
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorial;

    const-string v0, "Applets Drawer Closed"

    invoke-virtual {p1, v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :cond_2
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorial;

    const-string v0, "Applets Drawer Opened"

    invoke-virtual {p1, v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;->accept(Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;)V

    return-void
.end method
