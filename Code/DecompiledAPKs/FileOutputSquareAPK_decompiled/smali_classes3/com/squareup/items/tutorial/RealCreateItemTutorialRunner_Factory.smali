.class public final Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;
.super Ljava/lang/Object;
.source "RealCreateItemTutorialRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final createItemTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->createItemTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->homeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;)",
            "Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/Home;)Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;-><init>(Lflow/Flow;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/Home;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->createItemTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    iget-object v2, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v3, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/main/Home;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/Home;)Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner_Factory;->get()Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;

    move-result-object v0

    return-object v0
.end method
