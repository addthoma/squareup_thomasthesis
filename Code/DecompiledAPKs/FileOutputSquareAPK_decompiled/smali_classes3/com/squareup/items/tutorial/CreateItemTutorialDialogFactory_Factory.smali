.class public final Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;
.super Ljava/lang/Object;
.source "CreateItemTutorialDialogFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    invoke-direct {v0, p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;-><init>(Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;->newInstance(Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory_Factory;->get()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object v0

    return-object v0
.end method
