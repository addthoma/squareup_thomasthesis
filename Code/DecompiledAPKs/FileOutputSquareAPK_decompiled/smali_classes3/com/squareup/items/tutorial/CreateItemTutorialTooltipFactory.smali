.class public final Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;
.super Ljava/lang/Object;
.source "CreateItemTutorialTooltipFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\r\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0006\u0010\u0008\u001a\u00020\u0004J\u0006\u0010\t\u001a\u00020\u0004J\u0006\u0010\n\u001a\u00020\u0004J\u0006\u0010\u000b\u001a\u00020\u0004J\u0006\u0010\u000c\u001a\u00020\u0004J\u0006\u0010\r\u001a\u00020\u0004J\u0006\u0010\u000e\u001a\u00020\u0004J\u0006\u0010\u000f\u001a\u00020\u0004J\u0006\u0010\u0010\u001a\u00020\u0004J\u0006\u0010\u0011\u001a\u00020\u0004J\u0006\u0010\u0012\u001a\u00020\u0004\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
        "",
        "()V",
        "adjustInventory",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "dragAndDropItem",
        "tileTag",
        "",
        "editCategory",
        "enterEditMode",
        "enterItemName",
        "enterItemPrice",
        "saveItem",
        "selectAllItemsFromItemsApplet",
        "selectCreateItemPhone",
        "selectCreateItemTablet",
        "selectItemsFromApplet",
        "tapDoneEditing",
        "tapEmtpyTile",
        "Companion",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory$Companion;

.field public static final SKIP_ENTER_PRICE:Ljava/lang/String; = "Skip Enter Price"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final adjustInventory()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 69
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 70
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_adjust_inventory:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 72
    sget v1, Lcom/squareup/adjustinventory/R$id;->adjust_stock_specify_number_field:I

    .line 73
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final dragAndDropItem(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    const-string/jumbo v0, "tileTag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 90
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_tablet_drag_and_drop_item:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 93
    sget v0, Lcom/squareup/common/tutorial/R$string;->tutorial_button_ok:I

    const-string v1, "Favorites Grid Drag and Drop Item"

    .line 92
    invoke-virtual {p1, v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1
.end method

.method public final editCategory()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    .line 65
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 66
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_create_category:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final enterEditMode()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 77
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 78
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_tablet_enter_edit_mode_tooltip:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 80
    sget v1, Lcom/squareup/orderentryappletapi/R$id;->favorite_grid_empty_tile:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 81
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 79
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final enterItemName()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 43
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 44
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_enter_item_name:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 46
    sget v1, Lcom/squareup/edititem/R$id;->edit_item_details_item_name:I

    .line 47
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 45
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final enterItemPrice()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 51
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 52
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_enter_item_price:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 54
    sget v1, Lcom/squareup/edititem/R$id;->edit_item_main_view_price_input_field:I

    .line 55
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 53
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 57
    sget v1, Lcom/squareup/common/tutorial/R$string;->tutorial_button_skip:I

    const-string v2, "Skip Enter Price"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final saveItem()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 60
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 61
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_save_item:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 62
    sget v1, Lcom/squareup/edititem/R$id;->edit_item_save_button:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final selectAllItemsFromItemsApplet()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 23
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 24
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_phone_select_all_items:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 26
    sget v1, Lcom/squareup/widgets/pos/R$id;->title:I

    .line 27
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 25
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final selectCreateItemPhone()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 31
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 32
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_phone_select_create_item:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 34
    sget v1, Lcom/squareup/itemsapplet/R$id;->detail_searchable_list_view_add_button:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 33
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->tagAnchor(Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final selectCreateItemTablet()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    .line 39
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 40
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_phone_select_create_item:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final selectItemsFromApplet()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 15
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 16
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_phone_select_items:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 18
    sget v1, Lcom/squareup/registerlib/R$id;->applets_drawer_items_applet:I

    .line 19
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 17
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final tapDoneEditing()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 98
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 99
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_tablet_tap_done_editing:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 101
    sget v1, Lcom/squareup/orderentryappletapi/R$id;->done_editing:I

    .line 102
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_ABOVE_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 100
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public final tapEmtpyTile()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    .line 85
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 86
    sget v1, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_tablet_tap_empty_tile_tooltip:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method
