.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;
.super Ljava/lang/Object;
.source "SelectOptionsLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Factory;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionsLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,438:1\n1360#2:439\n1429#2,3:440\n1360#2:443\n1429#2,2:444\n1360#2:446\n1429#2,3:447\n704#2:450\n777#2,2:451\n1431#2:453\n1360#2:454\n1429#2,3:455\n49#3:458\n50#3,3:464\n53#3:511\n599#4,4:459\n601#4:463\n310#5,6:467\n310#5,6:473\n310#5,6:479\n310#5,6:485\n310#5,6:491\n310#5,6:497\n310#5,6:503\n43#6,2:509\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionsLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner\n*L\n331#1:439\n331#1,3:440\n356#1:443\n356#1,2:444\n356#1:446\n356#1,3:447\n356#1:450\n356#1,2:451\n356#1:453\n368#1:454\n368#1,3:455\n71#1:458\n71#1,3:464\n71#1:511\n71#1,4:459\n71#1:463\n71#1,6:467\n71#1,6:473\n71#1,6:479\n71#1,6:485\n71#1,6:491\n71#1,6:497\n71#1,6:503\n71#1,2:509\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 !2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003!\"#B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0016\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0018\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010\u001f\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010 \u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u000b\u001a\u0016\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \n*\u0004\u0018\u00010\u00130\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n \n*\u0004\u0018\u00010\u00150\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "moveHandler",
        "Lkotlin/Function2;",
        "",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "getFullScreenRows",
        "",
        "rendering",
        "getNullStateRows",
        "getSearchResultRows",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateRecycler",
        "updateSearchBar",
        "Companion",
        "Factory",
        "SelectOptionsRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Companion;

.field private static final numStaticRowsAboveAssignedOptionList:I = 0x1


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private moveHandler:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final searchBar:Lcom/squareup/noho/NohoEditRow;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->Companion:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->view:Landroid/view/View;

    .line 67
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 68
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->available_options_search_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 70
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_options_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 459
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 464
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 465
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 72
    sget-object p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 77
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p2, p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p2}, Lcom/squareup/cycler/RecyclerMutationsKt;->enableMutations(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V

    .line 468
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 93
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 468
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 467
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 474
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 117
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 138
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$4$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$4$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 473
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 480
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 142
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$5$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 480
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 479
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 486
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 157
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 177
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 485
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 492
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 181
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 202
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 491
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 498
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 206
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$8$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$8$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 234
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$8$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$8$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 497
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 504
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$7;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$$special$$inlined$row$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 238
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$9$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$9$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 504
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 503
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 509
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v1, 0x8

    .line 250
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 509
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 462
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 459
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getMoveHandler$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;)Lkotlin/jvm/functions/Function2;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method

.method public static final synthetic access$setMoveHandler$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method private final getFullScreenRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 355
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 356
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getAssignedOptions()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 443
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 444
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 445
    check-cast v5, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;

    .line 358
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v6

    .line 359
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    .line 361
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v8

    check-cast v8, Ljava/lang/Iterable;

    .line 446
    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v8, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v9, Ljava/util/Collection;

    .line 447
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 448
    check-cast v10, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 361
    invoke-virtual {v10}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 449
    :cond_0
    check-cast v9, Ljava/util/List;

    check-cast v9, Ljava/lang/Iterable;

    .line 450
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    check-cast v8, Ljava/util/Collection;

    .line 451
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    move-object v11, v10

    check-cast v11, Ljava/lang/String;

    .line 362
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;->getSelectedValues()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v8, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 452
    :cond_2
    check-cast v8, Ljava/util/List;

    move-object v9, v8

    check-cast v9, Ljava/lang/Iterable;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x3f

    const/16 v17, 0x0

    .line 363
    invoke-static/range {v9 .. v17}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 364
    new-instance v9, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$$inlined$map$lambda$1;

    invoke-direct {v9, v5, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$$inlined$map$lambda$1;-><init>(Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v9, Lkotlin/jvm/functions/Function0;

    .line 357
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AssignedOptionRow;

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AssignedOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 366
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 453
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 368
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getAvailableOptions()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 454
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 455
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 456
    check-cast v4, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 369
    new-instance v6, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;

    .line 370
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v7

    .line 371
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 372
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getSelectedAvailableOptionId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    .line 373
    new-instance v10, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$$inlined$map$lambda$2;

    invoke-direct {v10, v4, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$$inlined$map$lambda$2;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v10, Lkotlin/jvm/functions/Function0;

    .line 369
    invoke-direct {v6, v7, v8, v9, v10}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V

    .line 375
    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 457
    :cond_4
    check-cast v5, Ljava/util/List;

    .line 378
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getCanCreateNewOption()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 381
    move-object v2, v3

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 382
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$1;

    invoke-direct {v4, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getFullScreenRows$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 380
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateOptionButtonRow;

    invoke-direct {v0, v2, v4}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateOptionButtonRow;-><init>(ZLkotlin/jvm/functions/Function0;)V

    .line 379
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_5
    check-cast v3, Ljava/util/Collection;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 387
    move-object v0, v5

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    .line 388
    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    sget-object v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionsSectionHeader;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionsSectionHeader;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v2, v5}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    :cond_6
    return-object v1
.end method

.method private final getNullStateRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;

    .line 321
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsMessage;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsMessage;

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 322
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateOptionButtonRow;

    .line 324
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getNullStateRows$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getNullStateRows$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 322
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateOptionButtonRow;-><init>(ZLkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 326
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsCreateButtonHelpText;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getOnLearnMoreOnItemOptionsClicked()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsCreateButtonHelpText;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;

    const/4 p1, 0x2

    aput-object v1, v0, p1

    .line 320
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getSearchResultRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
            ">;"
        }
    .end annotation

    .line 330
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getAvailableOptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getAvailableOptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 439
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 440
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 441
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 332
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;

    .line 333
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v4

    .line 334
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 335
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getSelectedAvailableOptionId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 336
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getSearchResultRows$$inlined$map$lambda$1;

    invoke-direct {v7, v2, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getSearchResultRows$$inlined$map$lambda$1;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 332
    invoke-direct {v3, v4, v5, v6, v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V

    .line 338
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 442
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1

    .line 340
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getCanCreateNewOption()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getCanCreateFromSearch()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 341
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    if-eqz v0, :cond_6

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 342
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v2, 0x1

    :goto_2
    xor-int/2addr v1, v2

    if-eqz v1, :cond_5

    .line 344
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateFromSearchButtonRow;

    .line 346
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getSearchResultRows$2;

    invoke-direct {v2, p1, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$getSearchResultRows$2;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;Ljava/lang/String;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 344
    invoke-direct {v1, v0, v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$CreateFromSearchButtonRow;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 343
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 342
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 341
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 350
    :cond_7
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final updateActionBar(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V
    .locals 6

    .line 281
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 265
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 267
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    .line 268
    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->assign_option_to_item_select_options_action_bar_title:I

    .line 267
    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 266
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 271
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 273
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 274
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    .line 275
    sget v4, Lcom/squareup/items/assignitemoptions/impl/R$string;->assign_option_to_item_select_options_action_bar_primary_button_text:I

    .line 274
    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 277
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->isNextEnabled()Z

    move-result v4

    .line 278
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateActionBar$2;

    invoke-direct {v5, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateActionBar$2;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 272
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 281
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateRecycler(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V
    .locals 3

    .line 298
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    iput-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    .line 304
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->inNullState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->getNullStateRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 307
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->showingSearchResults()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->getSearchResultRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 310
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->getFullScreenRows(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)Ljava/util/List;

    move-result-object v0

    .line 314
    :goto_0
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$2;

    invoke-direct {v2, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$2;-><init>(Ljava/util/List;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 317
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$3;

    invoke-direct {v1, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$updateRecycler$3;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final updateSearchBar(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V
    .locals 3

    .line 287
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->inNullState()Z

    move-result v0

    const-string v1, "searchBar"

    if-eqz v0, :cond_0

    .line 288
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    goto :goto_0

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 291
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->updateActionBar(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    .line 258
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->updateSearchBar(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    .line 259
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->updateRecycler(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
