.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;
.super Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;
.source "ChangeOptionValuesState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Create"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\t\u001a\u00020\u0003H\u00d6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
        "numberOfExistingVariations",
        "",
        "(I)V",
        "getNumberOfExistingVariations",
        "()I",
        "component1",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final numberOfExistingVariations:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;IILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->copy(I)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;-><init>(I)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    iget v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    iget p1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNumberOfExistingVariations()I
    .locals 1

    .line 8
    iget v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Create(numberOfExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->numberOfExistingVariations:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
