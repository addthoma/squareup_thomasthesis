.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations$Creator;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    const-class v1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    sget-object v2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-direct {v0, v1, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 0

    new-array p1, p1, [Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    return-object p1
.end method
