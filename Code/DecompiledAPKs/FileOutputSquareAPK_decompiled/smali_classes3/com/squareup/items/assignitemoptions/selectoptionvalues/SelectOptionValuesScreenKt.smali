.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreenKt;
.super Ljava/lang/Object;
.source "SelectOptionValuesScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "itemOptionName",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final itemOptionName(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;
    .locals 2

    const-string v0, "$this$itemOptionName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object p0

    .line 66
    invoke-direct {v0, v1, p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
