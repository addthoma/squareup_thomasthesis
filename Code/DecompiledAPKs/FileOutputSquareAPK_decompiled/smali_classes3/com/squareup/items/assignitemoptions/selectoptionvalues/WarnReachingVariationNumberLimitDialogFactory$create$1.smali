.class final Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;
.super Ljava/lang/Object;
.source "WarnReachingVariationNumberLimitDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u001c\u0010\u0002\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;->this$0:Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;->this$0:Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;

    invoke-static {v0, v1, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;->access$createDialog(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;Landroid/content/Context;Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
