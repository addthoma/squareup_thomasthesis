.class public final Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;
.super Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action;
.source "FetchAllVariationsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SucceededToFetch"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c2\u0003J\u0019\u0010\u0007\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0018\u0010\u0010\u001a\u00020\u0011*\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012H\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action;",
        "variations",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
        "(Ljava/util/List;)V",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "variations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 109
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    return-void
.end method

.method private final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->copy(Ljava/util/List;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput$VariationsFetched;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput$VariationsFetched;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final copy(Ljava/util/List;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;"
        }
    .end annotation

    const-string/jumbo v0, "variations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SucceededToFetch(variations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action$SucceededToFetch;->variations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
