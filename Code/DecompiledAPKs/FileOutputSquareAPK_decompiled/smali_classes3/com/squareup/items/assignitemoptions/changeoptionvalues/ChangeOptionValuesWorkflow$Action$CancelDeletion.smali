.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;
.super Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action;
.source "ChangeOptionValuesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CancelDeletion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 88
    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action$CancelDeletion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput$Cancel;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput$Cancel;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
