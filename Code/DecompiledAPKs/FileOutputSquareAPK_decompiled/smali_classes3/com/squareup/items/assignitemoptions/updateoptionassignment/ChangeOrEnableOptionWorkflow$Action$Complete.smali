.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;
.super Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action;
.source "ChangeOrEnableOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Complete"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action;",
        "selection",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getSelection",
        "()Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 1

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 142
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->copy(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    if-eqz v0, :cond_0

    .line 145
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;->getChosenValueUsedToExtendExistingVariations()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v0

    .line 147
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;->getCombinations()Ljava/util/List;

    move-result-object v2

    .line 145
    invoke-virtual {v1, v0, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->updateExistingVariationsAndCreateNewVariations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->commitDeletionOfVariationsAndUpdateExistingVariations()V

    .line 151
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->cleanUpIntendedOptionsAndValues()V

    .line 153
    sget-object v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput$Done;->INSTANCE:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput$Done;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final copy(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;
    .locals 1

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getSelection()Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Complete(selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->selection:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action$Complete;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
