.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;
.super Ljava/lang/Object;
.source "WarnVariationsToDeleteProps.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006J\u0016\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;",
        "",
        "()V",
        "multipleValues",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;",
        "deletedValueCount",
        "",
        "deletedVariationCount",
        "singleValue",
        "deletedValueName",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final multipleValues(II)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p2, v1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final singleValue(Ljava/lang/String;I)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;
    .locals 3

    const-string v0, "deletedValueName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
