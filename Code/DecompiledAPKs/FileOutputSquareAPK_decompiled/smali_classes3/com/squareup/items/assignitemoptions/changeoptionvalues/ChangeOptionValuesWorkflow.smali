.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ChangeOptionValuesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChangeOptionValuesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChangeOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,128:1\n32#2,12:129\n1360#3:141\n1429#3,3:142\n*E\n*S KotlinDebug\n*F\n+ 1 ChangeOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow\n*L\n35#1,12:129\n59#1:141\n59#1,3:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u0018B\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00022\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016JN\u0010\u0013\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "reviewVariationsToDeleteWorkflow",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;",
        "selectVariationsToCreateWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reviewVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;

.field private final selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reviewVariationsToDeleteWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectVariationsToCreateWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->reviewVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p2, :cond_4

    .line 129
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 134
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "Parcel.obtain()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 136
    array-length v3, p2

    invoke-virtual {v1, p2, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 137
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 138
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v1

    .line 140
    :goto_2
    move-object v1, p2

    check-cast v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    :cond_4
    if-eqz v1, :cond_5

    goto :goto_4

    .line 39
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->hasDeletes()Z

    move-result p2

    if-nez p2, :cond_7

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->hasSelectionsToMake()Z

    move-result p2

    if-eqz p2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    :cond_7
    :goto_3
    if-eqz v0, :cond_9

    .line 44
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->hasDeletes()Z

    move-result p2

    if-eqz p2, :cond_8

    sget-object p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$ReviewDeletes;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$ReviewDeletes;

    move-object v1, p1

    check-cast v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    goto :goto_4

    :cond_8
    new-instance p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    .line 45
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    .line 44
    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;-><init>(I)V

    move-object v1, p2

    check-cast v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    :goto_4
    return-object v1

    .line 40
    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "ChangeOptionValuesWorkflow should only be used if there are variations to be created or deleted."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->render(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$ReviewDeletes;

    if-eqz v0, :cond_1

    .line 56
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->reviewVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 58
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findAllVariationsToDelete()Ljava/util/List;

    move-result-object p2

    const-string v0, "props.assignmentEngine.findAllVariationsToDelete()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Iterable;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 142
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 143
    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v3

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;

    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateVariationNameWithOptionValueCombination(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 57
    new-instance v2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteProps;

    invoke-direct {v2, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteProps;-><init>(Ljava/util/List;)V

    const/4 v3, 0x0

    .line 60
    new-instance p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$render$2;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$render$2;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 55
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_1

    .line 67
    :cond_1
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 69
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    .line 70
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getItemName()Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v5

    .line 72
    check-cast p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState$Create;->getNumberOfExistingVariations()I

    move-result v6

    .line 73
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v7

    const/4 v8, 0x1

    move-object v3, v0

    .line 69
    invoke-direct/range {v3 .. v8}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    const/4 v4, 0x0

    .line 76
    new-instance p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$render$3;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow$render$3;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 67
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
