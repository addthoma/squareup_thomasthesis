.class final Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditOptionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow;->prepareOptionValues(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;",
        "valueName",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $optionValueId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;->$optionValueId:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;
    .locals 2

    const-string/jumbo v0, "valueName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    new-instance v0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;->$optionValueId:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;->invoke(Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    move-result-object p1

    return-object p1
.end method
