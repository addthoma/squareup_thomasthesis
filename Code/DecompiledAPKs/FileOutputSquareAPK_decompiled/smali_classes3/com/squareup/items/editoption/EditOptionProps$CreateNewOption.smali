.class public final Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;
.super Lcom/squareup/items/editoption/EditOptionProps;
.source "EditOptionProps.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/EditOptionProps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateNewOption"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0007\u00a2\u0006\u0002\u0010\u0008J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0007H\u00c6\u0003J1\u0010\u0012\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;",
        "Lcom/squareup/items/editoption/EditOptionProps;",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "defaultOptionName",
        "",
        "existingOptionNames",
        "",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)V",
        "getDefaultOptionName",
        "()Ljava/lang/String;",
        "getExistingOptionNames",
        "()Ljava/util/Set;",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultOptionName:Ljava/lang/String;

.field private final existingOptionNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final option:Lcom/squareup/cogs/itemoptions/ItemOption;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "existingOptionNames"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/EditOptionProps;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-object p2, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->existingOptionNames:Ljava/util/Set;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 9
    move-object p1, v0

    check-cast p1, Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    .line 10
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;"
        }
    .end annotation

    const-string v0, "existingOptionNames"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDefaultOptionName()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    return-object v0
.end method

.method public getExistingOptionNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->existingOptionNames:Ljava/util/Set;

    return-object v0
.end method

.method public final getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateNewOption(option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultOptionName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->defaultOptionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", existingOptionNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
