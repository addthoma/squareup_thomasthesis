.class public final Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ReviewVariationsToDeleteGloballyWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReviewVariationsToDeleteGloballyWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReviewVariationsToDeleteGloballyWorkflow.kt\ncom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,56:1\n32#2,12:57\n149#3,5:69\n*E\n*S KotlinDebug\n*F\n+ 1 ReviewVariationsToDeleteGloballyWorkflow.kt\ncom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow\n*L\n27#1,12:57\n40#1,5:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u0015B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016JN\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00032\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0003H\u0016\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "initialState",
        "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 57
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 62
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 64
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 65
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 66
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 68
    :cond_3
    check-cast v1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 27
    :cond_4
    sget-object v1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;->INSTANCE:Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow;->initialState(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;

    check-cast p2, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow;->render(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyProps;",
            "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;",
            "-",
            "Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    instance-of p1, p2, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState$DefaultState;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyScreen;

    new-instance p2, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow$render$1;

    invoke-direct {p2, p3}, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow$render$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2}, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 70
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 71
    const-class p3, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 72
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 70
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 40
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyWorkflow;->snapshotState(Lcom/squareup/items/editoption/reviewvariationstodelete/ReviewVariationsToDeleteGloballyState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
