.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/StandardUnitsListScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "event",
        "Lcom/squareup/items/unit/StandardUnitsListScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/StandardUnitsListScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/StandardUnitsListScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    instance-of v0, p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$CreateCustomUnit;

    const/4 v1, 0x0

    const-string/jumbo v2, "unitBuilder.build()"

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    check-cast p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$CreateCustomUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$CreateCustomUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$buildNewCustomUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 143
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 144
    new-instance v5, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 145
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-direct {v5, v6, p1, v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    .line 143
    invoke-static {v0, v5, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 151
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$StandardUnitSelected;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    check-cast p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$StandardUnitSelected;

    invoke-virtual {p1}, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$StandardUnitSelected;->getStandardUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$buildNewStandardUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 153
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 154
    new-instance v5, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 155
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-direct {v5, v6, p1, v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    .line 153
    invoke-static {v0, v5, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 161
    :cond_1
    instance-of v0, p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$SearchQueryChanged;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 162
    new-instance v1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    check-cast p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$SearchQueryChanged;

    invoke-virtual {p1}, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$SearchQueryChanged;->getNewSearchQuery()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$getFilteredStandardUnits(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;-><init>(Ljava/util/List;)V

    .line 161
    invoke-static {v0, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 164
    :cond_2
    sget-object v0, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$Exit;->INSTANCE:Lcom/squareup/items/unit/StandardUnitsListScreen$Event$Exit;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;->INSTANCE:Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/StandardUnitsListScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;->invoke(Lcom/squareup/items/unit/StandardUnitsListScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
