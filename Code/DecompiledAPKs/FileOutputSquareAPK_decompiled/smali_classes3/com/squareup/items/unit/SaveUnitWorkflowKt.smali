.class public final Lcom/squareup/items/unit/SaveUnitWorkflowKt;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSaveUnitWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SaveUnitWorkflow.kt\ncom/squareup/items/unit/SaveUnitWorkflowKt\n*L\n1#1,283:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "isNew",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "edit-unit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isNew(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Z
    .locals 5

    const-string v0, "$this$isNew"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "this.id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v3, 0x0

    const-string v4, "#"

    invoke-static {p0, v4, v2, v0, v3}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_2
    return v1
.end method
