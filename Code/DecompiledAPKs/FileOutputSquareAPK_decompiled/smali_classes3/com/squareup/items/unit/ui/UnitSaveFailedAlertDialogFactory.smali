.class public final Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;
.super Ljava/lang/Object;
.source "UnitSaveFailedAlertDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitSaveFailedAlertDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitSaveFailedAlertDialogFactory.kt\ncom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;Landroid/content/Context;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;)Landroid/app/AlertDialog;
    .locals 3

    .line 34
    invoke-virtual {p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getShouldAllowRetry()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    sget v1, Lcom/squareup/common/strings/R$string;->retry:I

    goto :goto_0

    .line 37
    :cond_0
    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 39
    :goto_0
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    sget p1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    sget v2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 42
    new-instance v2, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$createDialog$1;

    invoke-direct {v2, v0, p2}, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$createDialog$1;-><init>(ZLcom/squareup/items/unit/UnitSaveFailedAlertScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 50
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 51
    new-instance v2, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$createDialog$$inlined$let$lambda$1;

    invoke-direct {v2, v0, p2}, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$createDialog$$inlined$let$lambda$1;-><init>(ZLcom/squareup/items/unit/UnitSaveFailedAlertScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 49
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 54
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getUnitSaveFailedAlertMessageId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getUnitSaveFailedAlertTitleId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 56
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026(false)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 25
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory$create$1;-><init>(Lcom/squareup/items/unit/ui/UnitSaveFailedAlertDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
