.class final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;
.super Ljava/lang/Object;
.source "StandardUnitsListCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012<\u0010\u0002\u001a8\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "s",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/StandardUnitsListScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;->$view:Landroid/view/View;

    const-string v2, "s"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/unit/StandardUnitsListScreen;

    invoke-static {v0, v1, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$update(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Landroid/view/View;Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
