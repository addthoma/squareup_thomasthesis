.class public final Lcom/squareup/cycler/BinderRowSpec;
.super Lcom/squareup/cycler/Recycler$RowSpec;
.source "BinderRowSpec.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/BinderRowSpec$BinderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "S::TI;V:",
        "Landroid/view/View;",
        ">",
        "Lcom/squareup/cycler/Recycler$RowSpec<",
        "TI;TS;TV;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBinderRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec\n*L\n1#1,56:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u0002H\u0001*\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006:\u0001\u001aB/\u0008\u0001\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u00020\u0008\u00a2\u0006\u0002\u0010\u000cJ%\u0010\u0013\u001a\u00020\u00102\u001a\u0008\u0004\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00100\u0015H\u0086\u0008J&\u0010\u0013\u001a\u00020\u00102\u001e\u0010\u0014\u001a\u001a\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00100\u000eJ\u001e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000fH\u0016R&\u0010\r\u001a\u001a\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00100\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/cycler/BinderRowSpec;",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/Recycler$RowSpec;",
        "typeMatchBlock",
        "Lkotlin/Function1;",
        "",
        "creatorBlock",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "bindBlock",
        "Lkotlin/Function3;",
        "",
        "",
        "getCreatorBlock",
        "()Lkotlin/jvm/functions/Function1;",
        "bind",
        "block",
        "Lkotlin/Function2;",
        "createViewHolder",
        "Lcom/squareup/cycler/Recycler$ViewHolder;",
        "creatorContext",
        "subType",
        "BinderViewHolder",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private bindBlock:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-TV;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final creatorBlock:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "+TV;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "typeMatchBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creatorBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/cycler/Recycler$RowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    iput-object p2, p0, Lcom/squareup/cycler/BinderRowSpec;->creatorBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final bind(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-TS;-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec$bind$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/BinderRowSpec$bind$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public final bind(Lkotlin/jvm/functions/Function3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/squareup/cycler/BinderRowSpec;->bindBlock:Lkotlin/jvm/functions/Function3;

    return-void
.end method

.method public createViewHolder(Lcom/squareup/cycler/Recycler$CreatorContext;I)Lcom/squareup/cycler/Recycler$ViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "I)",
            "Lcom/squareup/cycler/Recycler$ViewHolder<",
            "TV;>;"
        }
    .end annotation

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    .line 36
    iget-object p2, p0, Lcom/squareup/cycler/BinderRowSpec;->creatorBlock:Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-nez p2, :cond_1

    .line 38
    invoke-static {}, Lcom/squareup/cycler/UtilsKt;->createItemLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    :cond_1
    new-instance p2, Lcom/squareup/cycler/BinderRowSpec$BinderViewHolder;

    iget-object v0, p0, Lcom/squareup/cycler/BinderRowSpec;->bindBlock:Lkotlin/jvm/functions/Function3;

    if-nez v0, :cond_2

    const-string v1, "bindBlock"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p2, p1, v0}, Lcom/squareup/cycler/BinderRowSpec$BinderViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function3;)V

    check-cast p2, Lcom/squareup/cycler/Recycler$ViewHolder;

    return-object p2

    .line 34
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Expected subType == 0 ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class p2, Lcom/squareup/cycler/BinderRowSpec;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " doesn\'t use subTypes)."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 33
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final getCreatorBlock()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "TV;>;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/cycler/BinderRowSpec;->creatorBlock:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
