.class public final Lcom/squareup/cycler/mosaic/MosaicRowSpec;
.super Lcom/squareup/cycler/Recycler$RowSpec;
.source "MosaicRowSpec.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "S::TI;>",
        "Lcom/squareup/cycler/Recycler$RowSpec<",
        "TI;TS;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,198:1\n310#2,7:199\n*E\n*S KotlinDebug\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec\n*L\n158#1,7:199\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u0002H\u00012\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u00050\u0004:\u0001#B\u001b\u0008\u0001\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u001e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000cH\u0016J0\u0010\u0018\u001a\u00020\u000e2%\u0008\u0004\u0010\u0019\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\u001a\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000e0\u000b\u00a2\u0006\u0002\u0008\u001bH\u0086\u0008J1\u0010\u0018\u001a\u00020\u000e2)\u0010\u0019\u001a%\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000e0\u001c\u00a2\u0006\u0002\u0008\u001bJ#\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00028\u0001H\u0002\u00a2\u0006\u0002\u0010 J\u001d\u0010\u0017\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u000c2\u0006\u0010!\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\"R&\u0010\n\u001a\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00028\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u000f\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\r0\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u00120\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/cycler/mosaic/MosaicRowSpec;",
        "I",
        "",
        "S",
        "Lcom/squareup/cycler/Recycler$RowSpec;",
        "Landroid/view/View;",
        "typeMatchBlock",
        "Lkotlin/Function1;",
        "",
        "(Lkotlin/jvm/functions/Function1;)V",
        "modelFactory",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "",
        "representativeModels",
        "",
        "representativeViewRefs",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "createViewHolder",
        "Lcom/squareup/cycler/Recycler$ViewHolder;",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "subType",
        "model",
        "modelLambda",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lkotlin/ExtensionFunctionType;",
        "Lkotlin/Function3;",
        "modelFor",
        "index",
        "dataItem",
        "(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;",
        "any",
        "(ILjava/lang/Object;)I",
        "MosaicViewHolder",
        "recycler-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private modelFactory:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;+",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final representativeModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final representativeViewRefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "typeMatchBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/cycler/Recycler$RowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 130
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeModels:Ljava/util/List;

    .line 131
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$modelFor(Lcom/squareup/cycler/mosaic/MosaicRowSpec;ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;
    .locals 0

    .line 125
    invoke-direct {p0, p1, p2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->modelFor(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;

    move-result-object p0

    return-object p0
.end method

.method private final modelFor(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->modelFactory:Lkotlin/jvm/functions/Function2;

    if-nez v0, :cond_0

    const-string v1, "modelFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/mosaic/core/UiModel;

    return-object p1
.end method


# virtual methods
.method public createViewHolder(Lcom/squareup/cycler/Recycler$CreatorContext;I)Lcom/squareup/cycler/Recycler$ViewHolder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "I)",
            "Lcom/squareup/cycler/Recycler$ViewHolder<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 174
    iget-object v1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v0, :cond_0

    if-ge p2, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 179
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeModels:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$CreatorContext;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p1

    .line 180
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 183
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    new-instance p2, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;-><init>(Lcom/squareup/cycler/mosaic/MosaicRowSpec;Lcom/squareup/mosaic/core/ViewRef;)V

    check-cast p2, Lcom/squareup/cycler/Recycler$ViewHolder;

    return-object p2

    .line 176
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected subType ("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ") to be allocated"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "in representativeModels ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ") and representativeViewRefs ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p2, 0x29

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 175
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final model(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "modelLambda"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public final model(Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "modelLambda"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;

    invoke-direct {v0}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;-><init>()V

    .line 147
    new-instance v1, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;

    invoke-direct {v1, v0, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;-><init>(Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;Lkotlin/jvm/functions/Function3;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    iput-object v1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->modelFactory:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public subType(ILjava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITI;)I"
        }
    .end annotation

    const-string v0, "any"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, p1, p2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->modelFor(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    .line 158
    iget-object p2, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    .line 200
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 201
    check-cast v2, Lcom/squareup/mosaic/core/ViewRef;

    if-eqz v2, :cond_0

    .line 158
    invoke-virtual {v2, p1}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v2

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    :goto_2
    if-gez v1, :cond_3

    .line 161
    iget-object p2, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeModels:Ljava/util/List;

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeViewRefs:Ljava/util/List;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->representativeModels:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 v1, p1, -0x1

    :cond_3
    return v1
.end method
