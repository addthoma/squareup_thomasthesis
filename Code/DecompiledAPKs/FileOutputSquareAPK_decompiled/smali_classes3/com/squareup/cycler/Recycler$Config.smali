.class public final Lcom/squareup/cycler/Recycler$Config;
.super Ljava/lang/Object;
.source "Recycler.kt"


# annotations
.annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecycler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$RowSpec\n*L\n1#1,606:1\n342#1,5:613\n344#1,4:618\n205#2,2:607\n205#2,2:610\n527#3:609\n527#3:612\n*E\n*S KotlinDebug\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n328#1,5:613\n328#1,4:618\n251#1,2:607\n261#1,2:610\n252#1:609\n262#1:612\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u00020\u0002B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0003J*\u00104\u001a\u0002052\"\u00106\u001a\u001e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000c0\u000bj\u0008\u0012\u0004\u0012\u00028\u0001`\rJ\u0015\u00107\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010!H\u0000\u00a2\u0006\u0002\u00088J\u0014\u00109\u001a\u0002052\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0014J\u001e\u0010;\u001a\u0002052\u0014\u0010<\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0018H\u0001JN\u0010;\u001a\u000205\"\n\u0008\u0002\u0010=\u0018\u0001*\u00020\u0002\"\n\u0008\u0003\u0010>\u0018\u0001*\u00020?2+\u0008\u0004\u0010@\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H=\u0012\u0004\u0012\u0002H>0A\u0012\u0004\u0012\u0002050-\u00a2\u0006\u0002\u0008BH\u0086\u0008J$\u0010C\u001a\u0004\u0018\u0001HD\"\n\u0008\u0002\u0010D\u0018\u0001*\u00020\u00022\u0006\u0010E\u001a\u00020\u0002H\u0081\u0008\u00a2\u0006\u0002\u0010FJY\u0010G\u001a\u000205\"\n\u0008\u0002\u0010=\u0018\u0001*\u00028\u0001\"\n\u0008\u0003\u0010>\u0018\u0001*\u00020?2\u0014\u0008\u0008\u0010H\u001a\u000e\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u0002H>0-2 \u0008\u0008\u0010J\u001a\u001a\u0012\u0004\u0012\u00020L\u0012\u0004\u0012\u0002H=\u0012\u0004\u0012\u0002H>\u0012\u0004\u0012\u0002050KH\u0086\u0008Jd\u0010G\u001a\u000205\"\n\u0008\u0002\u0010=\u0018\u0001*\u00028\u0001\"\n\u0008\u0003\u0010>\u0018\u0001*\u00020?2\u0014\u0008\u0008\u0010H\u001a\u000e\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u0002H>0-2+\u0008\u0004\u0010@\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u0002H=\u0012\u0004\u0012\u0002H>0M\u0012\u0004\u0012\u0002050-\u00a2\u0006\u0002\u0008BH\u0086\u0008J\u001c\u0010G\u001a\u0002052\u0014\u0010<\u001a\u0010\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0018JN\u0010G\u001a\u000205\"\n\u0008\u0002\u0010=\u0018\u0001*\u00028\u0001\"\n\u0008\u0003\u0010>\u0018\u0001*\u00020?2+\u0008\u0004\u0010@\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u0002H=\u0012\u0004\u0012\u0002H>0A\u0012\u0004\u0012\u0002050-\u00a2\u0006\u0002\u0008BH\u0086\u0008J$\u0010N\u001a\u0004\u0018\u0001HD\"\n\u0008\u0002\u0010D\u0018\u0001*\u00020\u00022\u0006\u0010E\u001a\u00028\u0001H\u0081\u0008\u00a2\u0006\u0002\u0010FJ\u0016\u0010O\u001a\u0008\u0012\u0004\u0012\u00028\u00010P2\u0006\u0010Q\u001a\u00020RH\u0001J$\u0010S\u001a\u0002052\u001c\u00106\u001a\u0018\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020.0-j\u0008\u0012\u0004\u0012\u00028\u0001`/R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR:\u0010\n\u001a\"\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u000bj\n\u0012\u0004\u0012\u00028\u0001\u0018\u0001`\rX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R \u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00140\u0013X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R0\u0010\u0017\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00180\u00138\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0019\u0010\u0003\u001a\u0004\u0008\u001a\u0010\u0016R\u001a\u0010\u001b\u001a\u00020\u000cX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR\"\u0010 \u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010#\"\u0004\u0008$\u0010%R\u001a\u0010&\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010\u0007\"\u0004\u0008(\u0010\tR0\u0010)\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00180\u00138\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008*\u0010\u0003\u001a\u0004\u0008+\u0010\u0016R0\u0010,\u001a\u0018\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020.0-j\u0008\u0012\u0004\u0012\u00028\u0001`/X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00080\u00101\"\u0004\u00082\u00103\u00a8\u0006T"
    }
    d2 = {
        "Lcom/squareup/cycler/Recycler$Config;",
        "I",
        "",
        "()V",
        "backgroundDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "getBackgroundDispatcher",
        "()Lkotlinx/coroutines/CoroutineDispatcher;",
        "setBackgroundDispatcher",
        "(Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "contentComparator",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/cycler/ContentComparator;",
        "getContentComparator$lib_release",
        "()Lkotlin/jvm/functions/Function2;",
        "setContentComparator$lib_release",
        "(Lkotlin/jvm/functions/Function2;)V",
        "extensionSpecs",
        "",
        "Lcom/squareup/cycler/ExtensionSpec;",
        "getExtensionSpecs$lib_release",
        "()Ljava/util/List;",
        "extraItemSpecs",
        "Lcom/squareup/cycler/Recycler$RowSpec;",
        "extraItemSpecs$annotations",
        "getExtraItemSpecs",
        "hasStableIdProvider",
        "getHasStableIdProvider$lib_release",
        "()Z",
        "setHasStableIdProvider$lib_release",
        "(Z)V",
        "itemComparator",
        "Lcom/squareup/cycler/ItemComparator;",
        "getItemComparator",
        "()Lcom/squareup/cycler/ItemComparator;",
        "setItemComparator",
        "(Lcom/squareup/cycler/ItemComparator;)V",
        "mainDispatcher",
        "getMainDispatcher",
        "setMainDispatcher",
        "rowSpecs",
        "rowSpecs$annotations",
        "getRowSpecs",
        "stableIdProvider",
        "Lkotlin/Function1;",
        "",
        "Lcom/squareup/cycler/StableIdProvider;",
        "getStableIdProvider$lib_release",
        "()Lkotlin/jvm/functions/Function1;",
        "setStableIdProvider$lib_release",
        "(Lkotlin/jvm/functions/Function1;)V",
        "compareItemsContent",
        "",
        "block",
        "createItemComparator",
        "createItemComparator$lib_release",
        "extension",
        "spec",
        "extraItem",
        "rowSpec",
        "S",
        "V",
        "Landroid/view/View;",
        "specBlock",
        "Lcom/squareup/cycler/StandardRowSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "extraRowExtension",
        "T",
        "item",
        "(Ljava/lang/Object;)Ljava/lang/Object;",
        "row",
        "creatorBlock",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "bindBlock",
        "Lkotlin/Function3;",
        "",
        "Lcom/squareup/cycler/BinderRowSpec;",
        "rowExtension",
        "setUp",
        "Lcom/squareup/cycler/Recycler;",
        "view",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "stableId",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private contentComparator:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-TI;-TI;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final extensionSpecs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cycler/ExtensionSpec<",
            "TI;>;>;"
        }
    .end annotation
.end field

.field private final extraItemSpecs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "Ljava/lang/Object;",
            "**>;>;"
        }
    .end annotation
.end field

.field private hasStableIdProvider:Z

.field private itemComparator:Lcom/squareup/cycler/ItemComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/ItemComparator<",
            "-TI;>;"
        }
    .end annotation
.end field

.field private mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final rowSpecs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;**>;>;"
        }
    .end annotation
.end field

.field private stableIdProvider:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->rowSpecs:Ljava/util/List;

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extraItemSpecs:Ljava/util/List;

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extensionSpecs:Ljava/util/List;

    .line 270
    sget-object v0, Lcom/squareup/cycler/Recycler$Config$stableIdProvider$1;->INSTANCE:Lcom/squareup/cycler/Recycler$Config$stableIdProvider$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    .line 278
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getMain()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    .line 284
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getIO()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method public static synthetic extraItemSpecs$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic rowSpecs$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final compareItemsContent(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-TI;-TI;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->contentComparator:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final createItemComparator$lib_release()Lcom/squareup/cycler/ItemComparator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/ItemComparator<",
            "TI;>;"
        }
    .end annotation

    .line 369
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->itemComparator:Lcom/squareup/cycler/ItemComparator;

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    .line 370
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$Config;->contentComparator:Lkotlin/jvm/functions/Function2;

    if-eqz v1, :cond_1

    .line 371
    new-instance v2, Lcom/squareup/cycler/DefaultItemComparator;

    invoke-direct {v2, v0, v1}, Lcom/squareup/cycler/DefaultItemComparator;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 369
    :goto_0
    move-object v0, v2

    check-cast v0, Lcom/squareup/cycler/ItemComparator;

    :goto_1
    return-object v0
.end method

.method public final extension(Lcom/squareup/cycler/ExtensionSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/ExtensionSpec<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "spec"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extensionSpecs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "Ljava/lang/Object;",
            "**>;)V"
        }
    .end annotation

    const-string v0, "rowSpec"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extraItemSpecs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final synthetic extraItem(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Ljava/lang/Object;",
            "TS;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "specBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 356
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/Recycler$Config$extraItem$1;->INSTANCE:Lcom/squareup/cycler/Recycler$Config$extraItem$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 357
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 355
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public final synthetic extraRowExtension(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 610
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 261
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 612
    invoke-virtual {v1}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    const/4 v0, 0x4

    const-string v1, "T?"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    check-cast p1, Ljava/lang/Object;

    return-object p1

    .line 611
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public final getContentComparator$lib_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "TI;TI;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 273
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->contentComparator:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getExtensionSpecs$lib_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cycler/ExtensionSpec<",
            "TI;>;>;"
        }
    .end annotation

    .line 267
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extensionSpecs:Ljava/util/List;

    return-object v0
.end method

.method public final getExtraItemSpecs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "Ljava/lang/Object;",
            "**>;>;"
        }
    .end annotation

    .line 266
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->extraItemSpecs:Ljava/util/List;

    return-object v0
.end method

.method public final getHasStableIdProvider$lib_release()Z
    .locals 1

    .line 269
    iget-boolean v0, p0, Lcom/squareup/cycler/Recycler$Config;->hasStableIdProvider:Z

    return v0
.end method

.method public final getItemComparator()Lcom/squareup/cycler/ItemComparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/ItemComparator<",
            "TI;>;"
        }
    .end annotation

    .line 290
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->itemComparator:Lcom/squareup/cycler/ItemComparator;

    return-object v0
.end method

.method public final getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public final getRowSpecs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;**>;>;"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->rowSpecs:Ljava/util/List;

    return-object v0
.end method

.method public final getStableIdProvider$lib_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TI;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final row(Lcom/squareup/cycler/Recycler$RowSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;**>;)V"
        }
    .end annotation

    const-string v0, "rowSpec"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Config;->rowSpecs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final synthetic row(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "TI;TS;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "specBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/Recycler$Config$row$1;->INSTANCE:Lcom/squareup/cycler/Recycler$Config$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 312
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 310
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public final synthetic row(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "+TV;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/BinderRowSpec<",
            "TI;TS;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "creatorBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 344
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/Recycler$Config$row$4;->INSTANCE:Lcom/squareup/cycler/Recycler$Config$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 343
    invoke-direct {v0, v1, p1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 346
    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 342
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public final synthetic row(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "+TV;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "creatorBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 614
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 618
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/Recycler$Config$row$$inlined$row$1;->INSTANCE:Lcom/squareup/cycler/Recycler$Config$row$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 614
    invoke-direct {v0, v1, p1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 328
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 617
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 613
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public final synthetic rowExtension(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TI;)TT;"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 607
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 251
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 609
    invoke-virtual {v1}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    const/4 v0, 0x4

    const-string v1, "T?"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    check-cast p1, Ljava/lang/Object;

    return-object p1

    .line 608
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method public final setContentComparator$lib_release(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-TI;-TI;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 273
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->contentComparator:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setHasStableIdProvider$lib_release(Z)V
    .locals 0

    .line 269
    iput-boolean p1, p0, Lcom/squareup/cycler/Recycler$Config;->hasStableIdProvider:Z

    return-void
.end method

.method public final setItemComparator(Lcom/squareup/cycler/ItemComparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/ItemComparator<",
            "-TI;>;)V"
        }
    .end annotation

    .line 290
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->itemComparator:Lcom/squareup/cycler/ItemComparator;

    return-void
.end method

.method public final setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method public final setStableIdProvider$lib_release(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 397
    new-instance v0, Lcom/squareup/cycler/Recycler;

    invoke-direct {v0, p1, p0}, Lcom/squareup/cycler/Recycler;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/cycler/Recycler$Config;)V

    return-object v0
.end method

.method public final stableId(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Config;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    .line 295
    iput-boolean p1, p0, Lcom/squareup/cycler/Recycler$Config;->hasStableIdProvider:Z

    return-void
.end method
