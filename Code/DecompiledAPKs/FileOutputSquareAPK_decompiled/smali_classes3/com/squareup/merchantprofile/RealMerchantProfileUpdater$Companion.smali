.class public final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;",
        "",
        "()V",
        "FAILURE",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "JPEG_MEDIA_TYPE",
        "Lokhttp3/MediaType;",
        "TEXT_PLAIN",
        "buildErrorNotification",
        "Landroid/app/Notification;",
        "context",
        "Landroid/content/Context;",
        "notificationWrapper",
        "Lcom/squareup/notification/NotificationWrapper;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "logSecurityExceptionAndUri",
        "",
        "se",
        "Ljava/lang/SecurityException;",
        "featuredImageUri",
        "Landroid/net/Uri;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 310
    invoke-direct {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$buildErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;Landroid/content/Context;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Landroid/app/Notification;
    .locals 0

    .line 310
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$Companion;->buildErrorNotification(Landroid/content/Context;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Landroid/app/Notification;

    move-result-object p0

    return-object p0
.end method

.method private final buildErrorNotification(Landroid/content/Context;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Landroid/app/Notification;
    .locals 1

    .line 322
    sget v0, Lcom/squareup/merchantprofile/R$string;->merchant_profile_saving_profile_failed_title:I

    .line 321
    invoke-interface {p3, v0}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object p3

    .line 325
    sget-object v0, Lcom/squareup/notification/Channels;->MERCHANT_PROFILE:Lcom/squareup/notification/Channels;

    check-cast v0, Lcom/squareup/notification/Channel;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p2

    .line 326
    invoke-virtual {p2, p3}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p2

    .line 327
    invoke-virtual {p2, p3}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p2

    .line 328
    sget p3, Lcom/squareup/merchantprofile/R$string;->merchant_profile_saving_profile_failed_text:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p2, p3}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p2

    const-string p3, "SETTINGS"

    .line 330
    invoke-static {p1, p3}, Lcom/squareup/ui/main/PosIntentParser;->createPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p1

    .line 329
    invoke-virtual {p2, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 335
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    const-string p2, "notificationWrapper\n    \u2026     )\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final logSecurityExceptionAndUri(Ljava/lang/SecurityException;Landroid/net/Uri;)V
    .locals 3

    .line 341
    move-object v0, p1

    check-cast v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Caught SecurityException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method
