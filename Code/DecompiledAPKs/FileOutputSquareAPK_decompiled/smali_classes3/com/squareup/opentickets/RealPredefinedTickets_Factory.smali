.class public final Lcom/squareup/opentickets/RealPredefinedTickets_Factory;
.super Ljava/lang/Object;
.source "RealPredefinedTickets_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/opentickets/RealPredefinedTickets;",
        ">;"
    }
.end annotation


# instance fields
.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketGroupsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->cacheProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/opentickets/RealPredefinedTickets_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;)",
            "Lcom/squareup/opentickets/RealPredefinedTickets_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/cogs/Cogs;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lcom/squareup/tickets/TicketGroupsCache;)Lcom/squareup/opentickets/RealPredefinedTickets;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/opentickets/RealPredefinedTickets;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/opentickets/RealPredefinedTickets;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/cogs/Cogs;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lcom/squareup/tickets/TicketGroupsCache;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/opentickets/RealPredefinedTickets;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/Cogs;

    iget-object v3, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/opentickets/AvailableTemplateCountCache;

    iget-object v4, p0, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tickets/TicketGroupsCache;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/cogs/Cogs;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lcom/squareup/tickets/TicketGroupsCache;)Lcom/squareup/opentickets/RealPredefinedTickets;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealPredefinedTickets_Factory;->get()Lcom/squareup/opentickets/RealPredefinedTickets;

    move-result-object v0

    return-object v0
.end method
