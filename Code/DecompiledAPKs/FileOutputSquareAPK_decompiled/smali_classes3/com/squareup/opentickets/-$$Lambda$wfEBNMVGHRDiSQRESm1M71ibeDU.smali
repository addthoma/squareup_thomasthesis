.class public final synthetic Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;

    invoke-direct {v0}, Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;->INSTANCE:Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    check-cast p2, Lcom/squareup/tickets/TicketRowCursorList;

    invoke-static {p1, p2}, Lcom/squareup/opentickets/RealPredefinedTickets;->filterAvailableTicketTemplates(Ljava/util/List;Lcom/squareup/tickets/TicketRowCursorList;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
