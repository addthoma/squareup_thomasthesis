.class public final Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;
.super Ljava/lang/Object;
.source "TicketsModule_ProvideTicketDatabaseFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/TicketStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->contextProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->userDirProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTicketDatabase(Landroid/app/Application;Ljava/io/File;Lcom/squareup/util/Res;)Lcom/squareup/tickets/TicketStore;
    .locals 0

    .line 46
    invoke-static {p0, p1, p2}, Lcom/squareup/opentickets/TicketsModule;->provideTicketDatabase(Landroid/app/Application;Ljava/io/File;Lcom/squareup/util/Res;)Lcom/squareup/tickets/TicketStore;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/TicketStore;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/TicketStore;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->provideTicketDatabase(Landroid/app/Application;Ljava/io/File;Lcom/squareup/util/Res;)Lcom/squareup/tickets/TicketStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketsModule_ProvideTicketDatabaseFactory;->get()Lcom/squareup/tickets/TicketStore;

    move-result-object v0

    return-object v0
.end method
