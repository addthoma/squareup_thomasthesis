.class Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;
.super Ljava/lang/Object;
.source "SingleImagePicasso.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/merchantimages/SingleImagePicasso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UriOverride"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;
    }
.end annotation


# static fields
.field static final DEFAULT:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;


# instance fields
.field final lastSeenUri:Ljava/lang/String;

.field final override:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    invoke-direct {v0}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;-><init>()V

    sput-object v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->DEFAULT:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0, v0}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->lastSeenUri:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->override:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method withOverride(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    iget-object v1, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->lastSeenUri:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
