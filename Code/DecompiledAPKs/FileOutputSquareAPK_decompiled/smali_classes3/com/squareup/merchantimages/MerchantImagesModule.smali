.class public abstract Lcom/squareup/merchantimages/MerchantImagesModule;
.super Ljava/lang/Object;
.source "MerchantImagesModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideCuratedImage(Lcom/squareup/merchantimages/RealCuratedImage;)Lcom/squareup/merchantimages/CuratedImage;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
