.class public final Lcom/squareup/communications/RealMessageResolver;
.super Ljava/lang/Object;
.source "RealMessageResolver.kt"

# interfaces
.implements Lcom/squareup/communications/MessageResolver;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u001e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u000c\u0010\u000f\u001a\u00020\u0010*\u00020\u000bH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/communications/RealMessageResolver;",
        "Lcom/squareup/communications/MessageResolver;",
        "communicationsService",
        "Lcom/squareup/communications/service/CommunicationsService;",
        "(Lcom/squareup/communications/service/CommunicationsService;)V",
        "createTrackEngagementRequest",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
        "kotlin.jvm.PlatformType",
        "requestToken",
        "",
        "resolution",
        "Lcom/squareup/communications/MessageResolver$Resolution;",
        "resolve",
        "Lio/reactivex/Single;",
        "Lcom/squareup/communications/MessageResolver$Result;",
        "toAction",
        "Lcom/squareup/protos/messageservice/service/Action;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final communicationsService:Lcom/squareup/communications/service/CommunicationsService;


# direct methods
.method public constructor <init>(Lcom/squareup/communications/service/CommunicationsService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "communicationsService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/communications/RealMessageResolver;->communicationsService:Lcom/squareup/communications/service/CommunicationsService;

    return-void
.end method

.method private final createTrackEngagementRequest(Ljava/lang/String;Lcom/squareup/communications/MessageResolver$Resolution;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;-><init>()V

    .line 42
    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    move-result-object p1

    .line 43
    invoke-direct {p0, p2}, Lcom/squareup/communications/RealMessageResolver;->toAction(Lcom/squareup/communications/MessageResolver$Resolution;)Lcom/squareup/protos/messageservice/service/Action;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->action(Lcom/squareup/protos/messageservice/service/Action;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    return-object p1
.end method

.method private final toAction(Lcom/squareup/communications/MessageResolver$Resolution;)Lcom/squareup/protos/messageservice/service/Action;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/communications/MessageResolver$Resolution$Viewed;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Viewed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/protos/messageservice/service/Action;->VIEW:Lcom/squareup/protos/messageservice/service/Action;

    goto :goto_0

    .line 48
    :cond_0
    sget-object v0, Lcom/squareup/communications/MessageResolver$Resolution$Clicked;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Clicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/protos/messageservice/service/Action;->CLICK:Lcom/squareup/protos/messageservice/service/Action;

    goto :goto_0

    .line 49
    :cond_1
    sget-object v0, Lcom/squareup/communications/MessageResolver$Resolution$Dismissed;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Dismissed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/protos/messageservice/service/Action;->DISMISS:Lcom/squareup/protos/messageservice/service/Action;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public resolve(Ljava/lang/String;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/communications/MessageResolver$Resolution;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/communications/MessageResolver$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "requestToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resolution"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/communications/RealMessageResolver;->communicationsService:Lcom/squareup/communications/service/CommunicationsService;

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/communications/RealMessageResolver;->createTrackEngagementRequest(Ljava/lang/String;Lcom/squareup/communications/MessageResolver$Resolution;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    const-string p2, "createTrackEngagementReq\u2026requestToken, resolution)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/communications/service/CommunicationsService;->trackEngagement(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 31
    sget-object p2, Lcom/squareup/communications/RealMessageResolver$resolve$1;->INSTANCE:Lcom/squareup/communications/RealMessageResolver$resolve$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "communicationsService\n  \u2026ring())\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
