.class public final Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;
.super Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;
.source "IMMLeaks.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakfix/IMMLeaks;->fixFocusedViewLeak(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1",
        "Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;",
        "onActivityCreated",
        "",
        "activity",
        "Landroid/app/Activity;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $finishInputLockedMethod:Ljava/lang/reflect/Method;

.field final synthetic $inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field final synthetic $mHField:Ljava/lang/reflect/Field;

.field final synthetic $mServedViewField:Ljava/lang/reflect/Field;


# direct methods
.method constructor <init>(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/reflect/Field;Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iput-object p2, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$mHField:Ljava/lang/reflect/Field;

    iput-object p3, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$mServedViewField:Ljava/lang/reflect/Field;

    iput-object p4, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$finishInputLockedMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0}, Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    const-string p2, "activity"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance p2, Lcom/squareup/leakfix/IMMLeaks$ReferenceCleaner;

    .line 69
    iget-object v0, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$mHField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$mServedViewField:Ljava/lang/reflect/Field;

    .line 70
    iget-object v3, p0, Lcom/squareup/leakfix/IMMLeaks$fixFocusedViewLeak$1;->$finishInputLockedMethod:Ljava/lang/reflect/Method;

    .line 68
    invoke-direct {p2, v0, v1, v2, v3}, Lcom/squareup/leakfix/IMMLeaks$ReferenceCleaner;-><init>(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/reflect/Field;Ljava/lang/reflect/Field;Ljava/lang/reflect/Method;)V

    .line 72
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const-string v0, "activity.window"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    const-string v0, "activity.window\n            .decorView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p1

    const-string v0, "rootView"

    .line 75
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    .line 76
    check-cast p2, Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    return-void
.end method
