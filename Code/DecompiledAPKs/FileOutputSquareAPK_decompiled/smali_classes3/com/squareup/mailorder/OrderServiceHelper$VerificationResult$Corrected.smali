.class public final Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;
.super Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Corrected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u000e\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "res",
        "Landroid/content/res/Resources;",
        "address",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "verifiedAddressToken",
        "",
        "(Landroid/content/res/Resources;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;)V",
        "getAddress",
        "()Lcom/squareup/protos/common/location/GlobalAddress;",
        "message",
        "getMessage",
        "()Ljava/lang/String;",
        "title",
        "getTitle",
        "getVerifiedAddressToken",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final address:Lcom/squareup/protos/common/location/GlobalAddress;

.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final verifiedAddressToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 123
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->verifiedAddressToken:Ljava/lang/String;

    .line 125
    sget p2, Lcom/squareup/common/strings/R$string;->order_validation_modified_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "res.getString(com.square\u2026alidation_modified_title)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->title:Ljava/lang/String;

    .line 127
    sget p2, Lcom/squareup/common/strings/R$string;->order_validation_modified_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "res.getString(com.square\u2026validation_modified_text)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getAddress()Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getVerifiedAddressToken()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->verifiedAddressToken:Ljava/lang/String;

    return-object v0
.end method
