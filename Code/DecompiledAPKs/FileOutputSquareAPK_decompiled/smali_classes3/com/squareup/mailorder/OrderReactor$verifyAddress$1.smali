.class final Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;
.super Ljava/lang/Object;
.source "OrderReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor;->verifyAddress(Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "result",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->this$0:Lcom/squareup/mailorder/OrderReactor;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    .line 374
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 375
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    .line 376
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->getVerifiedAddressToken()Ljava/lang/String;

    move-result-object v4

    .line 377
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v5

    .line 378
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->getGlobalAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v6

    .line 379
    sget-object v7, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;->ENTER_ADDRESS:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    move-object v1, v0

    .line 373
    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto/16 :goto_0

    .line 381
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;

    if-eqz v0, :cond_2

    .line 382
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderReactor;->access$getConfiguration$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$Configuration;->getAllowSubmittingNonUSPSRecognizedAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    .line 384
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 385
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    .line 386
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v4

    .line 387
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->getVerifiedAddressToken()Ljava/lang/String;

    move-result-object v5

    .line 388
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v6

    .line 389
    sget-object v1, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->getAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/address/Address$Companion;->fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v7

    move-object v1, v0

    .line 383
    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto/16 :goto_0

    .line 392
    :cond_1
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    .line 393
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 394
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    .line 395
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 396
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 397
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Corrected;->getAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v6

    move-object v1, v0

    .line 392
    invoke-direct/range {v1 .. v6}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto/16 :goto_0

    .line 401
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;

    if-eqz v0, :cond_4

    .line 402
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderReactor;->access$getConfiguration$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$Configuration;->getAllowSubmittingNonUSPSRecognizedAddress()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 403
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    .line 404
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 405
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    .line 406
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v4

    .line 407
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->getToken()Ljava/lang/String;

    move-result-object v5

    .line 408
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v6

    move-object v1, v0

    .line 403
    invoke-direct/range {v1 .. v6}, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto :goto_0

    .line 411
    :cond_3
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;

    .line 412
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v1

    .line 413
    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    .line 414
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 415
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 411
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto :goto_0

    .line 419
    :cond_4
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    .line 420
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v1

    .line 421
    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    .line 422
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 423
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 419
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto :goto_0

    .line 425
    :cond_5
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    .line 426
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getItemToken()Ljava/lang/String;

    move-result-object v1

    .line 427
    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    .line 428
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 429
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServerError;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 425
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState;

    .line 432
    :goto_0
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1

    .line 425
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;->apply(Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
