.class public final Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
.super Ljava/lang/Object;
.source "OrderConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderConfirmationCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
        "",
        "orderConfirmedSubmessage",
        "",
        "orderConfirmedHelpMessage",
        "(II)V",
        "getOrderConfirmedHelpMessage",
        "()I",
        "getOrderConfirmedSubmessage",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderConfirmedHelpMessage:I

.field private final orderConfirmedSubmessage:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, v0, v1, v2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->orderConfirmedSubmessage:I

    iput p2, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->orderConfirmedHelpMessage:I

    return-void
.end method

.method public synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, -0x1

    if-eqz p4, :cond_0

    const/4 p1, -0x1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, -0x1

    .line 44
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;-><init>(II)V

    return-void
.end method


# virtual methods
.method public final getOrderConfirmedHelpMessage()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->orderConfirmedHelpMessage:I

    return v0
.end method

.method public final getOrderConfirmedSubmessage()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->orderConfirmedSubmessage:I

    return v0
.end method
