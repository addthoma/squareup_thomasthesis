.class public final Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderConfirmationCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
        "",
        "configuration",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
        "(Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V",
        "create",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        "Lcom/squareup/mailorder/OrderConfirmationScreen;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;->configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/mailorder/OrderConfirmationCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
            ">;>;)",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;->configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
