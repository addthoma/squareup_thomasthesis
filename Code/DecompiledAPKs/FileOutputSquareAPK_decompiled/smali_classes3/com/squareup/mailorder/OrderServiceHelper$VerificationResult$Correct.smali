.class public final Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;
.super Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Correct"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "verifiedAddressToken",
        "",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "(Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)V",
        "getContactInfo",
        "()Lcom/squareup/mailorder/ContactInfo;",
        "getGlobalAddress",
        "()Lcom/squareup/protos/common/location/GlobalAddress;",
        "getVerifiedAddressToken",
        "()Ljava/lang/String;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contactInfo:Lcom/squareup/mailorder/ContactInfo;

.field private final globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

.field private final verifiedAddressToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)V
    .locals 1

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "globalAddress"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 117
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->verifiedAddressToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    return-void
.end method


# virtual methods
.method public final getContactInfo()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final getGlobalAddress()Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->globalAddress:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object v0
.end method

.method public final getVerifiedAddressToken()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Correct;->verifiedAddressToken:Ljava/lang/String;

    return-object v0
.end method
