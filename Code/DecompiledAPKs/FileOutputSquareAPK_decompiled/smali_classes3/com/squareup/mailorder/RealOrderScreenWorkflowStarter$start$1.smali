.class final Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0002j\u0008\u0012\u0004\u0012\u00020\u0003`\u00060\u00012\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "reactorState",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/rx1/Workflow;

.field final synthetic this$0:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->this$0:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/workflow/ScreenState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ")",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "reactorState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    .line 61
    sget-object v1, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->ScreenResolver:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    iget-object v2, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object v3, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->this$0:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;

    invoke-static {v3}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->access$getFeatures$p(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/settings/server/Features;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->this$0:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;

    invoke-static {v4}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->access$getSettings$p(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v4

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->toScreenStack(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/Map;

    move-result-object v1

    .line 62
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 60
    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;->invoke(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method
