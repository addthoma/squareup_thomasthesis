.class final Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;
.super Ljava/lang/Object;
.source "RealRolodexAttachmentLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexAttachmentLoader;->fetch(Ljava/lang/String;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\"\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00010\u00012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "",
        "Lcom/squareup/protos/client/rolodex/Attachment;",
        "kotlin.jvm.PlatformType",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Ljava/lang/String;

.field final synthetic $pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$input:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/datafetch/AbstractLoader$Response;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;)",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$input:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    const-string v3, ""

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/datafetch/AbstractLoader$Response;

    goto :goto_0

    .line 59
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/datafetch/AbstractLoader$Response$Error;

    iget-object v0, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$input:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    new-instance v2, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    new-instance v3, Ljava/lang/Throwable;

    const-string v4, "Request failed."

    invoke-direct {v3, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    check-cast v2, Lcom/squareup/datafetch/LoaderError;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/datafetch/AbstractLoader$Response$Error;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/datafetch/AbstractLoader$Response;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexAttachmentLoader$fetch$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/datafetch/AbstractLoader$Response;

    move-result-object p1

    return-object p1
.end method
