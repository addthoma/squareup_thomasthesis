.class public final Lcom/squareup/crm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final card_on_file_expired:I = 0x7f12031c

.field public static final crm_cardonfile_savecard_save_failure:I = 0x7f12062e

.field public static final crm_cardonfile_savecard_save_failure_subtitle:I = 0x7f12062f

.field public static final crm_cardonfile_savecard_saved:I = 0x7f120630

.field public static final crm_cardonfile_savecard_saving:I = 0x7f120631

.field public static final crm_cardonfile_unlink_card_content_description:I = 0x7f120633

.field public static final crm_contact_default_display_header_label:I = 0x7f120656

.field public static final crm_contact_default_display_initials:I = 0x7f120657

.field public static final crm_contact_default_display_name:I = 0x7f120658

.field public static final crm_contact_search_empty:I = 0x7f12065e

.field public static final crm_contact_search_error:I = 0x7f12065f

.field public static final crm_coupon_creation:I = 0x7f120663

.field public static final crm_coupon_details:I = 0x7f120664

.field public static final crm_coupon_expiration:I = 0x7f120665

.field public static final crm_coupon_redeemed_date:I = 0x7f120666

.field public static final crm_create_new_customer_label:I = 0x7f12067a

.field public static final crm_customer_name_row_content_description:I = 0x7f120690

.field public static final crm_failed_to_load_customers:I = 0x7f1206c2

.field public static final crm_group_name_hint:I = 0x7f1206d8

.field public static final crm_profile_attachments_filename_template:I = 0x7f120738

.field public static final crm_reminder_row:I = 0x7f120751

.field public static final crm_search_customers_hint:I = 0x7f120767

.field public static final crm_search_customers_hint_filtering:I = 0x7f120768

.field public static final customer_unit_contact_full_name_format:I = 0x7f1207ab

.field public static final customer_unit_contact_info_format:I = 0x7f1207ac

.field public static final date_format:I = 0x7f1207c9

.field public static final date_unit_days:I = 0x7f1207d3

.field public static final date_unit_months:I = 0x7f1207d4

.field public static final date_unit_weeks:I = 0x7f1207d5

.field public static final date_unit_years:I = 0x7f1207d6

.field public static final expiration_policy:I = 0x7f120a9d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
