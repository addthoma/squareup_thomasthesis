.class public final Lcom/squareup/crm/RealRolodexGroupLoader;
.super Ljava/lang/Object;
.source "RealRolodexGroupLoader.kt"

# interfaces
.implements Lcom/squareup/crm/RolodexGroupLoader;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RealRolodexGroupLoader$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 22\u00020\u0001:\u00012B+\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0013H\u0016J\u0014\u0010\u001c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001f0\u001e0\u001dH\u0002J\u0014\u0010 \u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020!0\u001e0\u001dH\u0002J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0013H\u0016J\u0016\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00132\u0006\u0010#\u001a\u00020$H\u0016J\u001c\u0010%\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0&0\u00132\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010\'\u001a\u00020\u00182\u0006\u0010(\u001a\u00020)H\u0016J\u0008\u0010*\u001a\u00020\u0018H\u0016J\u0010\u0010+\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u001fH\u0002J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0013H\u0016J\u0008\u0010\u0016\u001a\u00020\u0018H\u0016J\u0008\u0010-\u001a\u00020\u0018H\u0002J\u0010\u0010.\u001a\u00020\u00182\u0006\u0010/\u001a\u00020\u0010H\u0016J\u0014\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0\u001a0\u0013H\u0016J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00132\u0006\u00100\u001a\u000201H\u0016J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00132\u0006\u0010#\u001a\u00020$H\u0016R\u001c\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000f\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\u00100\u00100\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\u00100\u00100\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0016\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\u00180\u00180\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0019\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u001b \u000e*\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001a0\u001a0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/crm/RealRolodexGroupLoader;",
        "Lcom/squareup/crm/RolodexGroupLoader;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V",
        "allCustomersCount",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "failure",
        "",
        "includeCounts",
        "internetState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/connectivity/InternetState;",
        "progress",
        "refresh",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "success",
        "",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "doGetMerchantRequest",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
        "doListGroupsRequest",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        "group",
        "groupToken",
        "",
        "groupOrEmpty",
        "Lcom/squareup/util/Optional;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onRolodexResponse",
        "response",
        "retryIfLastError",
        "setIncludeCounts",
        "value",
        "audienceType",
        "Lcom/squareup/protos/client/rolodex/AudienceType;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/RealRolodexGroupLoader$Companion;

.field private static final REFRESH_INTERVAL_MINUTES:J = 0x2L

.field private static final REFRESH_THROTTLE_MS:J = 0xc8L

.field private static final TOTAL_COUNT_UNDEFINED:I = -0x1


# instance fields
.field private final allCustomersCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private includeCounts:Z

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final refresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final success:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;>;"
        }
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/crm/RealRolodexGroupLoader$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/RealRolodexGroupLoader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/RealRolodexGroupLoader;->Companion:Lcom/squareup/crm/RealRolodexGroupLoader$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodex"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p4, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 37
    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "connectivityMonitor.internetState()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->internetState:Lio/reactivex/Observable;

    .line 38
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Int>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->allCustomersCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 39
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<List<Group>>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->success:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 p1, 0x0

    .line 40
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Boolean>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$doGetMerchantRequest(Lcom/squareup/crm/RealRolodexGroupLoader;)Lio/reactivex/Single;
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/crm/RealRolodexGroupLoader;->doGetMerchantRequest()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$doListGroupsRequest(Lcom/squareup/crm/RealRolodexGroupLoader;)Lio/reactivex/Single;
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/crm/RealRolodexGroupLoader;->doListGroupsRequest()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFailure$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getIncludeCounts$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Z
    .locals 0

    .line 31
    iget-boolean p0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->includeCounts:Z

    return p0
.end method

.method public static final synthetic access$getProgress$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getSuccess$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->success:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getThreadEnforcer$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method public static final synthetic access$onRolodexResponse(Lcom/squareup/crm/RealRolodexGroupLoader;Lcom/squareup/protos/client/rolodex/GetMerchantResponse;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/crm/RealRolodexGroupLoader;->onRolodexResponse(Lcom/squareup/protos/client/rolodex/GetMerchantResponse;)V

    return-void
.end method

.method public static final synthetic access$retryIfLastError(Lcom/squareup/crm/RealRolodexGroupLoader;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/crm/RealRolodexGroupLoader;->retryIfLastError()V

    return-void
.end method

.method public static final synthetic access$setIncludeCounts$p(Lcom/squareup/crm/RealRolodexGroupLoader;Z)V
    .locals 0

    .line 31
    iput-boolean p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->includeCounts:Z

    return-void
.end method

.method private final doGetMerchantRequest()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
            ">;>;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-boolean v1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->includeCounts:Z

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getMerchant(Z)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final doListGroupsRequest()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
            ">;>;"
        }
    .end annotation

    .line 165
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-boolean v1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->includeCounts:Z

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->listGroups(Z)Lio/reactivex/Single;

    move-result-object v0

    .line 166
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$doListGroupsRequest$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$doListGroupsRequest$1;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 170
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$doListGroupsRequest$2;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$doListGroupsRequest$2;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "rolodex.listGroups(inclu\u2026s.accept(false)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final groupOrEmpty(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;>;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->success:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 147
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$groupOrEmpty$1;

    invoke-direct {v1, p1}, Lcom/squareup/crm/RealRolodexGroupLoader$groupOrEmpty$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 150
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "success\n        .map { g\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onRolodexResponse(Lcom/squareup/protos/client/rolodex/GetMerchantResponse;)V
    .locals 1

    .line 177
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Merchant;->num_customers:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->allCustomersCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetMerchantResponse;->merchant:Lcom/squareup/protos/client/rolodex/Merchant;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Merchant;->num_customers:Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 181
    :cond_0
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->allCustomersCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final retryIfLastError()V
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 155
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public allCustomersCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->allCustomersCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "allCustomersCount.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public failure()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "failure.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public group(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    const-string v0, "groupToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/crm/RealRolodexGroupLoader;->groupOrEmpty(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 114
    invoke-static {p1}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 8

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->internetState:Lio/reactivex/Observable;

    .line 47
    sget-object v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$1;->INSTANCE:Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "internetState\n        .filter { it == CONNECTED }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$2;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 51
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 52
    iget-object v1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/Observable;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2

    invoke-static/range {v2 .. v7}, Lio/reactivex/Observable;->interval(JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "Observable.interval(0, R\u2026, MINUTES, mainScheduler)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 53
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v3, v4, v1, v2}, Lio/reactivex/Observable;->throttleFirst(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$3;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "onRefresh\n        .switc\u2026{ doListGroupsRequest() }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v2, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;

    invoke-direct {v2, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 71
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$6;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$6;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onRefresh\n        .filte\u2026rchantRequest()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$7;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$7;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 85
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexServiceHelper;->onContactsAddedOrRemoved()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v1}, Lcom/squareup/crm/RolodexServiceHelper;->onGroupsAddedOrRemoved()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 84
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026upsAddedOrRemoved()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$8;

    iget-object v2, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {v1, v2}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$8;-><init>(Lcom/jakewharton/rxrelay2/PublishRelay;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public progress()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "progress.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public refresh()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 141
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setIncludeCounts(Z)V
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->includeCounts:Z

    return-void
.end method

.method public success()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;>;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->success:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$success$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexGroupLoader$success$1;-><init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "success.doOnSubscribe { \u2026s.hasValue()) refresh() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public success(Lcom/squareup/protos/client/rolodex/AudienceType;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/AudienceType;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    const-string v0, "audienceType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader;->success:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 120
    new-instance v1, Lcom/squareup/crm/RealRolodexGroupLoader$success$3;

    invoke-direct {v1, p1}, Lcom/squareup/crm/RealRolodexGroupLoader$success$3;-><init>(Lcom/squareup/protos/client/rolodex/AudienceType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 123
    sget-object v0, Lcom/squareup/crm/RealRolodexGroupLoader$success$4;->INSTANCE:Lcom/squareup/crm/RealRolodexGroupLoader$success$4;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->takeWhile(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "success\n        .map { g\u2026akeWhile { it.isPresent }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {p1}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public success(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    const-string v0, "groupToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0, p1}, Lcom/squareup/crm/RealRolodexGroupLoader;->groupOrEmpty(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 107
    sget-object v0, Lcom/squareup/crm/RealRolodexGroupLoader$success$2;->INSTANCE:Lcom/squareup/crm/RealRolodexGroupLoader$success$2;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->takeWhile(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "groupOrEmpty(groupToken)\u2026akeWhile { it.isPresent }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static {p1}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
