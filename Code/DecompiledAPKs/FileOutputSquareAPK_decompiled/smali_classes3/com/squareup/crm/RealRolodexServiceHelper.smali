.class public final Lcom/squareup/crm/RealRolodexServiceHelper;
.super Ljava/lang/Object;
.source "RealRolodexServiceHelper.kt"

# interfaces
.implements Lcom/squareup/crm/RolodexServiceHelper;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RealRolodexServiceHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRolodexServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRolodexServiceHelper.kt\ncom/squareup/crm/RealRolodexServiceHelper\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,772:1\n132#2,3:773\n1360#3:776\n1429#3,3:777\n*E\n*S KotlinDebug\n*F\n+ 1 RealRolodexServiceHelper.kt\ncom/squareup/crm/RealRolodexServiceHelper\n*L\n396#1,3:773\n512#1:776\n512#1,3:777\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a4\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u009e\u00012\u00020\u0001:\u0002\u009e\u0001BK\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J.\u0010\u0017\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001a0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020\u001a`\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016JJ\u0010 \u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020!0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020!`\u001b2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u001f0#2\u0006\u0010&\u001a\u00020$2\u0006\u0010\'\u001a\u00020\u001fH\u0016J@\u0010(\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020)0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020)`\u001b2\u0006\u0010*\u001a\u00020\u001f2\u0006\u0010+\u001a\u00020\u001f2\u0008\u0010,\u001a\u0004\u0018\u00010-2\u0006\u0010.\u001a\u00020/H\u0017J&\u00100\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002010\u00190\u0018j\u0008\u0012\u0004\u0012\u000201`\u001b2\u0006\u00102\u001a\u00020\u001fH\u0016J&\u00103\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002040\u00190\u0018j\u0008\u0012\u0004\u0012\u000204`\u001b2\u0006\u00105\u001a\u000206H\u0016J&\u00107\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002080\u00190\u0018j\u0008\u0012\u0004\u0012\u000208`\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J&\u00109\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020:0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020:`\u001b2\u0006\u0010;\u001a\u00020<H\u0016J&\u0010=\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020>0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020>`\u001b2\u0006\u0010?\u001a\u00020@H\u0016J.\u0010A\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020B0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020B`\u001b2\u0006\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020FH\u0016J.\u0010G\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020H0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020H`\u001b2\u0006\u00102\u001a\u00020\u001f2\u0006\u0010I\u001a\u00020FH\u0016J\u001e\u0010J\u001a\u0004\u0018\u00010\u001f2\u0008\u0010K\u001a\u0004\u0018\u00010\u001f2\u0008\u0010L\u001a\u0004\u0018\u00010\u001fH\u0002J&\u0010M\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020N0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020N`\u001b2\u0006\u0010*\u001a\u00020\u001fH\u0016J.\u0010M\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020N0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020N`\u001b2\u0006\u0010*\u001a\u00020\u001f2\u0006\u0010O\u001a\u00020FH\u0016J\n\u0010P\u001a\u0004\u0018\u00010QH\u0016J&\u0010R\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020S0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020S`\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J&\u0010T\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020U0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020U`\u001b2\u0006\u0010V\u001a\u00020FH\u0016Je\u0010W\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020X0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020X`\u001b2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0008\u0010Y\u001a\u0004\u0018\u00010\u001f2\u000e\u0010Z\u001a\n\u0012\u0004\u0012\u00020[\u0018\u00010#2\u0008\u0010\\\u001a\u0004\u0018\u00010]2\u0008\u0010^\u001a\u0004\u0018\u00010\u001f2\u0008\u0010_\u001a\u0004\u0018\u00010`H\u0016\u00a2\u0006\u0002\u0010aJ?\u0010b\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020c0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020c`\u001b2\u0006\u0010*\u001a\u00020\u001f2\u0008\u0010d\u001a\u0004\u0018\u00010`2\u0008\u0010^\u001a\u0004\u0018\u00010\u001fH\u0016\u00a2\u0006\u0002\u0010eJ\u001e\u0010f\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020g0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020g`\u001bH\u0016J&\u0010h\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020i0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020i`\u001b2\u0006\u0010V\u001a\u00020FH\u0016J7\u0010j\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020k0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020k`\u001b2\u0008\u0010d\u001a\u0004\u0018\u00010`2\u0008\u0010^\u001a\u0004\u0018\u00010\u001fH\u0016\u00a2\u0006\u0002\u0010lJ.\u0010m\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020n0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020n`\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010.\u001a\u00020/H\u0016J8\u0010m\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020n0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020n`\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010.\u001a\u00020/2\u0008\u0010o\u001a\u0004\u0018\u00010pH\u0016J\u0014\u0010q\u001a\u00020\u00142\n\u0010r\u001a\u0006\u0012\u0002\u0008\u00030\u0019H\u0002J\u0014\u0010s\u001a\u00020\u00142\n\u0010r\u001a\u0006\u0012\u0002\u0008\u00030\u0019H\u0002J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140tH\u0016J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00140tH\u0016J\u001e\u0010u\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020v0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020v`\u001bH\u0016J\u0008\u0010w\u001a\u00020xH\u0016J(\u0010y\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020z0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020z`\u001b2\u0008\u0010{\u001a\u0004\u0018\u00010\u001fH\u0016J.\u0010|\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020}0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020}`\u001b2\u0006\u0010*\u001a\u00020\u001f2\u0006\u0010~\u001a\u00020\u001fH\u0016J9\u0010\u007f\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0080\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u0080\u0001`\u001b2\u0006\u00102\u001a\u00020\u001f2\u0007\u0010\u0081\u0001\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020\u001fH\u0016J/\u0010\u0082\u0001\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020)0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020)`\u001b2\u0006\u0010?\u001a\u00020@2\u0006\u0010+\u001a\u00020\u001fH\u0017J:\u0010\u0083\u0001\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020H0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020H`\u001b2\u0006\u0010*\u001a\u00020\u001f2\u0007\u0010\u0081\u0001\u001a\u00020\u001f2\u0008\u0010\u0084\u0001\u001a\u00030\u0085\u0001H\u0016J=\u0010\u0086\u0001\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020!0\u00190\u0018j\u0008\u0012\u0004\u0012\u00020!`\u001b2\r\u0010\u0087\u0001\u001a\u0008\u0012\u0004\u0012\u00020$0#2\r\u0010\u0088\u0001\u001a\u0008\u0012\u0004\u0012\u00020\u001f0#H\u0016J1\u0010\u0089\u0001\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u008a\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u008a\u0001`\u001b2\u0006\u00105\u001a\u0002062\u0006\u0010.\u001a\u00020/H\u0016J>\u0010\u008b\u0001\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u008c\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u008c\u0001`\u001b2\u0007\u0010\u008d\u0001\u001a\u00020/2\u0008\u0010{\u001a\u0004\u0018\u00010\u001f2\u0008\u00105\u001a\u0004\u0018\u000106H\u0016J1\u0010\u008e\u0001\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u008f\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u008f\u0001`\u001b2\u0006\u0010;\u001a\u00020<2\u0006\u0010.\u001a\u00020/H\u0016J*\u0010\u0090\u0001\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0091\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u0091\u0001`\u001b2\u0007\u0010;\u001a\u00030\u0092\u0001H\u0016Jl\u0010\u0093\u0001\u001a\u001a\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0094\u00010\u00190\u0018j\t\u0012\u0005\u0012\u00030\u0094\u0001`\u001b2\u0006\u0010*\u001a\u00020\u001f2\t\u0010\u0095\u0001\u001a\u0004\u0018\u00010\u001f2\u0008\u0010\u0096\u0001\u001a\u00030\u0097\u00012\n\u0010\u0098\u0001\u001a\u0005\u0018\u00010\u0099\u00012\n\u0010\u009a\u0001\u001a\u0005\u0018\u00010\u009b\u00012\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010\u001f2\t\u0010\u009d\u0001\u001a\u0004\u0018\u00010\u001fH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0012\u001a\u0010\u0012\u000c\u0012\n \u0015*\u0004\u0018\u00010\u00140\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0016\u001a\u0010\u0012\u000c\u0012\n \u0015*\u0004\u0018\u00010\u00140\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u009f\u0001"
    }
    d2 = {
        "Lcom/squareup/crm/RealRolodexServiceHelper;",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "rolodexService",
        "Lcom/squareup/server/crm/RolodexService;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "rpcScheduler",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "unique",
        "Lcom/squareup/util/Unique;",
        "(Lcom/squareup/server/crm/RolodexService;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Unique;)V",
        "onContactsAddedOrRemoved",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "onGroupsAddedOrRemoved",
        "addContactsToGroup",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
        "Lcom/squareup/crm/Result;",
        "contactSet",
        "Lcom/squareup/protos/client/rolodex/ContactSet;",
        "groupToken",
        "",
        "createNewProfileAttribute",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
        "existingAttributeDefinitions",
        "",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "existingAttributeKeysInOrder",
        "newAttributeDefinition",
        "newAttributeKey",
        "createNote",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
        "contactToken",
        "body",
        "reminder",
        "Lcom/squareup/protos/client/rolodex/Reminder;",
        "requestUuid",
        "Ljava/util/UUID;",
        "deleteAttachment",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
        "attachmentToken",
        "deleteContact",
        "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "deleteContacts",
        "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
        "deleteGroup",
        "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
        "group",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "deleteNote",
        "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
        "note",
        "Lcom/squareup/protos/client/rolodex/Note;",
        "dismissMergeProposal",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
        "mergeProposal",
        "Lcom/squareup/protos/client/rolodex/MergeProposal;",
        "dismissed",
        "",
        "downloadAttachment",
        "Lokhttp3/ResponseBody;",
        "isPreview",
        "formatEmployeeName",
        "firstName",
        "lastName",
        "getContact",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "includeAttachments",
        "getCurrentEmployee",
        "Lcom/squareup/protos/client/Employee;",
        "getManualMergeProposal",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
        "getMerchant",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
        "includeCounts",
        "listContacts",
        "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
        "searchTerm",
        "filterList",
        "Lcom/squareup/protos/client/rolodex/Filter;",
        "sortType",
        "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
        "pagingKey",
        "pageSize",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;",
        "listEventsForContact",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
        "limit",
        "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;",
        "listFilterTemplates",
        "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
        "listGroups",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        "listMergeProposals",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "(Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;",
        "manualMergeContacts",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
        "loyaltyAccountMapping",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "notifyContactsAddedOrRemoved",
        "successOrFailure",
        "notifyGroupsAddedOrRemoved",
        "Lio/reactivex/Observable;",
        "runMergeAllJob",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "runMergeProposalJob",
        "Lio/reactivex/Completable;",
        "shouldShowEmailCollection",
        "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
        "paymentToken",
        "unlinkInstrumentOnFile",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
        "instrumentToken",
        "updateAttachment",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
        "fileName",
        "updateNote",
        "uploadAttachment",
        "requestBody",
        "Lokhttp3/RequestBody;",
        "upsertAttributeSchema",
        "attributeDefinitions",
        "attributeKeysInOrder",
        "upsertContact",
        "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
        "upsertContactForEmailCollection",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
        "requestToken",
        "upsertManualGroup",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
        "upsertSmartGroup",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        "verifyAndLinkCard",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
        "cardholderName",
        "card",
        "Lcom/squareup/Card;",
        "cardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "entryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "postalCode",
        "uniqueKey",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/RealRolodexServiceHelper$Companion;

.field public static final MERGE_POLL_SECONDS:J = 0x1L

.field private static final TEXT_PLAIN:Lokhttp3/MediaType;


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final onContactsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onGroupsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodexService:Lcom/squareup/server/crm/RolodexService;

.field private final rpcScheduler:Lio/reactivex/Scheduler;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final unique:Lcom/squareup/util/Unique;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/RealRolodexServiceHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/RealRolodexServiceHelper;->Companion:Lcom/squareup/crm/RealRolodexServiceHelper$Companion;

    .line 768
    sget-object v0, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Lokhttp3/MediaType$Companion;->get(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/crm/RealRolodexServiceHelper;->TEXT_PLAIN:Lokhttp3/MediaType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/crm/RolodexService;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Unique;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rolodexService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rpcScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unique"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    iput-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rpcScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p6, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->res:Lcom/squareup/util/Res;

    iput-object p7, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->features:Lcom/squareup/settings/server/Features;

    iput-object p8, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->unique:Lcom/squareup/util/Unique;

    .line 130
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onContactsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 131
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onGroupsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getOnContactsAddedOrRemoved$p(Lcom/squareup/crm/RealRolodexServiceHelper;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onContactsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getRolodexService$p(Lcom/squareup/crm/RealRolodexServiceHelper;)Lcom/squareup/server/crm/RolodexService;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    return-object p0
.end method

.method public static final synthetic access$getRpcScheduler$p(Lcom/squareup/crm/RealRolodexServiceHelper;)Lio/reactivex/Scheduler;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rpcScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$notifyContactsAddedOrRemoved(Lcom/squareup/crm/RealRolodexServiceHelper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/crm/RealRolodexServiceHelper;->notifyContactsAddedOrRemoved(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public static final synthetic access$notifyGroupsAddedOrRemoved(Lcom/squareup/crm/RealRolodexServiceHelper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/crm/RealRolodexServiceHelper;->notifyGroupsAddedOrRemoved(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method private final formatEmployeeName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 739
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    xor-int/2addr v3, v2

    .line 740
    move-object v4, p2

    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_2

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    xor-int/2addr v1, v2

    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    .line 744
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/employees/R$string;->crm_employee_full_name:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p2, "first_name"

    .line 745
    invoke-virtual {p1, p2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p2, "last_name"

    .line 746
    invoke-virtual {p1, p2, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 747
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 748
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_5

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    move-object p1, p2

    goto :goto_2

    .line 751
    :cond_6
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/employees/R$string;->employee_short_name_unknown:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private final notifyContactsAddedOrRemoved(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "*>;)V"
        }
    .end annotation

    .line 756
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    .line 757
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onContactsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final notifyGroupsAddedOrRemoved(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "*>;)V"
        }
    .end annotation

    .line 762
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    .line 763
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onGroupsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addContactsToGroup(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    new-instance v0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;-><init>()V

    .line 156
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->group_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;

    move-result-object p2

    .line 157
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;

    move-result-object p1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;

    move-result-object p1

    .line 160
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->addContactsToGroups(Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 161
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 162
    new-instance p2, Lcom/squareup/crm/RealRolodexServiceHelper$addContactsToGroup$1;

    invoke-direct {p2, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$addContactsToGroup$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.addContac\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public createNewProfileAttribute(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "existingAttributeDefinitions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingAttributeKeysInOrder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newAttributeDefinition"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newAttributeKey"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 710
    check-cast p1, Ljava/util/Collection;

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 711
    check-cast p2, Ljava/util/Collection;

    invoke-static {p2, p4}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    .line 709
    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/RealRolodexServiceHelper;->upsertAttributeSchema(Ljava/util/List;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public createNote(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Reminder;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Reminder;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use custom attributes instead"
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUuid"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;-><init>()V

    .line 363
    invoke-virtual {p4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object v0

    .line 364
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object v0

    .line 366
    new-instance v1, Lcom/squareup/protos/client/rolodex/Note$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/Note$Builder;-><init>()V

    .line 367
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/rolodex/Note$Builder;->body(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object p2

    .line 368
    invoke-virtual {p4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/rolodex/Note$Builder;->note_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object p2

    .line 369
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/rolodex/Note$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object p1

    .line 371
    new-instance p2, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 372
    invoke-virtual {p0}, Lcom/squareup/crm/RealRolodexServiceHelper;->getCurrentEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p2

    .line 373
    invoke-virtual {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p2

    .line 370
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/Note$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object p1

    .line 375
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/rolodex/Note$Builder;->reminder(Lcom/squareup/protos/client/rolodex/Reminder;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object p1

    .line 376
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Note$Builder;->build()Lcom/squareup/protos/client/rolodex/Note;

    move-result-object p1

    .line 365
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->note(Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object p1

    .line 378
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;

    move-result-object p1

    .line 380
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->upsertNote(Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 381
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 382
    sget-object p2, Lcom/squareup/crm/RealRolodexServiceHelper$createNote$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$createNote$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.upsertNot\u2026response.note != null } }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public deleteAttachment(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "attachmentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 658
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;-><init>()V

    .line 659
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->attachment_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;

    move-result-object p1

    .line 660
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;

    move-result-object p1

    .line 662
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->deleteAttachment(Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 663
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public deleteContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteContactRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteContactRequest$Builder;-><init>()V

    .line 143
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteContactRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/DeleteContactRequest$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteContactRequest;

    move-result-object p1

    .line 146
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->deleteContact(Lcom/squareup/protos/client/rolodex/DeleteContactRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 148
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$deleteContact$1;

    invoke-direct {v0, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$deleteContact$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodexService.deleteCon\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public deleteContacts(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteContactsRequest$Builder;-><init>()V

    .line 169
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteContactsRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/DeleteContactsRequest$Builder;

    move-result-object p1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;

    move-result-object p1

    .line 172
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->deleteContacts(Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 173
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 174
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$deleteContacts$1;

    invoke-direct {v0, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$deleteContacts$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodexService.deleteCon\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public deleteGroup(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "group"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;-><init>()V

    .line 181
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteGroupRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;

    move-result-object p1

    .line 184
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->deleteGroup(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 186
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$deleteGroup$1;

    invoke-direct {v0, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$deleteGroup$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodexService.deleteGro\u2026roupsAddedOrRemoved(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public deleteNote(Lcom/squareup/protos/client/rolodex/Note;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "note"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;-><init>()V

    .line 193
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;

    move-result-object v0

    .line 194
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;->note(Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;

    move-result-object p1

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteNoteRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;

    move-result-object p1

    .line 197
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->deleteNote(Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public dismissMergeProposal(Lcom/squareup/protos/client/rolodex/MergeProposal;Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "mergeProposal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 512
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->duplicate_contacts:Ljava/util/List;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    check-cast p1, Ljava/lang/Iterable;

    .line 776
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 777
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 778
    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 512
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 779
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 513
    new-instance p1, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;-><init>()V

    .line 515
    new-instance v1, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet$Builder;-><init>()V

    .line 516
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet$Builder;->duplicate_contact_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet$Builder;

    move-result-object v0

    .line 517
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet$Builder;->build()Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    move-result-object v0

    .line 514
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;

    move-result-object p1

    .line 519
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->dismissed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;

    move-result-object p1

    .line 520
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;

    move-result-object p1

    .line 522
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->dismissMergeProposal(Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 523
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public downloadAttachment(Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation

    const-string v0, "attachmentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/crm/RolodexService;->downloadAttachment(Ljava/lang/String;Z)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 671
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getContact(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0, p1, v0}, Lcom/squareup/crm/RealRolodexServiceHelper;->getContact(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getContact(Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;-><init>()V

    .line 212
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;

    move-result-object p1

    .line 213
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;

    move-result-object p1

    .line 215
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;-><init>()V

    const/4 v1, 0x1

    .line 216
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_instruments_on_file(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    .line 217
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_groups(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_notes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attributes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    .line 220
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_email_subscriptions(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    .line 221
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attachments(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 222
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_app_data(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 223
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_frequent_items(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 224
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactOptions;

    move-result-object p2

    .line 214
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetContactRequest;

    move-result-object p1

    .line 228
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->getContact(Lcom/squareup/protos/client/rolodex/GetContactRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 229
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 230
    sget-object p2, Lcom/squareup/crm/RealRolodexServiceHelper$getContact$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$getContact$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.getContac\u2026ponse.contact != null } }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getCurrentEmployee()Lcom/squareup/protos/client/Employee;
    .locals 3

    .line 691
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    return-object v0

    .line 694
    :cond_2
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    .line 695
    new-instance v1, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 696
    iget-object v2, v0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v1

    .line 697
    iget-object v2, v0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v1

    .line 698
    iget-object v2, v0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v1

    .line 699
    iget-object v2, v0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/squareup/crm/RealRolodexServiceHelper;->formatEmployeeName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/Employee$Builder;->read_only_full_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 700
    invoke-virtual {v0}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    return-object v0
.end method

.method public getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 576
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;-><init>()V

    .line 577
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    move-result-object p1

    .line 578
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object p1

    .line 580
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 581
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getMerchant(Z)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
            ">;>;"
        }
    .end annotation

    .line 236
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMerchantRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMerchantRequest$Builder;-><init>()V

    .line 238
    new-instance v1, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;-><init>()V

    .line 239
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;

    move-result-object p1

    .line 241
    iget-object v1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 240
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->include_default_attribute_definitions(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;

    move-result-object p1

    .line 242
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/MerchantOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/MerchantOptions;

    move-result-object p1

    .line 237
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GetMerchantRequest$Builder;->merchant_options(Lcom/squareup/protos/client/rolodex/MerchantOptions;)Lcom/squareup/protos/client/rolodex/GetMerchantRequest$Builder;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMerchantRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMerchantRequest;

    move-result-object p1

    .line 246
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->getMerchant(Lcom/squareup/protos/client/rolodex/GetMerchantRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 247
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public listContacts(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
            ">;>;"
        }
    .end annotation

    .line 258
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;-><init>()V

    .line 260
    new-instance v1, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;-><init>()V

    .line 261
    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v2, :cond_1

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    xor-int/2addr v2, v3

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_token(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 262
    invoke-virtual {p1, v4}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->automatic(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 263
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    if-eqz p3, :cond_3

    goto :goto_3

    .line 264
    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :goto_3
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 265
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->sort_type(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 266
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 267
    invoke-virtual {p1, p6}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->build()Lcom/squareup/protos/client/rolodex/QueryContext;

    move-result-object p1

    .line 259
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->context(Lcom/squareup/protos/client/rolodex/QueryContext;)Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;

    move-result-object p1

    .line 279
    new-instance p2, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;-><init>()V

    .line 280
    invoke-virtual {p2, v4}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_buyer_summary(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 281
    invoke-virtual {p2, v4}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_app_data(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 282
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactOptions;

    move-result-object p2

    .line 278
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;

    move-result-object p1

    .line 284
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListContactsRequest;

    move-result-object p1

    .line 286
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->listContacts(Lcom/squareup/protos/client/rolodex/ListContactsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 287
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public listEventsForContact(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;-><init>()V

    .line 413
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;

    move-result-object p1

    .line 415
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListOption$Builder;-><init>()V

    .line 416
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListOption$Builder;

    move-result-object p3

    .line 417
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListOption$Builder;

    move-result-object p2

    .line 418
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ListOption$Builder;->build()Lcom/squareup/protos/client/rolodex/ListOption;

    move-result-object p2

    .line 414
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->list_option(Lcom/squareup/protos/client/rolodex/ListOption;)Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;

    move-result-object p1

    .line 420
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;

    move-result-object p1

    .line 422
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->listEventsForContact(Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 423
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public listFilterTemplates()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
            ">;>;"
        }
    .end annotation

    .line 606
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    .line 607
    new-instance v1, Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;

    move-result-object v1

    const-string v2, "ListFilterTemplatesRequest.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/server/crm/RolodexService;->listFilterTemplates(Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 608
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public listGroups(Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
            ">;>;"
        }
    .end annotation

    .line 293
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;-><init>()V

    .line 295
    new-instance v1, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;-><init>()V

    .line 296
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;

    move-result-object p1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupOptions;

    move-result-object p1

    .line 294
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;->group_options(Lcom/squareup/protos/client/rolodex/GroupOptions;)Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;

    move-result-object p1

    .line 299
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListGroupsRequest;

    move-result-object p1

    .line 301
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->listGroups(Lcom/squareup/protos/client/rolodex/ListGroupsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 302
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public listMergeProposals(Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
            ">;>;"
        }
    .end annotation

    .line 486
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;-><init>()V

    .line 487
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;

    move-result-object v0

    .line 488
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;

    move-result-object p2

    .line 489
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;

    move-result-object p2

    .line 491
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p2}, Lcom/squareup/server/crm/RolodexService;->listMergeProposals(Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p2

    .line 492
    invoke-virtual {p2}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    .line 493
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;-><init>(Ljava/lang/Integer;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.listMerge\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUuid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 587
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/crm/RealRolodexServiceHelper;->manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/UUID;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUuid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 594
    new-instance v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;-><init>()V

    .line 595
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    move-result-object p1

    .line 596
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    move-result-object p1

    .line 597
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    move-result-object p1

    .line 598
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object p1

    .line 600
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->manualMergeContacts(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 601
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 602
    new-instance p2, Lcom/squareup/crm/RealRolodexServiceHelper$manualMergeContacts$1;

    invoke-direct {p2, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$manualMergeContacts$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.manualMer\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onContactsAddedOrRemoved()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onContactsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onContactsAddedOrRemoved\u2026LISECONDS, mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onGroupsAddedOrRemoved()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->onGroupsAddedOrRemoved:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onGroupsAddedOrRemoved.d\u2026LISECONDS, mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public runMergeAllJob()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 553
    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    .line 550
    iget-object v1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    .line 551
    new-instance v2, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;

    move-result-object v2

    const-string v3, "TriggerMergeAllRequest.Builder().build()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/server/crm/RolodexService;->triggerMergeAll(Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v1

    .line 552
    invoke-virtual {v1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 553
    invoke-virtual {v1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "rolodexService\n        .\u2026)\n        .toObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 554
    invoke-virtual {v0, v1}, Lcom/squareup/receiving/StandardReceiver$Companion;->filterSuccess(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 555
    new-instance v1, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 569
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 570
    new-instance v1, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$2;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$2;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "rolodexService\n        .\u2026dOrRemoved.accept(Unit) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public runMergeProposalJob()Lio/reactivex/Completable;
    .locals 4

    .line 530
    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    .line 527
    iget-object v1, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    .line 528
    new-instance v2, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;

    move-result-object v2

    const-string v3, "TriggerMergeProposalRequest.Builder().build()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/server/crm/RolodexService;->triggerMergeProposal(Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v1

    .line 529
    invoke-virtual {v1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 530
    invoke-virtual {v1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "rolodexService\n        .\u2026)\n        .toObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 531
    invoke-virtual {v0, v1}, Lcom/squareup/receiving/StandardReceiver$Companion;->filterSuccess(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 532
    new-instance v1, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeProposalJob$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeProposalJob$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 546
    invoke-virtual {v0}, Lio/reactivex/Observable;->ignoreElements()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "rolodexService\n        .\u2026        .ignoreElements()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public shouldShowEmailCollection(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
            ">;>;"
        }
    .end annotation

    .line 629
    new-instance v0, Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest$Builder;-><init>()V

    .line 630
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest$Builder;->tender_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest$Builder;

    move-result-object p1

    .line 631
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;

    move-result-object p1

    .line 633
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->shouldShowEmailCollection(Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 634
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public unlinkInstrumentOnFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 472
    new-instance v0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;-><init>()V

    .line 473
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    move-result-object p1

    .line 474
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    move-result-object p1

    .line 475
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    const-string v0, "settings.userSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->client_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    move-result-object p1

    .line 476
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object p1

    .line 478
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->unlinkInstrumentOnFile(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 479
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "attachmentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 642
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;-><init>()V

    .line 643
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->attachment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    move-result-object p1

    .line 644
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    move-result-object p1

    .line 645
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    move-result-object p1

    .line 646
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->build()Lcom/squareup/protos/client/rolodex/Attachment;

    move-result-object p1

    .line 647
    new-instance p2, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;-><init>()V

    .line 648
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment(Lcom/squareup/protos/client/rolodex/Attachment;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    move-result-object p1

    .line 649
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object p1

    .line 651
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->updateAttachment(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 652
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateNote(Lcom/squareup/protos/client/rolodex/Note;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Note;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use custom attributes instead"
    .end annotation

    const-string v0, "note"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 390
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;-><init>()V

    .line 391
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object v0

    .line 392
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Note;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object v0

    .line 396
    check-cast p1, Lcom/squareup/wire/Message;

    .line 774
    invoke-virtual {p1}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Note$Builder;

    .line 397
    iput-object p2, v1, Lcom/squareup/protos/client/rolodex/Note$Builder;->body:Ljava/lang/String;

    .line 775
    invoke-virtual {p1}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Note;

    .line 393
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->note(Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;

    move-result-object p1

    .line 400
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertNoteRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;

    move-result-object p1

    .line 402
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->upsertNote(Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 403
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 404
    sget-object p2, Lcom/squareup/crm/RealRolodexServiceHelper$updateNote$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$updateNote$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.upsertNot\u2026response.note != null } }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 774
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type B"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public uploadAttachment(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokhttp3/RequestBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestBody"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 679
    sget-object v0, Lokhttp3/MultipartBody$Part;->Companion:Lokhttp3/MultipartBody$Part$Companion;

    const-string/jumbo v1, "upload"

    invoke-virtual {v0, v1, p2, p3}, Lokhttp3/MultipartBody$Part$Companion;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p2

    .line 681
    iget-object p3, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    .line 682
    sget-object v0, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    sget-object v1, Lcom/squareup/crm/RealRolodexServiceHelper;->TEXT_PLAIN:Lokhttp3/MediaType;

    invoke-virtual {v0, p1, v1}, Lokhttp3/RequestBody$Companion;->create(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object p1

    .line 683
    sget-object v0, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID.randomUUID().toString()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/crm/RealRolodexServiceHelper;->TEXT_PLAIN:Lokhttp3/MediaType;

    invoke-virtual {v0, v1, v2}, Lokhttp3/RequestBody$Companion;->create(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object v0

    .line 684
    sget-object v1, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    sget-object v2, Lcom/squareup/crm/RealRolodexServiceHelper;->TEXT_PLAIN:Lokhttp3/MediaType;

    const-string v3, "TRUE"

    invoke-virtual {v1, v3, v2}, Lokhttp3/RequestBody$Companion;->create(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object v1

    .line 681
    invoke-interface {p3, p1, v0, v1, p2}, Lcom/squareup/server/crm/RolodexService;->uploadAttachment(Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 687
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public upsertAttributeSchema(Ljava/util/List;Ljava/util/List;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "attributeDefinitions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributeKeysInOrder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 719
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;-><init>()V

    .line 720
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_definitions(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    move-result-object p1

    .line 721
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_keys_in_order(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    move-result-object p1

    .line 722
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->unique:Lcom/squareup/util/Unique;

    invoke-interface {p2}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->schema_version(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    move-result-object p1

    .line 723
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;

    move-result-object p1

    .line 725
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->updateAttributeSchema(Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 726
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public upsertContact(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUuid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;-><init>()V

    .line 310
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;

    move-result-object p2

    .line 311
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;

    move-result-object p1

    .line 313
    new-instance p2, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;-><init>()V

    const/4 v0, 0x1

    .line 314
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_groups(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 315
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attributes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object p2

    .line 316
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactOptions;

    move-result-object p2

    .line 312
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;

    move-result-object p1

    .line 318
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertContactRequest;

    move-result-object p1

    .line 320
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->upsertContact(Lcom/squareup/protos/client/rolodex/UpsertContactRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 321
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 322
    new-instance p2, Lcom/squareup/crm/RealRolodexServiceHelper$upsertContact$1;

    invoke-direct {p2, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$upsertContact$1;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.upsertCon\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public upsertContactForEmailCollection(Ljava/util/UUID;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "requestToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 616
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;-><init>()V

    .line 617
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;

    move-result-object p1

    .line 618
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->tender_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;

    move-result-object p1

    .line 619
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;

    move-result-object p1

    .line 620
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;

    move-result-object p1

    .line 622
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->upsertContactForEmailCollection(Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 623
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "group"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUuid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;-><init>()V

    .line 330
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->group(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;

    move-result-object p1

    .line 331
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;

    move-result-object p1

    .line 332
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;

    move-result-object p1

    .line 334
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->upsertGroup(Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 335
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 336
    sget-object p2, Lcom/squareup/crm/RealRolodexServiceHelper$upsertManualGroup$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$upsertManualGroup$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 337
    new-instance p2, Lcom/squareup/crm/RealRolodexServiceHelper$upsertManualGroup$2;

    invoke-direct {p2, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$upsertManualGroup$2;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "rolodexService.upsertGro\u2026roupsAddedOrRemoved(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public upsertSmartGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
            ">;>;"
        }
    .end annotation

    const-string v0, "group"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;-><init>()V

    .line 344
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;->group(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;

    move-result-object p1

    .line 345
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;

    move-result-object p1

    .line 347
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->upsertGroupV2(Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 348
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 349
    sget-object v0, Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 350
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$2;

    invoke-direct {v0, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$2;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 352
    new-instance v0, Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$3;

    invoke-direct {v0, p0}, Lcom/squareup/crm/RealRolodexServiceHelper$upsertSmartGroup$3;-><init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodexService.upsertGro\u2026tactsAddedOrRemoved(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public verifyAndLinkCard(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/Card;",
            "Lcom/squareup/protos/client/bills/CardData;",
            "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;-><init>()V

    .line 436
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 437
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 438
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 440
    new-instance p2, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 441
    invoke-virtual {p0}, Lcom/squareup/crm/RealRolodexServiceHelper;->getCurrentEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object p5

    invoke-virtual {p2, p5}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p2

    .line 442
    invoke-virtual {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p2

    .line 439
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 445
    new-instance p2, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;-><init>()V

    .line 446
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;

    move-result-object p2

    .line 447
    invoke-virtual {p2}, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->build()Lcom/squareup/protos/client/instruments/LinkedInstrument;

    move-result-object p2

    .line 444
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument(Lcom/squareup/protos/client/instruments/LinkedInstrument;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 450
    new-instance p2, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;-><init>()V

    .line 451
    invoke-virtual {p2, p6}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    move-result-object p2

    .line 452
    invoke-virtual {p3}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    move-result-object p2

    .line 454
    new-instance p4, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;-><init>()V

    .line 455
    invoke-virtual {p3}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object p4

    .line 456
    invoke-virtual {p3}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object p3

    .line 457
    invoke-virtual {p3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object p3

    .line 453
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->expiration_date(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    move-result-object p2

    .line 459
    invoke-virtual {p2}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->build()Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object p2

    .line 449
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information(Lcom/squareup/protos/client/instruments/ValidationInformation;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 461
    invoke-virtual {p1, p7}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    .line 462
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->build()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object p1

    .line 464
    iget-object p2, p0, Lcom/squareup/crm/RealRolodexServiceHelper;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/RolodexService;->verifyAndLinkInstrument(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 465
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
