.class public final Lcom/squareup/crm/util/RolodexAttributeHelper;
.super Ljava/lang/Object;
.source "RolodexAttributeHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRolodexAttributeHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RolodexAttributeHelper.kt\ncom/squareup/crm/util/RolodexAttributeHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,114:1\n1587#2,3:115\n310#2,7:118\n*E\n*S KotlinDebug\n*F\n+ 1 RolodexAttributeHelper.kt\ncom/squareup/crm/util/RolodexAttributeHelper\n*L\n93#1,3:115\n112#1,7:118\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u001a\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u001a\u0012\u0010\u000b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006\u001a\u0018\u0010\u000c\u001a\u00020\u0002*\u00020\u00022\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "attributeIndex",
        "",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "key",
        "",
        "extractContactAttribute",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "attribute",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "definition",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "withContactAttribute",
        "withContactAttributes",
        "attributes",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final attributeIndex(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)I
    .locals 2

    .line 112
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v0, "attributes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 120
    check-cast v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 112
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    return v0
.end method

.method public static final extractContactAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/crm/model/ContactAttribute;
    .locals 4

    const-string v0, "$this$extractContactAttribute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attribute"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "definition"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    const-string v1, "definition.is_default"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_15

    .line 42
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    if-nez p1, :cond_0

    goto/16 :goto_9

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const/4 v0, 0x0

    const/4 v2, 0x1

    const-string v3, ""

    sparse-switch p2, :sswitch_data_0

    goto/16 :goto_9

    :sswitch_0
    const-string p2, "default:email_address"

    .line 53
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    .line 54
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object p1, v1

    :goto_0
    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    move-object p1, v3

    :goto_1
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 55
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object p1, v1

    .line 54
    :cond_4
    :goto_2
    check-cast p1, Ljava/lang/String;

    .line 57
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    :cond_5
    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_6

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_7

    :cond_6
    const/4 v0, 0x1

    .line 53
    :cond_7
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    invoke-direct {p0, p1, v0}, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;-><init>(Ljava/lang/String;Z)V

    move-object v1, p0

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_1
    const-string p2, "default:company"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    .line 69
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_8

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    .line 68
    :cond_8
    invoke-direct {p1, v1}, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;-><init>(Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_2
    const-string p2, "default:name"

    .line 43
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    .line 44
    iget-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p2, :cond_9

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    goto :goto_3

    :cond_9
    move-object p2, v1

    .line 45
    :goto_3
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_a

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    .line 43
    :cond_a
    invoke-direct {p1, p2, v1}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_3
    const-string p2, "default:birthday"

    .line 65
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;

    .line 66
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_b

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 65
    :cond_b
    invoke-direct {p1, v1}, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_4
    const-string p2, "default:groups"

    .line 62
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    .line 63
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    .line 62
    invoke-direct {p1, p0}, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;-><init>(Ljava/util/List;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_5
    const-string p2, "default:reference_id"

    .line 71
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    .line 72
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_c

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    .line 71
    :cond_c
    invoke-direct {p1, v1}, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;-><init>(Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_6
    const-string p2, "default:phone_number"

    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    .line 48
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_d

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    goto :goto_4

    :cond_d
    move-object p1, v1

    :goto_4
    if-eqz p1, :cond_e

    goto :goto_5

    :cond_e
    move-object p1, v3

    :goto_5
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_10

    .line 49
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p1, :cond_f

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    goto :goto_6

    :cond_f
    move-object p1, v1

    .line 48
    :cond_10
    :goto_6
    check-cast p1, Ljava/lang/String;

    .line 51
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_11

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    :cond_11
    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_12

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_13

    :cond_12
    const/4 v0, 0x1

    .line 47
    :cond_13
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;

    invoke-direct {p0, p1, v0}, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;-><init>(Ljava/lang/String;Z)V

    move-object v1, p0

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto/16 :goto_9

    :sswitch_7
    const-string p2, "default:address"

    .line 59
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    new-instance p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    .line 60
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_14

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz p0, :cond_14

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v1

    .line 59
    :cond_14
    invoke-direct {p1, v1}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;-><init>(Lcom/squareup/address/Address;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    goto :goto_9

    .line 77
    :cond_15
    iget-object p0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-nez p0, :cond_16

    goto :goto_7

    :cond_16
    sget-object v0, Lcom/squareup/crm/util/RolodexAttributeHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_7

    .line 86
    :pswitch_0
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 85
    :pswitch_1
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 84
    :pswitch_2
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 83
    :pswitch_3
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 82
    :pswitch_4
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 81
    :pswitch_5
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 80
    :pswitch_6
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 79
    :pswitch_7
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 78
    :pswitch_8
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    goto :goto_8

    .line 87
    :goto_7
    new-instance p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    invoke-direct {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    check-cast p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    .line 77
    :goto_8
    move-object v1, p0

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    :cond_17
    :goto_9
    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x48263445 -> :sswitch_7
        -0x4028282d -> :sswitch_6
        -0x399c80d8 -> :sswitch_5
        -0x395cead3 -> :sswitch_4
        -0x343ed46a -> :sswitch_3
        -0x28ce47dc -> :sswitch_2
        0x34e96c44 -> :sswitch_1
        0x78bd30d8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final withContactAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    const-string v0, "$this$withContactAttribute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attribute"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;->getFirstName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->withFirstName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;->getLastName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withLastName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto/16 :goto_0

    .line 99
    :cond_0
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;->getPhone()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withPhone(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto/16 :goto_0

    .line 100
    :cond_1
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;->getEmail()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withEmail(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 101
    :cond_2
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAddress(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/address/Address;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 102
    :cond_3
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;->getGroups()Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withGroups(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 103
    :cond_4
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;->getDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withBirthday(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 104
    :cond_5
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;->getCompanyName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withCompany(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 105
    :cond_6
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;->getReferenceId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withReferenceId(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 106
    :cond_7
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    if-eqz v0, :cond_8

    .line 107
    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    invoke-virtual {v1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->toAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexAttributeHelper;->attributeIndex(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)I

    move-result p1

    .line 106
    invoke-static {p0, v0, v1, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpdatedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_8
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final withContactAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Collection;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact;"
        }
    .end annotation

    const-string v0, "$this$withContactAttributes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    check-cast p1, Ljava/lang/Iterable;

    .line 116
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/model/ContactAttribute;

    .line 93
    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexAttributeHelper;->withContactAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    :cond_0
    return-object p0
.end method
