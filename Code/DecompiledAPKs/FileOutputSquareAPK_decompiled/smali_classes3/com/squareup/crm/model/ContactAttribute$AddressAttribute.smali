.class public final Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;
.super Lcom/squareup/crm/model/ContactAttribute;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$AddressAttribute$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u000e\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0008H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0010H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "address",
        "Lcom/squareup/address/Address;",
        "(Lcom/squareup/address/Address;)V",
        "getAddress",
        "()Lcom/squareup/address/Address;",
        "key",
        "",
        "key$annotations",
        "()V",
        "getKey",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final address:Lcom/squareup/address/Address;

.field private final key:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute$Creator;

    invoke-direct {v0}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute$Creator;-><init>()V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/address/Address;)V
    .locals 1

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/crm/model/ContactAttribute;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    const-string p1, "default:address"

    .line 56
    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->key:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;Lcom/squareup/address/Address;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->copy(Lcom/squareup/address/Address;)Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic key$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final component1()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final copy(Lcom/squareup/address/Address;)Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;
    .locals 1

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    invoke-direct {v0, p1}, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;-><init>(Lcom/squareup/address/Address;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    iget-object p1, p1, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->key:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddressAttribute(address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;->address:Lcom/squareup/address/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
