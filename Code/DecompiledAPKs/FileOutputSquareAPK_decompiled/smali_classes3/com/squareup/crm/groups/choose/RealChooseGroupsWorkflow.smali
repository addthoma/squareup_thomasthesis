.class public final Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealChooseGroupsWorkflow.kt"

# interfaces
.implements Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsProps;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        ">;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealChooseGroupsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealChooseGroupsWorkflow.kt\ncom/squareup/crm/groups/choose/RealChooseGroupsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,119:1\n32#2,12:120\n704#3:132\n777#3,2:133\n950#3:135\n1360#3:136\n1429#3,3:137\n*E\n*S KotlinDebug\n*F\n+ 1 RealChooseGroupsWorkflow.kt\ncom/squareup/crm/groups/choose/RealChooseGroupsWorkflow\n*L\n34#1,12:120\n36#1:132\n36#1,2:133\n37#1:135\n64#1:136\n64#1,3:137\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u00012\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ$\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u001c\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u000b2\u0006\u0010\u000c\u001a\u00020\u0004H\u0002J\u001a\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J,\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\u00042\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0016H\u0016J\u001c\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u000b2\u0006\u0010\u000c\u001a\u00020\u0004H\u0002J\u001c\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u000b2\u0006\u0010\u000c\u001a\u00020\u0004H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u000c\u001a\u00020\u0004H\u0016J,\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsProps;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "editGroupWorkflow",
        "Lcom/squareup/crm/groups/edit/EditGroupWorkflow;",
        "(Lcom/squareup/crm/groups/edit/EditGroupWorkflow;)V",
        "addGroupAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "group",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "closeAction",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showCreateGroupAction",
        "showListAction",
        "snapshotState",
        "updateCheckedStateAction",
        "checked",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editGroupWorkflow:Lcom/squareup/crm/groups/edit/EditGroupWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/groups/edit/EditGroupWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editGroupWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->editGroupWorkflow:Lcom/squareup/crm/groups/edit/EditGroupWorkflow;

    return-void
.end method

.method public static final synthetic access$addGroupAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->addGroupAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$closeAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->closeAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showCreateGroupAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->showCreateGroupAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showListAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->showListAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateCheckedStateAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->updateCheckedStateAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final addGroupAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$addGroupAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$addGroupAction$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final closeAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$closeAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$closeAction$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showCreateGroupAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showListAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    .line 99
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showListAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showListAction$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final updateCheckedStateAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/protos/client/rolodex/Group;",
            "Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    .line 81
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$updateCheckedStateAction$1;

    invoke-direct {v0, p1, p3, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$updateCheckedStateAction$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;ZLcom/squareup/protos/client/rolodex/Group;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/crm/groups/choose/ChooseGroupsState;
    .locals 6

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 120
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v3

    :goto_1
    if-eqz p2, :cond_3

    .line 125
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v3, "Parcel.obtain()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 127
    array-length v3, p2

    invoke-virtual {v2, p2, v1, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 128
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 129
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 131
    :cond_3
    check-cast v3, Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    if-eqz v3, :cond_4

    goto :goto_4

    .line 35
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;->getGroups()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 132
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 133
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/rolodex/Group;

    .line 36
    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v5, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 134
    :cond_7
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 135
    new-instance p2, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$initialState$$inlined$sortedBy$1;

    invoke-direct {p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$initialState$$inlined$sortedBy$1;-><init>()V

    check-cast p2, Ljava/util/Comparator;

    invoke-static {v2, p2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p2

    .line 38
    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;->getSelectedGroupsTokens()Ljava/util/Set;

    move-result-object p1

    .line 34
    new-instance v0, Lcom/squareup/crm/groups/choose/ChooseGroupsState$ShowingList;

    invoke-direct {v0, p2, p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsState$ShowingList;-><init>(Ljava/util/List;Ljava/util/Set;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    :goto_4
    return-object v3
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->initialState(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsProps;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "-",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/V2Screen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    instance-of p1, p2, Lcom/squareup/crm/groups/choose/ChooseGroupsState$CreatingGroup;

    if-eqz p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->editGroupWorkflow:Lcom/squareup/crm/groups/edit/EditGroupWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 52
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v3, 0x0

    .line 53
    new-instance p1, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;-><init>(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 50
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    goto/16 :goto_1

    .line 59
    :cond_0
    instance-of p1, p2, Lcom/squareup/crm/groups/choose/ChooseGroupsState$ShowingList;

    if-eqz p1, :cond_3

    .line 61
    new-instance p1, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$CreateGroupRow;

    .line 62
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;-><init>(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 61
    invoke-direct {p1, v0}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$CreateGroupRow;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 60
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    .line 64
    invoke-virtual {p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsState;->getGroups()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 137
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 138
    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 65
    new-instance v3, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$GroupRow;

    .line 66
    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    const-string v5, "group.display_name"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Lcom/squareup/crm/groups/choose/ChooseGroupsState;->getSelectedGroupsTokens()Ljava/util/Set;

    move-result-object v5

    iget-object v6, v2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 68
    new-instance v6, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$$inlined$map$lambda$1;

    invoke-direct {v6, v2, p0, p2, p3}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$$inlined$map$lambda$1;-><init>(Lcom/squareup/protos/client/rolodex/Group;Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/workflow/RenderContext;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 65
    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$GroupRow;-><init>(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    .line 71
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 72
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    sget-object v0, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 72
    :cond_2
    check-cast v1, Ljava/lang/Iterable;

    .line 64
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    .line 75
    new-instance v0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$5;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$5;-><init>(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 59
    new-instance p2, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;

    invoke-direct {p2, p1, v0}, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/crm/groups/choose/ChooseGroupsProps;

    check-cast p2, Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->render(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->snapshotState(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
