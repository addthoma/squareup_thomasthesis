.class final Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealChooseGroupsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->render(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "+",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "output",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

.field final synthetic this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;

    iput-object p2, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/crm/groups/edit/EditGroupOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v0, Lcom/squareup/crm/groups/edit/EditGroupOutput$Close;->INSTANCE:Lcom/squareup/crm/groups/edit/EditGroupOutput$Close;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;

    iget-object v0, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-static {p1, v0}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->access$showListAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_0
    instance-of v0, p1, Lcom/squareup/crm/groups/edit/EditGroupOutput$Save;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;

    iget-object v1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    check-cast p1, Lcom/squareup/crm/groups/edit/EditGroupOutput$Save;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupOutput$Save;->getGroup()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->access$addGroupAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/crm/groups/edit/EditGroupOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$1;->invoke(Lcom/squareup/crm/groups/edit/EditGroupOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
