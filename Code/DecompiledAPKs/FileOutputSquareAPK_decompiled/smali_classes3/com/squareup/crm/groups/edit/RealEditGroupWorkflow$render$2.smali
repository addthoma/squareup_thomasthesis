.class final Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditGroupWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->render(Lkotlin/Unit;Lcom/squareup/crm/groups/edit/EditGroupState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/crm/groups/edit/EditGroupState;",
        "+",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/crm/groups/edit/EditGroupState;",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/crm/groups/edit/EditGroupState;

.field final synthetic this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    iput-object p2, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;->$state:Lcom/squareup/crm/groups/edit/EditGroupState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    iget-object v1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;->$state:Lcom/squareup/crm/groups/edit/EditGroupState;

    invoke-static {v0, v1, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->access$updateName(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
