.class final Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;
.super Ljava/lang/Object;
.source "RealEditGroupWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->saveGroup(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/crm/groups/edit/EditGroupState;",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/crm/groups/edit/EditGroupState;

.field final synthetic this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    iput-object p2, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->$state:Lcom/squareup/crm/groups/edit/EditGroupState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->group:Lcom/squareup/protos/client/rolodex/Group;

    const-string v1, "it.response.group"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->access$completeSave(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 80
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    .line 81
    iget-object v1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->$state:Lcom/squareup/crm/groups/edit/EditGroupState;

    .line 82
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 83
    instance-of v2, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    goto :goto_0

    .line 84
    :cond_1
    iget-object p1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->this$0:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;

    invoke-static {p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->access$getRes$p(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v2, Lcom/squareup/crm/groups/impl/R$string;->crm_groups_group_saving_error:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    const-string/jumbo v2, "when (val received = it.\u2026_error)\n                }"

    .line 82
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v0, v1, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->access$showError(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
