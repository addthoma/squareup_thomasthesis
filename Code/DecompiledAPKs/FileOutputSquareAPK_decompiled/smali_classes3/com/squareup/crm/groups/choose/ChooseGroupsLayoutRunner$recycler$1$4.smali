.class final Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseGroupsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;",
        "Lcom/squareup/noho/NohoLabel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "<anonymous parameter 1>",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;",
        "row",
        "Lcom/squareup/noho/NohoLabel;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;

    invoke-direct {v0}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;-><init>()V

    sput-object v0, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;->INSTANCE:Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;

    check-cast p3, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$recycler$1$4;->invoke(ILcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;Lcom/squareup/noho/NohoLabel;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/crm/groups/choose/ChooseGroupsRendering$Row$NoGroupsRow;Lcom/squareup/noho/NohoLabel;)V
    .locals 0

    const-string p1, "<anonymous parameter 1>"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p3}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/squareup/crm/groups/impl/R$string;->crm_groups_no_groups_exist_message:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x11

    .line 73
    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoLabel;->setGravity(I)V

    return-void
.end method
