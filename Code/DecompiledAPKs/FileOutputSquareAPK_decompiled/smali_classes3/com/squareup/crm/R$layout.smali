.class public final Lcom/squareup/crm/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final crm_add_coupon_row:I = 0x7f0d010e

.field public static final crm_appointment_row:I = 0x7f0d0114

.field public static final crm_contact_list_bottom_row:I = 0x7f0d011f

.field public static final crm_conversation_coupon_right_row:I = 0x7f0d0121

.field public static final crm_conversation_message_left_row:I = 0x7f0d0122

.field public static final crm_conversation_message_right_row:I = 0x7f0d0123

.field public static final crm_customer_lookup_view:I = 0x7f0d0130

.field public static final crm_edit_boolean_attribute_row:I = 0x7f0d0136

.field public static final crm_edit_email_attribute_row:I = 0x7f0d0139

.field public static final crm_edit_enum_attribute_row:I = 0x7f0d013b

.field public static final crm_edit_number_attribute_row:I = 0x7f0d013e

.field public static final crm_edit_phone_attribute_row:I = 0x7f0d013f

.field public static final crm_edit_text_attribute_row:I = 0x7f0d0140

.field public static final crm_group_edit_view:I = 0x7f0d0146

.field public static final crm_multi_select_row:I = 0x7f0d014d

.field public static final crm_note_row:I = 0x7f0d014e

.field public static final crm_recent_customer_row:I = 0x7f0d0156

.field public static final crm_search_customer_row:I = 0x7f0d015d

.field public static final crm_selectable_loyalty_phone_row:I = 0x7f0d015f

.field public static final crm_single_select_row:I = 0x7f0d0162

.field public static final crm_summary_row:I = 0x7f0d0164

.field public static final crm_unknown_attribute_row:I = 0x7f0d0165

.field public static final crm_v2_cardonfile_card_row:I = 0x7f0d016a

.field public static final crm_v2_contact_list_top_row_create_customer:I = 0x7f0d0171

.field public static final crm_v2_customer_unit_row:I = 0x7f0d0174

.field public static final crm_v2_edit_attribute_row_boolean:I = 0x7f0d0176

.field public static final crm_v2_edit_attribute_row_date:I = 0x7f0d0177

.field public static final crm_v2_edit_attribute_row_email:I = 0x7f0d0178

.field public static final crm_v2_edit_attribute_row_enum:I = 0x7f0d0179

.field public static final crm_v2_edit_attribute_row_number:I = 0x7f0d017a

.field public static final crm_v2_edit_attribute_row_phone:I = 0x7f0d017b

.field public static final crm_v2_edit_attribute_row_text:I = 0x7f0d017c

.field public static final crm_v2_edit_attribute_row_unknown:I = 0x7f0d017d

.field public static final crm_v2_intial_circle_view:I = 0x7f0d0183

.field public static final crm_v2_list_header_row:I = 0x7f0d0184

.field public static final crm_v2_profile_line_data_row:I = 0x7f0d018f

.field public static final crm_v2_profile_section_header:I = 0x7f0d0192

.field public static final crm_view_customer_attribute_row:I = 0x7f0d01a0

.field public static final save_card_spinner_view:I = 0x7f0d04b2

.field public static final save_spinner_contents:I = 0x7f0d04b3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
