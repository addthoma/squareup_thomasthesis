.class public final Lcom/squareup/disputes/DisputesDetailKt;
.super Ljava/lang/Object;
.source "DisputesDetail.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0000\u001a\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000bH\u0000\"\u001a\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0004\"\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0004\u00a8\u0006\u000e"
    }
    d2 = {
        "NO_ACTION_STATUSES",
        "",
        "Lcom/squareup/protos/client/cbms/ActionableStatus;",
        "getNO_ACTION_STATUSES",
        "()Ljava/util/List;",
        "OPEN_DISPUTE_RESOLUTIONS",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;",
        "getOPEN_DISPUTE_RESOLUTIONS",
        "cardBrandForInstrumentType",
        "Lcom/squareup/Card$Brand;",
        "instrumentType",
        "Lcom/squareup/protos/common/instrument/InstrumentType;",
        "nameForInstrument",
        "",
        "disputes_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_ACTION_STATUSES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/ActionableStatus;",
            ">;"
        }
    .end annotation
.end field

.field private static final OPEN_DISPUTE_RESOLUTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 33
    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v5, 0x3

    aput-object v1, v0, v5

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/disputes/DisputesDetailKt;->OPEN_DISPUTE_RESOLUTIONS:Ljava/util/List;

    new-array v0, v5, [Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 36
    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    aput-object v1, v0, v4

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/disputes/DisputesDetailKt;->NO_ACTION_STATUSES:Ljava/util/List;

    return-void
.end method

.method public static final cardBrandForInstrumentType(Lcom/squareup/protos/common/instrument/InstrumentType;)Lcom/squareup/Card$Brand;
    .locals 1

    const-string v0, "instrumentType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 56
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 55
    :pswitch_1
    sget-object p0, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 54
    :pswitch_2
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 53
    :pswitch_3
    sget-object p0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 52
    :pswitch_4
    sget-object p0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 51
    :pswitch_5
    sget-object p0, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 50
    :pswitch_6
    sget-object p0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 49
    :pswitch_7
    sget-object p0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 48
    :pswitch_8
    sget-object p0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 47
    :pswitch_9
    sget-object p0, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 46
    :pswitch_a
    sget-object p0, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 45
    :pswitch_b
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 44
    :pswitch_c
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 43
    :pswitch_d
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 42
    :pswitch_e
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 41
    :pswitch_f
    sget-object p0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 40
    :pswitch_10
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final getNO_ACTION_STATUSES()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/ActionableStatus;",
            ">;"
        }
    .end annotation

    .line 35
    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt;->NO_ACTION_STATUSES:Ljava/util/List;

    return-object v0
.end method

.method public static final getOPEN_DISPUTE_RESOLUTIONS()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;",
            ">;"
        }
    .end annotation

    .line 32
    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt;->OPEN_DISPUTE_RESOLUTIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final nameForInstrument(Lcom/squareup/protos/common/instrument/InstrumentType;)I
    .locals 1

    const-string v0, "instrumentType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0}, Lcom/squareup/disputes/DisputesDetailKt;->cardBrandForInstrumentType(Lcom/squareup/protos/common/instrument/InstrumentType;)Lcom/squareup/Card$Brand;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p0

    iget p0, p0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    return p0
.end method
