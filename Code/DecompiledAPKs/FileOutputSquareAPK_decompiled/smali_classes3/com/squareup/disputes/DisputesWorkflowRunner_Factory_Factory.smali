.class public final Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "DisputesWorkflowRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final starterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/Starter;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesViewFactory$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/Starter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->viewFactoryFactoryProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->starterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->containerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/Starter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/disputes/DisputesViewFactory$Factory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;-><init>(Lcom/squareup/disputes/DisputesViewFactory$Factory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->viewFactoryFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/disputes/DisputesViewFactory$Factory;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/Starter;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->newInstance(Lcom/squareup/disputes/DisputesViewFactory$Factory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesWorkflowRunner_Factory_Factory;->get()Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;

    move-result-object v0

    return-object v0
.end method
