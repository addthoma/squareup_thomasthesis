.class final Lcom/squareup/disputes/DisputesReactor$onReact$6$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$6;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/disputes/DisputesState$DetailState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/disputes/DisputesState$DetailState;",
        "it",
        "Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$6;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$6;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$6$1;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$6;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/disputes/DisputesState$DetailState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$6$1;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$6;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$6;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {v0}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;->getPaymentToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->goneToWeb(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$6$1;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$6;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$6;->this$0:Lcom/squareup/disputes/DisputesReactor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dashboard/sales/disputes/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;->getPaymentToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;->getIrfToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/disputes/DisputesReactor;->access$openUrl(Lcom/squareup/disputes/DisputesReactor;Ljava/lang/String;)V

    .line 152
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/disputes/DisputesState$DetailState;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$6$1;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$6;

    iget-object v1, v1, Lcom/squareup/disputes/DisputesReactor$onReact$6;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v1, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputesState$ActionDialogState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/disputes/DisputesState$DetailState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$6$1;->invoke(Lcom/squareup/disputes/DisputeActionDialogEvent$GoToWeb;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
