.class public final Lcom/squareup/disputes/DisputesLoaderKt;
.super Ljava/lang/Object;
.source "DisputesLoader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0004\u001a\u00020\u0005*\u00020\u0006H\u0002\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0007"
    }
    d2 = {
        "FIVE_YEARS_MILLIS",
        "",
        "getFIVE_YEARS_MILLIS",
        "()J",
        "fiveYearsAgo",
        "Lcom/squareup/protos/common/time/DateTime;",
        "Lcom/squareup/util/DateTimeFactory;",
        "disputes_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final FIVE_YEARS_MILLIS:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 126
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x721

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/disputes/DisputesLoaderKt;->FIVE_YEARS_MILLIS:J

    return-void
.end method

.method public static final synthetic access$fiveYearsAgo(Lcom/squareup/util/DateTimeFactory;)Lcom/squareup/protos/common/time/DateTime;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/disputes/DisputesLoaderKt;->fiveYearsAgo(Lcom/squareup/util/DateTimeFactory;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    return-object p0
.end method

.method private static final fiveYearsAgo(Lcom/squareup/util/DateTimeFactory;)Lcom/squareup/protos/common/time/DateTime;
    .locals 4

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/squareup/disputes/DisputesLoaderKt;->FIVE_YEARS_MILLIS:J

    sub-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/squareup/util/DateTimeFactory;->build(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    const-string v0, "build(System.currentTime\u2026is() - FIVE_YEARS_MILLIS)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getFIVE_YEARS_MILLIS()J
    .locals 2

    .line 126
    sget-wide v0, Lcom/squareup/disputes/DisputesLoaderKt;->FIVE_YEARS_MILLIS:J

    return-wide v0
.end method
