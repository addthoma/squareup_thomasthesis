.class public abstract Lcom/squareup/disputes/SummaryAdapter$FormData;
.super Ljava/lang/Object;
.source "SummaryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/SummaryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FormData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/SummaryAdapter$FormData$Title;,
        Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;,
        Lcom/squareup/disputes/SummaryAdapter$FormData$FileUpload;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/SummaryAdapter$FormData;",
        "",
        "viewType",
        "Lcom/squareup/disputes/SummaryAdapter$ViewType;",
        "(Lcom/squareup/disputes/SummaryAdapter$ViewType;)V",
        "getViewType",
        "()Lcom/squareup/disputes/SummaryAdapter$ViewType;",
        "FileUpload",
        "TextAnswer",
        "Title",
        "Lcom/squareup/disputes/SummaryAdapter$FormData$Title;",
        "Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;",
        "Lcom/squareup/disputes/SummaryAdapter$FormData$FileUpload;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final viewType:Lcom/squareup/disputes/SummaryAdapter$ViewType;


# direct methods
.method private constructor <init>(Lcom/squareup/disputes/SummaryAdapter$ViewType;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/SummaryAdapter$FormData;->viewType:Lcom/squareup/disputes/SummaryAdapter$ViewType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/disputes/SummaryAdapter$ViewType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/disputes/SummaryAdapter$FormData;-><init>(Lcom/squareup/disputes/SummaryAdapter$ViewType;)V

    return-void
.end method


# virtual methods
.method public final getViewType()Lcom/squareup/disputes/SummaryAdapter$ViewType;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$FormData;->viewType:Lcom/squareup/disputes/SummaryAdapter$ViewType;

    return-object v0
.end method
