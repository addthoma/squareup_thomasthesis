.class final Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesSerializer;->snapshotState(Lcom/squareup/disputes/DisputesState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/disputes/DisputesState;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 28
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    .line 29
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$WaitingToStart;

    if-eqz v1, :cond_0

    goto/16 :goto_1

    .line 30
    :cond_0
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    if-eqz v1, :cond_2

    .line 31
    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;->getStartingPaymentToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 32
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;->getStartingPaymentToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;->getStartingPaymentToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 34
    :cond_2
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 35
    :cond_3
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    if-eqz v1, :cond_4

    .line 36
    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;->getLastResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 37
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;->getCursor()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 39
    :cond_4
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    if-eqz v1, :cond_5

    .line 40
    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;->getPreviousResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 42
    :cond_5
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    if-eqz v1, :cond_6

    .line 43
    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 44
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 46
    :cond_6
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$DetailState;

    if-eqz v1, :cond_7

    check-cast v0, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$DetailState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 47
    :cond_7
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    if-eqz v1, :cond_8

    check-cast v0, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ActionDialogState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 48
    :cond_8
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    if-eqz v1, :cond_9

    .line 49
    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 51
    :cond_9
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    if-eqz v1, :cond_a

    .line 52
    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;->getResponse()Lcom/squareup/protos/client/irf/GetFormResponse;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 53
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 55
    :cond_a
    instance-of v1, v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    if-eqz v1, :cond_b

    .line 56
    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 57
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 58
    iget-object v0, p0, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_b
    :goto_1
    return-void
.end method
