.class public final Lcom/squareup/disputes/RealDisputesLoader_Factory;
.super Ljava/lang/Object;
.source "RealDisputesLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/RealDisputesLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/disputes/DisputesService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/disputes/DisputesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->serviceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->messagesProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/RealDisputesLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/disputes/DisputesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/disputes/RealDisputesLoader_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/disputes/RealDisputesLoader_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/disputes/RealDisputesLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/disputes/DisputesService;Lio/reactivex/Scheduler;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)Lcom/squareup/disputes/RealDisputesLoader;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/disputes/RealDisputesLoader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/disputes/RealDisputesLoader;-><init>(Lcom/squareup/server/disputes/DisputesService;Lio/reactivex/Scheduler;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/RealDisputesLoader;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/disputes/DisputesService;

    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->messagesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v3, p0, Lcom/squareup/disputes/RealDisputesLoader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/disputes/RealDisputesLoader_Factory;->newInstance(Lcom/squareup/server/disputes/DisputesService;Lio/reactivex/Scheduler;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)Lcom/squareup/disputes/RealDisputesLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/disputes/RealDisputesLoader_Factory;->get()Lcom/squareup/disputes/RealDisputesLoader;

    move-result-object v0

    return-object v0
.end method
