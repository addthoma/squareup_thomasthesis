.class public final Lcom/squareup/disputes/DisputesWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "DisputesWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;,
        Lcom/squareup/disputes/DisputesWorkflowRunner$ParentComponent;,
        Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/SimpleWorkflowRunner<",
        "Lcom/squareup/disputes/InitializeWorkflowEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u000e2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\u000e\u000f\u0010B\u001f\u0008\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "Lcom/squareup/disputes/InitializeWorkflowEvent;",
        "",
        "viewFactory",
        "Lcom/squareup/disputes/DisputesViewFactory;",
        "starter",
        "Lcom/squareup/disputes/Starter;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/disputes/DisputesViewFactory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "Companion",
        "Factory",
        "ParentComponent",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/disputes/DisputesWorkflowRunner;->Companion:Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;

    .line 60
    const-class v0, Lcom/squareup/disputes/DisputesWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisputesWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/disputes/DisputesWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/disputes/DisputesViewFactory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V
    .locals 8

    .line 27
    sget-object v1, Lcom/squareup/disputes/DisputesWorkflowRunner;->NAME:Ljava/lang/String;

    .line 28
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v2

    .line 29
    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 30
    move-object v4, p2

    check-cast v4, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    .line 26
    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p3, p0, Lcom/squareup/disputes/DisputesWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/disputes/DisputesViewFactory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/disputes/DisputesWorkflowRunner;-><init>(Lcom/squareup/disputes/DisputesViewFactory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/disputes/DisputesWorkflowRunner;)V
    .locals 0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/disputes/DisputesWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/disputes/DisputesWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/disputes/DisputesWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$sendEvent(Lcom/squareup/disputes/DisputesWorkflowRunner;Lcom/squareup/disputes/InitializeWorkflowEvent;)V
    .locals 0

    .line 21
    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Lcom/squareup/container/SimpleWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/disputes/DisputesWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/disputes/DisputesWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/disputes/DisputesWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe { c\u2026cope<WorkflowTreeKey>() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method
