.class public interface abstract Lcom/squareup/disputes/api/HandlesDisputes;
.super Ljava/lang/Object;
.source "HandlesDisputes.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/api/HandlesDisputes$NoDisputes;,
        Lcom/squareup/disputes/api/HandlesDisputes$NoDisputesModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0002\u000b\u000cJ\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0008H&J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0006H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0004\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "",
        "isVisible",
        "",
        "()Z",
        "disputeNotification",
        "Lio/reactivex/Observable;",
        "markPopupClosed",
        "",
        "markReportSeen",
        "shouldShowPopup",
        "NoDisputes",
        "NoDisputesModule",
        "disputes"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract disputeNotification()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isVisible()Z
.end method

.method public abstract markPopupClosed()V
.end method

.method public abstract markReportSeen()V
.end method

.method public abstract shouldShowPopup()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method
