.class public final Lcom/squareup/disputes/DisputesTutorial_Factory;
.super Ljava/lang/Object;
.source "DisputesTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/DisputesTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/DisputesTutorial_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/disputes/DisputesTutorial_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/disputes/DisputesTutorial_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/disputes/DisputesTutorialRunner;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/analytics/Analytics;)Lcom/squareup/disputes/DisputesTutorial;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/disputes/DisputesTutorial;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesTutorial;-><init>(Lcom/squareup/disputes/DisputesTutorialRunner;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/DisputesTutorial;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/disputes/DisputesTutorialRunner;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/api/HandlesDisputes;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/disputes/DisputesTutorial_Factory;->newInstance(Lcom/squareup/disputes/DisputesTutorialRunner;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/analytics/Analytics;)Lcom/squareup/disputes/DisputesTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesTutorial_Factory;->get()Lcom/squareup/disputes/DisputesTutorial;

    move-result-object v0

    return-object v0
.end method
