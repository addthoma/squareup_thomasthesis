.class public final Lcom/squareup/disputes/DisputesViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "DisputesViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesViewFactory$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u000bB-\u0008\u0000\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "allDisputesFactory",
        "Lcom/squareup/disputes/AllDisputesCoordinator$Factory;",
        "disputesDetailFactory",
        "Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;",
        "challengeSummaryFactory",
        "Lcom/squareup/disputes/SummaryCoordinator$Factory;",
        "(Ljava/lang/Class;Lcom/squareup/disputes/AllDisputesCoordinator$Factory;Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;Lcom/squareup/disputes/SummaryCoordinator$Factory;)V",
        "Factory",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/squareup/disputes/AllDisputesCoordinator$Factory;Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;Lcom/squareup/disputes/SummaryCoordinator$Factory;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/disputes/AllDisputesCoordinator$Factory;",
            "Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;",
            "Lcom/squareup/disputes/SummaryCoordinator$Factory;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    const-string v3, "allDisputesFactory"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "disputesDetailFactory"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "challengeSummaryFactory"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 15
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    sget-object v5, Lcom/squareup/disputes/AllDisputes;->INSTANCE:Lcom/squareup/disputes/AllDisputes;

    invoke-virtual {v5}, Lcom/squareup/disputes/AllDisputes;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 17
    sget v6, Lcom/squareup/disputes/R$layout;->all_disputes_view:I

    .line 18
    new-instance v19, Lcom/squareup/workflow/ScreenHint;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-string v16, "Disputes: Disputes Report"

    const/16 v17, 0xf7

    const/16 v18, 0x0

    move-object/from16 v7, v19

    move-object/from16 v11, p1

    invoke-direct/range {v7 .. v18}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 22
    new-instance v7, Lcom/squareup/disputes/DisputesViewFactory$1;

    invoke-direct {v7, v0}, Lcom/squareup/disputes/DisputesViewFactory$1;-><init>(Lcom/squareup/disputes/AllDisputesCoordinator$Factory;)V

    move-object v9, v7

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object/from16 v7, v19

    .line 15
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 24
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 25
    sget-object v0, Lcom/squareup/disputes/Summary;->INSTANCE:Lcom/squareup/disputes/Summary;

    invoke-virtual {v0}, Lcom/squareup/disputes/Summary;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 26
    sget v7, Lcom/squareup/disputes/R$layout;->challenge_summary_view:I

    .line 27
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const-string v17, "Disputes: View Challenge Submission"

    const/16 v18, 0xf7

    const/16 v19, 0x0

    move-object v8, v0

    move-object/from16 v12, p1

    invoke-direct/range {v8 .. v19}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 31
    new-instance v4, Lcom/squareup/disputes/DisputesViewFactory$2;

    invoke-direct {v4, v2}, Lcom/squareup/disputes/DisputesViewFactory$2;-><init>(Lcom/squareup/disputes/SummaryCoordinator$Factory;)V

    move-object v10, v4

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/16 v11, 0x8

    const/4 v12, 0x0

    .line 24
    invoke-static/range {v5 .. v12}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x1

    aput-object v0, v3, v2

    .line 33
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 34
    sget-object v0, Lcom/squareup/disputes/DisputesDetail;->INSTANCE:Lcom/squareup/disputes/DisputesDetail;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesDetail;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 35
    sget v6, Lcom/squareup/disputes/R$layout;->disputes_detail_view:I

    .line 36
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v14, 0x0

    const-string v16, "Disputes: Disputes Detail"

    const/16 v17, 0xf7

    const/16 v18, 0x0

    move-object v7, v0

    move-object/from16 v11, p1

    invoke-direct/range {v7 .. v18}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 40
    new-instance v2, Lcom/squareup/disputes/DisputesViewFactory$3;

    invoke-direct {v2, v1}, Lcom/squareup/disputes/DisputesViewFactory$3;-><init>(Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0x8

    const/4 v11, 0x0

    .line 33
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v3, v1

    .line 42
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v1, Lcom/squareup/disputes/DisputeActionDialog;->INSTANCE:Lcom/squareup/disputes/DisputeActionDialog;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputeActionDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lcom/squareup/disputes/DisputesViewFactory$4;->INSTANCE:Lcom/squareup/disputes/DisputesViewFactory$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v3, v1

    .line 43
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v1, Lcom/squareup/disputes/DisputesErrorDialog;->INSTANCE:Lcom/squareup/disputes/DisputesErrorDialog;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputesErrorDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lcom/squareup/disputes/DisputesViewFactory$5;->INSTANCE:Lcom/squareup/disputes/DisputesViewFactory$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v3, v1

    move-object/from16 v0, p0

    .line 14
    invoke-direct {v0, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
