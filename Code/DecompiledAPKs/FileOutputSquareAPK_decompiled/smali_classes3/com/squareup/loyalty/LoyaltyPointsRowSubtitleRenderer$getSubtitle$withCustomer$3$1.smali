.class final Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;
.super Ljava/lang/Object;
.source "LoyaltyPointsRowSubtitleRenderer.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;

    invoke-direct {v0}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;-><init>()V

    sput-object v0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/LoyaltyStatusResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;)",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;"
        }
    .end annotation

    const-string v0, "it"

    .line 40
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponseKt;->toPointsResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object p1

    return-object p1
.end method
