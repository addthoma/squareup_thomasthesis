.class Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;
.super Ljava/lang/Object;
.source "FlyingStarsBackgroundView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Star"
.end annotation


# static fields
.field private static final MAX_ROTATION_SPEED:F = 30.0f

.field private static final MAX_TRAVEL_SPEED:F = 16.0f

.field private static final MIN_ROTATION_SPEED:F = 24.0f

.field private static final MIN_TRAVEL_SPEED:F = 8.0f

.field private static final PI:F = 3.1415927f


# instance fields
.field private rotationSpeed:F

.field private starSize:F

.field final synthetic this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;

.field private travelAngle:F

.field private travelSpeed:F

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;Landroid/content/Context;)V
    .locals 1

    .line 151
    iput-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    sget p1, Lcom/squareup/loyalty/R$layout;->background_star:I

    const/4 v0, 0x0

    invoke-static {p2, p1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    .line 155
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;)Landroid/view/View;
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method initialize(II)V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    invoke-static {v1, v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    const/high16 v0, 0x41c00000    # 24.0f

    const/high16 v2, 0x41f00000    # 30.0f

    .line 163
    invoke-static {v0, v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result v0

    iput v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->rotationSpeed:F

    const/high16 v0, 0x41000000    # 8.0f

    const/high16 v2, 0x41800000    # 16.0f

    .line 164
    invoke-static {v0, v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result v0

    iget-object v2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;

    invoke-static {v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$300(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;)F

    move-result v2

    mul-float v0, v0, v2

    iput v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelSpeed:F

    const v0, -0x3fb6f025

    const v2, 0x40490fdb    # (float)Math.PI

    .line 165
    invoke-static {v0, v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result v0

    iput v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    .line 167
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->starSize:F

    .line 170
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    int-to-float p1, p1

    iget v2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->starSize:F

    sub-float/2addr p1, v2

    invoke-static {v1, p1}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setX(F)V

    .line 171
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    int-to-float p2, p2

    iget v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->starSize:F

    sub-float/2addr p2, v0

    invoke-static {v1, p2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$200(FF)F

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    return-void
.end method

.method update(FII)V
    .locals 10

    .line 175
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRotation()F

    move-result v1

    iget v2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->rotationSpeed:F

    mul-float v2, v2, p1

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 177
    iget v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 178
    iget v1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    .line 180
    iget-object v2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    iget v3, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelSpeed:F

    mul-float v3, v3, v0

    mul-float v3, v3, p1

    add-float/2addr v2, v3

    .line 181
    iget-object v3, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v3

    iget v4, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelSpeed:F

    mul-float v4, v4, v1

    mul-float v4, v4, p1

    add-float/2addr v3, v4

    .line 183
    iget-object v4, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setX(F)V

    .line 184
    iget-object v4, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->view:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setY(F)V

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v5, 0x0

    cmpg-float v6, v2, v5

    if-gez v6, :cond_0

    float-to-double v6, v1

    .line 188
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p2

    float-to-double v8, p2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float p2, v6

    iput p2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_0

    .line 189
    :cond_0
    iget v6, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->starSize:F

    cmpg-float v7, v2, v6

    if-gez v7, :cond_1

    float-to-double v6, v1

    add-float p2, v0, p1

    float-to-double v8, p2

    .line 191
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float p2, v6

    iput p2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_0

    :cond_1
    int-to-float p2, p2

    sub-float v7, p2, v6

    cmpl-float v7, v2, v7

    if-lez v7, :cond_2

    float-to-double v6, v1

    .line 194
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p2

    neg-float p2, p2

    float-to-double v8, p2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float p2, v6

    iput p2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_0

    :cond_2
    mul-float v6, v6, v4

    sub-float/2addr p2, v6

    cmpl-float p2, v2, p2

    if-lez p2, :cond_3

    float-to-double v6, v1

    sub-float p2, v0, p1

    float-to-double v8, p2

    .line 197
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float p2, v6

    iput p2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    :cond_3
    :goto_0
    cmpg-float p2, v3, v5

    if-gez p2, :cond_4

    .line 202
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double p1, p1

    float-to-double v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_1

    .line 203
    :cond_4
    iget p2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->starSize:F

    cmpg-float v2, v3, p2

    if-gez v2, :cond_5

    add-float/2addr v1, p1

    float-to-double p1, v1

    float-to-double v0, v0

    .line 205
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_1

    :cond_5
    int-to-float p3, p3

    sub-float v2, p3, p2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_6

    .line 208
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    neg-float p1, p1

    float-to-double p1, p1

    float-to-double v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    goto :goto_1

    :cond_6
    mul-float p2, p2, v4

    sub-float/2addr p3, p2

    cmpl-float p2, v3, p3

    if-lez p2, :cond_7

    sub-float/2addr v1, p1

    float-to-double p1, v1

    float-to-double v0, v0

    .line 211
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->travelAngle:F

    :cond_7
    :goto_1
    return-void
.end method
