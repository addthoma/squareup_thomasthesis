.class Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;
.super Ljava/lang/Object;
.source "FlyingStarsBackgroundView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startAnimation(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private oldValue:F

.field final synthetic this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;->this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .line 104
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 105
    iget v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;->oldValue:F

    sub-float v0, p1, v0

    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    goto :goto_0

    :cond_0
    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    return-void

    .line 111
    :cond_1
    :goto_0
    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;->oldValue:F

    .line 113
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;->this$0:Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;

    invoke-static {p1, v0}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->access$100(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;F)V

    return-void
.end method
