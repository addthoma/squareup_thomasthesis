.class public final Lcom/squareup/loyalty/CartRewardState$RewardTierState;
.super Ljava/lang/Object;
.source "CartRewardState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/CartRewardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RewardTierState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/CartRewardState$RewardTierState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0006H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0010H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/loyalty/CartRewardState$RewardTierState;",
        "Landroid/os/Parcelable;",
        "rewardTier",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "appliedCouponTokens",
        "",
        "",
        "(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)V",
        "getAppliedCouponTokens",
        "()Ljava/util/List;",
        "getRewardTier",
        "()Lcom/squareup/server/account/protos/RewardTier;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final appliedCouponTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardTier:Lcom/squareup/server/account/protos/RewardTier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyalty/CartRewardState$RewardTierState$Creator;

    invoke-direct {v0}, Lcom/squareup/loyalty/CartRewardState$RewardTierState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/RewardTier;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "rewardTier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appliedCouponTokens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    iput-object p2, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyalty/CartRewardState$RewardTierState;Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/loyalty/CartRewardState$RewardTierState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->copy(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/server/account/protos/RewardTier;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)Lcom/squareup/loyalty/CartRewardState$RewardTierState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/RewardTier;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/loyalty/CartRewardState$RewardTierState;"
        }
    .end annotation

    const-string v0, "rewardTier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appliedCouponTokens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;-><init>(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    iget-object v1, p1, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAppliedCouponTokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    return-object v0
.end method

.method public final getRewardTier()Lcom/squareup/server/account/protos/RewardTier;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RewardTierState(rewardTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", appliedCouponTokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->appliedCouponTokens:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void
.end method
