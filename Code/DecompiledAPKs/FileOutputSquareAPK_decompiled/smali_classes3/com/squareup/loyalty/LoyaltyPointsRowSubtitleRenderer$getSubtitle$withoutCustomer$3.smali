.class final Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;
.super Ljava/lang/Object;
.source "LoyaltyPointsRowSubtitleRenderer.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->getSubtitle(Lcom/squareup/payment/Transaction;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)I"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transaction:Lcom/squareup/payment/Transaction;

.field final synthetic this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/payment/Transaction;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;->$transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Unit;)I
    .locals 1

    .line 77
    iget-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->access$getLoyaltyCalculator$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltyCalculator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;->$transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->calculateCartPoints(Lcom/squareup/payment/Transaction;)Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;->call(Lkotlin/Unit;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
