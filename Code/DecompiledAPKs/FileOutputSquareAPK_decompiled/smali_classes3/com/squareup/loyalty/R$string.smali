.class public final Lcom/squareup/loyalty/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final counter_view_minus_icon_description:I = 0x7f1204e4

.field public static final counter_view_plus_icon_description:I = 0x7f1204e5

.field public static final loyalty_cash_app_sms:I = 0x7f120f11

.field public static final loyalty_congratulations_earned_multiple_points:I = 0x7f120f18

.field public static final loyalty_congratulations_linked_card:I = 0x7f120f19

.field public static final loyalty_congratulations_linked_phone:I = 0x7f120f1a

.field public static final loyalty_congratulations_multiple_available_rewards:I = 0x7f120f1b

.field public static final loyalty_congratulations_multiple_available_single_reward:I = 0x7f120f1c

.field public static final loyalty_congratulations_no_points_newly_enrolled_sms:I = 0x7f120f1d

.field public static final loyalty_congratulations_reward_single_tier:I = 0x7f120f1e

.field public static final loyalty_congratulations_title:I = 0x7f120f1f

.field public static final loyalty_congratulations_total_points_multiple:I = 0x7f120f20

.field public static final loyalty_congratulations_welcome_check_sms:I = 0x7f120f21

.field public static final loyalty_congratulations_welcome_multiple_points:I = 0x7f120f22

.field public static final loyalty_congratulations_welcome_one_point:I = 0x7f120f23

.field public static final loyalty_did_not_qualify:I = 0x7f120f24

.field public static final loyalty_enroll_button_help:I = 0x7f120f28

.field public static final loyalty_enroll_button_help_marketing_integration:I = 0x7f120f29

.field public static final loyalty_enroll_button_help_shorter:I = 0x7f120f2a

.field public static final loyalty_enroll_button_help_shorter_marketing_integration:I = 0x7f120f2b

.field public static final loyalty_points_amount:I = 0x7f120f3c

.field public static final loyalty_points_amount_new:I = 0x7f120f3d

.field public static final loyalty_points_default_plural:I = 0x7f120f3f

.field public static final loyalty_points_default_singular:I = 0x7f120f40

.field public static final loyalty_points_with_suffix:I = 0x7f120f43

.field public static final loyalty_rewards_amount:I = 0x7f120f6a

.field public static final loyalty_rewards_amount_new:I = 0x7f120f6b

.field public static final loyalty_rewards_term_plural:I = 0x7f120f6e

.field public static final loyalty_rewards_term_singular:I = 0x7f120f6f

.field public static final loyalty_rule_item:I = 0x7f120f74

.field public static final loyalty_rule_spend:I = 0x7f120f75

.field public static final loyalty_rule_visit:I = 0x7f120f76

.field public static final points_redeem_button:I = 0x7f121451

.field public static final points_redeem_button_applied:I = 0x7f121452

.field public static final points_redeem_button_disabled:I = 0x7f121453


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
