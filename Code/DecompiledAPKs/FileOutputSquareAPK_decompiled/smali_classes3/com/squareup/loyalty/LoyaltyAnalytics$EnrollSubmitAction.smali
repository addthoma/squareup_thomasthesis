.class Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "LoyaltyAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnrollSubmitAction"
.end annotation


# instance fields
.field final buyerToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "buyer_token"
    .end annotation
.end field

.field final clientPaymentToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_payment_token"
    .end annotation
.end field

.field final timeSpentOnScreen:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "time_spent_on_screen"
    .end annotation
.end field

.field final unitToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unit_token"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 101
    invoke-direct/range {v0 .. v6}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;DLcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;DLcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p6}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 107
    invoke-virtual {p3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;->clientPaymentToken:Ljava/lang/String;

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    move-object p6, p1

    goto :goto_0

    .line 108
    :cond_0
    iget-object p6, p3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    :goto_0
    iput-object p6, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;->unitToken:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 109
    invoke-static {p3}, Lcom/squareup/loyalty/LoyaltyAnalytics;->access$000(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;->buyerToken:Ljava/lang/String;

    .line 110
    iput-wide p4, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;->timeSpentOnScreen:D

    return-void
.end method
