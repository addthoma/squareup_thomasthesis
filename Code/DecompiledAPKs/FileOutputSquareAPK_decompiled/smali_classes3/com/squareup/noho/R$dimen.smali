.class public final Lcom/squareup/noho/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final noho_banner_height:I = 0x7f07035a

.field public static final noho_button_charge_height:I = 0x7f07035b

.field public static final noho_button_height:I = 0x7f07035c

.field public static final noho_button_height_two_lines:I = 0x7f07035d

.field public static final noho_button_horizontal_margin:I = 0x7f07035e

.field public static final noho_button_large_height:I = 0x7f07035f

.field public static final noho_button_two_lines_spacing:I = 0x7f070360

.field public static final noho_card_editor_min_height:I = 0x7f070361

.field public static final noho_checkable_row_widget_size:I = 0x7f070362

.field public static final noho_dialog_button_height:I = 0x7f070363

.field public static final noho_dialog_height:I = 0x7f070364

.field public static final noho_dialog_width:I = 0x7f070365

.field public static final noho_divider_hairline:I = 0x7f070366

.field public static final noho_divider_prominent:I = 0x7f070367

.field public static final noho_dropdown_icon_margin_end:I = 0x7f070368

.field public static final noho_dropdown_item_height:I = 0x7f070369

.field public static final noho_dropdown_item_padding_end:I = 0x7f07036a

.field public static final noho_dropdown_item_padding_start:I = 0x7f07036b

.field public static final noho_dropdown_menu_divider:I = 0x7f07036c

.field public static final noho_edit_default_margin:I = 0x7f07036d

.field public static final noho_edit_label_margin_with_text:I = 0x7f07036e

.field public static final noho_edit_note_margin_before:I = 0x7f07036f

.field public static final noho_edit_search_margin:I = 0x7f070370

.field public static final noho_gap_0:I = 0x7f070371

.field public static final noho_gap_12:I = 0x7f070372

.field public static final noho_gap_16:I = 0x7f070373

.field public static final noho_gap_20:I = 0x7f070374

.field public static final noho_gap_4:I = 0x7f070375

.field public static final noho_gap_40:I = 0x7f070376

.field public static final noho_gap_8:I = 0x7f070377

.field public static final noho_gap_big_not_converted_from_marin:I = 0x7f070378

.field public static final noho_gap_large_not_converted_from_marin:I = 0x7f070379

.field public static final noho_gap_medium_not_converted_from_marin:I = 0x7f07037a

.field public static final noho_gap_small_not_converted_from_marin:I = 0x7f07037b

.field public static final noho_gutter_card_alert_horizontal:I = 0x7f07037c

.field public static final noho_gutter_card_alert_vertical:I = 0x7f07037d

.field public static final noho_gutter_card_horizontal:I = 0x7f07037e

.field public static final noho_gutter_card_vertical:I = 0x7f07037f

.field public static final noho_gutter_detail:I = 0x7f070380

.field public static final noho_gutter_half_not_converted_from_marin:I = 0x7f070381

.field public static final noho_gutter_not_converted_from_marin:I = 0x7f070382

.field public static final noho_gutter_sheet_alert_horizontal:I = 0x7f070383

.field public static final noho_gutter_sheet_alert_vertical:I = 0x7f070384

.field public static final noho_gutter_sheet_horizontal:I = 0x7f070385

.field public static final noho_gutter_sheet_vertical:I = 0x7f070386

.field public static final noho_gutter_sheet_vertical_payment_flow:I = 0x7f070387

.field public static final noho_icon_button_padding:I = 0x7f070388

.field public static final noho_inner_shadow_height:I = 0x7f070389

.field public static final noho_message_null_state_icon_height:I = 0x7f07038a

.field public static final noho_message_text_height:I = 0x7f07038b

.field public static final noho_recycler_edges:I = 0x7f07038c

.field public static final noho_responsive_104_144_200:I = 0x7f07038d

.field public static final noho_responsive_16_16_16:I = 0x7f07038e

.field public static final noho_responsive_16_16_24:I = 0x7f07038f

.field public static final noho_responsive_16_24_32:I = 0x7f070390

.field public static final noho_responsive_24_24_40:I = 0x7f070391

.field public static final noho_responsive_40_40_64:I = 0x7f070392

.field public static final noho_responsive_4_4_8:I = 0x7f070393

.field public static final noho_responsive_56_64_96:I = 0x7f070394

.field public static final noho_responsive_text_13_13_21:I = 0x7f070395

.field public static final noho_responsive_text_14_16_20:I = 0x7f070396

.field public static final noho_responsive_text_16_18_24:I = 0x7f070397

.field public static final noho_responsive_text_18_22_28:I = 0x7f070398

.field public static final noho_responsive_text_24_26_34:I = 0x7f070399

.field public static final noho_responsive_text_32_40_56:I = 0x7f07039a

.field public static final noho_responsive_text_44_48_72:I = 0x7f07039b

.field public static final noho_row_accessory_gap_size:I = 0x7f07039c

.field public static final noho_row_accessory_size:I = 0x7f07039d

.field public static final noho_row_gap_size:I = 0x7f07039e

.field public static final noho_row_height:I = 0x7f07039f

.field public static final noho_row_height_payment_flow:I = 0x7f0703a0

.field public static final noho_row_section_header_lower_gap:I = 0x7f0703a1

.field public static final noho_row_section_header_upper_gap:I = 0x7f0703a2

.field public static final noho_row_standard_height:I = 0x7f0703a3

.field public static final noho_space_large:I = 0x7f0703a4

.field public static final noho_space_small:I = 0x7f0703a5

.field public static final noho_spacing_before_helper_text:I = 0x7f0703a6

.field public static final noho_spacing_large:I = 0x7f0703a7

.field public static final noho_spacing_small:I = 0x7f0703a8

.field public static final noho_switch_height:I = 0x7f0703ba

.field public static final noho_switch_padding:I = 0x7f0703bb

.field public static final noho_switch_thumb_size:I = 0x7f0703bc

.field public static final noho_switch_width:I = 0x7f0703bd

.field public static final noho_text_size_banner:I = 0x7f0703be

.field public static final noho_text_size_body:I = 0x7f0703bf

.field public static final noho_text_size_body_2:I = 0x7f0703c0

.field public static final noho_text_size_button:I = 0x7f0703c1

.field public static final noho_text_size_display:I = 0x7f0703c2

.field public static final noho_text_size_display_2:I = 0x7f0703c3

.field public static final noho_text_size_heading:I = 0x7f0703c4

.field public static final noho_text_size_heading_2:I = 0x7f0703c5

.field public static final noho_text_size_helper:I = 0x7f0703c6

.field public static final noho_text_size_hint:I = 0x7f0703c7

.field public static final noho_text_size_input:I = 0x7f0703c8

.field public static final noho_text_size_label:I = 0x7f0703c9

.field public static final noho_text_size_label_2:I = 0x7f0703ca

.field public static final noho_text_size_notification:I = 0x7f0703cb

.field public static final noho_text_size_oversized:I = 0x7f0703cc

.field public static final noho_text_size_row_text_icon:I = 0x7f0703cd

.field public static final noho_text_size_secondary_label:I = 0x7f0703ce

.field public static final noho_text_size_subheader:I = 0x7f0703cf

.field public static final noho_text_size_topbar_action:I = 0x7f0703d0

.field public static final noho_text_size_topbar_title:I = 0x7f0703d1

.field public static final noho_title_over_text_padding:I = 0x7f0703d2

.field public static final noho_topbar_action_width:I = 0x7f0703d3

.field public static final noho_topbar_height:I = 0x7f0703d4

.field public static final noho_topbar_icon:I = 0x7f0703d5

.field public static final noho_topbar_padding:I = 0x7f0703d6

.field public static final responsive_112_128_128_128:I = 0x7f070456

.field public static final responsive_12_16_24:I = 0x7f070457

.field public static final responsive_12_20_32:I = 0x7f070458

.field public static final responsive_160_184_216_240:I = 0x7f070459

.field public static final responsive_16P48L_64_72_96:I = 0x7f07045a

.field public static final responsive_16P96L_64P0L_72P0L_96P0L:I = 0x7f07045b

.field public static final responsive_16_16_24:I = 0x7f07045c

.field public static final responsive_16_20_24:I = 0x7f07045d

.field public static final responsive_16_20_24_32:I = 0x7f07045e

.field public static final responsive_16_20_24_40:I = 0x7f07045f

.field public static final responsive_16_20_96:I = 0x7f070460

.field public static final responsive_16_64P0L_72P0L_96P0L:I = 0x7f070462

.field public static final responsive_16_64_72_96:I = 0x7f070463

.field public static final responsive_192_240_384:I = 0x7f070464

.field public static final responsive_24_24_40:I = 0x7f070465

.field public static final responsive_24_32_40_40:I = 0x7f070466

.field public static final responsive_24_32_40_48:I = 0x7f070467

.field public static final responsive_24_32_48:I = 0x7f070468

.field public static final responsive_2_2_4:I = 0x7f070469

.field public static final responsive_320_400_480_560:I = 0x7f07046a

.field public static final responsive_32P48L_64_72_96:I = 0x7f07046b

.field public static final responsive_32_40_48_64:I = 0x7f07046c

.field public static final responsive_32_40_72:I = 0x7f07046d

.field public static final responsive_32_64_72_96:I = 0x7f07046e

.field public static final responsive_40_40_64:I = 0x7f070470

.field public static final responsive_48P96L_60P120L_96P192L:I = 0x7f070471

.field public static final responsive_48_48_60:I = 0x7f070472

.field public static final responsive_48_60_48:I = 0x7f070473

.field public static final responsive_48_60_72:I = 0x7f070474

.field public static final responsive_48_60_96:I = 0x7f070475

.field public static final responsive_4_4_8:I = 0x7f070476

.field public static final responsive_56_64_72_96:I = 0x7f070477

.field public static final responsive_56_64_96:I = 0x7f070478

.field public static final responsive_56_72_80_96:I = 0x7f070479

.field public static final responsive_56_72_96:I = 0x7f07047a

.field public static final responsive_60_60_72:I = 0x7f07047b

.field public static final responsive_60_72_96:I = 0x7f07047c

.field public static final responsive_64_72_104:I = 0x7f07047d

.field public static final responsive_8_12_15:I = 0x7f07047e

.field public static final responsive_96P192L_120P240L_192P384L:I = 0x7f07047f

.field public static final responsive_96_120_192:I = 0x7f070480

.field public static final responsive_96_128_128_128:I = 0x7f070481

.field public static final responsive_text_13_14_16_18:I = 0x7f070486

.field public static final responsive_text_13_15_17_20:I = 0x7f070487

.field public static final responsive_text_16_18_21_24:I = 0x7f070488

.field public static final responsive_text_18_21_24_28:I = 0x7f070489

.field public static final responsive_text_24_27_31_36:I = 0x7f07048a

.field public static final responsive_text_32_40_56_56:I = 0x7f07048b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
