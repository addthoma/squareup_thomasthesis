.class final Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationDrawable.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NotificationDrawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/res/TypedArray;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDrawable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDrawable.kt\ncom/squareup/noho/NotificationDrawable$inflate$1$1\n*L\n1#1,281:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Landroid/content/res/TypedArray;",
        "invoke",
        "com/squareup/noho/NotificationDrawable$inflate$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $theme$inlined:Landroid/content/res/Resources$Theme;

.field final synthetic this$0:Lcom/squareup/noho/NotificationDrawable;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/Resources$Theme;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->this$0:Lcom/squareup/noho/NotificationDrawable;

    iput-object p2, p0, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->$theme$inlined:Landroid/content/res/Resources$Theme;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 52
    check-cast p1, Landroid/content/res/TypedArray;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->invoke(Landroid/content/res/TypedArray;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/res/TypedArray;)V
    .locals 4

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget v0, Lcom/squareup/noho/R$styleable;->NotificationDrawable_android_drawable:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 111
    iget-object v1, p0, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->this$0:Lcom/squareup/noho/NotificationDrawable;

    iget-object v2, p0, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->$theme$inlined:Landroid/content/res/Resources$Theme;

    const-string/jumbo v3, "theme"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1, v2, v0}, Lcom/squareup/noho/NotificationDrawable;->access$configureFrom(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/TypedArray;Landroid/content/res/Resources$Theme;I)V

    .line 112
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;->this$0:Lcom/squareup/noho/NotificationDrawable;

    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawable_android_text:I

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NotificationDrawable;->setText(Ljava/lang/String;)V

    return-void
.end method
