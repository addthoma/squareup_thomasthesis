.class public abstract Lcom/squareup/noho/ListenerAttacher;
.super Ljava/lang/Object;
.source "CheckableGroups.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;,
        Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;,
        Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002:\u0003\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J9\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u00002\"\u0010\u0007\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u0008j\u0008\u0012\u0004\u0012\u00028\u0000`\nH&\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u000c\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\r\u0082\u0001\u0003\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/noho/ListenerAttacher;",
        "T",
        "",
        "()V",
        "attach",
        "",
        "item",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V",
        "detach",
        "(Ljava/lang/Object;)V",
        "CheckableViewAttacher",
        "CompoundButtonAttacher",
        "ListenableCheckableAttacher",
        "Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;",
        "Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;",
        "Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 246
    invoke-direct {p0}, Lcom/squareup/noho/ListenerAttacher;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract attach(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract detach(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
