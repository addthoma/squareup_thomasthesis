.class public final Lcom/squareup/noho/NohoCheckableRowKt;
.super Ljava/lang/Object;
.source "NohoCheckableRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "CHECKED_STATE_SET",
        "",
        "getCHECKED_STATE_SET",
        "()[I",
        "CHECK_TYPES_FROM_XML",
        "",
        "Lcom/squareup/noho/CheckType;",
        "[Lcom/squareup/noho/CheckType;",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKED_STATE_SET:[I

.field private static final CHECK_TYPES_FROM_XML:[Lcom/squareup/noho/CheckType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/CheckType;

    .line 131
    sget-object v1, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v1, Lcom/squareup/noho/CheckType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    check-cast v1, Lcom/squareup/noho/CheckType;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/noho/CheckType$SWITCH;->INSTANCE:Lcom/squareup/noho/CheckType$SWITCH;

    check-cast v1, Lcom/squareup/noho/CheckType;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/noho/NohoCheckableRowKt;->CHECK_TYPES_FROM_XML:[Lcom/squareup/noho/CheckType;

    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    .line 204
    sput-object v0, Lcom/squareup/noho/NohoCheckableRowKt;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public static final synthetic access$getCHECK_TYPES_FROM_XML$p()[Lcom/squareup/noho/CheckType;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoCheckableRowKt;->CHECK_TYPES_FROM_XML:[Lcom/squareup/noho/CheckType;

    return-object v0
.end method

.method public static final getCHECKED_STATE_SET()[I
    .locals 1

    .line 204
    sget-object v0, Lcom/squareup/noho/NohoCheckableRowKt;->CHECKED_STATE_SET:[I

    return-object v0
.end method
