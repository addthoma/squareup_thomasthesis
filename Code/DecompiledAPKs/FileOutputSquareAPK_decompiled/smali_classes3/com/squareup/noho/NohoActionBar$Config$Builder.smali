.class public final Lcom/squareup/noho/NohoActionBar$Config$Builder;
.super Ljava/lang/Object;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoActionBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoActionBar.kt\ncom/squareup/noho/NohoActionBar$Config$Builder\n*L\n1#1,418:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002BG\u0008\u0000\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0010\u0008\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000eJ\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0000J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0000J4\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u00172\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u0019\u001a\u00020\t2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001bJ@\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u0019\u001a\u00020\t2\u0008\u0008\u0002\u0010 \u001a\u00020\u001e2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001bJ\u000e\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u0004J\u0016\u0010#\u001a\u00020\u00002\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006J\u000e\u0010$\u001a\u00020\u00002\u0006\u0010%\u001a\u00020\tJ\u001c\u0010&\u001a\u00020\u00002\u0006\u0010\'\u001a\u00020(2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001bJ,\u0010&\u001a\u00020\u00002\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001bR\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "",
        "()V",
        "leftBadge",
        "Lcom/squareup/noho/Badge;",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "centeredTitle",
        "",
        "upButton",
        "Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
        "action",
        "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
        "(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V",
        "build",
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "hideAction",
        "hideLeftBadge",
        "",
        "hideUpButton",
        "setActionButton",
        "style",
        "Lcom/squareup/noho/NohoActionButtonStyle;",
        "label",
        "isEnabled",
        "command",
        "Lkotlin/Function0;",
        "setActionIcon",
        "iconRes",
        "",
        "tooltip",
        "countBadge",
        "setLeftBadge",
        "badge",
        "setTitle",
        "setTitleCentered",
        "isTitleCentered",
        "setUpButton",
        "icon",
        "Lcom/squareup/noho/UpIcon;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

.field private centeredTitle:Z

.field private leftBadge:Lcom/squareup/noho/Badge;

.field private title:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1d

    const/4 v7, 0x0

    move-object v0, p0

    .line 228
    invoke-direct/range {v0 .. v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/Badge;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;Z",
            "Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
            "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
            ")V"
        }
    .end annotation

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->leftBadge:Lcom/squareup/noho/Badge;

    iput-object p2, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->title:Lcom/squareup/resources/TextModel;

    iput-boolean p3, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->centeredTitle:Z

    iput-object p4, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iput-object p5, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 221
    move-object p1, v0

    check-cast p1, Lcom/squareup/noho/Badge;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 222
    move-object p2, v0

    check-cast p2, Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    const/4 p3, 0x0

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, p3

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 224
    move-object p4, v0

    check-cast p4, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    :cond_3
    move-object v2, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 225
    move-object p5, v0

    check-cast p5, Lcom/squareup/noho/NohoActionBar$ActionConfig;

    :cond_4
    move-object v0, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v1

    move-object p6, v2

    move-object p7, v0

    invoke-direct/range {p2 .. p7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V

    return-void
.end method

.method public static synthetic setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x1

    .line 292
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic setActionIcon$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    const/4 p3, 0x1

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    const/4 p4, -0x1

    const/4 v4, -0x1

    goto :goto_1

    :cond_1
    move v4, p4

    :goto_1
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v5, p5

    .line 279
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionIcon(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final build()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 7

    .line 309
    new-instance v6, Lcom/squareup/noho/NohoActionBar$Config;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->leftBadge:Lcom/squareup/noho/Badge;

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->title:Lcom/squareup/resources/TextModel;

    iget-boolean v3, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->centeredTitle:Z

    iget-object v4, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iget-object v5, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoActionBar$Config;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V

    return-object v6
.end method

.method public final hideAction()Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 2

    .line 299
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    const/4 v1, 0x0

    .line 300
    check-cast v1, Lcom/squareup/noho/NohoActionBar$ActionConfig;

    iput-object v1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-object v0
.end method

.method public final hideLeftBadge()V
    .locals 1

    const/4 v0, 0x0

    .line 240
    check-cast v0, Lcom/squareup/noho/Badge;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->leftBadge:Lcom/squareup/noho/Badge;

    return-void
.end method

.method public final hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 2

    .line 267
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    const/4 v1, 0x0

    .line 268
    check-cast v1, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iput-object v1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    return-object v0
.end method

.method public final setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoActionButtonStyle;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;"
        }
    .end annotation

    const-string v0, "style"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 295
    new-instance v1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;-><init>(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/noho/NohoActionBar$ActionConfig;

    iput-object v1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-object v0
.end method

.method public final setActionIcon(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;ZI",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;"
        }
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 282
    new-instance v7, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    move-object v1, v7

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;-><init>(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)V

    check-cast v7, Lcom/squareup/noho/NohoActionBar$ActionConfig;

    iput-object v7, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-object v0
.end method

.method public final setLeftBadge(Lcom/squareup/noho/Badge;)V
    .locals 1

    const-string v0, "badge"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iput-object p1, p0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->leftBadge:Lcom/squareup/noho/Badge;

    return-void
.end method

.method public final setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;"
        }
    .end annotation

    .line 231
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 232
    iput-object p1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final setTitleCentered(Z)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 1

    .line 304
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 305
    iput-boolean p1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->centeredTitle:Z

    return-object v0
.end method

.method public final setUpButton(ILcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;"
        }
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 252
    new-instance v1, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;-><init>(ILcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;)V

    iput-object v1, v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    return-object v0
.end method

.method public final setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/UpIcon;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;"
        }
    .end annotation

    const-string v0, "icon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 263
    invoke-virtual {p1}, Lcom/squareup/noho/UpIcon;->getIconRes()I

    move-result v1

    new-instance v2, Lcom/squareup/resources/ResourceString;

    invoke-virtual {p1}, Lcom/squareup/noho/UpIcon;->getTooltipRes()I

    move-result p1

    invoke-direct {v2, p1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1, v2, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(ILcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    return-object v0
.end method
