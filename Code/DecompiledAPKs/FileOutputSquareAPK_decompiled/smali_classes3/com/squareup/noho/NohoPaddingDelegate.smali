.class public Lcom/squareup/noho/NohoPaddingDelegate;
.super Ljava/lang/Object;
.source "NohoPaddingDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;,
        Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;,
        Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;
    }
.end annotation


# static fields
.field private static final NO_PADDING:I

.field private static final XML_VALUES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private paddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field private final resources:Landroid/content/res/Resources;

.field private final screenHeight:I

.field private final screenMinDimen:I

.field private final screenWidth:I

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    invoke-static {}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->values()[Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate;->XML_VALUES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->view:Landroid/view/View;

    .line 191
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->resources:Landroid/content/res/Resources;

    .line 193
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/noho/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 194
    iget v0, p1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenWidth:I

    .line 195
    iget p1, p1, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenHeight:I

    .line 196
    iget p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenWidth:I

    iget v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenHeight:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenMinDimen:I

    return-void
.end method

.method public static getEnum(Landroid/content/res/TypedArray;I)Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;
    .locals 2

    .line 29
    sget-object v0, Lcom/squareup/noho/NohoPaddingDelegate;->XML_VALUES:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/squareup/noho/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    return-object p0
.end method

.method private getPaddingSize(I)I
    .locals 1

    if-eqz p1, :cond_0

    .line 219
    iget-object v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->resources:Landroid/content/res/Resources;

    .line 220
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public setContentPadding(Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;Z)V
    .locals 3

    if-eqz p2, :cond_1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->supportsAlert()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Cannot use alert padding for %s!"

    .line 207
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 209
    :cond_1
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->getPaddingClass(Z)Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->paddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 211
    iget-object p1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->paddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    invoke-static {p1}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->access$300(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/noho/NohoPaddingDelegate;->resources:Landroid/content/res/Resources;

    iget v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenMinDimen:I

    iget v1, p0, Lcom/squareup/noho/NohoPaddingDelegate;->screenWidth:I

    iget-object v2, p0, Lcom/squareup/noho/NohoPaddingDelegate;->paddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 212
    invoke-static {v2}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->access$200(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/squareup/noho/NohoPaddingDelegate;->getPaddingSize(I)I

    move-result v2

    .line 211
    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->computeHorizontalPadding(Landroid/content/res/Resources;III)I

    move-result p1

    .line 213
    iget-object p2, p0, Lcom/squareup/noho/NohoPaddingDelegate;->paddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    invoke-static {p2}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->access$400(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)I

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/noho/NohoPaddingDelegate;->getPaddingSize(I)I

    move-result p2

    .line 215
    iget-object v0, p0, Lcom/squareup/noho/NohoPaddingDelegate;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p1, p2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method
