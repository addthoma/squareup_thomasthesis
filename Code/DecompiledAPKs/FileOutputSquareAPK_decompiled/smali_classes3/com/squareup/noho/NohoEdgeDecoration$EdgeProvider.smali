.class public interface abstract Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;
.super Ljava/lang/Object;
.source "NohoEdgeDecoration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEdgeDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EdgeProvider"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u001a\u0010\u0008\u001a\u00020\t2\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\n\u001a\u00020\tH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;",
        "",
        "dividerPadding",
        "",
        "vh",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "padding",
        "Landroid/graphics/Rect;",
        "edges",
        "",
        "index",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract dividerPadding(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroid/graphics/Rect;)V
.end method

.method public abstract edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I
.end method
