.class public final Lcom/squareup/noho/BadgeParceler;
.super Ljava/lang/Object;
.source "NohoActionBar.kt"

# interfaces
.implements Lkotlinx/android/parcel/Parceler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlinx/android/parcel/Parceler<",
        "Lcom/squareup/noho/Badge;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoActionBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoActionBar.kt\ncom/squareup/noho/BadgeParceler\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,418:1\n180#2:419\n148#2:420\n*E\n*S KotlinDebug\n*F\n+ 1 NohoActionBar.kt\ncom/squareup/noho/BadgeParceler\n*L\n398#1:419\n398#1:420\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001c\u0010\u0007\u001a\u00020\u0008*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/noho/BadgeParceler;",
        "Lkotlinx/android/parcel/Parceler;",
        "Lcom/squareup/noho/Badge;",
        "()V",
        "create",
        "parcel",
        "Landroid/os/Parcel;",
        "write",
        "",
        "flags",
        "",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/BadgeParceler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 391
    new-instance v0, Lcom/squareup/noho/BadgeParceler;

    invoke-direct {v0}, Lcom/squareup/noho/BadgeParceler;-><init>()V

    sput-object v0, Lcom/squareup/noho/BadgeParceler;->INSTANCE:Lcom/squareup/noho/BadgeParceler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/os/Parcel;)Lcom/squareup/noho/Badge;
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 394
    new-array v0, v0, [B

    .line 395
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 397
    sget-object p1, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object p1

    .line 419
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 399
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 420
    const-class v1, Lcom/squareup/noho/Badge$Type;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    aget-object p1, v1, p1

    const-string v1, "T::class.java.enumConstants[readInt()]"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    check-cast p1, Lcom/squareup/noho/Badge$Type;

    .line 401
    new-instance v1, Lcom/squareup/noho/Badge;

    invoke-direct {v1, v0, p1}, Lcom/squareup/noho/Badge;-><init>(Ljava/lang/String;Lcom/squareup/noho/Badge$Type;)V

    return-object v1
.end method

.method public bridge synthetic create(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 391
    invoke-virtual {p0, p1}, Lcom/squareup/noho/BadgeParceler;->create(Landroid/os/Parcel;)Lcom/squareup/noho/Badge;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/noho/Badge;
    .locals 0

    .line 391
    invoke-static {p0, p1}, Lkotlinx/android/parcel/Parceler$DefaultImpls;->newArray(Lkotlinx/android/parcel/Parceler;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/noho/Badge;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 391
    invoke-virtual {p0, p1}, Lcom/squareup/noho/BadgeParceler;->newArray(I)[Lcom/squareup/noho/Badge;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/squareup/noho/Badge;Landroid/os/Parcel;I)V
    .locals 1

    const-string p3, "$this$write"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "parcel"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    sget-object p3, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v0, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;

    invoke-direct {v0, p1}, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;-><init>(Lcom/squareup/noho/Badge;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p3, v0}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 413
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 414
    array-length p3, p1

    invoke-virtual {p2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 415
    invoke-virtual {p2, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Landroid/os/Parcel;I)V
    .locals 0

    .line 391
    check-cast p1, Lcom/squareup/noho/Badge;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/noho/BadgeParceler;->write(Lcom/squareup/noho/Badge;Landroid/os/Parcel;I)V

    return-void
.end method
