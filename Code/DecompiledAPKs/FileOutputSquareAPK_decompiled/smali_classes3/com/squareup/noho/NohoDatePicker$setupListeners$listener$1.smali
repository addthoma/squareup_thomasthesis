.class final Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;
.super Ljava/lang/Object;
.source "NohoDatePicker.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoDatePicker;->setupListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "<anonymous parameter 2>",
        "onValueChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoDatePicker;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoDatePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;->this$0:Lcom/squareup/noho/NohoDatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onValueChange(Lcom/squareup/noho/NohoNumberPicker;II)V
    .locals 0

    .line 108
    iget-object p1, p0, Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;->this$0:Lcom/squareup/noho/NohoDatePicker;

    invoke-static {p1}, Lcom/squareup/noho/NohoDatePicker;->access$recalculateMaxPickerValues(Lcom/squareup/noho/NohoDatePicker;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;->this$0:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoDatePicker;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/noho/NohoDatePicker;->access$notifyChange(Lcom/squareup/noho/NohoDatePicker;Lorg/threeten/bp/LocalDate;)Lkotlin/Unit;

    return-void
.end method
