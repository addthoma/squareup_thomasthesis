.class public interface abstract Lcom/squareup/noho/NohoRow$ArrangeableProvider;
.super Ljava/lang/Object;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "ArrangeableProvider"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008d\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
        "",
        "get",
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
.end method
