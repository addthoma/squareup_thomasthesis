.class public final Lcom/squareup/noho/NohoEdgeController;
.super Ljava/lang/Object;
.source "NohoEdgeController.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0007R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEdgeController;",
        "",
        "hostView",
        "Landroid/view/View;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "defStyleRes",
        "(Landroid/view/View;Landroid/util/AttributeSet;II)V",
        "borderPainter",
        "Lcom/squareup/noho/EdgePainter;",
        "focusPainter",
        "hideShadow",
        "",
        "shadowPainter",
        "onDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "setBorderEdges",
        "edges",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final borderPainter:Lcom/squareup/noho/EdgePainter;

.field private final focusPainter:Lcom/squareup/noho/EdgePainter;

.field private final hideShadow:Z

.field private final hostView:Landroid/view/View;

.field private final shadowPainter:Lcom/squareup/noho/EdgePainter;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/util/AttributeSet;II)V
    .locals 2

    const-string v0, "hostView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 33
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoEdges:[I

    .line 32
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "hostView.context.obtainS\u2026leAttr, defStyleRes\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEdges_sqFocusHeight:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 37
    sget p4, Lcom/squareup/noho/R$styleable;->NohoEdges_sqFocusColor:I

    invoke-virtual {p1, p4, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p4

    .line 38
    new-instance v0, Lcom/squareup/noho/EdgePainter;

    iget-object v1, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    invoke-direct {v0, v1, p2, p4}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->focusPainter:Lcom/squareup/noho/EdgePainter;

    .line 39
    iget-object p2, p0, Lcom/squareup/noho/NohoEdgeController;->focusPainter:Lcom/squareup/noho/EdgePainter;

    const/16 p4, 0x8

    invoke-virtual {p2, p4}, Lcom/squareup/noho/EdgePainter;->addEdges(I)V

    .line 41
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEdges_sqEdgeWidth:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 42
    sget p4, Lcom/squareup/noho/R$styleable;->NohoEdges_sqEdgeColor:I

    invoke-virtual {p1, p4, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p4

    .line 43
    new-instance v0, Lcom/squareup/noho/EdgePainter;

    iget-object v1, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    invoke-direct {v0, v1, p2, p4}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->borderPainter:Lcom/squareup/noho/EdgePainter;

    .line 44
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEdges_sqHideBorder:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 45
    iget-object p4, p0, Lcom/squareup/noho/NohoEdgeController;->borderPainter:Lcom/squareup/noho/EdgePainter;

    not-int p2, p2

    and-int/lit8 p2, p2, 0xf

    invoke-virtual {p4, p2}, Lcom/squareup/noho/EdgePainter;->addEdges(I)V

    .line 47
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEdges_sqInnerShadowHeight:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 48
    sget p4, Lcom/squareup/noho/R$styleable;->NohoEdges_sqInnerShadowColor:I

    invoke-virtual {p1, p4, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p4

    .line 49
    new-instance v0, Lcom/squareup/noho/EdgePainter;

    iget-object v1, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    invoke-direct {v0, v1, p2, p4}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    .line 50
    iget-object p2, p0, Lcom/squareup/noho/NohoEdgeController;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    const/4 p4, 0x2

    invoke-virtual {p2, p4}, Lcom/squareup/noho/EdgePainter;->addEdges(I)V

    .line 52
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEdges_sqHideShadow:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/noho/NohoEdgeController;->hideShadow:Z

    .line 53
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 22
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 24
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NohoEdgeController;-><init>(Landroid/view/View;Landroid/util/AttributeSet;II)V

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEdgeController;->hideShadow:Z

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->borderPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->hostView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->focusPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method public final setBorderEdges(I)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeController;->borderPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    return-void
.end method
