.class public final enum Lcom/squareup/noho/NohoLabel$Type;
.super Ljava/lang/Enum;
.source "NohoLabel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoLabel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoLabel$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000e\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/noho/NohoLabel$Type;",
        "",
        "styleAttr",
        "",
        "styleRes",
        "(Ljava/lang/String;III)V",
        "getStyleAttr",
        "()I",
        "getStyleRes",
        "DISPLAY",
        "DISPLAY_2",
        "HEADING",
        "HEADING_2",
        "BODY",
        "BODY_2",
        "LABEL",
        "LABEL_2",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum BODY:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum BODY_2:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum DISPLAY:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum DISPLAY_2:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum HEADING:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum HEADING_2:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum LABEL:Lcom/squareup/noho/NohoLabel$Type;

.field public static final enum LABEL_2:Lcom/squareup/noho/NohoLabel$Type;


# instance fields
.field private final styleAttr:I

.field private final styleRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/noho/NohoLabel$Type;

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 48
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelDisplayStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Display:I

    const/4 v4, 0x0

    const-string v5, "DISPLAY"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->DISPLAY:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 49
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelDisplay2Style:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Display2:I

    const/4 v4, 0x1

    const-string v5, "DISPLAY_2"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->DISPLAY_2:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 50
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelHeadingStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Heading:I

    const/4 v4, 0x2

    const-string v5, "HEADING"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->HEADING:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 51
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelHeading2Style:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Heading2:I

    const/4 v4, 0x3

    const-string v5, "HEADING_2"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->HEADING_2:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 52
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelBodyStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Body:I

    const/4 v4, 0x4

    const-string v5, "BODY"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->BODY:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 53
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelBody2Style:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Body2:I

    const/4 v4, 0x5

    const-string v5, "BODY_2"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->BODY_2:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 54
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelLabelStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Label:I

    const/4 v4, 0x6

    const-string v5, "LABEL"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->LABEL:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoLabel$Type;

    .line 55
    sget v2, Lcom/squareup/noho/R$attr;->nohoLabelLabel2Style:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Label2:I

    const/4 v4, 0x7

    const-string v5, "LABEL_2"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoLabel$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoLabel$Type;->LABEL_2:Lcom/squareup/noho/NohoLabel$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/noho/NohoLabel$Type;->$VALUES:[Lcom/squareup/noho/NohoLabel$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/noho/NohoLabel$Type;->styleAttr:I

    iput p4, p0, Lcom/squareup/noho/NohoLabel$Type;->styleRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoLabel$Type;
    .locals 1

    const-class v0, Lcom/squareup/noho/NohoLabel$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoLabel$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoLabel$Type;
    .locals 1

    sget-object v0, Lcom/squareup/noho/NohoLabel$Type;->$VALUES:[Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoLabel$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoLabel$Type;

    return-object v0
.end method


# virtual methods
.method public final getStyleAttr()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/noho/NohoLabel$Type;->styleAttr:I

    return v0
.end method

.method public final getStyleRes()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/noho/NohoLabel$Type;->styleRes:I

    return v0
.end method
