.class final Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;
.super Ljava/lang/Object;
.source "RealNotificationCenterApplet.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1;->apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/applet/Applet$Badge;",
        "badgeState",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;->INSTANCE:Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;)Lcom/squareup/applet/Applet$Badge;
    .locals 3

    const-string v0, "badgeState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    instance-of v0, p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowImportant;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    check-cast p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowImportant;

    invoke-virtual {p1}, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowImportant;->getCount()I

    move-result p1

    sget-object v1, Lcom/squareup/applet/Applet$Badge$Priority;->NORMAL:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    goto :goto_0

    .line 83
    :cond_0
    sget-object v0, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$NoNotification;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$NoNotification;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    goto :goto_0

    .line 84
    :cond_1
    sget-object v0, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowGeneral;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowGeneral;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/applet/Applet$Badge$Visible;

    .line 86
    sget-object v0, Lcom/squareup/applet/Applet$Badge$Priority;->NORMAL:Lcom/squareup/applet/Applet$Badge$Priority;

    const-string v2, ""

    .line 87
    check-cast v2, Ljava/lang/CharSequence;

    .line 84
    invoke-direct {p1, v1, v2, v0}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    goto :goto_0

    .line 89
    :cond_2
    sget-object v0, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowCritical;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowCritical;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    sget-object v0, Lcom/squareup/applet/Applet$Badge$Priority;->FATAL:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1$1;->apply(Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method
