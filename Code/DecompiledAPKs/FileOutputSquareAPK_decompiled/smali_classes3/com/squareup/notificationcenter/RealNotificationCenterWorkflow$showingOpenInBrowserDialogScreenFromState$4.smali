.class final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealNotificationCenterWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)Lcom/squareup/notificationcenter/NotificationCenterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

.field final synthetic this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/notificationcenterdata/Notification;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->invoke(Lcom/squareup/notificationcenterdata/Notification;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 4

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getNotificationResolver$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenterdata/NotificationResolver;

    move-result-object v0

    .line 149
    invoke-interface {v0, p1}, Lcom/squareup/notificationcenterdata/NotificationResolver;->resolve(Lcom/squareup/notificationcenterdata/Notification;)Lio/reactivex/Completable;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 151
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logNotificationOpened(Lcom/squareup/notificationcenterdata/Notification;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->$sink:Lcom/squareup/workflow/Sink;

    .line 155
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->access$getOpenInBrowser$p(Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    new-instance v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$ShowBrowserDialog;

    .line 157
    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    .line 159
    iget-object v3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v3

    .line 156
    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$ShowBrowserDialog;-><init>(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    check-cast v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;

    goto :goto_0

    .line 162
    :cond_0
    new-instance v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    invoke-direct {v1, p1, v2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V

    check-cast v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;

    .line 152
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
