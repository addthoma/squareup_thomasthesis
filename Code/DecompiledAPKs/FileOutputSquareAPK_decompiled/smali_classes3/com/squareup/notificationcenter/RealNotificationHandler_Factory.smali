.class public final Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;
.super Ljava/lang/Object;
.source "RealNotificationHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenter/RealNotificationHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;)",
            "Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationHandler;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationHandler;-><init>(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenter/RealNotificationHandler;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/BrowserLauncher;

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->newInstance(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/RealNotificationHandler_Factory;->get()Lcom/squareup/notificationcenter/RealNotificationHandler;

    move-result-object v0

    return-object v0
.end method
