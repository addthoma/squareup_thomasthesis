.class final Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "OpenInBrowserDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "onCancel",
        "com/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$3$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dialogView$inlined:Landroid/view/View;

.field final synthetic $screen$inlined:Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;->$dialogView$inlined:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;->$screen$inlined:Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .line 73
    iget-object p1, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;->$screen$inlined:Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;->getOnDismissPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
