.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;
.super Ljava/lang/Object;
.source "RealNotificationCenterWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterWorkflow.kt\ncom/squareup/notificationcenter/RealNotificationCenterWorkflowKt\n*L\n1#1,366:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u001e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\"\u001e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006*\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u000b\"\u0018\u0010\u000c\u001a\u00020\u0001*\u00020\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "hasNotifications",
        "",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
        "getHasNotifications",
        "(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Z",
        "notifications",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "getNotifications",
        "(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
        "(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Ljava/util/List;",
        "openInBrowser",
        "getOpenInBrowser",
        "(Lcom/squareup/notificationcenterdata/Notification;)Z",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getHasNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->getHasNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->getNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->getNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOpenInBrowser$p(Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->getOpenInBrowser(Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result p0

    return p0
.end method

.method private static final getHasNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Z
    .locals 0

    .line 344
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->getNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private static final getNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 350
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    .line 351
    instance-of v1, v0, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getImportantNotifications()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 352
    :cond_0
    instance-of v0, v0, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getGeneralNotifications()Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getNotifications(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 359
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    .line 360
    instance-of v1, v0, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getImportantNotifications()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 361
    :cond_0
    instance-of v0, v0, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getGeneralNotifications()Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getOpenInBrowser(Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 1

    .line 365
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object p0

    instance-of p0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
