.class public Lcom/squareup/notification/LocaleChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocaleChangeReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1a

    if-lt p2, v0, :cond_0

    .line 16
    new-instance p2, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    .line 17
    new-instance v0, Lcom/squareup/notification/NotificationWrapper;

    invoke-direct {v0, p2}, Lcom/squareup/notification/NotificationWrapper;-><init>(Lcom/squareup/util/Res;)V

    .line 18
    invoke-virtual {v0, p1}, Lcom/squareup/notification/NotificationWrapper;->localizeChannelNames(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
