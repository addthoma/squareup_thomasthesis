.class public final Lcom/squareup/mosaic/lists/ModelsList;
.super Ljava/lang/Object;
.source "ModelsList.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/lists/ModelsList$Changes;,
        Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;,
        Lcom/squareup/mosaic/lists/ModelsList$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/mosaic/core/UiModel<",
        "TP;>;P::",
        "Lcom/squareup/mosaic/lists/IdParams;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nModelsList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ModelsList.kt\ncom/squareup/mosaic/lists/ModelsList\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n+ 4 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,196:1\n1313#2:197\n1382#2,3:198\n1313#2:201\n1382#2,3:202\n37#3,2:205\n1084#4,2:207\n*E\n*S KotlinDebug\n*F\n+ 1 ModelsList.kt\ncom/squareup/mosaic/lists/ModelsList\n*L\n35#1:197\n35#1,3:198\n177#1:201\n177#1,3:202\n178#1,2:205\n188#1,2:207\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000*\u000e\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00030\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0005:\u0003%&\'B\u001b\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00000\t\u00a2\u0006\u0002\u0010\nJ\u0014\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0010J$\u0010\u001c\u001a\u00020\u001a2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00102\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0010H\u0002J\u000e\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!J\u0006\u0010\"\u001a\u00020#J \u0010$\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u00030\u00170\u0010*\u0008\u0012\u0004\u0012\u00028\u00000\u0010R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00000\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\"\u0010\u000f\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001f\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00170\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/mosaic/lists/ModelsList;",
        "T",
        "Lcom/squareup/mosaic/core/UiModel;",
        "P",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "",
        "context",
        "Landroid/content/Context;",
        "callback",
        "Lcom/squareup/mosaic/lists/ModelsList$Changes;",
        "(Landroid/content/Context;Lcom/squareup/mosaic/lists/ModelsList$Changes;)V",
        "getCallback",
        "()Lcom/squareup/mosaic/lists/ModelsList$Changes;",
        "getContext",
        "()Landroid/content/Context;",
        "currentItems",
        "",
        "getCurrentItems",
        "()Ljava/util/List;",
        "setCurrentItems",
        "(Ljava/util/List;)V",
        "subViews",
        "",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "getSubViews",
        "bind",
        "",
        "subItems",
        "diffing",
        "oldList",
        "newList",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "Lcom/squareup/mosaic/lists/ModelsList$State;",
        "toViews",
        "Changes",
        "ListsComparator",
        "State",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final callback:Lcom/squareup/mosaic/lists/ModelsList$Changes;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/lists/ModelsList$Changes<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private currentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+TT;>;"
        }
    .end annotation
.end field

.field private final subViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/mosaic/lists/ModelsList$Changes;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/lists/ModelsList$Changes<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/mosaic/lists/ModelsList;->callback:Lcom/squareup/mosaic/lists/ModelsList$Changes;

    .line 33
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    return-void
.end method

.method private final diffing(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    .line 125
    new-instance v0, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/mosaic/lists/ModelsList$diffing$results$1;-><init>(Lcom/squareup/mosaic/lists/ModelsList;Ljava/util/List;)V

    .line 164
    sget-object v1, Lcom/squareup/mosaic/lists/DiffUtilX;->INSTANCE:Lcom/squareup/mosaic/lists/DiffUtilX;

    .line 165
    new-instance v2, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;

    iget-object v3, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    new-instance v4, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;

    invoke-direct {v4, p0, p1, p2}, Lcom/squareup/mosaic/lists/ModelsList$diffing$1;-><init>(Lcom/squareup/mosaic/lists/ModelsList;Ljava/util/List;Ljava/util/List;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-direct {v2, v3, p1, p2, v4}, Lcom/squareup/mosaic/lists/ModelsList$ListsComparator;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lkotlin/jvm/functions/Function2;)V

    check-cast v2, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    .line 172
    check-cast v0, Lcom/squareup/mosaic/lists/DiffUtilX$Results;

    .line 164
    invoke-virtual {v1, v2, v0}, Lcom/squareup/mosaic/lists/DiffUtilX;->calculateDiff(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;Lcom/squareup/mosaic/lists/DiffUtilX$Results;)V

    return-void
.end method


# virtual methods
.method public final bind(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "subItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->currentItems:Ljava/util/List;

    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 56
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/squareup/mosaic/lists/ModelsList;->diffing(Ljava/util/List;Ljava/util/List;)V

    goto :goto_1

    .line 48
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 49
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->callback:Lcom/squareup/mosaic/lists/ModelsList$Changes;

    invoke-interface {v0}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onCleared()V

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/lists/ModelsList;->toViews(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 53
    iget-object v1, p0, Lcom/squareup/mosaic/lists/ModelsList;->callback:Lcom/squareup/mosaic/lists/ModelsList$Changes;

    const/4 v2, 0x0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 54
    sget-object v3, Lcom/squareup/mosaic/lists/ModelsList$bind$1$1;->INSTANCE:Lkotlin/reflect/KProperty1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 53
    invoke-static {v0, v3}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-interface {v1, v2, p1, v0}, Lcom/squareup/mosaic/lists/ModelsList$Changes;->onInserted(ILjava/util/List;Lkotlin/sequences/Sequence;)V

    .line 59
    :goto_1
    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList;->currentItems:Ljava/util/List;

    return-void
.end method

.method public final getCallback()Lcom/squareup/mosaic/lists/ModelsList$Changes;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/lists/ModelsList$Changes<",
            "TT;>;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->callback:Lcom/squareup/mosaic/lists/ModelsList$Changes;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->context:Landroid/content/Context;

    return-object v0
.end method

.method public final getCurrentItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->currentItems:Ljava/util/List;

    return-object v0
.end method

.method public final getSubViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    return-object v0
.end method

.method public final restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    instance-of v0, p1, Lcom/squareup/mosaic/lists/ModelsList$State;

    if-eqz v0, :cond_1

    .line 186
    check-cast p1, Lcom/squareup/mosaic/lists/ModelsList$State;

    invoke-virtual {p1}, Lcom/squareup/mosaic/lists/ModelsList$State;->getItems()[Landroid/os/Parcelable;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/ArraysKt;->asSequence([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 187
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 207
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    .line 189
    invoke-virtual {v0, v1}, Lcom/squareup/mosaic/core/ViewRef;->restoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_0
    return-void

    .line 183
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected parcelable to be "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lcom/squareup/mosaic/lists/ModelsList$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ". Instead it is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/mosaic/lists/ModelsList;->subViews:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 201
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 202
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 203
    check-cast v2, Lcom/squareup/mosaic/core/ViewRef;

    .line 177
    invoke-virtual {v2}, Lcom/squareup/mosaic/core/ViewRef;->saveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    const/4 v0, 0x0

    new-array v0, v0, [Landroid/os/Parcelable;

    .line 206
    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, [Landroid/os/Parcelable;

    .line 175
    new-instance v1, Lcom/squareup/mosaic/lists/ModelsList$State;

    invoke-direct {v1, v0}, Lcom/squareup/mosaic/lists/ModelsList$State;-><init>([Landroid/os/Parcelable;)V

    return-object v1

    .line 206
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setCurrentItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/squareup/mosaic/lists/ModelsList;->currentItems:Ljava/util/List;

    return-void
.end method

.method public final toViews(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TT;*>;>;"
        }
    .end annotation

    const-string v0, "$this$toViews"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    check-cast p1, Ljava/lang/Iterable;

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 198
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 199
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    .line 35
    iget-object v2, p0, Lcom/squareup/mosaic/lists/ModelsList;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 200
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
