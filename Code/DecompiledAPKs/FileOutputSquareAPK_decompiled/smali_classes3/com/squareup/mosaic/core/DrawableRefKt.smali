.class public final Lcom/squareup/mosaic/core/DrawableRefKt;
.super Ljava/lang/Object;
.source "DrawableRef.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a(\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00042\u0006\u0010\u0005\u001a\u00020\u0003H\u0001\u001a\\\u0010\u0006\u001a\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u0004\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u00022\u001a\u0010\n\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u000c\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u00010\u000bH\u0086\u0008\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "castAndBind",
        "",
        "M",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "model",
        "updateOrReplace",
        "context",
        "Landroid/content/Context;",
        "newModel",
        "replaceDrawableBlock",
        "Lkotlin/Function2;",
        "Landroid/graphics/drawable/Drawable;",
        "(Lcom/squareup/mosaic/core/DrawableRef;Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/core/DrawableRef;",
        "mosaic-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final castAndBind(Lcom/squareup/mosaic/core/DrawableRef;Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/mosaic/core/DrawableModel;",
            ">(",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "TM;*>;",
            "Lcom/squareup/mosaic/core/DrawableModel;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$castAndBind"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/core/DrawableRef;->bind(Lcom/squareup/mosaic/core/DrawableModel;)V

    return-void
.end method

.method public static final updateOrReplace(Lcom/squareup/mosaic/core/DrawableRef;Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/core/DrawableRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/mosaic/core/DrawableModel;",
            ">(",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;",
            "Landroid/content/Context;",
            "TM;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/graphics/drawable/Drawable;",
            "-",
            "Landroid/graphics/drawable/Drawable;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "TM;*>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "replaceDrawableBlock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 45
    invoke-virtual {p0, p2}, Lcom/squareup/mosaic/core/DrawableRef;->canAccept(Lcom/squareup/mosaic/core/DrawableModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-static {p0, p2}, Lcom/squareup/mosaic/core/DrawableRefKt;->castAndBind(Lcom/squareup/mosaic/core/DrawableRef;Lcom/squareup/mosaic/core/DrawableModel;)V

    return-object p0

    .line 50
    :cond_0
    invoke-static {p2, p1}, Lcom/squareup/mosaic/core/DrawableModelKt;->toDrawableRef(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;

    move-result-object p1

    if-eqz p0, :cond_1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-interface {p3, p0, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1
.end method
