.class final Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "Responsive.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/basic/ResponsiveViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/basic/ResponsiveModel;)Landroid/widget/FrameLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\u0008\n\u0002\u0008\t\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "<anonymous>",
        "",
        "view",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "left",
        "",
        "top",
        "right",
        "bottom",
        "<anonymous parameter 5>",
        "<anonymous parameter 6>",
        "<anonymous parameter 7>",
        "<anonymous parameter 8>",
        "onLayoutChange",
        "com/squareup/mosaic/basic/ResponsiveViewRef$createView$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_apply:Landroid/widget/FrameLayout;

.field final synthetic this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef;


# direct methods
.method constructor <init>(Landroid/widget/FrameLayout;Lcom/squareup/mosaic/basic/ResponsiveViewRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->$this_apply:Landroid/widget/FrameLayout;

    iput-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 10

    move-object v7, p0

    .line 51
    iget-object v0, v7, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef;

    invoke-static {v0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->access$getUpdatingContent$p(Lcom/squareup/mosaic/basic/ResponsiveViewRef;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v8, v7, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->$this_apply:Landroid/widget/FrameLayout;

    new-instance v9, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move v4, p2

    move v5, p5

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;-><init>(Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;Landroid/view/View;IIII)V

    check-cast v9, Ljava/lang/Runnable;

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
