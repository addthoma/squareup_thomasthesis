.class public final Lcom/squareup/mosaic/components/ThemeUiModel;
.super Lcom/squareup/mosaic/core/UiModel;
.source "ThemeUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/UiModel<",
        "TP;>;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nThemeUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ThemeUiModel.kt\ncom/squareup/mosaic/components/ThemeUiModel\n*L\n1#1,49:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000e\n\u0002\u0010\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u0004B)\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u0012\u0010\u0008\u0002\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003H\u0016J\u0008\u0010\u0018\u001a\u00020\u0016H\u0001J\u000e\u0010\u0019\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0011J\t\u0010\u001a\u001a\u00020\u0007H\u00c6\u0003J\u0016\u0010\u001b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001cJ:\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00072\u0010\u0008\u0002\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001eJ\r\u0010\u001f\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0011J\u0018\u0010 \u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030!2\u0006\u0010\"\u001a\u00020#H\u0016J\u0013\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\'\u001a\u00020\u0007H\u00d6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001R,\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00038\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0012\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/ThemeUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "params",
        "themeId",
        "",
        "innerModel",
        "(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)V",
        "innerModel$annotations",
        "()V",
        "getInnerModel",
        "()Lcom/squareup/mosaic/core/UiModel;",
        "setInnerModel",
        "(Lcom/squareup/mosaic/core/UiModel;)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getThemeId",
        "()I",
        "add",
        "",
        "model",
        "checkInitialized",
        "component1",
        "component2",
        "component3",
        "component3$public_release",
        "copy",
        "(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/ThemeUiModel;",
        "createParams",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private innerModel:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private final themeId:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/mosaic/core/UiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->params:Ljava/lang/Object;

    iput p2, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    iput-object p3, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 30
    check-cast p3, Lcom/squareup/mosaic/core/UiModel;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/ThemeUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/ThemeUiModel;Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;ILjava/lang/Object;)Lcom/squareup/mosaic/components/ThemeUiModel;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/ThemeUiModel;->copy(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/ThemeUiModel;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic innerModel$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 40
    iput-object p1, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected theme model to only have one inner model, but got "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final checkInitialized()V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected NightModel to get an inner model."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    return v0
.end method

.method public final component3$public_release()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/ThemeUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)",
            "Lcom/squareup/mosaic/components/ThemeUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/ThemeUiModel;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/mosaic/components/ThemeUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;)V

    return-object v0
.end method

.method public createParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/mosaic/components/ThemeViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/ThemeViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/ThemeUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/ThemeUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    iget v1, p1, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    iget-object p1, p1, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInnerModel()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getThemeId()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final setInnerModel(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ThemeUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ThemeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", themeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->themeId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", innerModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/ThemeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
