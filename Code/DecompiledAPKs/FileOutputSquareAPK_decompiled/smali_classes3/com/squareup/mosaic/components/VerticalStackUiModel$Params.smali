.class public final Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;
.super Ljava/lang/Object;
.source "VerticalStackUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/IdParams;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/components/VerticalStackUiModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Params"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0008\u0001\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u0002\u001a\u00020\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "itemId",
        "",
        "spacing",
        "Lcom/squareup/resources/DimenModel;",
        "height",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;",
        "(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)V",
        "getHeight",
        "()Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;",
        "getItemId",
        "()I",
        "setItemId",
        "(I)V",
        "getSpacing",
        "()Lcom/squareup/resources/DimenModel;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

.field private itemId:I

.field private final spacing:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)V
    .locals 1

    const-string v0, "spacing"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "height"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->itemId:I

    iput-object p2, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    iput-object p3, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 48
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;-><init>(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;ILjava/lang/Object;)Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->copy(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-object v0
.end method

.method public final copy(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;
    .locals 1

    const-string v0, "spacing"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "height"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;-><init>(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    iget-object p1, p1, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHeight()Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->itemId:I

    return v0
.end method

.method public final getSpacing()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result v0

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public setItemId(I)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->itemId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Params(itemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", spacing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->spacing:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;->height:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
