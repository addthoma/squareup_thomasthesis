.class public final Lcom/squareup/mosaic/components/NohoActionBarViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "NohoActionBarViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/NohoActionBarUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoActionBar;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/NohoActionBarViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/NohoActionBarUiModel;",
        "Lcom/squareup/noho/NohoActionBar;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 7
    check-cast p2, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NohoActionBarViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/NohoActionBarUiModel;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/NohoActionBarUiModel;)Lcom/squareup/noho/NohoActionBar;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/NohoActionBarUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoActionBar;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance p2, Lcom/squareup/noho/NohoActionBar;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/NohoActionBarUiModel;Lcom/squareup/mosaic/components/NohoActionBarUiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/NohoActionBarUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/NohoActionBarUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 23
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getConfigBuilder$public_release()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NohoActionBarViewRef;->doBind(Lcom/squareup/mosaic/components/NohoActionBarUiModel;Lcom/squareup/mosaic/components/NohoActionBarUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NohoActionBarViewRef;->doBind(Lcom/squareup/mosaic/components/NohoActionBarUiModel;Lcom/squareup/mosaic/components/NohoActionBarUiModel;)V

    return-void
.end method
