.class public final Lcom/squareup/mosaic/components/IconButtonViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "IconButtonViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/IconButtonUiModel<",
        "*>;",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIconButtonViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IconButtonViewRef.kt\ncom/squareup/mosaic/components/IconButtonViewRef\n+ 2 DrawableRef.kt\ncom/squareup/mosaic/core/DrawableRefKt\n+ 3 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n1#1,44:1\n45#2,8:45\n15#3,2:53\n*E\n*S KotlinDebug\n*F\n+ 1 IconButtonViewRef.kt\ncom/squareup/mosaic/components/IconButtonViewRef\n*L\n31#1,8:45\n38#1,2:53\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\t\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u000b\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u000c\u001a\u00020\r2\u000c\u0010\u000e\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000f\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016R\u0018\u0010\u0007\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/IconButtonViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/IconButtonUiModel;",
        "Landroid/widget/ImageView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "drawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "createView",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private drawableRef:Lcom/squareup/mosaic/core/DrawableRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 10
    check-cast p2, Lcom/squareup/mosaic/components/IconButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/IconButtonViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/IconButtonUiModel;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/IconButtonUiModel;)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/IconButtonUiModel<",
            "*>;)",
            "Landroidx/appcompat/widget/AppCompatImageView;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance p2, Landroidx/appcompat/widget/AppCompatImageView;

    .line 22
    sget v0, Lcom/squareup/noho/R$attr;->nohoIconButtonStyle:I

    const/4 v1, 0x0

    .line 19
    invoke-direct {p2, p1, v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/IconButtonUiModel;Lcom/squareup/mosaic/components/IconButtonUiModel;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/IconButtonUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/IconButtonUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/mosaic/components/IconButtonViewRef;->drawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "androidView.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/IconButtonUiModel;->getDrawable()Lcom/squareup/mosaic/core/DrawableModel;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v0, v3}, Lcom/squareup/mosaic/core/DrawableRef;->canAccept(Lcom/squareup/mosaic/core/DrawableModel;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    invoke-static {v0, v3}, Lcom/squareup/mosaic/core/DrawableRefKt;->castAndBind(Lcom/squareup/mosaic/core/DrawableRef;Lcom/squareup/mosaic/core/DrawableModel;)V

    goto :goto_0

    .line 50
    :cond_1
    invoke-static {v3, v1}, Lcom/squareup/mosaic/core/DrawableModelKt;->toDrawableRef(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;

    move-result-object v1

    if-eqz v0, :cond_2

    .line 51
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    :cond_2
    invoke-virtual {v1}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 52
    :goto_0
    iput-object v0, p0, Lcom/squareup/mosaic/components/IconButtonViewRef;->drawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    .line 37
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/IconButtonUiModel;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 38
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/IconButtonUiModel;->getTooltip()Lcom/squareup/resources/TextModel;

    move-result-object v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    :goto_1
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/IconButtonUiModel;->getTooltip()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 53
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 39
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    :cond_4
    move-object v2, v0

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 41
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/IconButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/IconButtonUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    :cond_6
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/IconButtonUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-static {v1, v0, p1}, Lcom/squareup/mosaic/components/ViewRefUtilsKt;->setOnClickDebouncedIfChanged(Landroid/view/View;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/mosaic/components/IconButtonUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/IconButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/IconButtonViewRef;->doBind(Lcom/squareup/mosaic/components/IconButtonUiModel;Lcom/squareup/mosaic/components/IconButtonUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/mosaic/components/IconButtonUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/IconButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/IconButtonViewRef;->doBind(Lcom/squareup/mosaic/components/IconButtonUiModel;Lcom/squareup/mosaic/components/IconButtonUiModel;)V

    return-void
.end method
