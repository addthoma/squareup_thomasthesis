.class public final Lcom/squareup/mosaic/components/HairlineViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "HairlineViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/HairlineUiModel<",
        "*>;",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHairlineViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HairlineViewRef.kt\ncom/squareup/mosaic/components/HairlineViewRef\n*L\n1#1,23:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/HairlineViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/HairlineUiModel;",
        "Landroid/widget/ImageView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "createView",
        "model",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 13
    check-cast p2, Lcom/squareup/mosaic/components/HairlineUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/HairlineViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/HairlineUiModel;)Landroid/widget/ImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/HairlineUiModel;)Landroid/widget/ImageView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/HairlineUiModel<",
            "*>;)",
            "Landroid/widget/ImageView;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance p2, Landroid/widget/ImageView;

    invoke-direct {p2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 19
    sget p1, Lcom/squareup/noho/R$drawable;->noho_divider_both_hairline:I

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 20
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-object p2
.end method
