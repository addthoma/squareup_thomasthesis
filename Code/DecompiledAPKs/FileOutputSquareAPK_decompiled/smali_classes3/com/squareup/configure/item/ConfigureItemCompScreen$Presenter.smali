.class public Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;
.super Lcom/squareup/configure/item/VoidCompPresenter;
.source "ConfigureItemCompScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemCompScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/configure/item/VoidCompPresenter<",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

.field private final scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

.field private screen:Lcom/squareup/configure/item/ConfigureItemCompScreen;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/util/Res;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0, p3, p5}, Lcom/squareup/configure/item/VoidCompPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    .line 59
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 60
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method protected getCancelButtonTextResourceId()I
    .locals 1

    .line 88
    sget v0, Lcom/squareup/configure/item/R$string;->comp_item:I

    return v0
.end method

.method protected getPrimaryButtonTextResourceId()I
    .locals 1

    .line 92
    sget v0, Lcom/squareup/configure/item/R$string;->comp_initial:I

    return v0
.end method

.method protected getReasonHeaderTextResourceId()I
    .locals 1

    .line 96
    sget v0, Lcom/squareup/configure/item/R$string;->comp_uppercase_reason:I

    return v0
.end method

.method public synthetic lambda$null$0$ConfigureItemCompScreen$Presenter(Lcom/squareup/util/Optional;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    .line 76
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->getCheckedReason()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/configure/item/ConfigureItemState;->compItem(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_COMPED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {p1}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    return-void
.end method

.method public synthetic lambda$onPrimaryClicked$1$ConfigureItemCompScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->screen:Lcom/squareup/configure/item/ConfigureItemCompScreen;

    .line 71
    invoke-static {v1}, Lcom/squareup/configure/item/ConfigureItemCompScreen;->access$000(Lcom/squareup/configure/item/ConfigureItemCompScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/configure/item/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 73
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$Presenter$z3Bcs-VZiBmGUDLgMgxZF3QftN4;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$Presenter$z3Bcs-VZiBmGUDLgMgxZF3QftN4;-><init>(Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;)V

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCancelClicked()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goBack()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 65
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemCompScreen;

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->screen:Lcom/squareup/configure/item/ConfigureItemCompScreen;

    return-void
.end method

.method public onPrimaryClicked()V
    .locals 2

    .line 69
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$Presenter$wia7jNEOtq8OSuLqDawvPtGoYW0;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$Presenter$wia7jNEOtq8OSuLqDawvPtGoYW0;-><init>(Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
