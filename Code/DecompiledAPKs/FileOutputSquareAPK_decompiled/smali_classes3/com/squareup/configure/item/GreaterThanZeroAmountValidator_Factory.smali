.class public final Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory;
.super Ljava/lang/Object;
.source "GreaterThanZeroAmountValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory$InstanceHolder;->access$000()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;

    invoke-direct {v0}, Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory;->newInstance()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/configure/item/GreaterThanZeroAmountValidator_Factory;->get()Lcom/squareup/configure/item/GreaterThanZeroAmountValidator;

    move-result-object v0

    return-object v0
.end method
