.class public final Lcom/squareup/configure/item/ConfigureItemDiscountUtils;
.super Ljava/lang/Object;
.source "ConfigureItemDiscountUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfigureItemDiscountUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConfigureItemDiscountUtils.kt\ncom/squareup/configure/item/ConfigureItemDiscountUtils\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,76:1\n501#2:77\n486#2,6:78\n501#2:94\n486#2,6:95\n1360#3:84\n1429#3,3:85\n1099#3,2:88\n1127#3,4:90\n*E\n*S KotlinDebug\n*F\n+ 1 ConfigureItemDiscountUtils.kt\ncom/squareup/configure/item/ConfigureItemDiscountUtils\n*L\n32#1:77\n32#1,6:78\n46#1:94\n46#1,6:95\n37#1:84\n37#1,3:85\n38#1,2:88\n38#1,4:90\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a>\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0008\u001aX\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0006\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "getItemScopedDiscount",
        "Lcom/squareup/checkout/Discount;",
        "availablePerItemDiscountsInOrder",
        "",
        "",
        "appliedDiscounts",
        "discountId",
        "applied",
        "",
        "toggleableDiscountsForItem",
        "discountsAppliedAtCartScopeById",
        "availableFixedPercentageDiscounts",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "discountsAppliedToItemById",
        "isCompAllowed",
        "isDiscountApplicationIdEnabled",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getItemScopedDiscount(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Z)Lcom/squareup/checkout/Discount;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/checkout/Discount;"
        }
    .end annotation

    const-string v0, "availablePerItemDiscountsInOrder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appliedDiscounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_1

    .line 67
    invoke-interface {p0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Discount;

    .line 69
    new-instance p1, Lcom/squareup/checkout/Discount$Builder;

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p1, p0}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/checkout/Discount;)V

    .line 70
    sget-object p0, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    invoke-virtual {p1, p0}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    move-result-object p0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p0

    return-object p0

    .line 74
    :cond_1
    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Discount;

    return-object p0
.end method

.method public static final toggleableDiscountsForItem(Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZZ)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;ZZ)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    const-string v0, "discountsAppliedAtCartScopeById"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableFixedPercentageDiscounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountsAppliedToItemById"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 77
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 78
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 79
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 32
    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    .line 80
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 33
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 36
    check-cast p1, Ljava/lang/Iterable;

    .line 84
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 85
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 86
    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 37
    sget-object v5, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    invoke-static {v2, v5, p4}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 87
    :cond_3
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 88
    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result p1

    const/16 p4, 0x10

    invoke-static {p1, p4}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p1

    .line 89
    new-instance p4, Ljava/util/LinkedHashMap;

    invoke-direct {p4, p1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast p4, Ljava/util/Map;

    .line 90
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 91
    move-object v1, p1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 38
    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {p4, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 36
    :cond_4
    invoke-interface {v0, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 94
    new-instance p0, Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p0, Ljava/util/Map;

    .line 95
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/checkout/Discount;

    .line 46
    invoke-virtual {p4}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p4}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result p4

    if-nez p4, :cond_6

    const/4 p4, 0x1

    goto :goto_5

    :cond_6
    const/4 p4, 0x0

    :goto_5
    if-eqz p4, :cond_5

    .line 97
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p4

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p0, p4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 47
    :cond_7
    invoke-interface {v0, p0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    if-eqz p3, :cond_8

    .line 51
    invoke-static {v0}, Lcom/squareup/checkout/Discounts;->removeCompDiscounts(Ljava/util/Map;)V

    .line 54
    :cond_8
    invoke-static {v0}, Lcom/squareup/checkout/Adjustment;->sortToDisplayOrder(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    const-string p1, "sortToDisplayOrder(discounts)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
