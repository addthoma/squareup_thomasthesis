.class public final Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ConfigureItemPriceScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final hostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final vibratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->hostProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;"
        }
    .end annotation

    .line 65
    new-instance v9, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Landroid/os/Vibrator;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;)Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;
    .locals 10

    .line 71
    new-instance v9, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;-><init>(Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Landroid/os/Vibrator;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;
    .locals 9

    .line 56
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/os/Vibrator;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->hostProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-static/range {v1 .. v8}, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->newInstance(Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Landroid/os/Vibrator;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;)Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen_Presenter_Factory;->get()Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
