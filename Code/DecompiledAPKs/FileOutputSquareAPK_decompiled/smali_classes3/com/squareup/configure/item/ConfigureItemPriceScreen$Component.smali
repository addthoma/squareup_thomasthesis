.class public interface abstract Lcom/squareup/configure/item/ConfigureItemPriceScreen$Component;
.super Ljava/lang/Object;
.source "ConfigureItemPriceScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinActionBarModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemPriceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/configure/item/ConfigureItemPriceView;)V
.end method
