.class public final Lcom/squareup/configure/item/ScaleDataHelper;
.super Ljava/lang/Object;
.source "ScaleDataHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScaleDataHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScaleDataHelper.kt\ncom/squareup/configure/item/ScaleDataHelper\n*L\n1#1,131:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003\u001a\u0014\u0010\u0004\u001a\u00020\u0005*\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u001a\n\u0010\t\u001a\u00020\u0002*\u00020\n\u001a\n\u0010\u000b\u001a\u00020\u000c*\u00020\r\u001a\u000c\u0010\u000e\u001a\u00020\u000f*\u00020\u0010H\u0002\u00a8\u0006\u0011"
    }
    d2 = {
        "firstConnectedScaleEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/configure/item/ScaleDisplayEvent;",
        "Lcom/squareup/scales/ScaleTracker;",
        "isCompatibleWith",
        "",
        "Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;",
        "quantityUnit",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "toDisplayEvent",
        "Lcom/squareup/scales/ScaleEvent;",
        "toMeasurementUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "weightAsBigDecimal",
        "Ljava/math/BigDecimal;",
        "Lcom/squareup/scales/ScaleEvent$StableReadingEvent;",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final firstConnectedScaleEvents(Lcom/squareup/scales/ScaleTracker;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScaleTracker;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/configure/item/ScaleDisplayEvent;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$firstConnectedScaleEvents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-interface {p0}, Lcom/squareup/scales/ScaleTracker;->getConnectedScales()Lio/reactivex/Observable;

    move-result-object p0

    .line 69
    sget-object v0, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;->INSTANCE:Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "connectedScales\n      .s\u2026yEvent)\n        }\n      }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final isCompatibleWith(Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;Lcom/squareup/orders/model/Order$QuantityUnit;)Z
    .locals 1

    const-string v0, "$this$isCompatibleWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 130
    iget-object p1, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {p0}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final toDisplayEvent(Lcom/squareup/scales/ScaleEvent;)Lcom/squareup/configure/item/ScaleDisplayEvent;
    .locals 4

    const-string v0, "$this$toDisplayEvent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    instance-of v0, p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    .line 88
    check-cast p0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    invoke-static {p0}, Lcom/squareup/configure/item/ScaleDataHelper;->weightAsBigDecimal(Lcom/squareup/scales/ScaleEvent$StableReadingEvent;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->getNumberOfDecimalPlaces()I

    move-result v2

    .line 90
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->getUnitOfMeasurement()Lcom/squareup/scales/UnitOfMeasurement;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/configure/item/ScaleDataHelper;->toMeasurementUnit(Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v3

    .line 91
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p0

    .line 87
    invoke-direct {v0, p0, v3, v2, v1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)V

    check-cast v0, Lcom/squareup/configure/item/ScaleDisplayEvent;

    goto :goto_0

    .line 93
    :cond_0
    instance-of v0, p0, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayEvent$UnstableReading;

    check-cast p0, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;

    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ScaleDisplayEvent$UnstableReading;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v0, Lcom/squareup/configure/item/ScaleDisplayEvent;

    goto :goto_0

    .line 94
    :cond_1
    instance-of v0, p0, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;->getStatus()Lcom/squareup/scales/Status;

    move-result-object v0

    sget-object v1, Lcom/squareup/configure/item/ScaleDataHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/scales/Status;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 96
    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;

    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v0, Lcom/squareup/configure/item/ScaleDisplayEvent;

    goto :goto_0

    .line 95
    :cond_2
    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayEvent$ReadingError;

    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ScaleDisplayEvent$ReadingError;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v0, Lcom/squareup/configure/item/ScaleDisplayEvent;

    :goto_0
    return-object v0

    .line 94
    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final toMeasurementUnit(Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    const-string v0, "$this$toMeasurementUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    sget-object v0, Lcom/squareup/configure/item/ScaleDataHelper$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/scales/UnitOfMeasurement;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    .line 111
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 110
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    goto :goto_0

    .line 109
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    goto :goto_0

    .line 108
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    goto :goto_0

    .line 107
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-static {p0}, Lcom/squareup/quantity/ItemQuantities;->toUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final weightAsBigDecimal(Lcom/squareup/scales/ScaleEvent$StableReadingEvent;)Ljava/math/BigDecimal;
    .locals 2

    .line 121
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;->getWeight()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object p0

    const-string v0, "BigDecimal.valueOf(weight)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
