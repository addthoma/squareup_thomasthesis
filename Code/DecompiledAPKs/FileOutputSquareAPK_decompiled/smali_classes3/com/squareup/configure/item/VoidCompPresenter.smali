.class public abstract Lcom/squareup/configure/item/VoidCompPresenter;
.super Lmortar/ViewPresenter;
.source "VoidCompPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/configure/item/VoidCompView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SELECTED_REASON_INDEX_KEY:Ljava/lang/String; = "selected_reason_index"


# instance fields
.field private final cache:Lcom/squareup/tickets/voidcomp/CogsCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tickets/voidcomp/CogsCache<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final cancelButtonText:Ljava/lang/CharSequence;

.field private loaded:Z

.field private final primaryButtonText:Ljava/lang/CharSequence;

.field private final reasonHeaderText:Ljava/lang/CharSequence;

.field protected final res:Lcom/squareup/util/Res;

.field private selectedReasonIndex:I


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/CogsCache<",
            "TT;>;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    .line 23
    iput-object p1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->res:Lcom/squareup/util/Res;

    .line 24
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getCancelButtonTextResourceId()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cancelButtonText:Ljava/lang/CharSequence;

    .line 25
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getPrimaryButtonTextResourceId()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->primaryButtonText:Ljava/lang/CharSequence;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getReasonHeaderTextResourceId()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->reasonHeaderText:Ljava/lang/CharSequence;

    .line 27
    iput-object p2, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cache:Lcom/squareup/tickets/voidcomp/CogsCache;

    return-void
.end method

.method private setSelectedReasonIndex(I)V
    .locals 1

    if-ltz p1, :cond_1

    .line 111
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cache:Lcom/squareup/tickets/voidcomp/CogsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/CogsCache;->getReasons()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 114
    :cond_0
    iput p1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    .line 112
    iput p1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    :goto_1
    return-void
.end method

.method private updateReasons()V
    .locals 3

    .line 76
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/VoidCompView;

    .line 77
    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cache:Lcom/squareup/tickets/voidcomp/CogsCache;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/CogsCache;->getReasons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->populateReasons(Ljava/util/List;)V

    .line 78
    iget v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setCheckedIndex(I)V

    :cond_0
    return-void
.end method

.method private updateView()V
    .locals 5

    .line 57
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/VoidCompView;

    .line 60
    iget v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setPrimaryButtonEnabled(Z)V

    .line 61
    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->primaryButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cancelButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setActionBarCancelButtonText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->isCompingTicket()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->isVoidingTicket()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 65
    :cond_2
    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/configure/item/VoidCompView;->setHelpTextVisibleOrGone(Z)V

    if-eqz v2, :cond_3

    .line 67
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getHelpText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setHelpText(Ljava/lang/CharSequence;)V

    .line 71
    :cond_3
    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->reasonHeaderText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/VoidCompView;->setReasonHeader(Ljava/lang/CharSequence;)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->updateReasons()V

    return-void
.end method


# virtual methods
.method protected abstract getCancelButtonTextResourceId()I
.end method

.method public getCheckedReason()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 52
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/VoidCompView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/VoidCompView;->getCheckedReasonIndex()I

    move-result v0

    .line 53
    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cache:Lcom/squareup/tickets/voidcomp/CogsCache;

    invoke-virtual {v1, v0}, Lcom/squareup/tickets/voidcomp/CogsCache;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getHelpText()Ljava/lang/String;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->void_ticket_help_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getPrimaryButtonTextResourceId()I
.end method

.method protected abstract getReasonHeaderTextResourceId()I
.end method

.method public isCompingTicket()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isVoidingTicket()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$onLoad$0$VoidCompPresenter(Landroid/os/Bundle;)V
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "selected_reason_index"

    .line 37
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->setSelectedReasonIndex(I)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->updateReasons()V

    :cond_0
    return-void
.end method

.method public abstract onCancelClicked()V
.end method

.method public onCheckedIndexChanged(I)V
    .locals 2

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->setSelectedReasonIndex(I)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/VoidCompView;

    iget v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/VoidCompView;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 31
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 33
    iget-boolean v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->loaded:Z

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->cache:Lcom/squareup/tickets/voidcomp/CogsCache;

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$VoidCompPresenter$cyTx_u8_ZXLZNSyKGmi270foNFc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/configure/item/-$$Lambda$VoidCompPresenter$cyTx_u8_ZXLZNSyKGmi270foNFc;-><init>(Lcom/squareup/configure/item/VoidCompPresenter;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/squareup/tickets/voidcomp/CogsCache;->loadAndPost(Ljava/lang/Runnable;)V

    .line 42
    :cond_0
    invoke-direct {p0}, Lcom/squareup/configure/item/VoidCompPresenter;->updateView()V

    const/4 p1, 0x1

    .line 43
    iput-boolean p1, p0, Lcom/squareup/configure/item/VoidCompPresenter;->loaded:Z

    return-void
.end method

.method public abstract onPrimaryClicked()V
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 47
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 48
    iget v0, p0, Lcom/squareup/configure/item/VoidCompPresenter;->selectedReasonIndex:I

    const-string v1, "selected_reason_index"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
