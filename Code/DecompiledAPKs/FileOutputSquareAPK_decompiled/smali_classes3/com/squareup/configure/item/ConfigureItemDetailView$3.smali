.class Lcom/squareup/configure/item/ConfigureItemDetailView$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ConfigureItemDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailView;->buildVariablePriceEditText(Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$3;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$3;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    .line 243
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$3;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/Money;->newBuilder()Lcom/squareup/protos/common/Money$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$3;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onEditingPriceEditText(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
