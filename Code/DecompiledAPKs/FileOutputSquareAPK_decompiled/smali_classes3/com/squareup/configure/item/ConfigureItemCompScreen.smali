.class public final Lcom/squareup/configure/item/ConfigureItemCompScreen;
.super Lcom/squareup/configure/item/InConfigureItemScope;
.source "ConfigureItemCompScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/configure/item/ConfigureItemCompScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemCompScreen$Component;,
        Lcom/squareup/configure/item/ConfigureItemCompScreen$Module;,
        Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/configure/item/ConfigureItemCompScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authorizedEmployeeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$6RTKDokidrDXd4GeuOCCxxDKj58;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$ConfigureItemCompScreen$6RTKDokidrDXd4GeuOCCxxDKj58;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemCompScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    .line 36
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/configure/item/ConfigureItemCompScreen;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/configure/item/ConfigureItemCompScreen;
    .locals 2

    .line 117
    const-class v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 118
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 119
    new-instance v1, Lcom/squareup/configure/item/ConfigureItemCompScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/configure/item/ConfigureItemCompScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 111
    invoke-super {p0, p1, p2}, Lcom/squareup/configure/item/InConfigureItemScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 112
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 113
    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_COMP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 123
    sget v0, Lcom/squareup/configure/item/R$layout;->void_comp_view:I

    return v0
.end method
