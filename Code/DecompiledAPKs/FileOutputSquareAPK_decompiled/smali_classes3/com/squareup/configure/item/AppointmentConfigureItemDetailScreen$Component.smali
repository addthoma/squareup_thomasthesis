.class public interface abstract Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen$Component;
.super Ljava/lang/Object;
.source "AppointmentConfigureItemDetailScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;
.implements Lcom/squareup/configure/item/ConfigureItemDetailView$Component;


# annotations
.annotation runtime Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinActionBarModule;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$GreaterThanZeroAmountValidationModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
