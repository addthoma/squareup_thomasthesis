.class public final Lcom/squareup/configure/item/ScaleDisplayErrorEvent;
.super Lcom/squareup/configure/item/ScaleLogEvent;
.source "ScaleLogEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/configure/item/ScaleDisplayErrorEvent;",
        "Lcom/squareup/configure/item/ScaleLogEvent;",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 3

    .line 69
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SCALE_ERROR:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string v2, "SCALE_ERROR.value"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/squareup/configure/item/ScaleLogEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
