.class public final Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;
.super Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;
.source "FavoritesTileItemSelectionEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CatalogPlaceholderSelected"
.end annotation


# instance fields
.field private final placeholderName:Ljava/lang/String;

.field private final placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;


# direct methods
.method public constructor <init>(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 65
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 77
    instance-of v0, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 81
    :cond_0
    check-cast p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;

    .line 82
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iget-object v2, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderName:Ljava/lang/String;

    .line 83
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getPlaceholderName()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceholderType()Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 87
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderType:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->placeholderName:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
