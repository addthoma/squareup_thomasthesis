.class public final Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;
.super Ljava/lang/Object;
.source "PaymentPadPhoneView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/PaymentPadPhoneView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/PaymentPadPhonePresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/PaymentPadPhonePresenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/PaymentPadPhonePresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/PaymentPadPhoneView;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/PaymentPadPhoneView;Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->presenter:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/PaymentPadPhoneView;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/PaymentPadPhoneView;Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhoneView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;->injectMembers(Lcom/squareup/orderentry/PaymentPadPhoneView;)V

    return-void
.end method
