.class public Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;
.super Ljava/lang/Object;
.source "CheckoutLibraryListConfiguration.java"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListConfiguration;


# instance fields
.field private final itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/ItemSuggestionsManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;->itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

    return-void
.end method


# virtual methods
.method public getCanAlwaysShowCategoryPlaceholders()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCanLongClickOnItem()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getCanShowEditItemsButton()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getTopLevelItemSuggestions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;->itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

    invoke-virtual {v0}, Lcom/squareup/librarylist/ItemSuggestionsManager;->getTopLevelItemSuggestions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelPlaceholders()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 31
    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
