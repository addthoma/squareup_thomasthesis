.class Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;
.super Ljava/lang/Object;
.source "OrderEntryPages.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/pages/OrderEntryPages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CatalogPageComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/shared/catalog/models/CatalogPage;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/orderentry/pages/OrderEntryPages$1;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/shared/catalog/models/CatalogPage;Lcom/squareup/shared/catalog/models/CatalogPage;)I
    .locals 0

    .line 77
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogPage;->getPageIndex()I

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogPage;->getPageIndex()I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 75
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogPage;

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogPage;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;->compare(Lcom/squareup/shared/catalog/models/CatalogPage;Lcom/squareup/shared/catalog/models/CatalogPage;)I

    move-result p1

    return p1
.end method
