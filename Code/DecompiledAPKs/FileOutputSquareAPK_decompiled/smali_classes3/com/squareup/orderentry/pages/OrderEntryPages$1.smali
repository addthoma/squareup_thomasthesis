.class Lcom/squareup/orderentry/pages/OrderEntryPages$1;
.super Ljava/lang/Object;
.source "OrderEntryPages.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/pages/OrderEntryPages;->loadAndPost(Ljava/lang/Runnable;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/pages/OrderEntryPages;Ljava/lang/Runnable;)V
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->this$0:Lcom/squareup/orderentry/pages/OrderEntryPages;

    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;)V"
        }
    .end annotation

    .line 313
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    .line 314
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->this$0:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-static {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->access$100(Lcom/squareup/orderentry/pages/OrderEntryPages;)Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->this$0:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-static {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->access$200(Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    goto :goto_0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->this$0:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-static {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->access$300(Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    .line 319
    :goto_0
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->val$callback:Ljava/lang/Runnable;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$1;->val$callback:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
