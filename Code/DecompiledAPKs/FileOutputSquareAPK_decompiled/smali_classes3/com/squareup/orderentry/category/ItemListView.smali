.class public Lcom/squareup/orderentry/category/ItemListView;
.super Landroid/widget/LinearLayout;
.source "ItemListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;
    }
.end annotation


# static fields
.field private static final ALL_DISCOUNTS_TYPE:I = 0x2

.field private static final ALL_ITEMS_TYPE:I = 0x1

.field private static final BUTTONS_TYPE:I = 0x5

.field private static final COGS_ITEM_TYPE:I = 0x0

.field private static final GIFT_CARDS_TYPE:I = 0x3

.field private static final REWARDS_TYPE:I = 0x4


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field durationFormatter:Lcom/squareup/text/DurationFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final isTextTile:Z

.field private itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private itemCardEmptyNote:Landroid/view/View;

.field private itemCardEmptyTitle:Landroid/widget/TextView;

.field private itemList:Landroid/widget/ListView;

.field itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private openItemsButton:Landroid/widget/Button;

.field percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    const-class p2, Lcom/squareup/orderentry/category/ItemListScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/category/ItemListScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/category/ItemListScreen$Component;->inject(Lcom/squareup/orderentry/category/ItemListView;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/orderentry/category/ItemListView;->isTextTile:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/category/ItemListView;)Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/category/ItemListView;)Z
    .locals 0

    .line 71
    iget-boolean p0, p0, Lcom/squareup/orderentry/category/ItemListView;->isTextTile:Z

    return p0
.end method

.method private bindViews()V
    .locals 1

    .line 417
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 418
    sget v0, Lcom/squareup/orderentry/R$id;->item_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemList:Landroid/widget/ListView;

    .line 419
    sget v0, Lcom/squareup/orderentry/R$id;->item_card_empty_note:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyNote:Landroid/view/View;

    .line 420
    sget v0, Lcom/squareup/orderentry/R$id;->item_card_empty_note_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 421
    sget v0, Lcom/squareup/orderentry/R$id;->item_card_empty_note_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    .line 422
    sget v0, Lcom/squareup/orderentry/R$id;->item_card_empty_open_items_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->openItemsButton:Landroid/widget/Button;

    return-void
.end method

.method static synthetic lambda$onAttachedToWindow$2()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    .line 148
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ItemListView()Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 146
    sget-object v0, Lcom/squareup/tutorialv2/RequestLayoutTimeout;->FAST:Lcom/squareup/tutorialv2/RequestLayoutTimeout;

    invoke-static {p0, v0}, Lcom/squareup/tutorialv2/TutorialUtilities;->requestLayouts(Landroid/view/View;Lcom/squareup/tutorialv2/RequestLayoutTimeout;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$1$ItemListView()Lkotlin/Unit;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onItemListScreenShown()V

    .line 146
    new-instance v0, Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$8L7zcJfAr-eOe6mewawbJCcJeNE;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$8L7zcJfAr-eOe6mewawbJCcJeNE;-><init>(Lcom/squareup/orderentry/category/ItemListView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 109
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 111
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListView;->bindViews()V

    .line 112
    new-instance v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;-><init>(Lcom/squareup/orderentry/category/ItemListView;)V

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    .line 113
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemList:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/orderentry/category/ItemListView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/ItemListView$1;-><init>(Lcom/squareup/orderentry/category/ItemListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->itemList:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$Dsdtfve7fLqPPHLz6-CplPQQh84;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$Dsdtfve7fLqPPHLz6-CplPQQh84;-><init>(Lcom/squareup/orderentry/category/ItemListView;)V

    sget-object v2, Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$mzMqT1Y_OY7j0DVYINNpC03NTJo;->INSTANCE:Lcom/squareup/orderentry/category/-$$Lambda$ItemListView$mzMqT1Y_OY7j0DVYINNpC03NTJo;

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/squareup/tutorialv2/TutorialUtilities;->setupTutorialScrollListener(Landroid/widget/AbsListView;ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->openItemsButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/orderentry/category/ItemListView$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/ItemListView$2;-><init>(Lcom/squareup/orderentry/category/ItemListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->dropView(Lcom/squareup/orderentry/category/ItemListView;)V

    .line 159
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setEmptyItemCardViewCategory(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V
    .locals 2

    .line 163
    sget-object v0, Lcom/squareup/orderentry/category/ItemListView$3;->$SwitchMap$com$squareup$librarylist$LibraryListState$Filter:[I

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 p2, 0x5

    if-ne v0, p2, :cond_0

    .line 188
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 189
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    sget p2, Lcom/squareup/orderentry/R$string;->item_list_no_items_title:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 192
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown library mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 180
    :cond_1
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 181
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    .line 182
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_category_note_title:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "category_name"

    .line 184
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 185
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 181
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 175
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    sget p2, Lcom/squareup/librarylist/R$string;->item_library_empty_gift_cards_note_title:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 177
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    goto :goto_0

    .line 170
    :cond_3
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 171
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    sget p2, Lcom/squareup/librarylist/R$string;->item_library_empty_discounts_note_title:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 165
    :cond_4
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 166
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyTitle:Landroid/widget/TextView;

    sget p2, Lcom/squareup/librarylist/R$string;->item_library_empty_note_title:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 167
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->openItemsButton:Landroid/widget/Button;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setVisibility(I)V

    .line 194
    :goto_0
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemList:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/squareup/orderentry/category/ItemListView;->itemCardEmptyNote:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    .line 199
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method

.method updateCursorWithPlaceholders(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Lcom/squareup/orderentry/PlaceholderCounts;)V
    .locals 2

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x5

    .line 205
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseRewards()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    .line 207
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    .line 210
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v1, 0x2

    .line 212
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    .line 213
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListView;->adapter:Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    iput-object p2, v1, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    .line 215
    iput-object v0, v1, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    .line 216
    invoke-virtual {v1, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method
