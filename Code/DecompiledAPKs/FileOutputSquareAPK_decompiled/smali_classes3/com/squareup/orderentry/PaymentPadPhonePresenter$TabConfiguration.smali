.class final Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;
.super Ljava/lang/Object;
.source "PaymentPadPhonePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/PaymentPadPhonePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TabConfiguration"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0082\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c2\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00032\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;",
        "",
        "isSearching",
        "",
        "layoutRequiresSlimTabs",
        "isOrderEntryV2",
        "(ZZZ)V",
        "tabFormat",
        "Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;",
        "getTabFormat",
        "()Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isOrderEntryV2:Z

.field private final isSearching:Z

.field private final layoutRequiresSlimTabs:Z


# direct methods
.method public constructor <init>(ZZZ)V
    .locals 0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    iput-boolean p2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    iput-boolean p3, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    return-void
.end method

.method private final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    return v0
.end method

.method private final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    return v0
.end method

.method private final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;ZZZILjava/lang/Object;)Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->copy(ZZZ)Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZZZ)Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;
    .locals 1

    new-instance v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;-><init>(ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    iget-boolean v1, p1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    iget-boolean v1, p1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    iget-boolean p1, p1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTabFormat()Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;
    .locals 1

    .line 119
    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 120
    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;->FULL:Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;

    goto :goto_2

    :cond_2
    :goto_1
    sget-object v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;->SLIM:Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;

    :goto_2
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TabConfiguration(isSearching="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isSearching:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", layoutRequiresSlimTabs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->layoutRequiresSlimTabs:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOrderEntryV2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->isOrderEntryV2:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
