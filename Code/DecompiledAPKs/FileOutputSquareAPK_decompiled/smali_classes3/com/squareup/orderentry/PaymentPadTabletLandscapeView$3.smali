.class Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PaymentPadTabletLandscapeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateToCartList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 144
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$002(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 145
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->editFrame:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 147
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->endAnimation()V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "End animate to Cart List"

    .line 149
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 136
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->editFrame:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->startAnimation()V

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "Start animate to Cart List"

    .line 140
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
