.class final Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;
.super Ljava/lang/Object;
.source "PaymentPadPhonePresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Triple<",
        "+",
        "Ljava/lang/Boolean;",
        "+",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012R\u0010\u0002\u001aN\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;->accept(Lkotlin/Triple;)V

    return-void
.end method

.method public final accept(Lkotlin/Triple;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 58
    iget-object v2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;

    iget-object v2, v2, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    .line 59
    new-instance v3, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;

    const-string v4, "isSearching"

    .line 60
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 61
    invoke-virtual {v1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLandscape()Z

    move-result v1

    const-string v4, "isOrderEntryV2"

    .line 62
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 59
    invoke-direct {v3, v0, v1, p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;-><init>(ZZZ)V

    .line 58
    invoke-static {v2, v3}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$updateTabsSize(Lcom/squareup/orderentry/PaymentPadPhonePresenter;Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;)V

    return-void
.end method
