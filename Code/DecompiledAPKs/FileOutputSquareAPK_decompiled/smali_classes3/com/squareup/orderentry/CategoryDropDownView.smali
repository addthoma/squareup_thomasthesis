.class public Lcom/squareup/orderentry/CategoryDropDownView;
.super Lcom/squareup/ui/DropDownContainer;
.source "CategoryDropDownView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

.field private final borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

.field catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance p1, Lcom/squareup/marin/widgets/BorderPainter;

    invoke-direct {p1, p0}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 40
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/ui/DropDownContainer;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownView;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected inject()V
    .locals 2

    .line 44
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;->inject(Lcom/squareup/orderentry/CategoryDropDownView;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$CategoryDropDownView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 54
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 56
    iget-object p2, p0, Lcom/squareup/orderentry/CategoryDropDownView;->presenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->categoryClicked(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 58
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    iget-object p1, p1, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 59
    iget-object p2, p0, Lcom/squareup/orderentry/CategoryDropDownView;->presenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V

    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->presenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropView(Ljava/lang/Object;)V

    .line 68
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 48
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onFinishInflate()V

    .line 49
    sget v0, Lcom/squareup/orderentry/R$id;->category_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->listView:Landroid/widget/ListView;

    .line 51
    new-instance v0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;-><init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    iput-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    .line 52
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownView;->adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownView$GETsEvsk8Zm97viOTdeHdSYsRIg;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownView$GETsEvsk8Zm97viOTdeHdSYsRIg;-><init>(Lcom/squareup/orderentry/CategoryDropDownView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->presenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method updateRows(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)V"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView;->adapter:Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V

    return-void
.end method
