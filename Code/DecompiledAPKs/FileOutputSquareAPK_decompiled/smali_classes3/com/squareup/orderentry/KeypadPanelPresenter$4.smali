.class synthetic Lcom/squareup/orderentry/KeypadPanelPresenter$4;
.super Ljava/lang/Object;
.source "KeypadPanelPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadPanelPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$flowlegacy$YesNo:[I

.field static final synthetic $SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 276
    invoke-static {}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->values()[Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I

    sget-object v2, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CARD:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {v2}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I

    sget-object v3, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->SALE:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {v3}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v2, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I

    sget-object v3, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {v3}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->ordinal()I

    move-result v3

    const/4 v4, 0x3

    aput v4, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 259
    :catch_2
    invoke-static {}, Lcom/squareup/flowlegacy/YesNo;->values()[Lcom/squareup/flowlegacy/YesNo;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$flowlegacy$YesNo:[I

    :try_start_3
    sget-object v2, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$flowlegacy$YesNo:[I

    sget-object v3, Lcom/squareup/flowlegacy/YesNo;->YES:Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {v3}, Lcom/squareup/flowlegacy/YesNo;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$flowlegacy$YesNo:[I

    sget-object v2, Lcom/squareup/flowlegacy/YesNo;->NO:Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {v2}, Lcom/squareup/flowlegacy/YesNo;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
