.class final Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2$1;
.super Ljava/lang/Object;
.source "PaymentPadPhonePresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "page",
        "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;

    iget-object v0, v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/PaymentPadPhoneView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/PaymentPadPhoneView;->setSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;

    iget-object v0, v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/PaymentPadPhoneView;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/PaymentPadPhoneView;->setTabsOffset(F)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2$1;->accept(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-void
.end method
