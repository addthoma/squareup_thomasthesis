.class Lcom/squareup/orderentry/NavigationBarView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "NavigationBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/NavigationBarView;->updateTabs(Lcom/squareup/orderentry/pages/OrderEntryPageList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/NavigationBarView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/NavigationBarView;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarView$1;->this$0:Lcom/squareup/orderentry/NavigationBarView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView$1;->this$0:Lcom/squareup/orderentry/NavigationBarView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/NavigationBarView;->getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->tabSelected(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-void
.end method
