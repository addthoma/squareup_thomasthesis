.class public Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;
.super Ljava/lang/Object;
.source "FavoritesTileItemSelectionEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;,
        Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$Cancelled;,
        Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;,
        Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;,
        Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;
    }
.end annotation


# instance fields
.field private final isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSelectionStartedRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionStartedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public createCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    invoke-direct {v1, p1, p2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 129
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public discardChanges()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$Cancelled;->INSTANCE:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$Cancelled;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public isSelectingItem()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onItemSelectionFinished()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;",
            ">;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onItemSelectionStarted()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionStartedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public selectCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    invoke-direct {v1, p1, p2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public selectPlaceholderItem(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionFinishedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;

    invoke-direct {v1, p1, p2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;-><init>(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public selectTile(Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->itemSelectionStartedRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
