.class public Lcom/squareup/orderentry/GridTileView;
.super Landroid/widget/LinearLayout;
.source "GridTileView.java"

# interfaces
.implements Lcom/squareup/orderentry/HasEnabledLook;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/GridTileView$Component;
    }
.end annotation


# instance fields
.field private final defaultTileColor:I

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private imageUrl:Ljava/lang/String;

.field private lastMeasuredGlyphHeight:I

.field private lastMeasuredGlyphWidth:I

.field private nameView:Landroid/widget/TextView;

.field picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final placeholderMaxHeight:I

.field private final selector:Landroid/graphics/drawable/Drawable;

.field private final shadowColor:I

.field private final shadowDx:F

.field private final shadowDy:F

.field private final shadowRadius:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, -0x1

    .line 42
    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphHeight:I

    .line 43
    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphWidth:I

    .line 48
    const-class p2, Lcom/squareup/orderentry/GridTileView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/GridTileView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/GridTileView$Component;->inject(Lcom/squareup/orderentry/GridTileView;)V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 50
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_dim_translucent_pressed:I

    .line 51
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orderentry/GridTileView;->selector:Landroid/graphics/drawable/Drawable;

    .line 52
    sget p2, Lcom/squareup/glyph/R$dimen;->glyph_shadow_radius:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->shadowRadius:I

    .line 53
    sget p2, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->shadowDx:F

    .line 54
    sget p2, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dy:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->shadowDy:F

    .line 55
    sget p2, Lcom/squareup/marin/R$color;->marin_text_shadow:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->shadowColor:I

    .line 56
    sget p2, Lcom/squareup/marin/R$dimen;->grid_placeholder_text_max_size:I

    .line 57
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->placeholderMaxHeight:I

    .line 58
    invoke-static {p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    return-void
.end method

.method private redraw(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .line 157
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 159
    instance-of v0, p1, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    if-eqz v0, :cond_0

    .line 160
    move-object v0, p1

    check-cast v0, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    .line 161
    iget-object v1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$GridTileView$NjL0syza4cOt6BniNMgcqsEVGMs;

    invoke-direct {v2, p0, v0}, Lcom/squareup/orderentry/-$$Lambda$GridTileView$NjL0syza4cOt6BniNMgcqsEVGMs;-><init>(Lcom/squareup/orderentry/GridTileView;Lcom/squareup/register/widgets/ItemPlaceholderDrawable;)V

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 171
    :cond_1
    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 172
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/orderentry/GridTileView;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/orderentry/GridTileView;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p1}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphWidth:I

    iget v1, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphHeight:I

    .line 176
    invoke-virtual {p1, v0, v1}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 178
    invoke-virtual {p1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    :goto_0
    return-void
.end method

.method private setPlaceholderDrawable(Ljava/lang/String;Lcom/squareup/register/widgets/ItemPlaceholderDrawable;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/orderentry/GridTileView;->imageUrl:Ljava/lang/String;

    .line 153
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/GridTileView;->redraw(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 201
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 202
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .line 189
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 190
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 191
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->invalidate()V

    return-void
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$redraw$0$GridTileView(Lcom/squareup/register/widgets/ItemPlaceholderDrawable;Landroid/view/View;II)V
    .locals 0

    .line 162
    invoke-virtual {p1, p3, p4}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->setSize(II)V

    int-to-float p2, p4

    const p3, 0x3f19999a    # 0.6f

    mul-float p2, p2, p3

    .line 163
    iget p3, p0, Lcom/squareup/orderentry/GridTileView;->placeholderMaxHeight:I

    int-to-float p3, p3

    invoke-static {p2, p3}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 164
    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->setMaxHeight(F)V

    return-void
.end method

.method public looksEnabled()Z
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 185
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 63
    sget v0, Lcom/squareup/widgets/pos/R$id;->item_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 64
    sget v0, Lcom/squareup/widgets/pos/R$id;->item_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 138
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 139
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredHeight()I

    move-result p1

    .line 140
    iget-object p2, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result p2

    .line 141
    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphHeight:I

    if-eq v0, p1, :cond_0

    .line 142
    iput p1, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphHeight:I

    .line 143
    iput p2, p0, Lcom/squareup/orderentry/GridTileView;->lastMeasuredGlyphWidth:I

    .line 144
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->imageUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 145
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/GridTileView;->redraw(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 195
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 196
    iget-object p3, p0, Lcom/squareup/orderentry/GridTileView;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getPaddingLeft()I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 197
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr p2, v1

    .line 196
    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method

.method public setCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-static {p3, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p2, p3, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {p0, p2, p1}, Lcom/squareup/orderentry/GridTileView;->setPlaceholderDrawable(Ljava/lang/String;Lcom/squareup/register/widgets/ItemPlaceholderDrawable;)V

    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {p0, p2, p1}, Lcom/squareup/orderentry/GridTileView;->setPlaceholderDrawable(Ljava/lang/String;Lcom/squareup/register/widgets/ItemPlaceholderDrawable;)V

    return-void
.end method

.method public setDiscount(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    .line 76
    new-instance p1, Lcom/squareup/marin/widgets/MarinTagDrawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/marin/widgets/MarinTagDrawable;-><init>(Landroid/content/Context;)V

    .line 77
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinTagDrawable;->setText(Ljava/lang/String;)V

    .line 78
    iget-object p2, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setGiftCard(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    new-instance p1, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;-><init>(Landroid/content/Context;)V

    .line 103
    invoke-virtual {p1, p3}, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;->setText(Ljava/lang/String;)V

    .line 104
    iget-object p3, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-static {p2, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p2

    invoke-virtual {p3, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    .line 105
    iget-object p2, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setGlyph(Ljava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 70
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget p2, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    return-void
.end method

.method public setItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-static {p3, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/orderentry/GridTileView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p2, p3, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p1

    .line 113
    invoke-direct {p0, p4, p1}, Lcom/squareup/orderentry/GridTileView;->setPlaceholderDrawable(Ljava/lang/String;Lcom/squareup/register/widgets/ItemPlaceholderDrawable;)V

    return-void
.end method

.method public setLooksEnabled(Z)V
    .locals 4

    .line 82
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 86
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/GridTileView;->shadowRadius:I

    iget v1, p0, Lcom/squareup/orderentry/GridTileView;->shadowDx:F

    iget v2, p0, Lcom/squareup/orderentry/GridTileView;->shadowDy:F

    iget v3, p0, Lcom/squareup/orderentry/GridTileView;->shadowColor:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphShadow(IFFI)V

    goto :goto_0

    .line 89
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphShadow(IFFI)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 92
    iget-object p1, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public setStackedBackground()V
    .locals 2

    .line 129
    sget v0, Lcom/squareup/widgets/pos/R$drawable;->stacked_panel_background:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/GridTileView;->setBackgroundResource(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/orderentry/GridTileView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v1, p0, Lcom/squareup/orderentry/GridTileView;->defaultTileColor:I

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    return-void
.end method
