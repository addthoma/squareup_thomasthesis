.class public Lcom/squareup/orderentry/LibraryPanelTabletPresenter;
.super Lmortar/ViewPresenter;
.source "LibraryPanelTabletPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/LibraryPanelTablet;",
        ">;"
    }
.end annotation


# static fields
.field private static final ANIMATE:Z = true

.field private static final IMMEDIATELY:Z


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final device:Lcom/squareup/util/Device;

.field private final libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

.field private final libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CheckoutLibraryListPresenter;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->device:Lcom/squareup/util/Device;

    .line 38
    iput-object p2, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 39
    iput-object p3, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    .line 40
    iput-object p4, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 41
    iput-object p5, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->res:Lcom/squareup/util/Res;

    .line 42
    iput-object p6, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method private setSearchState(Lcom/squareup/orderentry/LibraryPanelTablet;Ljava/lang/Boolean;Z)V
    .locals 0

    .line 80
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 81
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/LibraryPanelTablet;->showSearchExit(Z)V

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/LibraryPanelTablet;->hideSearchExit(Z)V

    .line 84
    invoke-virtual {p1}, Lcom/squareup/orderentry/LibraryPanelTablet;->clearSearchText()V

    .line 85
    invoke-virtual {p1}, Lcom/squareup/orderentry/LibraryPanelTablet;->hideKeyboard()V

    .line 86
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private updateViewMode(Lcom/squareup/orderentry/LibraryPanelTablet;Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 4

    .line 67
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 66
    :goto_1
    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/LibraryPanelTablet;->setLibraryBarVisible(Z)V

    .line 69
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v1, :cond_2

    .line 70
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/LibraryPanelTablet;->setLibraryBarTitle(Ljava/lang/String;)V

    goto :goto_2

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-nez v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->res:Lcom/squareup/util/Res;

    .line 73
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-virtual {p2, v1, v3}, Lcom/squareup/librarylist/LibraryListState$Filter;->getStringResId(Lcom/squareup/catalogapi/CatalogIntegrationController;Z)I

    move-result p2

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 72
    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/LibraryPanelTablet;->setLibraryBarTitle(Ljava/lang/String;)V

    .line 75
    :cond_3
    :goto_2
    iget-object p2, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->device:Lcom/squareup/util/Device;

    .line 76
    invoke-interface {p2}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {p2}, Lcom/squareup/librarylist/LibraryListStateManager;->isTopLevel()Z

    move-result p2

    if-nez p2, :cond_5

    :cond_4
    const/4 v2, 0x1

    .line 75
    :cond_5
    invoke-virtual {p1, v2}, Lcom/squareup/orderentry/LibraryPanelTablet;->setBackButtonVisible(Z)V

    return-void
.end method


# virtual methods
.method public backButtonClicked()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->goBack()Z

    return-void
.end method

.method public isSearching()Z
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$null$0$LibraryPanelTabletPresenter(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 51
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/LibraryPanelTablet;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->setSearchState(Lcom/squareup/orderentry/LibraryPanelTablet;Ljava/lang/Boolean;Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$2$LibraryPanelTabletPresenter(Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLibrarySearchActive(Z)V

    .line 59
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/LibraryPanelTablet;

    invoke-direct {p0, v0, p1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->updateViewMode(Lcom/squareup/orderentry/LibraryPanelTablet;Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$LibraryPanelTabletPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->librarySearchIsActive()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$wVLBaC3ky17PNQBLnsDN2H5BpWA;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$wVLBaC3ky17PNQBLnsDN2H5BpWA;-><init>(Lcom/squareup/orderentry/LibraryPanelTabletPresenter;)V

    .line 50
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$3$LibraryPanelTabletPresenter()Lrx/Subscription;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->holder()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$iOSPzZW-BPGNDJ7O8ZxrSlYcDHo;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$iOSPzZW-BPGNDJ7O8ZxrSlYcDHo;-><init>(Lcom/squareup/orderentry/LibraryPanelTabletPresenter;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/LibraryPanelTablet;

    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->isSearching()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->setSearchState(Lcom/squareup/orderentry/LibraryPanelTablet;Ljava/lang/Boolean;Z)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$8QO0ES3-p8N4VMclQLhiG6yloDE;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$8QO0ES3-p8N4VMclQLhiG6yloDE;-><init>(Lcom/squareup/orderentry/LibraryPanelTabletPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$Jw0RR8J6IZLp54NW_MXD1IpSris;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTabletPresenter$Jw0RR8J6IZLp54NW_MXD1IpSris;-><init>(Lcom/squareup/orderentry/LibraryPanelTabletPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onTextSearched(Ljava/lang/String;)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setHasLibrarySearchText(Z)V

    .line 100
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    return-void
.end method

.method public searchExitClicked()V
    .locals 0

    .line 95
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->stopSearching()V

    return-void
.end method

.method public startSearching()V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLibrarySearchActive(Z)V

    return-void
.end method

.method public stopSearching()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLibrarySearchActive(Z)V

    return-void
.end method
