.class public Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;
.super Lcom/squareup/orderentry/PaymentPadPresenter;
.source "PaymentPadTabletPortraitPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/orderentry/PaymentPadPresenter<",
        "Lcom/squareup/orderentry/PaymentPadTabletPortraitView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

.field private final cartRecyclerViewPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0, p1, p9}, Lcom/squareup/orderentry/PaymentPadPresenter;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/tutorialv2/TutorialCore;)V

    .line 42
    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 43
    iput-object p3, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 44
    iput-object p4, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 45
    iput-object p5, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 46
    iput-object p6, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartRecyclerViewPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 47
    iput-object p7, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 48
    iput-object p8, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Z
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->hasView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Ljava/lang/Object;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Z
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->hasView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Ljava/lang/Object;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private closeCartIfVisible()V
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->closeCart()V

    :cond_0
    return-void
.end method

.method private onCartChanged()V
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-direct {p0, v0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->updateOpenTicketsTotalAmount(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    :cond_0
    return-void
.end method

.method private onTicketRemoved()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->closeCartIfVisible()V

    return-void
.end method

.method private onTicketSave()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->closeCartIfVisible()V

    return-void
.end method

.method private updateOpenTicketsTotalAmount(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->setTotalAmount(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$PaymentPadTabletPortraitPresenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->onCartChanged()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$PaymentPadTabletPortraitPresenter(Lcom/squareup/payment/OrderEntryEvents$TicketSaved;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->onTicketSave()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$PaymentPadTabletPortraitPresenter(Lcom/squareup/payment/OrderEntryEvents$TicketRemoved;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->onTicketRemoved()V

    return-void
.end method

.method onCurrentSaleClicked()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartRecyclerViewPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->snapToLastRow()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->toggleCart()V

    .line 106
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_SHOW_CART:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :cond_1
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/orderentry/PaymentPadPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$t-79d3onr9SVlN0QM8ApqQPFPs4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$t-79d3onr9SVlN0QM8ApqQPFPs4;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$KUTXDQPs1D3X78nMWsqo5Rcmq9s;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$KUTXDQPs1D3X78nMWsqo5Rcmq9s;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$TicketRemoved;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$17bv5CQvDNt2abRik2hPWGyhPnc;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletPortraitPresenter$17bv5CQvDNt2abRik2hPWGyhPnc;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/orderentry/PaymentPadPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->initializeForOpenTickets()V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->updateOpenTicketsTotalAmount(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    .line 65
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    new-instance v0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    return-void
.end method
