.class public final Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;
.super Ljava/lang/Object;
.source "LibraryPanelTabletPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/LibraryPanelTabletPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->libraryListPresenterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->libraryListStateManagerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CheckoutLibraryListPresenter;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/LibraryPanelTabletPresenter;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CheckoutLibraryListPresenter;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/LibraryPanelTabletPresenter;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->libraryListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->libraryListStateManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v1 .. v6}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CheckoutLibraryListPresenter;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter_Factory;->get()Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    move-result-object v0

    return-object v0
.end method
