.class public final Lcom/squareup/orderentry/KeypadPanel_MembersInjector;
.super Ljava/lang/Object;
.source "KeypadPanel_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/KeypadPanel;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadPanelPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadPanelPresenter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadPanelPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/KeypadPanel;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/util/Device;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanel;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/orderentry/KeypadPanelPresenter;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/KeypadPanel;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->injectDevice(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/util/Device;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->injectMembers(Lcom/squareup/orderentry/KeypadPanel;)V

    return-void
.end method
