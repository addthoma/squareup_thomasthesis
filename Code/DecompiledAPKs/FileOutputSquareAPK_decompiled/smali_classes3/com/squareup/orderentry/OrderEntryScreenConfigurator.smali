.class public Lcom/squareup/orderentry/OrderEntryScreenConfigurator;
.super Ljava/lang/Object;
.source "OrderEntryScreenConfigurator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method getConfiguration()Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;
    .locals 13

    .line 25
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    sget v2, Lcom/squareup/orderentry/R$layout;->order_entry_view_pager_v2:I

    sget v3, Lcom/squareup/marin/R$dimen;->marin_order_entry_tab_buttons_height:I

    sget v4, Lcom/squareup/marin/R$dimen;->marin_order_entry_tab_buttons_gone_height:I

    sget v5, Lcom/squareup/marin/R$color;->marin_white:I

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;-><init>(IIIIZ)V

    return-object v0

    .line 33
    :cond_0
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    sget v8, Lcom/squareup/orderentry/R$layout;->order_entry_view_pager_v1:I

    sget v9, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    sget v10, Lcom/squareup/marin/R$dimen;->marin_order_entry_tab_buttons_slim_height:I

    sget v11, Lcom/squareup/marin/R$color;->marin_ultra_light_gray:I

    const/4 v12, 0x0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;-><init>(IIIIZ)V

    return-object v0
.end method
