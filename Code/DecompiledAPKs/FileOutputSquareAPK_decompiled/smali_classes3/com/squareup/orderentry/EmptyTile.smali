.class public Lcom/squareup/orderentry/EmptyTile;
.super Landroid/view/View;
.source "EmptyTile.java"


# instance fields
.field private dropTarget:Z

.field private final dropTargetPaint:Landroid/graphics/Paint;

.field private final plusButtonMaxSize:I

.field private final selector:Landroid/graphics/drawable/StateListDrawable;

.field private sizeRatio:F

.field private final strokePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 27
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_dim_translucent_pressed:I

    .line 28
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/orderentry/EmptyTile;->selector:Landroid/graphics/drawable/StateListDrawable;

    .line 30
    sget p2, Lcom/squareup/widgets/pos/R$dimen;->grid_plus_button_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/EmptyTile;->plusButtonMaxSize:I

    .line 31
    sget p2, Lcom/squareup/widgets/pos/R$dimen;->grid_plus_button_stroke:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    .line 33
    iget-object v0, p0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    iget-object v0, p0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    int-to-float p2, p2

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 35
    iget-object p2, p0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/EmptyTile;->dropTargetPaint:Landroid/graphics/Paint;

    .line 38
    iget-object p2, p0, Lcom/squareup/orderentry/EmptyTile;->dropTargetPaint:Landroid/graphics/Paint;

    sget v0, Lcom/squareup/marin/R$color;->marin_barely_dark_translucent_pressed:I

    .line 39
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 38
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 73
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/EmptyTile;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .line 61
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/EmptyTile;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 63
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->invalidate()V

    return-void
.end method

.method public getPlusButtonSize()F
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/orderentry/EmptyTile;->sizeRatio:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17

    move-object/from16 v0, p0

    .line 78
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 80
    iget v1, v0, Lcom/squareup/orderentry/EmptyTile;->sizeRatio:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    return-void

    .line 84
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/EmptyTile;->getWidth()I

    move-result v1

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/EmptyTile;->getHeight()I

    move-result v2

    .line 88
    iget-boolean v3, v0, Lcom/squareup/orderentry/EmptyTile;->dropTarget:Z

    if-eqz v3, :cond_1

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    add-int/lit8 v3, v1, -0x1

    int-to-float v7, v3

    add-int/lit8 v3, v2, -0x1

    int-to-float v8, v3

    .line 89
    iget-object v9, v0, Lcom/squareup/orderentry/EmptyTile;->dropTargetPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 93
    :cond_1
    div-int/lit8 v1, v1, 0x2

    .line 94
    div-int/lit8 v2, v2, 0x2

    .line 95
    iget v3, v0, Lcom/squareup/orderentry/EmptyTile;->sizeRatio:F

    iget v4, v0, Lcom/squareup/orderentry/EmptyTile;->plusButtonMaxSize:I

    int-to-float v4, v4

    mul-float v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    sub-int v4, v1, v3

    int-to-float v6, v4

    int-to-float v9, v2

    add-int v4, v1, v3

    int-to-float v8, v4

    .line 96
    iget-object v10, v0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v7, v9

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    int-to-float v14, v1

    sub-int v1, v2, v3

    int-to-float v13, v1

    add-int/2addr v2, v3

    int-to-float v15, v2

    .line 97
    iget-object v1, v0, Lcom/squareup/orderentry/EmptyTile;->strokePaint:Landroid/graphics/Paint;

    move-object/from16 v11, p1

    move v12, v14

    move-object/from16 v16, v1

    invoke-virtual/range {v11 .. v16}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 67
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 68
    iget-object p3, p0, Lcom/squareup/orderentry/EmptyTile;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getPaddingLeft()I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->getPaddingBottom()I

    move-result v1

    sub-int/2addr p2, v1

    .line 68
    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    return-void
.end method

.method public setDropTarget(Z)V
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/orderentry/EmptyTile;->dropTarget:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 52
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/orderentry/EmptyTile;->dropTarget:Z

    .line 53
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->invalidate()V

    return-void
.end method

.method public setPlusButtonSize(F)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/orderentry/EmptyTile;->sizeRatio:F

    .line 45
    invoke-virtual {p0}, Lcom/squareup/orderentry/EmptyTile;->invalidate()V

    return-void
.end method
