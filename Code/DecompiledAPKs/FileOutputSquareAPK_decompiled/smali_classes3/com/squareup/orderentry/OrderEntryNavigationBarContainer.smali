.class public Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;
.super Landroid/widget/FrameLayout;
.source "OrderEntryNavigationBarContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;
    }
.end annotation


# static fields
.field protected static final FADE_DURATION_MS:J = 0x5aL


# instance fields
.field private currentAnimator:Landroid/animation/ObjectAnimator;

.field private editActionBar:Lcom/squareup/orderentry/NavigationBarEditView;

.field private final isTablet:Z

.field presenter:Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->isTablet:Z

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarSaleView;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarEditView;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->editActionBar:Lcom/squareup/orderentry/NavigationBarEditView;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method private fadeToEditActionBar()V
    .locals 4

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/squareup/orderentry/NavigationBarSaleView;->getAlpha()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    const-string v2, "alpha"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 95
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x5a

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 96
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$1;-><init>(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private fadeToSaleActionBar()V
    .locals 4

    .line 114
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/squareup/orderentry/NavigationBarSaleView;->getAlpha()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    const-string v2, "alpha"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 118
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x5a

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 119
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;-><init>(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->presenter:Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;->dropView(Ljava/lang/Object;)V

    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 38
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 39
    sget v0, Lcom/squareup/orderentry/R$id;->home_navigation_bar_edit_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/NavigationBarEditView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->editActionBar:Lcom/squareup/orderentry/NavigationBarEditView;

    .line 40
    sget v0, Lcom/squareup/orderentry/R$id;->home_navigation_bar_sale_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/NavigationBarSaleView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    .line 41
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->presenter:Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 54
    check-cast p1, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 57
    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;->access$100(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 58
    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->showSaleActionbar(Z)V

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->showEditActionbar(Z)V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 50
    new-instance v0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    invoke-virtual {v2}, Lcom/squareup/orderentry/NavigationBarSaleView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$SavedState;-><init>(Landroid/os/Parcelable;ZLcom/squareup/orderentry/OrderEntryNavigationBarContainer$1;)V

    return-object v0
.end method

.method showEditActionbar(Z)V
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->isTablet:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 80
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->fadeToEditActionBar()V

    goto :goto_0

    .line 82
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->editActionBar:Lcom/squareup/orderentry/NavigationBarEditView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarEditView;->setVisibility(I)V

    .line 83
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarSaleView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showSaleActionbar(Z)V
    .locals 2

    .line 65
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->isTablet:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->fadeToSaleActionBar()V

    goto :goto_0

    .line 69
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->editActionBar:Lcom/squareup/orderentry/NavigationBarEditView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarEditView;->setVisibility(I)V

    .line 70
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->saleActionBar:Lcom/squareup/orderentry/NavigationBarSaleView;

    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/NavigationBarSaleView;->setVisibility(I)V

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p0, v1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->setVisibility(I)V

    :goto_0
    return-void
.end method
