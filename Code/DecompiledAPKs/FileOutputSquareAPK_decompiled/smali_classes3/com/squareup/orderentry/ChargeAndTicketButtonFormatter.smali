.class public final Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;
.super Ljava/lang/Object;
.source "ChargeAndTicketButtonFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;,
        Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0002#$B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ!\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\n2\n\u0008\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002\u00a2\u0006\u0002\u0010\u0011J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J(\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0018H\u0001J\u0008\u0010\u001b\u001a\u00020\u0013H\u0002J\u0010\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u000e\u001a\u00020\nH\u0002J\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u0010H\u0001J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u0016H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)V",
        "buildAmountText",
        "",
        "amountDue",
        "maybeStringRes",
        "",
        "(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;",
        "copyWithNonZeroAmountAndOpenTickets",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;",
        "copyWithZeroAmount",
        "newChargeState",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;",
        "isOpenTicketsEnabled",
        "",
        "formatChargeButtonCopy",
        "hasAvailableCashDrawers",
        "formatConfirmingWithoutCashDrawer",
        "formatPhoneIncludingTaxes",
        "formatTicketButtonCopy",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;",
        "launchesTicketsScreen",
        "unlockedItemsCount",
        "getCashDrawerCopy",
        "chargeState",
        "ChargeButtonCopy",
        "TicketButtonCopy",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p4, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private final buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;
    .locals 1

    if-nez p2, :cond_0

    .line 240
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 241
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 244
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "amount"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 246
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method static synthetic buildAmountText$default(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 238
    check-cast p2, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final copyWithNonZeroAmountAndOpenTickets()Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 7

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/orderentry/R$string;->open_tickets_charge:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v1, v0

    goto :goto_0

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->shouldShowIncludesTaxes()Z

    move-result v1

    const-string v4, "amountDue"

    if-eqz v1, :cond_1

    .line 117
    new-instance v1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    .line 118
    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_charge:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/orderentry/R$string;->charge_amount_including_tax:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-direct {v1, v2, v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_1
    new-instance v1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    .line 130
    iget-object v5, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->open_tickets_charge:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0, v3, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText$default(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-direct {v1, v5, v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1
.end method

.method private final copyWithZeroAmount(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Z)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 5

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 74
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_charge:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v3, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    const-string v4, "amountDue"

    if-nez v1, :cond_1

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-ne p1, v1, :cond_1

    if-eqz p2, :cond_1

    .line 84
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_charge:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0, v3, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText$default(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_1
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/orderentry/R$string;->charge_amount:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {p0, v0, p2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v3, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p1
.end method

.method private final formatConfirmingWithoutCashDrawer()Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 4

    .line 166
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string/jumbo v2, "transaction.amountDue"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/squareup/orderentry/R$string;->charge_amount_confirm:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final formatPhoneIncludingTaxes(Lcom/squareup/protos/common/Money;)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 3

    .line 177
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    .line 178
    sget v1, Lcom/squareup/orderentry/R$string;->charge_amount:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p1

    .line 179
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->including_tax:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 177
    invoke-direct {v0, p1, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getCashDrawerCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 4

    .line 136
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    .line 155
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->cash_drawers_charge_button_open:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2, v1, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 143
    :cond_0
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->cash_drawers_charge_button_open_confirm:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2, v1, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final formatChargeButtonCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;
    .locals 4

    const-string v0, "newChargeState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountDue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 40
    iget-object p4, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->getCashDrawerCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object p1

    goto :goto_0

    .line 42
    :cond_0
    sget-object p4, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-ne p1, p4, :cond_1

    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->formatConfirmingWithoutCashDrawer()Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object p1

    goto :goto_0

    .line 44
    :cond_1
    iget-object p4, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p4, v0, v2

    if-gtz p4, :cond_2

    invoke-direct {p0, p1, p3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->copyWithZeroAmount(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Z)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object p1

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    .line 46
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->copyWithNonZeroAmountAndOpenTickets()Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object p1

    goto :goto_0

    .line 48
    :cond_3
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->shouldShowIncludesTaxes()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-direct {p0, p2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->formatPhoneIncludingTaxes(Lcom/squareup/protos/common/Money;)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_4
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    sget p3, Lcom/squareup/orderentry/R$string;->charge_amount:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p0, p2, p3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->buildAmountText(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {p1, p2, p3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public final formatTicketButtonCopy(ZI)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 197
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_tickets:I

    invoke-interface {p2, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 205
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-nez p1, :cond_3

    if-gtz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    if-ne p2, p1, :cond_2

    .line 216
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;

    .line 217
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_save:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 218
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_new_items_one:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-direct {p1, p2, v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 228
    :cond_2
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;

    .line 229
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_save:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_new_items_multiple:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 231
    invoke-virtual {v1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 232
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 233
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 228
    invoke-direct {p1, v0, p2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 205
    :cond_3
    :goto_0
    new-instance p1, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;

    .line 206
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_save:I

    invoke-interface {p2, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 205
    invoke-direct {p1, p2, v1, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_1
    return-object p1
.end method
