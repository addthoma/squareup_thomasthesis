.class public Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "OrderEntryScreen.java"

# interfaces
.implements Lcom/squareup/orderentry/FlyBySource;
.implements Lcom/squareup/pauses/PausesAndResumes;
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/OrderEntryView;",
        ">;",
        "Lcom/squareup/orderentry/FlyBySource;",
        "Lcom/squareup/pauses/PausesAndResumes;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final backHandler:Lcom/squareup/orderentry/OrderEntryScreenBackHandler;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final clockSkew:Lcom/squareup/orderentry/ClockSkew;

.field private coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

.field private final drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final isTablet:Z

.field private final loggedInQueuesEmpty:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final notificationPresenter:Lcom/squareup/user/NotificationPresenter;

.field private final o1ReminderLauncher:Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final orderEntryViewPagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

.field private final pauseNarcRegistry:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private screen:Lcom/squareup/orderentry/OrderEntryScreen;

.field private screenLoaded:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

.field private final switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

.field private final tempPhotoDir:Ljava/io/File;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/pauses/PauseAndResumeRegistrar;Ljavax/inject/Provider;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/io/File;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenBackHandler;Lcom/squareup/user/NotificationPresenter;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/orderentry/ClockSkew;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Ljava/util/concurrent/Executor;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;)V
    .locals 3
    .param p21    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Ljava/io/File;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler;",
            "Lcom/squareup/user/NotificationPresenter;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/orderentry/ClockSkew;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 201
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 178
    new-instance v1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    const-string v2, "WARNING_POPUP"

    invoke-direct {v1, v2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 183
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screenLoaded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-object v1, p2

    .line 202
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p1

    .line 203
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p3

    .line 204
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    move-object v1, p4

    .line 205
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->pauseNarcRegistry:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    move-object v1, p8

    .line 206
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v1, p9

    .line 207
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p10

    .line 208
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryViewPagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    move-object v1, p5

    .line 209
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->loggedInQueuesEmpty:Ljavax/inject/Provider;

    move-object v1, p6

    .line 210
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-object v1, p7

    .line 211
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->tempPhotoDir:Ljava/io/File;

    .line 212
    invoke-interface {p11}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isTablet:Z

    move-object v1, p12

    .line 213
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->backHandler:Lcom/squareup/orderentry/OrderEntryScreenBackHandler;

    move-object/from16 v1, p13

    .line 214
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->notificationPresenter:Lcom/squareup/user/NotificationPresenter;

    move-object/from16 v1, p14

    .line 215
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-object/from16 v1, p15

    .line 216
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->clockSkew:Lcom/squareup/orderentry/ClockSkew;

    move-object/from16 v1, p16

    .line 217
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    move-object/from16 v1, p17

    .line 218
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    move-object/from16 v1, p18

    .line 219
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-object/from16 v1, p19

    .line 220
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p20

    .line 221
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object/from16 v1, p21

    .line 222
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v1, p22

    .line 223
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object/from16 v1, p23

    .line 224
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->o1ReminderLauncher:Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    move-object/from16 v1, p24

    .line 225
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object/from16 v1, p25

    .line 226
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p26

    .line 227
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v1, p27

    .line 228
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

    move-object/from16 v1, p28

    .line 229
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    return-void
.end method

.method private cleanUpTransactionLedger()V
    .locals 1

    .line 510
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->expireOldPayments()V

    return-void
.end method

.method private clearCoordinator()V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->destroyFlyByViews()V

    const/4 v0, 0x0

    .line 362
    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

    :cond_0
    return-void
.end method

.method private clearTempPhotoDirectoryIfCartEmpty()V
    .locals 4

    .line 493
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->loggedInQueuesEmpty:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->tempPhotoDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 495
    array-length v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Found %d files to delete"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    array-length v1, v0

    if-lez v1, :cond_0

    .line 498
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$AKZj7oagCKZ9VrEf7CnXMD2I6cA;

    invoke-direct {v2, v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$AKZj7oagCKZ9VrEf7CnXMD2I6cA;-><init>([Ljava/io/File;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private isUnobscured()Z
    .locals 2

    .line 514
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screen:Lcom/squareup/orderentry/OrderEntryScreen;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/TopScreenChecker;->isUnobscured(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$MWQ8RKFMftijQRQyI_NV3cBTeTk(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->onEntryFlyBy(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)V

    return-void
.end method

.method static synthetic lambda$clearTempPhotoDirectoryIfCartEmpty$6([Ljava/io/File;)V
    .locals 3

    .line 499
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p0, v1

    .line 502
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic lambda$onEnterScope$1(Lkotlin/Unit;Ljava/lang/Boolean;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Lkotlin/Pair;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 245
    new-instance p0, Lkotlin/Pair;

    invoke-direct {p0, p2, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method

.method public static synthetic lambda$uGyTP7_T1Y998LJMC9vy6KuhbiA(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->postStatusToTutorial(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V

    return-void
.end method

.method private onEntryFlyBy(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)V
    .locals 9

    .line 433
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getCoordinator()Lcom/squareup/orderentry/FlyByCoordinator;

    move-result-object v0

    .line 434
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getDestination()Landroid/widget/TextView;

    move-result-object v3

    .line 436
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v7

    .line 438
    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->access$000(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Landroid/view/View;

    move-result-object v2

    .line 439
    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->access$100(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 440
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x1

    .line 441
    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->access$200(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Lcom/squareup/orderentry/FlyByListener;

    move-result-object p1

    move v6, v7

    move-object v7, p1

    .line 440
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/orderentry/FlyByCoordinator;->addFlyByDiscount(ILandroid/view/View;Landroid/widget/TextView;ZZILcom/squareup/orderentry/FlyByListener;)V

    goto :goto_1

    .line 444
    :cond_0
    instance-of v1, v2, Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 445
    move-object v1, v2

    check-cast v1, Landroid/widget/ImageView;

    .line 446
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 448
    :cond_1
    invoke-static {v2}, Lcom/squareup/util/Views;->tryCopyToBitmapDrawable(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    :goto_0
    move-object v4, v1

    if-eqz v4, :cond_2

    .line 452
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    const/4 v5, 0x1

    const/4 v6, 0x1

    .line 453
    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->access$200(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Lcom/squareup/orderentry/FlyByListener;

    move-result-object v8

    .line 452
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->addFlyByView(ILandroid/view/View;Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByListener;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private onGiftCardWarning()V
    .locals 4

    .line 406
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/orderentry/R$string;->gift_card_account_unsupported_title:I

    sget v3, Lcom/squareup/orderentry/R$string;->gift_card_account_unsupported_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private playFlyByAnimation()V
    .locals 3

    .line 459
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isFlyByAnimationEnqueued()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    goto :goto_0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 464
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    .line 465
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$-GgAvwr1avtHsPfG-knbUtQXTeA;

    invoke-direct {v2, p0, v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$-GgAvwr1avtHsPfG-knbUtQXTeA;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;)V

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private postStatusToTutorial(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isOnPostTransactionMonitor()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 281
    :cond_0
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_1

    const-string p1, "On home edit mode"

    goto :goto_0

    .line 284
    :cond_1
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->cardOptionShouldBeEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "On OrderEntryScreen and ready for card"

    goto :goto_0

    :cond_2
    const-string p1, "Shown OrderEntryScreen"

    .line 286
    :goto_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    if-eqz p2, :cond_3

    const-string p2, "Home visible"

    goto :goto_1

    :cond_3
    const-string p2, "Home not visible"

    :goto_1
    invoke-interface {v0, p1, p2}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public barcodeScanned(Ljava/lang/String;)V
    .locals 2

    .line 389
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isUnobscured()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    .line 390
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeProcessBarcodeScanned(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 393
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isUnobscured()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Ignoring barcode scanned. Screen in foreground: %b, interaction mode: %s"

    .line 392
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method checkForClockSkew()V
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->clockSkew:Lcom/squareup/orderentry/ClockSkew;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ClockSkew;->showClockSkewLockoutIfNeeded()V

    return-void
.end method

.method public dropView(Lcom/squareup/orderentry/OrderEntryView;)V
    .locals 2

    .line 349
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->clearCoordinator()V

    .line 351
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->dismiss()V

    .line 354
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryView;->getSwitchEmployeeEducationPopup()Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->notificationPresenter:Lcom/squareup/user/NotificationPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/user/NotificationPresenter;->dropView(Lcom/squareup/user/NotificationPresenter$View;)V

    .line 356
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 145
    check-cast p1, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->dropView(Lcom/squareup/orderentry/OrderEntryView;)V

    return-void
.end method

.method public getCoordinator()Lcom/squareup/orderentry/FlyByCoordinator;
    .locals 3

    .line 367
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

    if-eqz v0, :cond_0

    return-object v0

    .line 368
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No coordinator. hasView:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->hasView()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDestination()Landroid/widget/TextView;
    .locals 1

    .line 374
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isTablet:Z

    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryView;->getTabletFlyByDestination()Landroid/widget/TextView;

    move-result-object v0

    return-object v0

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryView;->getPhoneFlyByDestination()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method getSwitchEmployeesEducationMessage()Ljava/lang/String;
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getWarningPopupPresenter()Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation

    .line 411
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$OrderEntryScreen$Presenter(Lcom/squareup/orderentry/GiftCardWarning;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 236
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->onGiftCardWarning()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$OrderEntryScreen$Presenter(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 251
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->setHomeScreenIsEditing(Z)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$3$OrderEntryScreen$Presenter(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 255
    instance-of p1, p1, Lcom/squareup/orderentry/OrderEntryScreen;

    if-eqz p1, :cond_0

    .line 256
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->playFlyByAnimation()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$4$OrderEntryScreen$Presenter(Lkotlin/Unit;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 269
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screen:Lcom/squareup/orderentry/OrderEntryScreen;

    invoke-virtual {p2, p1}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->checkForClockSkew()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$playFlyByAnimation$5$OrderEntryScreen$Presenter(Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;Landroid/view/View;II)V
    .locals 7

    .line 468
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getCoordinator()Lcom/squareup/orderentry/FlyByCoordinator;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->getSourceSize()I

    move-result v1

    .line 469
    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->getSourcePosition()[I

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->getDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 470
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v4

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getDestination()Landroid/widget/TextView;

    move-result-object v5

    sget-object v6, Lcom/squareup/orderentry/FlyByListener;->NONE:Lcom/squareup/orderentry/FlyByListener;

    .line 468
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/orderentry/FlyByCoordinator;->addFlyByViewAfterPanel(I[ILandroid/graphics/drawable/Drawable;ILandroid/widget/TextView;Lcom/squareup/orderentry/FlyByListener;)V

    .line 472
    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->clearData()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 518
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->backHandler:Lcom/squareup/orderentry/OrderEntryScreenBackHandler;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 233
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screen:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 235
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/orderentry/GiftCardWarning;

    .line 236
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$vDHgEJhcl8UFaS0JGGuDB-B1qGo;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$vDHgEJhcl8UFaS0JGGuDB-B1qGo;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 235
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$MWQ8RKFMftijQRQyI_NV3cBTeTk;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$MWQ8RKFMftijQRQyI_NV3cBTeTk;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screenLoaded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screen:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 242
    invoke-virtual {v1, v2}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 243
    invoke-virtual {v2}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 244
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v4

    invoke-virtual {v3, v4}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;

    .line 240
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$uGyTP7_T1Y998LJMC9vy6KuhbiA;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$uGyTP7_T1Y998LJMC9vy6KuhbiA;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    .line 247
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 239
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$-ej0HM9EQ-c6l1tv_kpGBWG3d1o;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$-ej0HM9EQ-c6l1tv_kpGBWG3d1o;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    .line 251
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 249
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 253
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 254
    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$aVud18uepxVD-K7OQpXTnm4kGwg;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$aVud18uepxVD-K7OQpXTnm4kGwg;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 253
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->cancelCheckout()V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 266
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 267
    invoke-interface {v1}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 266
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1sXsR_L0gsZBJfjkQ6JzXMNy1Qk;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1sXsR_L0gsZBJfjkQ6JzXMNy1Qk;-><init>(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    .line 268
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 265
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 290
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->clearCoordinator()V

    .line 291
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->endEditing()V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 303
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 305
    new-instance v0, Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/FlyByCoordinator;-><init>(Landroid/widget/FrameLayout;)V

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

    .line 307
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryView;->getSwitchEmployeeEducationPopup()Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->takeView(Ljava/lang/Object;)V

    .line 308
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isUnobscured()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->switchEmployeesEducationPresenter:Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->showIfUnseen()V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->pauseNarcRegistry:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 313
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->enqueueBillInFlightAwaitingReceiptInfo()V

    .line 314
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->clearTempPhotoDirectoryIfCartEmpty()V

    .line 315
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->cleanUpTransactionLedger()V

    .line 317
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->isUnobscured()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 318
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 319
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 321
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Manual card data not expected on keypad"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 324
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->notificationPresenter:Lcom/squareup/user/NotificationPresenter;

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/user/NotificationPresenter;->takeView(Ljava/lang/Object;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    invoke-interface {v0}, Lcom/squareup/applet/ActionBarNavigationHelper;->getHasDrawerButton()Z

    move-result v0

    if-nez v0, :cond_3

    .line 327
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryView;->hideDrawerButton()V

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    if-nez p1, :cond_4

    .line 332
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->O1_DEPRECATION_WARNING_JP:Lcom/squareup/settings/server/Features$Feature;

    .line 333
    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 334
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->o1ReminderLauncher:Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;->attemptToShowContent(Ljava/lang/Void;)Z

    .line 336
    :cond_4
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->screenLoaded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->coordinator:Lcom/squareup/orderentry/FlyByCoordinator;

    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->destroyFlyByViews()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method onStartVisualTransition()V
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logEvent(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method onTwoFingerFling(Z)V
    .locals 1

    .line 522
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryViewPagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->animate(Z)V

    return-void
.end method

.method shouldDrawShadow()Z
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isShowingCashDrawerDialog()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
