.class public Lcom/squareup/orderentry/PaymentPadPhoneView;
.super Landroid/widget/RelativeLayout;
.source "PaymentPadPhoneView.java"


# instance fields
.field private final actionBarHeight:I

.field private chargeAndTicketsButton:Landroid/view/View;

.field private final fullTabHeight:I

.field private keypadTab:Landroid/widget/TextView;

.field private libraryTab:Landroid/widget/TextView;

.field presenter:Lcom/squareup/orderentry/PaymentPadPhonePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

.field private final slimTabHeight:I

.field private tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

.field private final upperSectionBackgroundPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-class v0, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-static {p2, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;->inject(Lcom/squareup/orderentry/PaymentPadPhoneView;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->presenter:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-virtual {p2}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getScreenConfiguration()Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    .line 46
    iget-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    iget p2, p2, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->orderEntryViewPagerId:I

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 49
    sget v0, Lcom/squareup/marin/R$dimen;->marin_action_bar_height:I

    .line 50
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->actionBarHeight:I

    .line 51
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    iget v0, v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->diminishedTabHeight:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->slimTabHeight:I

    .line 52
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    iget v0, v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->fullTabHeight:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->fullTabHeight:I

    .line 53
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->upperSectionBackgroundPaint:Landroid/graphics/Paint;

    .line 54
    iget-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->upperSectionBackgroundPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    iget v0, v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->upperSectionBackgroundColor:I

    .line 55
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 54
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 p1, 0x0

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/PaymentPadPhoneView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->presenter:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->dropView(Ljava/lang/Object;)V

    .line 112
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 102
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getRight()I

    move-result v0

    int-to-float v4, v0

    iget v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->actionBarHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->upperSectionBackgroundPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 104
    iget v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->actionBarHeight:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->chargeAndTicketsButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->chargeAndTicketsButton:Landroid/view/View;

    .line 105
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->upperSectionBackgroundPaint:Landroid/graphics/Paint;

    .line 104
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->chargeAndTicketsButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v2, v0

    iget v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->actionBarHeight:I

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getRight()I

    move-result v0

    int-to-float v4, v0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->chargeAndTicketsButton:Landroid/view/View;

    .line 107
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->upperSectionBackgroundPaint:Landroid/graphics/Paint;

    .line 106
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 70
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 71
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_tabs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SlidingTwoTabLayout;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    .line 72
    sget v0, Lcom/squareup/orderentry/R$id;->tab_keypad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    .line 73
    sget v0, Lcom/squareup/orderentry/R$id;->tab_library:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    .line 74
    sget v0, Lcom/squareup/orderentry/R$id;->phone_payment_pad_charge_and_tickets_buttons:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->chargeAndTicketsButton:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadPhoneView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadPhoneView$1;-><init>(Lcom/squareup/orderentry/PaymentPadPhoneView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadPhoneView$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadPhoneView$2;-><init>(Lcom/squareup/orderentry/PaymentPadPhoneView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->screenConfiguration:Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    iget-boolean v0, v0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->configureOrderEntryTabsV2:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_order_entry_tab_keypad:I

    invoke-static {v1, v2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x0

    .line 87
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhoneView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/squareup/orderentry/R$drawable;->selector_order_entry_tab_library:I

    invoke-static {v1, v3}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->presenter:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 4

    .line 62
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isActivated()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    .line 63
    invoke-virtual {v0}, Landroid/widget/TextView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 65
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setActivated(Z)V

    :cond_4
    return-void
.end method

.method public setTabsOffset(F)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SlidingTwoTabLayout;->setOffset(F)V

    return-void
.end method

.method public showFullTabs()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    invoke-virtual {v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 131
    iget v1, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->fullTabHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 132
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    invoke-virtual {v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->requestLayout()V

    return-void
.end method

.method public showSlimTabs()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->keypadTab:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->libraryTab:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    invoke-virtual {v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 123
    iget v1, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->slimTabHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 124
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhoneView;->tabLayout:Lcom/squareup/widgets/SlidingTwoTabLayout;

    invoke-virtual {v0}, Lcom/squareup/widgets/SlidingTwoTabLayout;->requestLayout()V

    return-void
.end method
