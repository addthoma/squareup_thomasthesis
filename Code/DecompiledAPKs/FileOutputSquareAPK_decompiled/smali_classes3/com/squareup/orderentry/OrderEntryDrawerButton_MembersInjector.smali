.class public final Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;
.super Ljava/lang/Object;
.source "OrderEntryDrawerButton_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/OrderEntryDrawerButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/OrderEntryDrawerButton;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectBadgePresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/BadgePresenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/AppletsDrawerPresenter;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->presenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->injectBadgePresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/BadgePresenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/AppletsDrawerPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/orderentry/OrderEntryDrawerButton;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->injectMembers(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V

    return-void
.end method
