.class final Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;
.super Ljava/lang/Object;
.source "OnboardingAddressViewHolder.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->observeAddressChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "postal",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->$component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->accept(Ljava/lang/String;)V

    return-void
.end method

.method public final accept(Ljava/lang/String;)V
    .locals 4

    .line 78
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->$component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    const-string v2, "postal"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->postalCodeOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->$component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;

    invoke-static {v2}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->access$getAddressLayout$p(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;)Lcom/squareup/address/AddressLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/address/AddressLayout;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v3

    invoke-static {v2, p1, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->access$checkPostalCode(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Ljava/lang/String;Lcom/squareup/CountryCode;)Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->postalCodeIsValidOutput(Z)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    .line 79
    invoke-interface {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
