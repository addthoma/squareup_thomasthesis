.class final Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;
.super Ljava/lang/Object;
.source "OnboardingReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor;->launch(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $secureScreenToken:Ljava/lang/String;

.field final synthetic $subs:Lio/reactivex/disposables/CompositeDisposable;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Ljava/lang/String;Lio/reactivex/disposables/CompositeDisposable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->$secureScreenToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->$subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    invoke-static {v0}, Lcom/squareup/onboarding/flow/OnboardingReactor;->access$getSecureScopeManager$p(Lcom/squareup/onboarding/flow/OnboardingReactor;)Lcom/squareup/secure/SecureScopeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->$secureScreenToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/secure/SecureScopeManager;->leaveSecureSession(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;->$subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method
