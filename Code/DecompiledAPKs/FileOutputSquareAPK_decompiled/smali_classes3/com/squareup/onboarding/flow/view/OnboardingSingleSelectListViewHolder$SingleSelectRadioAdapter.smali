.class final Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;
.super Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;
.source "OnboardingSingleSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SingleSelectRadioAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter<",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingSingleSelectListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingSingleSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter\n*L\n1#1,159:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\rH\u0016J\u0010\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;",
        "component",
        "Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V",
        "getComponent",
        "()Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "getInputHandler",
        "()Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "selectedRow",
        "",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "onRowTapped",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

.field private final inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

.field private selectedRow:I


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    const/4 p1, -0x1

    .line 87
    iput p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    .line 91
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->keySelectedByDefault()Ljava/lang/String;

    move-result-object p1

    .line 92
    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->getKeys()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    iput p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    .line 94
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->getKeys()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->keyOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final getComponent()Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    return-object v0
.end method

.method public final getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;I)V
    .locals 2

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;->getTextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;->getTextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->getValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 107
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;->getTextView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iget v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 83
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;
    .locals 2

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;

    sget v1, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_single_list_item:I

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic onRowTapped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;)V

    return-void
.end method

.method public onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;)V
    .locals 3

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;->getAdapterPosition()I

    move-result v0

    iget v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    if-ne v0, v1, :cond_0

    return-void

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;->getAdapterPosition()I

    move-result p1

    .line 116
    iput p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    .line 119
    invoke-virtual {p0, v1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->notifyItemChanged(I)V

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->notifyItemChanged(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->getKeys()Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;->selectedRow:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->keyOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
