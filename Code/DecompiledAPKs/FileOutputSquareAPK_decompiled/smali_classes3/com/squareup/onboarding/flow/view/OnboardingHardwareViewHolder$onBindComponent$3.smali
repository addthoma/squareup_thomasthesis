.class final Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;
.super Ljava/lang/Object;
.source "OnboardingHardwareViewHolder.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingHardwareViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingHardwareViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->$component:Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    return-void

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-static {p1}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->access$getItems$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Lcom/squareup/noho/NohoCheckableGroup;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoCheckableRow;->getTag()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getImageUrl()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_2

    .line 56
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-static {p2}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->access$getImageView$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Landroid/widget/ImageView;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 58
    :cond_2
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->getPicasso$onboarding_release()Lcom/squareup/picasso/Picasso;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getImageUrl()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p2

    .line 59
    iget-object p3, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-static {p3}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->access$getImageView$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Landroid/widget/ImageView;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 62
    :goto_1
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-static {p2}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->access$getCaptionView$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Lcom/squareup/widgets/MessageView;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getDisclaimer()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p2, p3}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 64
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;->$component:Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->keyOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void

    .line 54
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.onboarding.flow.data.OnboardingHardwareItem.Product"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
