.class final Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingBankAccountItem.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->invoke()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/onboard/Output;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/client/onboard/Output;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/protos/client/onboard/Output;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;->invoke(Lcom/squareup/protos/client/onboard/Output;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/onboard/Output;)Z
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil;->INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    const-string v2, "it.string_value"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/squareup/banklinking/InstitutionNumberUtil;->isSecondaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
