.class public interface abstract Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;
.super Ljava/lang/Object;
.source "OnboardingWorkflowParentComponent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0008H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;",
        "",
        "inject",
        "",
        "view",
        "Lcom/squareup/onboarding/flow/OnboardingPanelView;",
        "viewHolder",
        "Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V
.end method

.method public abstract inject(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)V
.end method

.method public abstract inject(Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;)V
.end method

.method public abstract inject(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;)V
.end method
