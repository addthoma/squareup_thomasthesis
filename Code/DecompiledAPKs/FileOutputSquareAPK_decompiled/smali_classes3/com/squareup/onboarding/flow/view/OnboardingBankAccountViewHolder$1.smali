.class public final Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;
.super Ljava/lang/Object;
.source "OnboardingBankAccountViewHolder.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "onViewAttachedToWindow",
        "",
        "v",
        "Landroid/view/View;",
        "onViewDetachedFromWindow",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-static {p1}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->access$observeBankAccountChanges(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-static {p1}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->access$getSubs$p(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)Lrx/subscriptions/CompositeSubscription;

    move-result-object p1

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method
