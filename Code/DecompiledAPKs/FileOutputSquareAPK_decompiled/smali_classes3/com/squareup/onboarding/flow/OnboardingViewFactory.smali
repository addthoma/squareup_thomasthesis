.class public final Lcom/squareup/onboarding/flow/OnboardingViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "OnboardingViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "coordinatorFactory",
        "Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;",
        "(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "coordinatorFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 12
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingPanel;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingPanel;

    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/OnboardingPanel;->getBINDING_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/onboarding/flow/R$layout;->onboarding_panel_view:I

    .line 13
    new-instance v4, Lcom/squareup/onboarding/flow/OnboardingViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/onboarding/flow/OnboardingViewFactory$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 11
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 15
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    sget-object v1, Lcom/squareup/onboarding/flow/OnboardingExitDialog;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingExitDialog;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/OnboardingExitDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 17
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingViewFactory$2;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingViewFactory$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 15
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
