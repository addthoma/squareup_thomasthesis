.class public final Lcom/squareup/onboarding/flow/FinishFlowEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "OnboardingWorkflowAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/FinishFlowEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "result",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "(Lcom/squareup/onboarding/OnboardingWorkflowResult;)V",
        "flow_result",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final flow_result:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingWorkflowResult;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Onboarding Workflow: Finished"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 32
    invoke-static {p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowAnalyticsKt;->access$fromResult(Lcom/squareup/onboarding/OnboardingWorkflowResult;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/FinishFlowEvent;->flow_result:Ljava/lang/String;

    return-void
.end method
