.class final Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;
.super Ljava/lang/Object;
.source "OnboardingCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u0004\u0012\u00020\u0004\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008 \t* \u0012\u0004\u0012\u00020\u0004\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "Lcom/squareup/onboarding/flow/OnboardingPanelScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingCoordinator$attach$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 46
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 47
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;->getActionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;->getOutputs()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;->getFromTimeout()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    .line 46
    invoke-interface {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
