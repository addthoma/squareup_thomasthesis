.class public final Lcom/squareup/onboarding/flow/PanelVerifier;
.super Ljava/lang/Object;
.source "PanelVerifier.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanelVerifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PanelVerifier.kt\ncom/squareup/onboarding/flow/PanelVerifier\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,24:1\n1265#2,12:25\n*E\n*S KotlinDebug\n*F\n+ 1 PanelVerifier.kt\ncom/squareup/onboarding/flow/PanelVerifier\n*L\n17#1,12:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelVerifier;",
        "",
        "()V",
        "unsatisfiedValidators",
        "",
        "Lcom/squareup/protos/client/onboard/Validator;",
        "panel",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "outputs",
        "",
        "Lcom/squareup/protos/client/onboard/Output;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final unsatisfiedValidators(Lcom/squareup/protos/client/onboard/Panel;Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/onboard/Panel;",
            "Ljava/util/Collection<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            ">;"
        }
    .end annotation

    const-string v0, "panel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/onboarding/flow/PanelMemory;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/PanelMemory;-><init>(Ljava/util/Collection;)V

    .line 17
    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->components:Ljava/util/List;

    const-string p2, "panel.components"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 25
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 32
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 33
    check-cast v1, Lcom/squareup/protos/client/onboard/Component;

    .line 18
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;

    const-string v3, "component"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v0}, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->createComponentItem(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    move-result-object v2

    .line 19
    iget-object v3, v1, Lcom/squareup/protos/client/onboard/Component;->name:Ljava/lang/String;

    const-string v4, "component.name"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/squareup/onboarding/flow/PanelMemory;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->outputs()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->validator(Ljava/util/List;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    move-result-object v2

    .line 20
    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Component;->validators:Ljava/util/List;

    const-string v3, "component.validators"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;->unsatisfiedValidators(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 34
    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 36
    :cond_0
    check-cast p2, Ljava/util/List;

    return-object p2
.end method
