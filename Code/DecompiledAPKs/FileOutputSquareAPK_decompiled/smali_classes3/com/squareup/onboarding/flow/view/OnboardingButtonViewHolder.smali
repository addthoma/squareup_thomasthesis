.class public final Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingButtonViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingButtonViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingButtonViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingButtonViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,43:1\n1103#2,7:44\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingButtonViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingButtonViewHolder\n*L\n33#1,7:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "buttonRoot",
        "Landroid/widget/FrameLayout;",
        "destructiveButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "primaryButton",
        "secondaryButton",
        "buttonForStyle",
        "style",
        "",
        "onBindComponent",
        "",
        "component",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buttonRoot:Landroid/widget/FrameLayout;

.field private final destructiveButton:Lcom/squareup/marketfont/MarketButton;

.field private final primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private final secondaryButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_button:I

    .line 16
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 19
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_button_root:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->buttonRoot:Landroid/widget/FrameLayout;

    .line 20
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_button_primary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 21
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_button_secondary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 23
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_button_destructive:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->destructiveButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final buttonForStyle(Ljava/lang/String;)Lcom/squareup/marketfont/MarketButton;
    .locals 2

    .line 36
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x6d138a7e

    if-eq v0, v1, :cond_2

    const v1, -0x30bb8e8c    # -3.2957696E9f

    if-eq v0, v1, :cond_1

    const v1, -0x12c2f1fe

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "primary"

    .line 37
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    goto :goto_1

    :cond_1
    const-string v0, "secondary"

    .line 38
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    goto :goto_1

    :cond_2
    const-string v0, "destructive"

    .line 39
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->destructiveButton:Lcom/squareup/marketfont/MarketButton;

    goto :goto_1

    .line 40
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    :goto_1
    return-object p1
.end method


# virtual methods
.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;)V
    .locals 6

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;->style()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->buttonForStyle(Ljava/lang/String;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->buttonRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 28
    iget-object v4, p0, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->buttonRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "child"

    .line 29
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-ne v4, v0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    invoke-static {v4, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 33
    check-cast v0, Landroid/view/View;

    .line 44
    new-instance v1, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder$onBindComponent$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder$onBindComponent$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;)V

    return-void
.end method
