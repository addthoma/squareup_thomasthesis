.class public final Lcom/squareup/onboarding/ActivationServiceHelper_Factory;
.super Ljava/lang/Object;
.source "ActivationServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/ActivationServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/ActivationServiceHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;)",
            "Lcom/squareup/onboarding/ActivationServiceHelper_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ActivationServiceHelper;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/onboarding/ActivationServiceHelper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/ActivationServiceHelper;-><init>(Landroid/app/Application;Lcom/squareup/server/activation/ActivationService;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/ActivationServiceHelper;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/activation/ActivationService;

    invoke-static {v0, v1}, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ActivationServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/onboarding/ActivationServiceHelper_Factory;->get()Lcom/squareup/onboarding/ActivationServiceHelper;

    move-result-object v0

    return-object v0
.end method
