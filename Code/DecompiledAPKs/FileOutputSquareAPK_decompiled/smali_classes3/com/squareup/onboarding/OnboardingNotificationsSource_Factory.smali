.class public final Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankLinkingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final localNotificationAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationStateStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->localNotificationAnalyticsProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->notificationStateStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;)",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/notificationcenterdata/NotificationStateStore;)Lcom/squareup/onboarding/OnboardingNotificationsSource;
    .locals 8

    .line 66
    new-instance v7, Lcom/squareup/onboarding/OnboardingNotificationsSource;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/OnboardingNotificationsSource;-><init>(Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/OnboardingNotificationsSource;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->localNotificationAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/banklinking/BankLinkingStarter;

    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->notificationStateStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/notificationcenterdata/NotificationStateStore;

    invoke-static/range {v1 .. v6}, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->newInstance(Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/notificationcenterdata/NotificationStateStore;)Lcom/squareup/onboarding/OnboardingNotificationsSource;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/onboarding/OnboardingNotificationsSource_Factory;->get()Lcom/squareup/onboarding/OnboardingNotificationsSource;

    move-result-object v0

    return-object v0
.end method
