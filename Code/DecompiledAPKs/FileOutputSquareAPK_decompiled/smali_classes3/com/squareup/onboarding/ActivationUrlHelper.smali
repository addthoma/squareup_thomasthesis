.class public Lcom/squareup/onboarding/ActivationUrlHelper;
.super Ljava/lang/Object;
.source "ActivationUrlHelper.java"


# instance fields
.field private final activationService:Lcom/squareup/onboarding/ActivationServiceHelper;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final progressGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailureRxGlue<",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;"
        }
    .end annotation
.end field

.field private final progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p2, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->activationService:Lcom/squareup/onboarding/ActivationServiceHelper;

    .line 23
    iput-object p3, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 25
    new-instance p2, Lcom/squareup/request/RequestMessages;

    sget p3, Lcom/squareup/common/strings/R$string;->loading:I

    sget v0, Lcom/squareup/onboarding/common/R$string;->onboarding_get_web_link_fail:I

    invoke-direct {p2, p1, p3, v0}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;II)V

    .line 29
    new-instance p1, Lcom/squareup/caller/ProgressAndFailurePresenter;

    new-instance p3, Lcom/squareup/onboarding/ActivationUrlHelper$1;

    invoke-direct {p3, p0}, Lcom/squareup/onboarding/ActivationUrlHelper$1;-><init>(Lcom/squareup/onboarding/ActivationUrlHelper;)V

    const-string v0, "activationUrl"

    invoke-direct {p1, v0, p2, p3}, Lcom/squareup/caller/ProgressAndFailurePresenter;-><init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    .line 39
    iget-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {p4, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;->forSimpleResponse(Lcom/squareup/caller/ProgressAndFailurePresenter;)Lcom/squareup/caller/ProgressAndFailureRxGlue;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    return-void
.end method

.method public synthetic lambda$loadAndOpen$0$ActivationUrlHelper(Lcom/squareup/server/activation/ActivationUrl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object p1, p1, Lcom/squareup/server/activation/ActivationUrl;->activation_url:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public loadAndOpen()V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->activationService:Lcom/squareup/onboarding/ActivationServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/onboarding/ActivationServiceHelper;->activationUrl()Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    .line 47
    invoke-virtual {v1}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->showRetrofitProgress()Lio/reactivex/SingleTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    new-instance v2, Lcom/squareup/onboarding/-$$Lambda$ActivationUrlHelper$w4JpeKVM9MSoTwKtqqowBbosugk;

    invoke-direct {v2, p0}, Lcom/squareup/onboarding/-$$Lambda$ActivationUrlHelper$w4JpeKVM9MSoTwKtqqowBbosugk;-><init>(Lcom/squareup/onboarding/ActivationUrlHelper;)V

    .line 48
    invoke-virtual {v1, v2}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->handler(Lio/reactivex/functions/Consumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public takeView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationUrlHelper;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
