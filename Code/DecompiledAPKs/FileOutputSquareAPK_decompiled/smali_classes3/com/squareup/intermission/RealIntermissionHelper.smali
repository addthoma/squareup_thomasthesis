.class public final Lcom/squareup/intermission/RealIntermissionHelper;
.super Ljava/lang/Object;
.source "RealIntermissionHelper.kt"

# interfaces
.implements Lcom/squareup/intermission/IntermissionHelper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\u0007\u001a\u0004\u0018\u00010\u00082\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/intermission/RealIntermissionHelper;",
        "Lcom/squareup/intermission/IntermissionHelper;",
        "durationPickerRunner",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "(Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/text/DurationFormatter;)V",
        "getGapTimeInfo",
        "Lcom/squareup/intermission/GapTimeInfo;",
        "variation",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "intermissions",
        "",
        "Lcom/squareup/api/items/Intermission;",
        "duration",
        "Lorg/threeten/bp/Duration;",
        "startDurationPicker",
        "",
        "allowZeroDuration",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/text/DurationFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "durationPickerRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    iput-object p2, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method


# virtual methods
.method public getGapTimeInfo(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/intermission/GapTimeInfo;
    .locals 3

    const-string/jumbo v0, "variation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "variation.intermissions"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-string v1, "Duration.ofMillis(variation.duration)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/squareup/intermission/RealIntermissionHelper;->getGapTimeInfo(Ljava/util/List;Lorg/threeten/bp/Duration;)Lcom/squareup/intermission/GapTimeInfo;

    move-result-object p1

    return-object p1
.end method

.method public getGapTimeInfo(Ljava/util/List;Lorg/threeten/bp/Duration;)Lcom/squareup/intermission/GapTimeInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;",
            "Lorg/threeten/bp/Duration;",
            ")",
            "Lcom/squareup/intermission/GapTimeInfo;"
        }
    .end annotation

    const-string v0, "intermissions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "duration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 27
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/Intermission;

    .line 29
    invoke-static {p1}, Lcom/squareup/intermission/IntermissionHelperKt;->getInitialDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object v2

    .line 30
    invoke-static {p1}, Lcom/squareup/intermission/IntermissionHelperKt;->getGapDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object v3

    .line 31
    invoke-static {p1, p2}, Lcom/squareup/intermission/IntermissionHelperKt;->getFinalDuration(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 33
    iget-object p2, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    const/4 v4, 0x2

    invoke-static {p2, v2, v0, v4, v1}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 34
    iget-object v2, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-static {v2, v3, v0, v4, v1}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 35
    iget-object v3, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-static {v3, p1, v0, v4, v1}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 37
    new-instance v1, Lcom/squareup/intermission/GapTimeInfo;

    invoke-direct {v1, p2, v2, p1}, Lcom/squareup/intermission/GapTimeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1
.end method

.method public startDurationPicker(Lorg/threeten/bp/Duration;Z)J
    .locals 5

    const-string v0, "duration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    sget-object v0, Lkotlin/random/Random;->Default:Lkotlin/random/Random$Default;

    invoke-virtual {v0}, Lkotlin/random/Random$Default;->nextLong()J

    move-result-wide v0

    .line 59
    iget-object v2, p0, Lcom/squareup/intermission/RealIntermissionHelper;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    .line 60
    sget v3, Lcom/squareup/widgets/pos/R$string;->duration_title:I

    .line 61
    new-instance v4, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    invoke-direct {v4, p1, v0, v1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    .line 59
    invoke-virtual {v2, v3, v4, p2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-wide v0
.end method
