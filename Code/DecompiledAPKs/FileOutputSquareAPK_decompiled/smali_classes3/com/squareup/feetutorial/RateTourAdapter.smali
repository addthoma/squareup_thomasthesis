.class public final Lcom/squareup/feetutorial/RateTourAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "RateTourAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/feetutorial/RateTourAdapter$TopCropTransformation;
    }
.end annotation


# static fields
.field private static final BPS_DENOMINATOR:Ljava/math/BigDecimal;

.field private static final MAX_BPS:I = 0x2710

.field private static final SAMPLE_TRANSACTION_SUBUNITS:I = 0x2710


# instance fields
.field private final context:Landroid/content/Context;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final isLandscape:Z

.field private final isTablet:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field final pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RatePage;",
            ">;"
        }
    .end annotation
.end field

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final rateImagePadding:I

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x2710

    .line 44
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/squareup/feetutorial/RateTourAdapter;->BPS_DENOMINATOR:Ljava/math/BigDecimal;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;ZZLcom/squareup/text/RateFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/picasso/Picasso;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/util/Res;",
            "ZZ",
            "Lcom/squareup/text/RateFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/picasso/Picasso;",
            ")V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    .line 62
    iput-object p1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->context:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/squareup/feetutorial/RateTourAdapter;->res:Lcom/squareup/util/Res;

    .line 64
    iput-boolean p3, p0, Lcom/squareup/feetutorial/RateTourAdapter;->isTablet:Z

    .line 65
    iput-boolean p4, p0, Lcom/squareup/feetutorial/RateTourAdapter;->isLandscape:Z

    .line 66
    iput-object p5, p0, Lcom/squareup/feetutorial/RateTourAdapter;->rateFormatter:Lcom/squareup/text/RateFormatter;

    .line 67
    iput-object p6, p0, Lcom/squareup/feetutorial/RateTourAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 68
    iput-object p7, p0, Lcom/squareup/feetutorial/RateTourAdapter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->rateImagePadding:I

    .line 71
    iput-object p8, p0, Lcom/squareup/feetutorial/RateTourAdapter;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method

.method private static feeFor(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 145
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    .line 146
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/squareup/feetutorial/RateTourAdapter;->BPS_DENOMINATOR:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 149
    sget-object v1, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-static {p0, v0, v1}, Lcom/squareup/money/MoneyMath;->multiply(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 150
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v2, p1

    add-long/2addr v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 154
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public indexFromCategory(Lcom/squareup/feetutorial/RateCategory;)I
    .locals 3

    const/4 v0, 0x0

    .line 162
    :goto_0
    iget-object v1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 163
    iget-object v1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/feetutorial/RatePage;

    iget-object v1, v1, Lcom/squareup/feetutorial/RatePage;->category:Lcom/squareup/feetutorial/RateCategory;

    if-ne v1, p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Category not in list of pages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .line 85
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/feetutorial/RatePage;

    .line 86
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/feetutorial/impl/R$layout;->rate_page:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 87
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 90
    sget p1, Lcom/squareup/feetutorial/impl/R$id;->rate_title:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 91
    iget-object v1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iget-object v2, p2, Lcom/squareup/feetutorial/RatePage;->rate:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v2, v2, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, Lcom/squareup/feetutorial/RateTourAdapter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v3, p2, Lcom/squareup/feetutorial/RatePage;->rate:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, v3, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/squareup/util/Percentage;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object v3

    .line 92
    invoke-virtual {v1, v2, v3}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 94
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    sget p1, Lcom/squareup/feetutorial/impl/R$id;->rate_subtitle:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 98
    iget v2, p2, Lcom/squareup/feetutorial/RatePage;->subtitle:I

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 101
    sget p1, Lcom/squareup/feetutorial/impl/R$id;->rate_heading:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 102
    iget-object v2, p0, Lcom/squareup/feetutorial/RateTourAdapter;->res:Lcom/squareup/util/Res;

    iget v3, p2, Lcom/squareup/feetutorial/RatePage;->heading:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "rate"

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    sget p1, Lcom/squareup/feetutorial/impl/R$id;->rate_content:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    .line 106
    iget-object v2, p0, Lcom/squareup/feetutorial/RateTourAdapter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v4, 0x2710

    invoke-static {v4, v5, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 108
    iget-object v4, p0, Lcom/squareup/feetutorial/RateTourAdapter;->res:Lcom/squareup/util/Res;

    iget v5, p2, Lcom/squareup/feetutorial/RatePage;->message:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/feetutorial/RateTourAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 109
    invoke-interface {v5, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    const-string/jumbo v6, "total"

    invoke-virtual {v4, v6, v5}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/feetutorial/RateTourAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v6, p2, Lcom/squareup/feetutorial/RatePage;->rate:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 111
    invoke-static {v2, v6}, Lcom/squareup/feetutorial/RateTourAdapter;->feeFor(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v5, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v5, "after_fee"

    .line 110
    invoke-virtual {v4, v5, v2}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 112
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    sget v1, Lcom/squareup/feetutorial/impl/R$id;->rate_image:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 117
    iget v2, p2, Lcom/squareup/feetutorial/RatePage;->imageResId:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    iget-boolean v2, p0, Lcom/squareup/feetutorial/RateTourAdapter;->isLandscape:Z

    if-eqz v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/squareup/feetutorial/RateTourAdapter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/feetutorial/impl/R$dimen;->rate_max_message_width:I

    .line 123
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v2

    .line 122
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/MessageView;->setMaxWidth(I)V

    .line 124
    iget-boolean p1, p2, Lcom/squareup/feetutorial/RatePage;->cropImageInLandscape:Z

    if-eqz p1, :cond_0

    .line 127
    iget-object p1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->picasso:Lcom/squareup/picasso/Picasso;

    iget v2, p2, Lcom/squareup/feetutorial/RatePage;->imageResId:I

    invoke-virtual {p1, v2}, Lcom/squareup/picasso/Picasso;->load(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    new-instance v2, Lcom/squareup/feetutorial/RateTourAdapter$TopCropTransformation;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/squareup/feetutorial/RateTourAdapter$TopCropTransformation;-><init>(Lcom/squareup/feetutorial/RateTourAdapter$1;)V

    invoke-virtual {p1, v2}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 132
    :cond_0
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    .line 133
    iget-boolean p2, p2, Lcom/squareup/feetutorial/RatePage;->imageGravityBottom:Z

    if-eqz p2, :cond_1

    const/16 p2, 0x51

    .line 134
    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 136
    :cond_1
    iget p1, p0, Lcom/squareup/feetutorial/RateTourAdapter;->rateImagePadding:I

    invoke-virtual {v1, p1, p1, p1, p1}, Landroid/widget/ImageView;->setPadding(IIII)V

    :goto_0
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method updatePages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RatePage;",
            ">;)V"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 76
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 77
    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateTourAdapter;->notifyDataSetChanged()V

    return-void
.end method
