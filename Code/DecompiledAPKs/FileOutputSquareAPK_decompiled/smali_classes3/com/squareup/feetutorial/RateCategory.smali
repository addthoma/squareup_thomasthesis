.class final enum Lcom/squareup/feetutorial/RateCategory;
.super Ljava/lang/Enum;
.source "RateCategory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/feetutorial/RateCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/feetutorial/RateCategory;

.field public static final enum DIP_R12:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum DIP_R6:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum MANUAL:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum SWIPE:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum TAP:Lcom/squareup/feetutorial/RateCategory;

.field public static final enum TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 14
    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v1, 0x0

    const-string v2, "DIP_R12"

    invoke-direct {v0, v2, v1}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v2, 0x1

    const-string v3, "DIP_R6"

    invoke-direct {v0, v3, v2}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v3, 0x2

    const-string v4, "DIP_R6_JCB"

    invoke-direct {v0, v4, v3}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v4, 0x3

    const-string v5, "MANUAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v5, 0x4

    const-string v6, "SWIPE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v6, 0x5

    const-string v7, "TAP"

    invoke-direct {v0, v7, v6}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    new-instance v0, Lcom/squareup/feetutorial/RateCategory;

    const/4 v7, 0x6

    const-string v8, "TAP_INTERAC"

    invoke-direct {v0, v8, v7}, Lcom/squareup/feetutorial/RateCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/feetutorial/RateCategory;

    .line 13
    sget-object v8, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/feetutorial/RateCategory;->$VALUES:[Lcom/squareup/feetutorial/RateCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static categories(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RateCategory;",
            ">;"
        }
    .end annotation

    .line 21
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p0

    .line 23
    sget-object v1, Lcom/squareup/feetutorial/RateCategory$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v0, v4, :cond_4

    const/4 v5, 0x4

    if-eq v0, v3, :cond_3

    if-eq v0, v2, :cond_2

    if-eq v0, v5, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    .line 35
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_0
    new-array v0, v3, [Lcom/squareup/feetutorial/RateCategory;

    .line 33
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    invoke-static {p0, v0}, Lcom/squareup/feetutorial/RateCategory;->filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_1
    new-array v0, v2, [Lcom/squareup/feetutorial/RateCategory;

    .line 31
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, Lcom/squareup/feetutorial/RateCategory;->filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_2
    new-array v0, v2, [Lcom/squareup/feetutorial/RateCategory;

    .line 29
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, Lcom/squareup/feetutorial/RateCategory;->filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_3
    new-array v0, v5, [Lcom/squareup/feetutorial/RateCategory;

    .line 27
    sget-object v5, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lcom/squareup/feetutorial/RateCategory;->filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_4
    new-array v0, v2, [Lcom/squareup/feetutorial/RateCategory;

    .line 25
    sget-object v2, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, Lcom/squareup/feetutorial/RateCategory;->filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static varargs filterByPresent(Lcom/squareup/settings/server/PaymentSettings;[Lcom/squareup/feetutorial/RateCategory;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/PaymentSettings;",
            "[",
            "Lcom/squareup/feetutorial/RateCategory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RateCategory;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p1, v3

    .line 44
    sget-object v5, Lcom/squareup/feetutorial/RateCategory$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {v4}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 66
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown category "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 62
    :pswitch_0
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->hasInteracFee()Z

    move-result v5

    goto :goto_1

    .line 58
    :pswitch_1
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->hasCnpFee()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->hasCpFee()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 53
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->hasJCBFee()Z

    move-result v5

    goto :goto_1

    .line 49
    :pswitch_3
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->hasCpFee()Z

    move-result v5

    :goto_1
    if-eqz v5, :cond_1

    .line 70
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/feetutorial/RateCategory;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/feetutorial/RateCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/feetutorial/RateCategory;

    return-object p0
.end method

.method public static values()[Lcom/squareup/feetutorial/RateCategory;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/feetutorial/RateCategory;->$VALUES:[Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v0}, [Lcom/squareup/feetutorial/RateCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/feetutorial/RateCategory;

    return-object v0
.end method


# virtual methods
.method getCloseEvent()Lcom/squareup/analytics/RegisterActionName;
    .locals 3

    .line 77
    sget-object v0, Lcom/squareup/feetutorial/RateCategory$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected category: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :pswitch_0
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_INTERAC_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 90
    :pswitch_1
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_MANUAL_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 85
    :pswitch_2
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_JCB_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 83
    :pswitch_3
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_TAP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 79
    :pswitch_4
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_SWIPE_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 88
    :pswitch_5
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 3

    .line 97
    sget-object v0, Lcom/squareup/feetutorial/RateCategory$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected category: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :pswitch_0
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_INTERAC:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    .line 110
    :pswitch_1
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_MANUAL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    .line 105
    :pswitch_2
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP_JCB:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    .line 103
    :pswitch_3
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_TAP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    .line 99
    :pswitch_4
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_SWIPE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    .line 108
    :pswitch_5
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
