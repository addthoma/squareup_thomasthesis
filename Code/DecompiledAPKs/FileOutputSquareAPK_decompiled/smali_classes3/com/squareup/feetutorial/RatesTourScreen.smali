.class final Lcom/squareup/feetutorial/RatesTourScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "RatesTourScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/spot/ModalBodyScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/feetutorial/RatesTourScreenComponent;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/feetutorial/RatesTourScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/feetutorial/RatesTourScreen;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/feetutorial/RatesTourScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/feetutorial/RatesTourScreen;

    invoke-direct {v0}, Lcom/squareup/feetutorial/RatesTourScreen;-><init>()V

    sput-object v0, Lcom/squareup/feetutorial/RatesTourScreen;->INSTANCE:Lcom/squareup/feetutorial/RatesTourScreen;

    .line 107
    sget-object v0, Lcom/squareup/feetutorial/RatesTourScreen;->INSTANCE:Lcom/squareup/feetutorial/RatesTourScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/feetutorial/RatesTourScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 54
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 42
    sget v0, Lcom/squareup/feetutorial/impl/R$layout;->rates_tour_view:I

    return v0
.end method
