.class synthetic Lcom/squareup/feetutorial/RateText$1;
.super Ljava/lang/Object;
.source "RateText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feetutorial/RateText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$feetutorial$RateCategory:[I

.field static final synthetic $SwitchMap$com$squareup$protos$common$CurrencyCode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 43
    invoke-static {}, Lcom/squareup/protos/common/CurrencyCode;->values()[Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->AUD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v2}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    sget-object v3, Lcom/squareup/protos/common/CurrencyCode;->CAD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    sget-object v4, Lcom/squareup/protos/common/CurrencyCode;->GBP:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v4}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    sget-object v5, Lcom/squareup/protos/common/CurrencyCode;->JPY:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v5}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 23
    :catch_3
    invoke-static {}, Lcom/squareup/feetutorial/RateCategory;->values()[Lcom/squareup/feetutorial/RateCategory;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    :try_start_4
    sget-object v4, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v5, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v5}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v4, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v4}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    return-void
.end method
