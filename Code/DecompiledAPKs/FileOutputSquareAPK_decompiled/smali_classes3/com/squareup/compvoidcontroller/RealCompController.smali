.class public final Lcom/squareup/compvoidcontroller/RealCompController;
.super Ljava/lang/Object;
.source "RealCompController.kt"

# interfaces
.implements Lcom/squareup/compvoidcontroller/CompController;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCompController.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCompController.kt\ncom/squareup/compvoidcontroller/RealCompController\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,107:1\n713#2,10:108\n1651#2,2:118\n723#2,2:120\n1653#2:122\n725#2:123\n1651#2,3:124\n*E\n*S KotlinDebug\n*F\n+ 1 RealCompController.kt\ncom/squareup/compvoidcontroller/RealCompController\n*L\n96#1,10:108\n96#1,2:118\n96#1,2:120\n96#1:122\n96#1:123\n97#1,3:124\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BS\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J,\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u001a\u0010\u001f\u001a\u00020 2\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u001e\u0010!\u001a\u00020 *\u00020\"2\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/compvoidcontroller/RealCompController;",
        "Lcom/squareup/compvoidcontroller/CompController;",
        "compVoidTicketPaymentFactory",
        "Lcom/squareup/payment/CompVoidTicketPaymentFactory;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "res",
        "Lcom/squareup/util/Res;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "transactionComps",
        "Lcom/squareup/payment/TransactionComps;",
        "(Lcom/squareup/payment/CompVoidTicketPaymentFactory;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;)V",
        "compTicket",
        "Lcom/squareup/protos/client/IdPair;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "compDiscount",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "baseOrder",
        "Lcom/squareup/payment/OrderSnapshot;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "compTransactionTicket",
        "",
        "compAllItems",
        "Lcom/squareup/payment/Order;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionComps:Lcom/squareup/payment/TransactionComps;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/CompVoidTicketPaymentFactory;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "compVoidTicketPaymentFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionComps"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/compvoidcontroller/RealCompController;->compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    iput-object p2, p0, Lcom/squareup/compvoidcontroller/RealCompController;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/compvoidcontroller/RealCompController;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/compvoidcontroller/RealCompController;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/compvoidcontroller/RealCompController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p6, p0, Lcom/squareup/compvoidcontroller/RealCompController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object p7, p0, Lcom/squareup/compvoidcontroller/RealCompController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p8, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p9, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transactionComps:Lcom/squareup/payment/TransactionComps;

    return-void
.end method

.method private final compAllItems(Lcom/squareup/payment/Order;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 10

    .line 95
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    const-string v3, "items"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 108
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 119
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const-string v7, "cartItem"

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v8, v5, 0x1

    if-gez v5, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 120
    :cond_0
    move-object v9, v6

    check-cast v9, Lcom/squareup/checkout/CartItem;

    if-ne v5, v0, :cond_2

    .line 96
    invoke-static {v9, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_3

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_3
    move v5, v8

    goto :goto_0

    .line 123
    :cond_4
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 125
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v4, 0x1

    if-gez v4, :cond_5

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_5
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 98
    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v3

    if-nez v3, :cond_6

    .line 99
    invoke-virtual {p1, v4, p3, p2, v1}, Lcom/squareup/payment/Order;->compItem(ILcom/squareup/protos/client/Employee;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;)V

    :cond_6
    move v4, v2

    goto :goto_3

    .line 104
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->invalidate()V

    return-void
.end method


# virtual methods
.method public compTicket(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;
    .locals 3

    const-string v0, "openTicket"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compDiscount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "baseOrder"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 71
    check-cast p3, Lcom/squareup/payment/Order;

    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->res:Lcom/squareup/util/Res;

    const/4 v1, 0x1

    invoke-static {p3, p1, v0, v1}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p3

    .line 72
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    return-object v2

    .line 73
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v2

    .line 76
    :cond_1
    invoke-direct {p0, p3, p2, p4}, Lcom/squareup/compvoidcontroller/RealCompController;->compAllItems(Lcom/squareup/payment/Order;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 79
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealCompController;->features:Lcom/squareup/settings/server/Features;

    sget-object p4, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    .line 77
    invoke-virtual {p1, p3, p2}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 82
    iget-object p1, p0, Lcom/squareup/compvoidcontroller/RealCompController;->compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    invoke-virtual {p1, v1, p3}, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->create(ZLcom/squareup/payment/Order;)Lcom/squareup/payment/BillPayment;

    move-result-object p1

    const-string p2, "billPayment"

    .line 83
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealCompController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object p3, p0, Lcom/squareup/compvoidcontroller/RealCompController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-static {p1, p2, p3}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->addZeroAmountLocalTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;)V

    .line 84
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealCompController;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, p2}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->capture(Lcom/squareup/payment/BillPayment;Lio/reactivex/Scheduler;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 86
    new-instance p2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getUniqueClientId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    return-object p1

    .line 84
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Comp ticket payment must be capturable."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public compTransactionTicket(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    const-string v0, "compDiscount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 47
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transactionComps:Lcom/squareup/payment/TransactionComps;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/TransactionComps;->compAllItems(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    const-string/jumbo p2, "transaction.startSingleTenderBillPayment()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealCompController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealCompController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-static {p1, p2, v0}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->addZeroAmountLocalTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/compvoidcontroller/RealCompController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->captureLocalPaymentAndResetInSellerFlow()V

    return-void

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unable to comp an empty transaction"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 47
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must load ticket to void transaction ticket."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
