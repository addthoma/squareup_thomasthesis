.class public Lcom/squareup/gen2/Gen2DenialEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "Gen2DenialEvent.java"


# static fields
.field private static final LEARN_MORE_STRING:Ljava/lang/String; = "Learn More"

.field private static final OK_STRING:Ljava/lang/String; = "OK"

.field private static final REQUEST_A_READER_STRING:Ljava/lang/String; = "Request a Reader"

.field private static final VALUE:Ljava/lang/String; = "Gen2 Reader Denial Notice"


# instance fields
.field public final detail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/gen2/Gen2DenialDialog$Result;)V
    .locals 2

    .line 18
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Gen2 Reader Denial Notice"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/gen2/Gen2DenialEvent$1;->$SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I

    invoke-virtual {p1}, Lcom/squareup/gen2/Gen2DenialDialog$Result;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const-string p1, "Request a Reader"

    .line 27
    iput-object p1, p0, Lcom/squareup/gen2/Gen2DenialEvent;->detail:Ljava/lang/String;

    goto :goto_0

    .line 30
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Gen2DenialPopup.Result type unknown"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const-string p1, "Learn More"

    .line 24
    iput-object p1, p0, Lcom/squareup/gen2/Gen2DenialEvent;->detail:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string p1, "OK"

    .line 21
    iput-object p1, p0, Lcom/squareup/gen2/Gen2DenialEvent;->detail:Ljava/lang/String;

    :goto_0
    return-void
.end method
