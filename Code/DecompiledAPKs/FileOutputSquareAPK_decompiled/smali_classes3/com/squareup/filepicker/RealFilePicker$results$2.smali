.class final Lcom/squareup/filepicker/RealFilePicker$results$2;
.super Ljava/lang/Object;
.source "RealFilePicker.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/filepicker/RealFilePicker;->results()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "it",
        "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/filepicker/RealFilePicker$results$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/filepicker/RealFilePicker$results$2;

    invoke-direct {v0}, Lcom/squareup/filepicker/RealFilePicker$results$2;-><init>()V

    sput-object v0, Lcom/squareup/filepicker/RealFilePicker$results$2;->INSTANCE:Lcom/squareup/filepicker/RealFilePicker$results$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Lcom/squareup/filepicker/FilePickerResult;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getResultCode()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object p1, Lcom/squareup/filepicker/FilePickerResult$Cancelled;->INSTANCE:Lcom/squareup/filepicker/FilePickerResult$Cancelled;

    check-cast p1, Lcom/squareup/filepicker/FilePickerResult;

    goto :goto_1

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getData()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    sget-object p1, Lcom/squareup/filepicker/FilePickerResult$Cancelled;->INSTANCE:Lcom/squareup/filepicker/FilePickerResult$Cancelled;

    check-cast p1, Lcom/squareup/filepicker/FilePickerResult;

    goto :goto_1

    .line 44
    :cond_2
    new-instance v0, Lcom/squareup/filepicker/FilePickerResult$Selected;

    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getData()Landroid/content/Intent;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    const-string v1, "it.data!!.data!!"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/filepicker/FilePickerResult$Selected;-><init>(Landroid/net/Uri;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/filepicker/FilePickerResult;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/ActivityResultHandler$IntentResult;

    invoke-virtual {p0, p1}, Lcom/squareup/filepicker/RealFilePicker$results$2;->apply(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Lcom/squareup/filepicker/FilePickerResult;

    move-result-object p1

    return-object p1
.end method
