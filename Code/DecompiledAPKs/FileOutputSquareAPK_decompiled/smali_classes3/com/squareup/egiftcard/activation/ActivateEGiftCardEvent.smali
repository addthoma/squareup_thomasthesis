.class public abstract Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;
.super Ljava/lang/Object;
.source "ActivateEGiftCardEvent.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$InitEvent;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$DesignSelected;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$AmountSelected;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$CustomAmount;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00072\u00020\u0001:\u0008\u0005\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0007\r\u000e\u000f\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Landroid/os/Parcelable;",
        "()V",
        "toByteString",
        "Lokio/ByteString;",
        "AmountSelected",
        "BackPressed",
        "Companion",
        "CustomAmount",
        "DesignSelected",
        "EmailSelected",
        "ExitEntireFlow",
        "InitEvent",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$InitEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$DesignSelected;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$AmountSelected;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$CustomAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;-><init>()V

    return-void
.end method

.method public static final fromByteString(Lokio/ByteString;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final toByteString()Lokio/ByteString;
    .locals 1

    .line 42
    move-object v0, p0

    check-cast v0, Landroid/os/Parcelable;

    invoke-static {v0}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method
