.class public final Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ChooseDesignRecyclerAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u000fH\u0016J\"\u0010\u0012\u001a\n \u0014*\u0004\u0018\u00010\u00130\u00132\u0006\u0010\u0015\u001a\u00020\u00162\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u000fH\u0002J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u000fH\u0016J\u0018\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u000fH\u0016R0\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "(Lcom/squareup/picasso/Picasso;)V",
        "value",
        "",
        "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
        "items",
        "getItems",
        "()Ljava/util/List;",
        "setItems",
        "(Ljava/util/List;)V",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "inflate",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "parent",
        "Landroid/view/ViewGroup;",
        "res",
        "onBindViewHolder",
        "",
        "holder",
        "onCreateViewHolder",
        "viewType",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
            ">;"
        }
    .end annotation
.end field

.field private final picasso:Lcom/squareup/picasso/Picasso;


# direct methods
.method public constructor <init>(Lcom/squareup/picasso/Picasso;)V
    .locals 1

    const-string v0, "picasso"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->picasso:Lcom/squareup/picasso/Picasso;

    .line 17
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    return-void
.end method

.method private final inflate(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    .line 35
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/egiftcard/activation/ChooseDesignItem;

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ChooseDesignItem;->getOrdinal()I

    move-result p1

    return p1
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->onBindViewHolder(Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/egiftcard/activation/ChooseDesignItem;

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;->bind(Lcom/squareup/egiftcard/activation/ChooseDesignItem;Lcom/squareup/picasso/Picasso;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 29
    new-instance p2, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;

    sget v0, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_design_image:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026card_choose_design_image)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 27
    :cond_1
    new-instance p2, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$HeaderViewHolder;

    sget v0, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_design_header:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026ard_choose_design_header)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$HeaderViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;

    :goto_0
    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->items:Ljava/util/List;

    .line 20
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
