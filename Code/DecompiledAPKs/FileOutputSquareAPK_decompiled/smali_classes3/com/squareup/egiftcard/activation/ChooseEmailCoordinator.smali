.class public final Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseEmailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseEmailCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseEmailCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseEmailCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,113:1\n1103#2,7:114\n1103#2,7:121\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseEmailCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseEmailCoordinator\n*L\n62#1,7:114\n84#1,7:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0012\u0010\u0019\u001a\u00020\u000f2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0014H\u0002J&\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u00052\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001eH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00140\u00140\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseEmailScreen;",
        "(Lio/reactivex/Observable;)V",
        "backButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "doneButton",
        "Lcom/squareup/noho/NohoButton;",
        "doneClicked",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "emailEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "validEmailEntered",
        "",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "emailEntered",
        "email",
        "update",
        "state",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Factory",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private doneButton:Lcom/squareup/noho/NohoButton;

.field private final doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private emailEditText:Lcom/squareup/noho/NohoEditText;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final validEmailEntered:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->screens:Lio/reactivex/Observable;

    .line 38
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<String>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->validEmailEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 39
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$emailEntered(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEntered(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getDoneClicked$p(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 108
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_actionbarbutton:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 109
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_choose_email_edit_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    .line 110
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_done:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final emailEntered(Ljava/lang/String;)V
    .locals 2

    .line 99
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "doneButton"

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->validEmailEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_0

    .line 103
    :cond_1
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez v0, :cond_0

    const-string v1, "backButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 114
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p3}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    const-string v1, "emailEditText"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-static {v2}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$3;

    invoke-direct {v2, p0}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$3;-><init>(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$4;

    invoke-direct {v2, p0}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$4;-><init>(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;)V

    check-cast v2, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_5

    const-string v2, "doneButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    .line 121
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {v2, p0}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 86
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->validEmailEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v2, Lio/reactivex/ObservableSource;

    sget-object v3, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$6;->INSTANCE:Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$6;

    check-cast v3, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v2, v3}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v2, "doneClicked\n        .wit\u2026email: String -> email })"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$7;

    invoke-direct {v2, p3}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$update$7;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 90
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;->getMaybePresetEmail()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 91
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;->getMaybePresetEmail()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :cond_7
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->emailEntered(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$attach$1;-><init>(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
