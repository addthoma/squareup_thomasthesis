.class public final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardEventsRelay.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0005R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;",
        "",
        "()V",
        "events",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
        "getEvents",
        "()Lio/reactivex/Observable;",
        "relay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "sendEvent",
        "",
        "event",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final events:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
            ">;"
        }
    .end annotation
.end field

.field private final relay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->relay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 16
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->relay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->events:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public final getEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->events:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final sendEvent(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->relay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
