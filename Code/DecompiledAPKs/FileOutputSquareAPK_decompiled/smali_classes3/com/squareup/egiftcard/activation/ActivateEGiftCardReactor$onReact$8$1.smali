.class final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
        "it",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    iget-object v1, v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    invoke-virtual {v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    iget-object v2, v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    invoke-virtual {v2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;->getDesignId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    iget-object v3, v3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    iget-object v4, v4, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v4, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    invoke-virtual {v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;->getEmail()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8$1;->invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
