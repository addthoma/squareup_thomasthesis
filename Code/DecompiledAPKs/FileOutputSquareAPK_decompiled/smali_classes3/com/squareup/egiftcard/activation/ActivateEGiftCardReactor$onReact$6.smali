.class final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;
.super Lkotlin/jvm/internal/Lambda;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->onReact(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/workflow/legacy/FinishWith<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "finishSuccess",
        "Lcom/squareup/workflow/legacy/FinishWith;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/workflow/legacy/FinishWith;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/FinishWith<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
            ">;"
        }
    .end annotation

    .line 97
    new-instance v0, Lcom/squareup/workflow/legacy/FinishWith;

    new-instance v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    invoke-virtual {v2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getEGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;-><init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;->invoke()Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object v0

    return-object v0
.end method
