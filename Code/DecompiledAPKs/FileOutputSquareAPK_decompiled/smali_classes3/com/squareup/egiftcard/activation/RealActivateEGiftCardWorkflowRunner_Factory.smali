.class public final Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealActivateEGiftCardWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
            ">;)",
            "Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;)Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;

    invoke-static {v0, v1, v2}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;)Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner_Factory;->get()Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
