.class public final Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory;
.super Ljava/lang/Object;
.source "NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notifications/PendingPaymentNotifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory$InstanceHolder;->access$000()Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideNoPendingPaymentsNotifier()Lcom/squareup/notifications/PendingPaymentNotifier;
    .locals 2

    .line 22
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule;->provideNoPendingPaymentsNotifier()Lcom/squareup/notifications/PendingPaymentNotifier;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notifications/PendingPaymentNotifier;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notifications/PendingPaymentNotifier;
    .locals 1

    .line 14
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory;->provideNoPendingPaymentsNotifier()Lcom/squareup/notifications/PendingPaymentNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoPendingPaymentsNotifierFactory;->get()Lcom/squareup/notifications/PendingPaymentNotifier;

    move-result-object v0

    return-object v0
.end method
