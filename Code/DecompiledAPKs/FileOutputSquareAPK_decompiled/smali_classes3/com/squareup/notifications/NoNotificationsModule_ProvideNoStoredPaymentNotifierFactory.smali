.class public final Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory;
.super Ljava/lang/Object;
.source "NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notifications/StoredPaymentNotifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory$InstanceHolder;->access$000()Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideNoStoredPaymentNotifier()Lcom/squareup/notifications/StoredPaymentNotifier;
    .locals 2

    .line 22
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule;->provideNoStoredPaymentNotifier()Lcom/squareup/notifications/StoredPaymentNotifier;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notifications/StoredPaymentNotifier;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notifications/StoredPaymentNotifier;
    .locals 1

    .line 14
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory;->provideNoStoredPaymentNotifier()Lcom/squareup/notifications/StoredPaymentNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/notifications/NoNotificationsModule_ProvideNoStoredPaymentNotifierFactory;->get()Lcom/squareup/notifications/StoredPaymentNotifier;

    move-result-object v0

    return-object v0
.end method
