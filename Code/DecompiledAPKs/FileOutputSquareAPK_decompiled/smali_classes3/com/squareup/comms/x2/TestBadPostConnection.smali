.class public Lcom/squareup/comms/x2/TestBadPostConnection;
.super Ljava/lang/Object;
.source "TestBadPostConnection.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBusConnection;


# instance fields
.field private posted:Z

.field public subject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/x2/TestBadPostConnection;->subject:Lrx/subjects/PublishSubject;

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/squareup/comms/x2/TestBadPostConnection;->posted:Z

    return-void
.end method


# virtual methods
.method public observable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 18
    iget-boolean v0, p0, Lcom/squareup/comms/x2/TestBadPostConnection;->posted:Z

    if-nez v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/squareup/comms/x2/TestBadPostConnection;->subject:Lrx/subjects/PublishSubject;

    return-object v0

    .line 19
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not start observing if already posted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public post(Lcom/squareup/wire/Message;)V
    .locals 0

    const/4 p1, 0x1

    .line 14
    iput-boolean p1, p0, Lcom/squareup/comms/x2/TestBadPostConnection;->posted:Z

    return-void
.end method
