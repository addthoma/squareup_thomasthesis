.class public final Lcom/squareup/comms/x2/X2RemoteBusBuilders;
.super Ljava/lang/Object;
.source "X2RemoteBusBuilders.java"


# static fields
.field public static final ANDROID_EMULATOR_HOST_IP:Ljava/lang/String; = "10.0.2.2"

.field public static final GENYMOTION_HOST_IP:Ljava/lang/String; = "10.0.3.2"

.field public static final LOCALHOST_IP:Ljava/lang/String; = "127.0.0.1"

.field private static final PORT:I = 0x2711

.field private static final USB_BFD_HOSTNAME:Ljava/lang/String; = "bfd.local"

.field private static final USB_SFD_HOSTNAME:Ljava/lang/String; = "sfd.local"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createClientIpBus(Ljava/lang/String;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 2

    const/16 v0, 0x2711

    const/4 v1, 0x0

    .line 36
    invoke-static {p0, v0, v1}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->forClient(Ljava/lang/String;ILjava/lang/String;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    move-result-object p0

    return-object p0
.end method

.method public static createClientUsbBus(Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 3

    const-string v0, "bfd.local"

    const/16 v1, 0x2711

    const-string v2, "sfd.local"

    .line 54
    invoke-static {v0, v1, v2, p0}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->forClient(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    move-result-object p0

    return-object p0
.end method

.method public static createNoopBus()Lcom/squareup/comms/RemoteBus;
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/comms/x2/X2RemoteBusBuilders$1;

    invoke-direct {v0}, Lcom/squareup/comms/x2/X2RemoteBusBuilders$1;-><init>()V

    return-object v0
.end method

.method public static createServerIpBus()Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x2711

    .line 44
    invoke-static {v0, v1}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->forServer(Ljava/lang/String;I)Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static createServerUsbBus()Lcom/squareup/comms/x2/X2RemoteBusBuilder;
    .locals 2

    const-string v0, "bfd.local"

    const/16 v1, 0x2711

    .line 63
    invoke-static {v0, v1}, Lcom/squareup/comms/x2/X2RemoteBusBuilder;->forServer(Ljava/lang/String;I)Lcom/squareup/comms/x2/X2RemoteBusBuilder;

    move-result-object v0

    return-object v0
.end method
