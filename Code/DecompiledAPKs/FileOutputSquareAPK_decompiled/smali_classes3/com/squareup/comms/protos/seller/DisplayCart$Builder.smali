.class public final Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayCart.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayCart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayCart;",
        "Lcom/squareup/comms/protos/seller/DisplayCart$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0008\u0010\u0012\u001a\u00020\u0002H\u0016J\u0015\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0013J\u0014\u0010\u000b\u001a\u00020\u00002\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cJ\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0007J\u0015\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0014R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayCart$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayCart;",
        "()V",
        "banner",
        "Lcom/squareup/comms/protos/seller/DisplayBanner;",
        "enable_bran_cart_scroll_logging",
        "",
        "Ljava/lang/Boolean;",
        "has_return",
        "is_card_still_inserted",
        "items",
        "",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "offline_mode",
        "total_amount",
        "",
        "Ljava/lang/Long;",
        "build",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public banner:Lcom/squareup/comms/protos/seller/DisplayBanner;

.field public enable_bran_cart_scroll_logging:Ljava/lang/Boolean;

.field public has_return:Ljava/lang/Boolean;

.field public is_card_still_inserted:Ljava/lang/Boolean;

.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field public offline_mode:Ljava/lang/Boolean;

.field public total_amount:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 147
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 149
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final banner(Lcom/squareup/comms/protos/seller/DisplayBanner;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->banner:Lcom/squareup/comms/protos/seller/DisplayBanner;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/DisplayCart;
    .locals 10

    .line 211
    new-instance v9, Lcom/squareup/comms/protos/seller/DisplayCart;

    .line 212
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->items:Ljava/util/List;

    .line 213
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->banner:Lcom/squareup/comms/protos/seller/DisplayBanner;

    .line 214
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->total_amount:Ljava/lang/Long;

    .line 215
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->offline_mode:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 216
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->is_card_still_inserted:Ljava/lang/Boolean;

    .line 217
    iget-object v6, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->has_return:Ljava/lang/Boolean;

    .line 218
    iget-object v7, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->enable_bran_cart_scroll_logging:Ljava/lang/Boolean;

    .line 219
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    .line 211
    invoke-direct/range {v0 .. v8}, Lcom/squareup/comms/protos/seller/DisplayCart;-><init>(Ljava/util/List;Lcom/squareup/comms/protos/seller/DisplayBanner;Ljava/lang/Long;ZLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "offline_mode"

    aput-object v2, v1, v0

    .line 215
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayCart;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final enable_bran_cart_scroll_logging(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->enable_bran_cart_scroll_logging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final has_return(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->has_return:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_card_still_inserted(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->is_card_still_inserted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final items(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/DisplayCart$Builder;"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 171
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->items:Ljava/util/List;

    return-object p0
.end method

.method public final offline_mode(Z)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 186
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->offline_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final total_amount(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->total_amount:Ljava/lang/Long;

    return-object p0
.end method
