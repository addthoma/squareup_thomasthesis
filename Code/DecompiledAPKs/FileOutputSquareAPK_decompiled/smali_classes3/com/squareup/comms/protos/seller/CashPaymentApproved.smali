.class public final Lcom/squareup/comms/protos/seller/CashPaymentApproved;
.super Lcom/squareup/wire/AndroidMessage;
.source "CashPaymentApproved.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;,
        Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCashPaymentApproved.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CashPaymentApproved.kt\ncom/squareup/comms/protos/seller/CashPaymentApproved\n*L\n1#1,164:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0014\u0015B#\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ&\u0010\n\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0002H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;",
        "receipt_options",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "amount_covered",
        "",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion;


# instance fields
.field public final amount_covered:J
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.ReceiptOptions#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->Companion:Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion;

    .line 121
    new-instance v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion$ADAPTER$1;

    .line 122
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 123
    const-class v2, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 161
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-wide p2, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    .line 35
    check-cast p1, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    .line 46
    sget-object p4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;-><init>(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/CashPaymentApproved;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/CashPaymentApproved;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 83
    iget-object p1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 84
    iget-wide p2, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object p4

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->copy(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/CashPaymentApproved;
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;-><init>(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 57
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 58
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    iget-wide v5, p1, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 65
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 68
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 69
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;
    .locals 3

    .line 49
    new-instance v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 51
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->amount_covered:Ljava/lang/Long;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->newBuilder()Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 77
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receipt_options="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_0
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "amount_covered="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;->amount_covered:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 79
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "CashPaymentApproved{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
