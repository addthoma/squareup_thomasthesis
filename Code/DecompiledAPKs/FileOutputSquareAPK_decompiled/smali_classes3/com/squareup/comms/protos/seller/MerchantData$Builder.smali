.class public final Lcom/squareup/comms/protos/seller/MerchantData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/MerchantData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/MerchantData;",
        "Lcom/squareup/comms/protos/seller/MerchantData$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0014\u0010\u0004\u001a\u00020\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/MerchantData$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/MerchantData;",
        "()V",
        "background_images",
        "",
        "Lokio/ByteString;",
        "merchant_name",
        "",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public background_images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field public merchant_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 90
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->background_images:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final background_images(Ljava/util/List;)Lcom/squareup/comms/protos/seller/MerchantData$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lokio/ByteString;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/MerchantData$Builder;"
        }
    .end annotation

    const-string v0, "background_images"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->background_images:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/MerchantData;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/comms/protos/seller/MerchantData;

    .line 107
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->merchant_name:Ljava/lang/String;

    .line 108
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->background_images:Ljava/util/List;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 106
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/seller/MerchantData;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->build()Lcom/squareup/comms/protos/seller/MerchantData;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final merchant_name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/MerchantData$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/MerchantData$Builder;->merchant_name:Ljava/lang/String;

    return-object p0
.end method
