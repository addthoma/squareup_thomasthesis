.class public final Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
.super Lcom/squareup/wire/AndroidMessage;
.source "EmvPaymentSuccessResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;,
        Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmvPaymentSuccessResult.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmvPaymentSuccessResult.kt\ncom/squareup/comms/protos/seller/EmvPaymentSuccessResult\n*L\n1#1,266:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u001a2\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0019\u001aBG\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u000eJJ\u0010\u000f\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u0004J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0096\u0002J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0002H\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016R\u0010\u0010\u000b\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;",
        "arpc",
        "Lokio/ByteString;",
        "tip_requirements",
        "Lcom/squareup/comms/protos/seller/TipRequirements;",
        "signature_requirements",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements;",
        "receipt_options",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "amount_covered",
        "",
        "unknownFields",
        "(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion;


# instance fields
.field public final amount_covered:J
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation
.end field

.field public final arpc:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.ReceiptOptions#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.SignatureRequirements#ADAPTER"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.TipRequirements#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->Companion:Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion;

    .line 205
    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;

    .line 207
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 208
    const-class v2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 263
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    iput-object p2, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iput-object p3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iput-object p4, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-wide p5, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    return-void
.end method

.method public synthetic constructor <init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 35
    move-object v0, v1

    check-cast v0, Lokio/ByteString;

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p1

    :goto_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_1

    .line 44
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/TipRequirements;

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p2

    :goto_1
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_2

    .line 54
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/SignatureRequirements;

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, p3

    :goto_2
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    .line 63
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    move-object v6, v0

    goto :goto_3

    :cond_3
    move-object v6, p4

    :goto_3
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_4

    .line 75
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v9, v0

    goto :goto_4

    :cond_4
    move-object/from16 v9, p7

    :goto_4
    move-object v2, p0

    move-wide v7, p5

    invoke-direct/range {v2 .. v9}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    .line 125
    iget-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    .line 126
    iget-object p2, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    .line 127
    iget-object p3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    .line 128
    iget-object p4, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    .line 129
    iget-wide p5, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    :cond_4
    move-wide v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    .line 130
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object p7

    :cond_5
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-wide p7, v2

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->copy(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
    .locals 9

    const-string/jumbo v0, "unknownFields"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 90
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 91
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    iget-wide v5, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 101
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 103
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 3

    .line 79
    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->arpc:Lokio/ByteString;

    .line 81
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    .line 82
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    .line 83
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 84
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->amount_covered:Ljava/lang/Long;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->newBuilder()Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 116
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "arpc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tip_requirements="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    const-string v2, "signature_requirements=\u2588\u2588"

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receipt_options="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_3
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "amount_covered="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 121
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "EmvPaymentSuccessResult{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
