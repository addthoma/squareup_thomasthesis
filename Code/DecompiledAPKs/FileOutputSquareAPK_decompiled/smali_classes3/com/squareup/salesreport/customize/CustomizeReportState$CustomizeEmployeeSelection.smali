.class public abstract Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;
.super Lcom/squareup/salesreport/customize/CustomizeReportState;
.source "CustomizeReportState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/customize/CustomizeReportState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CustomizeEmployeeSelection"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadingEmployeeSelection;,
        Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "(Lcom/squareup/customreport/data/ReportConfig;)V",
        "getReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "LoadedEmployeeSelection",
        "LoadingEmployeeSelection",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadingEmployeeSelection;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;


# direct methods
.method private constructor <init>(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, v0}, Lcom/squareup/salesreport/customize/CustomizeReportState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;-><init>(Lcom/squareup/customreport/data/ReportConfig;)V

    return-void
.end method


# virtual methods
.method public getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method
