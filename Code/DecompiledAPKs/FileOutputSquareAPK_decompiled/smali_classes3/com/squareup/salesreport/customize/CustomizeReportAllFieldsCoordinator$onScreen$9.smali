.class final Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;
.super Ljava/lang/Object;
.source "CustomizeReportAllFieldsCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field final synthetic $screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;


# direct methods
.method constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;->$reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 196
    sget p1, Lcom/squareup/salesreport/impl/R$id;->config_this_device:I

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 197
    :goto_0
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;->$reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {p2}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result p2

    if-eq p1, p2, :cond_1

    .line 198
    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$9;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    new-instance p3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;

    invoke-direct {p3, p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;-><init>(Z)V

    invoke-interface {p2, p3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method
