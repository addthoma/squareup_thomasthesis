.class public final Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;
.super Ljava/lang/Object;
.source "CustomizeReportAllFieldsCoordinator.kt"

# interfaces
.implements Lcom/squareup/register/widgets/date/DatePickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005H\u0016J\u0018\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8",
        "Lcom/squareup/register/widgets/date/DatePickerListener;",
        "onDaySelected",
        "",
        "year",
        "",
        "month",
        "day",
        "onMonthChange",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field final synthetic $screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->this$0:Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->$reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDaySelected(III)V
    .locals 4

    .line 178
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->this$0:Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;

    .line 179
    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    .line 180
    iget-object v2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->$reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 181
    iget-object v3, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$8;->$reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    add-int/lit8 p2, p2, 0x1

    .line 183
    invoke-static {p1, p2, p3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string p2, "LocalDate.of(year, month + 1, day)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->access$handleDateRangeSelection(Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public onMonthChange(II)V
    .locals 0

    return-void
.end method
