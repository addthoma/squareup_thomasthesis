.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getCustomizeAllFieldsCardScreen(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->invoke(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iget-object v2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    invoke-static {v1, p1, v2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->access$handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
