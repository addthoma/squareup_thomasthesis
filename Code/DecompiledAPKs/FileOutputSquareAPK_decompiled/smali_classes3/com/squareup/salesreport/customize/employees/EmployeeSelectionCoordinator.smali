.class public final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EmployeeSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;,
        Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmployeeSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,188:1\n49#2:189\n50#2,3:195\n53#2:219\n599#3,4:190\n601#3:194\n100#4:198\n114#4,5:199\n120#4:215\n101#4:216\n328#5:204\n342#5,5:205\n344#5,4:210\n329#5:214\n43#6,2:217\n*E\n*S KotlinDebug\n*F\n+ 1 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator\n*L\n168#1:189\n168#1,3:195\n168#1:219\n168#1,4:190\n168#1:194\n168#1:198\n168#1,5:199\n168#1:215\n168#1:216\n168#1:204\n168#1,5:205\n168#1,4:210\n168#1:214\n168#1,2:217\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0002\u001f B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0018\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "employeeRowClicked",
        "Lio/reactivex/subjects/BehaviorSubject;",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
        "employeeSelectionAnimator",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onScreen",
        "screen",
        "EmployeeFilterRow",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final employeeRowClicked:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
            ">;"
        }
    .end annotation
.end field

.field private employeeSelectionAnimator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;",
            ">;"
        }
    .end annotation
.end field

.field private searchBar:Lcom/squareup/noho/NohoEditRow;


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p4, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->res:Lcom/squareup/util/Res;

    .line 60
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object p1

    const-string p2, "BehaviorSubject.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeRowClicked:Lio/reactivex/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getEmployeeRowClicked$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lio/reactivex/subjects/BehaviorSubject;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeRowClicked:Lio/reactivex/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getEmployeeSelectionAnimator$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lcom/squareup/widgets/SquareViewAnimator;
    .locals 1

    .line 46
    iget-object p0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeSelectionAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p0, :cond_0

    const-string v0, "employeeSelectionAnimator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;Landroid/view/View;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$setEmployeeSelectionAnimator$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;Lcom/squareup/widgets/SquareViewAnimator;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeSelectionAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 5

    .line 147
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 148
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.e\u2026loyee_selection_animator)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 149
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_search:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.employee_selection_search)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 150
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_0

    const-string v1, "searchBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedShortText(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 154
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "searchBar\n        .debou\u2026ty text.\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$1;->INSTANCE:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 160
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeRowClicked:Lio/reactivex/subjects/BehaviorSubject;

    check-cast v0, Lio/reactivex/Observable;

    .line 161
    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 162
    sget-object v1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$2;->INSTANCE:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 166
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_list_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.e\u2026_selection_list_animator)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->employeeSelectionAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 168
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    sget v1, Lcom/squareup/salesreport/impl/R$id;->employee_selection_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v1, "view.findViewById(R.id.e\u2026_selection_recycler_view)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 189
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 190
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 191
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 195
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 196
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 169
    sget-object v0, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    check-cast v0, Lcom/squareup/noho/CheckType;

    .line 198
    new-instance v2, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;

    invoke-direct {v2, p0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 200
    new-instance v3, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$2;

    invoke-direct {v3, v0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$2;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 206
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 210
    sget-object v4, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$3;->INSTANCE:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 206
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 204
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 209
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 205
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 217
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0x8

    .line 178
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 179
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 217
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 193
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 190
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;Landroid/view/View;)V
    .locals 12

    .line 91
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 95
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    .line 115
    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 98
    :cond_0
    new-instance v2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 99
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->customize_report_employee_filter_title:I

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 100
    sget-object v3, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v4, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$2;

    invoke-direct {v4, v0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v5

    .line 102
    sget-object v6, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    if-eqz p2, :cond_1

    .line 104
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->customize_report_employee_filter_deselect_all:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    goto :goto_0

    .line 106
    :cond_1
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->customize_report_employee_filter_select_all:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 103
    :goto_0
    move-object v7, v2

    check-cast v7, Lcom/squareup/resources/TextModel;

    const/4 v8, 0x0

    .line 108
    new-instance v2, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$3;

    invoke-direct {v2, p2, v0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$3;-><init>(ZLkotlin/jvm/functions/Function1;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function0;

    const/4 v10, 0x4

    const/4 v11, 0x0

    .line 101
    invoke-static/range {v5 .. v11}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 115
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 118
    instance-of p2, p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadingEmployeeSelection;

    const-string v0, "animator"

    if-eqz p2, :cond_3

    .line 119
    iget-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget p2, Lcom/squareup/salesreport/impl/R$id;->employee_selection_progress_bar:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    goto :goto_1

    .line 121
    :cond_3
    instance-of p2, p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    if-eqz p2, :cond_6

    .line 122
    iget-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p2, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_container:I

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 123
    iget-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p2, :cond_5

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;-><init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->bindViews(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    .line 83
    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$attach$1;-><init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
