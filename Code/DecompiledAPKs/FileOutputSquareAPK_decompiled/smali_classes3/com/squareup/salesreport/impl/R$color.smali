.class public final Lcom/squareup/salesreport/impl/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final sales_report_amount_count_toggle_background_color_selected:I = 0x7f0602aa

.field public static final sales_report_amount_count_toggle_background_color_unselected:I = 0x7f0602ab

.field public static final sales_report_amount_count_toggle_text_color_selected:I = 0x7f0602ac

.field public static final sales_report_amount_count_toggle_text_color_unselected:I = 0x7f0602ad

.field public static final sales_report_comparison_range_background:I = 0x7f0602ae

.field public static final sales_report_comparison_range_text_color:I = 0x7f0602af

.field public static final sales_report_emplyee_search_no_employees_found:I = 0x7f0602b0

.field public static final sales_report_info_color:I = 0x7f0602b1

.field public static final sales_report_payment_method_card_bar_color:I = 0x7f0602b2

.field public static final sales_report_payment_method_cash_bar_color:I = 0x7f0602b3

.field public static final sales_report_payment_method_emoney_bar_color:I = 0x7f0602b4

.field public static final sales_report_payment_method_external_bar_color:I = 0x7f0602b5

.field public static final sales_report_payment_method_fees_bar_color:I = 0x7f0602b6

.field public static final sales_report_payment_method_gift_card_bar_color:I = 0x7f0602b7

.field public static final sales_report_payment_method_installment_bar_color:I = 0x7f0602b8

.field public static final sales_report_payment_method_other_bar_color:I = 0x7f0602b9

.field public static final sales_report_payment_method_split_tender_bar_color:I = 0x7f0602ba

.field public static final sales_report_payment_method_third_party_card_bar_color:I = 0x7f0602bb

.field public static final sales_report_payment_method_zero_amount_bar_color:I = 0x7f0602bc

.field public static final sales_report_prominent_edge_color:I = 0x7f0602bd

.field public static final sales_report_summary_details_row_text_color:I = 0x7f0602be

.field public static final sales_report_summary_details_subrow_text_color:I = 0x7f0602bf

.field public static final sales_report_summary_negative_percentage_color:I = 0x7f0602c0

.field public static final sales_report_summary_positive_percentage_color:I = 0x7f0602c1

.field public static final sales_report_time_selector_background:I = 0x7f0602c2

.field public static final sales_report_top_bar_comparison_icon_color:I = 0x7f0602c3

.field public static final sales_report_top_bar_comparison_icon_disabled:I = 0x7f0602c4

.field public static final sales_report_top_bar_comparison_icon_enabled:I = 0x7f0602c5

.field public static final sales_report_top_bar_comparison_icon_selected:I = 0x7f0602c6

.field public static final sales_report_top_bar_title_text_color:I = 0x7f0602c7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
