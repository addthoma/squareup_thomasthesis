.class public Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;
.super Ljava/lang/Object;
.source "RealSalesReportPrintingDispatcher.java"

# interfaces
.implements Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;


# static fields
.field private static final PRINT_JOB_SOURCE:Ljava/lang/String; = "SalesReport"


# instance fields
.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 25
    iput-object p2, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 26
    iput-object p3, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public canPrintSalesReport()Z
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method public print(Lcom/squareup/salesreport/print/SalesReportPayload;)V
    .locals 5

    .line 34
    iget-object v0, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/salesreport/print/impl/R$string;->sales_report_print_job_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v1

    .line 36
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 37
    iget-object v3, p0, Lcom/squareup/salesreport/print/RealSalesReportPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    const-string v4, "SalesReport"

    invoke-virtual {v3, v2, p1, v0, v4}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method
