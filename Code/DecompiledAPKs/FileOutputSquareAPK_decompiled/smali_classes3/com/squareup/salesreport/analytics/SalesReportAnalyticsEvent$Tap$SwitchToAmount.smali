.class public abstract Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;
.super Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SwitchToAmount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToItemAmount;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToCategoryAmount;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;",
        "source",
        "",
        "(Ljava/lang/String;)V",
        "getSource",
        "()Ljava/lang/String;",
        "SwitchToCategoryAmount",
        "SwitchToItemAmount",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToItemAmount;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToCategoryAmount;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final source:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 338
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;->source:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 338
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getSource()Ljava/lang/String;
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;->source:Ljava/lang/String;

    return-object v0
.end method
