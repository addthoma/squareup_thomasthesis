.class public abstract Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action;
.super Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportEmailed;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportPrinted;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;",
        "registerActionName",
        "Lcom/squareup/analytics/RegisterActionName;",
        "(Lcom/squareup/analytics/RegisterActionName;)V",
        "SalesReportEmailed",
        "SalesReportPrinted",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportEmailed;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportPrinted;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 2

    .line 408
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "registerActionName.value"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 407
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/analytics/RegisterActionName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 407
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method
