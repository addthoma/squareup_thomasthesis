.class public final Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;
.super Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowAllItems"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 358
    new-instance v0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;

    invoke-direct {v0}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "items"

    const/4 v1, 0x0

    .line 358
    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
