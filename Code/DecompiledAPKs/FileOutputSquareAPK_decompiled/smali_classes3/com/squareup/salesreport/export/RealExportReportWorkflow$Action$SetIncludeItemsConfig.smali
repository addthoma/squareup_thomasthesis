.class public final Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;
.super Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
.source "ExportReportWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SetIncludeItemsConfig"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\u000f\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c2\u0003J\t\u0010\u0008\u001a\u00020\u0004H\u00c2\u0003J#\u0010\t\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u00042\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u0012*\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0016\u00a2\u0006\u0002\u0010\u0015R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;",
        "includeItemsInReportLocalSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "newValue",
        "(Lcom/squareup/settings/LocalSetting;Z)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final newValue:Z


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "includeItemsInReportLocalSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 209
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    iput-boolean p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    return-void
.end method

.method private final component1()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    return-object v0
.end method

.method private final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;Lcom/squareup/settings/LocalSetting;ZILjava/lang/Object;)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->copy(Lcom/squareup/settings/LocalSetting;Z)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    iget-boolean v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public final copy(Lcom/squareup/settings/LocalSetting;Z)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;Z)",
            "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;"
        }
    .end annotation

    const-string v0, "includeItemsInReportLocalSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;

    invoke-direct {v0, p1, p2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;-><init>(Lcom/squareup/settings/LocalSetting;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    iget-object v1, p1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    iget-boolean p1, p1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetIncludeItemsConfig(includeItemsInReportLocalSetting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;->newValue:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
