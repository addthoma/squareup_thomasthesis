.class public final Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "EmailReportCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 45
    new-instance v6, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iget-object v2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator_Factory_Factory;->get()Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
