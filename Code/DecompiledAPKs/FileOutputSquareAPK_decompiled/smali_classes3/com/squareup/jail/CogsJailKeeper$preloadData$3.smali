.class final Lcom/squareup/jail/CogsJailKeeper$preloadData$3;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jail/CogsJailKeeper;->preloadData(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "throwable",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jail/CogsJailKeeper;


# direct methods
.method constructor <init>(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 72
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;->accept(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final accept(Ljava/lang/Throwable;)V
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->FAILED:Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-static {v0, v1}, Lcom/squareup/jail/CogsJailKeeper;->access$setState$p(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/jailkeeper/JailKeeper$State;)V

    .line 348
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper$preloadData$3;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    const-string/jumbo v1, "throwable"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/jail/CogsJailKeeper;->access$propagateCatalogException(Lcom/squareup/jail/CogsJailKeeper;Ljava/lang/Throwable;)V

    return-void
.end method
