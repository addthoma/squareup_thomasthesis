.class public Lcom/squareup/jail/JailView;
.super Landroid/widget/LinearLayout;
.source "JailView.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field apiRequestController:Lcom/squareup/api/ApiRequestController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private loadingText:Lcom/squareup/marketfont/MarketTextView;

.field private networkButton:Landroid/widget/Button;

.field presenter:Lcom/squareup/jail/JailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressCircle:Lcom/squareup/jail/DeterminateProgressView;

.field private progressContainer:Landroid/view/View;

.field private progressText:Landroid/widget/TextView;

.field private retryButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/jail/JailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/jail/JailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/jail/JailScreen$Component;->inject(Lcom/squareup/jail/JailView;)V

    return-void
.end method

.method static synthetic lambda$showNetworkButton$3(Landroid/content/Intent;Landroid/view/View;)V
    .locals 1

    .line 132
    invoke-static {p1}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p1

    .line 133
    invoke-virtual {p1, p0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 134
    sget p0, Lcom/squareup/widgets/R$anim;->slide_in_bottom_opaque:I

    sget v0, Lcom/squareup/widgets/R$anim;->slide_out_bottom_opaque:I

    invoke-virtual {p1, p0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method


# virtual methods
.method hideNetworkButton()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/jail/JailView;->networkButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/squareup/jail/JailView;->networkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$null$1$JailView(Ljava/lang/Float;)V
    .locals 2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/jail/JailView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/jail/R$string;->loading_register_sync_percent:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 93
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float p1, p1, v1

    float-to-int p1, p1

    const-string v1, "percent"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 95
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$JailView()Lrx/Subscription;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressCircle:Lcom/squareup/jail/DeterminateProgressView;

    invoke-virtual {v0}, Lcom/squareup/jail/DeterminateProgressView;->observeProgressPercentage()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/jail/-$$Lambda$JailView$APOBZ1d1xBXI_gREATimZs8Wve0;

    invoke-direct {v1, p0}, Lcom/squareup/jail/-$$Lambda$JailView$APOBZ1d1xBXI_gREATimZs8Wve0;-><init>(Lcom/squareup/jail/JailView;)V

    .line 89
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$JailView()Lkotlin/Unit;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/jail/JailView;->presenter:Lcom/squareup/jail/JailPresenter;

    invoke-virtual {v0}, Lcom/squareup/jail/JailPresenter;->exit()Lkotlin/Unit;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 85
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 87
    new-instance v0, Lcom/squareup/jail/-$$Lambda$JailView$S6iApVcZPSqypDVbSS6nBym61bI;

    invoke-direct {v0, p0}, Lcom/squareup/jail/-$$Lambda$JailView$S6iApVcZPSqypDVbSS6nBym61bI;-><init>(Lcom/squareup/jail/JailView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/jail/JailView;->presenter:Lcom/squareup/jail/JailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/jail/JailPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/jail/JailView;->presenter:Lcom/squareup/jail/JailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/jail/JailPresenter;->dropView(Ljava/lang/Object;)V

    .line 104
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 53
    sget v0, Lcom/squareup/jail/R$id;->jail_progress_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/jail/JailView;->progressContainer:Landroid/view/View;

    .line 54
    sget v0, Lcom/squareup/jail/R$id;->jail_loading_percentage_circle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/jail/DeterminateProgressView;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->progressCircle:Lcom/squareup/jail/DeterminateProgressView;

    .line 55
    sget v0, Lcom/squareup/jail/R$id;->jail_loading_percentage_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->progressText:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/squareup/jail/R$id;->jail_loading_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->loadingText:Lcom/squareup/marketfont/MarketTextView;

    .line 57
    iget-object v0, p0, Lcom/squareup/jail/JailView;->loadingText:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/jail/JailView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/jail/R$string;->loading_register:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    sget v0, Lcom/squareup/jail/R$id;->error:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 60
    iget-object v0, p0, Lcom/squareup/jail/JailView;->error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/jail/JailView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/jail/R$string;->loading_register_failed:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/jail/JailView;->error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget v1, Lcom/squareup/jail/R$string;->loading_register_failed_description:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(I)V

    .line 63
    sget v0, Lcom/squareup/jail/R$id;->retry_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->retryButton:Landroid/widget/Button;

    .line 64
    iget-object v0, p0, Lcom/squareup/jail/JailView;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->isApiRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/squareup/ui/main/PosContainer;->Companion:Lcom/squareup/ui/main/PosContainer$Companion;

    invoke-virtual {p0}, Lcom/squareup/jail/JailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeCardBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/jail/JailView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/jail/JailView;->retryButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/jail/JailView$1;

    invoke-direct {v1, p0}, Lcom/squareup/jail/JailView$1;-><init>(Lcom/squareup/jail/JailView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    sget v0, Lcom/squareup/jail/R$id;->network_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->networkButton:Landroid/widget/Button;

    .line 76
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/jail/JailView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 77
    iget-object v0, p0, Lcom/squareup/jail/JailView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/jail/-$$Lambda$JailView$9AoqsJZxblqWIhK1m1YyM3G5EZM;

    invoke-direct {v3, p0}, Lcom/squareup/jail/-$$Lambda$JailView$9AoqsJZxblqWIhK1m1YyM3G5EZM;-><init>(Lcom/squareup/jail/JailView;)V

    .line 78
    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/jail/R$string;->loading_register_failed_screen_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    .line 79
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 80
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/jail/JailView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    return-void
.end method

.method resetProgress()V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressCircle:Lcom/squareup/jail/DeterminateProgressView;

    invoke-virtual {v0}, Lcom/squareup/jail/DeterminateProgressView;->resetProgress()V

    return-void
.end method

.method setProgress(IJ)V
    .locals 6

    .line 108
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressCircle:Lcom/squareup/jail/DeterminateProgressView;

    const-wide/16 v4, 0x320

    move v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jail/DeterminateProgressView;->setProgress(IJJ)V

    return-void
.end method

.method showNetworkButton(Landroid/content/Intent;)V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/jail/JailView;->networkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/jail/JailView;->networkButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/jail/-$$Lambda$JailView$WjT3GxEzcMl8wAkT9jJpgbi8RB8;

    invoke-direct {v1, p1}, Lcom/squareup/jail/-$$Lambda$JailView$WjT3GxEzcMl8wAkT9jJpgbi8RB8;-><init>(Landroid/content/Intent;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method syncFailed()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/jail/JailView;->error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/squareup/jail/JailView;->presenter:Lcom/squareup/jail/JailPresenter;

    invoke-virtual {v0}, Lcom/squareup/jail/JailPresenter;->shouldShowActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/jail/JailView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method syncInProgress()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/jail/JailView;->progressContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/squareup/jail/JailView;->error:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/squareup/jail/JailView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    return-void
.end method
