.class Lcom/squareup/container/SquarePathContainer$1;
.super Lcom/squareup/util/RunnableOnce;
.source "SquarePathContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/SquarePathContainer;->changeChild(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/SquarePathContainer;

.field final synthetic val$addedBackground:Z

.field final synthetic val$alsoPreserve:[Lflow/path/PathContext;

.field final synthetic val$callback:Lflow/TraversalCallback;

.field final synthetic val$containerView:Landroid/view/ViewGroup;

.field final synthetic val$newPathContext:Lflow/path/PathContext;

.field final synthetic val$oldPathContext:Lflow/path/PathContext;

.field final synthetic val$oldView:Landroid/view/View;

.field final synthetic val$topChild:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/container/SquarePathContainer;Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Lflow/path/PathContext;Lflow/path/PathContext;[Lflow/path/PathContext;Lflow/TraversalCallback;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/container/SquarePathContainer$1;->this$0:Lcom/squareup/container/SquarePathContainer;

    iput-object p2, p0, Lcom/squareup/container/SquarePathContainer$1;->val$containerView:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/squareup/container/SquarePathContainer$1;->val$oldView:Landroid/view/View;

    iput-boolean p4, p0, Lcom/squareup/container/SquarePathContainer$1;->val$addedBackground:Z

    iput-object p5, p0, Lcom/squareup/container/SquarePathContainer$1;->val$topChild:Landroid/view/View;

    iput-object p6, p0, Lcom/squareup/container/SquarePathContainer$1;->val$oldPathContext:Lflow/path/PathContext;

    iput-object p7, p0, Lcom/squareup/container/SquarePathContainer$1;->val$newPathContext:Lflow/path/PathContext;

    iput-object p8, p0, Lcom/squareup/container/SquarePathContainer$1;->val$alsoPreserve:[Lflow/path/PathContext;

    iput-object p9, p0, Lcom/squareup/container/SquarePathContainer$1;->val$callback:Lflow/TraversalCallback;

    invoke-direct {p0}, Lcom/squareup/util/RunnableOnce;-><init>()V

    return-void
.end method


# virtual methods
.method protected runOnce()V
    .locals 4

    .line 156
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->this$0:Lcom/squareup/container/SquarePathContainer;

    invoke-static {v0}, Lcom/squareup/container/SquarePathContainer;->access$000(Lcom/squareup/container/SquarePathContainer;)Lcom/squareup/util/RunnableOnce;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->this$0:Lcom/squareup/container/SquarePathContainer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/container/SquarePathContainer;->access$102(Lcom/squareup/container/SquarePathContainer;Landroid/view/View;)Landroid/view/View;

    .line 161
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->this$0:Lcom/squareup/container/SquarePathContainer;

    invoke-static {v0, v1}, Lcom/squareup/container/SquarePathContainer;->access$002(Lcom/squareup/container/SquarePathContainer;Lcom/squareup/util/RunnableOnce;)Lcom/squareup/util/RunnableOnce;

    .line 162
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 164
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->val$containerView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/squareup/container/SquarePathContainer$1;->val$oldView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 165
    iget-boolean v0, p0, Lcom/squareup/container/SquarePathContainer$1;->val$addedBackground:Z

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->val$topChild:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->val$oldPathContext:Lflow/path/PathContext;

    iget-object v1, p0, Lcom/squareup/container/SquarePathContainer$1;->this$0:Lcom/squareup/container/SquarePathContainer;

    invoke-static {v1}, Lcom/squareup/container/SquarePathContainer;->access$200(Lcom/squareup/container/SquarePathContainer;)Lflow/path/PathContextFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/container/SquarePathContainer$1;->val$newPathContext:Lflow/path/PathContext;

    iget-object v3, p0, Lcom/squareup/container/SquarePathContainer$1;->val$alsoPreserve:[Lflow/path/PathContext;

    invoke-virtual {v0, v1, v2, v3}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer$1;->val$callback:Lflow/TraversalCallback;

    invoke-interface {v0}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 157
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "completeTraversal should not change before we clean up here."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
