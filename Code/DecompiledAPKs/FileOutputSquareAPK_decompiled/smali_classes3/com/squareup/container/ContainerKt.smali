.class public final Lcom/squareup/container/ContainerKt;
.super Ljava/lang/Object;
.source "Container.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c\u001a\u0016\u0010\r\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f\u001a\u000e\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u0002\u001a\u000e\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0002\u001a1\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\u0014\"\n\u0008\u0000\u0010\u0015\u0018\u0001*\u00020\u00162\u0014\u0008\u0004\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u0002H\u00150\u0018H\u0086\u0008\u001a\u0012\u0010\u001a\u001a\u00020\u0008*\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\u00a8\u0006\u001e"
    }
    d2 = {
        "screenSize",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "Landroid/content/Context;",
        "getScreenSize",
        "(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;",
        "Landroid/view/View;",
        "(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;",
        "addDeviceToScope",
        "",
        "scopeBuilder",
        "Lmortar/MortarScope$Builder;",
        "device",
        "Lcom/squareup/util/Device;",
        "addViewFactoryToScope",
        "viewFactory",
        "Lcom/squareup/container/ContainerViewFactory;",
        "getDevice",
        "context",
        "getViewFactory",
        "pathCreator",
        "Landroid/os/Parcelable$Creator;",
        "T",
        "Lcom/squareup/container/ContainerTreeKey;",
        "creator",
        "Lkotlin/Function1;",
        "Landroid/os/Parcel;",
        "goBackPastWorkflow",
        "Lflow/Flow;",
        "runnerServiceName",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final addDeviceToScope(Lmortar/MortarScope$Builder;Lcom/squareup/util/Device;)V
    .locals 1

    const-string v0, "scopeBuilder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    const-class v0, Lcom/squareup/util/Device;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method public static final addViewFactoryToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/ContainerViewFactory;)V
    .locals 1

    const-string v0, "scopeBuilder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    const-class v0, Lcom/squareup/container/ContainerViewFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method public static final getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    const-class v0, Lcom/squareup/util/Device;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "MortarScope.getScope(con\u2026(Device::class.java.name)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/util/Device;

    return-object p0
.end method

.method public static final getScreenSize(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 1

    const-string v0, "$this$screenSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {p0}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p0

    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 1

    const-string v0, "$this$screenSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final getViewFactory(Landroid/content/Context;)Lcom/squareup/container/ContainerViewFactory;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    const-class v0, Lcom/squareup/container/ContainerViewFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "MortarScope.getScope(con\u2026Factory::class.java.name)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/container/ContainerViewFactory;

    return-object p0
.end method

.method public static final goBackPastWorkflow(Lflow/Flow;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$goBackPastWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/container/ContainerKt$goBackPastWorkflow$1;

    invoke-direct {v0, p1}, Lcom/squareup/container/ContainerKt$goBackPastWorkflow$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/container/Flows;->goBackWhile(Lflow/Flow;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic pathCreator(Lkotlin/jvm/functions/Function1;)Landroid/os/Parcelable$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/os/Parcel;",
            "+TT;>;)",
            "Landroid/os/Parcelable$Creator<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "creator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance v0, Lcom/squareup/container/ContainerKt$pathCreator$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/ContainerKt$pathCreator$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    return-object v0
.end method
