.class public final Lcom/squareup/container/PosWorkflowStarter$DefaultImpls;
.super Ljava/lang/Object;
.source "PosWorkflowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/PosWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic start$default(Lcom/squareup/container/PosWorkflowStarter;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 19
    check-cast p1, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-interface {p0, p1}, Lcom/squareup/container/PosWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: start"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
