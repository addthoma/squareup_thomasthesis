.class public final Lcom/squareup/container/DispatchStep$Result;
.super Ljava/lang/Object;
.source "DispatchStep.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/DispatchStep;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Result"
.end annotation


# instance fields
.field final logString:Ljava/lang/String;

.field private final terminate:Z

.field private final wrappedCallback:Lflow/TraversalCallback;


# direct methods
.method private constructor <init>(Ljava/lang/String;ZLflow/TraversalCallback;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-boolean p2, p0, Lcom/squareup/container/DispatchStep$Result;->terminate:Z

    .line 100
    iput-object p3, p0, Lcom/squareup/container/DispatchStep$Result;->wrappedCallback:Lflow/TraversalCallback;

    const-string p2, "logString"

    .line 101
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/container/DispatchStep$Result;->logString:Ljava/lang/String;

    return-void
.end method

.method public static go()Lcom/squareup/container/DispatchStep$Result;
    .locals 4

    .line 60
    new-instance v0, Lcom/squareup/container/DispatchStep$Result;

    const-string v1, ""

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/container/DispatchStep$Result;-><init>(Ljava/lang/String;ZLflow/TraversalCallback;)V

    return-object v0
.end method

.method public static stop(Ljava/lang/String;)Lcom/squareup/container/DispatchStep$Result;
    .locals 3

    .line 73
    new-instance v0, Lcom/squareup/container/DispatchStep$Result;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/container/DispatchStep$Result;-><init>(Ljava/lang/String;ZLflow/TraversalCallback;)V

    return-object v0
.end method

.method public static wrap(Ljava/lang/String;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/container/DispatchStep$Result;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/container/DispatchStep$Result;-><init>(Ljava/lang/String;ZLflow/TraversalCallback;)V

    return-object v0
.end method


# virtual methods
.method redirectOrContinue(Lflow/TraversalCallback;)Lflow/TraversalCallback;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/container/DispatchStep$Result;->logString:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/squareup/container/DispatchStep$Result;->logString:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/DispatchStep$Result;->wrappedCallback:Lflow/TraversalCallback;

    if-eqz v0, :cond_1

    move-object p1, v0

    .line 89
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/container/DispatchStep$Result;->terminate:Z

    if-eqz v0, :cond_2

    .line 90
    invoke-interface {p1}, Lflow/TraversalCallback;->onTraversalCompleted()V

    const/4 p1, 0x0

    :cond_2
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/squareup/container/DispatchStep$Result;->logString:Ljava/lang/String;

    .line 106
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/container/DispatchStep$Result;->logString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/container/DispatchStep$Result;->wrappedCallback:Lflow/TraversalCallback;

    if-nez v2, :cond_1

    const-string v2, "no"

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "yes"

    :goto_1
    aput-object v2, v0, v1

    const-string v1, "Result%s{callback? %s}"

    .line 105
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
