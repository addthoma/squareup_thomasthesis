.class public final Lcom/squareup/container/RedirectPipelineFactoryKt;
.super Ljava/lang/Object;
.source "RedirectPipelineFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedirectPipelineFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedirectPipelineFactory.kt\ncom/squareup/container/RedirectPipelineFactoryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,320:1\n250#2,2:321\n*E\n*S KotlinDebug\n*F\n+ 1 RedirectPipelineFactory.kt\ncom/squareup/container/RedirectPipelineFactoryKt\n*L\n283#1,2:321\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0002\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\t2\u0006\u0010\u0008\u001a\u00020\tH\u0002\u001a\u001c\u0010\n\u001a\u00020\t*\u000c\u0012\u0004\u0012\u00020\u000c0\u000bj\u0002`\r2\u0006\u0010\u0008\u001a\u00020\t\u001a\u001e\u0010\u000e\u001a\u00020\u000f*\u000c\u0012\u0004\u0012\u00020\u000c0\u000bj\u0002`\r2\u0006\u0010\u0008\u001a\u00020\tH\u0002\"\u001e\u0010\u0000\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004*\u0016\u0010\u0010\"\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a8\u0006\u0011"
    }
    d2 = {
        "section",
        "Ljava/lang/Class;",
        "",
        "getSection",
        "(Ljava/lang/Object;)Ljava/lang/Class;",
        "matches",
        "",
        "Lcom/squareup/container/Command;",
        "traversal",
        "Lflow/Traversal;",
        "process",
        "",
        "Lcom/squareup/container/RedirectStep;",
        "Lcom/squareup/container/RedirectPipeline;",
        "processOnce",
        "Lcom/squareup/container/RedirectPipelineResult;",
        "RedirectPipeline",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getSection$p(Ljava/lang/Object;)Ljava/lang/Class;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/container/RedirectPipelineFactoryKt;->getSection(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method private static final getSection(Ljava/lang/Object;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 319
    instance-of v0, p0, Lcom/squareup/container/ContainerTreeKey;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/container/ContainerTreeKey;

    if-eqz p0, :cond_1

    invoke-static {p0}, Lcom/squareup/container/Masters;->getSection(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private static final matches(Lcom/squareup/container/Command;Lflow/Traversal;)Z
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/container/Command;->history:Lflow/History;

    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/container/Command;->direction:Lflow/Direction;

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final matches(Lflow/Traversal;Lflow/Traversal;)Z
    .locals 2

    .line 266
    iget-object v0, p0, Lflow/Traversal;->destination:Lflow/History;

    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lflow/Traversal;->direction:Lflow/Direction;

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final process(Ljava/util/List;Lflow/Traversal;)Lflow/Traversal;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/RedirectStep;",
            ">;",
            "Lflow/Traversal;",
            ")",
            "Lflow/Traversal;"
        }
    .end annotation

    const-string v0, "$this$process"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lflow/Traversal;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 277
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 281
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/container/RedirectPipelineFactoryKt;->processOnce(Ljava/util/List;Lflow/Traversal;)Lcom/squareup/container/RedirectPipelineResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineResult;->component1()Lflow/Traversal;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/container/RedirectPipelineResult;->component2()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 283
    move-object p1, v0

    check-cast p1, Ljava/lang/Iterable;

    .line 321
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lflow/Traversal;

    .line 283
    invoke-static {v3, v1}, Lcom/squareup/container/RedirectPipelineFactoryKt;->matches(Lflow/Traversal;Lflow/Traversal;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 322
    :goto_1
    check-cast v2, Lflow/Traversal;

    if-nez v2, :cond_2

    .line 292
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p1, v1

    goto :goto_0

    .line 285
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 286
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redirect loop detected:\n"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\treturned to: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "\tprevious:   "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 285
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_3
    return-object v1
.end method

.method private static final processOnce(Ljava/util/List;Lflow/Traversal;)Lcom/squareup/container/RedirectPipelineResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/RedirectStep;",
            ">;",
            "Lflow/Traversal;",
            ")",
            "Lcom/squareup/container/RedirectPipelineResult;"
        }
    .end annotation

    .line 304
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/RedirectStep;

    .line 305
    invoke-interface {v0, p1}, Lcom/squareup/container/RedirectStep;->maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v2, v0, Lcom/squareup/container/RedirectStep$Result;->command:Lcom/squareup/container/Command;

    const-string v3, "processed.command"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p1}, Lcom/squareup/container/RedirectPipelineFactoryKt;->matches(Lcom/squareup/container/Command;Lflow/Traversal;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p0, 0x1

    new-array v2, p0, [Ljava/lang/Object;

    .line 308
    iget-object v3, v0, Lcom/squareup/container/RedirectStep$Result;->logString:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v1, "redirect: %s"

    invoke-static {v1, v2}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    new-instance v1, Lcom/squareup/container/RedirectPipelineResult;

    .line 310
    new-instance v2, Lflow/Traversal;

    iget-object p1, p1, Lflow/Traversal;->origin:Lflow/History;

    iget-object v3, v0, Lcom/squareup/container/RedirectStep$Result;->command:Lcom/squareup/container/Command;

    iget-object v3, v3, Lcom/squareup/container/Command;->history:Lflow/History;

    iget-object v0, v0, Lcom/squareup/container/RedirectStep$Result;->command:Lcom/squareup/container/Command;

    iget-object v0, v0, Lcom/squareup/container/Command;->direction:Lflow/Direction;

    invoke-direct {v2, p1, v3, v0}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    .line 309
    invoke-direct {v1, v2, p0}, Lcom/squareup/container/RedirectPipelineResult;-><init>(Lflow/Traversal;Z)V

    return-object v1

    .line 316
    :cond_1
    new-instance p0, Lcom/squareup/container/RedirectPipelineResult;

    invoke-direct {p0, p1, v1}, Lcom/squareup/container/RedirectPipelineResult;-><init>(Lflow/Traversal;Z)V

    return-object p0
.end method
