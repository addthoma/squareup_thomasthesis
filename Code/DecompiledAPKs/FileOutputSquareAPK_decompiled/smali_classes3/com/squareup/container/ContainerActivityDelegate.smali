.class public Lcom/squareup/container/ContainerActivityDelegate;
.super Ljava/lang/Object;
.source "ContainerActivityDelegate.java"


# static fields
.field private static final SCOPE_NAME:Ljava/lang/String;


# instance fields
.field private final containerBackgrounds:Lcom/squareup/container/ContainerBackgroundsProvider;

.field private final device:Lcom/squareup/util/Device;

.field private final orientationLock:Lcom/squareup/container/OrientationLock;

.field private final viewFactory:Lcom/squareup/container/ContainerViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/ContainerActivityDelegate;->SCOPE_NAME:Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/container/ContainerBackgroundsProvider;Lcom/squareup/container/ContainerViewFactory;[Lcom/squareup/container/ContainerViewFactory;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/container/ContainerActivityDelegate;->device:Lcom/squareup/util/Device;

    .line 39
    iput-object p2, p0, Lcom/squareup/container/ContainerActivityDelegate;->containerBackgrounds:Lcom/squareup/container/ContainerBackgroundsProvider;

    .line 40
    new-instance p2, Lcom/squareup/container/OrientationLock;

    invoke-direct {p2, p1}, Lcom/squareup/container/OrientationLock;-><init>(Lcom/squareup/util/Device;)V

    iput-object p2, p0, Lcom/squareup/container/ContainerActivityDelegate;->orientationLock:Lcom/squareup/container/OrientationLock;

    .line 42
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 43
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    new-instance p2, Lcom/squareup/container/CompoundContainerViewFactory;

    invoke-direct {p2, p1}, Lcom/squareup/container/CompoundContainerViewFactory;-><init>(Ljava/util/List;)V

    iput-object p2, p0, Lcom/squareup/container/ContainerActivityDelegate;->viewFactory:Lcom/squareup/container/ContainerViewFactory;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/container/ContainerActivityDelegate;->SCOPE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/OrientationLock;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/container/ContainerActivityDelegate;->orientationLock:Lcom/squareup/container/OrientationLock;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/ContainerBackgroundsProvider;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/container/ContainerActivityDelegate;->containerBackgrounds:Lcom/squareup/container/ContainerBackgroundsProvider;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/util/Device;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/container/ContainerActivityDelegate;->device:Lcom/squareup/util/Device;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/ContainerViewFactory;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/container/ContainerActivityDelegate;->viewFactory:Lcom/squareup/container/ContainerViewFactory;

    return-object p0
.end method

.method private makeBootstrapContextFactory()Lflow/path/PathContextFactory;
    .locals 1

    .line 88
    new-instance v0, Lcom/squareup/container/ContainerActivityDelegate$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/ContainerActivityDelegate$1;-><init>(Lcom/squareup/container/ContainerActivityDelegate;)V

    return-object v0
.end method


# virtual methods
.method public dropActivity(Lcom/squareup/container/ContainerActivity;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate;->orientationLock:Lcom/squareup/container/OrientationLock;

    invoke-virtual {v0, p1}, Lcom/squareup/container/OrientationLock;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public inflateRootScreen(Lflow/path/Path;Landroid/content/Context;I)Landroid/view/View;
    .locals 1

    .line 70
    invoke-direct {p0}, Lcom/squareup/container/ContainerActivityDelegate;->makeBootstrapContextFactory()Lflow/path/PathContextFactory;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lflow/path/Path;->contextFactory(Lflow/path/PathContextFactory;)Lflow/path/PathContextFactory;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lflow/path/PathContextFactory;->setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    .line 72
    invoke-static {p1}, Lflow/path/PathContext;->root(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object p1

    .line 75
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 76
    invoke-virtual {p2, p1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 77
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public takeActivity(Lcom/squareup/container/ContainerActivity;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate;->orientationLock:Lcom/squareup/container/OrientationLock;

    invoke-virtual {v0, p1}, Lcom/squareup/container/OrientationLock;->takeView(Ljava/lang/Object;)V

    return-void
.end method
