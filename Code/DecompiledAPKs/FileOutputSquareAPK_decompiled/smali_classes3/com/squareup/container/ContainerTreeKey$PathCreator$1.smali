.class final Lcom/squareup/container/ContainerTreeKey$PathCreator$1;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "ContainerTreeKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$instance:Lcom/squareup/container/ContainerTreeKey;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;->val$instance:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 357
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .line 359
    iget-object p1, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;->val$instance:Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation

    .line 364
    iget-object v0, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;->val$instance:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/container/ContainerTreeKey;

    check-cast p1, [Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 357
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator$1;->newArray(I)[Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method
