.class public interface abstract Lcom/squareup/container/ContainerActivity;
.super Ljava/lang/Object;
.source "ContainerActivity.java"


# virtual methods
.method public abstract getBundleService()Lmortar/bundler/BundleService;
.end method

.method public abstract getRequestedOrientation()I
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract getSystemService(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getWindow()Landroid/view/Window;
.end method

.method public abstract isRunning()Z
.end method

.method public abstract setRequestedOrientation(I)V
.end method
