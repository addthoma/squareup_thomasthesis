.class public abstract Lcom/squareup/container/WorkflowTreeKey;
.super Lcom/squareup/container/ContainerTreeKey;
.source "WorkflowTreeKey.kt"

# interfaces
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;
.implements Lcom/squareup/container/MaybePersistent;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/WorkflowTreeKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowTreeKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0014\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008&\u0018\u0000 @2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005:\u0001@BM\u0008\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0001\u0012\u0012\u0010\t\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0018\u00105\u001a\u0002062\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0014J\u0010\u0010;\u001a\u0002062\u0006\u0010\u0015\u001a\u00020\u000fH\u0002J\u0008\u0010<\u001a\u00020\u0007H\u0016J\u0008\u0010=\u001a\u00020\u0001H\u0016J\u0010\u0010>\u001a\u00020\u00002\u0006\u0010?\u001a\u00020\u0001H\'R,\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u000f8\u0006@FX\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0017\u0010\u0018\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u001a\u0010\u000c\u001a\u00020\rX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u0011\u0010!\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010&R\u0011\u0010\'\u001a\u00020(\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0011\u0010+\u001a\u00020(\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010*R\u000e\u0010-\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u000e\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010\u001aR\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010/\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u00080\u0010\u001eR\u0011\u00101\u001a\u000202\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u00104\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowTreeKey;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/LocksOrientation;",
        "Lcom/squareup/container/HasSoftInputModeForPhone;",
        "Lcom/squareup/container/MaybePersistent;",
        "Landroid/os/Parcelable;",
        "runnerServiceName",
        "",
        "maybeParent",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "_snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V",
        "value",
        "_props",
        "_props$annotations",
        "()V",
        "get_props",
        "()Ljava/lang/Object;",
        "set_props",
        "(Ljava/lang/Object;)V",
        "get_snapshot$public_release",
        "()Lcom/squareup/workflow/Snapshot;",
        "set_snapshot$public_release",
        "(Lcom/squareup/workflow/Snapshot;)V",
        "analyticsName",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "()Z",
        "orientationForPhone",
        "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "getOrientationForPhone",
        "()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "orientationForTablet",
        "getOrientationForTablet",
        "parent",
        "getProps",
        "snapshot",
        "getSnapshot",
        "softInputMode",
        "Lcom/squareup/workflow/SoftInputMode;",
        "getSoftInputMode",
        "()Lcom/squareup/workflow/SoftInputMode;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "enforcePersistence",
        "getName",
        "getParentKey",
        "reparent",
        "newParent",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;


# instance fields
.field private _props:Ljava/lang/Object;

.field private _snapshot:Lcom/squareup/workflow/Snapshot;

.field private final analyticsName:Ljava/lang/String;

.field private final hint:Lcom/squareup/workflow/ScreenHint;

.field private final isPersistent:Z

.field private final orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private final orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private final parent:Lcom/squareup/container/ContainerTreeKey;

.field public final runnerServiceName:Ljava/lang/String;

.field public final screenKey:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;"
        }
    .end annotation
.end field

.field private final softInputMode:Lcom/squareup/workflow/SoftInputMode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/WorkflowTreeKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/WorkflowTreeKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "_snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->runnerServiceName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p4, p0, Lcom/squareup/container/WorkflowTreeKey;->_snapshot:Lcom/squareup/workflow/Snapshot;

    iput-object p6, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    iput-boolean p7, p0, Lcom/squareup/container/WorkflowTreeKey;->isPersistent:Z

    .line 43
    iget-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getPhoneOrientation()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    .line 44
    iget-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getTabletOrientation()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    .line 45
    iget-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getPhoneSoftInputMode()Lcom/squareup/workflow/SoftInputMode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->softInputMode:Lcom/squareup/workflow/SoftInputMode;

    .line 46
    iget-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getAnalyticsName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->analyticsName:Ljava/lang/String;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    sget-object p1, Lcom/squareup/container/NoParent;->INSTANCE:Lcom/squareup/container/NoParent;

    move-object p2, p1

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    iput-object p2, p0, Lcom/squareup/container/WorkflowTreeKey;->parent:Lcom/squareup/container/ContainerTreeKey;

    .line 61
    iput-object p5, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    .line 69
    iget-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/squareup/container/WorkflowTreeKey;->enforcePersistence(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic _props$annotations()V
    .locals 0

    return-void
.end method

.method protected static final synthetic creator(Lkotlin/jvm/functions/Function7;)Landroid/os/Parcelable$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">(",
            "Lkotlin/jvm/functions/Function7<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Lcom/squareup/container/ContainerTreeKey;",
            "-",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;-",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "-",
            "Lcom/squareup/workflow/ScreenHint;",
            "-",
            "Ljava/lang/Boolean;",
            "+TT;>;)",
            "Landroid/os/Parcelable$Creator<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    invoke-static {v0, p0}, Lcom/squareup/container/WorkflowTreeKey$Companion;->access$creator(Lcom/squareup/container/WorkflowTreeKey$Companion;Lkotlin/jvm/functions/Function7;)Landroid/os/Parcelable$Creator;

    move-result-object p0

    return-object p0
.end method

.method private final enforcePersistence(Ljava/lang/Object;)V
    .locals 2

    .line 73
    instance-of v0, p1, Landroid/os/Parcelable;

    if-nez v0, :cond_1

    instance-of v1, p1, Lkotlin/Unit;

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/container/WorkflowTreeKey;->isPersistent:Z

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_3

    .line 78
    iget-boolean v1, p0, Lcom/squareup/container/WorkflowTreeKey;->isPersistent:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/container/ParcelableTester;->Companion:Lcom/squareup/container/ParcelableTester$Companion;

    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {v0, p1}, Lcom/squareup/container/ParcelableTester$Companion;->assertParcelable(Landroid/os/Parcelable;)V

    :cond_2
    return-void

    .line 74
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Persistent keys must have props of type Unit or Parcelable,"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "but "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " received "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getProps()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "To opt out of persistence, use LayeredScreen.withPersistence(false) in your rendering."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-boolean v0, p0, Lcom/squareup/container/WorkflowTreeKey;->isPersistent:Z

    if-eqz v0, :cond_5

    .line 123
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getProps()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/os/Parcelable;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getProps()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lkotlin/Unit;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 126
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->runnerServiceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getParentKey()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen$Key;->typeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen$Key;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 133
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    instance-of v1, v0, Landroid/os/Parcelable;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 134
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    .line 129
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.os.Parcelable"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 123
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Props must be Parcelable or Unit in order to save."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 122
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "LayeredScreen must be marked as persistent in order to save."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->hint:Lcom/squareup/workflow/ScreenHint;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/WorkflowTreeKey;->parent:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v1}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v1, v1, Lcom/squareup/workflow/legacy/Screen$Key;->typeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v1, v1, Lcom/squareup/workflow/legacy/Screen$Key;->value:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x28

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen$Key;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public final getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public final getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->parent:Lcom/squareup/container/ContainerTreeKey;

    sget-object v1, Lcom/squareup/container/NoParent;->INSTANCE:Lcom/squareup/container/NoParent;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->parent:Lcom/squareup/container/ContainerTreeKey;

    return-object v0

    .line 106
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No parent key set for screenKey "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Register "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-class v1, Lcom/squareup/container/PosWorkflowRunner;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " below the root scope."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowTreeKey;->getParentKey()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public final getProps()Ljava/lang/Object;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    return-object v0
.end method

.method public final getSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->_snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public final getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->softInputMode:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public final get_props()Ljava/lang/Object;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    return-object v0
.end method

.method public final get_snapshot$public_release()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/container/WorkflowTreeKey;->_snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public final isPersistent()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/squareup/container/WorkflowTreeKey;->isPersistent:Z

    return v0
.end method

.method public abstract reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .annotation runtime Lkotlin/Deprecated;
        message = "Don\'t play crafty scoping games. RF-1301"
    .end annotation
.end method

.method public final set_props(Ljava/lang/Object;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/container/WorkflowTreeKey;->enforcePersistence(Ljava/lang/Object;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->_props:Ljava/lang/Object;

    return-void
.end method

.method public final set_snapshot$public_release(Lcom/squareup/workflow/Snapshot;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/squareup/container/WorkflowTreeKey;->_snapshot:Lcom/squareup/workflow/Snapshot;

    return-void
.end method
