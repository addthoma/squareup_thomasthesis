.class public Lcom/squareup/container/CardScreenTabletLayout;
.super Landroid/view/ViewGroup;
.source "CardScreenTabletLayout.java"

# interfaces
.implements Lcom/squareup/container/CardScreenContainer;


# instance fields
.field private final animationDuration:I

.field private cardAnimatorSet:Landroid/animation/AnimatorSet;

.field private final cardRect:Landroid/graphics/Rect;

.field private horizontalCardMargin:I

.field private final isLandscape:Z

.field private final panelHeight:I

.field private final panelWidth:I

.field private final scrimColor:I

.field private verticalCardMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 57
    iget p2, p1, Landroid/graphics/Point;->x:I

    iput p2, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelWidth:I

    .line 58
    iget p1, p1, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelHeight:I

    .line 59
    iget p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelWidth:I

    iget p2, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelHeight:I

    if-le p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->isLandscape:Z

    .line 61
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/container/ContainerBackgroundsService;->getCardScrimColor(Landroid/content/Context;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/container/CardScreenTabletLayout;->scrimColor:I

    .line 65
    sget p2, Lcom/squareup/container/R$integer;->config_activityShortDur:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->animationDuration:I

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/container/CardScreenTabletLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method private dismiss(ZLflow/TraversalCallback;)V
    .locals 8

    .line 210
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    if-nez p1, :cond_0

    const/16 p1, 0x8

    .line 213
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    const/4 p1, 0x0

    .line 214
    invoke-direct {p0, p1}, Lcom/squareup/container/CardScreenTabletLayout;->setScrimOpacity(F)V

    .line 215
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    goto :goto_0

    .line 217
    :cond_0
    sget-object p1, Lcom/squareup/container/CardScreenTabletLayout;->SCALE_X:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    invoke-static {v0, p1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 218
    sget-object v2, Lcom/squareup/container/CardScreenTabletLayout;->SCALE_Y:Landroid/util/Property;

    new-array v3, v1, [F

    fill-array-data v3, :array_1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 219
    sget-object v3, Lcom/squareup/container/CardScreenTabletLayout;->ALPHA:Landroid/util/Property;

    new-array v4, v1, [F

    fill-array-data v4, :array_2

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    new-array v4, v1, [F

    .line 220
    fill-array-data v4, :array_3

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 221
    new-instance v5, Lcom/squareup/container/-$$Lambda$CardScreenTabletLayout$o_9tU4Az01qRW57b5m8Sbk3mqps;

    invoke-direct {v5, p0}, Lcom/squareup/container/-$$Lambda$CardScreenTabletLayout$o_9tU4Az01qRW57b5m8Sbk3mqps;-><init>(Lcom/squareup/container/CardScreenTabletLayout;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 223
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    .line 224
    iget-object v5, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v6, 0x4

    new-array v6, v6, [Landroid/animation/Animator;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 p1, 0x1

    aput-object v2, v6, p1

    aput-object v3, v6, v1

    const/4 p1, 0x3

    aput-object v4, v6, p1

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 225
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcom/squareup/container/CardScreenTabletLayout;->animationDuration:I

    int-to-long v1, v1

    invoke-virtual {p1, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 226
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 227
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/squareup/container/CardScreenTabletLayout$2;

    invoke-direct {v1, p0, v0, p2}, Lcom/squareup/container/CardScreenTabletLayout$2;-><init>(Lcom/squareup/container/CardScreenTabletLayout;Landroid/view/View;Lflow/TraversalCallback;)V

    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 235
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-static {p1, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f666666    # 0.9f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f666666    # 0.9f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private isShowing()Z
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isShown()Z

    move-result v0

    return v0
.end method

.method private setScrimOpacity(F)V
    .locals 2

    .line 241
    iget v0, p0, Lcom/squareup/container/CardScreenTabletLayout;->scrimColor:I

    const/high16 v1, -0x1000000

    and-int/2addr v1, v0

    ushr-int/lit8 v1, v1, 0x18

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int p1, v1

    shl-int/lit8 p1, p1, 0x18

    const v1, 0xffffff

    and-int/2addr v0, v1

    or-int/2addr p1, v0

    .line 243
    invoke-virtual {p0, p1}, Lcom/squareup/container/CardScreenTabletLayout;->setBackgroundColor(I)V

    .line 244
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->invalidate()V

    return-void
.end method

.method private show(ZLflow/TraversalCallback;)V
    .locals 8

    .line 163
    invoke-direct {p0}, Lcom/squareup/container/CardScreenTabletLayout;->isShowing()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 168
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 170
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v2, 0x1

    .line 173
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 174
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_1

    const/high16 p1, 0x3f800000    # 1.0f

    .line 177
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 178
    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    .line 179
    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleY(F)V

    .line 180
    invoke-direct {p0, p1}, Lcom/squareup/container/CardScreenTabletLayout;->setScrimOpacity(F)V

    .line 181
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    goto :goto_0

    .line 183
    :cond_1
    sget-object p1, Lcom/squareup/container/CardScreenTabletLayout;->SCALE_X:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v4, v3, [F

    fill-array-data v4, :array_0

    invoke-static {v0, p1, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 184
    sget-object v4, Lcom/squareup/container/CardScreenTabletLayout;->SCALE_Y:Landroid/util/Property;

    new-array v5, v3, [F

    fill-array-data v5, :array_1

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 185
    sget-object v5, Lcom/squareup/container/CardScreenTabletLayout;->ALPHA:Landroid/util/Property;

    new-array v6, v3, [F

    fill-array-data v6, :array_2

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-array v5, v3, [F

    .line 186
    fill-array-data v5, :array_3

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 187
    new-instance v6, Lcom/squareup/container/-$$Lambda$CardScreenTabletLayout$AhwgzZyU84ZmmEU294vbgxMs2oY;

    invoke-direct {v6, p0}, Lcom/squareup/container/-$$Lambda$CardScreenTabletLayout$AhwgzZyU84ZmmEU294vbgxMs2oY;-><init>(Lcom/squareup/container/CardScreenTabletLayout;)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 189
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v6, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    .line 190
    iget-object v6, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v7, 0x4

    new-array v7, v7, [Landroid/animation/Animator;

    aput-object p1, v7, v1

    aput-object v4, v7, v2

    aput-object v0, v7, v3

    const/4 p1, 0x3

    aput-object v5, v7, p1

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 191
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    iget v0, p0, Lcom/squareup/container/CardScreenTabletLayout;->animationDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 192
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 193
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v0, Lcom/squareup/container/CardScreenTabletLayout$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/container/CardScreenTabletLayout$1;-><init>(Lcom/squareup/container/CardScreenTabletLayout;Lflow/TraversalCallback;)V

    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-static {p1, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 201
    iget-object p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f666666    # 0.9f
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public asViewGroup()Landroid/view/ViewGroup;
    .locals 2

    const/4 v0, 0x0

    .line 146
    invoke-virtual {p0, v0}, Lcom/squareup/container/CardScreenTabletLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "Must have a child to show"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    .line 134
    instance-of p1, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 69
    invoke-direct {p0}, Lcom/squareup/container/CardScreenTabletLayout;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    .line 70
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 75
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .line 142
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .line 130
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 138
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public synthetic lambda$dismiss$1$CardScreenTabletLayout(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 221
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/squareup/container/CardScreenTabletLayout;->setScrimOpacity(F)V

    return-void
.end method

.method public synthetic lambda$show$0$CardScreenTabletLayout(Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 187
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/container/CardScreenTabletLayout;->setScrimOpacity(F)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 79
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/container/ContainerBackgroundsService;->getCardBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 81
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 117
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getMeasuredWidth()I

    move-result p1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->getMeasuredHeight()I

    move-result p2

    .line 120
    iget-object p3, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    iget p4, p0, Lcom/squareup/container/CardScreenTabletLayout;->horizontalCardMargin:I

    iget p5, p0, Lcom/squareup/container/CardScreenTabletLayout;->verticalCardMargin:I

    sub-int/2addr p1, p4

    sub-int/2addr p2, p5

    invoke-virtual {p3, p4, p5, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 124
    invoke-direct {p0}, Lcom/squareup/container/CardScreenTabletLayout;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->left:I

    iget-object p3, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    iget p3, p3, Landroid/graphics/Rect;->top:I

    iget-object p4, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    iget p4, p4, Landroid/graphics/Rect;->right:I

    iget-object p5, p0, Lcom/squareup/container/CardScreenTabletLayout;->cardRect:Landroid/graphics/Rect;

    iget p5, p5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/ViewGroup;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .line 86
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 87
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 89
    iget-boolean v2, p0, Lcom/squareup/container/CardScreenTabletLayout;->isLandscape:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 90
    iget v2, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelHeight:I

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/squareup/container/CardScreenTabletLayout;->horizontalCardMargin:I

    .line 91
    iput v3, p0, Lcom/squareup/container/CardScreenTabletLayout;->verticalCardMargin:I

    goto :goto_0

    .line 93
    :cond_0
    iput v3, p0, Lcom/squareup/container/CardScreenTabletLayout;->horizontalCardMargin:I

    .line 94
    iget v2, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelWidth:I

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/squareup/container/CardScreenTabletLayout;->verticalCardMargin:I

    .line 99
    :goto_0
    invoke-direct {p0}, Lcom/squareup/container/CardScreenTabletLayout;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    invoke-virtual {p0}, Lcom/squareup/container/CardScreenTabletLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v2

    .line 101
    iget-boolean v4, p0, Lcom/squareup/container/CardScreenTabletLayout;->isLandscape:Z

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v6, -0x1

    if-eqz v4, :cond_1

    .line 102
    iget p1, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelHeight:I

    .line 103
    invoke-static {p1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 104
    invoke-static {p2, v3, v6}, Lcom/squareup/container/CardScreenTabletLayout;->getChildMeasureSpec(III)I

    move-result p2

    .line 102
    invoke-virtual {v2, p1, p2}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 107
    :cond_1
    invoke-static {p1, v3, v6}, Lcom/squareup/container/CardScreenTabletLayout;->getChildMeasureSpec(III)I

    move-result p1

    iget p2, p0, Lcom/squareup/container/CardScreenTabletLayout;->panelWidth:I

    .line 108
    invoke-static {p2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 106
    invoke-virtual {v2, p1, p2}, Landroid/view/View;->measure(II)V

    .line 113
    :cond_2
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/squareup/container/CardScreenTabletLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCardVisible(ZZLflow/TraversalCallback;)V
    .locals 0

    if-nez p1, :cond_0

    .line 152
    invoke-direct {p0, p2, p3}, Lcom/squareup/container/CardScreenTabletLayout;->dismiss(ZLflow/TraversalCallback;)V

    goto :goto_0

    .line 154
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/squareup/container/CardScreenTabletLayout;->show(ZLflow/TraversalCallback;)V

    :goto_0
    return-void
.end method
