.class final Lcom/squareup/container/Histories$scrubbed$1;
.super Lkotlin/jvm/internal/Lambda;
.source "Histories.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/Histories;->scrubbed(Lflow/History;)Lflow/History;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/Histories$scrubbed$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/container/Histories$scrubbed$1;

    invoke-direct {v0}, Lcom/squareup/container/Histories$scrubbed$1;-><init>()V

    sput-object v0, Lcom/squareup/container/Histories$scrubbed$1;->INSTANCE:Lcom/squareup/container/Histories$scrubbed$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/container/Histories$scrubbed$1;->invoke(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;)Z
    .locals 2

    .line 32
    instance-of v0, p1, Lcom/squareup/container/ContainerTreeKey;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p1}, Lcom/squareup/container/ContainerTreeKey;->anyElementsNotPersistent()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 33
    :cond_1
    instance-of v0, p1, Lcom/squareup/container/MaybePersistent;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/container/MaybePersistent;

    invoke-interface {p1}, Lcom/squareup/container/MaybePersistent;->isPersistent()Z

    move-result v1

    :cond_2
    :goto_0
    return v1
.end method
