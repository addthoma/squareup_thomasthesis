.class public final Lcom/squareup/container/layer/DialogReference;
.super Ljava/lang/Object;
.source "DialogReference.java"


# static fields
.field public static final NONE:Lcom/squareup/container/layer/DialogReference;

.field private static final NO_KEY:Lflow/path/Path;

.field private static final onDialogViewDetachedFromWindow:Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field private final dialog:Landroid/app/Dialog;

.field private final key:Lflow/path/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/container/layer/DialogReference$1;

    invoke-direct {v0}, Lcom/squareup/container/layer/DialogReference$1;-><init>()V

    sput-object v0, Lcom/squareup/container/layer/DialogReference;->NO_KEY:Lflow/path/Path;

    .line 32
    new-instance v0, Lcom/squareup/container/layer/DialogReference;

    sget-object v1, Lcom/squareup/container/layer/DialogReference;->NO_KEY:Lflow/path/Path;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/container/layer/DialogReference;-><init>(Lflow/path/Path;Landroid/app/Dialog;)V

    sput-object v0, Lcom/squareup/container/layer/DialogReference;->NONE:Lcom/squareup/container/layer/DialogReference;

    .line 34
    new-instance v0, Lcom/squareup/container/layer/DialogReference$2;

    invoke-direct {v0}, Lcom/squareup/container/layer/DialogReference$2;-><init>()V

    sput-object v0, Lcom/squareup/container/layer/DialogReference;->onDialogViewDetachedFromWindow:Landroid/view/View$OnAttachStateChangeListener;

    return-void
.end method

.method private constructor <init>(Lflow/path/Path;Landroid/app/Dialog;)V
    .locals 2

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_1

    .line 56
    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Dialog must already have been shown."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    .line 58
    iput-object p2, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    return-void
.end method

.method public static expect(Lflow/path/Path;)Lcom/squareup/container/layer/DialogReference;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string v1, "Waiting for new dialog for %s"

    .line 73
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    new-instance v0, Lcom/squareup/container/layer/DialogReference;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/container/layer/DialogReference;-><init>(Lflow/path/Path;Landroid/app/Dialog;)V

    return-object v0
.end method

.method private static getDecorView(Landroid/app/Dialog;)Landroid/view/View;
    .locals 1

    .line 149
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    const-string v0, "dialog.getWindow()"

    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/Window;

    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private hasDialog()Z
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public isShowing()Z
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogReference;->hasDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public matches(Lflow/path/Path;)Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method requireContext()Landroid/content/Context;
    .locals 2

    .line 144
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogReference;->hasDialog()Z

    move-result v0

    const-string v1, "No dialog, no context."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    const-string/jumbo v1, "this.dialog"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    invoke-static {v0}, Lcom/squareup/container/layer/DialogReference;->getDecorView(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public show(Landroid/app/Dialog;)Lcom/squareup/container/layer/DialogReference;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 86
    iget-object v2, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Showing new dialog for %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-object v1, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Expect not to be showing a dialog yet."

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 91
    invoke-static {p1}, Lcom/squareup/container/layer/DialogReference;->getDecorView(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v2

    .line 93
    iget-object v4, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    aput-object v6, v5, v3

    aput-object v2, v5, v0

    const-string v0, "Expected to show dialog for %s, found %s"

    invoke-static {v4, v0, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->key:Lflow/path/Path;

    instance-of v0, v0, Lcom/squareup/container/WorkflowDialogKey;

    if-nez v0, :cond_1

    .line 98
    sget-object v0, Lcom/squareup/container/layer/DialogReference;->onDialogViewDetachedFromWindow:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 101
    :cond_1
    new-instance v0, Lcom/squareup/container/layer/DialogReference;

    invoke-direct {v0, v2, p1}, Lcom/squareup/container/layer/DialogReference;-><init>(Lflow/path/Path;Landroid/app/Dialog;)V

    return-object v0
.end method

.method public tearDown()Landroid/content/Context;
    .locals 5

    .line 112
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogReference;->hasDialog()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    invoke-static {v0}, Lcom/squareup/container/layer/DialogReference;->getDecorView(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 119
    invoke-static {v1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "tearing down dialog %s"

    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    sget-object v2, Lcom/squareup/container/layer/DialogReference;->onDialogViewDetachedFromWindow:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/container/layer/DialogReference;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-object v1
.end method
