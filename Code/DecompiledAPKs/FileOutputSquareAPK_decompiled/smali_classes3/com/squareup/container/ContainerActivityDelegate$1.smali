.class Lcom/squareup/container/ContainerActivityDelegate$1;
.super Ljava/lang/Object;
.source "ContainerActivityDelegate.java"

# interfaces
.implements Lflow/path/PathContextFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerActivityDelegate;->makeBootstrapContextFactory()Lflow/path/PathContextFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/ContainerActivityDelegate;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerActivityDelegate;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/container/ContainerActivityDelegate$1;->this$0:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;
    .locals 1

    .line 90
    invoke-static {p2}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    .line 91
    invoke-static {}, Lcom/squareup/container/ContainerActivityDelegate;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v0

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate$1;->this$0:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {v0}, Lcom/squareup/container/ContainerActivityDelegate;->access$100(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/OrientationLock;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/OrientationLock;->addToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/OrientationLock;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate$1;->this$0:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {v0}, Lcom/squareup/container/ContainerActivityDelegate;->access$200(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/ContainerBackgroundsProvider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/ContainerBackgroundsService;->addToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/ContainerBackgroundsProvider;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate$1;->this$0:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {v0}, Lcom/squareup/container/ContainerActivityDelegate;->access$300(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/util/Device;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/ContainerKt;->addDeviceToScope(Lmortar/MortarScope$Builder;Lcom/squareup/util/Device;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/container/ContainerActivityDelegate$1;->this$0:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {v0}, Lcom/squareup/container/ContainerActivityDelegate;->access$400(Lcom/squareup/container/ContainerActivityDelegate;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/ContainerKt;->addViewFactoryToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/ContainerViewFactory;)V

    .line 99
    invoke-static {}, Lcom/squareup/container/ContainerActivityDelegate;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v0

    .line 102
    :cond_0
    invoke-virtual {v0, p2}, Lmortar/MortarScope;->createContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1
.end method

.method public tearDownContext(Landroid/content/Context;)V
    .locals 0

    .line 106
    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    invoke-virtual {p1}, Lmortar/MortarScope;->destroy()V

    return-void
.end method
