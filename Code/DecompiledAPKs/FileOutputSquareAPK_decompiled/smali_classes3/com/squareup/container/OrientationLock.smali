.class public Lcom/squareup/container/OrientationLock;
.super Lmortar/Presenter;
.source "OrientationLock.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/container/ContainerActivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final SERVICE_NAME:Ljava/lang/String;


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/container/OrientationLock;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/OrientationLock;->SERVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/squareup/util/Device;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static addToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/OrientationLock;)V
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/container/OrientationLock;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method public static get(Lmortar/MortarScope;)Lcom/squareup/container/OrientationLock;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/container/OrientationLock;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/OrientationLock;

    return-object p0
.end method

.method private getRequestedOrientation(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;)I
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x6

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1

    :cond_2
    const/4 p1, 0x7

    return p1

    :cond_3
    return v0
.end method

.method private lock(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;)Z
    .locals 6

    .line 96
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "already unlocked"

    .line 97
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 100
    :cond_0
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    const/4 v2, 0x1

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 101
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/container/OrientationLock;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/ContainerActivity;

    .line 102
    invoke-interface {v3}, Lcom/squareup/container/ContainerActivity;->getRequestedOrientation()I

    move-result v4

    const/4 v5, 0x2

    if-eqz v4, :cond_4

    if-eq v4, v2, :cond_3

    packed-switch v4, :pswitch_data_0

    goto :goto_2

    :cond_3
    :pswitch_0
    if-nez v0, :cond_5

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v1

    .line 118
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "already %s: portrait(%s)"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_4
    :pswitch_1
    if-eqz v0, :cond_5

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v1

    .line 109
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "already %s: landscape(%s)"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_5
    :goto_2
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    const-string v4, "lock: %s"

    .line 124
    invoke-static {v4, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/container/OrientationLock;->getRequestedOrientation(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;)I

    move-result p1

    invoke-interface {v3, p1}, Lcom/squareup/container/ContainerActivity;->setRequestedOrientation(I)V

    .line 129
    invoke-interface {v3}, Lcom/squareup/container/ContainerActivity;->isRunning()Z

    move-result p1

    if-nez p1, :cond_6

    return v1

    .line 133
    :cond_6
    iget-object p1, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    if-eqz v0, :cond_7

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPortrait()Z

    move-result p1

    goto :goto_3

    :cond_7
    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result p1

    :goto_3
    return p1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/container/ContainerActivity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 46
    invoke-interface {p1}, Lcom/squareup/container/ContainerActivity;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/container/ContainerActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/container/OrientationLock;->extractBundleService(Lcom/squareup/container/ContainerActivity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method getOrientationForPath(Lflow/path/Path;)Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isAndroidOWithBadAspectRatio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object p1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object p1

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/squareup/container/LocksOrientation;

    if-nez v0, :cond_1

    .line 82
    sget-object p1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object p1

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/container/LocksOrientation;

    .line 86
    iget-object v0, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    invoke-interface {p1}, Lcom/squareup/container/LocksOrientation;->getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    return-object p1

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    invoke-interface {p1}, Lcom/squareup/container/LocksOrientation;->getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    return-object p1

    .line 92
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Device is somehow neither a phone nor a tablet."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method requestLock(Lflow/path/Path;)Z
    .locals 0

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/container/OrientationLock;->getOrientationForPath(Lflow/path/Path;)Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/container/OrientationLock;->lock(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;)Z

    move-result p1

    return p1
.end method

.method requestUnlockOrientation()V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/container/OrientationLock;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isAndroidOWithBadAspectRatio()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/container/OrientationLock;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/container/OrientationLock;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivity;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "unlock"

    .line 72
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-direct {p0, v1}, Lcom/squareup/container/OrientationLock;->getRequestedOrientation(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/container/ContainerActivity;->setRequestedOrientation(I)V

    :cond_1
    return-void
.end method

.method shouldLock(Lflow/path/Path;)Z
    .locals 1

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/container/OrientationLock;->getOrientationForPath(Lflow/path/Path;)Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object p1

    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
