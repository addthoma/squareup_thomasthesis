.class final Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$4;
.super Ljava/lang/Object;
.source "RedirectPipelineFactory.kt"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/RedirectPipelineFactory;->buildPipeline(Lmortar/MortarScope;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/RedirectStep$Result;",
        "it",
        "Lflow/Traversal;",
        "kotlin.jvm.PlatformType",
        "maybeRedirect"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/RedirectPipelineFactory;


# direct methods
.method constructor <init>(Lcom/squareup/container/RedirectPipelineFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$4;->this$0:Lcom/squareup/container/RedirectPipelineFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 3

    .line 48
    sget-object v0, Lcom/squareup/container/RedirectPipelineFactory;->Companion:Lcom/squareup/container/RedirectPipelineFactory$Companion;

    iget-object v1, p0, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$4;->this$0:Lcom/squareup/container/RedirectPipelineFactory;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;->access$redirectFromHidesMasterToMaster(Lcom/squareup/container/RedirectPipelineFactory$Companion;Lcom/squareup/container/RedirectPipelineFactory;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p1

    return-object p1
.end method
