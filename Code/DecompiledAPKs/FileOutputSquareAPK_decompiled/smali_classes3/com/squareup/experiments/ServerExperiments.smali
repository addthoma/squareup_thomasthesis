.class public Lcom/squareup/experiments/ServerExperiments;
.super Ljava/lang/Object;
.source "ServerExperiments.java"

# interfaces
.implements Lcom/squareup/experiments/ExperimentStorage;
.implements Lmortar/Scoped;


# static fields
.field static final REFRESH_HOURS:I = 0x4

.field public static final RUNNING:Ljava/lang/String; = "RUNNING"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final cachedResponse:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;"
        }
    .end annotation
.end field

.field private final experimentNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final lastResponse:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;"
        }
    .end annotation
.end field

.field private final refreshHandler:Lio/reactivex/functions/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/server/ExperimentsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final refreshObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/ExperimentsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/PublicApiService;Lio/reactivex/Scheduler;Ljavax/inject/Provider;Ljavax/inject/Provider;Lio/reactivex/Scheduler;)V
    .locals 6
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p9    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;",
            "Lcom/squareup/server/PublicApiService;",
            "Lio/reactivex/Scheduler;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    .line 58
    new-instance v0, Lcom/squareup/experiments/ServerExperiments$1;

    invoke-direct {v0, p0}, Lcom/squareup/experiments/ServerExperiments$1;-><init>(Lcom/squareup/experiments/ServerExperiments;)V

    iput-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->refreshHandler:Lio/reactivex/functions/Consumer;

    .line 73
    iput-object p2, p0, Lcom/squareup/experiments/ServerExperiments;->analytics:Lcom/squareup/analytics/Analytics;

    .line 74
    iput-object p3, p0, Lcom/squareup/experiments/ServerExperiments;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    .line 75
    iput-object p4, p0, Lcom/squareup/experiments/ServerExperiments;->cachedResponse:Lcom/squareup/settings/LocalSetting;

    .line 76
    iput-object p7, p0, Lcom/squareup/experiments/ServerExperiments;->userTokenProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/experiments/ServerExperiments;->installationIdProvider:Ljavax/inject/Provider;

    .line 79
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/experiments/ServerExperiments;->experimentNames:Ljava/util/List;

    .line 80
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/experiments/ExperimentProfile;

    .line 81
    iget-object p3, p0, Lcom/squareup/experiments/ServerExperiments;->experimentNames:Ljava/util/List;

    invoke-virtual {p2}, Lcom/squareup/experiments/ExperimentProfile;->name()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_0
    iget-object p1, p0, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    new-instance p2, Lcom/squareup/experiments/ExperimentMap;

    invoke-direct {p2}, Lcom/squareup/experiments/ExperimentMap;-><init>()V

    invoke-interface {p4, p2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x4

    .line 88
    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    move-object v5, p9

    invoke-static/range {v0 .. v5}, Lio/reactivex/Observable;->interval(JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/experiments/-$$Lambda$ServerExperiments$anbf4r1cX47GU9ir6vh1qhITJ_k;

    invoke-direct {p2, p0, p5}, Lcom/squareup/experiments/-$$Lambda$ServerExperiments$anbf4r1cX47GU9ir6vh1qhITJ_k;-><init>(Lcom/squareup/experiments/ServerExperiments;Lcom/squareup/server/PublicApiService;)V

    .line 89
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/experiments/-$$Lambda$ServerExperiments$U5QtwvaLxPFu-Xzwzr-iyyS1Lk0;->INSTANCE:Lcom/squareup/experiments/-$$Lambda$ServerExperiments$U5QtwvaLxPFu-Xzwzr-iyyS1Lk0;

    .line 94
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 100
    invoke-virtual {p1, p6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/experiments/ServerExperiments;->refreshObservable:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/experiments/ServerExperiments;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/experiments/ServerExperiments;->cachedResponse:Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method

.method private determineLastUpdated()J
    .locals 7

    .line 122
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/experiments/ExperimentMap;

    .line 123
    invoke-virtual {v0}, Lcom/squareup/experiments/ExperimentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    .line 124
    iget-object v4, v3, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    iget-wide v4, v4, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    cmp-long v6, v4, v1

    if-lez v6, :cond_0

    .line 125
    iget-object v1, v3, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    iget-wide v1, v1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method private static findBucket(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 188
    iget-object v2, v1, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 192
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find bucket "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " in experiment "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getLatestConfiguration(Lcom/squareup/experiments/ExperimentMap;Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 142
    invoke-virtual {p2}, Lcom/squareup/experiments/ExperimentProfile;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/experiments/ExperimentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    if-nez p1, :cond_0

    .line 144
    invoke-virtual {p2}, Lcom/squareup/experiments/ExperimentProfile;->defaultConfiguration()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method static synthetic lambda$new$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/ExperimentsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/ExperimentsResponse;

    return-object p0

    .line 98
    :cond_0
    sget-object p0, Lcom/squareup/server/ExperimentsResponse;->EMPTY:Lcom/squareup/server/ExperimentsResponse;

    return-object p0
.end method


# virtual methods
.method public assignedBucket(Lcom/squareup/experiments/ExperimentProfile;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/experiments/-$$Lambda$ServerExperiments$-1dWAJIRINTJRC8p_BHD1hpPyAA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/experiments/-$$Lambda$ServerExperiments$-1dWAJIRINTJRC8p_BHD1hpPyAA;-><init>(Lcom/squareup/experiments/ServerExperiments;Lcom/squareup/experiments/ExperimentProfile;)V

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getAssignedBucket(Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 1

    .line 133
    invoke-virtual {p0, p1}, Lcom/squareup/experiments/ServerExperiments;->getLatestConfiguration(Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/experiments/ExperimentProfile;->controlBucketName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/experiments/ServerExperiments;->getAssignedBucket(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$Bucket;

    move-result-object p1

    return-object p1
.end method

.method getAssignedBucket(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 8

    .line 156
    iget-object v0, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    if-eqz v0, :cond_0

    .line 157
    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-object p1

    .line 159
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    const-string v1, "RUNNING"

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    invoke-static {p1, p2}, Lcom/squareup/experiments/ServerExperiments;->findBucket(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$Bucket;

    move-result-object p1

    return-object p1

    .line 162
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/experiments/ServerExperiments;->getLocalIdentifier()Ljava/lang/String;

    move-result-object v1

    .line 163
    iget-object p2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    .line 164
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v2, "There must be at least one bucket"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 166
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 167
    iget v4, v4, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    add-int/2addr v3, v4

    goto :goto_0

    .line 169
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lcom/squareup/util/Strings;->createSHA1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-static {v0}, Lcom/squareup/util/Strings;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 172
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    rem-int/2addr v0, v3

    add-int/2addr v0, v3

    rem-int/2addr v0, v3

    .line 174
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 175
    iget v3, v6, Lcom/squareup/server/ExperimentsResponse$Bucket;->weight:I

    add-int/2addr v2, v3

    if-le v2, v0, :cond_3

    .line 177
    iget-object p2, p0, Lcom/squareup/experiments/ServerExperiments;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/analytics/AssignmentEvent;

    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    iget-object v3, v6, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    iget v4, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    iget v5, v6, Lcom/squareup/server/ExperimentsResponse$Bucket;->id:I

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/analytics/AssignmentEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {p2, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v6

    .line 182
    :cond_4
    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Found no assignment. experiment "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getLatestConfiguration(Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/experiments/ExperimentMap;

    invoke-direct {p0, v0, p1}, Lcom/squareup/experiments/ServerExperiments;->getLatestConfiguration(Lcom/squareup/experiments/ExperimentMap;Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object p1

    return-object p1
.end method

.method getLocalIdentifier()Ljava/lang/String;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->userTokenProvider:Ljavax/inject/Provider;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->installationIdProvider:Ljavax/inject/Provider;

    :goto_0
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public synthetic lambda$assignedBucket$2$ServerExperiments(Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/experiments/ExperimentMap;)Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 114
    invoke-direct {p0, p2, p1}, Lcom/squareup/experiments/ServerExperiments;->getLatestConfiguration(Lcom/squareup/experiments/ExperimentMap;Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object p2

    .line 115
    invoke-virtual {p1}, Lcom/squareup/experiments/ExperimentProfile;->controlBucketName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/experiments/ServerExperiments;->getAssignedBucket(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$Bucket;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$new$0$ServerExperiments(Lcom/squareup/server/PublicApiService;Ljava/lang/Long;)Lio/reactivex/SingleSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    new-instance p2, Lcom/squareup/server/ExperimentsRequest;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->experimentNames:Ljava/util/List;

    .line 91
    invoke-direct {p0}, Lcom/squareup/experiments/ServerExperiments;->determineLastUpdated()J

    move-result-wide v1

    invoke-direct {p2, v0, v1, v2}, Lcom/squareup/server/ExperimentsRequest;-><init>(Ljava/util/List;J)V

    .line 92
    invoke-interface {p1, p2}, Lcom/squareup/server/PublicApiService;->retrieveExperiments(Lcom/squareup/server/ExperimentsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments;->refreshObservable:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/experiments/ServerExperiments;->refreshHandler:Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
