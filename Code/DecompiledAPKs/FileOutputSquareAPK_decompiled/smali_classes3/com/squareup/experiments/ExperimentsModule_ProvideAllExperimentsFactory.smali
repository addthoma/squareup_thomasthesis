.class public final Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;
.super Ljava/lang/Object;
.source "ExperimentsModule_ProvideAllExperimentsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/experiments/ExperimentProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final branPaymentPromptVariationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private final iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation
.end field

.field private final showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private final squareCardUpsellExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->branPaymentPromptVariationsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->squareCardUpsellExperimentProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;)",
            "Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAllExperiments(Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;Lcom/squareup/experiments/SquareCardUpsellExperiment;Lcom/squareup/experiments/ExperimentProfile;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation

    .line 55
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/experiments/ExperimentsModule;->provideAllExperiments(Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;Lcom/squareup/experiments/SquareCardUpsellExperiment;Lcom/squareup/experiments/ExperimentProfile;)Ljava/util/List;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    iget-object v1, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->branPaymentPromptVariationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;

    iget-object v2, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->squareCardUpsellExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/experiments/SquareCardUpsellExperiment;

    iget-object v3, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/experiments/ExperimentProfile;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->provideAllExperiments(Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;Lcom/squareup/experiments/SquareCardUpsellExperiment;Lcom/squareup/experiments/ExperimentProfile;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
