.class public final Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;
.super Ljava/lang/Object;
.source "ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/experiments/ExperimentProfile;",
        ">;"
    }
.end annotation


# instance fields
.field private final productionExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final stagingExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->serverProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->productionExperimentProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->stagingExperimentProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;",
            ">;)",
            "Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideIposSkipOnboardingExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;)Lcom/squareup/experiments/ExperimentProfile;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/experiments/ExperimentsModule;->provideIposSkipOnboardingExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;)Lcom/squareup/experiments/ExperimentProfile;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/experiments/ExperimentProfile;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/experiments/ExperimentProfile;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/Server;

    iget-object v1, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->productionExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;

    iget-object v2, p0, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->stagingExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;

    invoke-static {v0, v1, v2}, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->provideIposSkipOnboardingExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;)Lcom/squareup/experiments/ExperimentProfile;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->get()Lcom/squareup/experiments/ExperimentProfile;

    move-result-object v0

    return-object v0
.end method
