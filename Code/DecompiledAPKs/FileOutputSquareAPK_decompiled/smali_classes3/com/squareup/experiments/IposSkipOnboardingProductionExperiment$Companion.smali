.class public final Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Companion;
.super Ljava/lang/Object;
.source "IposSkipOnboardingProductionExperiment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Companion;",
        "",
        "()V",
        "CONTROL",
        "Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "getCONTROL",
        "()Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "DEFAULT_CONFIGURATION",
        "Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
        "getDEFAULT_CONFIGURATION",
        "()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
        "TEST",
        "getTEST",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCONTROL()Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 1

    .line 30
    invoke-static {}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;->access$getCONTROL$cp()Lcom/squareup/server/ExperimentsResponse$Bucket;

    move-result-object v0

    return-object v0
.end method

.method public final getDEFAULT_CONFIGURATION()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 33
    invoke-static {}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;->access$getDEFAULT_CONFIGURATION$cp()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    return-object v0
.end method

.method public final getTEST()Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 1

    .line 31
    invoke-static {}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;->access$getTEST$cp()Lcom/squareup/server/ExperimentsResponse$Bucket;

    move-result-object v0

    return-object v0
.end method
