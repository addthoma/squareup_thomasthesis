.class public final Lcom/squareup/hardware/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/hardware/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final barcode_scanner_connected:I = 0x7f120164

.field public static final barcode_scanner_disconnected:I = 0x7f120165

.field public static final cash_drawer_connected:I = 0x7f120363

.field public static final cash_drawer_disconnected:I = 0x7f120366

.field public static final cash_drawer_failed_to_connect:I = 0x7f120369

.field public static final printer_connected_by_name:I = 0x7f1214b8

.field public static final printer_disconnected_by_name:I = 0x7f1214ba

.field public static final printer_stations_role_receipt:I = 0x7f1214d3

.field public static final printer_stations_role_receipts:I = 0x7f1214d4

.field public static final printer_stations_role_stub:I = 0x7f1214d5

.field public static final printer_stations_role_stubs:I = 0x7f1214d6

.field public static final printer_stations_role_ticket:I = 0x7f1214d7

.field public static final printer_stations_role_tickets:I = 0x7f1214d8

.field public static final scale_connected:I = 0x7f121774

.field public static final scale_disconnected:I = 0x7f121775

.field public static final scanner_unknown_product:I = 0x7f12177c

.field public static final scanner_unknown_vendor:I = 0x7f12177d

.field public static final scanner_vendor_and_product:I = 0x7f12177e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
