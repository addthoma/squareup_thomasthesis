.class public abstract Lcom/squareup/dipper/events/AclEvent;
.super Lcom/squareup/dipper/events/CardReaderDataEvent;
.source "CardReaderDataEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0003\u0004\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/dipper/events/AclEvent;",
        "Lcom/squareup/dipper/events/CardReaderDataEvent;",
        "()V",
        "Lcom/squareup/dipper/events/AclConnectedEvent;",
        "Lcom/squareup/dipper/events/AclDisconnectedEvent;",
        "Lcom/squareup/dipper/events/AclErrorEvent;",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0}, Lcom/squareup/dipper/events/CardReaderDataEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/dipper/events/AclEvent;-><init>()V

    return-void
.end method
