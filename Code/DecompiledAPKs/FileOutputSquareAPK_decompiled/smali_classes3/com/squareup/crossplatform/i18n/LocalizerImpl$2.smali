.class synthetic Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;
.super Ljava/lang/Object;
.source "LocalizerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crossplatform/i18n/LocalizerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

.field static final synthetic $SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

.field static final synthetic $SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 146
    invoke-static {}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->values()[Lcom/squareup/shared/i18n/Localizer$MoneyType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

    sget-object v2, Lcom/squareup/shared/i18n/Localizer$MoneyType;->DEFAULT:Lcom/squareup/shared/i18n/Localizer$MoneyType;

    invoke-virtual {v2}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

    sget-object v3, Lcom/squareup/shared/i18n/Localizer$MoneyType;->SHORTER:Lcom/squareup/shared/i18n/Localizer$MoneyType;

    invoke-virtual {v3}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

    sget-object v4, Lcom/squareup/shared/i18n/Localizer$MoneyType;->CENTS:Lcom/squareup/shared/i18n/Localizer$MoneyType;

    invoke-virtual {v4}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 121
    :catch_2
    invoke-static {}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->values()[Lcom/squareup/shared/i18n/Localizer$PercentageType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    :try_start_3
    sget-object v3, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    sget-object v4, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DEFAULT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-virtual {v4}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    sget-object v4, Lcom/squareup/shared/i18n/Localizer$PercentageType;->TAX:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-virtual {v4}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    sget-object v4, Lcom/squareup/shared/i18n/Localizer$PercentageType;->DISCOUNT:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-virtual {v4}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v3, 0x4

    :try_start_6
    sget-object v4, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    sget-object v5, Lcom/squareup/shared/i18n/Localizer$PercentageType;->WHOLE_NUMBER:Lcom/squareup/shared/i18n/Localizer$PercentageType;

    invoke-virtual {v5}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 101
    :catch_6
    invoke-static {}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->values()[Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    :try_start_7
    sget-object v4, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v5, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v5}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v4, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v4}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v1}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v1}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->LONG_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v1}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v1}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    return-void
.end method
