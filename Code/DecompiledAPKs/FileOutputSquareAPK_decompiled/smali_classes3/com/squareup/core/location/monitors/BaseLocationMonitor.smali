.class public abstract Lcom/squareup/core/location/monitors/BaseLocationMonitor;
.super Ljava/lang/Object;
.source "BaseLocationMonitor.java"

# interfaces
.implements Lcom/squareup/core/location/monitors/LocationMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;
    }
.end annotation


# static fields
.field private static final FUSED_PROVIDER:Ljava/lang/String; = "fused"


# instance fields
.field protected final context:Landroid/content/Context;

.field protected currentLocation:Landroid/location/Location;

.field protected final gpsProvider:Lcom/squareup/core/location/providers/LocationProvider;

.field protected final internalListener:Landroid/location/LocationListener;

.field private lastFused:Landroid/location/Location;

.field private lastGps:Landroid/location/Location;

.field private lastNetwork:Landroid/location/Location;

.field private lastPassive:Landroid/location/Location;

.field protected final locationComparer:Lcom/squareup/core/location/comparer/LocationComparer;

.field protected final locationManager:Landroid/location/LocationManager;

.field protected final networkProvider:Lcom/squareup/core/location/providers/LocationProvider;

.field protected final passiveProvider:Lcom/squareup/core/location/providers/LocationProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/core/location/providers/LocationProvider;Lcom/squareup/core/location/providers/LocationProvider;Lcom/squareup/core/location/providers/LocationProvider;Landroid/location/LocationManager;Lcom/squareup/core/location/comparer/LocationComparer;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->context:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->gpsProvider:Lcom/squareup/core/location/providers/LocationProvider;

    .line 55
    iput-object p3, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->networkProvider:Lcom/squareup/core/location/providers/LocationProvider;

    .line 56
    iput-object p4, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->passiveProvider:Lcom/squareup/core/location/providers/LocationProvider;

    .line 57
    iput-object p5, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationManager:Landroid/location/LocationManager;

    .line 58
    iput-object p6, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationComparer:Lcom/squareup/core/location/comparer/LocationComparer;

    .line 59
    new-instance p1, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;-><init>(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Lcom/squareup/core/location/monitors/BaseLocationMonitor$1;)V

    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->internalListener:Landroid/location/LocationListener;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->getBestLastKnownLocation()Landroid/location/Location;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastGps:Landroid/location/Location;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastGps:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastNetwork:Landroid/location/Location;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastNetwork:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastPassive:Landroid/location/Location;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastPassive:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastFused:Landroid/location/Location;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->lastFused:Landroid/location/Location;

    return-object p1
.end method


# virtual methods
.method public getBestLastKnownLocation()Landroid/location/Location;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    return-object v0
.end method

.method protected getMinTime()J
    .locals 2

    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method public hasPermission()Z
    .locals 2

    .line 65
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 3

    const/4 v0, 0x0

    .line 70
    :try_start_0
    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "Could not find location settings"

    .line 73
    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return v0
.end method

.method public isGpsEnabled()Z
    .locals 2

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public isNetworkEnabled()Z
    .locals 2

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract isSingleShot()Z
.end method

.method protected abstract notifyLocationChanged(Landroid/location/Location;)V
.end method

.method protected removeUpdates()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Stopping location updates until requested again"

    .line 124
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-virtual {p0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->hasPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->internalListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->gpsProvider:Lcom/squareup/core/location/providers/LocationProvider;

    invoke-interface {v0}, Lcom/squareup/core/location/providers/LocationProvider;->removeUpdates()V

    .line 130
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->networkProvider:Lcom/squareup/core/location/providers/LocationProvider;

    invoke-interface {v0}, Lcom/squareup/core/location/providers/LocationProvider;->removeUpdates()V

    .line 131
    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->passiveProvider:Lcom/squareup/core/location/providers/LocationProvider;

    invoke-interface {v0}, Lcom/squareup/core/location/providers/LocationProvider;->removeUpdates()V

    return-void
.end method

.method protected requestLocationUpdates()V
    .locals 4

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Requesting location updates from network and gps"

    .line 100
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->getMinTime()J

    move-result-wide v0

    .line 102
    iget-object v2, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->networkProvider:Lcom/squareup/core/location/providers/LocationProvider;

    iget-object v3, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->internalListener:Landroid/location/LocationListener;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/core/location/providers/LocationProvider;->requestLocationUpdate(Landroid/location/LocationListener;J)V

    .line 103
    iget-object v2, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->gpsProvider:Lcom/squareup/core/location/providers/LocationProvider;

    iget-object v3, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->internalListener:Landroid/location/LocationListener;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/core/location/providers/LocationProvider;->requestLocationUpdate(Landroid/location/LocationListener;J)V

    .line 104
    iget-object v2, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->passiveProvider:Lcom/squareup/core/location/providers/LocationProvider;

    iget-object v3, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->internalListener:Landroid/location/LocationListener;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/core/location/providers/LocationProvider;->requestLocationUpdate(Landroid/location/LocationListener;J)V

    return-void
.end method
