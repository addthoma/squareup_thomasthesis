.class public abstract Lcom/squareup/core/location/CommonLocationModule;
.super Ljava/lang/Object;
.source "CommonLocationModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideLastBestLocationPersistent(Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Ljava/util/concurrent/Executor;)Lcom/squareup/core/location/LastBestLocationStore;
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 43
    new-instance v0, Ljava/io/File;

    const-string v1, "last-best-location"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 45
    new-instance p1, Lcom/squareup/core/location/LastBestLocationStore;

    invoke-interface {p2, v0}, Lcom/squareup/persistent/PersistentFactory;->getStringFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;

    move-result-object p2

    invoke-direct {p1, p0, p2, p3}, Lcom/squareup/core/location/LastBestLocationStore;-><init>(Lcom/google/gson/Gson;Lcom/squareup/persistent/Persistent;Ljava/util/concurrent/Executor;)V

    return-object p1
.end method

.method static provideLocation(Lcom/squareup/location/ValidatedLocationCacheProvider;)Landroid/location/Location;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    invoke-virtual {p0}, Lcom/squareup/location/ValidatedLocationCacheProvider;->getLatestLocation()Landroid/location/Location;

    move-result-object p0

    return-object p0
.end method

.method static provideLocationComparer(Lcom/squareup/util/Clock;)Lcom/squareup/core/location/comparer/LocationComparer;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/core/location/constraint/MaxAgeConstraint;

    const-wide v1, 0x202fbf000L

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/core/location/constraint/MaxAgeConstraint;-><init>(Lcom/squareup/util/Clock;J)V

    .line 52
    new-instance p0, Lcom/squareup/core/location/comparer/LocationComparer;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/core/location/constraint/LocationConstraint;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/squareup/core/location/comparer/LocationComparer;-><init>([Lcom/squareup/core/location/constraint/LocationConstraint;)V

    return-object p0
.end method
