.class public final Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;
.super Ljava/lang/Object;
.source "CogsModule_RealCogsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cogs/RealCogs;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsService;",
            ">;"
        }
    .end annotation
.end field

.field private final connectV2ServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->cogsServiceProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p2, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->connectV2ServiceProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p3, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p4, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p5, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p6, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->persistentProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p7, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p8, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->userDirProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p9, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p10, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p11, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->clockProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p12, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->applicationContextProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p13, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->eventSinkProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p14, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;"
        }
    .end annotation

    .line 97
    new-instance v15, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static realCogs(Lcom/squareup/cogs/CogsService;Lcom/squareup/server/catalog/CatalogConnectV2Service;Lcom/squareup/settings/server/AccountStatusSettings;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cogs/RealCogs;
    .locals 0

    .line 105
    invoke-static/range {p0 .. p13}, Lcom/squareup/features/cogs/CogsModule;->realCogs(Lcom/squareup/cogs/CogsService;Lcom/squareup/server/catalog/CatalogConnectV2Service;Lcom/squareup/settings/server/AccountStatusSettings;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cogs/RealCogs;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cogs/RealCogs;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cogs/RealCogs;
    .locals 15

    .line 86
    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->cogsServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cogs/CogsService;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->connectV2ServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/catalog/CatalogConnectV2Service;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->persistentProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/persistent/PersistentFactory;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/FileThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->applicationContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v14}, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->realCogs(Lcom/squareup/cogs/CogsService;Lcom/squareup/server/catalog/CatalogConnectV2Service;Lcom/squareup/settings/server/AccountStatusSettings;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cogs/RealCogs;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/features/cogs/CogsModule_RealCogsFactory;->get()Lcom/squareup/cogs/RealCogs;

    move-result-object v0

    return-object v0
.end method
