.class final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceV2SectionRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "checked",
        "",
        "invoke",
        "com/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$5$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onEvent$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;->invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V
    .locals 2

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getEvent()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;-><init>(Ljava/lang/Object;Z)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
