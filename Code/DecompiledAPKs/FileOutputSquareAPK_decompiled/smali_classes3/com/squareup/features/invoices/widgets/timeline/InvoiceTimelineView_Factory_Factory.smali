.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;
.super Ljava/lang/Object;
.source "InvoiceTimelineView_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;-><init>(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;

    invoke-static {v0, v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->newInstance(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView_Factory_Factory;->get()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    move-result-object v0

    return-object v0
.end method
