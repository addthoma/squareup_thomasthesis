.class public final Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;
.super Ljava/lang/Object;
.source "InvoiceV2CartEntryViewsFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;)Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;-><init>(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->newInstance(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;)Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory_Factory;->get()Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    move-result-object v0

    return-object v0
.end method
