.class public final Lcom/squareup/features/invoices/widgets/RenderDelegate;
.super Ljava/lang/Object;
.source "InvoiceSectionRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceSectionRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceSectionRenderer.kt\ncom/squareup/features/invoices/widgets/RenderDelegate\n*L\n1#1,162:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/RenderDelegate;",
        "",
        "()V",
        "render",
        "Landroid/view/View;",
        "sectionElement",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "renderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/features/invoices/widgets/RenderDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 126
    new-instance v0, Lcom/squareup/features/invoices/widgets/RenderDelegate;

    invoke-direct {v0}, Lcom/squareup/features/invoices/widgets/RenderDelegate;-><init>()V

    sput-object v0, Lcom/squareup/features/invoices/widgets/RenderDelegate;->INSTANCE:Lcom/squareup/features/invoices/widgets/RenderDelegate;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const-string v0, "sectionElement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 135
    instance-of v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    new-instance v0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$1;

    invoke-direct {v0, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$1;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, p4, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;

    invoke-interface {p2, p1, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 137
    :cond_1
    instance-of v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    if-eqz v1, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    new-instance v1, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;

    invoke-direct {v1, p4, p1, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;-><init>(Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-interface {p2, v0, p4, v1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    goto/16 :goto_0

    .line 142
    :cond_2
    instance-of v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    if-eqz v1, :cond_3

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    new-instance v0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$3;

    invoke-direct {v0, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$3;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, p4, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 143
    :cond_3
    instance-of v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;

    new-instance v0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$4;

    invoke-direct {v0, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$4;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, p4, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 144
    :cond_4
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    const-string v1, "context"

    if-eqz p4, :cond_5

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$5;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$5;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 145
    :cond_5
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    if-eqz p4, :cond_6

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$6;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$6;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 146
    :cond_6
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;

    if-eqz p4, :cond_7

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 147
    :cond_7
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;

    if-eqz p4, :cond_8

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 148
    :cond_8
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;

    if-eqz p4, :cond_9

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$7;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$7;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 149
    :cond_9
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;

    if-eqz p4, :cond_a

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 150
    :cond_a
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    if-eqz p4, :cond_b

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$8;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$8;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 151
    :cond_b
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;

    if-eqz p4, :cond_c

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 152
    :cond_c
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    if-eqz p4, :cond_d

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0, p3}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_0

    .line 153
    :cond_d
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;

    if-eqz p4, :cond_e

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$9;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$9;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 154
    :cond_e
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;

    if-eqz p4, :cond_f

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;Landroid/content/Context;)Landroid/webkit/WebView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    goto :goto_0

    .line 155
    :cond_f
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;

    if-eqz p4, :cond_10

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$10;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$10;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 156
    :cond_10
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;

    if-eqz p4, :cond_11

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$11;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$11;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 157
    :cond_11
    instance-of p4, p1, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;

    if-eqz p4, :cond_12

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$12;

    invoke-direct {p4, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$1$12;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1, v0, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 158
    :cond_12
    instance-of p3, p1, Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;

    if-eqz p3, :cond_13

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_13
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
