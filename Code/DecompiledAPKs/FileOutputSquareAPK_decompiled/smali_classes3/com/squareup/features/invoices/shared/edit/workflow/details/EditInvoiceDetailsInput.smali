.class public final Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "",
        "invoiceDetailsInfo",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "disableId",
        "",
        "forEstimate",
        "(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)V",
        "getDisableId",
        "()Z",
        "getForEstimate",
        "getInvoiceDetailsInfo",
        "()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disableId:Z

.field private final forEstimate:Z

.field private final invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)V
    .locals 1

    const-string v0, "invoiceDetailsInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    iput-boolean p2, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    iput-boolean p3, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZILjava/lang/Object;)Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->copy(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;
    .locals 1

    const-string v0, "invoiceDetailsInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    iget-object v1, p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    iget-boolean v1, p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    iget-boolean p1, p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDisableId()Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    return v0
.end method

.method public final getForEstimate()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    return v0
.end method

.method public final getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditInvoiceDetailsInput(invoiceDetailsInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->invoiceDetailsInfo:Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", disableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->disableId:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", forEstimate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->forEstimate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
