.class public final Lcom/squareup/features/invoices/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final add_customer_row_icon:I = 0x7f080077

.field public static final add_item_row_icon:I = 0x7f080078

.field public static final add_recipient_row_icon:I = 0x7f080079

.field public static final bell:I = 0x7f0800ae

.field public static final blank_file:I = 0x7f0800b1

.field public static final card_chip:I = 0x7f0800cf

.field public static final card_chip_disabled:I = 0x7f0800d0

.field public static final cart_entry_discount_icon:I = 0x7f0800d3

.field public static final cart_entry_tax_icon:I = 0x7f0800d4

.field public static final cart_entry_total_icon:I = 0x7f0800d5

.field public static final crossed_out_bell:I = 0x7f080116

.field public static final customer_circle_background:I = 0x7f080117

.field public static final details_row_icon:I = 0x7f08011f

.field public static final hand_coin:I = 0x7f0801ee

.field public static final hand_coin_pound:I = 0x7f0801ef

.field public static final hand_coin_yen:I = 0x7f0801f0

.field public static final invoice_calendar_divider:I = 0x7f080279

.field public static final invoices_app_color:I = 0x7f08027d

.field public static final invoices_app_icon:I = 0x7f08027e

.field public static final no_invoices:I = 0x7f08039f

.field public static final overflow_menu:I = 0x7f080423

.field public static final pdf_icon:I = 0x7f08043f

.field public static final reminders_row_icon:I = 0x7f08047c

.field public static final send_row_icon:I = 0x7f080494

.field public static final share_row_icon:I = 0x7f08049d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
