.class public final Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactoryKt;
.super Ljava/lang/Object;
.source "InvoiceV2CartEntryViewsFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "config",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;",
        "res",
        "Lcom/squareup/util/Res;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$config(Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactoryKt;->config(Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    move-result-object p0

    return-object p0
.end method

.method private static final config(Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;
    .locals 12

    .line 24
    new-instance v11, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    .line 25
    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p0

    int-to-float v1, p0

    .line 26
    new-instance v2, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_cyan:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 27
    new-instance v3, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 28
    new-instance v4, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v4, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 29
    new-instance v5, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v5, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 30
    new-instance v6, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v6, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 31
    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v7, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 32
    new-instance v8, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v8, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 33
    new-instance v9, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_dark_gray:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v9, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    .line 34
    new-instance v10, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget p0, Lcom/squareup/features/invoices/widgets/impl/R$color;->invoices_cart_cyan:I

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v10, p0, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    move-object v0, v11

    .line 24
    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;-><init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V

    return-object v11
.end method
