.class final Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DeliveryMethodState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 51
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    .line 52
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    if-eqz v1, :cond_0

    .line 53
    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 54
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentInstrumentToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentInstrumentToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 58
    :cond_0
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    if-eqz v1, :cond_1

    .line 59
    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 60
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;->getInstrumentToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$BadState;->getInstrumentToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 64
    :cond_1
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    if-eqz v1, :cond_2

    .line 65
    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 66
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 67
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstruments()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    .line 68
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getSelectedInstrumentIndex()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 69
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstrumentsChanged()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 70
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 71
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$takeSnapshot$1;->this$0:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getMessage()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    :cond_2
    :goto_0
    return-void
.end method
