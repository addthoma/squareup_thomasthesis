.class public final Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;
.super Lcom/squareup/features/invoices/widgets/SectionElement;
.source "InvoiceSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/SectionElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CartEntryData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0005H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "lineItemClickedEvent",
        "",
        "addItemClickedEvent",
        "(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)V",
        "getAddItemClickedEvent",
        "()Ljava/lang/Object;",
        "getCart",
        "()Lcom/squareup/protos/client/bills/Cart;",
        "getLineItemClickedEvent",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addItemClickedEvent:Ljava/lang/Object;

.field private final cart:Lcom/squareup/protos/client/bills/Cart;

.field private final lineItemClickedEvent:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "cart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemClickedEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addItemClickedEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/widgets/SectionElement;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 139
    sget-object p2, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 140
    sget-object p3, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;-><init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->copy(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final component2()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    return-object v0
.end method

.method public final component3()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;
    .locals 1

    const-string v0, "cart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemClickedEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addItemClickedEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;-><init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddItemClickedEvent()Ljava/lang/Object;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    return-object v0
.end method

.method public final getCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final getLineItemClickedEvent()Ljava/lang/Object;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CartEntryData(cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lineItemClickedEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->lineItemClickedEvent:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", addItemClickedEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->addItemClickedEvent:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
