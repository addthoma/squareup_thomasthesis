.class public final Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
.super Lcom/squareup/features/invoices/widgets/SectionElement;
.source "InvoiceSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/SectionElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ToggleData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00052\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0007H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "label",
        "",
        "checked",
        "",
        "event",
        "",
        "description",
        "",
        "(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V",
        "getChecked",
        "()Z",
        "getDescription",
        "()Ljava/lang/CharSequence;",
        "getEvent",
        "()Ljava/lang/Object;",
        "getLabel",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checked:Z

.field private final description:Ljava/lang/CharSequence;

.field private final event:Ljava/lang/Object;

.field private final label:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 151
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/widgets/SectionElement;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    .line 149
    sget-object p3, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 150
    check-cast p4, Ljava/lang/CharSequence;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->copy(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    return v0
.end method

.method public final component3()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    return-object v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    iget-boolean v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChecked()Z
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    return v0
.end method

.method public final getDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getEvent()Ljava/lang/Object;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ToggleData(label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", checked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->checked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->event:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->description:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
