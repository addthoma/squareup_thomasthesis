.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt;
.super Ljava/lang/Object;
.source "InvoiceSectionContainerView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceSectionContainerView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceSectionContainerView.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,56:1\n1642#2,2:57\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceSectionContainerView.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt\n*L\n41#1,2:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a2\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "setOn",
        "",
        "",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "container",
        "Landroid/view/ViewGroup;",
        "viewFactory",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "useV2Widgets",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;Lcom/squareup/features/invoices/widgets/EventHandler;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;",
            "Lcom/squareup/features/invoices/widgets/EventHandler;",
            "Z)V"
        }
    .end annotation

    const-string v0, "$this$setOn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 41
    check-cast p0, Ljava/lang/Iterable;

    .line 57
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 43
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "container.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0, v1, p3, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;->create(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;->asView()Landroid/view/View;

    move-result-object v0

    .line 42
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static synthetic setOn$default(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;Lcom/squareup/features/invoices/widgets/EventHandler;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 38
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt;->setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;Lcom/squareup/features/invoices/widgets/EventHandler;Z)V

    return-void
.end method
