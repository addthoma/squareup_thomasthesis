.class public final Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;
.super Ljava/lang/Object;
.source "SupportedFileFormats.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\u001a\n\u0010\t\u001a\u00020\n*\u00020\u0002\"\u0017\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0004\"\u000e\u0010\u0005\u001a\u00020\u0002X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0002X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0002X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0002X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "ANDROID_SUPPORTED_IMAGE_FORMATS",
        "",
        "",
        "getANDROID_SUPPORTED_IMAGE_FORMATS",
        "()Ljava/util/Set;",
        "JPEG_EXTENSION",
        "JPEG_MIME_TYPE",
        "PDF_EXTENSION",
        "PDF_MIME_TYPE",
        "isSupportedImageMimeType",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ANDROID_SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final JPEG_EXTENSION:Ljava/lang/String; = "jpg"

.field public static final JPEG_MIME_TYPE:Ljava/lang/String; = "image/jpeg"

.field public static final PDF_EXTENSION:Ljava/lang/String; = "pdf"

.field public static final PDF_MIME_TYPE:Ljava/lang/String; = "application/pdf"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "image/jpeg"

    const-string v1, "image/png"

    const-string v2, "image/bmp"

    const-string v3, "image/gif"

    .line 13
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->ANDROID_SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    return-void
.end method

.method public static final getANDROID_SUPPORTED_IMAGE_FORMATS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->ANDROID_SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    return-object v0
.end method

.method public static final isSupportedImageMimeType(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "$this$isSupportedImageMimeType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->ANDROID_SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method
