.class public abstract Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;
.super Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
.source "InvoiceAttachmentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ShowError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;,
        Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorDownloading;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0012\u0010\r\u001a\u00020\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u0012X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014\u0082\u0001\u0002\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "()V",
        "attachmentStatus",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "getAttachmentStatus",
        "()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "errorBody",
        "",
        "getErrorBody",
        "()Ljava/lang/String;",
        "errorTitle",
        "getErrorTitle",
        "fileMetadata",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "getFileMetadata",
        "()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "getInvoiceTokenType",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "ErrorDownloading",
        "ErrorUploading",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorDownloading;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 85
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;
.end method

.method public abstract getErrorBody()Ljava/lang/String;
.end method

.method public abstract getErrorTitle()Ljava/lang/String;
.end method

.method public abstract getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;
.end method

.method public abstract getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;
.end method
