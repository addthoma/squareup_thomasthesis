.class public abstract Lcom/squareup/itemsorter/SortableDiningOption;
.super Ljava/lang/Object;
.source "SortableDiningOption.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008&\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u0000H\u0096\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006R\u0012\u0010\t\u001a\u00020\nX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "",
        "()V",
        "id",
        "",
        "getId",
        "()Ljava/lang/String;",
        "name",
        "getName",
        "ordinal",
        "",
        "getOrdinal",
        "()I",
        "compareTo",
        "other",
        "itemsorter"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/itemsorter/SortableDiningOption;)I
    .locals 3

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {}, Lcom/squareup/util/ComparisonChain;->start()Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/itemsorter/SortableDiningOption;->getOrdinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/itemsorter/SortableDiningOption;->getOrdinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(II)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 20
    invoke-virtual {p0}, Lcom/squareup/itemsorter/SortableDiningOption;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-virtual {p1}, Lcom/squareup/itemsorter/SortableDiningOption;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/itemsorter/SortableDiningOption;->getId()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-virtual {p1}, Lcom/squareup/itemsorter/SortableDiningOption;->getId()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lcom/squareup/util/ComparisonChain;->result()I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/itemsorter/SortableDiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/itemsorter/SortableDiningOption;->compareTo(Lcom/squareup/itemsorter/SortableDiningOption;)I

    move-result p1

    return p1
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getOrdinal()I
.end method
