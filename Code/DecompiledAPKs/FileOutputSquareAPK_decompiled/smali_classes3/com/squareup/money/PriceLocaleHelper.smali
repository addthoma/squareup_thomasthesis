.class public Lcom/squareup/money/PriceLocaleHelper;
.super Ljava/lang/Object;
.source "PriceLocaleHelper.java"

# interfaces
.implements Lcom/squareup/money/MoneyExtractor;


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private watcher:Lcom/squareup/text/ScrubbingTextWatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .param p2    # Lcom/squareup/money/MoneyDigitsKeyListenerFactory;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    .line 28
    iput-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    .line 29
    iput-object p3, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 30
    iput-object p4, p0, Lcom/squareup/money/PriceLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    return-object p1
.end method

.method public configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 1

    const-string v0, ""

    .line 38
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    return-object p1
.end method

.method public configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    iget-object v1, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 44
    invoke-virtual {v1, p3}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-interface {v0, v1}, Lcom/squareup/money/MoneyDigitsKeyListenerFactory;->getDigitsKeyListener(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 45
    new-instance v0, Lcom/squareup/money/PerUnitScrubber;

    iget-object v1, p0, Lcom/squareup/money/PriceLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v2, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/squareup/money/PerUnitScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)V

    .line 47
    new-instance p2, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {p2, v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 48
    iget-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    return-object p1
.end method

.method public extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, ""

    .line 82
    invoke-virtual {p0, p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public extractMoney(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->trimUnitSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 87
    iget-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    invoke-virtual {p2, p1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public removeScrubbingTextWatcher(Lcom/squareup/text/HasSelectableText;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 p1, 0x0

    .line 56
    iput-object p1, p0, Lcom/squareup/money/PriceLocaleHelper;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    return-void
.end method

.method public setHint(Landroid/widget/TextView;I)V
    .locals 3

    .line 70
    invoke-virtual {p1}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object p2, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/squareup/money/PriceLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(I)V

    :goto_0
    return-void
.end method

.method public setHintToZeroMoney(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, ""

    .line 61
    invoke-virtual {p0, p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method public setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p0, Lcom/squareup/money/PriceLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method
