.class public final Lcom/squareup/money/MoneyLocaleHelper;
.super Ljava/lang/Object;
.source "MoneyLocaleHelper.kt"

# interfaces
.implements Lcom/squareup/money/MoneyExtractor;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMoneyLocaleHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MoneyLocaleHelper.kt\ncom/squareup/money/MoneyLocaleHelper\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\u0008\u0001\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u0004H\u0007J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0016\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u000bR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "Lcom/squareup/money/MoneyExtractor;",
        "moneyLocaleScrubberProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/text/SelectableTextScrubber;",
        "moneyLocaleDigitsKeyListener",
        "Landroid/text/method/DigitsKeyListener;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;)V",
        "configure",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "textView",
        "Lcom/squareup/text/HasSelectableText;",
        "scrubber",
        "extractMoney",
        "Lcom/squareup/protos/common/Money;",
        "text",
        "",
        "unconfigure",
        "",
        "watcher",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .param p2    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyLocaleScrubberProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleDigitsKeyListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/MoneyLocaleHelper;->moneyLocaleScrubberProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/money/MoneyLocaleHelper;->moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/money/MoneyLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static synthetic configure$default(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;ILjava/lang/Object;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 32
    iget-object p2, p0, Lcom/squareup/money/MoneyLocaleHelper;->moneyLocaleScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    const-string p3, "moneyLocaleScrubberProvider.get()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/text/SelectableTextScrubber;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/money/MoneyLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/money/MoneyLocaleHelper;->configure$default(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;ILjava/lang/Object;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    return-object p1
.end method

.method public final configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;)Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 1

    const-string/jumbo v0, "textView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleHelper;->moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/method/KeyListener;

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 35
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {v0, p2, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 36
    move-object p2, v0

    check-cast p2, Landroid/text/TextWatcher;

    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 37
    move-object p2, v0

    check-cast p2, Lcom/squareup/text/SelectionWatcher;

    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-object v0
.end method

.method public extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyLocaleHelperKt;->extractMoney(Ljava/lang/CharSequence;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public final unconfigure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/ScrubbingTextWatcher;)V
    .locals 1

    const-string/jumbo v0, "textView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "watcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    move-object v0, p2

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 50
    check-cast p2, Lcom/squareup/text/SelectionWatcher;

    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method
