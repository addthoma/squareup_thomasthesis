.class public final Lcom/squareup/money/CompactMoneyFormatter;
.super Lcom/squareup/text/CompactFormatter;
.source "CompactMoneyFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/money/CompactMoneyFormatter$Mode;,
        Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;,
        Lcom/squareup/money/CompactMoneyFormatter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/text/CompactFormatter<",
        "Lcom/squareup/protos/common/Money;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompactMoneyFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompactMoneyFormatter.kt\ncom/squareup/money/CompactMoneyFormatter\n*L\n1#1,151:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00182\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0018\u0019\u001aB\'\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0002H\u0016J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0014R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u000e\u001a\u00020\u000f*\u00020\u00028TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/money/CompactMoneyFormatter;",
        "Lcom/squareup/text/CompactFormatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "mode",
        "Lcom/squareup/money/CompactMoneyFormatter$Mode;",
        "(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;)V",
        "decimalFormatCacheThreadLocal",
        "Ljava/lang/ThreadLocal;",
        "Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;",
        "asBigDecimal",
        "Ljava/math/BigDecimal;",
        "getAsBigDecimal",
        "(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;",
        "format",
        "",
        "money",
        "formatterScale",
        "Lcom/squareup/text/CompactFormatter$FormatterScale;",
        "value",
        "Companion",
        "DecimalFormatCache",
        "Mode",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/money/CompactMoneyFormatter$Companion;

.field private static final DEFAULT_COMPACT_FRACTIONAL_DIGITS:I = 0x2


# instance fields
.field private final decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mode:Lcom/squareup/money/CompactMoneyFormatter$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/money/CompactMoneyFormatter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/money/CompactMoneyFormatter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/money/CompactMoneyFormatter;->Companion:Lcom/squareup/money/CompactMoneyFormatter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/money/CompactMoneyFormatter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/CompactMoneyFormatter$Mode;",
            ")V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/text/CompactFormatter;-><init>(Lcom/squareup/util/Res;)V

    iput-object p2, p0, Lcom/squareup/money/CompactMoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/money/CompactMoneyFormatter;->mode:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    .line 61
    new-instance p1, Ljava/lang/ThreadLocal;

    invoke-direct {p1}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/CompactMoneyFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 44
    sget-object p3, Lcom/squareup/money/CompactMoneyFormatter$Mode;->DEFAULT:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/money/CompactMoneyFormatter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;)V

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 7

    if-nez p1, :cond_0

    const-string p1, ""

    .line 76
    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    .line 78
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 79
    iget-object v1, p0, Lcom/squareup/money/CompactMoneyFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;

    .line 80
    iget-object v2, p0, Lcom/squareup/money/CompactMoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    const-string v3, "currencyCode"

    if-eqz v1, :cond_1

    .line 81
    invoke-virtual {v1}, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    if-ne v0, v4, :cond_1

    invoke-virtual {v1}, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    .line 82
    :cond_1
    invoke-static {v2, v0}, Lcom/squareup/currency_db/NumberFormats;->getCurrencyFormat(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/text/DecimalFormat;

    move-result-object v1

    .line 83
    new-instance v4, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "locale"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "decimalFormat"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v0, v2, v1}, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;-><init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Ljava/text/DecimalFormat;)V

    .line 84
    iget-object v1, p0, Lcom/squareup/money/CompactMoneyFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    move-object v1, v4

    .line 86
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;->getDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v2

    .line 89
    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->needsTruncation(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x2

    if-eqz v4, :cond_3

    .line 93
    iget-object v0, p0, Lcom/squareup/money/CompactMoneyFormatter;->mode:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    invoke-virtual {v0}, Lcom/squareup/money/CompactMoneyFormatter$Mode;->getMinimumFractionDigitsWhenTruncated()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 94
    invoke-virtual {v2, v5}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 96
    sget-object v0, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    goto :goto_0

    .line 98
    :cond_3
    iget-object v4, p0, Lcom/squareup/money/CompactMoneyFormatter;->mode:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    sget-object v6, Lcom/squareup/money/CompactMoneyFormatter$Mode;->SHORTER:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    if-ne v4, v6, :cond_4

    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->getAsBigDecimal(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/util/BigDecimals;->isIntegral(Ljava/math/BigDecimal;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v0, 0x0

    .line 101
    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 102
    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 104
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    goto :goto_0

    .line 107
    :cond_4
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/CompactMoneyFormatterKt;->access$getFractionDigits$p(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    .line 109
    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 110
    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 112
    sget-object v0, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    .line 116
    :goto_0
    invoke-virtual {p0, p1, v5}, Lcom/squareup/money/CompactMoneyFormatter;->truncatedValueAndScale(Ljava/lang/Object;I)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    .line 117
    invoke-virtual {v1}, Lcom/squareup/money/CompactMoneyFormatter$DecimalFormatCache;->getDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v1

    .line 118
    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "formattedAsCurrencyWithoutAbbreviation"

    .line 120
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->abbreviate(Ljava/lang/String;Lcom/squareup/text/CompactFormatter$FormatterScale;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method protected formatterScale(Lcom/squareup/protos/common/Money;)Lcom/squareup/text/CompactFormatter$FormatterScale;
    .locals 3

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string/jumbo v1, "value.currency_code"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/CompactMoneyFormatterKt;->access$getFractionDigits$p(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    if-gtz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->getAsBigDecimal(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v0

    const-string/jumbo v1, "value.asBigDecimal.abs()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/32 v1, 0xf4240

    invoke-static {v0, v1, v2}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;J)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_1

    .line 138
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/squareup/text/CompactFormatter;->formatterScale(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic formatterScale(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->formatterScale(Lcom/squareup/protos/common/Money;)Lcom/squareup/text/CompactFormatter$FormatterScale;

    move-result-object p1

    return-object p1
.end method

.method protected getAsBigDecimal(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$asBigDecimal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->asDecimalWholeUnits(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;

    move-result-object p1

    const-string v0, "MoneyMath.asDecimalWholeUnits(this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic getAsBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/money/CompactMoneyFormatter;->getAsBigDecimal(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method
