.class public abstract Lcom/squareup/money/MoneyModule;
.super Ljava/lang/Object;
.source "MoneyModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCompactAccountingFormatter(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 1
    .param p0    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/CompactMoney;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/money/CompactAccountingFormat;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/money/AccountingFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/AccountingFormatter;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method static provideCompactMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Lcom/squareup/money/CompactMoney;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/money/CompactMoneyFormatter;

    invoke-direct {v0, p1, p0}, Lcom/squareup/money/CompactMoneyFormatter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method static provideCompactShorterAccountingFormatter(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 1
    .param p0    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/CompactShorterMoney;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/money/CompactShorterAccountingFormat;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/money/AccountingFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/AccountingFormatter;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method static provideCompactShorterMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Lcom/squareup/money/CompactShorterMoney;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/money/CompactMoneyFormatter;

    sget-object v1, Lcom/squareup/money/CompactMoneyFormatter$Mode;->SHORTER:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    invoke-direct {v0, p1, p0, v1}, Lcom/squareup/money/CompactMoneyFormatter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/money/CompactMoneyFormatter$Mode;)V

    return-object v0
.end method

.method static provideMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/money/MoneyFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)V

    return-object v0
.end method

.method static provideMoneyLocaleFormatter()Lcom/squareup/money/MoneyLocaleFormatter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/money/MoneyLocaleFormatter;

    invoke-direct {v0}, Lcom/squareup/money/MoneyLocaleFormatter;-><init>()V

    return-object v0
.end method

.method static provideRealCentsMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Lcom/squareup/money/Cents;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/money/CentsMoneyFormatter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/CentsMoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)V

    return-object v0
.end method

.method static provideShortMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/money/MoneyFormatter;

    sget-object v1, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->SHORTER:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V

    return-object v0
.end method

.method static provideTaxPercentageFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Lcom/squareup/money/ForTaxPercentage;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 71
    new-instance v0, Lcom/squareup/money/TaxPercentageFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/TaxPercentageFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method abstract bindAccountingFormatter(Lcom/squareup/money/AccountingFormatter;)Lcom/squareup/text/Formatter;
    .annotation runtime Lcom/squareup/money/AccountingFormat;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/AccountingFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end method

.method abstract bindPlusSignFormatter(Lcom/squareup/money/PositiveNegativeFormatter;)Lcom/squareup/text/Formatter;
    .annotation runtime Lcom/squareup/money/PositiveNegativeFormat;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/PositiveNegativeFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end method
