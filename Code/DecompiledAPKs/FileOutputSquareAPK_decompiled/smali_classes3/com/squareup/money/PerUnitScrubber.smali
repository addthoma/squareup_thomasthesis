.class public final Lcom/squareup/money/PerUnitScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "PerUnitScrubber.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPerUnitScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PerUnitScrubber.kt\ncom/squareup/money/PerUnitScrubber\n*L\n1#1,26:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00020\rH\u0014J\u0008\u0010\u000e\u001a\u00020\tH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/money/PerUnitScrubber;",
        "Lcom/squareup/money/WholeUnitAmountScrubber;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "formatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "zeroState",
        "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
        "unitAbbreviation",
        "",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)V",
        "formatAmount",
        "proposedAmount",
        "",
        "nonSelectableSuffix",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final formatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final unitAbbreviation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)V
    .locals 2

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zeroState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitAbbreviation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-wide v0, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    invoke-direct {p0, v0, v1, p3}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    iput-object p1, p0, Lcom/squareup/money/PerUnitScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/money/PerUnitScrubber;->formatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p4, p0, Lcom/squareup/money/PerUnitScrubber;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/money/PerUnitScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 14
    iget-object p2, p0, Lcom/squareup/money/PerUnitScrubber;->formatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/money/PerUnitScrubber;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 15
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected nonSelectableSuffix()Ljava/lang/String;
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/money/PerUnitScrubber;->unitAbbreviation:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 21
    iget-object v0, p0, Lcom/squareup/money/PerUnitScrubber;->formatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p0, Lcom/squareup/money/PerUnitScrubber;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v0, ""

    return-object v0
.end method
