.class public final Lcom/squareup/money/TaxPercentageFormatter;
.super Lcom/squareup/text/PercentageFormatter;
.source "TaxPercentageFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/money/TaxPercentageFormatter;",
        "Lcom/squareup/text/PercentageFormatter;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Ljavax/inject/Provider;Lcom/squareup/util/Res;)V",
        "format",
        "",
        "percentage",
        "Lcom/squareup/util/Percentage;",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;)V

    iput-object p1, p0, Lcom/squareup/money/TaxPercentageFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/money/TaxPercentageFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/money/TaxPercentageFormatter;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public format(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/squareup/money/TaxPercentageFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/proto_utilities/R$string;->percent_character_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/squareup/money/TaxPercentageFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v1}, Lcom/squareup/currency_db/NumberFormats;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 23
    invoke-static {p1, v1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;Ljava/text/NumberFormat;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
