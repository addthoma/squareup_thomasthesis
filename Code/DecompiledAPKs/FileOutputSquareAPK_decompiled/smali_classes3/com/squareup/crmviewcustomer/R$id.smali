.class public final Lcom/squareup/crmviewcustomer/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmviewcustomer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final crm_activity_list_section:I = 0x7f0a03de

.field public static final crm_additional_info_section:I = 0x7f0a03e5

.field public static final crm_appointments_list:I = 0x7f0a03ee

.field public static final crm_bill_history_view:I = 0x7f0a03f0

.field public static final crm_bottom_button:I = 0x7f0a03f6

.field public static final crm_bottom_button_row:I = 0x7f0a03f7

.field public static final crm_buyer_summary_section:I = 0x7f0a03f8

.field public static final crm_contact_list:I = 0x7f0a040f

.field public static final crm_contact_list_v2:I = 0x7f0a0412

.field public static final crm_content_container:I = 0x7f0a0413

.field public static final crm_create_customer_from_search_term_button:I = 0x7f0a041a

.field public static final crm_customer_lookup:I = 0x7f0a0429

.field public static final crm_drop_down_container:I = 0x7f0a0438

.field public static final crm_frequent_items_section_header:I = 0x7f0a044f

.field public static final crm_frequent_items_section_rows:I = 0x7f0a0450

.field public static final crm_frequent_items_section_view_all_button:I = 0x7f0a0451

.field public static final crm_invoice_list:I = 0x7f0a0464

.field public static final crm_invoice_progress_bar:I = 0x7f0a0465

.field public static final crm_invoices_summary_section:I = 0x7f0a0466

.field public static final crm_notes_all_notes:I = 0x7f0a04aa

.field public static final crm_notes_rows:I = 0x7f0a04ab

.field public static final crm_past_appointments_section:I = 0x7f0a04ad

.field public static final crm_personal_information_section:I = 0x7f0a04ae

.field public static final crm_profile_action_add_card_on_file:I = 0x7f0a04b1

.field public static final crm_profile_action_add_note:I = 0x7f0a04b2

.field public static final crm_profile_action_delete_customer:I = 0x7f0a04b3

.field public static final crm_profile_action_edit:I = 0x7f0a04b4

.field public static final crm_profile_action_exposed_action:I = 0x7f0a04b5

.field public static final crm_profile_action_merge_customer:I = 0x7f0a04b6

.field public static final crm_profile_action_new_sale:I = 0x7f0a04b7

.field public static final crm_profile_action_send_message:I = 0x7f0a04b8

.field public static final crm_profile_action_upload_file:I = 0x7f0a04b9

.field public static final crm_profile_container:I = 0x7f0a04c1

.field public static final crm_profile_footer_add_or_remove_button:I = 0x7f0a04c2

.field public static final crm_profile_footer_button_container:I = 0x7f0a04c3

.field public static final crm_profile_footer_message_button:I = 0x7f0a04c4

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_recent_contacts:I = 0x7f0a04cd

.field public static final crm_recent_progress_bar:I = 0x7f0a04ce

.field public static final crm_recent_title:I = 0x7f0a04cf

.field public static final crm_review_for_merging_container:I = 0x7f0a04de

.field public static final crm_savecard_new_customer:I = 0x7f0a04e5

.field public static final crm_search_box:I = 0x7f0a04e7

.field public static final crm_search_contacts:I = 0x7f0a04e8

.field public static final crm_search_create_new_button:I = 0x7f0a04e9

.field public static final crm_search_message:I = 0x7f0a04ea

.field public static final crm_search_progress_bar:I = 0x7f0a04eb

.field public static final crm_section_card_on_file:I = 0x7f0a04ed

.field public static final crm_section_files:I = 0x7f0a04ee

.field public static final crm_section_frequent_items:I = 0x7f0a04ef

.field public static final crm_section_header:I = 0x7f0a04f0

.field public static final crm_section_loyalty:I = 0x7f0a04f1

.field public static final crm_section_notes:I = 0x7f0a04f2

.field public static final crm_section_rewards:I = 0x7f0a04f3

.field public static final crm_section_rows:I = 0x7f0a04f4

.field public static final crm_simple_section_cta_button:I = 0x7f0a04fa

.field public static final crm_simple_section_header:I = 0x7f0a04fb

.field public static final crm_simple_section_rows:I = 0x7f0a04fc

.field public static final crm_upcoming_appointments_section:I = 0x7f0a0507

.field public static final crm_view_customer:I = 0x7f0a050f

.field public static final crm_view_customer_drop_down:I = 0x7f0a0510

.field public static final customer_all_appointments_view:I = 0x7f0a0532

.field public static final customer_invoice_view:I = 0x7f0a0539

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final loyalty_dropdown_adjust_status:I = 0x7f0a0974

.field public static final loyalty_dropdown_copy_phone:I = 0x7f0a0975

.field public static final loyalty_dropdown_delete_account:I = 0x7f0a0976

.field public static final loyalty_dropdown_edit_phone:I = 0x7f0a0977

.field public static final loyalty_dropdown_send_status:I = 0x7f0a0978

.field public static final loyalty_dropdown_transfer_account:I = 0x7f0a0979

.field public static final loyalty_dropdown_view_expiring_points:I = 0x7f0a097a

.field public static final loyalty_phone_number:I = 0x7f0a0980

.field public static final loyalty_phone_update_hint:I = 0x7f0a0981

.field public static final loyalty_section_header:I = 0x7f0a0995

.field public static final loyalty_section_points:I = 0x7f0a0996

.field public static final loyalty_section_rows:I = 0x7f0a0997

.field public static final loyalty_section_see_all_rewards:I = 0x7f0a0998

.field public static final non_stable_action_bar:I = 0x7f0a0a44

.field public static final profile_section_header:I = 0x7f0a0c79

.field public static final profile_section_rows:I = 0x7f0a0c7a

.field public static final redeem_reward_tiers_list:I = 0x7f0a0d22

.field public static final rewards_section_bottom_button:I = 0x7f0a0d89

.field public static final rewards_section_coupon_rows:I = 0x7f0a0d8a

.field public static final rewards_section_header:I = 0x7f0a0d8b

.field public static final update_loyalty_phone_view_root:I = 0x7f0a10c4

.field public static final view_all_button:I = 0x7f0a10ee

.field public static final view_contents:I = 0x7f0a10f4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
