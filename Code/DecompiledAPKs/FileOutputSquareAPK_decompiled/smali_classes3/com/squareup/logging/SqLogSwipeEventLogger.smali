.class public Lcom/squareup/logging/SqLogSwipeEventLogger;
.super Ljava/lang/Object;
.source "SqLogSwipeEventLogger.java"

# interfaces
.implements Lcom/squareup/logging/SwipeEventLogger;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public logReaderCarrierDetectEvent(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V
    .locals 3

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event: "

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", is_early_packet: "

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19
    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    if-ne v1, v2, :cond_0

    const-string v1, "\nLink Type: "

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", classified as: "

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iget-object v1, v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", decision: "

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 25
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
