.class final Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;
.super Ljava/lang/Object;
.source "BluetoothAclConnectionReceiver.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/logging/BluetoothAclConnectionReceiver;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "intent",
        "Landroid/content/Intent;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/logging/BluetoothAclConnectionReceiver;


# direct methods
.method constructor <init>(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;->this$0:Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/content/Intent;)V
    .locals 5

    const-string v0, "intent"

    .line 28
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 29
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x11f77b4b

    const-string/jumbo v3, "wirelessConnection"

    const-string v4, "android.bluetooth.device.extra.DEVICE"

    if-eq v1, v2, :cond_2

    const v2, 0x6c9330ef

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    .line 41
    invoke-static {p1}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object p1

    .line 44
    iget-object v0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;->this$0:Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    invoke-static {v0}, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->access$getCardReaderListeners$p(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/squareup/dipper/events/AclDisconnectedEvent;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/dipper/events/AclDisconnectedEvent;-><init>(Lcom/squareup/cardreader/WirelessConnection;)V

    check-cast v1, Lcom/squareup/dipper/events/CardReaderDataEvent;

    .line 44
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V

    goto :goto_0

    :cond_2
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 33
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    .line 32
    invoke-static {p1}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object p1

    .line 35
    iget-object v0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;->this$0:Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    invoke-static {v0}, Lcom/squareup/logging/BluetoothAclConnectionReceiver;->access$getCardReaderListeners$p(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/dipper/events/AclConnectedEvent;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/squareup/dipper/events/AclConnectedEvent;-><init>(Lcom/squareup/cardreader/WirelessConnection;Z)V

    check-cast v1, Lcom/squareup/dipper/events/CardReaderDataEvent;

    .line 35
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/squareup/logging/BluetoothAclConnectionReceiver$onEnterScope$1;->call(Landroid/content/Intent;)V

    return-void
.end method
