.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/Map$Entry<",
        "Ljava/lang/String;",
        "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        ">;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\'\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0008\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "",
        "Lcom/squareup/ordermanagerdata/local/OrderId;",
        "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;

    invoke-direct {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;-><init>()V

    sput-object v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;->INSTANCE:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;->invoke(Ljava/util/Map$Entry;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/util/Map$Entry;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
