.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a.\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "result",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 465
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->access$getLocalOrderStore$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    move-result-object v0

    .line 467
    move-object v1, p1

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "result.response"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/orders/model/Order;

    invoke-interface {v0, v1}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->updateOrder(Lcom/squareup/orders/model/Order;)Lio/reactivex/Completable;

    move-result-object v0

    .line 468
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-virtual {v0, p1}, Lio/reactivex/Completable;->andThen(Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "localOrderStore\n        \u2026Then(Single.just(result))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 470
    :cond_0
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(result)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;->apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
