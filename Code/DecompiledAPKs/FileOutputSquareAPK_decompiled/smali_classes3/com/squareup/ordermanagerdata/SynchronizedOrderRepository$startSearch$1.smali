.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->startSearch(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $query:Lcom/squareup/ordermanagerdata/SearchQuery;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SearchQuery;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 273
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    .line 274
    new-instance v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object v2, v2, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->orders:Ljava/util/List;

    invoke-direct {v1, v2}, Lcom/squareup/ordermanagerdata/ResultState$Success;-><init>(Ljava/lang/Object;)V

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState;

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    .line 275
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->cursor:Ljava/lang/String;

    const/4 v3, 0x1

    .line 273
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;-><init>(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)V

    goto :goto_0

    .line 279
    :cond_0
    sget-object p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->Companion:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-virtual {p1, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;->genericSearchError(Lcom/squareup/ordermanagerdata/SearchQuery;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object p1

    return-object p1
.end method
