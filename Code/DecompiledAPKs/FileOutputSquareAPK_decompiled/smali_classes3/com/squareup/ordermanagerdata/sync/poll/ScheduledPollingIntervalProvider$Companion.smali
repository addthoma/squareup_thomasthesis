.class public final Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;
.super Ljava/lang/Object;
.source "ScheduledPollingIntervalProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;",
        "",
        "()V",
        "DEFAULT_INTERVAL",
        "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
        "getDEFAULT_INTERVAL",
        "()Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
        "TIME_UNIT",
        "Ljava/util/concurrent/TimeUnit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDEFAULT_INTERVAL()Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;
    .locals 1

    .line 38
    invoke-static {}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->access$getDEFAULT_INTERVAL$cp()Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    move-result-object v0

    return-object v0
.end method
