.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->loadNextPageOfCompletedOrders()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a,\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00050\u00020\u00012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/ResultState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->cursor:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->access$setCompletedOrdersPaginationToken$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;Ljava/lang/String;)V

    .line 246
    new-instance v0, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->orders:Ljava/util/List;

    invoke-direct {v0, p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/ordermanagerdata/ResultState;

    goto :goto_0

    .line 249
    :cond_0
    sget-object p1, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ordermanagerdata/ResultState;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object p1

    return-object p1
.end method
