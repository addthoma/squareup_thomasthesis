.class public final Lcom/squareup/edititem/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/edititem/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final editItemItemOptionAndValueRowStyle:I = 0x7f04014c

.field public static final labelPaddingLeft:I = 0x7f040234

.field public static final sectionTitle:I = 0x7f04033c

.field public static final showLabel:I = 0x7f04034b

.field public static final sqHideBorder:I = 0x7f040382

.field public static final sqLabelLayoutWeight:I = 0x7f040392

.field public static final sqOptionNamePhoneAppearance:I = 0x7f04039f

.field public static final sqOptionNameTabletAppearance:I = 0x7f0403a0

.field public static final sqOptionValuesPhoneAppearance:I = 0x7f0403a1

.field public static final sqOptionValuesTabletAppearance:I = 0x7f0403a2

.field public static final stockFieldAlignment:I = 0x7f0403f7

.field public static final stockFieldPaddingLeft:I = 0x7f0403f8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
