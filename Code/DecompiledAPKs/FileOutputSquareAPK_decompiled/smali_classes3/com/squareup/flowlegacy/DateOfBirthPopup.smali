.class public Lcom/squareup/flowlegacy/DateOfBirthPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "DateOfBirthPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/SquareDate;",
        "Lcom/squareup/ui/SquareDate;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/marin/widgets/MarinDatePicker;Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->clearFocus()V

    .line 46
    new-instance p2, Lcom/squareup/ui/SquareDate;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYear()I

    move-result p3

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getMonth()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getDayOfMonth()I

    move-result p0

    invoke-direct {p2, p3, v0, p0}, Lcom/squareup/ui/SquareDate;-><init>(III)V

    .line 46
    invoke-virtual {p1, p2}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/ui/SquareDate;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->createDialog(Lcom/squareup/ui/SquareDate;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/SquareDate;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/SquareDate;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/SquareDate;",
            "Lcom/squareup/ui/SquareDate;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 33
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 34
    sget v0, Lcom/squareup/flowlegacy/R$layout;->date_of_birth_popup:I

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 36
    sget v1, Lcom/squareup/flowlegacy/R$id;->date_picker:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinDatePicker;

    .line 37
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinDatePicker;->setMaxDate(J)V

    .line 38
    iget v2, p1, Lcom/squareup/ui/SquareDate;->year:I

    iget v3, p1, Lcom/squareup/ui/SquareDate;->month:I

    iget p1, p1, Lcom/squareup/ui/SquareDate;->dayOfMonth:I

    invoke-virtual {v1, v2, v3, p1}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    .line 39
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinDatePicker;->setSameWeightForPickers()V

    .line 41
    new-instance p1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$ntcuFQd0uRpJ-oXLDFaYAQ4_9Xs;

    invoke-direct {p2, v1, p3}, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$ntcuFQd0uRpJ-oXLDFaYAQ4_9Xs;-><init>(Lcom/squareup/marin/widgets/MarinDatePicker;Lcom/squareup/mortar/PopupPresenter;)V

    const v0, 0x104000a

    .line 43
    invoke-virtual {p1, v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 49
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$mw8O7AuSa8YAx_4tCQkR7DFGDPY;

    invoke-direct {p2, p3}, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$mw8O7AuSa8YAx_4tCQkR7DFGDPY;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 50
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$wHzJLirjtEkuVpLz8NVbbDSvD10;

    invoke-direct {p2, p3}, Lcom/squareup/flowlegacy/-$$Lambda$DateOfBirthPopup$wHzJLirjtEkuVpLz8NVbbDSvD10;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    const/high16 p3, 0x1040000

    .line 51
    invoke-virtual {p1, p3, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
