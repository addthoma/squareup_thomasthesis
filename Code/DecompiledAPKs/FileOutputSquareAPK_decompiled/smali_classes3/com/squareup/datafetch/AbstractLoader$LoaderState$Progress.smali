.class public final Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;
.super Lcom/squareup/datafetch/AbstractLoader$LoaderState;
.source "AbstractLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/AbstractLoader$LoaderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Progress"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState<",
        "TTInput;TTItem;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0004\u0008\u0004\u0010\u0001*\u0004\u0008\u0005\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B\u0017\u0008\u0017\u0012\u0006\u0010\u0004\u001a\u00028\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u0015\u0012\u0006\u0010\u0004\u001a\u00028\u0004\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000f\u001a\u00028\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0010\u001a\u00020\tH\u00c6\u0003J.\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u00050\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00028\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0012J\u0013\u0010\u0013\u001a\u00020\t2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0016\u0010\u0004\u001a\u00028\u0004X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;",
        "TInput",
        "TItem",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState;",
        "input",
        "pagingParams",
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V",
        "isFirstPage",
        "",
        "(Ljava/lang/Object;Z)V",
        "getInput",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "()Z",
        "component1",
        "component2",
        "copy",
        "(Ljava/lang/Object;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field private final isFirstPage:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            ")V"
        }
    .end annotation

    const-string v0, "pagingParams"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->isFirstPage()Z

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;-><init>(Ljava/lang/Object;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;Z)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 280
    invoke-direct {p0, v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->input:Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;Ljava/lang/Object;ZILjava/lang/Object;)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->copy(Ljava/lang/Object;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    return v0
.end method

.method public final copy(Ljava/lang/Object;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;Z)",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress<",
            "TTInput;TTItem;>;"
        }
    .end annotation

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    invoke-direct {v0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;-><init>(Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    iget-boolean p1, p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getInput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    .line 278
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->input:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isFirstPage()Z
    .locals 1

    .line 279
    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Progress(input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isFirstPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
