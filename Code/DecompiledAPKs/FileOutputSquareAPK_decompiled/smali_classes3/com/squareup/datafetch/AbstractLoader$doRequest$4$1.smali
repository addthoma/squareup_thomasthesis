.class final Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->apply(Lcom/squareup/datafetch/AbstractLoader$PagingParams;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00032\u000e\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "TInput",
        "",
        "TItem",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader$doRequest$4;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 4

    .line 184
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    iget-object p1, p1, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    invoke-static {p1}, Lcom/squareup/datafetch/AbstractLoader;->access$getLoaderState$p(Lcom/squareup/datafetch/AbstractLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    iget-object v1, v1, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->$input:Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    const-string v3, "pagingParams"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 69
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
