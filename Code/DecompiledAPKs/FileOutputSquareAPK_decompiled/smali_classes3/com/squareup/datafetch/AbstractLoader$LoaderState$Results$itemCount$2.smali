.class final Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AbstractLoader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;-><init>(Ljava/lang/Object;Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractLoader.kt\ncom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,321:1\n2073#2,5:322\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractLoader.kt\ncom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2\n*L\n243#1,5:322\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0004\u0008\u0002\u0010\u0002\"\u0004\u0008\u0003\u0010\u0003\"\u0008\u0008\u0004\u0010\u0002*\u00020\u0004\"\u0008\u0008\u0005\u0010\u0003*\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "TInput",
        "TItem",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;->this$0:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()I
    .locals 3

    .line 243
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;->this$0:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 323
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 324
    check-cast v2, Ljava/util/List;

    .line 243
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 238
    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;->invoke()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
