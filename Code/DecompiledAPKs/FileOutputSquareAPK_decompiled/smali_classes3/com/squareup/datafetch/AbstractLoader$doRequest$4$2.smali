.class final Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->apply(Lcom/squareup/datafetch/AbstractLoader$PagingParams;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/lang/Throwable;",
        "Lcom/squareup/datafetch/AbstractLoader$Response<",
        "TTInput;TTItem;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\"\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003 \u0004*\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "TInput",
        "TItem",
        "kotlin.jvm.PlatformType",
        "",
        "error",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader$doRequest$4;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;->this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Throwable;)Lcom/squareup/datafetch/AbstractLoader$Response;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;"
        }
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$Response$Error;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;->this$0:Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    iget-object v1, v1, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->$input:Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;->$pagingParams:Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    const-string v3, "pagingParams"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    invoke-direct {v3, p1}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    check-cast v3, Lcom/squareup/datafetch/LoaderError;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/datafetch/AbstractLoader$Response$Error;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    check-cast v0, Lcom/squareup/datafetch/AbstractLoader$Response;

    return-object v0

    .line 190
    :cond_0
    new-instance v0, Lio/reactivex/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lio/reactivex/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;->apply(Ljava/lang/Throwable;)Lcom/squareup/datafetch/AbstractLoader$Response;

    move-result-object p1

    return-object p1
.end method
