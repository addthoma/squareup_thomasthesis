.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DatabaseImpl.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectIdWithSourceAndState(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "cursor",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;->INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/sqldelight/db/SqlCursor;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectIdWithSourceAndState$1;->invoke(Lcom/squareup/sqldelight/db/SqlCursor;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/sqldelight/db/SqlCursor;)Ljava/lang/String;
    .locals 1

    const-string v0, "cursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 73
    invoke-interface {p1, v0}, Lcom/squareup/sqldelight/db/SqlCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p1
.end method
