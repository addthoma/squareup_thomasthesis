.class public abstract Lcom/squareup/notificationcenterdata/NotificationsReleaseModule;
.super Ljava/lang/Object;
.source "NotificationsReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/communications/CommunicationsDefaultModule;,
        Lcom/squareup/notificationcenterdata/db/NotificationStateStoreModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u000c\u0012\u0004\u0012\u00020\u00050\u0004j\u0002`\u00062\u0006\u0010\u0007\u001a\u00020\u0008H!J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH!J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/NotificationsReleaseModule;",
        "",
        "()V",
        "bindNotificationComparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lcom/squareup/notificationcenterdata/NotificationComparator;",
        "realNotificationComparator",
        "Lcom/squareup/notificationcenterdata/RealNotificationComparator;",
        "bindNotificationsRepository",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
        "realNotificationsRepository",
        "Lcom/squareup/notificationcenterdata/RealNotificationsRepository;",
        "bindRemoteNotificationsSource",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "remoteNotificationsSource",
        "Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindNotificationComparator(Lcom/squareup/notificationcenterdata/RealNotificationComparator;)Ljava/util/Comparator;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/RealNotificationComparator;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end method

.method public abstract bindNotificationsRepository(Lcom/squareup/notificationcenterdata/RealNotificationsRepository;)Lcom/squareup/notificationcenterdata/NotificationsRepository;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRemoteNotificationsSource(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)Lcom/squareup/notificationcenterdata/NotificationsSource;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
