.class public interface abstract Lcom/squareup/notificationcenterdata/NotificationStateStore;
.super Ljava/lang/Object;
.source "NotificationStateStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J$\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u000c0\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u001c\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u000c0\u000b2\u0006\u0010\u0008\u001a\u00020\tH&J\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000b2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "",
        "addNotification",
        "",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "getNotificationIds",
        "Lio/reactivex/Observable;",
        "",
        "getNotificationState",
        "removeNotificationWithId",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
.end method

.method public abstract getNotificationIds(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getNotificationIds(Lcom/squareup/notificationcenterdata/Notification$State;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeNotificationWithId(Ljava/lang/String;)V
.end method
