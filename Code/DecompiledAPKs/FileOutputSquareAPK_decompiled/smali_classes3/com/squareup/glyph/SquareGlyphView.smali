.class public Lcom/squareup/glyph/SquareGlyphView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "SquareGlyphView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/glyph/SquareGlyphView$SavedState;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION_MS:I = 0x96

.field private static final ROTATION_AXIS:Ljava/lang/String; = "rotationY"


# instance fields
.field protected colorStateList:Landroid/content/res/ColorStateList;

.field private flipOut:Landroid/animation/Animator;

.field private flipSet:Landroid/animation/AnimatorSet;

.field protected glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

.field protected glyphPaintSize:F

.field protected shadowColor:I

.field protected shadowDx:F

.field protected shadowDy:F

.field protected shadowRadius:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 59
    sget v0, Lcom/squareup/glyph/R$attr;->glyphViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/glyph/SquareGlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, -0x40800000    # -1.0f

    .line 44
    iput v0, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphPaintSize:F

    .line 65
    sget-object v0, Lcom/squareup/glyph/R$styleable;->SquareGlyphView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 66
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_android_textColor:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    .line 67
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyphFontSizeOverride:I

    const/4 p3, -0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    int-to-float p2, p2

    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphPaintSize:F

    .line 68
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyphShadowRadius:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowRadius:I

    .line 69
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyphShadowDx:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDx:F

    .line 70
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyphShadowDy:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDy:F

    .line 71
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyphShadowColor:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowColor:I

    .line 75
    sget-object p2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p2}, Lcom/squareup/glyph/SquareGlyphView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    if-nez p2, :cond_0

    const/high16 p2, -0x10000

    .line 78
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    .line 81
    :cond_0
    sget p2, Lcom/squareup/glyph/R$styleable;->SquareGlyphView_glyph:I

    sget-object p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p2, :cond_1

    .line 83
    invoke-virtual {p0, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 85
    :cond_1
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    const/4 v0, 0x0

    .line 54
    check-cast v0, Landroid/util/AttributeSet;

    invoke-direct {p0, p1, v0}, Lcom/squareup/glyph/SquareGlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-virtual {p0, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method private buildEditModeDrawable(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 137
    new-instance p1, Lcom/squareup/glyph/SquareGlyphView$1;

    invoke-direct {p1, p0}, Lcom/squareup/glyph/SquareGlyphView$1;-><init>(Lcom/squareup/glyph/SquareGlyphView;)V

    return-object p1
.end method


# virtual methods
.method protected buildGlyphDrawable(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 125
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    .line 126
    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList(Landroid/content/res/ColorStateList;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    .line 127
    iget v0, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphPaintSize:F

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 128
    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->textPaintSizeOverride(F)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 130
    :cond_0
    iget v0, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowColor:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 131
    iget v1, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowRadius:I

    iget v2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDx:F

    iget v3, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDy:F

    invoke-virtual {p1, v1, v2, v3, v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadow(IFFI)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 133
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    return-object p1
.end method

.method public flipTo(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 6

    .line 201
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->flipSet:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v1, v0, [F

    .line 202
    fill-array-data v1, :array_0

    const-string v2, "rotationY"

    invoke-static {p0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 203
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v3, 0x96

    .line 204
    invoke-virtual {v1, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-array v5, v0, [F

    .line 206
    fill-array-data v5, :array_1

    invoke-static {p0, v2, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    .line 207
    iget-object v2, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 208
    iget-object v2, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    invoke-virtual {v2, v3, v4}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 210
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/squareup/glyph/SquareGlyphView;->flipSet:Landroid/animation/AnimatorSet;

    .line 211
    iget-object v2, p0, Lcom/squareup/glyph/SquareGlyphView;->flipSet:Landroid/animation/AnimatorSet;

    new-array v0, v0, [Landroid/animation/Animator;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    aput-object v4, v0, v3

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->flipOut:Landroid/animation/Animator;

    new-instance v1, Lcom/squareup/glyph/SquareGlyphView$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/glyph/SquareGlyphView$2;-><init>(Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 220
    iget-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->flipSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x42b40000    # 90.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        -0x3d4c0000    # -90.0f
    .end array-data
.end method

.method public getColorStateList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGlyphColor()I
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 89
    instance-of v0, p1, Lcom/squareup/glyph/SquareGlyphView$SavedState;

    if-eqz v0, :cond_0

    .line 90
    check-cast p1, Lcom/squareup/glyph/SquareGlyphView$SavedState;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/appcompat/widget/AppCompatImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 92
    invoke-static {p1}, Lcom/squareup/glyph/SquareGlyphView$SavedState;->access$000(Lcom/squareup/glyph/SquareGlyphView$SavedState;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    goto :goto_0

    .line 95
    :cond_0
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 100
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatImageView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    new-instance v2, Lcom/squareup/glyph/SquareGlyphView$SavedState;

    invoke-direct {v2, v0, v1}, Lcom/squareup/glyph/SquareGlyphView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v2

    :cond_0
    return-object v0
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z
    .locals 2

    .line 162
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->isInEditMode()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 163
    invoke-direct {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->buildEditModeDrawable(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return v1

    .line 166
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->buildGlyphDrawable(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    .line 167
    iget-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return v1
.end method

.method public setGlyphColor(I)V
    .locals 0

    .line 177
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->colorStateList:Landroid/content/res/ColorStateList;

    .line 183
    iget-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    if-eqz p1, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    :cond_0
    return-void
.end method

.method public final setGlyphColorRes(I)V
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method public setGlyphShadow(IFFI)V
    .locals 0

    .line 115
    iput p4, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowColor:I

    .line 116
    iput p2, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDx:F

    .line 117
    iput p3, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowDy:F

    .line 118
    iput p1, p0, Lcom/squareup/glyph/SquareGlyphView;->shadowRadius:I

    .line 120
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public final setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 110
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    .line 111
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphView;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    return-void
.end method
