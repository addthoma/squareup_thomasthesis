.class Lcom/squareup/glyph/SquareGlyphView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SquareGlyphView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/glyph/SquareGlyphView;->flipTo(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic val$afterGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# direct methods
.method constructor <init>(Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphView$2;->this$0:Lcom/squareup/glyph/SquareGlyphView;

    iput-object p2, p0, Lcom/squareup/glyph/SquareGlyphView$2;->val$afterGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 217
    iget-object p1, p0, Lcom/squareup/glyph/SquareGlyphView$2;->this$0:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphView$2;->val$afterGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method
