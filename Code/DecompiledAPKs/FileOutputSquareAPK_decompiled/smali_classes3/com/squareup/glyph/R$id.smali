.class public final Lcom/squareup/glyph/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AUTOMATIC_PAYMENT_COF:I = 0x7f0a0001

.field public static final BACKSPACE:I = 0x7f0a0002

.field public static final BACK_ARROW:I = 0x7f0a0003

.field public static final BACK_ARROW_LARGE:I = 0x7f0a0004

.field public static final BATTERY_CHARGING:I = 0x7f0a0005

.field public static final BATTERY_DEAD:I = 0x7f0a0006

.field public static final BATTERY_FULL:I = 0x7f0a0007

.field public static final BATTERY_HIGH:I = 0x7f0a0008

.field public static final BATTERY_LOW:I = 0x7f0a0009

.field public static final BATTERY_MID:I = 0x7f0a000a

.field public static final BATTERY_OUTLINE:I = 0x7f0a000b

.field public static final BATTERY_TINY_CHARGING:I = 0x7f0a000c

.field public static final BATTERY_TINY_DEAD:I = 0x7f0a000d

.field public static final BATTERY_TINY_FULL:I = 0x7f0a000e

.field public static final BATTERY_TINY_HIGH:I = 0x7f0a000f

.field public static final BATTERY_TINY_LOW:I = 0x7f0a0010

.field public static final BATTERY_TINY_MID:I = 0x7f0a0011

.field public static final BATTERY_TINY_OUTLINE:I = 0x7f0a0012

.field public static final BIRTHDAY_CAKE:I = 0x7f0a0013

.field public static final BOX_DOLLAR_SIGN:I = 0x7f0a0017

.field public static final BOX_EQUALS:I = 0x7f0a0018

.field public static final BOX_PENNIES:I = 0x7f0a0019

.field public static final BOX_PERCENT:I = 0x7f0a001a

.field public static final BOX_PLUS:I = 0x7f0a001b

.field public static final BRIEFCASE:I = 0x7f0a001c

.field public static final BRIGHTNESS_HIGH:I = 0x7f0a001d

.field public static final BRIGHTNESS_LOW:I = 0x7f0a001e

.field public static final BURGER:I = 0x7f0a001f

.field public static final BURGER_SETTINGS:I = 0x7f0a0020

.field public static final CARD_AMEX:I = 0x7f0a0021

.field public static final CARD_BACK:I = 0x7f0a0022

.field public static final CARD_CHIP:I = 0x7f0a0023

.field public static final CARD_CUP:I = 0x7f0a0024

.field public static final CARD_DISCOVER:I = 0x7f0a0025

.field public static final CARD_DISCOVER_DINERS:I = 0x7f0a0026

.field public static final CARD_INTERAC:I = 0x7f0a0027

.field public static final CARD_JCB:I = 0x7f0a0028

.field public static final CARD_MC:I = 0x7f0a0029

.field public static final CARD_VISA:I = 0x7f0a002a

.field public static final CHECK:I = 0x7f0a002b

.field public static final CHECK_X2:I = 0x7f0a002c

.field public static final CIRCLE_CARD:I = 0x7f0a002d

.field public static final CIRCLE_CARD_CHIP:I = 0x7f0a002e

.field public static final CIRCLE_CHECK:I = 0x7f0a002f

.field public static final CIRCLE_CHECKLIST:I = 0x7f0a0030

.field public static final CIRCLE_CLOCK:I = 0x7f0a0031

.field public static final CIRCLE_CONTACTLESS:I = 0x7f0a0032

.field public static final CIRCLE_CONTACTS:I = 0x7f0a0033

.field public static final CIRCLE_EMPLOYEE_MANAGEMENT:I = 0x7f0a0034

.field public static final CIRCLE_ENVELOPE:I = 0x7f0a0035

.field public static final CIRCLE_EXCLAMATION:I = 0x7f0a0036

.field public static final CIRCLE_GIFT_CARD:I = 0x7f0a0037

.field public static final CIRCLE_HEART:I = 0x7f0a0038

.field public static final CIRCLE_INVOICE:I = 0x7f0a0039

.field public static final CIRCLE_LIGHTNING:I = 0x7f0a003a

.field public static final CIRCLE_LOCATION:I = 0x7f0a003b

.field public static final CIRCLE_LOCK:I = 0x7f0a003c

.field public static final CIRCLE_MICROPHONE:I = 0x7f0a003d

.field public static final CIRCLE_OPEN_TICKETS:I = 0x7f0a003e

.field public static final CIRCLE_PAYROLL:I = 0x7f0a003f

.field public static final CIRCLE_PHONE:I = 0x7f0a0040

.field public static final CIRCLE_PLAY:I = 0x7f0a0041

.field public static final CIRCLE_PRINTER:I = 0x7f0a0042

.field public static final CIRCLE_RECEIPT:I = 0x7f0a0043

.field public static final CIRCLE_REPORTS_CUSTOMIZE:I = 0x7f0a0044

.field public static final CIRCLE_REWARDS:I = 0x7f0a0045

.field public static final CIRCLE_SMS:I = 0x7f0a0046

.field public static final CIRCLE_STACK:I = 0x7f0a0047

.field public static final CIRCLE_STAR:I = 0x7f0a0048

.field public static final CIRCLE_STORAGE:I = 0x7f0a0049

.field public static final CIRCLE_SWIPE_ERROR:I = 0x7f0a004a

.field public static final CIRCLE_TAG:I = 0x7f0a004b

.field public static final CIRCLE_TIMECARDS:I = 0x7f0a004c

.field public static final CIRCLE_WARNING:I = 0x7f0a004d

.field public static final CIRCLE_X:I = 0x7f0a004e

.field public static final CLOCK:I = 0x7f0a004f

.field public static final CLOCK_SKEW:I = 0x7f0a0050

.field public static final CONTACTLESS:I = 0x7f0a0051

.field public static final CONTACTS:I = 0x7f0a0052

.field public static final CUSTOMER:I = 0x7f0a0054

.field public static final CUSTOMER_ADD:I = 0x7f0a0056

.field public static final CUSTOMER_CIRCLE:I = 0x7f0a0057

.field public static final CUSTOMER_GROUP:I = 0x7f0a0058

.field public static final CUSTOM_AMOUNT:I = 0x7f0a0059

.field public static final DOLLAR_BILL:I = 0x7f0a005a

.field public static final DOWN_CARET:I = 0x7f0a005b

.field public static final DRAG_HANDLE_BURGER:I = 0x7f0a005c

.field public static final DRAG_N_DROP:I = 0x7f0a005d

.field public static final ENVELOPE:I = 0x7f0a005e

.field public static final GIFT_CARD_LARGE:I = 0x7f0a0060

.field public static final GIFT_CARD_MEDIUM:I = 0x7f0a0061

.field public static final GIFT_CARD_SMALL:I = 0x7f0a0062

.field public static final HUD_BARCODE_SCANNER_CONNECTED:I = 0x7f0a0064

.field public static final HUD_BARCODE_SCANNER_DISCONNECTED:I = 0x7f0a0065

.field public static final HUD_CARD:I = 0x7f0a0066

.field public static final HUD_CASH_DRAWER_CONNECTED:I = 0x7f0a0067

.field public static final HUD_CASH_DRAWER_DISCONNECTED:I = 0x7f0a0068

.field public static final HUD_CHIP_CARD_NOT_USABLE:I = 0x7f0a0069

.field public static final HUD_CHIP_CARD_USABLE:I = 0x7f0a006a

.field public static final HUD_CONTACTLESS:I = 0x7f0a006b

.field public static final HUD_LOGOUT:I = 0x7f0a006c

.field public static final HUD_OVERLAY_BATTERY_DEAD:I = 0x7f0a006d

.field public static final HUD_OVERLAY_BATTERY_FULL:I = 0x7f0a006e

.field public static final HUD_OVERLAY_BATTERY_HIGH:I = 0x7f0a006f

.field public static final HUD_OVERLAY_BATTERY_LOW:I = 0x7f0a0070

.field public static final HUD_OVERLAY_BATTERY_MID:I = 0x7f0a0071

.field public static final HUD_PRINTER_CONNECTED:I = 0x7f0a0072

.field public static final HUD_PRINTER_DISCONNECTED:I = 0x7f0a0073

.field public static final HUD_R12:I = 0x7f0a0074

.field public static final HUD_R12_BATTERY_CHARGING:I = 0x7f0a0075

.field public static final HUD_R12_BATTERY_OUTLINE:I = 0x7f0a0076

.field public static final HUD_R12_DISCONNECTED:I = 0x7f0a0077

.field public static final HUD_R4_READER:I = 0x7f0a0078

.field public static final HUD_R6_BATTERY_CHARGING:I = 0x7f0a0079

.field public static final HUD_R6_BATTERY_OUTLINE:I = 0x7f0a007a

.field public static final HUD_R6_READER:I = 0x7f0a007b

.field public static final HUD_READER_DISCONNECTED:I = 0x7f0a007c

.field public static final HUD_REWARDS:I = 0x7f0a007d

.field public static final HUD_STORE:I = 0x7f0a007e

.field public static final HUD_SWIPE:I = 0x7f0a007f

.field public static final HUD_WARNING:I = 0x7f0a0080

.field public static final INVOICE:I = 0x7f0a0081

.field public static final KEYBOARD_ALPHA:I = 0x7f0a0084

.field public static final KEYBOARD_NUMBERS:I = 0x7f0a0085

.field public static final LEFT_CARET:I = 0x7f0a0087

.field public static final LOCATION:I = 0x7f0a0088

.field public static final LOCATION_CIRCLE:I = 0x7f0a0089

.field public static final LOCATION_PIN:I = 0x7f0a008a

.field public static final LOGOTYPE_EN:I = 0x7f0a008b

.field public static final LOGOTYPE_EN_WORLD:I = 0x7f0a008c

.field public static final LOGOTYPE_ES:I = 0x7f0a008d

.field public static final LOGOTYPE_ES_WORLD:I = 0x7f0a008e

.field public static final LOGOTYPE_FR:I = 0x7f0a008f

.field public static final LOGOTYPE_FR_WORLD:I = 0x7f0a0090

.field public static final LOGOTYPE_JA:I = 0x7f0a0091

.field public static final LOGOTYPE_JA_WORLD:I = 0x7f0a0092

.field public static final MEMO:I = 0x7f0a0093

.field public static final MICROPHONE:I = 0x7f0a0095

.field public static final MINUS:I = 0x7f0a0096

.field public static final NAVIGATION_1:I = 0x7f0a0097

.field public static final NAVIGATION_2:I = 0x7f0a0098

.field public static final NAVIGATION_3:I = 0x7f0a0099

.field public static final NAVIGATION_4:I = 0x7f0a009a

.field public static final NAVIGATION_5:I = 0x7f0a009b

.field public static final NAVIGATION_6:I = 0x7f0a009c

.field public static final NAVIGATION_7:I = 0x7f0a009d

.field public static final NAVIGATION_KEYPAD:I = 0x7f0a009e

.field public static final NAVIGATION_LIBRARY:I = 0x7f0a009f

.field public static final NOTE:I = 0x7f0a00a0

.field public static final NO_CIRCLE:I = 0x7f0a00a1

.field public static final OTHER_TENDER:I = 0x7f0a00a2

.field public static final PERSON:I = 0x7f0a00a3

.field public static final PHONE:I = 0x7f0a00a4

.field public static final PHONE_RECEIVER:I = 0x7f0a00a5

.field public static final PIN_CARD_AMEX:I = 0x7f0a00a6

.field public static final PIN_CARD_GENERIC:I = 0x7f0a00a7

.field public static final PIN_CARD_MASTER_CARD:I = 0x7f0a00a8

.field public static final PIN_CARD_VISA:I = 0x7f0a00a9

.field public static final PLUS:I = 0x7f0a00aa

.field public static final PLUS_CIRCLE:I = 0x7f0a00ab

.field public static final PRINTER:I = 0x7f0a00ac

.field public static final READER_MEDIUM:I = 0x7f0a00ad

.field public static final READER_SMALL:I = 0x7f0a00ae

.field public static final RECEIPT:I = 0x7f0a00af

.field public static final REDEMPTION_CODE:I = 0x7f0a00b0

.field public static final REFERRAL:I = 0x7f0a00b1

.field public static final REFUNDED:I = 0x7f0a00b2

.field public static final RELOAD:I = 0x7f0a00b4

.field public static final REWARDS:I = 0x7f0a00b6

.field public static final REWARDS_LARGE:I = 0x7f0a00b7

.field public static final REWARDS_MEDIUM:I = 0x7f0a00b8

.field public static final REWARD_TROPHY:I = 0x7f0a00b9

.field public static final RIBBON:I = 0x7f0a00ba

.field public static final RIGHT_CARET:I = 0x7f0a00bc

.field public static final SAVE_CARD:I = 0x7f0a00be

.field public static final SEARCH:I = 0x7f0a00bf

.field public static final SORT:I = 0x7f0a00c2

.field public static final SPEECH_BUBBLE:I = 0x7f0a00c3

.field public static final SPLIT_TENDER:I = 0x7f0a00c4

.field public static final SPLIT_TENDER_CASH_DOLLAR:I = 0x7f0a00c5

.field public static final SPLIT_TENDER_CASH_YEN:I = 0x7f0a00c6

.field public static final SPLIT_TENDER_CHIP:I = 0x7f0a00c7

.field public static final SPLIT_TENDER_OTHER:I = 0x7f0a00c8

.field public static final SQUARE_LOGO:I = 0x7f0a00c9

.field public static final SQUARE_LOGO_SW600:I = 0x7f0a00ca

.field public static final SQUARE_LOGO_SW720:I = 0x7f0a00cb

.field public static final SQUARE_WALLET_TENDER:I = 0x7f0a00cc

.field public static final STACK_HUGE_ADD:I = 0x7f0a00cd

.field public static final STACK_LARGE:I = 0x7f0a00ce

.field public static final STACK_MEDIUM:I = 0x7f0a00cf

.field public static final STACK_SMALL:I = 0x7f0a00d0

.field public static final STOPWATCH:I = 0x7f0a00d1

.field public static final STORAGE:I = 0x7f0a00d2

.field public static final SWITCHER_CUSTOMER:I = 0x7f0a00d3

.field public static final SWITCHER_DEVICE:I = 0x7f0a00d4

.field public static final SWITCHER_HELP:I = 0x7f0a00d5

.field public static final SWITCHER_ITEM:I = 0x7f0a00d6

.field public static final SWITCHER_REGISTER:I = 0x7f0a00d7

.field public static final SWITCHER_REPORT:I = 0x7f0a00d8

.field public static final SWITCHER_SETTINGS:I = 0x7f0a00d9

.field public static final SWITCHER_TRANSACTION:I = 0x7f0a00da

.field public static final TAG_LARGE:I = 0x7f0a00dc

.field public static final TAG_MEDIUM:I = 0x7f0a00dd

.field public static final TAG_SMALL:I = 0x7f0a00de

.field public static final TAG_TINY:I = 0x7f0a00df

.field public static final UNKNOWN_TENDER:I = 0x7f0a00e3

.field public static final USER_LARGE:I = 0x7f0a00e4

.field public static final VOLUME_HIGH:I = 0x7f0a00e5

.field public static final VOLUME_LOW:I = 0x7f0a00e6

.field public static final WARNING_SMALL:I = 0x7f0a00e7

.field public static final X:I = 0x7f0a00e8

.field public static final X2_ETHERNET:I = 0x7f0a00e9

.field public static final X2_ETHERNET_ERROR:I = 0x7f0a00ea

.field public static final X2_INFO:I = 0x7f0a00eb

.field public static final X2_NETWORK_ERROR:I = 0x7f0a00ec

.field public static final X2_WIFI_1:I = 0x7f0a00ed

.field public static final X2_WIFI_2:I = 0x7f0a00ee

.field public static final X2_WIFI_3:I = 0x7f0a00ef

.field public static final X2_WIFI_4:I = 0x7f0a00f0

.field public static final X2_WIFI_ERROR:I = 0x7f0a00f1

.field public static final X2_WIFI_LOCK:I = 0x7f0a00f2

.field public static final X_LARGE:I = 0x7f0a00f3

.field public static final YEN_BILL:I = 0x7f0a00f4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
