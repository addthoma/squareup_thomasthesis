.class public final Lcom/squareup/location/ValidatedLocationCacheProvider;
.super Ljava/lang/Object;
.source "ValidatedLocationCacheProvider.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nValidatedLocationCacheProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ValidatedLocationCacheProvider.kt\ncom/squareup/location/ValidatedLocationCacheProvider\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u0004\u0018\u00010\nR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/location/ValidatedLocationCacheProvider;",
        "",
        "locationMonitor",
        "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
        "lastBestLocationStore",
        "Lcom/squareup/core/location/LastBestLocationStore;",
        "comparer",
        "Lcom/squareup/core/location/comparer/LocationComparer;",
        "(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/core/location/LastBestLocationStore;Lcom/squareup/core/location/comparer/LocationComparer;)V",
        "getLatestLocation",
        "Landroid/location/Location;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final comparer:Lcom/squareup/core/location/comparer/LocationComparer;

.field private final lastBestLocationStore:Lcom/squareup/core/location/LastBestLocationStore;

.field private final locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/core/location/LastBestLocationStore;Lcom/squareup/core/location/comparer/LocationComparer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "locationMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastBestLocationStore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "comparer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    iput-object p2, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->lastBestLocationStore:Lcom/squareup/core/location/LastBestLocationStore;

    iput-object p3, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->comparer:Lcom/squareup/core/location/comparer/LocationComparer;

    return-void
.end method


# virtual methods
.method public final getLatestLocation()Landroid/location/Location;
    .locals 7

    .line 20
    iget-object v0, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->lastBestLocationStore:Lcom/squareup/core/location/LastBestLocationStore;

    invoke-virtual {v0}, Lcom/squareup/core/location/LastBestLocationStore;->get()Landroid/location/Location;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->getBestLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->comparer:Lcom/squareup/core/location/comparer/LocationComparer;

    invoke-virtual {v2, v0}, Lcom/squareup/core/location/comparer/LocationComparer;->isValidLocation(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 26
    iget-object v2, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->comparer:Lcom/squareup/core/location/comparer/LocationComparer;

    invoke-virtual {v2, v1}, Lcom/squareup/core/location/comparer/LocationComparer;->isValidLocation(Landroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_0

    return-object v0

    :cond_0
    const-string v2, "Required value was null."

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    .line 35
    invoke-virtual {v1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    cmp-long v6, v2, v4

    if-eqz v6, :cond_1

    .line 36
    invoke-virtual {v1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_4

    return-object v0

    .line 39
    :cond_1
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_4

    return-object v0

    .line 32
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 31
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_4
    iget-object v0, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->comparer:Lcom/squareup/core/location/comparer/LocationComparer;

    invoke-virtual {v0, v1}, Lcom/squareup/core/location/comparer/LocationComparer;->isValidLocation(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 45
    iget-object v0, p0, Lcom/squareup/location/ValidatedLocationCacheProvider;->lastBestLocationStore:Lcom/squareup/core/location/LastBestLocationStore;

    invoke-virtual {v0, v1}, Lcom/squareup/core/location/LastBestLocationStore;->set(Landroid/location/Location;)V

    return-object v1

    :cond_5
    const/4 v0, 0x0

    return-object v0
.end method
