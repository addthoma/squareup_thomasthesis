.class public interface abstract Lcom/squareup/location/CountryCodeGuesser;
.super Ljava/lang/Object;
.source "CountryCodeGuesser.kt"

# interfaces
.implements Lcom/squareup/location/CountryGuesser;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\n\u0010\t\u001a\u0004\u0018\u00010\u0006H&R&\u0010\u0002\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/location/CountryCodeGuesser;",
        "Lcom/squareup/location/CountryGuesser;",
        "countryCodeGuess",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/CountryCode;",
        "getCountryCodeGuess",
        "()Lio/reactivex/Single;",
        "tryTelephony",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCountryCodeGuess()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract tryTelephony()Lcom/squareup/CountryCode;
.end method
