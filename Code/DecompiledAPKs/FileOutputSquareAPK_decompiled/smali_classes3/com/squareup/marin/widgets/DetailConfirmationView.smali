.class public Lcom/squareup/marin/widgets/DetailConfirmationView;
.super Landroid/widget/LinearLayout;
.source "DetailConfirmationView.java"


# instance fields
.field private confirmationButton:Lcom/squareup/marketfont/MarketButton;

.field private glyph:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private helperText:Landroid/widget/TextView;

.field private message:Lcom/squareup/widgets/MessageView;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget v0, Lcom/squareup/marin/R$layout;->detail_confirmation_view:I

    invoke-static {p1, v0, p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 30
    invoke-direct {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->bindViews()V

    .line 31
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->helperText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 33
    sget-object v0, Lcom/squareup/marin/R$styleable;->DetailConfirmationView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 34
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_glyph:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    if-ltz p2, :cond_0

    .line 36
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 39
    :cond_0
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_android_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 40
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setTitle(Ljava/lang/CharSequence;)V

    .line 44
    :cond_1
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_message:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 45
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 46
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setMessage(Ljava/lang/CharSequence;)V

    .line 49
    :cond_2
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_equalizeLines:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 50
    iget-object p2, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->message:Lcom/squareup/widgets/MessageView;

    sget v0, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_equalizeLines:I

    const/4 v1, 0x1

    .line 51
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 50
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setEqualizeLines(Z)V

    .line 54
    :cond_3
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_buttonText:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 55
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 56
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 59
    :cond_4
    sget p2, Lcom/squareup/marin/R$styleable;->DetailConfirmationView_helperText:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 60
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 61
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 64
    :cond_5
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 96
    sget v0, Lcom/squareup/marin/R$id;->detail_confirmation_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->title:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/squareup/marin/R$id;->detail_confirmation_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->glyph:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 98
    sget v0, Lcom/squareup/marin/R$id;->detail_confirmation_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->message:Lcom/squareup/widgets/MessageView;

    .line 99
    sget v0, Lcom/squareup/marin/R$id;->detail_confirmation_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->confirmationButton:Lcom/squareup/marketfont/MarketButton;

    .line 100
    sget v0, Lcom/squareup/marin/R$id;->detail_confirmation_helper_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->helperText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 92
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->confirmationButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->glyph:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method public setHelperText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->helperText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnConfirmListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->confirmationButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/marin/widgets/DetailConfirmationView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
