.class Lcom/squareup/marin/widgets/MarinBaseActionBarView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "MarinBaseActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/marin/widgets/MarinBaseActionBarView;->bindViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/marin/widgets/MarinBaseActionBarView;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView$2;->this$0:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 340
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView$2;->this$0:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->onPrimaryButtonClicked()V

    return-void
.end method
