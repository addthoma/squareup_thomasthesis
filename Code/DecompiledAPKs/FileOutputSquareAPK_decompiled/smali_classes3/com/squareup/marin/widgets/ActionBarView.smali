.class public Lcom/squareup/marin/widgets/ActionBarView;
.super Lcom/squareup/marin/widgets/MarinBaseActionBarView;
.source "ActionBarView.java"


# instance fields
.field private final presenter:Lcom/squareup/marin/widgets/MarinActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/ActionBarView;->presenter:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method public static findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 30
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 31
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/marin/widgets/ActionBarView;->presenter:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method
