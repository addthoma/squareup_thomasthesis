.class public final Lcom/squareup/librarylist/SimpleEntryHandler;
.super Ljava/lang/Object;
.source "SimpleEntryHandler.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListEntryHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001!B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\u000bH\u0016J \u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\"\u0010\u0019\u001a\u00020\u000b2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u0018H\u0016J \u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u0018H\u0016J\u0010\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020 H\u0016R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/librarylist/SimpleEntryHandler;",
        "Lcom/squareup/librarylist/LibraryListEntryHandler;",
        "()V",
        "libraryEntryAction",
        "Lrx/Observable;",
        "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;",
        "getLibraryEntryAction",
        "()Lrx/Observable;",
        "libraryEntryRelay",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "createNewLibraryItemClicked",
        "",
        "customAmountClicked",
        "discountClicked",
        "sourceView",
        "Landroid/view/View;",
        "libraryEntry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "useDiscountDrawable",
        "",
        "isEntryEnabled",
        "catalogType",
        "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
        "objectId",
        "",
        "itemClicked",
        "itemThumbnail",
        "Landroid/widget/ImageView;",
        "searchFilter",
        "itemLongClicked",
        "itemSuggestionClicked",
        "itemSuggestion",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "LibraryEntryAction",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final libraryEntryAction:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 32
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    iput-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryAction:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public createNewLibraryItemClicked()V
    .locals 0

    return-void
.end method

.method public customAmountClicked()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$CustomAmount;->INSTANCE:Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$CustomAmount;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public discountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V
    .locals 2

    const-string v0, "sourceView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryEntry"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 49
    new-instance v1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;-><init>(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final getLibraryEntryAction()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryAction:Lrx/Observable;

    return-object v0
.end method

.method public isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "catalogType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "objectId"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
    .locals 0

    const-string p1, "libraryEntry"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "searchFilter"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 61
    new-instance p3, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;

    invoke-direct {p3, p2}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    .line 60
    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public itemLongClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
    .locals 1

    const-string v0, "itemThumbnail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "libraryEntry"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "searchFilter"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleEntryHandler;->libraryEntryRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 71
    new-instance p3, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;

    invoke-direct {p3, p2}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Item;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    .line 70
    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public itemSuggestionClicked(Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V
    .locals 1

    const-string v0, "itemSuggestion"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
