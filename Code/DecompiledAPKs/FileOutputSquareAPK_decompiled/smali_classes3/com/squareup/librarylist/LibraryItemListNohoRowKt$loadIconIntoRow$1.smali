.class final Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LibraryItemListNohoRow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRowKt;->loadIconIntoRow(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/Context;",
        "Lcom/squareup/noho/FadingDrawable;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryItemListNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryItemListNohoRow.kt\ncom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1\n*L\n1#1,455:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/FadingDrawable;",
        "it",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_loadIconIntoRow:Lcom/squareup/noho/NohoRow;

.field final synthetic $thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

.field final synthetic $transitionTime:I


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$this_loadIconIntoRow:Lcom/squareup/noho/NohoRow;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

    iput p3, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$transitionTime:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;)Lcom/squareup/noho/FadingDrawable;
    .locals 7

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$this_loadIconIntoRow:Lcom/squareup/noho/NohoRow;

    invoke-static {p1}, Lcom/squareup/noho/NohoRowUtilsKt;->iconToDrawable(Lcom/squareup/noho/NohoRow;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 443
    :cond_0
    new-instance v6, Lcom/squareup/noho/FadingDrawable;

    invoke-direct {v6, p1}, Lcom/squareup/noho/FadingDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 445
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

    check-cast p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    invoke-virtual {p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget v2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->$transitionTime:I

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-static/range {v0 .. v5}, Lcom/squareup/noho/FadingDrawable;->fadeTo$default(Lcom/squareup/noho/FadingDrawable;Landroid/graphics/drawable/Drawable;ILandroid/graphics/Rect;ILjava/lang/Object;)V

    return-object v6
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;->invoke(Landroid/content/Context;)Lcom/squareup/noho/FadingDrawable;

    move-result-object p1

    return-object p1
.end method
