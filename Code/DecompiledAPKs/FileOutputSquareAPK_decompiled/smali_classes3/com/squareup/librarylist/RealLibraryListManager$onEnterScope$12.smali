.class final Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/librarylist/LibraryListState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/librarylist/LibraryListState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListManager;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListManager;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/librarylist/LibraryListState;)V
    .locals 4

    .line 247
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v0}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getAnalytics$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v1, p1, v2, v3, v2}, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/librarylist/LibraryListState;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;->call(Lcom/squareup/librarylist/LibraryListState;)V

    return-void
.end method
