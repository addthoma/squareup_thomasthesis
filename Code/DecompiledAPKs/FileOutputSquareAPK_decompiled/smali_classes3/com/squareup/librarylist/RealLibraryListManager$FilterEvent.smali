.class public final Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;
.super Lcom/squareup/analytics/event/v1/TapEvent;
.source "LibraryListManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/RealLibraryListManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FilterEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u001b\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;",
        "Lcom/squareup/analytics/event/v1/TapEvent;",
        "filter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "category_name",
        "",
        "(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V",
        "getCategory_name",
        "()Ljava/lang/String;",
        "getFilter",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final category_name:Ljava/lang/String;

.field private final filter:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V
    .locals 1

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEM_LIBRARY_FILTER:Lcom/squareup/analytics/RegisterTapName;

    check-cast v0, Lcom/squareup/analytics/EventNamedTap;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TapEvent;-><init>(Lcom/squareup/analytics/EventNamedTap;)V

    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;->category_name:Ljava/lang/String;

    .line 326
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;->filter:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 324
    check-cast p2, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getCategory_name()Ljava/lang/String;
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;->category_name:Ljava/lang/String;

    return-object v0
.end method

.method public final getFilter()Ljava/lang/String;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;->filter:Ljava/lang/String;

    return-object v0
.end method
