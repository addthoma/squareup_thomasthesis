.class public final Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;
.super Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultItemRowState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\tR\u0014\u0010\n\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0014\u0010\u000b\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "()V",
        "amount",
        "",
        "getAmount",
        "()Ljava/lang/String;",
        "isChevronVisible",
        "",
        "()Z",
        "isTextTile",
        "itemName",
        "getItemName",
        "optionNote",
        "getOptionNote",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;

# The value of this static final field might be set in the static constructor
.field private static final amount:Ljava/lang/String; = ""

.field private static final isChevronVisible:Z = false

.field private static final isTextTile:Z = false

# The value of this static final field might be set in the static constructor
.field private static final itemName:Ljava/lang/String; = ""

# The value of this static final field might be set in the static constructor
.field private static final optionNote:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 111
    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;

    invoke-direct {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;-><init>()V

    sput-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->INSTANCE:Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;

    const-string v0, ""

    .line 112
    sput-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->itemName:Ljava/lang/String;

    .line 113
    sput-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->optionNote:Ljava/lang/String;

    .line 114
    sput-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->amount:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getAmount()Ljava/lang/String;
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    .line 112
    sget-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getOptionNote()Ljava/lang/String;
    .locals 1

    .line 113
    sget-object v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->optionNote:Ljava/lang/String;

    return-object v0
.end method

.method public isChevronVisible()Z
    .locals 1

    .line 115
    sget-boolean v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->isChevronVisible:Z

    return v0
.end method

.method public isTextTile()Z
    .locals 1

    .line 116
    sget-boolean v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->isTextTile:Z

    return v0
.end method
