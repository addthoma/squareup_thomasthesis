.class public final Lcom/squareup/librarylist/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final item_library_all_discounts:I = 0x7f120e1d

.field public static final item_library_all_gift_cards:I = 0x7f120e1e

.field public static final item_library_all_items:I = 0x7f120e1f

.field public static final item_library_all_items_and_services:I = 0x7f120e20

.field public static final item_library_all_services:I = 0x7f120e21

.field public static final item_library_create_new:I = 0x7f120e23

.field public static final item_library_custom_amount:I = 0x7f120e24

.field public static final item_library_empty_categories:I = 0x7f120e25

.field public static final item_library_empty_category_note_title:I = 0x7f120e26

.field public static final item_library_empty_discounts_note_title:I = 0x7f120e27

.field public static final item_library_empty_gift_cards_note_title:I = 0x7f120e28

.field public static final item_library_empty_items_title:I = 0x7f120e29

.field public static final item_library_empty_note_message_items_applet:I = 0x7f120e2a

.field public static final item_library_empty_note_services_message_items_applet:I = 0x7f120e2b

.field public static final item_library_empty_note_services_title:I = 0x7f120e2c

.field public static final item_library_empty_note_title:I = 0x7f120e2d

.field public static final item_library_empty_services_title:I = 0x7f120e2e

.field public static final item_library_multiple_durations:I = 0x7f120e2f

.field public static final item_library_multiple_prices:I = 0x7f120e30

.field public static final item_library_no_search_results:I = 0x7f120e31

.field public static final item_library_note_suggested:I = 0x7f120e32

.field public static final item_library_per_unit:I = 0x7f120e33

.field public static final item_library_redeem_rewards:I = 0x7f120e34

.field public static final item_library_variable_amount_discount:I = 0x7f120e35

.field public static final item_library_variable_percentage_discount:I = 0x7f120e36

.field public static final item_library_variable_price:I = 0x7f120e37

.field public static final item_library_variable_price_negation:I = 0x7f120e38

.field public static final library:I = 0x7f120ec8

.field public static final library_go_to_items_applet:I = 0x7f120ec9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
