.class public final Lcom/squareup/librarylist/LibraryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "LibraryListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;,
        Lcom/squareup/librarylist/LibraryListAdapter$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u000245BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0018\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010!\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\"\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010#\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010$\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0008\u0010%\u001a\u00020 H\u0016J\u0012\u0010&\u001a\u0004\u0018\u00010\'2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010(\u001a\u00020)2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u000e\u0010*\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 J\"\u0010+\u001a\u00020,2\u0006\u0010\u001f\u001a\u00020 2\u0008\u0010-\u001a\u0004\u0018\u00010,2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u001c\u00101\u001a\u00020\u001a2\u0006\u00102\u001a\u00020\u00152\u000c\u00103\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListAdapter;",
        "Landroid/widget/BaseAdapter;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "isTextTile",
        "",
        "itemDisabledController",
        "Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;)V",
        "cursor",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "placeholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "bindAllItemsRow",
        "",
        "view",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "bindAllServicesRow",
        "bindCatalogItemRow",
        "position",
        "",
        "bindCustomAmountRow",
        "bindDiscountsRow",
        "bindGiftCardsRow",
        "bindRewardsRow",
        "getCount",
        "getItem",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "getItemId",
        "",
        "getPlaceholder",
        "getView",
        "Landroid/view/View;",
        "convertView",
        "parent",
        "Landroid/view/ViewGroup;",
        "isEnabled",
        "update",
        "newCursor",
        "categoryPlaceholders",
        "Factory",
        "ItemDisabledController",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final isTextTile:Z

.field private final itemDisabledController:Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private placeholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Z",
            "Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "itemPhotos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemDisabledController"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/librarylist/LibraryListAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p5, p0, Lcom/squareup/librarylist/LibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-boolean p6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    iput-object p7, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;

    iput-object p8, p0, Lcom/squareup/librarylist/LibraryListAdapter;->res:Lcom/squareup/util/Res;

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    const-string p2, "emptyList<Placeholder>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    return-void
.end method

.method private final bindAllItemsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 156
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forAllItems(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 158
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 157
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindAllServicesRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 163
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forAllServices(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 165
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_services:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 164
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindCatalogItemRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 11

    .line 137
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 139
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    instance-of v1, v0, Lcom/squareup/prices/DiscountRulesLibraryCursor;

    const/4 v10, 0x1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    .line 140
    check-cast v0, Lcom/squareup/prices/DiscountRulesLibraryCursor;

    .line 142
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/LibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/LibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 143
    iget-object v5, p0, Lcom/squareup/librarylist/LibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-boolean v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    invoke-virtual {v0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountDescription()Ljava/lang/String;

    move-result-object v7

    const-string v0, "discountRulesCursor.discountDescription()"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/squareup/librarylist/LibraryListAdapter;->res:Lcom/squareup/util/Res;

    move-object v0, p2

    move-object v1, p1

    .line 141
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindDiscountItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;ZLjava/lang/String;Lcom/squareup/util/Res;)V

    goto :goto_0

    .line 140
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.prices.DiscountRulesLibraryCursor"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 147
    :cond_1
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/LibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/LibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/librarylist/LibraryListAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 148
    iget-object v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-boolean v8, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    iget-object v9, p0, Lcom/squareup/librarylist/LibraryListAdapter;->res:Lcom/squareup/util/Res;

    move-object v0, p2

    move-object v1, p1

    .line 146
    invoke-virtual/range {v0 .. v9}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;->isItemDisabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/2addr p1, v10

    invoke-virtual {p2, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    return-void

    .line 137
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unable to get LibraryEntry"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final bindCustomAmountRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 8

    .line 184
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forCustomAmount(Lcom/squareup/protos/common/CurrencyCode;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v0, "drawable"

    .line 186
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/squareup/librarylist/R$string;->item_library_custom_amount:I

    sget-object v6, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v7, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v5, 0x0

    move-object v2, p1

    .line 185
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindDiscountsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 170
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 172
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_discounts:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 171
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindGiftCardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 8

    .line 191
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v0, "drawable"

    .line 194
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    sget-object v6, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v7, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v5, 0x0

    move-object v2, p1

    .line 193
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindRewardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 177
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forReward(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 179
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_redeem_rewards:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/LibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 178
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .line 120
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    .line 121
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v1

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-ge p1, v0, :cond_0

    return-object v1

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 112
    :cond_1
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getPlaceholder(I)Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 65
    sget p2, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    goto :goto_0

    .line 67
    :cond_0
    check-cast p2, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 69
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindCatalogItemRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 73
    :cond_1
    iget-object p3, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 74
    sget-object p3, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 80
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindCustomAmountRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 79
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindRewardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 78
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindAllServicesRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 77
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindAllItemsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 76
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindGiftCardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 75
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->bindDiscountsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    .line 84
    :goto_1
    check-cast p2, Landroid/view/View;

    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;->isItemDisabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newCursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryPlaceholders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->placeholders:Ljava/util/List;

    .line 93
    iget-object p2, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eq p2, p1, :cond_0

    if-eqz p2, :cond_0

    .line 94
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    .line 95
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 98
    :cond_0
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryListAdapter;->notifyDataSetChanged()V

    return-void
.end method
