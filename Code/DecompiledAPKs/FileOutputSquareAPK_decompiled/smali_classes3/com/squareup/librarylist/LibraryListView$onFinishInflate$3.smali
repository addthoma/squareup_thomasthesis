.class public final Lcom/squareup/librarylist/LibraryListView$onFinishInflate$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "LibraryListView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/LibraryListView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/librarylist/LibraryListView$onFinishInflate$3",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "v",
        "Landroid/view/View;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/LibraryListView;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$3;->this$0:Lcom/squareup/librarylist/LibraryListView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$3;->this$0:Lcom/squareup/librarylist/LibraryListView;

    invoke-static {p1}, Lcom/squareup/librarylist/LibraryListView;->access$getActionSubject$p(Lcom/squareup/librarylist/LibraryListView;)Lrx/subjects/PublishSubject;

    move-result-object p1

    sget-object v0, Lcom/squareup/librarylist/LibraryListView$Action$OpenItemsApplet;->INSTANCE:Lcom/squareup/librarylist/LibraryListView$Action$OpenItemsApplet;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
