.class final Lcom/squareup/librarylist/RealLibraryListManager$2;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Single<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Single;",
        "Lcom/squareup/librarylist/CatalogQueryResult;",
        "state",
        "Lcom/squareup/librarylist/LibraryListState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListManager;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListManager;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$2;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/librarylist/LibraryListState;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListManager$2;->call(Lcom/squareup/librarylist/LibraryListState;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/librarylist/LibraryListState;)Lrx/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    .line 118
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getSearchQuery()Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState$SearchQuery;->getSearchText()Ljava/lang/String;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListManager$2;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v1}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getConfiguration$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListConfiguration;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->isTopLevel()Z

    move-result v2

    const-string v3, "state"

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$2;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v0}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getLibraryListSearcher$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object v0

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/librarylist/LibraryListSearcher;->topLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 123
    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$2;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v0}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getLibraryListSearcher$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object v0

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/librarylist/LibraryListSearcher;->readFromFilter(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$2;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v0}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getLibraryListSearcher$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object v0

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/librarylist/LibraryListSearcher;->searchByName(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method
