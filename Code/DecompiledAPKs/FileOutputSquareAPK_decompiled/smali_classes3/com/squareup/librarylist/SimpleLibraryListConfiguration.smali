.class public final Lcom/squareup/librarylist/SimpleLibraryListConfiguration;
.super Ljava/lang/Object;
.source "SimpleLibraryListConfiguration.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListConfiguration;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000bR\u0014\u0010\u000e\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u000bR\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u001a\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
        "Lcom/squareup/librarylist/LibraryListConfiguration;",
        "topLevelPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "topLevelItemSuggestions",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "(Ljava/util/List;Ljava/util/List;)V",
        "canAlwaysShowCategoryPlaceholders",
        "",
        "getCanAlwaysShowCategoryPlaceholders",
        "()Z",
        "canLongClickOnItem",
        "getCanLongClickOnItem",
        "canShowEditItemsButton",
        "getCanShowEditItemsButton",
        "getTopLevelItemSuggestions",
        "()Ljava/util/List;",
        "getTopLevelPlaceholders",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final topLevelItemSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field private final topLevelPlaceholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;-><init>(Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "topLevelPlaceholders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topLevelItemSuggestions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;->topLevelPlaceholders:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;->topLevelItemSuggestions:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 8
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getCanAlwaysShowCategoryPlaceholders()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getCanLongClickOnItem()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCanShowEditItemsButton()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getTopLevelItemSuggestions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;->topLevelItemSuggestions:Ljava/util/List;

    return-object v0
.end method

.method public getTopLevelPlaceholders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;->topLevelPlaceholders:Ljava/util/List;

    return-object v0
.end method
