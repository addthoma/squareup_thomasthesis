.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideLibraryListStateManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/LibraryListManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;

.field private final realLibraryListStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/RealLibraryListManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/RealLibraryListManager;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    .line 25
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->realLibraryListStateManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/RealLibraryListManager;",
            ">;)",
            "Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLibraryListStateManager(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListManager;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideLibraryListStateManager(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListManager;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/LibraryListManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/LibraryListManager;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->realLibraryListStateManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->provideLibraryListStateManager(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListStateManagerFactory;->get()Lcom/squareup/librarylist/LibraryListManager;

    move-result-object v0

    return-object v0
.end method
