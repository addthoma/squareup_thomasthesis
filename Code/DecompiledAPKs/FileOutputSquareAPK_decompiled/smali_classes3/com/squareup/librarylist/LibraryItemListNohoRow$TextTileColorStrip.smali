.class final Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;
.super Ljava/lang/Object;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TextTileColorStrip"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryItemListNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryItemListNohoRow.kt\ncom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip\n*L\n1#1,455:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;",
        "",
        "(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V",
        "colorStrip",
        "Lcom/squareup/marin/widgets/BorderPainter;",
        "marginWithColorStrip",
        "",
        "drawBorders",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "setColor",
        "colorHex",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

.field private final marginWithColorStrip:I

.field final synthetic this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 378
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v0, Lcom/squareup/marin/widgets/BorderPainter;

    .line 381
    move-object v1, p1

    check-cast v1, Landroid/view/View;

    .line 382
    sget v2, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    .line 380
    invoke-direct {v0, v1, v2}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    const/4 v1, 0x1

    .line 384
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    .line 387
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 388
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_thumbnail_content:I

    .line 387
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 389
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 390
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    .line 389
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->marginWithColorStrip:I

    return-void
.end method


# virtual methods
.method public final drawBorders(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final setColor(I)V
    .locals 2

    .line 394
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setColor(I)V

    .line 397
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemListNohoRow()Lcom/squareup/noho/NohoRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 398
    iget v0, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->marginWithColorStrip:I

    if-eq v0, v1, :cond_0

    .line 399
    iput v1, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 400
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemListNohoRow()Lcom/squareup/noho/NohoRow;

    move-result-object v0

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void

    .line 397
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
