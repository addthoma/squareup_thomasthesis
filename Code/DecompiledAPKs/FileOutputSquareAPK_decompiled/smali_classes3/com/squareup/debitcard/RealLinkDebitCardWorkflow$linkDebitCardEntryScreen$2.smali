.class final Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardEntryScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bills/CardData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "cardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;->invoke(Lcom/squareup/protos/client/bills/CardData;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/bills/CardData;)V
    .locals 2

    const-string v0, "cardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;-><init>(Lcom/squareup/protos/client/bills/CardData;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
