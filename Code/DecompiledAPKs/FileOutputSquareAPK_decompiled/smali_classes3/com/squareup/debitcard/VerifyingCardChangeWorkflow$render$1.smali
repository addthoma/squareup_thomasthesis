.class final Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "VerifyingCardChangeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/debitcard/VerifyingCardChangeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/debitcard/VerifyingCardChangeOutput;",
        "confirmCardResult",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2

    const-string v0, "confirmCardResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardSuccess;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 38
    :cond_0
    instance-of v0, p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;

    .line 39
    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;

    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 38
    invoke-direct {v0, v1, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;->invoke(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
