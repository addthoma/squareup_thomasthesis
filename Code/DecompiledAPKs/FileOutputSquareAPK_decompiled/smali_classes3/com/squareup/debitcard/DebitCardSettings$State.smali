.class public final Lcom/squareup/debitcard/DebitCardSettings$State;
.super Ljava/lang/Object;
.source "DebitCardSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/DebitCardSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BC\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0011\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0012\u001a\u00020\tH\u00c6\u0003JG\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\t2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0010\u0010\u000b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "",
        "linkCardState",
        "Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;",
        "resendEmailState",
        "Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "cardUnsupported",
        "",
        "clientUpgradeRequired",
        "attemptFailed",
        "(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final attemptFailed:Z

.field public final cardUnsupported:Z

.field public final clientUpgradeRequired:Z

.field public final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field public final linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

.field public final resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)V
    .locals 1

    const-string v0, "linkCardState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resendEmailState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    iput-object p2, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    iput-object p3, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iput-boolean p4, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    iput-boolean p5, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    iput-boolean p6, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 19
    sget-object p1, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->INITIAL:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    .line 20
    sget-object p2, Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;->INITIAL:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    .line 21
    move-object p3, p2

    check-cast p3, Lcom/squareup/receiving/FailureMessage;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    const/4 p3, 0x0

    if-eqz p2, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move v1, p4

    :goto_0
    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    move v2, p5

    :goto_1
    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    move v3, p6

    :goto_2
    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    .line 24
    invoke-direct/range {p2 .. p8}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/debitcard/DebitCardSettings$State;Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILjava/lang/Object;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/debitcard/DebitCardSettings$State;->copy(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)Lcom/squareup/debitcard/DebitCardSettings$State;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    return-object v0
.end method

.method public final component3()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    return v0
.end method

.method public final copy(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 8

    const-string v0, "linkCardState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resendEmailState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/debitcard/DebitCardSettings$State;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    iget-object v1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    iget-object v1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iget-object v1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    iget-boolean p1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(linkCardState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", resendEmailState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->resendEmailState:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", failureMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardUnsupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clientUpgradeRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", attemptFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
