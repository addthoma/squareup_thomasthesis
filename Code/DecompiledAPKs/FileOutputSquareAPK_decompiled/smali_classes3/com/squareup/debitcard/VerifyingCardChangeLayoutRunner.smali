.class public final Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner;
.super Ljava/lang/Object;
.source "VerifyingCardChangeLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/debitcard/VerifyingCardChangeScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000b2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/debitcard/VerifyingCardChangeScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner;->Companion:Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    sget-object v0, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$1;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/debitcard/VerifyingCardChangeScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "containerHints"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner;->showRendering(Lcom/squareup/debitcard/VerifyingCardChangeScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
