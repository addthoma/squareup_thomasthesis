.class public abstract Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action;
.super Ljava/lang/Object;
.source "VerifyingCardChangeWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;,
        Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\u0002\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/debitcard/VerifyingCardChangeOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Fail",
        "Succeed",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/debitcard/VerifyingCardChangeOutput;
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/debitcard/VerifyingCardChangeOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Succeed;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/debitcard/VerifyingCardChangeOutput$CardChangeVerified;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeOutput$CardChangeVerified;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :cond_0
    instance-of v0, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;

    if-eqz v0, :cond_1

    .line 88
    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeOutput$CardChangeNotVerified;

    .line 89
    move-object v1, p0

    check-cast v1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;

    invoke-virtual {v1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-virtual {v1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action$Fail;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-direct {v0, v2, v1}, Lcom/squareup/debitcard/VerifyingCardChangeOutput$CardChangeNotVerified;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
