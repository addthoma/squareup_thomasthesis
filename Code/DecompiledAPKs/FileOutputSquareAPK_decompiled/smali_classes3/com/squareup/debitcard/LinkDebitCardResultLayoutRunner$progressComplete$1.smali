.class final Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;
.super Ljava/lang/Object;
.source "LinkDebitCardResultLayoutRunner.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressComplete(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/debitcard/LinkDebitCardResultScreen;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;->$screen:Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;->$screen:Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-virtual {v0}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getOnPrimaryLinkButtonClicked()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;->$screen:Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-virtual {v1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getLinkDebitCardFailed()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
