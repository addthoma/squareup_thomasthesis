.class public final Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;
.super Lcom/squareup/debitcard/LinkDebitCardViewFactory;
.source "RealLinkDebitCardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;",
        "Lcom/squareup/debitcard/LinkDebitCardViewFactory;",
        "linkDebitCardEntryFactory",
        "Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Factory;",
        "(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Factory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "linkDebitCardEntryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Binding;-><init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 11
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->Companion:Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 12
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory;->Companion:Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/debitcard/LinkDebitCardViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
