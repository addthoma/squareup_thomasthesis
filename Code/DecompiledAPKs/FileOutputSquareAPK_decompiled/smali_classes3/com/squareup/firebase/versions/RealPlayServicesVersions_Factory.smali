.class public final Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;
.super Ljava/lang/Object;
.source "RealPlayServicesVersions_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/firebase/versions/RealPlayServicesVersions;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;)",
            "Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/content/pm/PackageManager;)Lcom/squareup/firebase/versions/RealPlayServicesVersions;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/firebase/versions/RealPlayServicesVersions;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/versions/RealPlayServicesVersions;-><init>(Landroid/content/pm/PackageManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/firebase/versions/RealPlayServicesVersions;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;->newInstance(Landroid/content/pm/PackageManager;)Lcom/squareup/firebase/versions/RealPlayServicesVersions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;->get()Lcom/squareup/firebase/versions/RealPlayServicesVersions;

    move-result-object v0

    return-object v0
.end method
