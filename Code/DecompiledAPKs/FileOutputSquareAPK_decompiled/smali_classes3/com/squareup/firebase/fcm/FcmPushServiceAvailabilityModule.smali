.class public abstract Lcom/squareup/firebase/fcm/FcmPushServiceAvailabilityModule;
.super Ljava/lang/Object;
.source "FcmPushServiceAvailabilityModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/firebase/common/RealFirebaseModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindFirebase(Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;)Lcom/squareup/push/PushServiceAvailability;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
