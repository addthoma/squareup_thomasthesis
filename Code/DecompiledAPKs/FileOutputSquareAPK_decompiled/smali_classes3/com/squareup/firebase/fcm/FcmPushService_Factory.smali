.class public final Lcom/squareup/firebase/fcm/FcmPushService_Factory;
.super Ljava/lang/Object;
.source "FcmPushService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/firebase/fcm/FcmPushService;",
        ">;"
    }
.end annotation


# instance fields
.field private final firebaseInstanceIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/iid/FirebaseInstanceId;",
            ">;"
        }
    .end annotation
.end field

.field private final firebaseMessagingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/messaging/FirebaseMessaging;",
            ">;"
        }
    .end annotation
.end field

.field private final pushServiceAvailabilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/messaging/FirebaseMessaging;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/iid/FirebaseInstanceId;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->pushServiceAvailabilityProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->firebaseMessagingProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->firebaseInstanceIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/FcmPushService_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/messaging/FirebaseMessaging;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/iid/FirebaseInstanceId;",
            ">;)",
            "Lcom/squareup/firebase/fcm/FcmPushService_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/firebase/fcm/FcmPushService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/push/PushServiceAvailability;Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)Lcom/squareup/firebase/fcm/FcmPushService;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/firebase/fcm/FcmPushService;-><init>(Lcom/squareup/push/PushServiceAvailability;Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/firebase/fcm/FcmPushService;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->pushServiceAvailabilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/push/PushServiceAvailability;

    iget-object v1, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->firebaseMessagingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/messaging/FirebaseMessaging;

    iget-object v2, p0, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->firebaseInstanceIdProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/iid/FirebaseInstanceId;

    invoke-static {v0, v1, v2}, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->newInstance(Lcom/squareup/push/PushServiceAvailability;Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)Lcom/squareup/firebase/fcm/FcmPushService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->get()Lcom/squareup/firebase/fcm/FcmPushService;

    move-result-object v0

    return-object v0
.end method
