.class public abstract Lcom/squareup/firebase/fcm/FcmModule;
.super Ljava/lang/Object;
.source "FcmModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/firebase/fcm/SharedFcmModule;,
        Lcom/squareup/firebase/common/RealFirebaseModule;,
        Lcom/squareup/firebase/fcm/FcmPushServiceAvailabilityModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindPushService(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/squareup/push/PushService;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
