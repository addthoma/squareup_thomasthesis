.class public final Lcom/squareup/firebase/fcm/FcmService_MembersInjector;
.super Ljava/lang/Object;
.source "FcmService_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/firebase/fcm/FcmService;",
        ">;"
    }
.end annotation


# instance fields
.field private final fcmPushServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/fcm/FcmPushService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/fcm/FcmPushService;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;->fcmPushServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/fcm/FcmPushService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/firebase/fcm/FcmService;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFcmPushService(Lcom/squareup/firebase/fcm/FcmService;Lcom/squareup/firebase/fcm/FcmPushService;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmService;->fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/firebase/fcm/FcmService;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;->fcmPushServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {p1, v0}, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;->injectFcmPushService(Lcom/squareup/firebase/fcm/FcmService;Lcom/squareup/firebase/fcm/FcmPushService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/firebase/fcm/FcmService;

    invoke-virtual {p0, p1}, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;->injectMembers(Lcom/squareup/firebase/fcm/FcmService;)V

    return-void
.end method
