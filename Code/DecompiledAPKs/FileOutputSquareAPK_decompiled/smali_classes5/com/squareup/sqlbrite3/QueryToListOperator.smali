.class final Lcom/squareup/sqlbrite3/QueryToListOperator;
.super Ljava/lang/Object;
.source "QueryToListOperator.java"

# interfaces
.implements Lio/reactivex/ObservableOperator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOperator<",
        "Ljava/util/List<",
        "TT;>;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# instance fields
.field private final mapper:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/functions/Function;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/sqlbrite3/QueryToListOperator;->mapper:Lio/reactivex/functions/Function;

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Observer;)Lio/reactivex/Observer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-",
            "Ljava/util/List<",
            "TT;>;>;)",
            "Lio/reactivex/Observer<",
            "-",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/QueryToListOperator;->mapper:Lio/reactivex/functions/Function;

    invoke-direct {v0, p1, v1}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;-><init>(Lio/reactivex/Observer;Lio/reactivex/functions/Function;)V

    return-object v0
.end method
