.class Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;
.super Ljava/lang/Object;
.source "BriteContentResolver.java"

# interfaces
.implements Lio/reactivex/functions/Cancellable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqlbrite3/BriteContentResolver$2;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/sqlbrite3/BriteContentResolver$2;

.field final synthetic val$observer:Landroid/database/ContentObserver;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteContentResolver$2;Landroid/database/ContentObserver;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;->this$1:Lcom/squareup/sqlbrite3/BriteContentResolver$2;

    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;->val$observer:Landroid/database/ContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;->this$1:Lcom/squareup/sqlbrite3/BriteContentResolver$2;

    iget-object v0, v0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-object v0, v0, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;->val$observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
