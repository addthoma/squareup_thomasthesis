.class Lcom/squareup/sqlbrite3/SqlBrite$Query$1;
.super Ljava/lang/Object;
.source "SqlBrite.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqlbrite3/SqlBrite$Query;->asRows(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqlbrite3/SqlBrite$Query;

.field final synthetic val$mapper:Lio/reactivex/functions/Function;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/SqlBrite$Query;Lio/reactivex/functions/Function;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;->this$0:Lcom/squareup/sqlbrite3/SqlBrite$Query;

    iput-object p2, p0, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;->val$mapper:Lio/reactivex/functions/Function;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;->this$0:Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->run()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 234
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;->val$mapper:Lio/reactivex/functions/Function;

    invoke-interface {v1, v0}, Lio/reactivex/functions/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 238
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw p1

    .line 241
    :cond_1
    :goto_1
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 242
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->onComplete()V

    :cond_2
    return-void
.end method
