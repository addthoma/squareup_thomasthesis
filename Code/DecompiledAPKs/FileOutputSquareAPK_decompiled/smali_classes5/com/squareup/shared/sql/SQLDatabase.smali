.class public interface abstract Lcom/squareup/shared/sql/SQLDatabase;
.super Ljava/lang/Object;
.source "SQLDatabase.java"


# virtual methods
.method public abstract beginTransaction()V
.end method

.method public abstract compileStatement(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatement;
.end method

.method public abstract endTransaction()V
.end method

.method public abstract execSQL(Ljava/lang/String;)V
.end method

.method public abstract rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
.end method

.method public abstract setTransactionSuccessful()V
.end method
