.class Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;
.super Ljava/lang/Object;
.source "RecurrenceRuleIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PositiveNegativeValueSet"
.end annotation


# instance fields
.field private final negativeValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final positiveValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    .line 650
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    .line 651
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 653
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-gez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 655
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->positiveValues:Ljava/util/Set;

    .line 656
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->negativeValues:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;
    .locals 0

    .line 642
    iget-object p0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->positiveValues:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;)Ljava/util/Set;
    .locals 0

    .line 642
    iget-object p0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;->negativeValues:Ljava/util/Set;

    return-object p0
.end method

.method static forValues(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 663
    :cond_0
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;

    invoke-direct {v0, p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$PositiveNegativeValueSet;-><init>(Ljava/util/Set;)V

    return-object v0
.end method
