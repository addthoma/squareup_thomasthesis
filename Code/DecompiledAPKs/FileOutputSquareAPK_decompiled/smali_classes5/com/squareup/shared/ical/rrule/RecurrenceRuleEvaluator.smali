.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator;
.super Ljava/lang/Object;
.source "RecurrenceRuleEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public evaluate(Lcom/squareup/shared/ical/rrule/RecurrenceRule;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;

    invoke-direct {v0, p1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;-><init>(Lcom/squareup/shared/ical/rrule/RecurrenceRule;Ljava/util/Date;)V

    .line 15
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 16
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 17
    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->next()Ljava/util/Date;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 20
    :cond_0
    new-instance p2, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;

    invoke-virtual {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->getEvaluatedAllInstants()Z

    move-result v0

    invoke-direct {p2, p1, v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;-><init>(Ljava/util/List;Z)V

    return-object p2
.end method
