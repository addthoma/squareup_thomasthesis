.class public Lcom/squareup/shared/pricing/engine/util/MapUtils;
.super Ljava/lang/Object;
.source "MapUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;,
        Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 64
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    sget-object v2, Lcom/squareup/shared/pricing/engine/util/MapUtils$$Lambda$1;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->executeOperation(Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static capacityExceeded(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;)Z"
        }
    .end annotation

    .line 88
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static executeOperation(Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;",
            ")",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 26
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 27
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 28
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 30
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 31
    invoke-interface {p0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    goto :goto_1

    :cond_1
    move-object v3, p2

    .line 32
    :goto_1
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    goto :goto_2

    :cond_2
    move-object v4, p3

    .line 33
    :goto_2
    invoke-interface {p4, v3, v4}, Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;->perform(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 35
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public static fetch(Ljava/util/Map;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;TT;",
            "Ljava/math/BigDecimal;",
            ")",
            "Ljava/math/BigDecimal;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 101
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/math/BigDecimal;

    return-object p0

    :cond_1
    :goto_0
    return-object p2
.end method

.method public static getOrDefault(Ljava/util/Map;Ljava/lang/Object;Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TKey:",
            "Ljava/lang/Object;",
            "TValue:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TTKey;TTValue;>;TTKey;",
            "Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer<",
            "TTValue;>;)TTValue;"
        }
    .end annotation

    .line 114
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;->get()Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static incrementMap(Ljava/util/Map;Ljava/util/Map;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .line 124
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 125
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 126
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int v0, v0, p2

    .line 127
    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 128
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 131
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static final synthetic lambda$merge$0$MapUtils(Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 0

    if-nez p1, :cond_0

    return-object p2

    :cond_0
    if-nez p2, :cond_1

    return-object p1

    .line 56
    :cond_1
    invoke-interface {p0, p1, p2}, Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;->perform(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method static final synthetic lambda$subtract$1$MapUtils(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method static final synthetic lambda$subtractFloor$2$MapUtils(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 1

    if-eqz p0, :cond_1

    .line 80
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x0

    :goto_1
    return-object p0
.end method

.method public static merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;",
            ")",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/shared/pricing/engine/util/MapUtils$$Lambda$0;

    invoke-direct {v0, p2}, Lcom/squareup/shared/pricing/engine/util/MapUtils$$Lambda$0;-><init>(Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)V

    const/4 p2, 0x0

    invoke-static {p0, p1, p2, p2, v0}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->executeOperation(Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static nonzeroKeys(Ljava/util/Map;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set<",
            "TT;>;"
        }
    .end annotation

    .line 137
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 138
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 139
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_0

    .line 140
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static subtract(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 71
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    sget-object v1, Lcom/squareup/shared/pricing/engine/util/MapUtils$$Lambda$2;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    const/4 v2, 0x0

    invoke-static {p0, p1, v2, v0, v1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->executeOperation(Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static subtractFloor(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 79
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    sget-object v1, Lcom/squareup/shared/pricing/engine/util/MapUtils$$Lambda$3;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    const/4 v2, 0x0

    invoke-static {p0, p1, v2, v0, v1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->executeOperation(Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method
