.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;
.super Ljava/lang/Object;
.source "CatalogFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;
    }
.end annotation


# virtual methods
.method public abstract execute(Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method
