.class public Lcom/squareup/shared/pricing/engine/rules/RuleSetLoaderImpl;
.super Ljava/lang/Object;
.source "RuleSetLoaderImpl.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation

    .line 18
    invoke-interface {p1, p2, p4, p5}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;->findActivePricingRules(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;)Ljava/util/Set;

    move-result-object p2

    .line 20
    invoke-interface {p1, p3}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;->findPricingRulesByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 21
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 23
    invoke-static {}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;->newBuilder()Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->setStart(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->setEnd(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->setRules(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl$Builder;->build()Lcom/squareup/shared/pricing/engine/rules/RuleSetImpl;

    move-result-object p1

    return-object p1
.end method

.method public load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation

    .line 34
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/shared/pricing/engine/rules/RuleSetLoaderImpl;->load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;

    move-result-object p1

    return-object p1
.end method

.method public load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;Z)Lcom/squareup/shared/pricing/engine/rules/RuleSet;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/TimeZone;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;"
        }
    .end annotation

    .line 45
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/shared/pricing/engine/rules/RuleSetLoaderImpl;->load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/rules/RuleSet;

    move-result-object p1

    return-object p1
.end method
