.class Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;
.super Ljava/util/LinkedHashMap;
.source "PricingEngineImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/PricingEngineImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleLRUMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap<",
        "TK;TV;>;"
    }
.end annotation


# instance fields
.field final capacity:I

.field final synthetic this$0:Lcom/squareup/shared/pricing/engine/PricingEngineImpl;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/PricingEngineImpl;IFZ)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;->this$0:Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

    .line 53
    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 54
    iput p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;->capacity:I

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;)Z"
        }
    .end annotation

    .line 58
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;->size()I

    move-result p1

    iget v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;->capacity:I

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
