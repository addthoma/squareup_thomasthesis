.class public interface abstract Lcom/squareup/shared/pricing/engine/search/RuleNode;
.super Ljava/lang/Object;
.source "RuleNode.java"


# virtual methods
.method public abstract bestApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
.end method

.method public abstract commitMatch(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V
.end method

.method public abstract getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
.end method
