.class public Lcom/squareup/shared/pricing/models/MonetaryAmount;
.super Ljava/lang/Object;
.source "MonetaryAmount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/models/MonetaryAmount$Builder;
    }
.end annotation


# instance fields
.field private amount:J

.field private currency:Ljava/util/Currency;


# direct methods
.method public constructor <init>(JLjava/util/Currency;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-wide p1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->amount:J

    .line 13
    iput-object p3, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->currency:Ljava/util/Currency;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 27
    :cond_1
    check-cast p1, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    .line 28
    iget-wide v2, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->amount:J

    iget-wide v4, p1, Lcom/squareup/shared/pricing/models/MonetaryAmount;->amount:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget-object v2, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->currency:Ljava/util/Currency;

    iget-object p1, p1, Lcom/squareup/shared/pricing/models/MonetaryAmount;->currency:Ljava/util/Currency;

    .line 29
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getAmount()Ljava/lang/Long;
    .locals 2

    .line 17
    iget-wide v0, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->amount:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getCurrency()Ljava/util/Currency;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->currency:Ljava/util/Currency;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 33
    iget-wide v1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->amount:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/pricing/models/MonetaryAmount;->currency:Ljava/util/Currency;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
