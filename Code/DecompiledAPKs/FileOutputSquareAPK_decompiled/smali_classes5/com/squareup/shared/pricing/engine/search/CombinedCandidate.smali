.class public Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;
.super Ljava/lang/Object;
.source "CombinedCandidate.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/Candidate;


# instance fields
.field private final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsID:Ljava/lang/String;

.field private quantity:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->cogsID:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    .line 34
    sget-object p1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->quantity:Ljava/math/BigDecimal;

    return-void
.end method


# virtual methods
.method public alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 160
    invoke-interface {v2, p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public canJoin(Lcom/squareup/shared/pricing/engine/search/Candidate;)Z
    .locals 2

    .line 201
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->getCaps()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->getApplications()Ljava/util/Map;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->subtract(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 202
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 203
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public canSplit()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;"
        }
    .end annotation

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 177
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 178
    invoke-interface {v2, p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 180
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->subtractFloor(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 184
    :cond_1
    new-instance p1, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->cogsID:Ljava/lang/String;

    invoke-direct {p1, v1, v0}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1
.end method

.method public getApplications()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 116
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 117
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 118
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    .line 119
    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 120
    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 123
    :cond_1
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/math/BigDecimal;

    invoke-virtual {v3, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getCaps()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 190
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getCaps()Ljava/util/Map;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate$$Lambda$0;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {v0, v2, v3}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getFractional()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMaxUnitPrice()J
    .locals 7

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 65
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitQuantity()Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 68
    :cond_1
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMaxUnitPrice()J

    move-result-wide v2

    if-eqz v1, :cond_2

    .line 69
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    .line 70
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    .line 78
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    .line 75
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Empty candidate has no max unit price"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMinUnitPrice()J
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 45
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitQuantity()Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 48
    :cond_1
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMinUnitPrice()J

    move-result-wide v2

    if-eqz v1, :cond_2

    .line 49
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    .line 50
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    .line 58
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    .line 55
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Empty candidate has no min unit price"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOffsets()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 140
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 141
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getPreMatched()Z
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 106
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getPreMatched()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getTiebreakers()Ljava/lang/String;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->cogsID:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitQuantity()Ljava/math/BigDecimal;
    .locals 4

    .line 96
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 97
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 99
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitQuantity()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getUnitValue()J
    .locals 7

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 88
    invoke-interface {v3}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v4

    .line 89
    invoke-interface {v3}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 90
    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public join(Lcom/squareup/shared/pricing/engine/search/Candidate;)V
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public take(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/Candidate;
    .locals 2

    .line 168
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-nez p1, :cond_0

    .line 171
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->quantity:Ljava/math/BigDecimal;

    .line 172
    new-instance p1, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->cogsID:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;->children:Ljava/util/List;

    invoke-direct {p1, v0, v1}, Lcom/squareup/shared/pricing/engine/search/CombinedCandidate;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1

    .line 169
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "A combined candidate can only be taken whole"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
