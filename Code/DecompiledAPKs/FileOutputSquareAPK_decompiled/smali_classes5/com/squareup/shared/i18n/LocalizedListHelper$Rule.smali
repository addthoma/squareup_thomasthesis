.class public Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;
.super Ljava/lang/Object;
.source "LocalizedListHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/i18n/LocalizedListHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Rule"
.end annotation


# instance fields
.field private final key:Lcom/squareup/shared/i18n/Key;

.field private final orMore:Z

.field private final quantity:I


# direct methods
.method private constructor <init>(ILcom/squareup/shared/i18n/Key;Z)V
    .locals 0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iput p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->quantity:I

    .line 138
    iput-object p2, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->key:Lcom/squareup/shared/i18n/Key;

    .line 139
    iput-boolean p3, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I
    .locals 0

    .line 130
    iget p0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->quantity:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Z
    .locals 0

    .line 130
    iget-boolean p0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/Key;
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->key:Lcom/squareup/shared/i18n/Key;

    return-object p0
.end method

.method public static exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;
    .locals 2

    .line 143
    sget-object v0, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "AS_IS can only be used with quantity == 1"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 146
    :cond_1
    :goto_0
    new-instance v0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;-><init>(ILcom/squareup/shared/i18n/Key;Z)V

    return-object v0
.end method

.method public static orMore(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;
    .locals 2

    .line 150
    new-instance v0, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;-><init>(ILcom/squareup/shared/i18n/Key;Z)V

    return-object v0
.end method
