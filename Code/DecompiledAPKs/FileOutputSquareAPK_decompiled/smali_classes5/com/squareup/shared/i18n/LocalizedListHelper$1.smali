.class Lcom/squareup/shared/i18n/LocalizedListHelper$1;
.super Ljava/lang/Object;
.source "LocalizedListHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/i18n/LocalizedListHelper;-><init>(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/i18n/LocalizedListHelper;


# direct methods
.method constructor <init>(Lcom/squareup/shared/i18n/LocalizedListHelper;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper$1;->this$0:Lcom/squareup/shared/i18n/LocalizedListHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I
    .locals 0

    .line 56
    invoke-static {p1}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result p1

    invoke-static {p2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    check-cast p2, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/i18n/LocalizedListHelper$1;->compare(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result p1

    return p1
.end method
