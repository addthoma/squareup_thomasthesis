.class Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;
.super Ljava/lang/Object;
.source "ItemOptionAssignmentState.java"


# instance fields
.field final allItemOptionsByIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;",
            ">;"
        }
    .end annotation
.end field

.field final allSortedVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;",
            ">;"
        }
    .end annotation
.end field

.field final assignedOptionIdsInOrder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;"
        }
    .end annotation
.end field

.field final intendedOptionIdsInOrder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;"
        }
    .end annotation
.end field

.field final itemId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;)V"
        }
    .end annotation

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "itemId"

    .line 81
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->itemId:Ljava/lang/String;

    .line 82
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    .line 83
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    .line 84
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p3}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    .line 85
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p5}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 87
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    .line 88
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p7}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 92
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueCombinationComparator()Ljava/util/Comparator;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private buildNewVariation(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
    .locals 3

    .line 238
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->itemId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    .line 239
    invoke-interface {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 241
    invoke-virtual {p0, v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v1

    const-string v2, "CatalogOptionValue"

    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 242
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private commitOptionAndOptionValueAssignment(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)V
    .locals 3

    .line 194
    iget-object v0, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const-string v2, "assignedOptionValueIdPairsByOptionIds should not contain a key if it\'s not in assignedOptionIdsInOrder."

    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 199
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;
    .locals 1

    .line 258
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 259
    invoke-virtual {v0, p0}, Lokio/Buffer;->write([B)Lokio/Buffer;

    .line 261
    :try_start_0
    invoke-static {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 263
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private getOptionValueIdPairComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation

    .line 227
    new-instance v0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState$$Lambda$1;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;)V

    return-object v0
.end method

.method private getSortedOptionValueIdPairsByOptionIds(Ljava/util/Map;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;"
        }
    .end annotation

    .line 211
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueIdPairComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 213
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 216
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 217
    new-instance v4, Ljava/util/ArrayList;

    .line 218
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 219
    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 220
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static final synthetic lambda$getOptionValueCombinationComparator$0$ItemOptionAssignmentState(Ljava/util/Comparator;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)I
    .locals 4

    .line 165
    invoke-interface {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p1

    .line 166
    invoke-interface {p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p2

    .line 167
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string/jumbo v1, "when comparing two optionValueCombinations, they should have the same size of values."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    .line 170
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 171
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 172
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 171
    invoke-interface {p0, v1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-eqz v1, :cond_1

    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return v2
.end method

.method private static readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 312
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 313
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_0

    .line 316
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readByteArrayWithLength(Lokio/BufferedSource;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v5

    .line 315
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 319
    :cond_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 320
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v0, :cond_1

    .line 322
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v6

    .line 324
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readByteArrayWithLength(Lokio/BufferedSource;)[B

    move-result-object v7

    invoke-static {v7}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    move-result-object v7

    .line 323
    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 327
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 328
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v0, :cond_2

    .line 330
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 333
    :cond_2
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 335
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v0, :cond_4

    .line 337
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v8

    .line 338
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 339
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v10

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v10, :cond_3

    .line 341
    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 343
    :cond_3
    invoke-interface {v6, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 346
    :cond_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 347
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v8, 0x0

    :goto_5
    if-ge v8, v0, :cond_5

    .line 349
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 352
    :cond_5
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    .line 354
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    const/4 v9, 0x0

    :goto_6
    if-ge v9, v0, :cond_7

    .line 356
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v10

    .line 357
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 358
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v12

    const/4 v13, 0x0

    :goto_7
    if-ge v13, v12, :cond_6

    .line 360
    invoke-static {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v14

    invoke-interface {v11, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 362
    :cond_6
    invoke-interface {v8, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 365
    :cond_7
    new-instance p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;

    move-object v0, p0

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;)V

    return-object p0
.end method

.method private writeToSink(Lokio/BufferedSink;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 268
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->itemId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 270
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 271
    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->toByteArray()[B

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeByteArrayWithLength(Lokio/BufferedSink;[B)V

    goto :goto_0

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 274
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 275
    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    .line 276
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->toByteArray()[B

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeByteArrayWithLength(Lokio/BufferedSink;[B)V

    goto :goto_1

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 279
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 280
    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    goto :goto_2

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 283
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 284
    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    .line 285
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 286
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 287
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1, v2}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 288
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 289
    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->writeToSink(Lokio/BufferedSink;)V

    goto :goto_3

    .line 292
    :cond_4
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 293
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 294
    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    goto :goto_4

    .line 296
    :cond_5
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 297
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 298
    invoke-static {p1, v1}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    .line 299
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    .line 300
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 301
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1, v2}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 302
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 303
    invoke-virtual {v2, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->writeToSink(Lokio/BufferedSink;)V

    goto :goto_5

    :cond_7
    return-void
.end method


# virtual methods
.method addVariationsAndCommitOptionAssignments(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;)V"
        }
    .end annotation

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 130
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueCombinationComparator()Ljava/util/Comparator;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 p1, 0x0

    const/4 v1, 0x0

    .line 132
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 133
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;

    .line 135
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionIdsInOrder:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v1, :cond_1

    .line 137
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    :goto_1
    const-string v3, "An item should only have one single variation if it has no item option before item option assignment."

    invoke-static {v4, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 140
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 142
    invoke-interface {v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 144
    invoke-virtual {p0, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v5

    .line 145
    invoke-interface {v3, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    move-result-object v3

    goto :goto_2

    .line 149
    :cond_1
    invoke-direct {p0, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->buildNewVariation(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    move-result-object v3

    .line 150
    iget-object v4, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_2
    invoke-interface {v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 155
    invoke-direct {p0, v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->commitOptionAndOptionValueAssignment(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)V

    goto :goto_3

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    :cond_4
    iget-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueCombinationComparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;
    .locals 4

    .line 183
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    .line 184
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    .line 183
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 185
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method getAllItemOptionsByIds()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 96
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 99
    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allItemOptionsByIds:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method getOptionValueCombinationComparator()Ljava/util/Comparator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 162
    invoke-direct {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getOptionValueIdPairComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 164
    new-instance v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState$$Lambda$0;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState$$Lambda$0;-><init>(Ljava/util/Comparator;)V

    return-object v1
.end method

.method getSortedAssignedOptionValueIdPairsByOptionIds()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->assignedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedOptionValueIdPairsByOptionIds(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method getSortedIntendedOptionValueIdPairsByOptionIds()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;>;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->intendedOptionValueIdPairsByOptionIds:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getSortedOptionValueIdPairsByOptionIds(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$getOptionValueIdPairComparator$1$ItemOptionAssignmentState(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)I
    .locals 2

    .line 228
    iget-object v0, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Two option value id pairs must have the same option id, in order to be compared."

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 230
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v0

    .line 231
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->getOrdinalWithAllItemOptionsByIds(Ljava/util/Map;)I

    move-result p1

    .line 232
    invoke-virtual {p2, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->getOrdinalWithAllItemOptionsByIds(Ljava/util/Map;)I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method public toByteArray()[B
    .locals 2

    .line 248
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 250
    :try_start_0
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->writeToSink(Lokio/BufferedSink;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    invoke-virtual {v0}, Lokio/Buffer;->readByteArray()[B

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    .line 252
    new-instance v1, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method updateExistingVariationsWithValue(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)V
    .locals 3

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->findCatalogOptionValueByOptionValuePair(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string/jumbo v2, "value to be added to existing variations doesn\'t exist"

    .line 117
    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 119
    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->allSortedVariations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    .line 120
    invoke-interface {v2, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;->addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;

    goto :goto_1

    .line 123
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentState;->commitOptionAndOptionValueAssignment(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)V

    return-void
.end method
