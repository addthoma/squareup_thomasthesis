.class public Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;
.super Ljava/lang/Object;
.source "CatalogOnline.java"

# interfaces
.implements Lcom/squareup/shared/catalog/Catalog$Online;


# static fields
.field private static final VARIATION_ATTRIBUTE_NAME_MEASUREMENT_UNIT_ID:Ljava/lang/String; = "measurement_unit_id"


# instance fields
.field private final endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    return-void
.end method

.method private handleResponseErrors(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "*>;"
        }
    .end annotation

    .line 210
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 214
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/resources/Error;

    .line 216
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 218
    :cond_1
    new-instance p1, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 219
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public batchRetrieveCatalogConnectV2Objects(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/shared/catalog/CatalogException;
        }
    .end annotation

    const-string v0, "objectIds"

    .line 127
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 128
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;-><init>()V

    .line 129
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->object_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 130
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_related_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object p1

    .line 133
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    .line 134
    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->batchRetrieveCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 135
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 136
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 139
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;

    .line 140
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 141
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->handleResponseErrors(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 144
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 145
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 146
    sget-object v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_2
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public batchUpsertCatalogConnectV2Objects(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "connect v2 objects to batch upsert"

    .line 175
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "idempotencyKey for batch upserting connect v2 objects"

    .line 176
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 179
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    .line 180
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_0
    new-instance p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;-><init>()V

    .line 184
    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;-><init>()V

    .line 185
    invoke-virtual {p2, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->batches(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 186
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;

    move-result-object p1

    .line 188
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->batchUpsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 190
    iget-object p2, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz p2, :cond_1

    .line 191
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 194
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    .line 195
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    .line 196
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->handleResponseErrors(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 200
    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 201
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 202
    sget-object v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 205
    :cond_3
    new-instance v0, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->id_mappings:Ljava/util/List;

    invoke-direct {v0, p2, p1}, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public countVariationsWithAssignedUnit(Ljava/lang/String;I)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/shared/catalog/CatalogException;
        }
    .end annotation

    .line 79
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 80
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;-><init>()V

    const-string v3, "measurement_unit_id"

    .line 83
    invoke-virtual {v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;->attribute_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;

    move-result-object v2

    .line 84
    invoke-virtual {v2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;->attribute_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;

    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    move-result-object p1

    .line 82
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object p1

    .line 81
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 88
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    const/4 p2, 0x0

    .line 94
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->searchCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    .line 97
    iget-object v1, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v1, :cond_0

    .line 98
    iget-object p1, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 102
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;

    .line 105
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    iget-object p1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->handleResponseErrors(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 110
    :cond_1
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr p2, v1

    .line 114
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->cursor:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 115
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p1

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->cursor:Ljava/lang/String;

    .line 116
    invoke-virtual {p1, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p1

    goto :goto_0

    .line 119
    :cond_2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public deleteCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/shared/catalog/CatalogException;
        }
    .end annotation

    const-string v0, "objectId"

    .line 46
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 48
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;-><init>()V

    .line 49
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->object_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object p1

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->delete(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 54
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 55
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;

    .line 62
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 63
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    .line 64
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Error$Category;->INVALID_REQUEST_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    if-ne v0, v3, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    .line 65
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-ne v0, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    .line 68
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->handleResponseErrors(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 73
    :cond_2
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public retrieveCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    const-string v0, "objectId"

    .line 154
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 157
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;->batchRetrieveCatalogConnectV2Objects(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    .line 158
    iget-object v1, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v1, :cond_0

    .line 159
    iget-object p1, v0, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 162
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 163
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2

    .line 165
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_OBJECT_NOT_FOUND:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    .line 169
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    .line 164
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " objects on fetching "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
