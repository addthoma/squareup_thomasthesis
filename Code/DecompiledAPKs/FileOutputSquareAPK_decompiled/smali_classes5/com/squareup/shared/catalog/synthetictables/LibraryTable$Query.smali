.class public final enum Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;
.super Ljava/lang/Enum;
.source "LibraryTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/LibraryTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE_COLLATION_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE_IMAGE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE_NAME_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum CREATE_TYPE_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum INSERT_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum READ_BY_ID_WITH_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum REPLACE_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum REPLACE_DISCOUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum REPLACE_ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum REPLACE_TICKET_GROUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum UPDATE_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum UPDATE_ITEM_VARIATIONS:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum UPDATE_VARIATION_COUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

.field public static final enum WRITE_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 106
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE TABLE {table} ({object_id} TEXT UNIQUE, {adapter_id} INTEGER PRIMARY_KEY, {object_type} INTEGER, {item_type} INTEGER, {image_id} TEXT, {image_url} TEXT, {name} TEXT, {variation_count} INTEGER, {price_amt} INTEGER, {price_currency} INTEGER, {duration} INTEGER, {discount_percentage} TEXT, {discount_type} INTEGER, {abbrev} TEXT, {color} TEXT, {default_variation_id} TEXT, {search_words} TEXT, {category_id} TEXT, {ordinal} INTEGER, {naming_method} INTEGER, {available_for_pickup} INTEGER, {collation_section_index} INTEGER, {measurement_unit_token} TEXT)"

    .line 107
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "table"

    const-string v3, "library"

    .line 116
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "object_id"

    .line 117
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "adapter_id"

    const-string v6, "_id"

    .line 118
    invoke-virtual {v1, v5, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "object_type"

    .line 119
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "item_type"

    .line 120
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "image_id"

    .line 121
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "image_url"

    .line 122
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v9, "name"

    .line 123
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v10, "variations"

    const-string/jumbo v11, "variation_count"

    .line 124
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "price_amt"

    const-string v13, "price_amount"

    .line 125
    invoke-virtual {v1, v12, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "price_currency"

    .line 126
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "duration"

    const-string v14, "duration"

    .line 127
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "discount_percentage"

    const-string v14, "discount_percentage"

    .line 128
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "discount_type"

    const-string v14, "discount_type"

    .line 129
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "abbrev"

    const-string v14, "abbreviation"

    .line 130
    invoke-virtual {v1, v13, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "color"

    .line 131
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "default_variation_id"

    const-string v15, "default_variation"

    .line 132
    invoke-virtual {v1, v14, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "search_words"

    .line 133
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "category_id"

    move-object/from16 v16, v12

    const-string v12, "category_id"

    .line 134
    invoke-virtual {v1, v15, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "ordinal"

    .line 135
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "naming_method"

    move-object/from16 v17, v12

    const-string v12, "naming_method"

    .line 136
    invoke-virtual {v1, v15, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "available_for_pickup"

    const-string v15, "available_for_pickup"

    .line 137
    invoke-virtual {v1, v12, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "collation_section_index"

    .line 138
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "measurement_unit_token"

    move-object/from16 v18, v10

    const-string v10, "measurement_unit_token"

    .line 139
    invoke-virtual {v1, v15, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 141
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE"

    const/4 v15, 0x0

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 143
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({collation_section_index}, {search_words} COLLATE NOCASE ASC)"

    .line 144
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "idx"

    const-string v15, "idx_collation_search"

    .line 146
    invoke-virtual {v1, v10, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 147
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 148
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 149
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 151
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE_COLLATION_SEARCH_INDEX"

    const/4 v15, 0x1

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_COLLATION_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 153
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE INDEX idx_id ON {table} ({object_id})"

    .line 154
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 155
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 158
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE_ID_INDEX"

    const/4 v15, 0x2

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 160
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE INDEX idx_image_id ON {table} ({image_id})"

    .line 161
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 162
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 163
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 164
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 165
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE_IMAGE_ID_INDEX"

    const/4 v15, 0x3

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_IMAGE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 167
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({search_words} COLLATE NOCASE ASC)"

    .line 168
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "idx"

    const-string v15, "idx_search_words"

    .line 169
    invoke-virtual {v1, v10, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 170
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 171
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 173
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE_NAME_SEARCH_INDEX"

    const/4 v15, 0x4

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_NAME_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 175
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "CREATE INDEX idx_object_type_name ON {table} ({object_type}, {name} COLLATE NOCASE ASC)"

    .line 176
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 178
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 179
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 180
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 182
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "CREATE_TYPE_NAME_INDEX"

    const/4 v15, 0x5

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_TYPE_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 184
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {object_id} = ?"

    .line 185
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 186
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 187
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 188
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 189
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "DELETE"

    const/4 v15, 0x6

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->DELETE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 191
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "INSERT INTO {table} ({object_id}, {object_type}, {item_type}, {name}, {search_words}, {image_id}, {image_url}, {abbrev}, {color}, {category_id}, {available_for_pickup}, {collation_section_index}) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 192
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 197
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 198
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 199
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 200
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 201
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 202
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 203
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 204
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "abbrev"

    const-string v15, "abbreviation"

    .line 205
    invoke-virtual {v1, v10, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 206
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "category_id"

    const-string v15, "category_id"

    .line 207
    invoke-virtual {v1, v10, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "available_for_pickup"

    const-string v15, "available_for_pickup"

    .line 208
    invoke-virtual {v1, v10, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 209
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 211
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "INSERT_ITEM"

    const/4 v15, 0x7

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->INSERT_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 213
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "SELECT {image_id}, {image_url}, {name} FROM {table} WHERE {object_id} = ? LIMIT 1"

    .line 214
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 216
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 217
    invoke-virtual {v1, v8, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 218
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 219
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 220
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 221
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 222
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v10, "READ_BY_ID_WITH_IMAGE"

    const/16 v15, 0x8

    invoke-direct {v0, v10, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->READ_BY_ID_WITH_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 224
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_type}, {name}, {search_words}, {variation_count}, {ordinal}, {abbreviation}, {color}, {collation_section_index}) VALUES (?, ?, ?, ?, 1, ?, ?, ?, ?)"

    .line 225
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 228
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 229
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 230
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 231
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 232
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v10, v18

    .line 233
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v15, v17

    .line 234
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v17, v8

    const-string v8, "abbreviation"

    move-object/from16 v18, v7

    const-string v7, "abbreviation"

    .line 235
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 236
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 237
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 238
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 239
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, "REPLACE_CATEGORY"

    const/16 v8, 0x9

    invoke-direct {v0, v7, v8, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 241
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_type}, {name},{search_words}, {color}, {price_amt}, {price_currency}, {discount_percentage}, {discount_type}, {variation_count}, {collation_section_index}) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?)"

    .line 242
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 246
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 247
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 248
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 249
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 250
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 251
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "price_amt"

    const-string v8, "price_amount"

    .line 252
    invoke-virtual {v1, v7, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v7, v16

    .line 253
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "discount_percentage"

    const-string v7, "discount_percentage"

    .line 254
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "discount_type"

    const-string v8, "discount_type"

    .line 255
    invoke-virtual {v1, v7, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 256
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 257
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 258
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 259
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, "REPLACE_DISCOUNT"

    const/16 v8, 0xa

    invoke-direct {v0, v7, v8, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_DISCOUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 261
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_type}, {name}, {search_words}, {variation_count}, {ordinal}) VALUES (?, ?, ?, ?, ?, ?)"

    .line 262
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 264
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 265
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 266
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 267
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 268
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 269
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 270
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 271
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 272
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, "REPLACE_ITEM_MODIFIER_LIST"

    const/16 v8, 0xb

    invoke-direct {v0, v7, v8, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 274
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({object_id}, {object_type}, {name}, {variation_count}, {ordinal}, {naming_method}, {search_words}) VALUES (?, ?, ?, ?, ?, ?, ?)"

    .line 275
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 278
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 279
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 280
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 281
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 282
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 283
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "naming_method"

    const-string v8, "naming_method"

    .line 284
    invoke-virtual {v1, v7, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 285
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 287
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, "REPLACE_TICKET_GROUP"

    const/16 v8, 0xc

    invoke-direct {v0, v7, v8, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_TICKET_GROUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 289
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "UPDATE {table} SET {object_type}=?, {item_type}=?, {name}=?, {search_words}=?, {image_id}=?, {image_url}=?, {abbrev}=?, {color}=?, {category_id}=?, {available_for_pickup}=?, {collation_section_index}=? WHERE {object_id} = ?"

    .line 290
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 295
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 296
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 297
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 298
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 299
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v2, v18

    .line 300
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v5, v17

    .line 301
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "abbrev"

    const-string v7, "abbreviation"

    .line 302
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 303
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "category_id"

    const-string v7, "category_id"

    .line 304
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "available_for_pickup"

    const-string v7, "available_for_pickup"

    .line 305
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 306
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 307
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 308
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 309
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "UPDATE_ITEM"

    const/16 v7, 0xd

    invoke-direct {v0, v6, v7, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 311
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "UPDATE {library} SET {variation_count} = ?, {price_amount} = ?, {price_currency} = ?, {duration} = ?, {default_variation_id} = ?, {measurement_unit_token} = ? WHERE {object_id} = ?"

    .line 312
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 315
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 316
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "price_amount"

    const-string v7, "price_amount"

    .line 317
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v6, v16

    .line 318
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "duration"

    const-string v7, "duration"

    .line 319
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "default_variation_id"

    const-string v7, "default_variation"

    .line 320
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "measurement_unit_token"

    const-string v7, "measurement_unit_token"

    .line 321
    invoke-virtual {v1, v6, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 322
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 323
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 324
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "UPDATE_ITEM_VARIATIONS"

    const/16 v7, 0xe

    invoke-direct {v0, v6, v7, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM_VARIATIONS:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 326
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "UPDATE {library} SET {variation_count} = ? WHERE {object_id} = ?"

    .line 327
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 328
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 329
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 330
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 331
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 332
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "UPDATE_VARIATION_COUNT"

    const/16 v6, 0xf

    invoke-direct {v0, v4, v6, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_VARIATION_COUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 334
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const-string v1, "UPDATE {library} SET {image_url} = ? WHERE {image_id} = ?"

    .line 335
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 336
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 337
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 338
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 339
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 340
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WRITE_IMAGE"

    const/16 v3, 0x10

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->WRITE_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    .line 105
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_COLLATION_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_IMAGE_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_NAME_SEARCH_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->CREATE_TYPE_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->DELETE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->INSERT_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->READ_BY_ID_WITH_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_CATEGORY:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_DISCOUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->REPLACE_TICKET_GROUP:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_ITEM_VARIATIONS:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->UPDATE_VARIATION_COUNT:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->WRITE_IMAGE:Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 344
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 345
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;
    .locals 1

    .line 105
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;
    .locals 1

    .line 105
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
