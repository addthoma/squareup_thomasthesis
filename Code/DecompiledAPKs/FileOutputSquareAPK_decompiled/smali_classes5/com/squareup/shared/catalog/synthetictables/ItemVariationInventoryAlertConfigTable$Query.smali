.class public final enum Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;
.super Ljava/lang/Enum;
.source "ItemVariationInventoryAlertConfigTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

.field public static final enum CREATE_TIMESTAMP_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

.field public static final enum REPLACE_VARIATION_ALERT_CONFIG:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

.field public static final enum UPDATE_VARIATION_DELETED:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 38
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const-string v1, "CREATE TABLE {table} ({variation_id} TEXT PRIMARY KEY, {variation_token} TEXT, {item_id} TEXT, {tracking_state} INTEGER, {alert_type} INTEGER, {alert_threshold} INTEGER, {deleted} INTEGER, {server_timestamp} INTEGER)"

    .line 39
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_alert_config"

    const-string v3, "table"

    .line 43
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v4, "variation_id"

    .line 44
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v5, "variation_token"

    .line 45
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "item_id"

    .line 46
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "inventory_tracking_state"

    const-string/jumbo v8, "tracking_state"

    .line 47
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v9, "inventory_alert_type"

    const-string v10, "alert_type"

    .line 48
    invoke-virtual {v1, v10, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v11, "inventory_alert_threshold"

    const-string v12, "alert_threshold"

    .line 49
    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "deleted"

    .line 50
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "server_timestamp"

    .line 51
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 53
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v15, 0x0

    move-object/from16 v16, v13

    const-string v13, "CREATE"

    invoke-direct {v0, v13, v15, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 56
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const-string v1, "CREATE INDEX idx_timestamp ON {table} ({server_timestamp}) "

    .line 57
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 61
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v13, 0x1

    const-string v15, "CREATE_TIMESTAMP_INDEX"

    invoke-direct {v0, v15, v13, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE_TIMESTAMP_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 63
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({variation_id}, {variation_token}, {item_id}, {tracking_state}, {alert_type}, {alert_threshold}, {deleted}, {server_timestamp}) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

    .line 64
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v10, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 74
    invoke-virtual {v1, v12, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    move-object/from16 v5, v16

    .line 75
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 76
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 78
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "REPLACE_VARIATION_ALERT_CONFIG"

    const/4 v7, 0x2

    invoke-direct {v0, v6, v7, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->REPLACE_VARIATION_ALERT_CONFIG:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 80
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const-string v1, "UPDATE {table} SET {deleted} = ?, {server_timestamp} = ? WHERE {variation_id} = ?"

    .line 81
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 83
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 85
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 88
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UPDATE_VARIATION_DELETED"

    const/4 v3, 0x3

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->UPDATE_VARIATION_DELETED:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 37
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE_TIMESTAMP_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->REPLACE_VARIATION_ALERT_CONFIG:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->UPDATE_VARIATION_DELETED:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;
    .locals 1

    .line 37
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
