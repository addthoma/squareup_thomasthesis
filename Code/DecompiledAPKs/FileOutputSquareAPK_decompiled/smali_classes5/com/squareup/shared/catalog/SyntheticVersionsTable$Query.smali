.class public final enum Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;
.super Ljava/lang/Enum;
.source "SyntheticVersionsTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/SyntheticVersionsTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

.field public static final enum DELETE_FROM_TABLE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

.field public static final enum READ_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

.field public static final enum WRITE_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 36
    new-instance v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    const-string v1, "CREATE TABLE {table} ({table_name} STRING, {table_version} INTEGER)"

    .line 37
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "synthetic_table_versions"

    const-string v3, "table"

    .line 38
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "table_name"

    .line 39
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "table_version"

    .line 40
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 42
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const-string v7, "CREATE"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->CREATE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    .line 44
    new-instance v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    const-string v1, "DELETE FROM {table}"

    .line 45
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 46
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 48
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    const-string v8, "DELETE_FROM_TABLE"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->DELETE_FROM_TABLE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    .line 50
    new-instance v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    const-string v1, "SELECT * FROM {table}"

    .line 51
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 52
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 54
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x2

    const-string v9, "READ_VERSIONS"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->READ_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    .line 56
    new-instance v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({table_name}, {table_version}) VALUES (?, ?)"

    .line 57
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 60
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 63
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const-string v3, "WRITE_VERSIONS"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->WRITE_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    .line 35
    sget-object v1, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->CREATE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->DELETE_FROM_TABLE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->READ_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->WRITE_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput-object p3, p0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;
    .locals 1

    .line 35
    const-class v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
