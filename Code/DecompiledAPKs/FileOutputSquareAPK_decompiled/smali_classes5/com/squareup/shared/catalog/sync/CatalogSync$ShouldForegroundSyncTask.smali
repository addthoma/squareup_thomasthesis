.class final Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ShouldForegroundSyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private completed:Z

.field private final maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

.field private shouldForegroundSync:Z

.field private throwable:Ljava/lang/Throwable;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;)V
    .locals 0

    .line 685
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    .line 686
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 687
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V
    .locals 0

    .line 677
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Ljava/lang/Throwable;
    .locals 0

    .line 677
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->throwable:Ljava/lang/Throwable;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Z
    .locals 0

    .line 677
    iget-boolean p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->shouldForegroundSync:Z

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Z
    .locals 0

    .line 677
    iget-boolean p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->completed:Z

    return p0
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 692
    :try_start_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$100(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    move-result-object p1

    .line 693
    iget-boolean v1, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    .line 694
    invoke-static {v1, v2, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$600(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->shouldForegroundSync:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 698
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    monitor-enter p1

    .line 699
    :try_start_1
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->completed:Z

    .line 700
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 701
    monitor-exit p1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception p1

    .line 696
    :try_start_2
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->throwable:Ljava/lang/Throwable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 698
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    monitor-enter p1

    .line 699
    :try_start_3
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->completed:Z

    .line 700
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 701
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 703
    :goto_2
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1

    :catchall_2
    move-exception v0

    .line 701
    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :catchall_3
    move-exception p1

    .line 698
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    monitor-enter v1

    .line 699
    :try_start_5
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->completed:Z

    .line 700
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 701
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    throw p1

    :catchall_4
    move-exception p1

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw p1
.end method
