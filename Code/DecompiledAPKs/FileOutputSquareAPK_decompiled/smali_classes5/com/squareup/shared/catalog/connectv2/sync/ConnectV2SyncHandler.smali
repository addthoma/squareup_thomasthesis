.class public Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;
.super Ljava/lang/Object;
.source "ConnectV2SyncHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;
    }
.end annotation


# static fields
.field private static final BEGIN_VERSION_INITIAL_SYNC:J


# instance fields
.field private final endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;

.field public final supportedObjectTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    .line 37
    iput-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->mainThread:Ljava/util/concurrent/Executor;

    .line 38
    iput-object p3, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->httpThread:Ljava/util/concurrent/Executor;

    if-nez p4, :cond_0

    .line 40
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    return-void
.end method

.method private doSync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v4, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    .line 101
    :cond_1
    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asRfc3339(J)Ljava/lang/String;

    move-result-object p1

    .line 102
    :goto_1
    invoke-static {p3, p4}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asRfc3339(J)Ljava/lang/String;

    move-result-object p2

    .line 104
    new-instance p3, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;-><init>()V

    .line 105
    invoke-virtual {p3, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->begin_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 106
    invoke-virtual {p3, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->end_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 107
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 108
    invoke-virtual {p3, p5}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 109
    invoke-virtual {p3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p3

    .line 112
    :goto_2
    iget-object p4, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    invoke-interface {p4, p3}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->searchCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p3

    .line 115
    iget-object p4, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$1;

    invoke-direct {v3, p6, p3}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    invoke-interface {p4, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 118
    iget-object p4, p3, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz p4, :cond_2

    return v1

    .line 125
    :cond_2
    :try_start_0
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    iget-object p3, p3, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->cursor:Ljava/lang/String;

    if-nez p3, :cond_3

    return v0

    .line 137
    :cond_3
    new-instance p4, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;-><init>()V

    .line 138
    invoke-virtual {p4, p3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 139
    invoke-virtual {p3, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->begin_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 140
    invoke-virtual {p3, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->end_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 141
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 142
    invoke-virtual {p3, p5}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;

    move-result-object p3

    .line 143
    invoke-virtual {p3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object p3

    goto :goto_2

    :catch_0
    return v1
.end method

.method static final synthetic lambda$doSync$2$ConnectV2SyncHandler(Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 115
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;->handle(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method static final synthetic lambda$sync$0$ConnectV2SyncHandler(Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Z)V
    .locals 0

    .line 86
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;->complete(Z)V

    return-void
.end method


# virtual methods
.method final synthetic lambda$sync$1$ConnectV2SyncHandler(JJLcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Ljava/util/List;)V
    .locals 8

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    .line 67
    iget-object v5, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->doSync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)Z

    move-result v0

    goto :goto_1

    .line 70
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    invoke-interface {v7, p6}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 72
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v1, 0x0

    move-object v0, p0

    move-wide v3, p3

    move-object v5, v7

    move-object v6, p5

    .line 74
    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->doSync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 78
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 79
    invoke-interface {v5, v7}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    if-eqz v0, :cond_2

    .line 80
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v6, p5

    .line 81
    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->doSync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)Z

    move-result v0

    .line 86
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$2;

    invoke-direct {v2, p5, v0}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$2;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Z)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public sync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;",
            ")V"
        }
    .end annotation

    move-object v8, p0

    .line 57
    iget-object v0, v8, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move-object/from16 v6, p6

    .line 58
    invoke-interface {v6, v0}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;->complete(Z)V

    return-void

    :cond_0
    move-object/from16 v6, p6

    .line 63
    iget-object v9, v8, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->httpThread:Ljava/util/concurrent/Executor;

    new-instance v10, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$0;

    move-object v0, v10

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;JJLcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;Ljava/util/List;)V

    invoke-interface {v9, v10}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
