.class public Lcom/squareup/shared/catalog/PendingWriteRequestsTable;
.super Ljava/lang/Object;
.source "PendingWriteRequestsTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;
    }
.end annotation


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "id"

.field public static final COLUMN_REQUEST:Ljava/lang/String; = "request"

.field private static INSTANCE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable; = null

.field public static final NAME:Ljava/lang/String; = "pending_write_requests"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->INSTANCE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->INSTANCE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    .line 31
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->INSTANCE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    return-object v0
.end method


# virtual methods
.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->CREATE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public delete(Lcom/squareup/shared/sql/SQLDatabase;J)I
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->DELETE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 97
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->executeUpdateDelete()I

    move-result p1

    return p1
.end method

.method public enqueue(Lcom/squareup/shared/sql/SQLDatabase;J[B)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->ENQUEUE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 90
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 91
    invoke-virtual {p1, p4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindBlob([B)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public hasPutRequests(Lcom/squareup/shared/sql/SQLDatabase;)Z
    .locals 2

    const/4 v0, 0x0

    .line 104
    :try_start_0
    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->HAS_PUT_REQUESTS:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 106
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {v0, v1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v0, :cond_1

    .line 108
    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return v1

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw p1
.end method

.method public readAll(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    .line 113
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->READ_ALL:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method
