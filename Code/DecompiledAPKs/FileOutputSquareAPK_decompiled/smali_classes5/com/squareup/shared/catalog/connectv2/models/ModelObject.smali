.class public abstract Lcom/squareup/shared/catalog/connectv2/models/ModelObject;
.super Ljava/lang/Object;
.source "ModelObject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "TT;>;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 34
    :cond_0
    instance-of v0, p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 38
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 39
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "TT;>;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    return-object v0
.end method

.method public final getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 43
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;->key:Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public abstract toByteArray()[B
.end method
