.class public abstract Lcom/squareup/shared/catalog/models/CatalogObject;
.super Ljava/lang/Object;
.source "CatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/wire/Message;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DIRTY:I = 0x1

.field private static final NOT_DIRTY:I


# instance fields
.field private final backingObject:Lcom/squareup/api/sync/ObjectWrapper;

.field private final catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field private final id:Ljava/lang/String;

.field private final object:Lcom/squareup/wire/Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "backingObject"

    .line 52
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectWrapper;

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    .line 53
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 54
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->matches(Lcom/squareup/shared/catalog/models/CatalogObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->messageObject(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/wire/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->object:Lcom/squareup/wire/Message;

    .line 61
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->extractId(Lcom/squareup/api/sync/ObjectWrapper;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->id:Ljava/lang/String;

    return-void

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Incompatible wrapper type: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "; expected "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static dirtyFlag([B)Z
    .locals 2

    .line 225
    array-length v0, p0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    aget-byte p0, p0, v1

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private static extractId(Lcom/squareup/api/sync/ObjectWrapper;)Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    iget-object p0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object p0, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method static fromByteArrayWithDirtyFlag([B)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 3

    .line 231
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    array-length v1, p0

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-direct {v0, p0, v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 232
    sget-object p0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lokio/Okio;->source(Ljava/io/InputStream;)Lokio/Source;

    move-result-object v0

    invoke-static {v0}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lokio/BufferedSource;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/ObjectWrapper;

    .line 233
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 235
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static toByteArrayWithDirtyPrefix(Lcom/squareup/api/sync/ObjectWrapper;Z)[B
    .locals 1

    .line 210
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 211
    invoke-virtual {v0, p1}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 213
    :try_start_0
    sget-object p1, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p1, v0, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Lokio/BufferedSink;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    invoke-virtual {v0}, Lokio/Buffer;->readByteArray()[B

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    .line 215
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Should not be possible: ioexception on BAOS"

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method


# virtual methods
.method public copy(Lcom/squareup/api/items/Item;)Lcom/squareup/shared/catalog/models/CatalogItem;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 152
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 103
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/ItemFeeMembership;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership(Lcom/squareup/api/items/ItemFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 162
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 133
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 112
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 113
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 122
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 123
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 143
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    return-object p1
.end method

.method public copy(Lcom/squareup/api/items/Page;)Lcom/squareup/shared/catalog/models/CatalogPage;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page(Lcom/squareup/api/items/Page;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 93
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogPage;

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 182
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 184
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 185
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->backingObject:Lcom/squareup/api/sync/ObjectWrapper;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 201
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 193
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method public hasObjectExtension()Z
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->object:Lcom/squareup/wire/Message;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->hashCode()I

    move-result v0

    return v0
.end method

.method public object()Lcom/squareup/wire/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObject;->object:Lcom/squareup/wire/Message;

    const-string v1, "messageObject"

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public toByteArray()[B
    .locals 2

    .line 205
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    .line 206
    const-class v1, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-static {v1}, Lcom/squareup/wire/ProtoAdapter;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 241
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->hasObjectExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/wire/Message;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 244
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
