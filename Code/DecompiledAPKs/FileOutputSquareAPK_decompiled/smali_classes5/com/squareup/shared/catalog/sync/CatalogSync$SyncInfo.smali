.class final Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;
.super Ljava/lang/Object;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SyncInfo"
.end annotation


# instance fields
.field final cogsServerVersion:J

.field final hasSession:Z

.field final lastSyncTimestamp:Ljava/util/Date;

.field final requiresSyntheticTableRebuild:Z

.field final syncedObjectTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field final versionSyncIncomplete:Z

.field final writeRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/PendingWriteRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(JLjava/util/Date;ZZZLjava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Date;",
            "ZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/PendingWriteRequest;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 606
    iput-wide p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    .line 607
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    .line 608
    iput-boolean p4, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->hasSession:Z

    .line 609
    iput-boolean p5, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->versionSyncIncomplete:Z

    .line 610
    iput-boolean p6, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    if-nez p7, :cond_0

    .line 611
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p7

    :cond_0
    iput-object p7, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    if-nez p8, :cond_1

    .line 612
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p8

    :cond_1
    iput-object p8, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(JLjava/util/Date;ZZZLjava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V
    .locals 0

    .line 588
    invoke-direct/range {p0 .. p8}, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;-><init>(JLjava/util/Date;ZZZLjava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_9

    .line 617
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_1

    .line 619
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    .line 621
    iget-wide v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    iget-wide v3, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_2

    return v0

    .line 622
    :cond_2
    iget-boolean v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->hasSession:Z

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->hasSession:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 623
    :cond_3
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    if-eqz v1, :cond_4

    iget-object v2, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_4
    iget-object v1, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    if-eqz v1, :cond_5

    :goto_0
    return v0

    .line 627
    :cond_5
    iget-boolean v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->versionSyncIncomplete:Z

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->versionSyncIncomplete:Z

    if-eq v1, v2, :cond_6

    return v0

    .line 628
    :cond_6
    iget-boolean v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    if-eq v1, v2, :cond_7

    return v0

    .line 630
    :cond_7
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    return v0

    .line 632
    :cond_8
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_9
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 4

    .line 636
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 637
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 638
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->hasSession:Z

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 639
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->versionSyncIncomplete:Z

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 640
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 641
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 642
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method
