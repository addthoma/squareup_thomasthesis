.class final Lcom/squareup/shared/catalog/CatalogLocal;
.super Ljava/lang/Object;
.source "CatalogLocal.java"

# interfaces
.implements Lcom/squareup/shared/catalog/Catalog$Local;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    }
.end annotation


# static fields
.field private static final COMP_DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private static final DINING_OPTION_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogDiningOption;",
            ">;"
        }
    .end annotation
.end field

.field private static final DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private static final MENU_NAME_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogMenu;",
            ">;"
        }
    .end annotation
.end field

.field private static final PAGE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation
.end field

.field private static final TICKET_TEMPLATE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private static final VOID_REASON_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
            "Lcom/squareup/shared/catalog/models/CatalogVoidReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

.field private final catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

.field private final endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

.field private final syntheticTableReaderLookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 83
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$1;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$1;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->PAGE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 94
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$2;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$2;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->MENU_NAME_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 105
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$3;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$3;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->DINING_OPTION_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 116
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$4;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$4;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->VOID_REASON_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 127
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$5;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$5;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->COMP_DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 138
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$6;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$6;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    .line 149
    new-instance v0, Lcom/squareup/shared/catalog/CatalogLocal$7;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/CatalogLocal$7;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->TICKET_TEMPLATE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/shared/catalog/CatalogStoreProvider;Lcom/squareup/shared/catalog/CatalogEndpoint;Ljava/util/List;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogStoreProvider;",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")V"
        }
    .end annotation

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->syntheticTableReaderLookup:Ljava/util/Map;

    .line 167
    iput-object p1, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    .line 168
    iput-object p2, p0, Lcom/squareup/shared/catalog/CatalogLocal;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 169
    iput-object p4, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    .line 170
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    .line 171
    iget-object p3, p0, Lcom/squareup/shared/catalog/CatalogLocal;->syntheticTableReaderLookup:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p4

    invoke-interface {p3, p4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private copyIdAsDeletedCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/api/sync/ObjectWrapper;
    .locals 1

    .line 718
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    const-string v0, "object_id"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/sync/ObjectId;

    .line 720
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->deleted(Ljava/lang/Boolean;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1
.end method

.method private createResult(Ljava/util/List;Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation

    .line 502
    new-instance v0, Ljava/util/HashMap;

    .line 503
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 505
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 508
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 509
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 510
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 514
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 515
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getModifierListId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 516
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method private getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;
    .locals 1

    .line 739
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->assertNoVersionSyncInProgress()V

    .line 740
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->get()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    return-object v0
.end method

.method private getNextSessionStateOrThrow()Lcom/squareup/api/sync/WritableSessionState;
    .locals 6

    .line 751
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readSessionId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/shared/catalog/CatalogStore;->readClientStateSeq()J

    move-result-wide v1

    .line 755
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v3

    const-wide/16 v4, 0x1

    add-long/2addr v4, v1

    invoke-interface {v3, v4, v5}, Lcom/squareup/shared/catalog/CatalogStore;->writeClientStateSeq(J)V

    .line 757
    new-instance v3, Lcom/squareup/api/sync/WritableSessionState$Builder;

    invoke-direct {v3}, Lcom/squareup/api/sync/WritableSessionState$Builder;-><init>()V

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/api/sync/WritableSessionState$Builder;->seq(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/api/sync/WritableSessionState$Builder;->session_id(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/WritableSessionState$Builder;->build()Lcom/squareup/api/sync/WritableSessionState;

    move-result-object v0

    return-object v0

    .line 752
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Catalog session ID."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private mapIds(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 491
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 492
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 493
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public canWrite()Z
    .locals 1

    .line 714
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readSessionId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public countDiscounts()I
    .locals 2

    .line 530
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->count(Lcom/squareup/shared/catalog/models/CatalogObjectType;)I

    move-result v0

    return v0
.end method

.method public countItems(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    .line 526
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->countItemsWithTypes(Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public countTaxItems(Ljava/lang/String;Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    .line 649
    sget-object v0, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    .line 650
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->countRelatedItems(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public findAllItemModifiers()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation

    .line 483
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 484
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/CatalogLocal;->mapIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 486
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-interface {v2, v3, v1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 487
    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/catalog/CatalogLocal;->createResult(Ljava/util/List;Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public findAllMenus()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogMenu;",
            ">;"
        }
    .end annotation

    .line 335
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 336
    sget-object v1, Lcom/squareup/shared/catalog/CatalogLocal;->MENU_NAME_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public findAllTicketTemplatesByGroupId()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    .line 616
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 619
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v1, v2}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v1

    .line 623
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 624
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v3

    .line 625
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_0

    .line 627
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 628
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    :cond_0
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 633
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 635
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 636
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 637
    sget-object v3, Lcom/squareup/shared/catalog/CatalogLocal;->TICKET_TEMPLATE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method public findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 209
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 213
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public findByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 217
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public findCatalogConnectV2Object(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 184
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readConnectV2ObjectById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    return-object p1
.end method

.method public findCatalogConnectV2Objects(Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 190
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readConnectV2ObjectsByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findCategoryItems(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation

    .line 457
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findEnabledItemModifiers(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation

    .line 463
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal;->findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 464
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 465
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    .line 466
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 467
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierListId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 471
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    .line 472
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v1

    const-class v2, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    invoke-interface {v1, v2, v0}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 475
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-interface {v1, v2, v0}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 477
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/CatalogLocal;->createResult(Ljava/util/List;Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public findItemFeeMemberships(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 563
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemFeeMemberships(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 572
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->resolveMemberships(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemFeeMembershipsForTax(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 567
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemModifierListMemberships(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation

    .line 590
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_ITEM_MODIFIER_LISTS:Lcom/squareup/shared/catalog/ManyToMany;

    .line 591
    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    .line 590
    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->resolveMemberships(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation

    .line 601
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemModifierRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;>;"
        }
    .end annotation

    .line 596
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_ITEM_MODIFIER_LISTS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p1

    return-object p1
.end method

.method public findItemPageMembershipsForPages(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            ">;"
        }
    .end annotation

    .line 449
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemTaxRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation

    .line 559
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_FEES:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p1

    return-object p1
.end method

.method public findItemTaxes(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation

    .line 453
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_FEES:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findItemVariations(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 549
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findModifierItemMemberships(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation

    .line 606
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findModifierOptions(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;"
        }
    .end annotation

    .line 553
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 554
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 553
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findObjectIdsForRelatedObjects(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 535
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/shared/catalog/CatalogStore;->findReferences(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 231
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation

    .line 226
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public findObjectsByTokens(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 222
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readByTokens(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public findPageTiles(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/PageTiles;
    .locals 9

    .line 358
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 360
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 362
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MEMBERSHIP_PAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v2, v3, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 364
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 365
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 366
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getTileObjectStringId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    :cond_0
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v2

    .line 372
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 373
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 375
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getTileObjectStringId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogObject;

    if-nez v5, :cond_1

    goto :goto_1

    .line 382
    :cond_1
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v6

    sget-object v7, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v6, v7, :cond_2

    .line 383
    move-object v6, v5

    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 384
    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 385
    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_2
    new-instance v6, Lcom/squareup/shared/catalog/models/Tile;

    invoke-direct {v6, v4, v5}, Lcom/squareup/shared/catalog/models/Tile;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;Lcom/squareup/shared/catalog/models/CatalogObject;)V

    .line 390
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v4

    sget-object v7, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v4, v7, :cond_4

    move-object v4, v5

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v4

    sget-object v7, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v4, v7, :cond_4

    .line 392
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_3

    .line 394
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 395
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    :cond_3
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 399
    :cond_4
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 403
    :cond_5
    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_9

    .line 406
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    new-instance v4, Ljava/util/ArrayList;

    .line 407
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 406
    invoke-interface {p1, v2, v4}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 408
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 409
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 410
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/Tile;

    .line 411
    new-instance v6, Lcom/squareup/shared/catalog/models/Tile;

    iget-object v7, v5, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget-object v5, v5, Lcom/squareup/shared/catalog/models/Tile;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 412
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-direct {v6, v7, v5, v8}, Lcom/squareup/shared/catalog/models/Tile;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/protos/common/Money;)V

    .line 411
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 416
    :cond_7
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 417
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/Tile;

    .line 418
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 424
    :cond_9
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_a

    .line 425
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p1

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-interface {p1, v1, v3}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    goto :goto_4

    .line 427
    :cond_a
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    .line 429
    :goto_4
    new-instance v1, Lcom/squareup/shared/catalog/models/PageTiles;

    invoke-direct {v1, v0, p1}, Lcom/squareup/shared/catalog/models/PageTiles;-><init>(Ljava/util/List;Ljava/util/Map;)V

    return-object v1
.end method

.method public findPagesForMenuGroup(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/PageLayout;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation

    .line 434
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 435
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 437
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 438
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 439
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogPage;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object v2

    if-ne v2, p2, :cond_0

    .line 440
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 443
    :cond_1
    sget-object p1, Lcom/squareup/shared/catalog/CatalogLocal;->PAGE_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public findRelatedItemIdsForTax(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 539
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->resolveIdsForRelationship(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findRelatedItemIdsForTax(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 544
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->resolveRelatedObjectIds(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findSurchargeFeeMemberships(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;",
            ">;"
        }
    .end annotation

    .line 578
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 579
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 578
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findSurchargeFeeMembershipsForTax(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;",
            ">;"
        }
    .end annotation

    .line 584
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findTaxItemRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation

    .line 655
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p1

    return-object p1
.end method

.method public findTaxItems(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation

    .line 659
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/ManyToMany;->createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findTicketTemplates(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation

    .line 644
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 645
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 644
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public findTilePageMemberships(Ljava/lang/String;Lcom/squareup/api/items/Type;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Type;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            ">;"
        }
    .end annotation

    .line 611
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-static {p2}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->buildTileRelation(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object p2

    .line 612
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 611
    invoke-interface {v0, p2, p1}, Lcom/squareup/shared/catalog/CatalogStore;->findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getCategoryNameMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 341
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 344
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->readAllCategories()Lcom/squareup/shared/catalog/CatalogObjectCursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 345
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->get()Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 347
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 352
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->close()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->close()V

    :cond_2
    throw v0
.end method

.method public getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 725
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    .line 727
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->syntheticTableReaderLookup:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    if-eqz v0, :cond_0

    return-object v0

    .line 729
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 730
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not found. Did you remember to register it in CogsBuilder?"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasAppliedServerVersion()Z
    .locals 1

    .line 666
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->hasAppliedServerVersion()Z

    move-result v0

    return v0
.end method

.method public readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    .line 178
    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromModelClass(Ljava/lang/Class;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 179
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public readAllCategories()Lcom/squareup/shared/catalog/CatalogObjectCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogObjectCursor<",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;"
        }
    .end annotation

    .line 299
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v0

    .line 300
    const-class v1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->from(Ljava/lang/Class;Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/CatalogObjectCursor;

    move-result-object v0

    return-object v0
.end method

.method public readAllDiningOptions()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiningOption;",
            ">;"
        }
    .end annotation

    .line 248
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 251
    sget-object v1, Lcom/squareup/shared/catalog/CatalogLocal;->DINING_OPTION_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 253
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v2, 0x0

    .line 254
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 255
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object v3

    .line 256
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v3, v2, -0x1

    .line 257
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v2, v3

    goto :goto_1

    .line 259
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public readAllFixedDiscounts()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation

    .line 305
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 306
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 307
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 308
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isVariable()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v3

    if-nez v3, :cond_0

    .line 309
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_1
    sget-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v1
.end method

.method public readAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 326
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public readAllPages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation

    .line 317
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public readAllPlaceholders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPlaceholder;",
            ">;"
        }
    .end annotation

    .line 321
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public readAllProductSets()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation

    .line 235
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public readAllTaxRules()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation

    .line 243
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public readAllTaxes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation

    .line 239
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public readAppliedServerVersion()J
    .locals 2

    .line 205
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogStore;->readCogsAppliedServerVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public readCompDiscounts()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation

    .line 287
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 288
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 289
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 290
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 294
    :cond_1
    sget-object v0, Lcom/squareup/shared/catalog/CatalogLocal;->COMP_DISCOUNT_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v1
.end method

.method public readConfiguration()Lcom/squareup/shared/catalog/models/CatalogConfiguration;
    .locals 4

    .line 267
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TILE_APPEARANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 268
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Catalog configuration must not be null. Resetting storage."

    .line 270
    iget-object v2, p0, Lcom/squareup/shared/catalog/CatalogLocal;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    const/4 v1, 0x0

    .line 272
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    return-object v0
.end method

.method public readIdsForAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 331
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogStore;->readIdsForAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public readItems(Ljava/util/Set;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation

    .line 522
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/CatalogStore;->readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public readVoidReasons()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogVoidReason;",
            ">;"
        }
    .end annotation

    .line 277
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->VOID_REASON:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;

    move-result-object v0

    .line 280
    sget-object v1, Lcom/squareup/shared/catalog/CatalogLocal;->VOID_REASON_COMPARATOR:Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public write(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 671
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/shared/catalog/CatalogLocal;->write(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    return-void
.end method

.method public write(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 678
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :cond_0
    if-nez p2, :cond_1

    .line 681
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_1
    if-nez p3, :cond_2

    .line 684
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p3

    .line 687
    :cond_2
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    return-void

    .line 691
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogLocal;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 695
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getNextSessionStateOrThrow()Lcom/squareup/api/sync/WritableSessionState;

    move-result-object v0

    .line 697
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 698
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 699
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 701
    :cond_4
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 702
    invoke-direct {p0, v3}, Lcom/squareup/shared/catalog/CatalogLocal;->copyIdAsDeletedCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 705
    :cond_5
    iget-object v2, p0, Lcom/squareup/shared/catalog/CatalogLocal;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    invoke-virtual {v2, v1, v0}, Lcom/squareup/shared/catalog/CatalogEndpoint;->createPutRequest(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/rpc/Request;

    move-result-object v0

    .line 707
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 708
    invoke-interface {v1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 710
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p2

    invoke-interface {p2, p1, v1, v0}, Lcom/squareup/shared/catalog/CatalogStore;->writeAndEnqueue(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/api/rpc/Request;)V

    return-void

    .line 692
    :cond_6
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "Catalog endpoint cannot update items."

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public writeConnectV2Object(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)V
    .locals 2

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 195
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->writeConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V

    return-void
.end method

.method public writeConnectV2Objects(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 201
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal;->getCatalogStore()Lcom/squareup/shared/catalog/CatalogStore;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/CatalogStore;->writeConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V

    return-void
.end method
