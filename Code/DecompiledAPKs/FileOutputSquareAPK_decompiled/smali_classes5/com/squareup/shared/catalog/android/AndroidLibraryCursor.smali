.class public Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;
.super Ljava/lang/Object;
.source "AndroidLibraryCursor.java"

# interfaces
.implements Landroid/database/Cursor;


# instance fields
.field private final cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 0

    .line 108
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "copyStringToBuffer is not supported"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public deactivate()V
    .locals 2

    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "deactivate is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBlob(I)[B
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getBlob(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getColumnCount()I
    .locals 2

    .line 96
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getColumnCount is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .line 88
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "getColumnName is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 2

    .line 92
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getColumnNames is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCount()I
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 1

    .line 128
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "getDouble is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 2

    .line 192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getExtras is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFloat(I)F
    .locals 1

    .line 124
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "getFloat is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getInt(I)I
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getInt(I)I

    move-result p1

    return p1
.end method

.method public getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    return-object v0
.end method

.method public getLong(I)J
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 2

    .line 180
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getNotificationUri is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPosition()I
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public getShort(I)S
    .locals 1

    .line 112
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "getShort is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getType(I)I
    .locals 1

    .line 132
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "getType is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 2

    .line 184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getWantsAllOnMoveCalls is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isAfterLast()Z
    .locals 2

    .line 76
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "isAfterLast is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isBeforeFirst()Z
    .locals 2

    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "isBeforeFirst is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isClosed()Z
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isFirst()Z
    .locals 2

    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "isFirst is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isLast()Z
    .locals 2

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "isLast is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isNull(I)Z
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isNull(I)Z

    move-result p1

    return p1
.end method

.method public move(I)Z
    .locals 1

    .line 40
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "move is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public moveToFirst()Z
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 2

    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "moveToLast is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public moveToNext()Z
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    move-result p1

    return p1
.end method

.method public moveToPrevious()Z
    .locals 2

    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "moveToPrevious is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    return-void
.end method

.method public requery()Z
    .locals 2

    .line 144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "requery is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .line 196
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "respond is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    .line 188
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setExtras is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0

    .line 176
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "setNotificationUri is not supported"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    return-void
.end method
