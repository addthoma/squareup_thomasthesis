.class public Lcom/squareup/shared/catalog/utils/Murmur3;
.super Ljava/lang/Object;
.source "Murmur3.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/Murmur3$IncrementalHash32;
    }
.end annotation


# static fields
.field private static final C1:J = -0x783c846eeebdac2bL

.field private static final C1_32:I = -0x3361d2af

.field private static final C2:J = 0x4cf5ad432745937fL

.field private static final C2_32:I = 0x1b873593

.field public static final DEFAULT_SEED:I = 0x19919

.field private static final M:I = 0x5

.field private static final M_32:I = 0x5

.field private static final N1:I = 0x52dce729

.field private static final N2:I = 0x38495ab5

.field public static final NULL_HASHCODE:J = 0x27bb2ee687b0b0fdL

.field private static final N_32:I = -0x19ab949c

.field private static final R1:I = 0x1f

.field private static final R1_32:I = 0xf

.field private static final R2:I = 0x1b

.field private static final R2_32:I = 0xd

.field private static final R3:I = 0x21


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(BBBB)I
    .locals 0

    .line 15
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/utils/Murmur3;->orBytes(BBBB)I

    move-result p0

    return p0
.end method

.method private static fmix64(J)J
    .locals 3

    const/16 v0, 0x21

    ushr-long v1, p0, v0

    xor-long/2addr p0, v1

    const-wide v1, -0xae502812aa7333L

    mul-long p0, p0, v1

    ushr-long v1, p0, v0

    xor-long/2addr p0, v1

    const-wide v1, -0x3b314601e57a13adL    # -2.902039044684214E23

    mul-long p0, p0, v1

    ushr-long v0, p0, v0

    xor-long/2addr p0, v0

    return-wide p0
.end method

.method public static hash128([B)[J
    .locals 3

    .line 260
    array-length v0, p0

    const/4 v1, 0x0

    const v2, 0x19919

    invoke-static {p0, v1, v0, v2}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash128([BIII)[J

    move-result-object p0

    return-object p0
.end method

.method public static hash128([BIII)[J
    .locals 25

    move/from16 v0, p2

    move/from16 v1, p3

    int-to-long v1, v1

    shr-int/lit8 v3, v0, 0x4

    const/4 v4, 0x0

    move-wide v5, v1

    :goto_0
    const/16 v8, 0x38

    const/16 v13, 0x30

    const/16 v14, 0x28

    const/16 v15, 0x20

    const/16 v16, 0x18

    const/16 v17, 0x10

    const/16 v18, 0x8

    if-ge v4, v3, :cond_0

    shl-int/lit8 v19, v4, 0x4

    add-int v19, p1, v19

    .line 280
    aget-byte v9, p0, v19

    int-to-long v9, v9

    const-wide/16 v20, 0xff

    and-long v9, v9, v20

    add-int/lit8 v22, v19, 0x1

    aget-byte v7, p0, v22

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long v11, v11, v18

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x2

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long v11, v11, v17

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x3

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long v11, v11, v16

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x4

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long/2addr v11, v15

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x5

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long/2addr v11, v14

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x6

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long/2addr v11, v13

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x7

    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    shl-long/2addr v11, v8

    or-long/2addr v9, v11

    add-int/lit8 v7, v19, 0x8

    .line 289
    aget-byte v7, p0, v7

    int-to-long v11, v7

    and-long v11, v11, v20

    add-int/lit8 v7, v19, 0x9

    aget-byte v7, p0, v7

    move-wide/from16 v23, v9

    int-to-long v8, v7

    and-long v7, v8, v20

    shl-long v7, v7, v18

    or-long/2addr v7, v11

    add-int/lit8 v9, v19, 0xa

    aget-byte v9, p0, v9

    int-to-long v9, v9

    and-long v9, v9, v20

    shl-long v9, v9, v17

    or-long/2addr v7, v9

    add-int/lit8 v9, v19, 0xb

    aget-byte v9, p0, v9

    int-to-long v9, v9

    and-long v9, v9, v20

    shl-long v9, v9, v16

    or-long/2addr v7, v9

    add-int/lit8 v9, v19, 0xc

    aget-byte v9, p0, v9

    int-to-long v9, v9

    and-long v9, v9, v20

    shl-long/2addr v9, v15

    or-long/2addr v7, v9

    add-int/lit8 v9, v19, 0xd

    aget-byte v9, p0, v9

    int-to-long v9, v9

    and-long v9, v9, v20

    shl-long/2addr v9, v14

    or-long/2addr v7, v9

    add-int/lit8 v9, v19, 0xe

    aget-byte v9, p0, v9

    int-to-long v9, v9

    and-long v9, v9, v20

    shl-long/2addr v9, v13

    or-long/2addr v7, v9

    add-int/lit8 v19, v19, 0xf

    aget-byte v9, p0, v19

    int-to-long v9, v9

    and-long v9, v9, v20

    const/16 v11, 0x38

    shl-long/2addr v9, v11

    or-long/2addr v7, v9

    const-wide v9, -0x783c846eeebdac2bL

    mul-long v11, v23, v9

    const/16 v9, 0x1f

    .line 300
    invoke-static {v11, v12, v9}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v10

    const-wide v12, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v10, v10, v12

    xor-long/2addr v1, v10

    const/16 v9, 0x1b

    .line 303
    invoke-static {v1, v2, v9}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v1

    add-long/2addr v1, v5

    const-wide/16 v9, 0x5

    mul-long v1, v1, v9

    const-wide/32 v9, 0x52dce729

    add-long/2addr v1, v9

    mul-long v7, v7, v12

    const/16 v9, 0x21

    .line 309
    invoke-static {v7, v8, v9}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v7

    const-wide v9, -0x783c846eeebdac2bL

    mul-long v7, v7, v9

    xor-long/2addr v5, v7

    const/16 v7, 0x1f

    .line 312
    invoke-static {v5, v6, v7}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v5

    add-long/2addr v5, v1

    const-wide/16 v7, 0x5

    mul-long v5, v5, v7

    const-wide/32 v7, 0x38495ab5

    add-long/2addr v5, v7

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_0
    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    shl-int/lit8 v3, v3, 0x4

    sub-int v4, v0, v3

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0xe

    .line 323
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long/2addr v11, v13

    xor-long/2addr v9, v11

    :pswitch_1
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0xd

    .line 325
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long/2addr v11, v14

    xor-long/2addr v9, v11

    :pswitch_2
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0xc

    .line 327
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long/2addr v11, v15

    xor-long/2addr v9, v11

    :pswitch_3
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0xb

    .line 329
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long v11, v11, v16

    xor-long/2addr v9, v11

    :pswitch_4
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0xa

    .line 331
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long v11, v11, v17

    xor-long/2addr v9, v11

    :pswitch_5
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x9

    .line 333
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    shl-long v11, v11, v18

    xor-long/2addr v9, v11

    :pswitch_6
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x8

    .line 335
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v11, v4

    xor-long/2addr v9, v11

    const-wide v11, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v9, v9, v11

    const/16 v4, 0x21

    .line 337
    invoke-static {v9, v10, v4}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v9

    const-wide v11, -0x783c846eeebdac2bL

    mul-long v9, v9, v11

    xor-long/2addr v5, v9

    :pswitch_7
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x7

    .line 342
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    const/16 v4, 0x38

    shl-long/2addr v9, v4

    xor-long/2addr v7, v9

    :pswitch_8
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x6

    .line 344
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long/2addr v9, v13

    xor-long/2addr v7, v9

    :pswitch_9
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x5

    .line 346
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long/2addr v9, v14

    xor-long/2addr v7, v9

    :pswitch_a
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x4

    .line 348
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long/2addr v9, v15

    xor-long/2addr v7, v9

    :pswitch_b
    add-int v4, p1, v3

    add-int/lit8 v4, v4, 0x3

    .line 350
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long v9, v9, v16

    xor-long/2addr v7, v9

    :pswitch_c
    add-int v4, p1, v3

    const/4 v9, 0x2

    add-int/2addr v4, v9

    .line 352
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long v9, v9, v17

    xor-long/2addr v7, v9

    :pswitch_d
    add-int v4, p1, v3

    const/4 v9, 0x1

    add-int/2addr v4, v9

    .line 354
    aget-byte v4, p0, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v9, v4

    shl-long v9, v9, v18

    xor-long/2addr v7, v9

    :pswitch_e
    add-int v3, p1, v3

    .line 356
    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v3, v3

    xor-long/2addr v3, v7

    const-wide v7, -0x783c846eeebdac2bL

    mul-long v3, v3, v7

    const/16 v7, 0x1f

    .line 358
    invoke-static {v3, v4, v7}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v3

    const-wide v7, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v3, v3, v7

    xor-long/2addr v1, v3

    :goto_1
    int-to-long v3, v0

    xor-long v0, v1, v3

    xor-long v2, v5, v3

    add-long/2addr v0, v2

    add-long/2addr v2, v0

    .line 370
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide v0

    .line 371
    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    add-long/2addr v2, v0

    const/4 v4, 0x2

    new-array v4, v4, [J

    const/4 v5, 0x0

    aput-wide v0, v4, v5

    const/4 v0, 0x1

    aput-wide v2, v4, v0

    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static hash32([B)I
    .locals 3

    .line 46
    array-length v0, p0

    const/4 v1, 0x0

    const v2, 0x19919

    invoke-static {p0, v1, v0, v2}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash32([BIII)I

    move-result p0

    return p0
.end method

.method public static hash32([BI)I
    .locals 2

    const/4 v0, 0x0

    const v1, 0x19919

    .line 57
    invoke-static {p0, v0, p1, v1}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash32([BIII)I

    move-result p0

    return p0
.end method

.method public static hash32([BII)I
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-static {p0, v0, p1, p2}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash32([BIII)I

    move-result p0

    return p0
.end method

.method public static hash32([BIII)I
    .locals 10

    shr-int/lit8 v0, p2, 0x2

    const/4 v1, 0x0

    move v2, p3

    const/4 p3, 0x0

    :goto_0
    const v3, 0x1b873593

    const/16 v4, 0xf

    const v5, -0x3361d2af    # -8.2930312E7f

    const/4 v6, 0x3

    if-ge p3, v0, :cond_0

    shl-int/lit8 v7, p3, 0x2

    add-int/2addr v7, p1

    .line 88
    aget-byte v8, p0, v7

    and-int/lit16 v8, v8, 0xff

    add-int/lit8 v9, v7, 0x1

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    or-int/2addr v8, v9

    add-int/lit8 v9, v7, 0x2

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x10

    or-int/2addr v8, v9

    add-int/2addr v7, v6

    aget-byte v6, p0, v7

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x18

    or-int/2addr v6, v8

    mul-int v6, v6, v5

    .line 95
    invoke-static {v6, v4}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v4

    mul-int v4, v4, v3

    xor-int/2addr v2, v4

    const/16 v3, 0xd

    .line 98
    invoke-static {v2, v3}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    const v3, -0x19ab949c

    add-int/2addr v2, v3

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x2

    shl-int/2addr v0, p3

    sub-int v7, p2, v0

    const/4 v8, 0x1

    if-eq v7, v8, :cond_3

    if-eq v7, p3, :cond_2

    if-eq v7, v6, :cond_1

    goto :goto_1

    :cond_1
    add-int v6, p1, v0

    add-int/2addr v6, p3

    .line 106
    aget-byte p3, p0, v6

    shl-int/lit8 p3, p3, 0x10

    xor-int/2addr v1, p3

    :cond_2
    add-int p3, p1, v0

    add-int/2addr p3, v8

    .line 108
    aget-byte p3, p0, p3

    shl-int/lit8 p3, p3, 0x8

    xor-int/2addr v1, p3

    :cond_3
    add-int/2addr p1, v0

    .line 110
    aget-byte p0, p0, p1

    xor-int/2addr p0, v1

    mul-int p0, p0, v5

    .line 114
    invoke-static {p0, v4}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result p0

    mul-int p0, p0, v3

    xor-int/2addr v2, p0

    :goto_1
    xor-int p0, v2, p2

    ushr-int/lit8 p1, p0, 0x10

    xor-int/2addr p0, p1

    const p1, -0x7a143595

    mul-int p0, p0, p1

    ushr-int/lit8 p1, p0, 0xd

    xor-int/2addr p0, p1

    const p1, -0x3d4d51cb

    mul-int p0, p0, p1

    ushr-int/lit8 p1, p0, 0x10

    xor-int/2addr p0, p1

    return p0
.end method

.method public static hash64(I)J
    .locals 4

    .line 157
    invoke-static {p0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result p0

    int-to-long v0, p0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    const-wide v2, -0x783c846eeebdac2bL

    mul-long v0, v0, v2

    const/16 p0, 0x1f

    .line 161
    invoke-static {v0, v1, p0}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v0

    const-wide v2, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v0, v0, v2

    const-wide/32 v2, 0x19919

    xor-long/2addr v0, v2

    const/4 p0, 0x4

    int-to-long v2, p0

    xor-long/2addr v0, v2

    .line 166
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64(J)J
    .locals 2

    .line 142
    invoke-static {p0, p1}, Ljava/lang/Long;->reverseBytes(J)J

    move-result-wide p0

    const-wide v0, -0x783c846eeebdac2bL

    mul-long p0, p0, v0

    const/16 v0, 0x1f

    .line 146
    invoke-static {p0, p1, v0}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide p0

    const-wide v0, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long p0, p0, v0

    const-wide/32 v0, 0x19919

    xor-long/2addr p0, v0

    const/16 v0, 0x1b

    .line 149
    invoke-static {p0, p1, v0}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide p0

    const-wide/16 v0, 0x5

    mul-long p0, p0, v0

    const-wide/32 v0, 0x52dce729

    add-long/2addr p0, v0

    const/16 v0, 0x8

    int-to-long v0, v0

    xor-long/2addr p0, v0

    .line 152
    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide p0

    return-wide p0
.end method

.method public static hash64(S)J
    .locals 7

    int-to-long v0, p0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    const/16 v4, 0x8

    shl-long/2addr v0, v4

    const-wide/16 v5, 0x0

    xor-long/2addr v0, v5

    const v5, 0xff00

    and-int/2addr p0, v5

    shr-int/2addr p0, v4

    int-to-long v4, p0

    and-long/2addr v2, v4

    xor-long/2addr v0, v2

    const-wide v2, -0x783c846eeebdac2bL

    mul-long v0, v0, v2

    const/16 p0, 0x1f

    .line 176
    invoke-static {v0, v1, p0}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v0

    const-wide v2, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v0, v0, v2

    const-wide/32 v2, 0x19919

    xor-long/2addr v0, v2

    const-wide/16 v2, 0x2

    xor-long/2addr v0, v2

    .line 182
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64([B)J
    .locals 3

    .line 137
    array-length v0, p0

    const/4 v1, 0x0

    const v2, 0x19919

    invoke-static {p0, v1, v0, v2}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash64([BIII)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hash64([BII)J
    .locals 1

    const v0, 0x19919

    .line 187
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/shared/catalog/utils/Murmur3;->hash64([BIII)J

    move-result-wide p0

    return-wide p0
.end method

.method public static hash64([BIII)J
    .locals 17

    move/from16 v0, p2

    move/from16 v1, p3

    int-to-long v1, v1

    shr-int/lit8 v3, v0, 0x3

    const/4 v4, 0x0

    :goto_0
    const/16 v10, 0x20

    const/16 v11, 0x18

    const/16 v12, 0x10

    const/16 v13, 0x8

    const-wide/16 v14, 0xff

    if-ge v4, v3, :cond_0

    shl-int/lit8 v16, v4, 0x3

    add-int v16, p1, v16

    .line 205
    aget-byte v5, p0, v16

    int-to-long v6, v5

    and-long v5, v6, v14

    add-int/lit8 v7, v16, 0x1

    aget-byte v7, p0, v7

    int-to-long v8, v7

    and-long v7, v8, v14

    shl-long/2addr v7, v13

    or-long/2addr v5, v7

    add-int/lit8 v7, v16, 0x2

    aget-byte v7, p0, v7

    int-to-long v7, v7

    and-long/2addr v7, v14

    shl-long/2addr v7, v12

    or-long/2addr v5, v7

    add-int/lit8 v7, v16, 0x3

    aget-byte v7, p0, v7

    int-to-long v7, v7

    and-long/2addr v7, v14

    shl-long/2addr v7, v11

    or-long/2addr v5, v7

    add-int/lit8 v7, v16, 0x4

    aget-byte v7, p0, v7

    int-to-long v7, v7

    and-long/2addr v7, v14

    shl-long/2addr v7, v10

    or-long/2addr v5, v7

    add-int/lit8 v7, v16, 0x5

    aget-byte v7, p0, v7

    int-to-long v7, v7

    and-long/2addr v7, v14

    const/16 v9, 0x28

    shl-long/2addr v7, v9

    or-long/2addr v5, v7

    add-int/lit8 v7, v16, 0x6

    aget-byte v7, p0, v7

    int-to-long v7, v7

    and-long/2addr v7, v14

    const/16 v9, 0x30

    shl-long/2addr v7, v9

    or-long/2addr v5, v7

    add-int/lit8 v16, v16, 0x7

    aget-byte v7, p0, v16

    int-to-long v7, v7

    and-long/2addr v7, v14

    const/16 v9, 0x38

    shl-long/2addr v7, v9

    or-long/2addr v5, v7

    const-wide v7, -0x783c846eeebdac2bL

    mul-long v5, v5, v7

    const/16 v7, 0x1f

    .line 216
    invoke-static {v5, v6, v7}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v5

    const-wide v7, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v5, v5, v7

    xor-long/2addr v1, v5

    const/16 v5, 0x1b

    .line 219
    invoke-static {v1, v2, v5}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v1

    const-wide/16 v5, 0x5

    mul-long v1, v1, v5

    const-wide/32 v5, 0x52dce729

    add-long/2addr v1, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x0

    shl-int/lit8 v3, v3, 0x3

    sub-int v6, v0, v3

    packed-switch v6, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x6

    .line 227
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    const/16 v8, 0x30

    shl-long/2addr v6, v8

    xor-long/2addr v4, v6

    :pswitch_1
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x5

    .line 229
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    const/16 v8, 0x28

    shl-long/2addr v6, v8

    xor-long/2addr v4, v6

    :pswitch_2
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x4

    .line 231
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    shl-long/2addr v6, v10

    xor-long/2addr v4, v6

    :pswitch_3
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x3

    .line 233
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    shl-long/2addr v6, v11

    xor-long/2addr v4, v6

    :pswitch_4
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x2

    .line 235
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    shl-long/2addr v6, v12

    xor-long/2addr v4, v6

    :pswitch_5
    add-int v6, p1, v3

    add-int/lit8 v6, v6, 0x1

    .line 237
    aget-byte v6, p0, v6

    int-to-long v6, v6

    and-long/2addr v6, v14

    shl-long/2addr v6, v13

    xor-long/2addr v4, v6

    :pswitch_6
    add-int v3, p1, v3

    .line 239
    aget-byte v3, p0, v3

    int-to-long v6, v3

    and-long/2addr v6, v14

    xor-long v3, v4, v6

    const-wide v5, -0x783c846eeebdac2bL

    mul-long v3, v3, v5

    const/16 v5, 0x1f

    .line 241
    invoke-static {v3, v4, v5}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v3

    const-wide v5, 0x4cf5ad432745937fL    # 5.573325460219186E62

    mul-long v3, v3, v5

    xor-long/2addr v1, v3

    :goto_1
    int-to-long v3, v0

    xor-long v0, v1, v3

    .line 248
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/Murmur3;->fmix64(J)J

    move-result-wide v0

    return-wide v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static orBytes(BBBB)I
    .locals 0

    and-int/lit16 p0, p0, 0xff

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p0, p1

    and-int/lit16 p1, p2, 0xff

    shl-int/lit8 p1, p1, 0x10

    or-int/2addr p0, p1

    and-int/lit16 p1, p3, 0xff

    shl-int/lit8 p1, p1, 0x18

    or-int/2addr p0, p1

    return p0
.end method
