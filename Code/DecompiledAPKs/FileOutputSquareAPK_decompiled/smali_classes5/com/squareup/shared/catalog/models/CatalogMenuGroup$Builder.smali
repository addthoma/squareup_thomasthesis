.class public final Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
.super Ljava/lang/Object;
.source "CatalogMenuGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogMenuGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final tag:Lcom/squareup/api/items/Tag$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 40
    new-instance v0, Lcom/squareup/api/items/Tag$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Tag$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Tag$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogMenuGroup;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Tag$Builder;->build()Lcom/squareup/api/items/Tag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag(Lcom/squareup/api/items/Tag;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 70
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Tag$Builder;->color(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Tag$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Tag$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;

    return-object p0
.end method

.method public setTagMemberships(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Tag$Builder;->tag_membership(Ljava/util/List;)Lcom/squareup/api/items/Tag$Builder;

    return-object p0
.end method

.method public setTagType(Lcom/squareup/api/items/Tag$Type;)Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;->tag:Lcom/squareup/api/items/Tag$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Tag$Builder;->type(Lcom/squareup/api/items/Tag$Type;)Lcom/squareup/api/items/Tag$Builder;

    return-object p0
.end method
