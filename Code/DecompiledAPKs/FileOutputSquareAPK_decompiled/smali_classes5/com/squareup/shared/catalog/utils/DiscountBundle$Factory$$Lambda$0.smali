.class final synthetic Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# instance fields
.field private final arg$1:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

.field private final arg$2:Ljava/lang/String;

.field private final arg$3:Ljava/util/TimeZone;

.field private final arg$4:Lcom/squareup/shared/i18n/Localizer;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$1:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$2:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$3:Ljava/util/TimeZone;

    iput-object p4, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$4:Lcom/squareup/shared/i18n/Localizer;

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$1:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$2:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$3:Ljava/util/TimeZone;

    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;->arg$4:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->lambda$forDiscount$0$DiscountBundle$Factory(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/utils/DiscountBundle;

    move-result-object p1

    return-object p1
.end method
