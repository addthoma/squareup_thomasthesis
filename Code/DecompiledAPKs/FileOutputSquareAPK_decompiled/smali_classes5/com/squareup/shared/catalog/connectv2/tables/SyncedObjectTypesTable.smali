.class public Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;
.super Ljava/lang/Object;
.source "SyncedObjectTypesTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;
    }
.end annotation


# static fields
.field public static COLUMN_OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field private static INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable; = null

.field public static NAME:Ljava/lang/String; = "connectv2_synced_object_types"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    .line 28
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    return-object v0
.end method


# virtual methods
.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public delete(Lcom/squareup/shared/sql/SQLDatabase;J)V
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 76
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public insert(Lcom/squareup/shared/sql/SQLDatabase;J)V
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->INSERT:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 82
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public readAllObjectTypes(Lcom/squareup/shared/sql/SQLDatabase;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    .line 87
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->READ_ALL_OBJECT_TYES:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method
