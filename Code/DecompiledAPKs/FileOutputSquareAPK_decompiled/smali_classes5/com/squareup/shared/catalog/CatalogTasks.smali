.class public Lcom/squareup/shared/catalog/CatalogTasks;
.super Ljava/lang/Object;
.source "CatalogTasks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/CatalogTasks$Failure;
    }
.end annotation


# static fields
.field private static final EXPLODE_ON_ERROR:Lcom/squareup/shared/catalog/CatalogCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$4;->$instance:Lcom/squareup/shared/catalog/CatalogCallback;

    sput-object v0, Lcom/squareup/shared/catalog/CatalogTasks;->EXPLODE_ON_ERROR:Lcom/squareup/shared/catalog/CatalogCallback;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static explodeOnError()Lcom/squareup/shared/catalog/CatalogCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/shared/catalog/CatalogTasks;->EXPLODE_ON_ERROR:Lcom/squareup/shared/catalog/CatalogCallback;

    return-object v0
.end method

.method public static fail(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$2;-><init>(Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Throwable;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static final synthetic lambda$fail$2$CatalogTasks(Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Throwable;)V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/shared/catalog/CatalogTasks$Failure;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogTasks$Failure;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method static final synthetic lambda$succeed$1$CatalogTasks(Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Object;)V
    .locals 0

    .line 34
    invoke-static {p1}, Lcom/squareup/shared/catalog/CatalogResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method static final synthetic lambda$syncWhenFinished$0$CatalogTasks(Lcom/squareup/shared/catalog/Catalog;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 27
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    .line 28
    sget-object p1, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$3;->$instance:Lcom/squareup/shared/catalog/sync/SyncCallback;

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lcom/squareup/shared/catalog/Catalog;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public static succeed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog;",
            ")",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$0;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/CatalogTasks$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/Catalog;)V

    return-object v0
.end method
