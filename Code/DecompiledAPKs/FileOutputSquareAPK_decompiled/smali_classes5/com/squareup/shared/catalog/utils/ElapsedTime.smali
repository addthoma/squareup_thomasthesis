.class public Lcom/squareup/shared/catalog/utils/ElapsedTime;
.super Ljava/lang/Object;
.source "ElapsedTime.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;
    }
.end annotation


# instance fields
.field private final interval:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

.field private final value:J


# direct methods
.method public constructor <init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V
    .locals 3

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_2

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    .line 25
    invoke-static {p3}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->access$000(Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    .line 30
    iput-object p3, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->interval:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    .line 31
    iput-wide p1, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->value:J

    return-void

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is more than Long.MAX_VALUE milliseconds."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Value is less than zero: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 20
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "interval"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public asMilliseconds()J
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->interval:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->value:J

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->asMilliseconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getInterval()Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->interval:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    return-object v0
.end method

.method public getValue()J
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/squareup/shared/catalog/utils/ElapsedTime;->value:J

    return-wide v0
.end method

.method public isGreaterThan(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .locals 4

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/ElapsedTime;->asMilliseconds()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;->asMilliseconds()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isLessThan(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .locals 4

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/ElapsedTime;->asMilliseconds()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;->asMilliseconds()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
