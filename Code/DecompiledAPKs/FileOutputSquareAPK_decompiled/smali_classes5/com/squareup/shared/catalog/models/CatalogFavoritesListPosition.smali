.class public final Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogFavoritesListPosition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/FavoritesListPosition;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getFavoriteObject()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FavoritesListPosition;

    iget-object v0, v0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 28
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FavoritesListPosition;

    iget-object v0, v0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FavoritesListPosition;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->getFavoriteObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 32
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 33
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v2, Lcom/squareup/api/items/Type;->FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

    .line 34
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->getFavoriteObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-direct {v1, v2, v3}, Lcom/squareup/shared/catalog/models/CatalogRelation;-><init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->getFavoriteObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;->getOrdinal()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;)V

    return-object v0
.end method
