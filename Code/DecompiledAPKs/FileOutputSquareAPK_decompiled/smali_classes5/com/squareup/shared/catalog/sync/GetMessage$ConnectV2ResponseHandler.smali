.class Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;
.super Ljava/lang/Object;
.source "GetMessage.java"

# interfaces
.implements Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectV2ResponseHandler"
.end annotation


# instance fields
.field private final callback:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final connectV2Objects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end field

.field private error:Lcom/squareup/shared/catalog/sync/SyncError;

.field private final lastBatchCogsObjects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/GetMessage;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 411
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 406
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    .line 412
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->lastBatchCogsObjects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    .line 413
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    .line 414
    new-instance p2, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    new-instance p3, Ljava/util/LinkedList;

    invoke-direct {p3}, Ljava/util/LinkedList;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {p2, p3, v0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->connectV2Objects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V
    .locals 0

    .line 401
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method private connectV2ObjectsFromSearchResponse(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;)Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ")",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 507
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 508
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 510
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->objects:Ljava/util/List;

    .line 511
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 512
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 513
    sget-object v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v3

    .line 514
    iget-object v2, v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_IS_DELETED:Ljava/lang/Boolean;

    invoke-static {v2, v4}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 515
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 517
    :cond_0
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 520
    :cond_1
    new-instance p1, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    const/4 v2, 0x0

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    return-object p1
.end method

.method static final synthetic lambda$handle$0$GetMessage$ConnectV2ResponseHandler(Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1

    .line 459
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 460
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 465
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 467
    :catch_0
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public complete(Z)V
    .locals 3

    .line 479
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$1200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 482
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 483
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 489
    new-instance p1, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const-string v1, "Connect v2 sync aborted."

    invoke-direct {p1, v0, v1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 495
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->lastBatchCogsObjects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$1600(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$1700(Lcom/squareup/shared/catalog/sync/GetMessage;I)V

    .line 498
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->lastBatchCogsObjects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->connectV2Objects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$1400(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public handle(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;)V"
        }
    .end annotation

    .line 418
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$1200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 421
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_0

    .line 422
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    return-void

    .line 426
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;

    .line 429
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 431
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/resources/Error;

    .line 432
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 434
    :cond_1
    new-instance p1, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    return-void

    .line 440
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->connectV2ObjectsFromSearchResponse(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;)Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    move-result-object p1

    .line 443
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$800(Lcom/squareup/shared/catalog/sync/GetMessage;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 445
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$1300(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-void

    .line 450
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    .line 453
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    new-instance v2, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v3, v3}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    new-instance v3, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler$$Lambda$0;

    invoke-direct {v3, v0}, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-static {v1, v2, p1, v3}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$1400(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    goto :goto_1

    .line 474
    :cond_4
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;->connectV2Objects:Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$1500(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)V

    :goto_1
    return-void
.end method
