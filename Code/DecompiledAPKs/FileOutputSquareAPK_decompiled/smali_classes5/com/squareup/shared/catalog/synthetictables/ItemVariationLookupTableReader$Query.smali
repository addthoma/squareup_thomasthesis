.class public final enum Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;
.super Ljava/lang/Enum;
.source "ItemVariationLookupTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

.field public static final enum LOOKUP_ITEM_FOR_SKU:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 50
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    const-string v1, "SELECT {sku}, {item_id}, {variation_id}, {item_name}, {variation_name}, {price_amt}, {price_currency}, {item_type}, {variation_ordinal}, {variation_count} FROM {sku_name} JOIN {library_name} ON {library_item_id} = {item_id} WHERE {sku} = ? COLLATE NOCASE and {item_type} IN (%item_types%)"

    .line 51
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "sku_name"

    const-string/jumbo v3, "variation_lookup"

    .line 56
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "library_name"

    const-string v3, "library"

    .line 57
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "sku"

    .line 58
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "item_id"

    .line 59
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_id"

    .line 60
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "item_name"

    const-string v3, "name"

    .line 61
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_name"

    .line 62
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "price_amt"

    const-string/jumbo v3, "variation_price_amount"

    .line 63
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "price_currency"

    const-string/jumbo v3, "variation_price_currency"

    .line 64
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "item_type"

    .line 65
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_ordinal"

    .line 66
    invoke-virtual {v1, v2, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_count"

    const-string/jumbo v3, "variations"

    .line 67
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "library_item_id"

    const-string v3, "object_id"

    .line 68
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 69
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 70
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "LOOKUP_ITEM_FOR_SKU"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->LOOKUP_ITEM_FOR_SKU:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    .line 49
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->LOOKUP_ITEM_FOR_SKU:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 75
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$Query;->query:Ljava/lang/String;

    return-object v0
.end method
