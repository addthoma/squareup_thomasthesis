.class final Lcom/squareup/shared/catalog/Storage$Locations;
.super Ljava/lang/Object;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/Storage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Locations"
.end annotation


# static fields
.field private static final OBJECTS_DIRECTORY_NAME:Ljava/lang/String; = "objects"

.field private static final STORAGE_METADATA_FILE_NAME:Ljava/lang/String; = "storage_metadata"


# instance fields
.field final metadataFile:Ljava/io/File;

.field final objectsDir:Ljava/io/File;

.field final storageDir:Ljava/io/File;


# direct methods
.method private constructor <init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 0

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iput-object p1, p0, Lcom/squareup/shared/catalog/Storage$Locations;->storageDir:Ljava/io/File;

    .line 273
    iput-object p2, p0, Lcom/squareup/shared/catalog/Storage$Locations;->objectsDir:Ljava/io/File;

    .line 274
    iput-object p3, p0, Lcom/squareup/shared/catalog/Storage$Locations;->metadataFile:Ljava/io/File;

    return-void
.end method

.method synthetic constructor <init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Lcom/squareup/shared/catalog/Storage$1;)V
    .locals 0

    .line 263
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/Storage$Locations;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    return-void
.end method
