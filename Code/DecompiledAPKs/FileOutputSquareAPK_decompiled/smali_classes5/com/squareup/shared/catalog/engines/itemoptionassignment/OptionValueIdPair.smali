.class public Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
.super Ljava/lang/Object;
.source "OptionValueIdPair.java"


# instance fields
.field public final optionId:Ljava/lang/String;

.field public final optionValueId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 89
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 90
    invoke-virtual {v0, p0}, Lokio/Buffer;->write([B)Lokio/Buffer;

    .line 91
    invoke-static {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object p0

    return-object p0
.end method

.method static readFromSource(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    .line 102
    new-instance v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    invoke-direct {v1, v0, p0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 36
    instance-of v2, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    if-nez v2, :cond_1

    goto :goto_1

    .line 40
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iget-object v2, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object p1, p1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    .line 41
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getOptionId()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public getOptionValueId()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    return-object v0
.end method

.method getOrdinalWithAllItemOptionsByIds(Ljava/util/Map;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v3, "allItemOptionsByIds must be nonnull."

    .line 64
    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 66
    iget-object v2, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    .line 67
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object p1

    const/4 v2, 0x0

    .line 69
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 70
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 71
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getOrdinal()Ljava/lang/Integer;

    move-result-object v2

    :cond_2
    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    const-string p1, "allItemOptionsByIds must contain a match for the optionValueIdPair."

    .line 76
    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 79
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method getValueNameWithAllItemOptionsByIds(Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "allItemOptionsByIds must be nonnull."

    .line 49
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkArgument(ZLjava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    .line 52
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    .line 54
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 55
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method

.method public toByteArray()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 83
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 84
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->writeToSink(Lokio/BufferedSink;)V

    .line 85
    invoke-virtual {v0}, Lokio/Buffer;->readByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method writeToSink(Lokio/BufferedSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/BufferUtils;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V

    return-void
.end method
