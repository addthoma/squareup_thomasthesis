.class public final Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemModifierOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemModifierOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static create(Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;
    .locals 3

    .line 26
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    new-instance v2, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    .line 29
    invoke-virtual {v2, p0}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p0

    .line 30
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p0

    iget-object p1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p0

    .line 32
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p0

    .line 33
    invoke-virtual {p0, p3}, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object p0

    .line 28
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v1
.end method


# virtual methods
.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getModifierListId()Ljava/lang/String;
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    .line 73
    iget-object v1, v0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 74
    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 68
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 50
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 82
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getModifierListId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getOrdinal()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isOnByDefault()Z
    .locals 2

    .line 54
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
