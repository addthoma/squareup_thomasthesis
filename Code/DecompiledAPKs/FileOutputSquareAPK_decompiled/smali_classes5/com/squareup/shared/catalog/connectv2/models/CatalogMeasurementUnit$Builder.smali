.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
.super Ljava/lang/Object;
.source "CatalogMeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 52
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 53
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)V
    .locals 2

    .line 70
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 72
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 71
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)V
    .locals 2

    .line 78
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 80
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 79
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)V
    .locals 2

    .line 102
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 103
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 104
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 103
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)V
    .locals 2

    .line 86
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 88
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 87
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)V
    .locals 2

    .line 94
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 95
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 96
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 95
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 111
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 58
    invoke-direct {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>()V

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;-><init>()V

    .line 61
    invoke-virtual {v2, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    .line 60
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 59
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    .line 155
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v1
.end method

.method public setCustomUnitAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 140
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const-string v2, "When setting an abbreviation to a CatalogMeasurementUnit, the unit must be a custom unit"

    .line 141
    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 144
    iget-object v2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    .line 145
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    .line 145
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 144
    invoke-virtual {v2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-object p0
.end method

.method public setCustomUnitName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 126
    iget-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const-string v2, "When setting a name to a CatalogMeasurementUnit, the unit must be a custom unit"

    .line 127
    invoke-static {v1, v2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 130
    iget-object v2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object v1

    .line 132
    invoke-virtual {v1, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    .line 131
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 130
    invoke-virtual {v2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    return-object p0
.end method

.method public setPrecision(I)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->measurementUnit:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->precision(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    return-object p0
.end method
