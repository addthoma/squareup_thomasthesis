.class public final Lcom/squareup/shared/catalog/models/CatalogProductSet;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogProductSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ProductSet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getAllProducts()Z
    .locals 2

    .line 96
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ProductSet;->DEFAULT_ALL_PRODUCTS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getEffectiveMax()Ljava/math/BigDecimal;
    .locals 2

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 45
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEffectiveMin()Ljava/math/BigDecimal;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 35
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProducts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 118
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->hasProductsAll()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAll()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAny()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getProductsAll()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 110
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    return-object v0
.end method

.method public getProductsAny()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 106
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMin()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMin()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getQuantityExact()Ljava/math/BigDecimal;
    .locals 2

    .line 52
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isGreedy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    return-object v0

    .line 55
    :cond_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0
.end method

.method public getQuantityMax()Ljava/math/BigDecimal;
    .locals 2

    .line 63
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0
.end method

.method public getQuantityMin()Ljava/math/BigDecimal;
    .locals 2

    .line 59
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ProductSet;

    iget-object v1, v1, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasProductsAll()Z
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAll()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAll()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasProductsAny()Z
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAny()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAny()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAppliesToCustomAmount()Z
    .locals 2

    .line 101
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ProductSet;->DEFAULT_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isExact()Z
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMin()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMax()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGreedy()Z
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMax()Z
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMin()Z
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isRange()Z
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMin()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMax()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "%s{id=%s, name=%s}"

    .line 130
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
