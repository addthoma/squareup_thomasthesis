.class Lcom/squareup/shared/catalog/sync/CatalogSync$5;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$5;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            ">;"
        }
    .end annotation

    .line 222
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->locked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$5;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$100(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    move-result-object p1

    .line 226
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method
