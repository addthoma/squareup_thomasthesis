.class Lcom/squareup/shared/catalog/sync/GetMessage$2;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;->handleModifiedObjectSets(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

.field final synthetic val$cogsDeleted:Ljava/util/List;

.field final synthetic val$cogsUpdated:Ljava/util/List;

.field final synthetic val$connectV2Deleted:Ljava/util/List;

.field final synthetic val$connectV2Updated:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsDeleted:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsUpdated:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Deleted:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Updated:Ljava/util/List;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 271
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$3;->$SwitchMap$com$squareup$shared$catalog$sync$GetMessage$MessageStatus:[I

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_6

    const/4 p1, 0x4

    if-eq v0, p1, :cond_0

    .line 308
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There is no default state."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 306
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "onResponse() must not be called after complete."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 274
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    .line 275
    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$600(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 276
    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$700(Lcom/squareup/shared/catalog/sync/GetMessage;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    .line 279
    invoke-static {v3}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$700(Lcom/squareup/shared/catalog/sync/GetMessage;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v3}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$600(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 282
    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v3}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$300(Lcom/squareup/shared/catalog/sync/GetMessage;)J

    move-result-wide v3

    invoke-virtual {p1, v3, v4, v0, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->beginVersionSync(JLjava/util/List;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsDeleted:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsUpdated:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Deleted:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Updated:Ljava/util/List;

    const/4 v7, 0x1

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->applyVersionBatch(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 286
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$800(Lcom/squareup/shared/catalog/sync/GetMessage;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 291
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$300(Lcom/squareup/shared/catalog/sync/GetMessage;)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->endVersionSync(JZ)V

    .line 293
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$800(Lcom/squareup/shared/catalog/sync/GetMessage;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->IN_PROGRESS:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$202(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    goto :goto_1

    .line 295
    :cond_4
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->SKIP:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$202(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    goto :goto_1

    .line 299
    :cond_5
    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsDeleted:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$cogsUpdated:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Deleted:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->val$connectV2Updated:Ljava/util/List;

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->applyVersionBatch(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    .line 310
    :cond_6
    :goto_1
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public shouldUpdateLastKnownServerVersion()Z
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$2;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$800(Lcom/squareup/shared/catalog/sync/GetMessage;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
