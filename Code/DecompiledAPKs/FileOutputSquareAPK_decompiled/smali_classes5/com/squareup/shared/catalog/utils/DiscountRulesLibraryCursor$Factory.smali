.class public Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor$Factory;
.super Ljava/lang/Object;
.source "DiscountRulesLibraryCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fromDiscountsCursor(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;
    .locals 4

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 53
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    new-instance p1, Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;

    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p1, p2, p3}, Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;)V

    return-object p1

    .line 57
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v2, v3, :cond_1

    .line 61
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1, p3, p1, p5}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->forDiscountSynchronous(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/utils/DiscountBundle;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    new-instance p1, Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;

    invoke-direct {p1, p2, v0}, Lcom/squareup/shared/catalog/utils/DiscountRulesLibraryCursor;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;)V

    return-object p1

    .line 59
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "cursor must contain only entires of type == DISCOUNT"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
