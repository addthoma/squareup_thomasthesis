.class public final Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;
.super Ljava/lang/Object;
.source "CatalogItemImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final image:Lcom/squareup/api/items/ItemImage$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 33
    new-instance v0, Lcom/squareup/api/items/ItemImage$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemImage$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemImage$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemImage$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->image:Lcom/squareup/api/items/ItemImage$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemImage;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 38
    new-instance p1, Lcom/squareup/api/items/ItemImage$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemImage$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->image:Lcom/squareup/api/items/ItemImage$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemImage;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->image:Lcom/squareup/api/items/ItemImage$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemImage$Builder;->build()Lcom/squareup/api/items/ItemImage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 55
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemImage;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 48
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 49
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->image:Lcom/squareup/api/items/ItemImage$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemImage$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemImage$Builder;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->image:Lcom/squareup/api/items/ItemImage$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemImage$Builder;->url(Ljava/lang/String;)Lcom/squareup/api/items/ItemImage$Builder;

    return-object p0
.end method
