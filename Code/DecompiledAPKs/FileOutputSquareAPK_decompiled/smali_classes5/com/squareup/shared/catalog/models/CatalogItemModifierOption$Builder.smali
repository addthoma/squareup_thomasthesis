.class public final Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
.super Ljava/lang/Object;
.source "CatalogItemModifierOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 95
    new-instance v0, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 100
    new-instance p1, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;
    .locals 3

    .line 135
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v2, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 136
    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 105
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    return-object p0
.end method

.method public setModifierListId(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object p0
.end method

.method public setOnByDefault(Z)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->on_by_default(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object p0
.end method

.method public setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object p0
.end method

.method public setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption$Builder;->itemModifierOption:Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-object p0
.end method
