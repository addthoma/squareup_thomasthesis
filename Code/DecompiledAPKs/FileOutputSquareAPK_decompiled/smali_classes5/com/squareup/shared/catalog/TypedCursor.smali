.class public abstract Lcom/squareup/shared/catalog/TypedCursor;
.super Ljava/lang/Object;
.source "TypedCursor.java"

# interfaces
.implements Lcom/squareup/shared/sql/SQLCursor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/sql/SQLCursor;"
    }
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/shared/sql/SQLCursor;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/sql/SQLCursor;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-void
.end method

.method public abstract get()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getBlob(I)[B
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getBlob(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getCount()I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    return p1
.end method

.method public getLong(I)J
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPosition()I
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isClosed()Z
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result p1

    return p1
.end method

.method public varargs mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public moveToFirst()Z
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/shared/catalog/TypedCursor;->delegate:Lcom/squareup/shared/sql/SQLCursor;

    invoke-interface {v0, p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToPosition(I)Z

    move-result p1

    return p1
.end method

.method public toList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/TypedCursor;->getCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/TypedCursor;->getPosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 27
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/TypedCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/TypedCursor;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/TypedCursor;->close()V

    .line 31
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
