.class public Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;
.super Ljava/lang/Object;
.source "DiscountBundle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/DiscountBundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private fillLeafProductSets(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Set;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 119
    const-class v1, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 120
    invoke-interface {p1, v1, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v1

    .line 121
    invoke-interface {p2}, Ljava/util/Set;->clear()V

    .line 122
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 123
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    .line 127
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 128
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/sync/ObjectId;

    .line 129
    iget-object v6, v5, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v6, v6, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    sget-object v7, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    if-ne v6, v7, :cond_2

    const/4 v3, 0x1

    .line 130
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 131
    iget-object v5, v5, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {p2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 134
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 135
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Reference loop including product set "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    return-object v0
.end method


# virtual methods
.method public forDiscount(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/i18n/Localizer;",
            ")",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)V

    return-object v0
.end method

.method public forDiscountSynchronous(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/utils/DiscountBundle;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    .line 58
    const-class v4, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 59
    invoke-static/range {p1 .. p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v4

    .line 60
    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 63
    sget-object v5, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    sget-object v6, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    .line 65
    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 64
    invoke-interface {v2, v5, v1, v6}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectIdsForRelatedObjects(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 66
    new-instance v6, Ljava/util/HashSet;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/HashSet;-><init>(I)V

    .line 67
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/api/sync/ObjectId;

    .line 68
    iget-object v7, v7, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 71
    const-class v7, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-interface {v2, v7, v6}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v6

    .line 72
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    .line 71
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 73
    new-instance v8, Ljava/util/HashSet;

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidity()Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 74
    new-instance v10, Ljava/util/ArrayList;

    const-class v9, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 75
    invoke-interface {v2, v9, v8}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-direct {v10, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 77
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 78
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v9

    .line 79
    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    const-class v11, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 81
    invoke-static {v9}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v12

    invoke-interface {v2, v11, v12}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v11, v9

    check-cast v11, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 82
    invoke-direct {v0, v2, v8}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->fillLeafProductSets(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Set;)Ljava/util/List;

    move-result-object v14

    .line 84
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 86
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x0

    if-eqz v9, :cond_1

    .line 87
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v9

    .line 88
    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    const-class v13, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 90
    invoke-static {v9}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v15

    invoke-interface {v2, v13, v15}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    move-object v13, v9

    goto :goto_2

    :cond_1
    move-object v13, v12

    .line 92
    :goto_2
    invoke-direct {v0, v2, v8}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->fillLeafProductSets(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Set;)Ljava/util/List;

    move-result-object v15

    .line 94
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 96
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 97
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v9

    .line 98
    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    const-class v12, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    move-object/from16 v18, v6

    .line 100
    invoke-static {v9}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v2, v12, v6}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    goto :goto_3

    :cond_2
    move-object/from16 v18, v6

    move-object v6, v12

    .line 102
    :goto_3
    invoke-direct {v0, v2, v8}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->fillLeafProductSets(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Set;)Ljava/util/List;

    move-result-object v16

    .line 105
    new-instance v12, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    move-object v8, v12

    move-object v9, v7

    move-object v0, v12

    move-object v12, v13

    move-object v13, v6

    move-object/from16 v17, p2

    invoke-direct/range {v8 .. v17}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;-><init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/TimeZone;)V

    .line 108
    invoke-virtual {v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    .line 110
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    move-object/from16 v6, v18

    goto/16 :goto_1

    .line 112
    :cond_3
    new-instance v0, Lcom/squareup/shared/catalog/utils/DiscountBundle;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-direct {v0, v1, v5, v3}, Lcom/squareup/shared/catalog/utils/DiscountBundle;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Ljava/util/Map;Lcom/squareup/shared/i18n/Localizer;)V

    return-object v0

    .line 61
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no discount found for discountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final synthetic lambda$forDiscount$0$DiscountBundle$Factory(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/utils/DiscountBundle;
    .locals 0

    .line 52
    invoke-virtual {p0, p1, p2, p4, p3}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->forDiscountSynchronous(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/utils/DiscountBundle;

    move-result-object p1

    return-object p1
.end method
