.class Lcom/squareup/shared/catalog/CatalogLocal$3;
.super Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
.source "CatalogLocal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/CatalogLocal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
        "Lcom/squareup/shared/catalog/models/CatalogDiningOption;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;-><init>()V

    return-void
.end method


# virtual methods
.method getName(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)Ljava/lang/String;
    .locals 0

    .line 111
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method bridge synthetic getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;
    .locals 0

    .line 105
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$3;->getName(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getOrdinal(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)I
    .locals 0

    .line 107
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getOrdinal()I

    move-result p1

    return p1
.end method

.method bridge synthetic getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I
    .locals 0

    .line 105
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$3;->getOrdinal(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)I

    move-result p1

    return p1
.end method
