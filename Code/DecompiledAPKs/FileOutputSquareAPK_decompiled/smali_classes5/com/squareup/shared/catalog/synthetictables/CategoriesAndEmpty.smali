.class public Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;
.super Ljava/lang/Object;
.source "CategoriesAndEmpty.java"


# instance fields
.field public final categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field public final isLibraryEmpty:Z


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Z)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 19
    iput-boolean p2, p0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->isLibraryEmpty:Z

    return-void
.end method
