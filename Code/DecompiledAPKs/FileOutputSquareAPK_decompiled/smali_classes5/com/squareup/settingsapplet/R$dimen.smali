.class public final Lcom/squareup/settingsapplet/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settingsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final passcode_settings_create_passcode_digit_margin:I = 0x7f070404

.field public static final passcode_settings_create_passcode_digit_text_size:I = 0x7f070405

.field public static final passcode_settings_create_passcode_digit_width:I = 0x7f070406

.field public static final passcode_settings_create_passcode_header_margin:I = 0x7f070407

.field public static final passcode_settings_gutter_horizontal:I = 0x7f070408

.field public static final passcode_settings_gutter_vertical:I = 0x7f070409

.field public static final payment_settings_preview_margin:I = 0x7f070424

.field public static final payment_settings_preview_padding_horizontal:I = 0x7f070425

.field public static final payment_settings_preview_padding_vertical:I = 0x7f070426

.field public static final payment_type_settings_header_top_padding:I = 0x7f070427

.field public static final payment_type_settings_margin:I = 0x7f070428

.field public static final tipping_screen_collect_tips_extra_bottom:I = 0x7f070534

.field public static final tipping_screen_headers_space_after:I = 0x7f070535

.field public static final tipping_screen_heading_extra_top:I = 0x7f070536

.field public static final tipping_screen_section_padding_horizontal:I = 0x7f070537

.field public static final tipping_screen_section_padding_vertical:I = 0x7f070538


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
