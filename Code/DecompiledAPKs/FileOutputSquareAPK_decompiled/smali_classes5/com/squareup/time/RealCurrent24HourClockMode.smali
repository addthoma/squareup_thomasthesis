.class public final Lcom/squareup/time/RealCurrent24HourClockMode;
.super Ljava/lang/Object;
.source "RealCurrent24HourClockMode.kt"

# interfaces
.implements Lcom/squareup/time/Current24HourClockMode;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0007H\u0016R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/time/RealCurrent24HourClockMode;",
        "Lcom/squareup/time/Current24HourClockMode;",
        "timeInfoChangedMonitor",
        "Lcom/squareup/time/TimeInfoChangedMonitor;",
        "(Lcom/squareup/time/TimeInfoChangedMonitor;)V",
        "is24HourClock",
        "Lio/reactivex/Observable;",
        "",
        "()Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/time/TimeInfoChangedMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "timeInfoChangedMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/RealCurrent24HourClockMode;->timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;

    return-void
.end method


# virtual methods
.method public is24HourClock()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/time/RealCurrent24HourClockMode;->timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-virtual {v0}, Lcom/squareup/time/TimeInfoChangedMonitor;->is24HourClock()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public is24HourClock()Z
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/time/RealCurrent24HourClockMode;->timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-virtual {v0}, Lcom/squareup/time/TimeInfoChangedMonitor;->is24HourClock()Z

    move-result v0

    return v0
.end method
