.class public Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;
.super Ljava/lang/Object;
.source "CashManagementSettingsPresenterHelper.java"


# instance fields
.field private final accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 34
    iput-object p2, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private static invalidEmail(Ljava/lang/String;)Z
    .locals 0

    .line 104
    invoke-static {p0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private viewHasInvalidEmail(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Z
    .locals 1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->isCashManagementEnabledChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->isEmailReportEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getEmailRecipient()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->invalidEmail(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public enableAutoEmailToggled(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)V
    .locals 0

    .line 77
    invoke-virtual {p1, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setEmailRecipientContainerVisible(Z)V

    if-eqz p2, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->scrollToBottom()V

    goto :goto_0

    .line 81
    :cond_0
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object v0
.end method

.method public handleBackPressed(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)Z
    .locals 0

    if-eqz p2, :cond_0

    .line 61
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public handleInvalidEmail(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Lflow/Flow;)Z
    .locals 2

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->viewHasInvalidEmail(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 51
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget v0, Lcom/squareup/common/strings/R$string;->invalid_email:I

    sget v1, Lcom/squareup/common/strings/R$string;->invalid_email_message:I

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 53
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v0, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onLoad(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->getDefaultStartingCash()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setDefaultStartingCash(Lcom/squareup/protos/common/Money;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isEmailReportEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setEmailReportEnabled(Z)V

    .line 40
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isPrintReportEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setPrintReportEnabled(Z)V

    .line 41
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->getEmailRecipient()Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_0
    invoke-virtual {p1, v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setEmailRecipient(Ljava/lang/String;)V

    return-void
.end method

.method public saveSettings(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->isCashManagementEnabledChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setCashManagementEnabled(Z)V

    .line 68
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getDefaultStartingCash()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setDefaultStartingCash(Lcom/squareup/protos/common/Money;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->isPrintReportEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setPrintReportEnabled(Z)V

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->viewHasInvalidEmail(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->isEmailReportEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setEmailReportEnabled(Z)V

    .line 72
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->getEmailRecipient()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setEmailRecipient(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
