.class public Lcom/squareup/settings/NetworkProxyLocalSetting;
.super Lcom/squareup/settings/StringLocalSetting;
.source "NetworkProxyLocalSetting.java"


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getProxy()Ljava/net/Proxy;
    .locals 6

    .line 17
    iget-object v0, p0, Lcom/squareup/settings/NetworkProxyLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/NetworkProxyLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 20
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/settings/NetworkProxyLocalSetting;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ":"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 21
    aget-object v3, v2, v0

    .line 22
    array-length v4, v2

    if-le v4, v1, :cond_1

    aget-object v2, v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    :cond_1
    const/16 v2, 0x50

    .line 24
    :goto_0
    new-instance v4, Ljava/net/Proxy;

    sget-object v5, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-static {v3, v2}, Ljava/net/InetSocketAddress;->createUnresolved(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    :catch_0
    move-exception v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/settings/NetworkProxyLocalSetting;->get()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v0

    const-string v0, "Invalid proxy: %s"

    invoke-static {v2, v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/settings/NetworkProxyLocalSetting;->remove()V

    .line 29
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    return-object v0
.end method
