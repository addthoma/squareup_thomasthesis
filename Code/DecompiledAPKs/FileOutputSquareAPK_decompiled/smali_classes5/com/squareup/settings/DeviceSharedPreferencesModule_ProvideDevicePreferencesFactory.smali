.class public final Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;
.super Ljava/lang/Object;
.source "DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/content/SharedPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field private final appProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;->appProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDevicePreferences(Landroid/app/Application;)Landroid/content/SharedPreferences;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/settings/DeviceSharedPreferencesModule;->provideDevicePreferences(Landroid/app/Application;)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/SharedPreferences;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/content/SharedPreferences;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;->appProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;->provideDevicePreferences(Landroid/app/Application;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
