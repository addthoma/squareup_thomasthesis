.class public final Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
.super Ljava/lang/Object;
.source "SettingsCardPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/ui/SettingsCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CoreParameters"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;",
        "",
        "flow",
        "Lflow/Flow;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lflow/Flow;Lcom/squareup/util/Device;)V",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "getFlow",
        "()Lflow/Flow;",
        "settings-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Device;)V
    .locals 1

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method public final getFlow()Lflow/Flow;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->flow:Lflow/Flow;

    return-object v0
.end method
