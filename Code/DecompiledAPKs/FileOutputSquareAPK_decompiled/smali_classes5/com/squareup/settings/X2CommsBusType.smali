.class public final enum Lcom/squareup/settings/X2CommsBusType;
.super Ljava/lang/Enum;
.source "X2CommsBusType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/X2CommsBusType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/X2CommsBusType;

.field public static final enum ANDROID_EMULATOR:Lcom/squareup/settings/X2CommsBusType;

.field public static final enum GENYMOTION:Lcom/squareup/settings/X2CommsBusType;

.field public static final enum IP:Lcom/squareup/settings/X2CommsBusType;

.field public static final enum SINGLE_DEVICE:Lcom/squareup/settings/X2CommsBusType;

.field public static final enum USB:Lcom/squareup/settings/X2CommsBusType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 6
    new-instance v0, Lcom/squareup/settings/X2CommsBusType;

    const/4 v1, 0x0

    const-string v2, "USB"

    invoke-direct {v0, v2, v1}, Lcom/squareup/settings/X2CommsBusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->USB:Lcom/squareup/settings/X2CommsBusType;

    .line 8
    new-instance v0, Lcom/squareup/settings/X2CommsBusType;

    const/4 v2, 0x1

    const-string v3, "IP"

    invoke-direct {v0, v3, v2}, Lcom/squareup/settings/X2CommsBusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->IP:Lcom/squareup/settings/X2CommsBusType;

    .line 13
    new-instance v0, Lcom/squareup/settings/X2CommsBusType;

    const/4 v3, 0x2

    const-string v4, "GENYMOTION"

    invoke-direct {v0, v4, v3}, Lcom/squareup/settings/X2CommsBusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->GENYMOTION:Lcom/squareup/settings/X2CommsBusType;

    .line 18
    new-instance v0, Lcom/squareup/settings/X2CommsBusType;

    const/4 v4, 0x3

    const-string v5, "ANDROID_EMULATOR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/settings/X2CommsBusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->ANDROID_EMULATOR:Lcom/squareup/settings/X2CommsBusType;

    .line 20
    new-instance v0, Lcom/squareup/settings/X2CommsBusType;

    const/4 v5, 0x4

    const-string v6, "SINGLE_DEVICE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/settings/X2CommsBusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->SINGLE_DEVICE:Lcom/squareup/settings/X2CommsBusType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/settings/X2CommsBusType;

    .line 4
    sget-object v6, Lcom/squareup/settings/X2CommsBusType;->USB:Lcom/squareup/settings/X2CommsBusType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/settings/X2CommsBusType;->IP:Lcom/squareup/settings/X2CommsBusType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/X2CommsBusType;->GENYMOTION:Lcom/squareup/settings/X2CommsBusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/settings/X2CommsBusType;->ANDROID_EMULATOR:Lcom/squareup/settings/X2CommsBusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/settings/X2CommsBusType;->SINGLE_DEVICE:Lcom/squareup/settings/X2CommsBusType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/settings/X2CommsBusType;->$VALUES:[Lcom/squareup/settings/X2CommsBusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static defaultType()Lcom/squareup/settings/X2CommsBusType;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/settings/X2CommsBusType;->USB:Lcom/squareup/settings/X2CommsBusType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/X2CommsBusType;
    .locals 1

    .line 4
    const-class v0, Lcom/squareup/settings/X2CommsBusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/X2CommsBusType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/X2CommsBusType;
    .locals 1

    .line 4
    sget-object v0, Lcom/squareup/settings/X2CommsBusType;->$VALUES:[Lcom/squareup/settings/X2CommsBusType;

    invoke-virtual {v0}, [Lcom/squareup/settings/X2CommsBusType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/X2CommsBusType;

    return-object v0
.end method
