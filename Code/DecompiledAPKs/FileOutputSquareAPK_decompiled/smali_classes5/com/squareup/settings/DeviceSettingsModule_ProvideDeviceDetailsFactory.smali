.class public final Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;
.super Ljava/lang/Object;
.source "DeviceSettingsModule_ProvideDeviceDetailsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Ljava/util/LinkedHashMap<",
        "Ljava/lang/String;",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/settings/DeviceSettingsModule;

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/DeviceSettingsModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/DeviceSettingsModule;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->module:Lcom/squareup/settings/DeviceSettingsModule;

    .line 30
    iput-object p2, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/settings/DeviceSettingsModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/DeviceSettingsModule;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;-><init>(Lcom/squareup/settings/DeviceSettingsModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDeviceDetails(Lcom/squareup/settings/DeviceSettingsModule;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/DeviceSettingsModule;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;"
        }
    .end annotation

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/squareup/settings/DeviceSettingsModule;->provideDeviceDetails(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->module:Lcom/squareup/settings/DeviceSettingsModule;

    iget-object v1, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/gson/Gson;

    invoke-static {v0, v1, v2}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->provideDeviceDetails(Lcom/squareup/settings/DeviceSettingsModule;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
