.class public final Lcom/squareup/settings/server/RstMenuBehaviorSettings;
.super Ljava/lang/Object;
.source "RstMenuBehaviorSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\tB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/settings/server/RstMenuBehaviorSettings;",
        "",
        "preferences",
        "Lcom/squareup/server/account/protos/Preferences;",
        "(Lcom/squareup/server/account/protos/Preferences;)V",
        "behaviorAfterAddingItem",
        "Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;",
        "getBehaviorAfterAddingItem",
        "()Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;",
        "BehaviorAfterAddingItem",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final preferences:Lcom/squareup/server/account/protos/Preferences;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/Preferences;)V
    .locals 1

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/server/RstMenuBehaviorSettings;->preferences:Lcom/squareup/server/account/protos/Preferences;

    return-void
.end method


# virtual methods
.method public final getBehaviorAfterAddingItem()Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;
    .locals 3

    .line 15
    iget-object v0, p0, Lcom/squareup/settings/server/RstMenuBehaviorSettings;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x3e766509

    if-eq v1, v2, :cond_2

    const v2, -0x3a7036a1

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "REMAIN_ON_CURRENT_SCREEN"

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;->REMAIN_ON_CURRENT_SCREEN:Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;

    goto :goto_1

    :cond_2
    const-string v1, "RETURN_TO_HOME_SCREEN"

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;->RETURN_TO_HOME_SCREEN:Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;

    goto :goto_1

    .line 18
    :cond_3
    :goto_0
    sget-object v0, Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;->RETURN_TO_HOME_SCREEN:Lcom/squareup/settings/server/RstMenuBehaviorSettings$BehaviorAfterAddingItem;

    :goto_1
    return-object v0
.end method
