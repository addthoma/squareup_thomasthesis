.class public final Lcom/squareup/settings/server/TipSettings$Companion;
.super Ljava/lang/Object;
.source "TipSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/TipSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipSettings.kt\ncom/squareup/settings/server/TipSettings$Companion\n*L\n1#1,171:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00060\t2\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\tH\u0002J\"\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00060\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/settings/server/TipSettings$Companion;",
        "",
        "()V",
        "DEFAULT_MANUAL_TIP_ENTRY_LARGEST_MAX_MONEY",
        "Lcom/squareup/protos/common/Money;",
        "DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE",
        "Lcom/squareup/util/Percentage;",
        "DEFAULT_MANUAL_TIP_ENTRY_SMALLEST_MAX_MONEY",
        "DEFAULT_PERCENTAGES",
        "",
        "DEFAULT_SMART_TIPPING_OVER_THRESHOLD_OPTIONS",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "kotlin.jvm.PlatformType",
        "DEFAULT_SMART_TIPPING_THRESHOLD_MONEY",
        "DEFAULT_SMART_TIPPING_UNDER_THRESHOLD_OPTIONS",
        "MAX_CUSTOM_PERCENTAGES",
        "",
        "cleanup",
        "tips",
        "from",
        "Lcom/squareup/settings/server/TipSettings;",
        "prefs",
        "Lcom/squareup/server/account/protos/Preferences;",
        "tipping",
        "Lcom/squareup/server/account/protos/Tipping;",
        "preAuthTippingRequired",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/settings/server/TipSettings$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$cleanup(Lcom/squareup/settings/server/TipSettings$Companion;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 109
    invoke-direct {p0, p1}, Lcom/squareup/settings/server/TipSettings$Companion;->cleanup(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final cleanup(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 134
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 135
    :cond_0
    sget-object p1, Lcom/squareup/settings/server/TipSettings;->DEFAULT_PERCENTAGES:Ljava/util/List;

    :cond_1
    return-object p1
.end method


# virtual methods
.method public final from(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/Tipping;Z)Lcom/squareup/settings/server/TipSettings;
    .locals 12
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "prefs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    .line 152
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_1
    const-string v2, "prefs.tipping_enabled?.r\u2026 ?: false\n      } ?: true"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 154
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string/jumbo v2, "this"

    .line 155
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_2

    .line 156
    :cond_2
    sget-object v0, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->POST_TAX_TIP_CALCULATION:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    :goto_2
    move-object v8, v0

    .line 158
    new-instance v0, Lcom/squareup/settings/server/TipSettings;

    .line 159
    iget-object v2, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_3
    move-object v2, v1

    :goto_3
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 160
    iget-object v2, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    goto :goto_4

    :cond_4
    move-object v2, v1

    :goto_4
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 162
    iget-object v2, p1, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    move-object v1, v2

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 164
    invoke-static {p1}, Lcom/squareup/settings/server/TipHelper;->getCustomTippingPercentages(Lcom/squareup/server/account/protos/Preferences;)Ljava/util/List;

    move-result-object v9

    move-object v3, v0

    move-object v10, p2

    move v11, p3

    .line 158
    invoke-direct/range {v3 .. v11}, Lcom/squareup/settings/server/TipSettings;-><init>(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;Lcom/squareup/server/account/protos/Tipping;Z)V

    return-object v0
.end method
