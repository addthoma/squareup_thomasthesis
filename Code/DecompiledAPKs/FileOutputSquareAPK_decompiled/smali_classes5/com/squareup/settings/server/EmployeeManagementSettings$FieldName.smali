.class public final enum Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;
.super Ljava/lang/Enum;
.source "EmployeeManagementSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/EmployeeManagementSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FieldName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

.field public static final enum GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

.field public static final enum PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

.field public static final enum TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

.field public static final enum TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 305
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    const/4 v1, 0x0

    const-string v2, "PASSCODE_EMPLOYEE_MANAGEMENT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    .line 306
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    const/4 v2, 0x1

    const-string v3, "TIME_TRACKING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    .line 307
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    const/4 v3, 0x2

    const-string v4, "GUEST_MODE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    .line 308
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    const/4 v4, 0x3

    const-string v5, "TRANSACTION_LOCK"

    invoke-direct {v0, v5, v4}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    .line 304
    sget-object v5, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;
    .locals 1

    .line 304
    const-class v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;
    .locals 1

    .line 304
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    return-object v0
.end method
