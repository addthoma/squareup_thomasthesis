.class public Lcom/squareup/settings/server/TutorialSettings;
.super Ljava/lang/Object;
.source "TutorialSettings.java"


# static fields
.field private static final EMPTY_FEES:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final response:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/TutorialSettings;->EMPTY_FEES:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/settings/server/TutorialSettings;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 16
    iput-object p2, p0, Lcom/squareup/settings/server/TutorialSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getFeeTutorialInfo()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/settings/server/TutorialSettings;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/TutorialSettings;->EMPTY_FEES:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    :goto_0
    return-object v0
.end method

.method public shouldAutoStartFirstPaymentTutorial()Z
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/settings/server/TutorialSettings;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/settings/server/TutorialSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SUPPRESS_AUTO_FPT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/TutorialSettings;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
