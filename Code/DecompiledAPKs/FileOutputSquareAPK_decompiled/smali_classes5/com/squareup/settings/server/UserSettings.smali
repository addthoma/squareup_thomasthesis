.class public Lcom/squareup/settings/server/UserSettings;
.super Ljava/lang/Object;
.source "UserSettings.java"


# static fields
.field private static final EMPTY_MERCHANT_PROFILE:Lcom/squareup/server/account/protos/User$MerchantProfile;


# instance fields
.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;-><init>()V

    .line 19
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->populateDefaults()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/UserSettings;->EMPTY_MERCHANT_PROFILE:Lcom/squareup/server/account/protos/User$MerchantProfile;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/settings/server/UserSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 27
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object p1

    if-nez p1, :cond_0

    .line 28
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Empty user in account status."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 1

    .line 206
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    :goto_0
    return-object v0
.end method

.method private getUser()Lcom/squareup/server/account/protos/User;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/settings/server/UserSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    return-object v0
.end method


# virtual methods
.method public canUseMobileInvoices()Z
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/settings/server/UserSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getAddressLine1()Ljava/lang/String;
    .locals 1

    .line 80
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getAddressLine2()Ljava/lang/String;
    .locals 1

    .line 85
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getAddressLine3()Ljava/lang/String;
    .locals 1

    .line 91
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getBusinessAbn()Ljava/lang/String;
    .locals 1

    .line 172
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getBusinessName()Ljava/lang/String;
    .locals 3

    .line 115
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 116
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    .line 117
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    return-object v0

    .line 120
    :cond_1
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 121
    :cond_2
    iget-object v1, v0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    :goto_1
    return-object v1
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 138
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    iget-object v0, v0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public getCountryCodeOrNull()Lcom/squareup/CountryCode;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 134
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/account/UserUtils;->getCountryCodeOrNull(Lcom/squareup/server/account/protos/User;)Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public getCuratedImageUrl()Ljava/lang/String;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getMerchantProfile()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 146
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/account/UserUtils;->getCurrency(Lcom/squareup/server/account/protos/User;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method public getCustomReceiptText()Ljava/lang/String;
    .locals 1

    .line 161
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getMerchantProfile()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 38
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 50
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getLocale()Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 1

    .line 127
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    :goto_0
    return-object v0
.end method

.method public getMcc()I
    .locals 1

    .line 192
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMerchantKey()Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 1

    .line 196
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    return-object v0
.end method

.method public getMerchantProfile()Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 2

    .line 224
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v1, v0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/settings/server/UserSettings;->EMPTY_MERCHANT_PROFILE:Lcom/squareup/server/account/protos/User$MerchantProfile;

    :goto_1
    return-object v0
.end method

.method public getMerchantToken()Ljava/lang/String;
    .locals 1

    .line 231
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 232
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 44
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .line 62
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getPrintedReceiptImageUrl()Ljava/lang/String;
    .locals 1

    .line 156
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getMerchantProfile()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileImageUrl()Ljava/lang/String;
    .locals 1

    .line 166
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getReceiptAddressWithUserCountryCode()Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 2

    .line 212
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 213
    iget-object v1, v0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-nez v1, :cond_0

    goto :goto_0

    .line 216
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    .line 217
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->newBuilder()Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    .line 218
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    iget-object v1, v1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->build()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .line 103
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddress()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getSubunitName()Ljava/lang/String;
    .locals 1

    .line 74
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .line 183
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 184
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getTwitter()Ljava/lang/String;
    .locals 1

    .line 56
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 57
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getWebsite()Ljava/lang/String;
    .locals 1

    .line 68
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public isImpersonating()Z
    .locals 1

    .line 200
    invoke-direct {p0}, Lcom/squareup/settings/server/UserSettings;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
