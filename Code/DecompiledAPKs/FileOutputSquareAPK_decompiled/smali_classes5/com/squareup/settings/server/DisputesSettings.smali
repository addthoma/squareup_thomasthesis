.class public Lcom/squareup/settings/server/DisputesSettings;
.super Ljava/lang/Object;
.source "DisputesSettings.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/settings/server/DisputesSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 13
    iput-object p2, p0, Lcom/squareup/settings/server/DisputesSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public canChallengeInApp()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/server/DisputesSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPUTES_CHALLENGES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canSeeDisputes()Z
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/settings/server/DisputesSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISPUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public hasActiveDisputes()Z
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/settings/server/DisputesSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/settings/server/DisputesSettings;->canSeeDisputes()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/DisputesSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Disputes;->has_active_disputes:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
