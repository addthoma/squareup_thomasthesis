.class public final Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;
.super Ljava/lang/Object;
.source "RealAccountStatusSettingsApiUrl.kt"

# interfaces
.implements Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\n\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;",
        "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;",
        "statusProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "(Ljavax/inject/Provider;)V",
        "getApiUrl",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;


# instance fields
.field private final statusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;->Companion:Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "statusProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;->statusProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public getApiUrl()Ljava/lang/String;
    .locals 2

    .line 18
    sget-object v0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;->Companion:Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;

    iget-object v1, p0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;->statusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl$Companion;->getBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
