.class public final Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;
.super Ljava/lang/Object;
.source "AccountStatusSettingsApiUrl.kt"

# interfaces
.implements Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoAccountStatusSettingsApiUrl"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;",
        "Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;",
        "()V",
        "getApiUrl",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;

    invoke-direct {v0}, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;-><init>()V

    sput-object v0, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;->INSTANCE:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl$NoAccountStatusSettingsApiUrl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApiUrl()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
