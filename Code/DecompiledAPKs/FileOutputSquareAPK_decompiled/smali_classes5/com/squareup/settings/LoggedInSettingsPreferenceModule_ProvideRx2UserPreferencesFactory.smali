.class public final Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field private final sharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRx2UserPreferences(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule;->provideRx2UserPreferences(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;->provideRx2UserPreferences(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsPreferenceModule_ProvideRx2UserPreferencesFactory;->get()Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v0

    return-object v0
.end method
