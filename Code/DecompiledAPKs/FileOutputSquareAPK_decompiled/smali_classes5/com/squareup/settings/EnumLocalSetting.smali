.class public Lcom/squareup/settings/EnumLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "EnumLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Enum<",
        "TE;>;>",
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final enumType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TE;>;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 12
    iput-object p3, p0, Lcom/squareup/settings/EnumLocalSetting;->enumType:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method protected doGet()Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/settings/EnumLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/EnumLocalSetting;->key:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v2

    .line 22
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/squareup/settings/EnumLocalSetting;->enumType:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v2
.end method

.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/settings/EnumLocalSetting;->doGet()Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Enum;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/settings/EnumLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/EnumLocalSetting;->key:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/EnumLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 6
    check-cast p1, Ljava/lang/Enum;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/EnumLocalSetting;->set(Ljava/lang/Enum;)V

    return-void
.end method
