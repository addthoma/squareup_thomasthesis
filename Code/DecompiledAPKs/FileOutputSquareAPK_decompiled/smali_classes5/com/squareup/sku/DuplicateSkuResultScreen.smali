.class public final Lcom/squareup/sku/DuplicateSkuResultScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "DuplicateSkuResultScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/sku/DuplicateSkuResultScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sku/DuplicateSkuResultScreen$ParentComponent;,
        Lcom/squareup/sku/DuplicateSkuResultScreen$Component;,
        Lcom/squareup/sku/DuplicateSkuResultScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDuplicateSkuResultScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DuplicateSkuResultScreen.kt\ncom/squareup/sku/DuplicateSkuResultScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,45:1\n52#2:46\n*E\n*S KotlinDebug\n*F\n+ 1 DuplicateSkuResultScreen.kt\ncom/squareup/sku/DuplicateSkuResultScreen\n*L\n26#1:46\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u000c2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0003\u000c\r\u000eB\u0005\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u000bH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/sku/DuplicateSkuResultScreen;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "Component",
        "ParentComponent",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sku/DuplicateSkuResultScreen$Companion;

.field public static final INSTANCE:Lcom/squareup/sku/DuplicateSkuResultScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sku/DuplicateSkuResultScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sku/DuplicateSkuResultScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sku/DuplicateSkuResultScreen;->Companion:Lcom/squareup/sku/DuplicateSkuResultScreen$Companion;

    .line 42
    new-instance v0, Lcom/squareup/sku/DuplicateSkuResultScreen;

    invoke-direct {v0}, Lcom/squareup/sku/DuplicateSkuResultScreen;-><init>()V

    sput-object v0, Lcom/squareup/sku/DuplicateSkuResultScreen;->INSTANCE:Lcom/squareup/sku/DuplicateSkuResultScreen;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 19
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    const-class v0, Lcom/squareup/sku/DuplicateSkuResultScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 26
    check-cast p1, Lcom/squareup/sku/DuplicateSkuResultScreen$Component;

    .line 27
    invoke-interface {p1}, Lcom/squareup/sku/DuplicateSkuResultScreen$Component;->duplicateSkuResultCoordinator()Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 39
    sget v0, Lcom/squareup/sku/R$layout;->duplicate_sku_result:I

    return v0
.end method
