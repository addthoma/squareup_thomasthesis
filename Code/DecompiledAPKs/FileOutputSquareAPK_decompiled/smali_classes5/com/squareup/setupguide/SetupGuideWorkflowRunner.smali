.class public interface abstract Lcom/squareup/setupguide/SetupGuideWorkflowRunner;
.super Ljava/lang/Object;
.source "SetupGuideWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupGuideWorkflowRunner$ParentComponent;,
        Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;,
        Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;,
        Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/setupguide/SetupGuideResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u0000 \u00062\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0005\u0006\u0007\u0008J\u0008\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/setupguide/SetupGuideResult;",
        "startWorkflow",
        "",
        "BootstrapScreen",
        "Companion",
        "ParentComponent",
        "Scope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner;->Companion:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startWorkflow()V
.end method
