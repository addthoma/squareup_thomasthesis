.class final Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;
.super Ljava/lang/Object;
.source "CollapsingHeaderView.kt"

# interfaces
.implements Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/widgets/CollapsingHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnimationOffsetListener"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCollapsingHeaderView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CollapsingHeaderView.kt\ncom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener\n*L\n1#1,219:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0003H\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;",
        "Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;",
        "totalScrollRange",
        "",
        "(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;I)V",
        "getTotalScrollRange",
        "()I",
        "setTotalScrollRange",
        "(I)V",
        "onOffsetChanged",
        "",
        "appBarLayout",
        "Lcom/google/android/material/appbar/AppBarLayout;",
        "verticalOffset",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/setupguide/widgets/CollapsingHeaderView;

.field private totalScrollRange:I


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 197
    iput-object p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->this$0:Lcom/squareup/setupguide/widgets/CollapsingHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->totalScrollRange:I

    return-void
.end method


# virtual methods
.method public final getTotalScrollRange()I
    .locals 1

    .line 198
    iget v0, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->totalScrollRange:I

    return v0
.end method

.method public onOffsetChanged(Lcom/google/android/material/appbar/AppBarLayout;I)V
    .locals 1

    const-string v0, "appBarLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iget p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->totalScrollRange:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    int-to-float p1, p1

    iget p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->totalScrollRange:I

    int-to-float p2, p2

    div-float/2addr p1, p2

    .line 208
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->this$0:Lcom/squareup/setupguide/widgets/CollapsingHeaderView;

    invoke-static {p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->access$getHeaderContainer$p(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;)Landroid/widget/LinearLayout;

    move-result-object p2

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 209
    iget-object p2, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->this$0:Lcom/squareup/setupguide/widgets/CollapsingHeaderView;

    invoke-static {p2}, Lcom/squareup/setupguide/widgets/CollapsingHeaderView;->access$getActionBarTitle$p(Lcom/squareup/setupguide/widgets/CollapsingHeaderView;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setAlpha(F)V

    return-void
.end method

.method public final setTotalScrollRange(I)V
    .locals 0

    .line 198
    iput p1, p0, Lcom/squareup/setupguide/widgets/CollapsingHeaderView$AnimationOffsetListener;->totalScrollRange:I

    return-void
.end method
