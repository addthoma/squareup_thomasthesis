.class public final Lcom/squareup/splitticket/RealSplitState;
.super Ljava/lang/Object;
.source "RealSplitState.kt"

# interfaces
.implements Lcom/squareup/splitticket/SplitState;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSplitState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSplitState.kt\ncom/squareup/splitticket/RealSplitState\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,266:1\n1265#2,9:267\n704#2:276\n777#2,2:277\n1274#2,3:279\n1099#2,2:282\n1127#2,4:284\n1360#2:288\n1429#2,3:289\n704#2:292\n777#2,2:293\n1642#2,2:295\n1642#2,2:297\n1360#2:299\n1429#2,3:300\n704#2:303\n777#2,2:304\n1360#2:306\n1429#2,2:307\n1431#2:312\n1360#2:313\n1429#2,3:314\n1360#2:317\n1429#2,3:318\n1360#2:321\n1429#2,3:322\n704#2:325\n777#2,2:326\n132#3,3:309\n*E\n*S KotlinDebug\n*F\n+ 1 RealSplitState.kt\ncom/squareup/splitticket/RealSplitState\n*L\n167#1,9:267\n167#1:276\n167#1,2:277\n167#1,3:279\n171#1,2:282\n171#1,4:284\n187#1:288\n187#1,3:289\n189#1:292\n189#1,2:293\n190#1,2:295\n199#1,2:297\n212#1:299\n212#1,3:300\n214#1:303\n214#1,2:304\n215#1:306\n215#1,2:307\n215#1:312\n241#1:313\n241#1,3:314\n251#1:317\n251#1,3:318\n252#1:321\n252#1,3:322\n53#1:325\n53#1,2:326\n215#1,3:309\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010K\u001a\u00020\u001dH\u0016J\u0008\u0010L\u001a\u00020MH\u0002J\u000e\u0010N\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0013H\u0002J\r\u0010O\u001a\u00020PH\u0000\u00a2\u0006\u0002\u0008QJ\u0008\u0010R\u001a\u00020SH\u0016J\r\u0010T\u001a\u00020\u001dH\u0000\u00a2\u0006\u0002\u0008UJ\r\u0010V\u001a\u00020\u001dH\u0000\u00a2\u0006\u0002\u0008WJ\u0008\u0010X\u001a\u00020\u001dH\u0016J\u0008\u0010Y\u001a\u00020\u001dH\u0016J\u0008\u0010Z\u001a\u00020\u001dH\u0016J\u0010\u0010[\u001a\u00020\u001d2\u0006\u0010\\\u001a\u00020\u0005H\u0016J\u0010\u0010]\u001a\u00020\u001d2\u0006\u0010\\\u001a\u00020\u0005H\u0016J\u000e\u0010^\u001a\u0008\u0012\u0004\u0012\u00020$0\u0013H\u0002J\u000e\u0010_\u001a\u0008\u0012\u0004\u0012\u00020M0`H\u0016J\u0008\u0010a\u001a\u00020\u001dH\u0016J\u0008\u0010b\u001a\u00020\u001dH\u0016J\u0010\u0010c\u001a\u00020\u001d2\u0006\u0010d\u001a\u00020\u001dH\u0016J\u0008\u0010e\u001a\u00020\u001dH\u0016J\u0010\u0010f\u001a\u00020\u001d2\u0006\u0010g\u001a\u00020\u0005H\u0016J\u0010\u0010h\u001a\u00020\u001d2\u0006\u0010g\u001a\u00020\u0005H\u0016J&\u0010i\u001a\u00020M2\u0008\u0010&\u001a\u0004\u0018\u00010\u00172\u0008\u0010*\u001a\u0004\u0018\u00010\u00172\u0008\u00100\u001a\u0004\u0018\u000101H\u0016J\u0015\u0010j\u001a\u00020\u00002\u0006\u0010k\u001a\u00020PH\u0000\u00a2\u0006\u0002\u0008lR\u001c\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\n0\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000fR\u001a\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u0004\u0018\u00010\u00178VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0014\u0010\u001a\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u000fR$\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001c\u001a\u00020\u001d@PX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001f\u0010 \"\u0004\u0008!\u0010\"R\u001a\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u0015R\u0016\u0010&\u001a\u0004\u0018\u00010\u00178VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010\u0019R\u0014\u0010(\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010\u000fR\u0016\u0010*\u001a\u0004\u0018\u00010\u00178VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008+\u0010\u0019R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010-R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0016\u00100\u001a\u0004\u0018\u0001018VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00082\u00103R\"\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u0005058\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u00086\u00107\u001a\u0004\u00088\u00109R\u0014\u0010:\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008;\u0010/R\"\u0010<\u001a\u0008\u0012\u0004\u0012\u00020\u0005058\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008=\u00107\u001a\u0004\u0008>\u00109R\u0014\u0010?\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008@\u0010\u000fR\u0014\u0010A\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008B\u0010/R\u0014\u0010C\u001a\u00020\u00178VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008D\u0010\u0019R\u0014\u0010E\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008F\u0010\u000fR$\u0010\u0006\u001a\u00020\u00052\u0006\u0010G\u001a\u00020\u0005@PX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008H\u0010/\"\u0004\u0008I\u0010J\u00a8\u0006m"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealSplitState;",
        "Lcom/squareup/splitticket/SplitState;",
        "order",
        "Lcom/squareup/payment/Order;",
        "ordinal",
        "",
        "viewIndex",
        "(Lcom/squareup/payment/Order;II)V",
        "_cartAmountDiscounts",
        "",
        "Lcom/squareup/checkout/Discount;",
        "kotlin.jvm.PlatformType",
        "additionalTaxAmount",
        "Lcom/squareup/protos/common/Money;",
        "getAdditionalTaxAmount",
        "()Lcom/squareup/protos/common/Money;",
        "automaticGratuityAmount",
        "getAutomaticGratuityAmount",
        "cartAmountDiscounts",
        "",
        "getCartAmountDiscounts",
        "()Ljava/util/List;",
        "cartDiningOptionText",
        "",
        "getCartDiningOptionText",
        "()Ljava/lang/String;",
        "compAmount",
        "getCompAmount",
        "value",
        "",
        "hasBeenSaved",
        "getHasBeenSaved",
        "()Z",
        "setHasBeenSaved$impl_release",
        "(Z)V",
        "items",
        "Lcom/squareup/checkout/CartItem;",
        "getItems",
        "name",
        "getName",
        "negativePerItemDiscountsAmount",
        "getNegativePerItemDiscountsAmount",
        "note",
        "getNote",
        "getOrder$impl_release",
        "()Lcom/squareup/payment/Order;",
        "getOrdinal",
        "()I",
        "predefinedTicket",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "getPredefinedTicket",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "selectedDiscountPositions",
        "Ljava/util/TreeSet;",
        "selectedDiscountPositions$annotations",
        "()V",
        "getSelectedDiscountPositions$impl_release",
        "()Ljava/util/TreeSet;",
        "selectedEntriesCount",
        "getSelectedEntriesCount",
        "selectedItemPositions",
        "selectedItemPositions$annotations",
        "getSelectedItemPositions$impl_release",
        "subtotalAmount",
        "getSubtotalAmount",
        "taxTypeId",
        "getTaxTypeId",
        "ticketId",
        "getTicketId",
        "totalAmount",
        "getTotalAmount",
        "<set-?>",
        "getViewIndex",
        "setViewIndex$impl_release",
        "(I)V",
        "autoGratuityIsTaxable",
        "clearSelectionsAndInvalidate",
        "",
        "discountsToMove",
        "getConfigurationForMoveAndInvalidate",
        "Lcom/squareup/splitticket/MoveConfiguration;",
        "getConfigurationForMoveAndInvalidate$impl_release",
        "getHoldsCustomer",
        "Lcom/squareup/payment/crm/HoldsCustomer;",
        "hasAnySelectedEntries",
        "hasAnySelectedEntries$impl_release",
        "hasAtLeastOneUnitPricedItem",
        "hasAtLeastOneUnitPricedItem$impl_release",
        "hasCartAmountDiscounts",
        "hasItems",
        "hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem",
        "isDiscountSelected",
        "positionToCheck",
        "isItemSelected",
        "itemsToMove",
        "onCustomerChanged",
        "Lrx/Observable;",
        "shouldShowAutoGratuityRow",
        "shouldShowComps",
        "shouldShowPerItemDiscounts",
        "includeComps",
        "shouldShowTaxRow",
        "toggleDiscountSelection",
        "position",
        "toggleItemSelection",
        "updateTicketDetails",
        "withMoveConfiguration",
        "moveConfiguration",
        "withMoveConfiguration$impl_release",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _cartAmountDiscounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private hasBeenSaved:Z

.field private final order:Lcom/squareup/payment/Order;

.field private final ordinal:I

.field private final selectedDiscountPositions:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedItemPositions:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private viewIndex:I


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Order;II)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    iput p2, p0, Lcom/squareup/splitticket/RealSplitState;->ordinal:I

    .line 22
    iget-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 25
    iput p3, p0, Lcom/squareup/splitticket/RealSplitState;->viewIndex:I

    .line 51
    iget-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    .line 52
    iget-object p2, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 51
    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 325
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 326
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object v0, p3

    check-cast v0, Lcom/squareup/checkout/Discount;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->isCartScopeAmountDiscount()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 327
    :cond_1
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/util/Collection;

    .line 54
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->_cartAmountDiscounts:Ljava/util/List;

    .line 57
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object p1

    const-string p2, "Collections.reverseOrder()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x0

    new-array v0, p3, [Ljava/lang/Integer;

    invoke-static {p1, v0}, Lkotlin/collections/SetsKt;->sortedSetOf(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/TreeSet;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    .line 71
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array p2, p3, [Ljava/lang/Integer;

    invoke-static {p1, p2}, Lkotlin/collections/SetsKt;->sortedSetOf(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/TreeSet;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    return-void

    .line 22
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "id"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final clearSelectionsAndInvalidate()V
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 262
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 263
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->invalidate()V

    return-void
.end method

.method private final discountsToMove()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 251
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    check-cast v0, Ljava/lang/Iterable;

    .line 317
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 318
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 319
    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 251
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Discount;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 320
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 322
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 323
    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 253
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isCartScopeAmountDiscount()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 254
    iget-object v3, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    iget-object v4, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/payment/Order;->manuallyRemoveDiscountFromAllItems(Ljava/lang/String;)Z

    .line 255
    iget-object v3, p0, Lcom/squareup/splitticket/RealSplitState;->_cartAmountDiscounts:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 256
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 253
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be cart-scoped fixed amount discount!"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 324
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 257
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final itemsToMove()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 241
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    check-cast v0, Ljava/lang/Iterable;

    .line 313
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 314
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 315
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 244
    iget-object v3, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 245
    iget-object v4, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v4, v2, v5, v6, v7}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    const-string v2, "item"

    .line 246
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/splitticket/OrdersKt;->withoutCartAmountDiscounts(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 316
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 247
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic selectedDiscountPositions$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic selectedItemPositions$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public autoGratuityIsTaxable()Z
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->isTaxable()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAdditionalTaxAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.additionalTaxes"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAutomaticGratuityAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAutoGratuityAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.autoGratuityAmount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCartAmountDiscounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->_cartAmountDiscounts:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCartDiningOptionText()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDiningOptionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.allComps"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getConfigurationForMoveAndInvalidate$impl_release()Lcom/squareup/splitticket/MoveConfiguration;
    .locals 8

    .line 207
    invoke-direct {p0}, Lcom/squareup/splitticket/RealSplitState;->itemsToMove()Ljava/util/List;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-static {v1}, Lcom/squareup/splitticket/OrdersKt;->getSourceTicketInformation(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    move-result-object v1

    .line 212
    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    .line 299
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 300
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 301
    check-cast v5, Lcom/squareup/checkout/CartItem;

    .line 212
    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->courseId:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 213
    iget-object v2, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 303
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    check-cast v5, Ljava/util/Collection;

    .line 304
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 214
    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v7, v7, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v3, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 305
    :cond_2
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 306
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v5, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 307
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 308
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 216
    check-cast v4, Lcom/squareup/wire/Message;

    .line 310
    invoke-virtual {v4}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v4

    if-eqz v4, :cond_4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    .line 217
    iget-object v6, v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-nez v6, :cond_3

    .line 218
    invoke-virtual {v5, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    .line 311
    :cond_3
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 220
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 310
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type B"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_5
    check-cast v2, Ljava/util/List;

    .line 223
    invoke-direct {p0}, Lcom/squareup/splitticket/RealSplitState;->discountsToMove()Ljava/util/List;

    move-result-object v1

    .line 225
    invoke-direct {p0}, Lcom/squareup/splitticket/RealSplitState;->clearSelectionsAndInvalidate()V

    .line 227
    new-instance v3, Lcom/squareup/splitticket/MoveConfiguration;

    invoke-direct {v3, v0, v1, v2}, Lcom/squareup/splitticket/MoveConfiguration;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v3
.end method

.method public getHasBeenSaved()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/squareup/splitticket/RealSplitState;->hasBeenSaved:Z

    return v0
.end method

.method public getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    check-cast v0, Lcom/squareup/payment/crm/HoldsCustomer;

    return-object v0
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "order.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNegativePerItemDiscountsAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNegativePerItemDiscountsAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.negativePerItemDiscountsAmount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getOrder$impl_release()Lcom/squareup/payment/Order;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/splitticket/RealSplitState;->ordinal:I

    return v0
.end method

.method public getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    return-object v0
.end method

.method public final getSelectedDiscountPositions$impl_release()Ljava/util/TreeSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    return-object v0
.end method

.method public getSelectedEntriesCount()I
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getSelectedItemPositions$impl_release()Ljava/util/TreeSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    return-object v0
.end method

.method public getSubtotalAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSubtotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.subtotal"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTaxTypeId()I
    .locals 5

    .line 129
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 130
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    return v0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getInclusiveTaxesAmount()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 133
    sget v0, Lcom/squareup/common/strings/R$string;->cart_tax_row_included:I

    return v0

    .line 135
    :cond_1
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    return v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "order.ticketId"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTotalAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "order.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getViewIndex()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/splitticket/RealSplitState;->viewIndex:I

    return v0
.end method

.method public final hasAnySelectedEntries$impl_release()Z
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public final hasAtLeastOneUnitPricedItem$impl_release()Z
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasAtLeastOneUnitPricedItem()Z

    move-result v0

    return v0
.end method

.method public hasCartAmountDiscounts()Z
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->_cartAmountDiscounts:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasItems()Z
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z

    move-result v0

    return v0
.end method

.method public isDiscountSelected(I)Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public isItemSelected(I)Z
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "order.onCustomerChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setHasBeenSaved$impl_release(Z)V
    .locals 1

    if-eqz p1, :cond_1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 36
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 38
    iput-boolean p1, p0, Lcom/squareup/splitticket/RealSplitState;->hasBeenSaved:Z

    return-void

    .line 34
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot resave a ticket"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 33
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot unsave a ticket"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public setViewIndex$impl_release(I)V
    .locals 0

    .line 25
    iput p1, p0, Lcom/squareup/splitticket/RealSplitState;->viewIndex:I

    return-void
.end method

.method public shouldShowAutoGratuityRow()Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldShowComps()Z
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasCompedItems()Z

    move-result v0

    return v0
.end method

.method public shouldShowPerItemDiscounts(Z)Z
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->hasItems()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->hasPerItemDiscounts(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldShowTaxRow()Z
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->hasItems()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasAvailableTaxes()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toggleDiscountSelection(I)Z
    .locals 1

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealSplitState;->isDiscountSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedDiscountPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public toggleItemSelection(I)Z
    .locals 1

    .line 62
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealSplitState;->isItemSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->selectedItemPositions:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public updateTicketDetails(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->setOpenTicketName(Ljava/lang/String;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Order;->setOpenTicketNote(Ljava/lang/String;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1, p3}, Lcom/squareup/payment/Order;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    return-void
.end method

.method public final withMoveConfiguration$impl_release(Lcom/squareup/splitticket/MoveConfiguration;)Lcom/squareup/splitticket/RealSplitState;
    .locals 8

    const-string v0, "moveConfiguration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 166
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 274
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 275
    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 168
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 276
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 277
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 169
    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v5

    iget-boolean v5, v5, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_1

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 278
    :cond_2
    check-cast v3, Ljava/util/List;

    .line 169
    check-cast v3, Ljava/lang/Iterable;

    .line 279
    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 281
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    const/16 v0, 0xa

    .line 282
    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v2

    .line 283
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v3, Ljava/util/Map;

    .line 284
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 285
    move-object v4, v2

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 171
    iget-object v4, v4, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 174
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v1

    .line 173
    invoke-static {v1, v3}, Lcom/squareup/splitticket/OrdersKt;->getIncomingCartScopedDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    iget-object v4, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v4}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v4

    const-string v5, "order.items"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Iterable;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    sget-object v4, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    .line 182
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    iget-object v5, p0, Lcom/squareup/splitticket/RealSplitState;->_cartAmountDiscounts:Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    .line 183
    iget-object v5, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v5}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v5

    const-string v6, "order.addedCouponsAndCartScopeDiscounts"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v5}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 185
    iget-object v5, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v5}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;

    move-result-object v5

    const-string v6, "order.addedCouponsAndCartScopeDiscountEvents"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v5}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 187
    iget-object v5, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v5}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 288
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v5, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 289
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 290
    check-cast v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 187
    iget-object v5, v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v6, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 291
    :cond_5
    check-cast v6, Ljava/util/List;

    .line 188
    invoke-virtual {p1}, Lcom/squareup/splitticket/MoveConfiguration;->getAssociatedCourses()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 292
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 293
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 189
    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v7, v7, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_6

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 294
    :cond_7
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 295
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 190
    iget-object v5, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v5}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/squareup/payment/OrderCoursingHandler;->addCourse(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;)V

    goto :goto_5

    .line 192
    :cond_8
    iget-object p1, p0, Lcom/squareup/splitticket/RealSplitState;->order:Lcom/squareup/payment/Order;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 193
    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p1, v2}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 194
    invoke-virtual {p1, v3}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 195
    invoke-virtual {p1, v1}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 196
    invoke-virtual {p1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p1

    .line 199
    check-cast v4, Ljava/lang/Iterable;

    .line 297
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 200
    invoke-virtual {p1, v1}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    goto :goto_6

    .line 203
    :cond_9
    new-instance v0, Lcom/squareup/splitticket/RealSplitState;

    const-string v1, "revisedOrder"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->getOrdinal()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/splitticket/RealSplitState;-><init>(Lcom/squareup/payment/Order;II)V

    return-object v0
.end method
