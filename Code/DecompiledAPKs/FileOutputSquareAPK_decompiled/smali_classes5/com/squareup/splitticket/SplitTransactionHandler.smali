.class public interface abstract Lcom/squareup/splitticket/SplitTransactionHandler;
.super Ljava/lang/Object;
.source "SplitTransactionHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\n\u001a\u00020\u000bH&J\u001a\u0010\u000c\u001a\u00020\u000b2\u0008\u0010\r\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000e\u001a\u00020\u0007H&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/splitticket/SplitTransactionHandler;",
        "",
        "currentTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "getCurrentTicket",
        "()Lcom/squareup/tickets/OpenTicket;",
        "order",
        "Lcom/squareup/payment/Order;",
        "getOrder",
        "()Lcom/squareup/payment/Order;",
        "reset",
        "",
        "setTicketFromSplit",
        "splitTicket",
        "splitTicketOrder",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCurrentTicket()Lcom/squareup/tickets/OpenTicket;
.end method

.method public abstract getOrder()Lcom/squareup/payment/Order;
.end method

.method public abstract reset()V
.end method

.method public abstract setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V
.end method
