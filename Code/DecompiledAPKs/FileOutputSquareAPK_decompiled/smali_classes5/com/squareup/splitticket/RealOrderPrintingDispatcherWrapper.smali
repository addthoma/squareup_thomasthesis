.class public final Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;
.super Ljava/lang/Object;
.source "RealOrderPrintingDispatcherWrapper.kt"

# interfaces
.implements Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016J\u001a\u0010\r\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;",
        "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "(Lcom/squareup/print/OrderPrintingDispatcher;)V",
        "getOrderPrintingDispatcher",
        "()Lcom/squareup/print/OrderPrintingDispatcher;",
        "printBill",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "ticketName",
        "",
        "printStubAndTicket",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/print/OrderPrintingDispatcher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-void
.end method


# virtual methods
.method public final getOrderPrintingDispatcher()Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-object v0
.end method

.method public printBill(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printBill(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    return-void
.end method

.method public printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/splitticket/RealOrderPrintingDispatcherWrapper;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    return-void
.end method
