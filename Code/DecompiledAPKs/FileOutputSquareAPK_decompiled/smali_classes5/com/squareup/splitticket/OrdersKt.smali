.class public final Lcom/squareup/splitticket/OrdersKt;
.super Ljava/lang/Object;
.source "Orders.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Orders.kt\ncom/squareup/splitticket/OrdersKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,214:1\n704#2:215\n777#2,2:216\n1265#2,12:218\n1288#2:230\n1313#2,3:231\n1316#2,3:241\n1550#2,3:247\n1609#2,8:250\n1360#2:259\n1429#2,3:260\n1360#2:263\n1429#2,3:264\n1360#2:267\n1429#2,3:268\n704#2:271\n777#2,2:272\n1265#2,9:274\n1360#2:283\n1429#2,3:284\n1274#2,3:287\n1265#2,12:290\n704#2:302\n777#2,2:303\n1642#2,2:305\n1265#2,9:307\n704#2:316\n777#2,2:317\n1360#2:319\n1429#2,3:320\n1274#2,3:323\n1642#2,2:326\n1265#2,9:328\n704#2:337\n777#2,2:338\n704#2:340\n777#2,2:341\n1274#2,3:343\n1099#2,2:346\n1127#2,4:348\n347#3,7:234\n67#4:244\n92#4,2:245\n94#4:258\n*E\n*S KotlinDebug\n*F\n+ 1 Orders.kt\ncom/squareup/splitticket/OrdersKt\n*L\n19#1:215\n19#1,2:216\n29#1,12:218\n49#1:230\n49#1,3:231\n49#1,3:241\n51#1,3:247\n51#1,8:250\n75#1:259\n75#1,3:260\n77#1:263\n77#1,3:264\n78#1:267\n78#1,3:268\n147#1:271\n147#1,2:272\n149#1,9:274\n149#1:283\n149#1,3:284\n149#1,3:287\n150#1,12:290\n155#1:302\n155#1,2:303\n155#1,2:305\n167#1,9:307\n167#1:316\n167#1,2:317\n167#1:319\n167#1,3:320\n167#1,3:323\n186#1,2:326\n199#1,9:328\n199#1:337\n199#1,2:338\n199#1:340\n199#1,2:341\n199#1,3:343\n205#1,2:346\n205#1,4:348\n49#1,7:234\n51#1:244\n51#1,2:245\n51#1:258\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001aF\u0010\u0005\u001a\u001e\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\t0\t0\u00062\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000e0\u0006H\u0000\u001a(\u0010\u000f\u001a\u00020\u0002*\u00020\u00022\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000bH\u0000\u001a\u000c\u0010\u0011\u001a\u00020\u0002*\u00020\u0002H\u0000\u001a\"\u0010\u0012\u001a\u00020\u0002*\u00020\u00022\u0014\u0010\u0013\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0014H\u0000\u001a\"\u0010\u0015\u001a\u00020\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00072\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019H\u0000\u001a:\u0010\u001b\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006*\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u00022\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000b2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0000\u001a\u000c\u0010 \u001a\u00020\u0002*\u00020\u0002H\u0000\u001a\u000c\u0010!\u001a\u00020\u0002*\u00020\u0002H\u0000\u001a\u000c\u0010\"\u001a\u00020\u0002*\u00020\u0002H\u0000\u001a\u0014\u0010#\u001a\n \u0008*\u0004\u0018\u00010\u000c0\u000c*\u00020\u000cH\u0000\u001a\u000c\u0010$\u001a\u00020\u0002*\u00020\u0002H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006%"
    }
    d2 = {
        "sourceTicketInformation",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;",
        "Lcom/squareup/payment/Order;",
        "getSourceTicketInformation",
        "(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;",
        "getIncomingCartScopedDiscountEvents",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/bills/Itemization$Event;",
        "incomingItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "incomingCartScopedDiscounts",
        "Lcom/squareup/checkout/Discount;",
        "buildOrderWithItemsAndDiscounts",
        "incomingCartAmountDiscounts",
        "explodeStackedItems",
        "flashNewItemIds",
        "oldIdsToNewIds",
        "",
        "mergeStates",
        "Lcom/squareup/splitticket/FosterState;",
        "ticketIdToSkip",
        "statesToIterate",
        "",
        "Lcom/squareup/splitticket/SplitState;",
        "pruneOrder",
        "sourceOfRemovals",
        "cartAmountDiscountsToRemove",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "rejoinExplodedItems",
        "removeUnlockedItems",
        "withSortedItems",
        "withoutCartAmountDiscounts",
        "withoutVoidedItems",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final buildOrderWithItemsAndDiscounts(Lcom/squareup/payment/Order;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/Order;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    const-string v0, "$this$buildOrderWithItemsAndDiscounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "incomingItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "incomingCartAmountDiscounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 307
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 314
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 315
    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 168
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 316
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 317
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/checkout/Discount;

    .line 169
    invoke-virtual {v6}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v6

    iget-boolean v6, v6, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_0

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 318
    :cond_1
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 319
    new-instance v3, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 320
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 321
    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 170
    iget-object v6, v5, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-static {v6, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 322
    :cond_2
    check-cast v3, Ljava/util/List;

    .line 170
    check-cast v3, Ljava/lang/Iterable;

    .line 323
    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 325
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 172
    invoke-static {v1}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v1

    .line 174
    invoke-static {p1, v1}, Lcom/squareup/splitticket/OrdersKt;->getIncomingCartScopedDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 178
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->cloneState(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 179
    sget-object v2, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 180
    invoke-virtual {p0, v1}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 182
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 183
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    .line 186
    check-cast p2, Ljava/lang/Iterable;

    .line 326
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/Discount;

    .line 186
    invoke-virtual {p0, p2}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    goto :goto_3

    :cond_4
    const-string p1, "revisedOrder"

    .line 187
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final explodeStackedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 8

    const-string v0, "$this$explodeStackedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p0

    const-string v1, "items"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 218
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 225
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 226
    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 30
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v4, "item.selectedVariation"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    goto :goto_2

    .line 33
    :cond_0
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v3, :cond_1

    .line 34
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 35
    sget-object v7, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v6, v7}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 36
    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v6

    .line 33
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    check-cast v4, Ljava/util/List;

    goto :goto_3

    .line 31
    :cond_2
    :goto_2
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 38
    :goto_3
    check-cast v4, Ljava/lang/Iterable;

    .line 227
    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 229
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 29
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string v0, "Order.Builder.fromOrder(\u2026    }\n    })\n    .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final flashNewItemIds(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/payment/Order;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    const-string v0, "$this$flashNewItemIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 259
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 260
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 261
    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 75
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v4, v5}, Lcom/squareup/payment/Order;->splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    :cond_0
    check-cast v2, Ljava/util/List;

    if-eqz p1, :cond_3

    .line 77
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 263
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 264
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 265
    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 77
    iget-object v4, v4, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 266
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 78
    move-object v0, v2

    check-cast v0, Ljava/lang/Iterable;

    .line 267
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 268
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 269
    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 78
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 270
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 78
    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->zip(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 77
    invoke-static {p1, v0}, Lkotlin/collections/MapsKt;->putAll(Ljava/util/Map;Ljava/lang/Iterable;)V

    .line 80
    :cond_3
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 81
    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p0, v2}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string p1, "Order.Builder.fromOrder(\u2026(newItems)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getIncomingCartScopedDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "incomingItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "incomingCartScopedDiscounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    check-cast p0, Ljava/lang/Iterable;

    .line 328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 335
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 336
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 200
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    const-string v2, "incomingItem.events"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 337
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 338
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 200
    iget-object v6, v6, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v7, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v6, v7, :cond_1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 339
    :cond_2
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 341
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 202
    iget-object v6, v6, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Discount;

    if-eqz v6, :cond_4

    .line 203
    invoke-virtual {v6}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v6

    iget-boolean v6, v6, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    if-nez v6, :cond_4

    const/4 v6, 0x1

    goto :goto_4

    :cond_4
    const/4 v6, 0x0

    :goto_4
    if-eqz v6, :cond_3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 342
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 204
    check-cast v1, Ljava/lang/Iterable;

    .line 343
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 345
    :cond_6
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    const/16 p0, 0xa

    .line 346
    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p0

    invoke-static {p0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result p0

    const/16 p1, 0x10

    invoke-static {p0, p1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p0

    .line 347
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast p1, Ljava/util/Map;

    .line 348
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 349
    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 205
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_7
    return-object p1
.end method

.method public static final getSourceTicketInformation(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;
    .locals 2

    const-string v0, "$this$sourceTicketInformation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;-><init>()V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;

    move-result-object p0

    .line 194
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    move-result-object p0

    const-string v0, "SourceTicketInformation.\u2026icketName)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mergeStates(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)Lcom/squareup/splitticket/FosterState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/splitticket/SplitState;",
            ">;)",
            "Lcom/squareup/splitticket/FosterState;"
        }
    .end annotation

    const-string v0, "$this$mergeStates"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketIdToSkip"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statesToIterate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    check-cast p2, Ljava/lang/Iterable;

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 272
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/splitticket/SplitState;

    .line 147
    invoke-interface {v2}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 273
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 149
    check-cast v0, Ljava/lang/Iterable;

    .line 274
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 281
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 282
    check-cast v1, Lcom/squareup/splitticket/SplitState;

    .line 149
    invoke-interface {v1}, Lcom/squareup/splitticket/SplitState;->getItems()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 283
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 284
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 285
    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 149
    invoke-static {v3}, Lcom/squareup/splitticket/OrdersKt;->withoutCartAmountDiscounts(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 286
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 149
    check-cast v2, Ljava/lang/Iterable;

    .line 287
    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 289
    :cond_3
    check-cast p1, Ljava/util/List;

    .line 290
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 297
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 298
    check-cast v1, Lcom/squareup/splitticket/SplitState;

    .line 150
    invoke-interface {v1}, Lcom/squareup/splitticket/SplitState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 299
    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_3

    .line 301
    :cond_4
    check-cast p2, Ljava/util/List;

    .line 151
    invoke-static {p0, p1, p2}, Lcom/squareup/splitticket/OrdersKt;->buildOrderWithItemsAndDiscounts(Lcom/squareup/payment/Order;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/Order;

    move-result-object p0

    .line 152
    new-instance p1, Lcom/squareup/splitticket/RealFosterState;

    invoke-static {p0}, Lcom/squareup/splitticket/OrdersKt;->rejoinExplodedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    check-cast p1, Lcom/squareup/splitticket/FosterState;

    return-object p1
.end method

.method public static final pruneOrder(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;Ljava/util/List;Lcom/squareup/protos/client/Employee;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Lcom/squareup/protos/client/Employee;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$pruneOrder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceOfRemovals"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartAmountDiscountsToRemove"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 106
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    const-string v2, "sourceOfRemovals.items"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getDeletedItems()Ljava/util/List;

    move-result-object p1

    const-string v2, "sourceOfRemovals.deletedItems"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 109
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    const/4 v2, 0x0

    .line 110
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    const-string v4, "items"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "items[pruneIndex]"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 112
    iget-object v5, v4, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v6, "itemToBePruned.idPair.client_id"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v6, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x1

    xor-int/2addr v6, v7

    if-eqz v6, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 118
    :cond_1
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v6, "sourceItem.quantity"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v4, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v8, "itemToBePruned.quantity"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v6}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_2

    .line 123
    iget-object v3, v4, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v3, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    const-string v3, "itemToBePruned.quantity.\u2026ract(sourceItem.quantity)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0, v2, v7, p3, v7}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    const-wide/16 v2, 0x0

    .line 126
    invoke-static {v1, v2, v3}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    invoke-static {v4, v1}, Lcom/squareup/payment/Order;->splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;

    move-result-object v1

    .line 129
    invoke-virtual {p0, v1}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 131
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 118
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Removed item cannot have higher quantity than original!"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 137
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/Discount;

    .line 138
    iget-object p2, p2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/payment/Order;->manuallyRemoveDiscountFromAllItems(Ljava/lang/String;)Z

    goto :goto_2

    :cond_4
    return-object v0
.end method

.method public static final rejoinExplodedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 8

    const-string v0, "$this$rejoinExplodedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 230
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 231
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 232
    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 49
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 234
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 233
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 237
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 241
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_1
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 245
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 246
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 52
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 53
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    goto/16 :goto_4

    .line 55
    :cond_2
    move-object v3, v2

    check-cast v3, Ljava/lang/Iterable;

    .line 247
    instance-of v5, v3, Ljava/util/Collection;

    const/4 v6, 0x0

    if-eqz v5, :cond_3

    move-object v5, v3

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_2

    .line 248
    :cond_3
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/CartItem;

    .line 55
    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v7, "it.selectedVariation"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v6, 0x1

    :cond_5
    :goto_2
    xor-int/lit8 v3, v6, 0x1

    if-eqz v3, :cond_7

    .line 58
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 59
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 60
    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 251
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 252
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v2, v5}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v2

    .line 253
    :goto_3
    invoke-interface {v2}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 254
    invoke-interface {v2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/CartItem;

    const-string v6, "acc"

    .line 60
    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v6, "item.quantity"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    const-string/jumbo v5, "this.add(other)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 61
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 62
    :goto_4
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 56
    :cond_7
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Shouldn\'t have exploded unit priced variation into "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 258
    :cond_8
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 51
    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 64
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string v0, "Order.Builder.fromOrder(\u2026}\n      })\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final removeUnlockedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 4

    const-string v0, "$this$removeUnlockedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p0

    const-string v1, "items"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 216
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 19
    iget-boolean v3, v3, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 19
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 20
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string v0, "Order.Builder.fromOrder(\u2026iguration })\n    .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withSortedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 2

    const-string v0, "$this$withSortedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p0

    const-string v1, "items"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    sget-object v1, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-virtual {v0, p0}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 209
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string v0, "Order.Builder.fromOrder(\u2026Comparator))\n    .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withoutCartAmountDiscounts(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 4

    const-string v0, "$this$withoutCartAmountDiscounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 156
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 302
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 303
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 156
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->isCartScopeDiscount()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 305
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 158
    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_1

    .line 160
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    return-object p0
.end method

.method public static final withoutVoidedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;
    .locals 1

    const-string v0, "$this$withoutVoidedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 212
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-virtual {v0, p0}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 213
    invoke-virtual {p0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p0

    const-string v0, "Order.Builder.fromOrder(\u2026VoidedItems)\n    .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
