.class public Lcom/squareup/tickets/Tickets$MergeResults;
.super Ljava/lang/Object;
.source "Tickets.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/Tickets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MergeResults"
.end annotation


# instance fields
.field public final canceledDueToClosedTicket:Z

.field public final mergedCount:I

.field public final ticketName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-object p1, p0, Lcom/squareup/tickets/Tickets$MergeResults;->ticketName:Ljava/lang/String;

    .line 374
    iput p2, p0, Lcom/squareup/tickets/Tickets$MergeResults;->mergedCount:I

    const/4 p1, 0x0

    .line 375
    iput-boolean p1, p0, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    iput-object p1, p0, Lcom/squareup/tickets/Tickets$MergeResults;->ticketName:Ljava/lang/String;

    .line 380
    iput p2, p0, Lcom/squareup/tickets/Tickets$MergeResults;->mergedCount:I

    .line 381
    iput-boolean p3, p0, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    return-void
.end method
