.class public Lcom/squareup/tickets/TicketCountsCache;
.super Ljava/lang/Object;
.source "TicketCountsCache.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private ticketCounter:Lcom/squareup/tickets/TicketCounter;

.field private final ticketCountsRefreshed:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tickets:Lcom/squareup/tickets/Tickets;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/tickets/TicketCountsCache;->tickets:Lcom/squareup/tickets/Tickets;

    .line 37
    iput-object p2, p0, Lcom/squareup/tickets/TicketCountsCache;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 38
    iput-object p3, p0, Lcom/squareup/tickets/TicketCountsCache;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 39
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCountsRefreshed:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 40
    sget-object p1, Lcom/squareup/tickets/TicketCounter;->EMPTY:Lcom/squareup/tickets/TicketCounter;

    iput-object p1, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    return-void
.end method


# virtual methods
.method public getAllTicketsCount()I
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCounter;->getAllTicketsCount()I

    move-result v0

    return v0
.end method

.method public getCustomTicketsCount()I
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCounter;->getCustomTicketsCount()I

    move-result v0

    return v0
.end method

.method public getGroupTicketCount(Ljava/lang/String;)I
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/TicketCounter;->getGroupTicketCount(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public hasTickets()Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCounter;->hasTickets()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$loadAndPost$1$TicketCountsCache(Lcom/squareup/tickets/TicketsResult;)V
    .locals 1

    .line 93
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketCounter;

    if-eqz p1, :cond_0

    goto :goto_0

    .line 94
    :cond_0
    sget-object p1, Lcom/squareup/tickets/TicketCounter;->EMPTY:Lcom/squareup/tickets/TicketCounter;

    :goto_0
    iput-object p1, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCounter:Lcom/squareup/tickets/TicketCounter;

    .line 95
    iget-object p1, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCountsRefreshed:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$TicketCountsCache(Ljava/util/List;)V
    .locals 2

    .line 46
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    .line 47
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isOpenedByUpdate()Z

    move-result v1

    if-nez v1, :cond_1

    .line 48
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isClosedByUpdate()Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isDeletedByUpdate()Z

    move-result v1

    if-nez v1, :cond_1

    .line 50
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isConvertedToCustomTicketByUpdate()Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isGroupChangedByUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketCountsCache;->loadAndPost()V

    :cond_2
    return-void
.end method

.method public loadAndPost()V
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    .line 89
    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 90
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 92
    :goto_0
    iget-object v1, p0, Lcom/squareup/tickets/TicketCountsCache;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v2, Lcom/squareup/tickets/-$$Lambda$TicketCountsCache$1gCQjfZxUNQS9iln9ar6-1I7_Nc;

    invoke-direct {v2, p0}, Lcom/squareup/tickets/-$$Lambda$TicketCountsCache$1gCQjfZxUNQS9iln9ar6-1I7_Nc;-><init>(Lcom/squareup/tickets/TicketCountsCache;)V

    invoke-interface {v1, v0, v2}, Lcom/squareup/tickets/Tickets;->getTicketCounts(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->tickets:Lcom/squareup/tickets/Tickets;

    .line 45
    invoke-interface {v0}, Lcom/squareup/tickets/Tickets;->onLocalTicketsUpdated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$TicketCountsCache$03Le4T-l0T8CBI-DoFqB_I_b-tk;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/-$$Lambda$TicketCountsCache$03Le4T-l0T8CBI-DoFqB_I_b-tk;-><init>(Lcom/squareup/tickets/TicketCountsCache;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTicketCountsRefreshed()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/tickets/TicketCountsCache;->ticketCountsRefreshed:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method
