.class public interface abstract Lcom/squareup/tickets/Tickets$InternalTickets;
.super Ljava/lang/Object;
.source "Tickets.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/Tickets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InternalTickets"
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract deleteExpiredClosedTickets()V
.end method

.method public abstract getBroker()Lcom/squareup/tickets/TicketsBroker;
.end method

.method public abstract processListResponse(Ljava/util/List;Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract processUpdateResponse(Lcom/squareup/tickets/OpenTicket;)V
.end method

.method public abstract retrieveNonzeroClientClockTickets(Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract retrieveTicketInfo(Lcom/squareup/tickets/TicketsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract setRemoteUnsyncedFixableTicketCount(J)V
.end method

.method public abstract setRemoteUnsyncedUnfixableTicketCount(J)V
.end method
