.class public abstract Lcom/squareup/tickets/NoTicketsModule;
.super Ljava/lang/Object;
.source "NoTicketsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAvailableTemplateCountCache()Lcom/squareup/opentickets/AvailableTemplateCountCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 24
    sget-object v0, Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;->INSTANCE:Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;

    return-object v0
.end method

.method static provideListScheduler()Lcom/squareup/opentickets/TicketsListScheduler;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 32
    sget-object v0, Lcom/squareup/opentickets/NoOpTicketListScheduler;->INSTANCE:Lcom/squareup/opentickets/NoOpTicketListScheduler;

    return-object v0
.end method

.method static providePredefinedTickets()Lcom/squareup/opentickets/PredefinedTickets;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    sget-object v0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->INSTANCE:Lcom/squareup/opentickets/PredefinedTickets$NoOp;

    return-object v0
.end method

.method static provideSweeperManager()Lcom/squareup/opentickets/TicketsSweeperManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 28
    sget-object v0, Lcom/squareup/opentickets/NoOpTicketSweeperManager;->INSTANCE:Lcom/squareup/opentickets/NoOpTicketSweeperManager;

    return-object v0
.end method


# virtual methods
.method abstract provideInternalTickets(Lcom/squareup/tickets/NoTickets;)Lcom/squareup/tickets/Tickets$InternalTickets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTicketStore(Lcom/squareup/tickets/NoTicketStore;)Lcom/squareup/tickets/TicketStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTickets(Lcom/squareup/tickets/NoTickets;)Lcom/squareup/tickets/Tickets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
