.class public Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomTicketsSyncListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketSort;",
            ")V"
        }
    .end annotation

    .line 405
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    .line 406
    invoke-direct/range {p0 .. p5}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    return-void
.end method


# virtual methods
.method protected fetchTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;",
            ")V"
        }
    .end annotation

    .line 412
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CustomTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$900(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/Tickets;

    move-result-object v0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/Tickets;->getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method
