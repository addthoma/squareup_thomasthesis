.class public Lcom/squareup/tickets/SqliteTicketStore;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SqliteTicketStore.java"

# interfaces
.implements Lcom/squareup/tickets/TicketStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;
    }
.end annotation


# static fields
.field private static final ALL_NONCLOSED_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

.field private static final ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

.field private static final ALL_NONCLOSED_TICKETS_UNORDERED:Ljava/lang/String;

.field private static final ALL_NONZERO_TICKETS_UNORDERED:Ljava/lang/String;

.field private static final COUNT_ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

.field private static final COUNT_OPEN:Lcom/squareup/phrase/Phrase;

.field private static final COUNT_OPEN_BY_GROUP:Lcom/squareup/phrase/Phrase;

.field private static final CREATE:Ljava/lang/String;

.field private static final CREATE_AMOUNT_INDEX:Ljava/lang/String;

.field private static final CREATE_EMPLOYEE_NAME_INDEX:Ljava/lang/String;

.field private static final CREATE_NAME_INDEX:Ljava/lang/String;

.field private static final CREATE_RECENT_INDEX:Ljava/lang/String;

.field private static final CREATE_TICKET_GROUP_ID_INDEX:Ljava/lang/String;

.field private static final CUSTOM_TICKET_FILTER:Ljava/lang/String;

.field public static final DATABASE_NAME:Ljava/lang/String; = "OpenTickets"

.field private static final DATABASE_V1_TO_V2_ALTER_P1:Ljava/lang/String;

.field private static final DATABASE_V1_TO_V2_ALTER_P2:Ljava/lang/String;

.field private static final DATABASE_V2_TO_V3_ALTER_P1:Ljava/lang/String;

.field private static final DATABASE_V2_TO_V3_ALTER_P2:Ljava/lang/String;

.field private static final DATABASE_VERSION:I = 0x3

.field private static final DELETE_TICKETS:Lcom/squareup/phrase/Phrase;

.field private static final EMPLOYEE_TOKEN_FILTER:Lcom/squareup/phrase/Phrase;

.field private static final EXCLUDED_TICKETS_FILTER:Lcom/squareup/phrase/Phrase;

.field private static final FALSE:I = 0x0

.field public static final INDEX_AMOUNT:Ljava/lang/String; = "idx_ticket_amount"

.field public static final INDEX_EMPLOYEE_NAME:Ljava/lang/String; = "idx_ticket_employee_name"

.field public static final INDEX_NAME:Ljava/lang/String; = "idx_ticket_name"

.field public static final INDEX_RECENT:Ljava/lang/String; = "idx_ticket_recent"

.field public static final INDEX_TICKET_GROUP_ID:Ljava/lang/String; = "idx_ticket_group_id"

.field private static final RETRIEVE:Lcom/squareup/phrase/Phrase;

.field private static final SEARCH_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

.field private static final SEARCH_TICKETS_FOR_ONE_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

.field private static final SYNCED_CLOSED_TICKETS_OLDEST_FIRST_CURSOR:Ljava/lang/String;

.field public static final TICKETS_TABLE:Ljava/lang/String; = "TICKETS_TABLE"

.field public static final TICKET_AMOUNT:Ljava/lang/String; = "TICKET_AMOUNT"

.field public static final TICKET_CLIENT_CLOCK_VERSION:Ljava/lang/String; = "TICKET_CLIENT_CLOCK_VERSION"

.field public static final TICKET_CLOSED:Ljava/lang/String; = "TICKET_CLOSED"

.field public static final TICKET_EMPLOYEE_NAME:Ljava/lang/String; = "TICKET_EMPLOYEE_NAME"

.field public static final TICKET_EMPLOYEE_TOKEN:Ljava/lang/String; = "TICKET_EMPLOYEE_TOKEN"

.field private static final TICKET_GROUP_FILTER:Lcom/squareup/phrase/Phrase;

.field public static final TICKET_GROUP_ID:Ljava/lang/String; = "TICKET_GROUP_ID"

.field public static final TICKET_ID:Ljava/lang/String; = "TICKET_ID"

.field public static final TICKET_NAME:Ljava/lang/String; = "TICKET_NAME"

.field public static final TICKET_PROTO_BLOB:Ljava/lang/String; = "DATA"

.field public static final TICKET_TEMPLATE_ID:Ljava/lang/String; = "TICKET_TEMPLATE_ID"

.field public static final TICKET_UPDATED:Ljava/lang/String; = "TICKET_UPDATED"

.field private static final TRUE:I = 0x1


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const-string v0, "CREATE TABLE {table} ( {id} STRING PRIMARY KEY, {name} STRING NOT NULL COLLATE LOCALIZED, {updated} STRING NOT NULL, {ticket_group_id} STRING, {ticket_template_id} STRING, {employee_token} STRING, {employee_name} STRING, {amount} LONG, {version} LONG, {data} BLOB, {completed} INT )"

    .line 113
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "TICKETS_TABLE"

    const-string v2, "table"

    .line 128
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "TICKET_ID"

    const-string v4, "id"

    .line 129
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "TICKET_NAME"

    const-string v6, "name"

    .line 130
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v7, "TICKET_UPDATED"

    const-string/jumbo v8, "updated"

    .line 131
    invoke-virtual {v0, v8, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v8, "ticket_group_id"

    const-string v9, "TICKET_GROUP_ID"

    .line 132
    invoke-virtual {v0, v8, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v8, "ticket_template_id"

    const-string v9, "TICKET_TEMPLATE_ID"

    .line 133
    invoke-virtual {v0, v8, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v8, "TICKET_EMPLOYEE_TOKEN"

    const-string v9, "employee_token"

    .line 134
    invoke-virtual {v0, v9, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "TICKET_EMPLOYEE_NAME"

    const-string v10, "employee_name"

    .line 135
    invoke-virtual {v0, v10, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v10, "amount"

    const-string v11, "TICKET_AMOUNT"

    .line 136
    invoke-virtual {v0, v10, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v10, "version"

    const-string v11, "TICKET_CLIENT_CLOCK_VERSION"

    .line 137
    invoke-virtual {v0, v10, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v10, "DATA"

    const-string v11, "data"

    .line 138
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v12, "completed"

    const-string v13, "TICKET_CLOSED"

    .line 139
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({name})"

    .line 143
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "idx"

    const-string v15, "idx_ticket_name"

    .line 146
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 147
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 148
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 150
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_NAME_INDEX:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({amount})"

    .line 152
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "idx"

    const-string v15, "idx_ticket_amount"

    .line 155
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 156
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "amount"

    const-string v15, "TICKET_AMOUNT"

    .line 157
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 159
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_AMOUNT_INDEX:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({recent})"

    .line 161
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "idx"

    const-string v15, "idx_ticket_recent"

    .line 164
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 165
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "recent"

    .line 166
    invoke-virtual {v0, v14, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_RECENT_INDEX:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({ticket_group_id})"

    .line 170
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "idx"

    const-string v15, "idx_ticket_group_id"

    .line 173
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 174
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v14, "ticket_group_id"

    const-string v15, "TICKET_GROUP_ID"

    .line 175
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 177
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_TICKET_GROUP_ID_INDEX:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({employee_name})"

    .line 179
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "idx"

    const-string v15, "idx_ticket_employee_name"

    .line 182
    invoke-virtual {v0, v14, v15}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 183
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "employee_name"

    .line 184
    invoke-virtual {v0, v14, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 186
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_EMPLOYEE_NAME_INDEX:Ljava/lang/String;

    const-string v0, "SELECT COUNT(*) FROM {table} WHERE {completed} IS {false}     {employee_token_filter}     {custom_ticket_filter}"

    .line 188
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 194
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 195
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v14, 0x0

    const-string v15, "false"

    .line 196
    invoke-virtual {v0, v15, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_OPEN:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {ticket_group_id}, COUNT(*) AS GROUP_TICKET_COUNT FROM {table} WHERE {completed} IS {false} AND {ticket_group_id} IS NOT NULL     {employee_token_filter} GROUP BY {ticket_group_id}"

    .line 198
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v14, "ticket_group_id"

    move-object/from16 v16, v9

    const-string v9, "TICKET_GROUP_ID"

    .line 204
    invoke-virtual {v0, v14, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 205
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 206
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v9, 0x0

    .line 207
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_OPEN_BY_GROUP:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {data} FROM {table} WHERE {version} IS NOT {zero}"

    .line 209
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 214
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v9, "version"

    const-string v14, "TICKET_CLIENT_CLOCK_VERSION"

    .line 215
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-wide/16 v17, 0x0

    .line 216
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v14, "zero"

    invoke-virtual {v0, v14, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 218
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONZERO_TICKETS_UNORDERED:Ljava/lang/String;

    const-string v0, "SELECT {data} FROM {table} WHERE {completed} IS {false}"

    .line 220
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 225
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 226
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v9, 0x0

    .line 227
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 229
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_UNORDERED:Ljava/lang/String;

    const-string v0, "SELECT {id}, {name}, {updated}, {employee_token}, {employee_name}, {amount}, {data} FROM {table} WHERE {completed} IS {false}     {excluded_tickets_filter}     {ticket_group_filter}     {custom_ticket_filter} ORDER BY {order_by}"

    .line 231
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 239
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 240
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v9, "updated"

    .line 241
    invoke-virtual {v0, v9, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_token"

    .line 242
    invoke-virtual {v0, v9, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_name"

    move-object/from16 v14, v16

    .line 243
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "amount"

    const-string v14, "TICKET_AMOUNT"

    .line 244
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 245
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 246
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 247
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v9, 0x0

    .line 248
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {id}, {name}, {updated}, {employee_token}, {employee_name}, {amount}, {data} FROM {table} WHERE {completed} IS {false}     {excluded_tickets_filter}     {ticket_group_filter}     {custom_ticket_filter} ORDER BY ifnull({employee_token} == \"{token}\", 0) DESC,     {order_by}"

    .line 250
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 259
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 260
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v9, "updated"

    .line 261
    invoke-virtual {v0, v9, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_token"

    .line 262
    invoke-virtual {v0, v9, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_name"

    move-object/from16 v14, v16

    .line 263
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "amount"

    const-string v14, "TICKET_AMOUNT"

    .line 264
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 265
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 266
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 267
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v9, 0x0

    .line 268
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT count(*) FROM {table} WHERE {completed} IS {false}     AND {employee_token} == \"{token}\"     {excluded_tickets_filter}     {ticket_group_filter}     {custom_ticket_filter}"

    .line 270
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v14, "employee_token"

    .line 278
    invoke-virtual {v0, v14, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 279
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 280
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {id}, {name}, {updated}, {employee_token}, {employee_name}, {amount}, {data} FROM {table} WHERE {completed} IS {false} AND {name} LIKE ?     {excluded_tickets_filter} ORDER BY {order_by}"

    .line 283
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 289
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 290
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v9, "updated"

    .line 291
    invoke-virtual {v0, v9, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_token"

    .line 292
    invoke-virtual {v0, v9, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "employee_name"

    move-object/from16 v14, v16

    .line 293
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "amount"

    const-string v14, "TICKET_AMOUNT"

    .line 294
    invoke-virtual {v0, v9, v14}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 295
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 296
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 297
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v9, 0x0

    .line 298
    invoke-virtual {v0, v15, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->SEARCH_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

    const-string v0, " SELECT {id}, {name}, {updated}, {employee_token}, {employee_name}, {amount}, {data} FROM {table} WHERE {completed} IS {false}      AND {name} LIKE ?     AND {employee_token} == \"{token}\"     {excluded_tickets_filter} ORDER BY {order_by}"

    .line 300
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 308
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 309
    invoke-virtual {v0, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v5, "updated"

    .line 310
    invoke-virtual {v0, v5, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "employee_token"

    .line 311
    invoke-virtual {v0, v5, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "employee_name"

    move-object/from16 v6, v16

    .line 312
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "amount"

    const-string v7, "TICKET_AMOUNT"

    .line 313
    invoke-virtual {v0, v5, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 314
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 315
    invoke-virtual {v0, v12, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 316
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v5, 0x0

    .line 317
    invoke-virtual {v0, v15, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->SEARCH_TICKETS_FOR_ONE_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {data} FROM {table} WHERE {id} IS \"{retrieve_id}\""

    .line 319
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 323
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 324
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 325
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->RETRIEVE:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {id}, {data} FROM {table} WHERE {closed} IS {true} AND {version} IS {zero} ORDER BY {order_by}"

    .line 327
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 332
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 333
    invoke-virtual {v0, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 334
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "closed"

    .line 335
    invoke-virtual {v0, v5, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v5, "true"

    const/4 v7, 0x1

    .line 336
    invoke-virtual {v0, v5, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v5, "version"

    const-string v7, "TICKET_CLIENT_CLOCK_VERSION"

    .line 337
    invoke-virtual {v0, v5, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-wide/16 v9, 0x0

    .line 338
    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "zero"

    invoke-virtual {v0, v7, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget-object v5, Lcom/squareup/tickets/TicketSort;->OLDEST:Lcom/squareup/tickets/TicketSort;

    .line 339
    invoke-static {v5}, Lcom/squareup/tickets/SqliteTicketStore;->getOrderBy(Lcom/squareup/tickets/TicketSort;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "order_by"

    invoke-virtual {v0, v7, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 341
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->SYNCED_CLOSED_TICKETS_OLDEST_FIRST_CURSOR:Ljava/lang/String;

    const-string v0, "ALTER TABLE {table} ADD COLUMN {employee_token_column} STRING;"

    .line 343
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 345
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "employee_token_column"

    .line 346
    invoke-virtual {v0, v5, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 348
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V1_TO_V2_ALTER_P1:Ljava/lang/String;

    const-string v0, "ALTER TABLE {table} ADD COLUMN {employee_name_column} STRING;"

    .line 350
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 352
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "employee_name_column"

    .line 353
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 354
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 355
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V1_TO_V2_ALTER_P2:Ljava/lang/String;

    const-string v0, "ALTER TABLE {table} ADD COLUMN {ticket_group_id_column} STRING;"

    .line 357
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 359
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v5, "ticket_group_id_column"

    const-string v6, "TICKET_GROUP_ID"

    .line 360
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 362
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V2_TO_V3_ALTER_P1:Ljava/lang/String;

    const-string v0, "ALTER TABLE {table} ADD COLUMN {ticket_template_id_column} STRING;"

    .line 364
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 366
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "ticket_template_id_column"

    const-string v2, "TICKET_TEMPLATE_ID"

    .line 367
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 369
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V2_TO_V3_ALTER_P2:Ljava/lang/String;

    const-string v0, "  AND {ticket_employee_token} == \"{employee_token}\""

    .line 376
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "ticket_employee_token"

    .line 378
    invoke-virtual {v0, v1, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->EMPLOYEE_TOKEN_FILTER:Lcom/squareup/phrase/Phrase;

    const-string v0, "  AND {id} NOT IN ({id_list})"

    .line 384
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 386
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->EXCLUDED_TICKETS_FILTER:Lcom/squareup/phrase/Phrase;

    const-string v0, "  AND {ticket_group_id} == \"{group_id}\""

    .line 392
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "ticket_group_id"

    const-string v2, "TICKET_GROUP_ID"

    .line 394
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->TICKET_GROUP_FILTER:Lcom/squareup/phrase/Phrase;

    const-string v0, "  AND {ticket_template_id} IS NULL"

    .line 400
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "ticket_template_id"

    const-string v2, "TICKET_TEMPLATE_ID"

    .line 402
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 404
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CUSTOM_TICKET_FILTER:Ljava/lang/String;

    const-string/jumbo v0, "{id} IN ({id_list})"

    .line 410
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 412
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DELETE_TICKETS:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Lcom/squareup/util/Res;)V
    .locals 2

    .line 418
    new-instance v0, Ljava/io/File;

    const-string v1, "OpenTickets.db"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 419
    iput-object p3, p0, Lcom/squareup/tickets/SqliteTicketStore;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private buildListQueryPhrase(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;"
        }
    .end annotation

    .line 742
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    invoke-virtual {p5}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "custom_ticket_filter"

    const-string/jumbo v4, "ticket_group_filter"

    const-string v5, "excluded_tickets_filter"

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 765
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Cannot understand EmployeeAccess type "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 752
    :cond_1
    :goto_0
    sget-object p5, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    .line 753
    invoke-static {p4}, Lcom/squareup/tickets/SqliteTicketStore;->nullToStringNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "token"

    invoke-virtual {p5, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p5

    .line 754
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v5, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p5

    .line 755
    invoke-static {p2}, Lcom/squareup/tickets/SqliteTicketStore;->ticketGroupFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p5

    .line 756
    invoke-static {p3}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p5

    .line 757
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_ALL_NONCLOSED_TICKETS_FOR_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    .line 758
    invoke-static {p4}, Lcom/squareup/tickets/SqliteTicketStore;->nullToStringNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, v1, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 759
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, v5, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 760
    invoke-static {p2}, Lcom/squareup/tickets/SqliteTicketStore;->ticketGroupFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v4, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 761
    invoke-static {p3}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 762
    new-instance p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    invoke-direct {p2, p5, p1, v2}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V

    return-object p2

    .line 744
    :cond_2
    sget-object p4, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

    .line 745
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, v5, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 746
    invoke-static {p2}, Lcom/squareup/tickets/SqliteTicketStore;->ticketGroupFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v4, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 747
    invoke-static {p3}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 748
    new-instance p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    invoke-direct {p2, p1, v2}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V

    return-object p2
.end method

.method private buildSearchQueryPhrase(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;"
        }
    .end annotation

    .line 723
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketStore$EmployeeAccess:[I

    invoke-virtual {p3}, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "excluded_tickets_filter"

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 730
    new-instance p3, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->SEARCH_TICKETS_FOR_ONE_EMPLOYEE_CURSOR:Lcom/squareup/phrase/Phrase;

    .line 731
    invoke-static {p2}, Lcom/squareup/tickets/SqliteTicketStore;->nullToStringNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string/jumbo v1, "token"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 732
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-direct {p3, p1, v2}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V

    return-object p3

    .line 735
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot understand EmployeeAccess type "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 726
    :cond_1
    new-instance p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    sget-object p3, Lcom/squareup/tickets/SqliteTicketStore;->SEARCH_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

    .line 727
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-direct {p2, p1, v2}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V

    return-object p2
.end method

.method private buildSelectionArgs(Ljava/lang/String;ZLjava/util/List;)[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 706
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    .line 708
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p3, :cond_1

    .line 711
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 712
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 718
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    return-object p1
.end method

.method private buildTicketContentValues(Lcom/squareup/tickets/OpenTicket;)Landroid/content/ContentValues;
    .locals 4

    .line 656
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 657
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 658
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TICKET_ID"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TICKET_NAME"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientUpdatedAt()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "TICKET_UPDATED"

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v1, :cond_0

    .line 661
    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "TICKET_AMOUNT"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 662
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientClockVersion()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "TICKET_CLIENT_CLOCK_VERSION"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 663
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "TICKET_CLOSED"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 664
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getProto()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/tickets/OpenTicket;->byteArrayFromProto(Lcom/squareup/protos/client/tickets/Ticket;)[B

    move-result-object v1

    const-string v2, "DATA"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 665
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getGroupId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TICKET_GROUP_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getTemplateId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TICKET_TEMPLATE_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v1, p0, Lcom/squareup/tickets/SqliteTicketStore;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v1}, Lcom/squareup/tickets/OpenTicket;->getEmployeeTableName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "TICKET_EMPLOYEE_NAME"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getEmployeeToken()Ljava/lang/String;

    move-result-object p1

    const-string v1, "TICKET_EMPLOYEE_TOKEN"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static customTicketFilter(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    .line 855
    sget-object p0, Lcom/squareup/tickets/SqliteTicketStore;->CUSTOM_TICKET_FILTER:Ljava/lang/String;

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method private doGetTicketList(Ljava/util/List;Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/TicketRowCursorList;"
        }
    .end annotation

    .line 515
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 517
    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 519
    invoke-direct {p0, p1, p6, p7}, Lcom/squareup/tickets/SqliteTicketStore;->buildSearchQueryPhrase(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p6

    move-object v7, p7

    .line 520
    invoke-direct/range {v2 .. v7}, Lcom/squareup/tickets/SqliteTicketStore;->buildListQueryPhrase(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;

    move-result-object p2

    :goto_0
    const/4 p3, -0x1

    .line 525
    iget-object p6, p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;->countQueryPhrase:Lcom/squareup/phrase/Phrase;

    if-eqz p6, :cond_2

    .line 526
    iget-object p6, p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;->countQueryPhrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {p6}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p6

    invoke-interface {p6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p6

    const/4 p7, 0x0

    const/4 v2, 0x0

    .line 527
    invoke-direct {p0, p7, v2, p1}, Lcom/squareup/tickets/SqliteTicketStore;->buildSelectionArgs(Ljava/lang/String;ZLjava/util/List;)[Ljava/lang/String;

    move-result-object p7

    .line 526
    invoke-virtual {v0, p6, p7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p6

    .line 528
    invoke-interface {p6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p7

    if-eqz p7, :cond_1

    .line 529
    invoke-interface {p6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    .line 531
    :cond_1
    invoke-interface {p6}, Landroid/database/Cursor;->close()V

    .line 534
    :cond_2
    iget-object p2, p2, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;->listQueryPhrase:Lcom/squareup/phrase/Phrase;

    .line 535
    invoke-static {p4}, Lcom/squareup/tickets/SqliteTicketStore;->getOrderBy(Lcom/squareup/tickets/TicketSort;)Ljava/lang/String;

    move-result-object p6

    const-string p7, "order_by"

    invoke-virtual {p2, p7, p6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 536
    invoke-direct {p0, p5, v1, p1}, Lcom/squareup/tickets/SqliteTicketStore;->buildSelectionArgs(Ljava/lang/String;ZLjava/util/List;)[Ljava/lang/String;

    move-result-object p1

    .line 534
    invoke-virtual {v0, p2, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 537
    new-instance p2, Lcom/squareup/tickets/TicketRowCursorList;

    new-instance p6, Lcom/squareup/tickets/TicketRowCursor;

    invoke-direct {p6, p1}, Lcom/squareup/tickets/TicketRowCursor;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p2, p6, p4, p5, p3}, Lcom/squareup/tickets/TicketRowCursorList;-><init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;I)V

    return-object p2
.end method

.method private doMigrateFromV1toV2(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .line 674
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V1_TO_V2_ALTER_P1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 675
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V1_TO_V2_ALTER_P2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 676
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_EMPLOYEE_NAME_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private doMigrateFromV2toV3(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .line 681
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V2_TO_V3_ALTER_P1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 682
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->DATABASE_V2_TO_V3_ALTER_P2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 683
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_TICKET_GROUP_ID_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private static employeeTokenFilter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 825
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->EMPLOYEE_TOKEN_FILTER:Lcom/squareup/phrase/Phrase;

    const-string v1, "employee_token"

    .line 826
    invoke-virtual {v0, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 827
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 828
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method private static excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 835
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->EXCLUDED_TICKETS_FILTER:Lcom/squareup/phrase/Phrase;

    .line 836
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const-string v1, "?"

    const-string v2, ", "

    invoke-static {v1, v2, p0}, Lcom/squareup/util/Strings;->joinRepeated(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object p0

    const-string v1, "id_list"

    invoke-virtual {v0, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 837
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 838
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method private static getAllNonclosedTickets(Landroid/database/sqlite/SQLiteDatabase;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 4

    .line 770
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_CURSOR:Lcom/squareup/phrase/Phrase;

    const/4 v1, 0x0

    .line 771
    invoke-static {v1}, Lcom/squareup/tickets/SqliteTicketStore;->excludedTicketsFilter(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "excluded_tickets_filter"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 772
    invoke-static {v1}, Lcom/squareup/tickets/SqliteTicketStore;->ticketGroupFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ticket_group_filter"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v2, 0x0

    .line 773
    invoke-static {v2}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "custom_ticket_filter"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget-object v2, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    .line 774
    invoke-static {v2}, Lcom/squareup/tickets/SqliteTicketStore;->getOrderBy(Lcom/squareup/tickets/TicketSort;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "order_by"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 775
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 776
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 770
    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    .line 777
    new-instance v0, Lcom/squareup/tickets/TicketRowCursorList;

    new-instance v1, Lcom/squareup/tickets/TicketRowCursor;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/TicketRowCursor;-><init>(Landroid/database/Cursor;)V

    sget-object p0, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    invoke-direct {v0, v1, p0}, Lcom/squareup/tickets/TicketRowCursorList;-><init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;)V

    return-object v0
.end method

.method private static getCursorEntryCount(Landroid/database/Cursor;)I
    .locals 2

    .line 782
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 783
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 785
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return v1
.end method

.method private static getOrderBy(Lcom/squareup/tickets/TicketSort;)Ljava/lang/String;
    .locals 3

    .line 791
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    invoke-virtual {p0}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 812
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Woah, what kinda sort style is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, "??"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 808
    :pswitch_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Must specify sort style, TicketSort "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/squareup/tickets/TicketSort;->UNORDERED:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " not allowed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_1
    const-string p0, "TICKET_EMPLOYEE_NAME COLLATE NOCASE ASC"

    return-object p0

    :pswitch_2
    const-string p0, "TICKET_AMOUNT DESC"

    return-object p0

    :pswitch_3
    const-string p0, "TICKET_UPDATED ASC"

    return-object p0

    :pswitch_4
    const-string p0, "TICKET_UPDATED DESC"

    return-object p0

    :pswitch_5
    const-string p0, "TICKET_NAME ASC"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static nullToStringNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-nez p0, :cond_0

    const-string p0, "NULL"

    :cond_0
    return-object p0
.end method

.method private populateNewColumns(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .line 687
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->getAllNonclosedTickets(Landroid/database/sqlite/SQLiteDatabase;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v0

    .line 688
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 690
    :try_start_0
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 691
    invoke-interface {v1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/tickets/SqliteTicketStore;->buildTicketContentValues(Lcom/squareup/tickets/OpenTicket;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "TICKETS_TABLE"

    const/4 v3, 0x0

    .line 692
    invoke-virtual {p1, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 694
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 696
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 697
    throw v0
.end method

.method private static ticketGroupFilter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 845
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->TICKET_GROUP_FILTER:Lcom/squareup/phrase/Phrase;

    const-string v1, "group_id"

    .line 846
    invoke-virtual {v0, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 847
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 848
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method


# virtual methods
.method public addTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    .line 581
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 582
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 584
    :try_start_0
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore;->buildTicketContentValues(Lcom/squareup/tickets/OpenTicket;)Landroid/content/ContentValues;

    move-result-object p1

    const-string v1, "TICKETS_TABLE"

    const/4 v2, 0x0

    .line 585
    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 586
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 588
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 589
    throw p1
.end method

.method public debugWipeStore()V
    .locals 2

    .line 644
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DROP INDEX IF EXISTS idx_ticket_name"

    .line 645
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS idx_ticket_amount"

    .line 646
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS idx_ticket_recent"

    .line 647
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS idx_ticket_group_id"

    .line 648
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS idx_ticket_employee_name"

    .line 649
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP TABLE IF EXISTS TICKETS_TABLE"

    .line 650
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 651
    invoke-virtual {p0, v0}, Lcom/squareup/tickets/SqliteTicketStore;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public deleteTickets(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 612
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 613
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, ", "

    .line 617
    invoke-static {p1, v2}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const-string v3, "[Sqlite_Delete_Tickets] Deleting the following IDs: %s"

    .line 616
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->DELETE_TICKETS:Lcom/squareup/phrase/Phrase;

    array-length v3, p1

    const-string v5, "?"

    .line 622
    invoke-static {v5, v2, v3}, Lcom/squareup/util/Strings;->joinRepeated(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "id_list"

    invoke-virtual {v1, v5, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 623
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 624
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 628
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 633
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "TICKETS_TABLE"

    aput-object v6, v5, v4

    aput-object v1, v5, v0

    const/4 v7, 0x2

    .line 635
    invoke-static {p1, v2}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    const-string v2, "[Sqlite_Delete_Tickets] DELETE FROM %s WHERE %s (%s)"

    .line 634
    invoke-static {v2, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 637
    invoke-virtual {v3, v6, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p1

    new-array v0, v0, [Ljava/lang/Object;

    .line 638
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    const-string p1, "[Sqlite_Delete_Tickets] %d rows deleted"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 629
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid WHERE string, would delete all rows: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public getAllDeletableTicketsOldestFirst()Lcom/squareup/util/ReadOnlyCursorList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;"
        }
    .end annotation

    .line 606
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 607
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->SYNCED_CLOSED_TICKETS_OLDEST_FIRST_CURSOR:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 608
    new-instance v1, Lcom/squareup/tickets/TicketRowCursor;

    invoke-direct {v1, v0}, Lcom/squareup/tickets/TicketRowCursor;-><init>(Landroid/database/Cursor;)V

    return-object v1
.end method

.method public getCustomTicketCount(Ljava/lang/String;)I
    .locals 3

    .line 459
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 460
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_OPEN:Lcom/squareup/phrase/Phrase;

    .line 461
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->employeeTokenFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "employee_token_filter"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const/4 v1, 0x1

    .line 462
    invoke-static {v1}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "custom_ticket_filter"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 463
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 464
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 460
    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 465
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->getCursorEntryCount(Landroid/database/Cursor;)I

    move-result p1

    return p1
.end method

.method public getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    .line 500
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tickets/SqliteTicketStore;->doGetTicketList(Ljava/util/List;Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method

.method public getGroupTicketCounts(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 469
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 470
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_OPEN_BY_GROUP:Lcom/squareup/phrase/Phrase;

    .line 471
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->employeeTokenFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "employee_token_filter"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 472
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 473
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 470
    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 475
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 477
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 480
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 481
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    const/4 v0, 0x0

    .line 483
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 486
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method public getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 8

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 509
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tickets/SqliteTicketStore;->doGetTicketList(Ljava/util/List;Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1

    .line 507
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "GroupId cannot be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getNonClosedTicketsUnordered()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 549
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 550
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONCLOSED_TICKETS_UNORDERED:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const-string v1, "DATA"

    .line 551
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 553
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 554
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 556
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 557
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/tickets/OpenTicket;->protoFromByteArray([B)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v3

    .line 558
    new-instance v4, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;-><init>()V

    iget-object v5, v3, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 559
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;

    move-result-object v4

    .line 560
    invoke-static {v3}, Lcom/squareup/tickets/VectorClocks;->getClientlessVectorClock(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;

    move-result-object v3

    .line 561
    invoke-virtual {v3}, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->build()Lcom/squareup/protos/client/tickets/v2/TicketInfo;

    move-result-object v3

    .line 562
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 565
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method public getNonZeroClientClockTickets()Lcom/squareup/tickets/TicketRowCursorList;
    .locals 3

    .line 542
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 543
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->ALL_NONZERO_TICKETS_UNORDERED:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 544
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 545
    new-instance v1, Lcom/squareup/tickets/TicketRowCursorList;

    new-instance v2, Lcom/squareup/tickets/TicketRowCursor;

    invoke-direct {v2, v0}, Lcom/squareup/tickets/TicketRowCursor;-><init>(Landroid/database/Cursor;)V

    sget-object v0, Lcom/squareup/tickets/TicketSort;->UNORDERED:Lcom/squareup/tickets/TicketSort;

    invoke-direct {v1, v2, v0}, Lcom/squareup/tickets/TicketRowCursorList;-><init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;)V

    return-object v1
.end method

.method public getTicketCount(Ljava/lang/String;)I
    .locals 3

    .line 449
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 450
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->COUNT_OPEN:Lcom/squareup/phrase/Phrase;

    .line 451
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->employeeTokenFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "employee_token_filter"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const/4 v1, 0x0

    .line 452
    invoke-static {v1}, Lcom/squareup/tickets/SqliteTicketStore;->customTicketFilter(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "custom_ticket_filter"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 453
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 454
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 450
    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 455
    invoke-static {p1}, Lcom/squareup/tickets/SqliteTicketStore;->getCursorEntryCount(Landroid/database/Cursor;)I

    move-result p1

    return p1
.end method

.method public getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/TicketRowCursorList;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 493
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tickets/SqliteTicketStore;->doGetTicketList(Ljava/util/List;Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .line 424
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 425
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_NAME_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 426
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_AMOUNT_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 427
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_RECENT_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 430
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_EMPLOYEE_NAME_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 433
    sget-object v0, Lcom/squareup/tickets/SqliteTicketStore;->CREATE_TICKET_GROUP_ID_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq p2, v2, :cond_1

    if-ne p2, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    new-array v4, v1, [Ljava/lang/Object;

    .line 438
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v4, v2

    const-string p3, "Unable to handle upgrade from version %d to version %d"

    .line 437
    invoke-static {v3, p3, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-ge p2, v1, :cond_2

    .line 440
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore;->doMigrateFromV1toV2(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    const/4 p3, 0x3

    if-ge p2, p3, :cond_3

    .line 443
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore;->doMigrateFromV2toV3(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 444
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore;->populateNewColumns(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_3
    return-void
.end method

.method public retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;
    .locals 3

    .line 570
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 571
    sget-object v1, Lcom/squareup/tickets/SqliteTicketStore;->RETRIEVE:Lcom/squareup/phrase/Phrase;

    const-string v2, "retrieve_id"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 572
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    .line 575
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/tickets/OpenTicket;->protoFromByteArray([B)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    .line 576
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 577
    invoke-static {v0}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    return-object p1
.end method

.method public updateTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 3

    .line 593
    invoke-virtual {p0}, Lcom/squareup/tickets/SqliteTicketStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 594
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 596
    :try_start_0
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore;->buildTicketContentValues(Lcom/squareup/tickets/OpenTicket;)Landroid/content/ContentValues;

    move-result-object p1

    const-string v1, "TICKETS_TABLE"

    const/4 v2, 0x0

    .line 597
    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 598
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 601
    throw p1
.end method
