.class public final Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;
.super Ljava/lang/Object;
.source "VoidCompSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p5, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;)",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;"
        }
    .end annotation

    .line 42
    new-instance v6, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljavax/inject/Provider;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;)Lcom/squareup/tickets/voidcomp/VoidCompSettings;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ")",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljavax/inject/Provider;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/voidcomp/VoidCompSettings;
    .locals 5

    .line 36
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/OpenTicketEnabledSetting;

    iget-object v2, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    iget-object v4, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljavax/inject/Provider;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;)Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings_Factory;->get()Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-result-object v0

    return-object v0
.end method
