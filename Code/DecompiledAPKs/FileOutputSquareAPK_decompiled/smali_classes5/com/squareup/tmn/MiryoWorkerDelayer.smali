.class public interface abstract Lcom/squareup/tmn/MiryoWorkerDelayer;
.super Ljava/lang/Object;
.source "MiryoWorkerDelayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;,
        Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008f\u0018\u00002\u00020\u0001:\u0002\u0010\u0011J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\u0007H&J\u0008\u0010\t\u001a\u00020\u0007H&J\u0008\u0010\n\u001a\u00020\u0007H&J\u0008\u0010\u000b\u001a\u00020\u0007H&J\u0010\u0010\u000c\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u0003H&J\u0010\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u0005H&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/tmn/MiryoWorkerDelayer;",
        "",
        "getDelay",
        "",
        "getMiryoType",
        "Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;",
        "maybeDelayAfterFirstRoundTripAfterMiryo",
        "",
        "maybeDelayAfterWriteNotify",
        "maybeDelayBeforeWriteNotify",
        "miryoStarted",
        "reset",
        "setDelay",
        "delayMs",
        "setMiryoType",
        "miryoType",
        "MiryoDelayType",
        "MiryoPhase",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDelay()J
.end method

.method public abstract getMiryoType()Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;
.end method

.method public abstract maybeDelayAfterFirstRoundTripAfterMiryo()V
.end method

.method public abstract maybeDelayAfterWriteNotify()V
.end method

.method public abstract maybeDelayBeforeWriteNotify()V
.end method

.method public abstract miryoStarted()V
.end method

.method public abstract reset()V
.end method

.method public abstract setDelay(J)V
.end method

.method public abstract setMiryoType(Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;)V
.end method
