.class public final synthetic Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->values()[Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_QP_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_ID_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_SUICA_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_SUICA_SUCCESS_2:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_FAILURE:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v5, 0x5

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_RE_TOUCH_LOOP:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v5, 0x6

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->values()[Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
