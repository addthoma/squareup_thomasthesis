.class public final Lcom/squareup/tmn/logging/TmnEvents;
.super Ljava/lang/Object;
.source "TmnEvents.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/logging/TmnEvents$TmnStateTransitionEvent;,
        Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;,
        Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/tmn/logging/TmnEvents;",
        "",
        "()V",
        "TmnCommunicationEvent",
        "TmnEventValue",
        "TmnStateTransitionEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tmn/logging/TmnEvents;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/tmn/logging/TmnEvents;

    invoke-direct {v0}, Lcom/squareup/tmn/logging/TmnEvents;-><init>()V

    sput-object v0, Lcom/squareup/tmn/logging/TmnEvents;->INSTANCE:Lcom/squareup/tmn/logging/TmnEvents;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
