.class public final Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory;
.super Ljava/lang/Object;
.source "ProdMiryoWorkerDelayer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tmn/ProdMiryoWorkerDelayer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory$InstanceHolder;->access$000()Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/tmn/ProdMiryoWorkerDelayer;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/tmn/ProdMiryoWorkerDelayer;

    invoke-direct {v0}, Lcom/squareup/tmn/ProdMiryoWorkerDelayer;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tmn/ProdMiryoWorkerDelayer;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory;->newInstance()Lcom/squareup/tmn/ProdMiryoWorkerDelayer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/tmn/ProdMiryoWorkerDelayer_Factory;->get()Lcom/squareup/tmn/ProdMiryoWorkerDelayer;

    move-result-object v0

    return-object v0
.end method
