.class public final Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;
.super Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;
.source "RealTmnTransactionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendingProxyMessageToServer"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J?\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0011\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "packetData",
        "",
        "transactionId",
        "",
        "cardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "afterWriteNotifyData",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "connId",
        "([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V",
        "getAfterWriteNotifyData",
        "()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "getCardReader",
        "()Lcom/squareup/cardreader/CardReader;",
        "getConnId",
        "()Ljava/lang/String;",
        "getPacketData",
        "()[B",
        "getTransactionId",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

.field private final cardReader:Lcom/squareup/cardreader/CardReader;

.field private final connId:Ljava/lang/String;

.field private final packetData:[B

.field private final transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V
    .locals 1

    const-string v0, "packetData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReader"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 767
    invoke-direct {p0, v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    iput-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    iput-object p5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x8

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 765
    move-object p4, v0

    check-cast p4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    :cond_0
    move-object v5, p4

    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    .line 766
    move-object p5, v0

    check-cast p5, Ljava/lang/String;

    :cond_1
    move-object v6, p5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;-><init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;[BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->copy([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/cardreader/CardReader;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object v0
.end method

.method public final component4()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;
    .locals 7

    const-string v0, "packetData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReader"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;-><init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;
    .locals 1

    .line 765
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    return-object v0
.end method

.method public final getCardReader()Lcom/squareup/cardreader/CardReader;
    .locals 1

    .line 764
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object v0
.end method

.method public final getConnId()Ljava/lang/String;
    .locals 1

    .line 766
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPacketData()[B
    .locals 1

    .line 762
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    return-object v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    .line 763
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendingProxyMessageToServer(packetData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->packetData:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardReader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", afterWriteNotifyData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", connId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->connId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
