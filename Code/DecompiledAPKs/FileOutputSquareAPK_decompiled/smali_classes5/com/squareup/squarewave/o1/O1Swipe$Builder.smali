.class public final Lcom/squareup/squarewave/o1/O1Swipe$Builder;
.super Ljava/lang/Object;
.source "O1Swipe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/o1/O1Swipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private derivative:[S

.field private readings:[Lcom/squareup/squarewave/gen2/Reading;

.field private signal:Lcom/squareup/squarewave/Signal;

.field private threshold:Lcom/squareup/squarewave/o1/Threshold;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/squarewave/o1/O1Swipe;
    .locals 7

    .line 127
    new-instance v6, Lcom/squareup/squarewave/o1/O1Swipe;

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    iget-object v2, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->derivative:[S

    iget-object v3, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    iget-object v4, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/squarewave/o1/O1Swipe;-><init>(Lcom/squareup/squarewave/Signal;[SLcom/squareup/squarewave/o1/Threshold;[Lcom/squareup/squarewave/gen2/Reading;Lcom/squareup/squarewave/o1/O1Swipe$1;)V

    return-object v6
.end method

.method public setDerivative([S)Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->derivative:[S

    return-object p0
.end method

.method public setReadings([Lcom/squareup/squarewave/gen2/Reading;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    return-object p0
.end method

.method public setSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    return-object p0
.end method

.method public setThreshold(Lcom/squareup/squarewave/o1/Threshold;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    return-object p0
.end method
