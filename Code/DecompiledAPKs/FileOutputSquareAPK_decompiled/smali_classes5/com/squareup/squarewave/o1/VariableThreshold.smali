.class public Lcom/squareup/squarewave/o1/VariableThreshold;
.super Ljava/lang/Object;
.source "VariableThreshold.java"

# interfaces
.implements Lcom/squareup/squarewave/o1/Threshold;


# static fields
.field private static final NUM_WINDOWS:I = 0x14


# instance fields
.field private divider:I

.field private sortedSamples:[S

.field private final thresholds:[S

.field private final windowSize:I


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/o1/O1Swipe;)V
    .locals 5

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getDerivative()[S

    move-result-object p1

    .line 20
    array-length v0, p1

    const/16 v1, 0x14

    .line 22
    div-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->windowSize:I

    new-array v0, v1, [S

    .line 23
    iput-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->thresholds:[S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 26
    iget v2, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->windowSize:I

    mul-int v3, v0, v2

    .line 27
    array-length v4, p1

    add-int/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ne v3, v2, :cond_0

    goto :goto_1

    .line 30
    :cond_0
    iget-object v4, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->thresholds:[S

    invoke-direct {p0, p1, v3, v2}, Lcom/squareup/squarewave/o1/VariableThreshold;->computeThreshold([SII)S

    move-result v2

    aput-short v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private averageLeft()S
    .locals 6

    .line 92
    iget v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    aget-short v0, v0, v1

    return v0

    :cond_0
    const-wide/16 v2, 0x0

    .line 95
    :goto_0
    iget v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    aget-short v0, v0, v1

    int-to-long v4, v0

    add-long/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    int-to-long v0, v0

    .line 96
    div-long/2addr v2, v0

    long-to-int v0, v2

    int-to-short v0, v0

    return v0
.end method

.method private averageRight()S
    .locals 5

    .line 101
    iget v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    const-wide/16 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    array-length v4, v3

    if-ge v0, v4, :cond_0

    aget-short v3, v3, v0

    int-to-long v3, v3

    add-long/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    array-length v0, v3

    iget v3, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    sub-int/2addr v0, v3

    int-to-long v3, v0

    div-long/2addr v1, v3

    long-to-int v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private computeThreshold([SII)S
    .locals 2

    add-int/lit8 v0, p3, -0x1

    if-ne p2, v0, :cond_0

    .line 42
    aget-short p1, p1, p2

    return p1

    :cond_0
    sub-int/2addr p3, p2

    .line 44
    new-array v0, p3, [S

    iput-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    .line 45
    iget-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    iget-object p1, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    invoke-static {p1}, Ljava/util/Arrays;->sort([S)V

    .line 48
    iget-object p1, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    array-length p1, p1

    shr-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    const p1, 0x7fffffff

    .line 52
    invoke-direct {p0}, Lcom/squareup/squarewave/o1/VariableThreshold;->averageLeft()S

    move-result p2

    .line 53
    invoke-direct {p0}, Lcom/squareup/squarewave/o1/VariableThreshold;->averageRight()S

    move-result p3

    :goto_0
    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 56
    iget p1, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    add-int/2addr p2, p3

    shr-int/lit8 p2, p2, 0x1

    int-to-short p2, p2

    .line 59
    invoke-direct {p0, p2}, Lcom/squareup/squarewave/o1/VariableThreshold;->indexAt(S)I

    move-result p2

    iput p2, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    .line 60
    iget p2, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->divider:I

    sub-int/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p1

    .line 62
    invoke-direct {p0}, Lcom/squareup/squarewave/o1/VariableThreshold;->averageLeft()S

    move-result p2

    .line 63
    invoke-direct {p0}, Lcom/squareup/squarewave/o1/VariableThreshold;->averageRight()S

    move-result p3

    goto :goto_0

    :cond_1
    add-int/2addr p2, p3

    shr-int/lit8 p1, p2, 0x1

    int-to-short p1, p1

    return p1
.end method

.method private indexAt(S)I
    .locals 5

    .line 75
    iget-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v0, v1, :cond_1

    add-int v2, v1, v0

    shr-int/lit8 v2, v2, 0x1

    .line 80
    iget-object v3, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->sortedSamples:[S

    aget-short v4, v3, v2

    if-ge v4, p1, :cond_0

    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 82
    :cond_0
    aget-short v0, v3, v2

    if-le v0, p1, :cond_1

    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    :cond_1
    return v2
.end method


# virtual methods
.method public getNumWindows()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public getValue(I)S
    .locals 2

    .line 106
    iget v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->windowSize:I

    div-int/2addr p1, v0

    .line 107
    iget-object v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->thresholds:[S

    array-length v1, v0

    if-lt p1, v1, :cond_0

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    aget-short p1, v0, p1

    return p1
.end method

.method public getWindowSize()I
    .locals 1

    .line 115
    iget v0, p0, Lcom/squareup/squarewave/o1/VariableThreshold;->windowSize:I

    return v0
.end method
