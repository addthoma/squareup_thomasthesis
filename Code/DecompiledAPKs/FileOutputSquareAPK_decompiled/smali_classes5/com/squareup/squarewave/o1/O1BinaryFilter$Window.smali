.class public final Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;
.super Ljava/lang/Object;
.source "O1BinaryFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/o1/O1BinaryFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "Window"
.end annotation


# instance fields
.field private final end:I

.field private final halfway:I

.field private final start:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lt p2, p1, :cond_0

    .line 253
    iput p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->start:I

    .line 254
    iput p2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->end:I

    shr-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v0, v1

    and-int/lit8 p1, p1, 0x1

    and-int/2addr p1, p2

    add-int/2addr v0, p1

    .line 257
    iput v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->halfway:I

    return-void

    .line 252
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "end < start"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic access$100(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;)I
    .locals 0

    .line 246
    iget p0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->halfway:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;)I
    .locals 0

    .line 246
    iget p0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->start:I

    return p0
.end method


# virtual methods
.method public centerOn(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;
    .locals 1

    .line 279
    iget v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->halfway:I

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->translate(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    move-result-object p1

    return-object p1
.end method

.method public getEnd()I
    .locals 1

    .line 265
    iget v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->end:I

    return v0
.end method

.method public getHalfway()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->halfway:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .line 261
    iget v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->start:I

    return v0
.end method

.method public translate(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;
    .locals 3

    .line 274
    new-instance v0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    iget v1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->start:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->end:I

    add-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;-><init>(II)V

    return-object v0
.end method

.method public within(II)Z
    .locals 1

    .line 284
    iget v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->start:I

    if-lt v0, p1, :cond_0

    iget p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->end:I

    if-gt p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
