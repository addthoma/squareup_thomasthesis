.class public final Lcom/squareup/squarewave/Signal$Builder;
.super Ljava/lang/Object;
.source "Signal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/Signal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

.field private sampleRate:I

.field private samples:[S

.field private final signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

.field private triggersError:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/squarewave/Signal$Builder;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    .line 120
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/squarewave/Signal$Builder;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 121
    iget-object v0, p0, Lcom/squareup/squarewave/Signal$Builder;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/squarewave/Signal;)V
    .locals 1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-static {p1}, Lcom/squareup/squarewave/Signal;->access$500(Lcom/squareup/squarewave/Signal;)[S

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/Signal$Builder;->samples:[S

    .line 126
    invoke-static {p1}, Lcom/squareup/squarewave/Signal;->access$600(Lcom/squareup/squarewave/Signal;)I

    move-result v0

    iput v0, p0, Lcom/squareup/squarewave/Signal$Builder;->sampleRate:I

    .line 127
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iput-object v0, p0, Lcom/squareup/squarewave/Signal$Builder;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 128
    invoke-static {p1}, Lcom/squareup/squarewave/Signal;->access$700(Lcom/squareup/squarewave/Signal;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/squarewave/Signal$Builder;->triggersError:Z

    .line 129
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    iput-object p1, p0, Lcom/squareup/squarewave/Signal$Builder;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/squarewave/Signal$Builder;)[S
    .locals 0

    .line 110
    iget-object p0, p0, Lcom/squareup/squarewave/Signal$Builder;->samples:[S

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/squarewave/Signal$Builder;)I
    .locals 0

    .line 110
    iget p0, p0, Lcom/squareup/squarewave/Signal$Builder;->sampleRate:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/squarewave/Signal$Builder;)Z
    .locals 0

    .line 110
    iget-boolean p0, p0, Lcom/squareup/squarewave/Signal$Builder;->triggersError:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/squarewave/Signal$Builder;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    .locals 0

    .line 110
    iget-object p0, p0, Lcom/squareup/squarewave/Signal$Builder;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/squarewave/Signal$Builder;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 0

    .line 110
    iget-object p0, p0, Lcom/squareup/squarewave/Signal$Builder;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/squarewave/Signal;
    .locals 2

    .line 133
    new-instance v0, Lcom/squareup/squarewave/Signal;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/squarewave/Signal;-><init>(Lcom/squareup/squarewave/Signal$Builder;Lcom/squareup/squarewave/Signal$1;)V

    return-object v0
.end method

.method public sampleRate(I)Lcom/squareup/squarewave/Signal$Builder;
    .locals 0

    .line 143
    iput p1, p0, Lcom/squareup/squarewave/Signal$Builder;->sampleRate:I

    return-object p0
.end method

.method public samples([S)Lcom/squareup/squarewave/Signal$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/squarewave/Signal$Builder;->samples:[S

    return-object p0
.end method

.method public triggersError(Z)Lcom/squareup/squarewave/Signal$Builder;
    .locals 0

    .line 152
    iput-boolean p1, p0, Lcom/squareup/squarewave/Signal$Builder;->triggersError:Z

    return-object p0
.end method
