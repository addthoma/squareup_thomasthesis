.class public final Lcom/squareup/squarewave/decode/DemodResult;
.super Ljava/lang/Object;
.source "DemodResult.java"


# static fields
.field private static final ACTIVELY_IGNORE_SIGNAL:Lcom/squareup/squarewave/decode/DemodResult;

.field private static final FAILED_DEMOD:Lcom/squareup/squarewave/decode/DemodResult;

.field private static final PROBABLY_NOISE:Lcom/squareup/squarewave/decode/DemodResult;


# instance fields
.field private final card:Lcom/squareup/Card;

.field private final hasTrack1Data:Z

.field private final hasTrack2Data:Z

.field private final isActivelyIgnored:Z

.field private final probablyNoise:Z

.field private final successfulDemod:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 16
    new-instance v7, Lcom/squareup/squarewave/decode/DemodResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/squarewave/decode/DemodResult;-><init>(ZZZLcom/squareup/Card;ZZ)V

    sput-object v7, Lcom/squareup/squarewave/decode/DemodResult;->FAILED_DEMOD:Lcom/squareup/squarewave/decode/DemodResult;

    .line 18
    new-instance v0, Lcom/squareup/squarewave/decode/DemodResult;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/squarewave/decode/DemodResult;-><init>(ZZZLcom/squareup/Card;ZZ)V

    sput-object v0, Lcom/squareup/squarewave/decode/DemodResult;->PROBABLY_NOISE:Lcom/squareup/squarewave/decode/DemodResult;

    .line 20
    new-instance v0, Lcom/squareup/squarewave/decode/DemodResult;

    const/4 v2, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/squarewave/decode/DemodResult;-><init>(ZZZLcom/squareup/Card;ZZ)V

    sput-object v0, Lcom/squareup/squarewave/decode/DemodResult;->ACTIVELY_IGNORE_SIGNAL:Lcom/squareup/squarewave/decode/DemodResult;

    return-void
.end method

.method private constructor <init>(ZZZLcom/squareup/Card;ZZ)V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean p1, p0, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod:Z

    .line 57
    iput-boolean p2, p0, Lcom/squareup/squarewave/decode/DemodResult;->probablyNoise:Z

    .line 58
    iput-boolean p3, p0, Lcom/squareup/squarewave/decode/DemodResult;->isActivelyIgnored:Z

    .line 59
    iput-object p4, p0, Lcom/squareup/squarewave/decode/DemodResult;->card:Lcom/squareup/Card;

    .line 60
    iput-boolean p5, p0, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack1Data:Z

    .line 61
    iput-boolean p6, p0, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack2Data:Z

    return-void
.end method

.method public static activelyIgnoredSignal()Lcom/squareup/squarewave/decode/DemodResult;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/squarewave/decode/DemodResult;->ACTIVELY_IGNORE_SIGNAL:Lcom/squareup/squarewave/decode/DemodResult;

    return-object v0
.end method

.method public static failedDemod()Lcom/squareup/squarewave/decode/DemodResult;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/squarewave/decode/DemodResult;->FAILED_DEMOD:Lcom/squareup/squarewave/decode/DemodResult;

    return-object v0
.end method

.method public static probablyNoise()Lcom/squareup/squarewave/decode/DemodResult;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/squarewave/decode/DemodResult;->PROBABLY_NOISE:Lcom/squareup/squarewave/decode/DemodResult;

    return-object v0
.end method

.method public static successfulDemod()Lcom/squareup/squarewave/decode/DemodResult;
    .locals 8

    .line 24
    new-instance v7, Lcom/squareup/squarewave/decode/DemodResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/squarewave/decode/DemodResult;-><init>(ZZZLcom/squareup/Card;ZZ)V

    return-object v7
.end method

.method public static successfulDemodWithCard(Lcom/squareup/Card;ZZ)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 8

    if-eqz p0, :cond_0

    .line 32
    new-instance v7, Lcom/squareup/squarewave/decode/DemodResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, v7

    move-object v4, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/squarewave/decode/DemodResult;-><init>(ZZZLcom/squareup/Card;ZZ)V

    return-object v7

    .line 30
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "card should not be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getCard()Lcom/squareup/Card;
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->card:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    return-object v0

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "card is null, please check hasCard() before calling getCard()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasCard()Z
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->card:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTrack1Data()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack1Data:Z

    return v0
.end method

.method public hasTrack2Data()Z
    .locals 1

    .line 97
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->hasTrack2Data:Z

    return v0
.end method

.method public isActivelyIgnored()Z
    .locals 2

    .line 69
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod:Z

    if-eqz v0, :cond_0

    .line 75
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->isActivelyIgnored:Z

    return v0

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The demod was NOT successful, you should not worry about this being ignored. Please check isSuccessfulDemod()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isProbablyNoise()Z
    .locals 2

    .line 79
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod:Z

    if-nez v0, :cond_0

    .line 85
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->probablyNoise:Z

    return v0

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The demod was successful, you should not worry about this being noise. Please check isSuccessfulDemod()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isSuccessfulDemod()Z
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 109
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod:Z

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->card:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    const-string v0, "SUCCESSFUL_SWIPE"

    return-object v0

    :cond_0
    const-string v0, "SUCCESSFUL_DEMOD"

    return-object v0

    .line 116
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/squarewave/decode/DemodResult;->probablyNoise:Z

    if-eqz v0, :cond_2

    const-string v0, "PROBABLY_NOISE"

    return-object v0

    :cond_2
    const-string v0, "FAILED_DEMOD"

    return-object v0
.end method
