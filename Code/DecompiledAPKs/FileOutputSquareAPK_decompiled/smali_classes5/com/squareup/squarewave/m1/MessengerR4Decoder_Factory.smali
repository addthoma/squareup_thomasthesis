.class public final Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;
.super Ljava/lang/Object;
.source "MessengerR4Decoder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final messengerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;->messengerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
            ">;)",
            "Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/SingleCardreaderMessenger;)Lcom/squareup/squarewave/m1/MessengerR4Decoder;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/squarewave/m1/MessengerR4Decoder;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder;-><init>(Lcom/squareup/cardreader/SingleCardreaderMessenger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/m1/MessengerR4Decoder;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;->messengerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SingleCardreaderMessenger;

    invoke-static {v0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;->newInstance(Lcom/squareup/cardreader/SingleCardreaderMessenger;)Lcom/squareup/squarewave/m1/MessengerR4Decoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/squarewave/m1/MessengerR4Decoder_Factory;->get()Lcom/squareup/squarewave/m1/MessengerR4Decoder;

    move-result-object v0

    return-object v0
.end method
