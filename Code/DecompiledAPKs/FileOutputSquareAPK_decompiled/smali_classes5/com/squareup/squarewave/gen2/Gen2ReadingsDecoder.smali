.class public interface abstract Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;
.super Ljava/lang/Object;
.source "Gen2ReadingsDecoder.java"


# virtual methods
.method public abstract createCard([C)Lcom/squareup/Card;
.end method

.method public abstract decode([Lcom/squareup/squarewave/gen2/Reading;Lcom/squareup/squarewave/gen2/CardDataStart;Ljava/util/List;Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/squarewave/gen2/Reading;",
            "Lcom/squareup/squarewave/gen2/CardDataStart;",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/CharacterReading;",
            ">;",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe;",
            ")",
            "Lcom/squareup/squarewave/gen2/Gen2DemodResult;"
        }
    .end annotation
.end method

.method public abstract startSentinelIndex(Ljava/lang/String;)I
.end method
