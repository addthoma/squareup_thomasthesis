.class public Lcom/squareup/squarewave/gen2/CharacterReading;
.super Ljava/lang/Object;
.source "CharacterReading.java"


# instance fields
.field private final cardCharacter:Lcom/squareup/squarewave/gen2/CardCharacter;

.field private final fromIndex:I

.field private final readings:[Lcom/squareup/squarewave/gen2/Reading;

.field private final toIndex:I


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/CardCharacter;[Lcom/squareup/squarewave/gen2/Reading;II)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->cardCharacter:Lcom/squareup/squarewave/gen2/CardCharacter;

    .line 27
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    .line 28
    iput p3, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->fromIndex:I

    .line 29
    iput p4, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->toIndex:I

    return-void
.end method


# virtual methods
.method public getCharacter()C
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->cardCharacter:Lcom/squareup/squarewave/gen2/CardCharacter;

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/CardCharacter;->getCharacter()C

    move-result v0

    return v0
.end method

.method public getFirstReading()Lcom/squareup/squarewave/gen2/Reading;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    iget v1, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->fromIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLastReading()Lcom/squareup/squarewave/gen2/Reading;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    iget v1, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->toIndex:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getType()Lcom/squareup/squarewave/gen2/CardCharacter$Type;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->cardCharacter:Lcom/squareup/squarewave/gen2/CardCharacter;

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/CardCharacter;->getType()Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CharacterReading;->cardCharacter:Lcom/squareup/squarewave/gen2/CardCharacter;

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/CardCharacter;->getCharacter()C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
