.class public Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;
.super Ljava/lang/Object;
.source "Gen2SignalDecoder.java"

# interfaces
.implements Lcom/squareup/squarewave/SignalDecoder;


# instance fields
.field private final gen2Pipelines:[Lcom/squareup/squarewave/gen2/Gen2Pipeline;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/squarewave/gen2/DecoderOptions;->getBest()[Lcom/squareup/squarewave/gen2/DecoderOptions;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;-><init>([Lcom/squareup/squarewave/gen2/DecoderOptions;)V

    return-void
.end method

.method public constructor <init>([Lcom/squareup/squarewave/gen2/DecoderOptions;)V
    .locals 3

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    .line 39
    array-length v0, p1

    new-array v0, v0, [Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->gen2Pipelines:[Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    const/4 v0, 0x0

    .line 40
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->gen2Pipelines:[Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->gen2Pipeline(Lcom/squareup/squarewave/gen2/DecoderOptions;)Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 36
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "optionsArray"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private addDemodInfo(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/gen2/Gen2DemodResult;I)V
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$Builder;-><init>()V

    .line 71
    iget-object p2, p2, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    iput-object p2, v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 72
    new-instance p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;-><init>()V

    .line 73
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    iput-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 74
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 75
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    .line 76
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private runAllPipelines(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 3

    const/4 v0, 0x0

    .line 61
    :goto_0
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->gen2Pipelines:[Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 62
    aget-object v1, v1, v0

    .line 63
    invoke-virtual {v1, p1}, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v1

    .line 64
    iget-object v2, v1, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v2}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 5

    .line 51
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->runAllPipelines(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v2

    .line 53
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr v3, v0

    long-to-int v0, v3

    .line 55
    invoke-direct {p0, p1, v2, v0}, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;->addDemodInfo(Lcom/squareup/squarewave/Signal;Lcom/squareup/squarewave/gen2/Gen2DemodResult;I)V

    .line 57
    iget-object p1, v2, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    return-object p1
.end method

.method gen2Pipeline(Lcom/squareup/squarewave/gen2/DecoderOptions;)Lcom/squareup/squarewave/gen2/Gen2Pipeline;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/squarewave/gen2/Gen2Pipeline;

    invoke-direct {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2Pipeline;-><init>(Lcom/squareup/squarewave/gen2/DecoderOptions;)V

    return-object v0
.end method
