.class public Lcom/squareup/squarewave/gen2/WindowPeakFilter;
.super Ljava/lang/Object;
.source "WindowPeakFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private final cutoffPercent:I

.field private lock:Ljava/util/concurrent/locks/Lock;

.field private final next:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

.field private final windowSize:I


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;II)V
    .locals 1

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->lock:Ljava/util/concurrent/locks/Lock;

    if-ltz p2, :cond_0

    const/16 v0, 0x3e8

    if-gt p2, v0, :cond_0

    .line 81
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->next:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    .line 82
    iput p2, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->windowSize:I

    .line 83
    iput p3, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->cutoffPercent:I

    return-void

    .line 79
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "windowSize"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private computeCutoff(S)S
    .locals 1

    .line 196
    iget v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->cutoffPercent:I

    mul-int p1, p1, v0

    div-int/lit8 p1, p1, 0x64

    int-to-short p1, p1

    return p1
.end method

.method public static computeTargetArea(II)I
    .locals 2

    int-to-long v0, p0

    int-to-long p0, p1

    mul-long v0, v0, p0

    const-wide/16 p0, 0x3e8

    .line 231
    div-long/2addr v0, p0

    long-to-int p0, v0

    return p0
.end method

.method public static measureArea([S)I
    .locals 4

    .line 240
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 241
    aget-short v3, p0, v1

    .line 242
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v2
.end method

.method private static recomputeMax(Ljava/util/Collection;)S
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;)S"
        }
    .end annotation

    .line 203
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/gen2/Peak;

    .line 205
    invoke-virtual {v1}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v1

    if-le v1, v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private static removePeaks([Lcom/squareup/squarewave/gen2/Peak;Ljava/util/Set;)[Lcom/squareup/squarewave/gen2/Peak;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/squarewave/gen2/Peak;",
            "Ljava/util/Set<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;)[",
            "Lcom/squareup/squarewave/gen2/Peak;"
        }
    .end annotation

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    .line 215
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    .line 216
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 217
    aget-object v2, p0, v1

    .line 218
    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 220
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [Lcom/squareup/squarewave/gen2/Peak;

    invoke-interface {v0, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/squarewave/gen2/Peak;

    return-object p0
.end method

.method private rescan(SLjava/util/ArrayDeque;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S",
            "Ljava/util/ArrayDeque<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;)V"
        }
    .end annotation

    .line 187
    invoke-virtual {p2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 188
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/gen2/Peak;

    .line 190
    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v1

    if-ge v1, p1, :cond_0

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private scan(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Ljava/util/Set;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;"
        }
    .end annotation

    .line 103
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->samples()[S

    move-result-object v0

    .line 106
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex()[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v1

    .line 109
    invoke-static {v0}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->measureArea([S)I

    move-result v2

    iget v3, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->windowSize:I

    invoke-static {v2, v3}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->computeTargetArea(II)I

    move-result v2

    .line 112
    new-instance v3, Ljava/util/ArrayDeque;

    invoke-direct {v3}, Ljava/util/ArrayDeque;-><init>()V

    .line 115
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 118
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks()[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object p1

    const/4 v5, 0x0

    aget-object p1, p1, v5

    .line 119
    invoke-virtual {v3, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v5

    .line 123
    iget p1, p1, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    .line 135
    invoke-direct {p0, v5}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->computeCutoff(S)S

    move-result v6

    .line 138
    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    move v8, v5

    move v9, v6

    move v6, p1

    :cond_0
    :goto_0
    if-ge p1, v7, :cond_5

    if-gt v5, v2, :cond_2

    add-int/lit8 p1, p1, 0x1

    .line 142
    aget-short v10, v0, p1

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    add-int/2addr v5, v10

    .line 145
    aget-object v10, v1, p1

    if-eqz v10, :cond_0

    .line 147
    invoke-virtual {v3, v10}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 149
    invoke-virtual {v10}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v11

    if-le v11, v8, :cond_1

    .line 153
    invoke-direct {p0, v11}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->computeCutoff(S)S

    move-result v8

    .line 154
    invoke-direct {p0, v8, v3, v4}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->rescan(SLjava/util/ArrayDeque;Ljava/util/Set;)V

    move v9, v8

    move v8, v11

    goto :goto_0

    :cond_1
    if-ge v11, v9, :cond_0

    .line 156
    invoke-interface {v4, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    :cond_2
    aget-short v10, v0, v6

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    sub-int/2addr v5, v10

    add-int/lit8 v10, v6, 0x1

    .line 164
    aget-object v6, v1, v6

    if-eqz v6, :cond_4

    .line 166
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 167
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    .line 170
    :cond_3
    invoke-virtual {v6}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v6

    if-ne v6, v8, :cond_4

    .line 171
    invoke-static {v3}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->recomputeMax(Ljava/util/Collection;)S

    move-result v6

    .line 172
    invoke-direct {p0, v6}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->computeCutoff(S)S

    move-result v8

    move v9, v8

    move v8, v6

    :cond_4
    move v6, v10

    goto :goto_0

    :cond_5
    return-object v4
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks()[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v0

    .line 90
    iget v1, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->windowSize:I

    if-lez v1, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->scan(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Ljava/util/Set;

    move-result-object v1

    .line 92
    invoke-static {v0, v1}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->removePeaks([Lcom/squareup/squarewave/gen2/Peak;Ljava/util/Set;)[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->withPeaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->next:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 97
    throw p1

    .line 87
    :cond_1
    new-instance p1, Ljava/lang/IllegalThreadStateException;

    const-string v0, "Could not obtain lock"

    invoke-direct {p1, v0}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
