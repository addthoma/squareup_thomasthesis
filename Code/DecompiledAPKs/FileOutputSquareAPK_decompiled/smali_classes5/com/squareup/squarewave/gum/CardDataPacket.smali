.class public final Lcom/squareup/squarewave/gum/CardDataPacket;
.super Ljava/lang/Object;
.source "CardDataPacket.java"


# instance fields
.field public final cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

.field public final cardDataEncrypted:Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

.field public final cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

.field public final fullPacket:Ljava/nio/ByteBuffer;

.field public final sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 2

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/squareup/squarewave/gum/SqLinkHeader;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/squarewave/gum/SqLinkHeader;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    .line 24
    iput-object p1, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->fullPacket:Ljava/nio/ByteBuffer;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    move-object p2, p1

    goto :goto_0

    .line 26
    :cond_0
    new-instance p2, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 27
    invoke-virtual {p3, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_0
    iput-object p2, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    if-nez p4, :cond_1

    move-object p2, p1

    goto :goto_1

    .line 29
    :cond_1
    new-instance p2, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    sget-object p3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 30
    invoke-virtual {p4, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_1
    iput-object p2, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    if-nez p5, :cond_2

    goto :goto_2

    .line 32
    :cond_2
    new-instance p1, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

    sget-object p2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 33
    invoke-virtual {p5, p2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_2
    iput-object p1, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataEncrypted:Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

    return-void
.end method


# virtual methods
.method public clearAllData()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->fullPacket:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 44
    :goto_0
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->fullPacket:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->fullPacket:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getPacketType()Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/SqLinkHeader;->packetType:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    return-object v0
.end method

.method public track1Data()Ljava/nio/ByteBuffer;
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/SqLinkHeader;->packetType:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    sget-object v1, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    if-ne v0, v1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataEncrypted:Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    .line 62
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 63
    iget-object v1, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v1, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 64
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Packet must have type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "to read track data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public track2Data()Ljava/nio/ByteBuffer;
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/SqLinkHeader;->packetType:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    sget-object v1, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    if-ne v0, v1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataEncrypted:Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v1, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v2, v2, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Len:I

    invoke-virtual {v1, v2}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 75
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Packet must have type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "to read track data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public track3Data()Ljava/nio/ByteBuffer;
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->sqLinkHeader:Lcom/squareup/squarewave/gum/SqLinkHeader;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/SqLinkHeader;->packetType:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    sget-object v1, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    if-ne v0, v1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataEncrypted:Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;

    iget-object v0, v0, Lcom/squareup/squarewave/gum/M1PacketCardDataEncrypted;->trackData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v1, v1, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    iget-object v2, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v2, v2, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Len:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/squarewave/gum/CardDataPacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    iget v2, v2, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Len:I

    .line 86
    invoke-virtual {v1, v2}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 87
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Packet must have type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "to read track data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
