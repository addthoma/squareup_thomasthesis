.class public final Lcom/squareup/sqldelight/EnumColumnAdapterKt;
.super Ljava/lang/Object;
.source "EnumColumnAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnumColumnAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnumColumnAdapter.kt\ncom/squareup/sqldelight/EnumColumnAdapterKt\n*L\n1#1,32:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0010\n\u0000\u001a!\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0010\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\u0008\u00a8\u0006\u0004"
    }
    d2 = {
        "EnumColumnAdapter",
        "Lcom/squareup/sqldelight/EnumColumnAdapter;",
        "T",
        "",
        "runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic EnumColumnAdapter()Lcom/squareup/sqldelight/EnumColumnAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>()",
            "Lcom/squareup/sqldelight/EnumColumnAdapter<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x5

    const-string v1, "T"

    .line 30
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Enum;

    new-instance v1, Lcom/squareup/sqldelight/EnumColumnAdapter;

    invoke-direct {v1, v0}, Lcom/squareup/sqldelight/EnumColumnAdapter;-><init>([Ljava/lang/Enum;)V

    return-object v1
.end method
