.class public final Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;
.super Ljava/lang/Object;
.source "RealSonicBrandingAudioPlayer.kt"

# interfaces
.implements Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;",
        "Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;",
        "sonicBrandingAudioPlayer",
        "Lcom/squareup/brandaudio/BrandAudioPlayer;",
        "(Lcom/squareup/brandaudio/BrandAudioPlayer;)V",
        "initialize",
        "",
        "playSonicBrandingAudio",
        "audioId",
        "Lcom/squareup/cardreader/lcr/CrAudioVisualId;",
        "shutDown",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$Companion;

.field private static final MASTERCARD_SUCCESS:Ljava/lang/String; = "mastercard_success.mp3"


# instance fields
.field private final sonicBrandingAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;->Companion:Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/brandaudio/BrandAudioPlayer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sonicBrandingAudioPlayer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;->sonicBrandingAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;->sonicBrandingAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/brandaudio/BrandAudioPlayer;->initialize(Z)V

    return-void
.end method

.method public playSonicBrandingAudio(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 4

    const-string v0, "audioId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SonicBranding playing audio "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    sget-object v0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 33
    new-instance v0, Lcom/squareup/sonicbranding/UnhandledAudioVisualId;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected SonicBranding ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/sonicbranding/UnhandledAudioVisualId;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 31
    :cond_0
    iget-object p1, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;->sonicBrandingAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const/4 v0, 0x2

    const/4 v2, 0x0

    const-string v3, "mastercard_success.mp3"

    invoke-static {p1, v3, v1, v0, v2}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public shutDown()V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;->sonicBrandingAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-interface {v0}, Lcom/squareup/brandaudio/BrandAudioPlayer;->shutDown()V

    return-void
.end method
