.class public interface abstract Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;
.super Ljava/lang/Object;
.source "SonicBrandingAudioPlayer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0003H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;",
        "",
        "initialize",
        "",
        "playSonicBrandingAudio",
        "audioId",
        "Lcom/squareup/cardreader/lcr/CrAudioVisualId;",
        "shutDown",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract initialize()V
.end method

.method public abstract playSonicBrandingAudio(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
.end method

.method public abstract shutDown()V
.end method
