.class public abstract Lcom/squareup/statusbar/workers/NoStatusBarWorkerModule;
.super Ljava/lang/Object;
.source "NoStatusBarWorkerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindHideStatusBarWorker(Lcom/squareup/statusbar/workers/NoHideStatusBarWorker;)Lcom/squareup/statusbar/workers/HideStatusBarWorker;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
