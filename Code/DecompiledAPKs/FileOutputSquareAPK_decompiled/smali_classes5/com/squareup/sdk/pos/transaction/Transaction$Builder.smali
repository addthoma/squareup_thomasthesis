.class public abstract Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.super Ljava/lang/Object;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;->autoTenders(Ljava/util/Set;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;

    return-void
.end method


# virtual methods
.method public addTender(Lcom/squareup/sdk/pos/transaction/Tender;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;->autoTenders()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method abstract autoBuild()Lcom/squareup/sdk/pos/transaction/Transaction;
.end method

.method abstract autoTenders(Ljava/util/Set;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;)",
            "Lcom/squareup/sdk/pos/transaction/Transaction$Builder;"
        }
    .end annotation
.end method

.method abstract autoTenders()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation
.end method

.method public build()Lcom/squareup/sdk/pos/transaction/Transaction;
    .locals 3

    .line 104
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;->autoBuild()Lcom/squareup/sdk/pos/transaction/Transaction;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Transaction;->toBuilder()Lcom/squareup/sdk/pos/transaction/Transaction$Builder;

    move-result-object v1

    new-instance v2, Ljava/util/LinkedHashSet;

    .line 106
    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Transaction;->tenders()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;->autoTenders(Ljava/util/Set;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;->autoBuild()Lcom/squareup/sdk/pos/transaction/Transaction;

    move-result-object v0

    return-object v0
.end method

.method public abstract clientId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method

.method public abstract createdAt(Lcom/squareup/sdk/pos/transaction/DateTime;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method

.method public abstract locationId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method

.method public abstract order(Lcom/squareup/sdk/pos/transaction/Order;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method

.method public abstract serverId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.end method
