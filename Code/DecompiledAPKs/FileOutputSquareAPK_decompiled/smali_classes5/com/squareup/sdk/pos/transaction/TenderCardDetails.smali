.class public abstract Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
.super Ljava/lang/Object;
.source "TenderCardDetails.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/TenderCardDetails$Builder;,
        Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/squareup/sdk/pos/transaction/TenderCardDetails$Builder;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCardDetails$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/TenderCardDetails;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCardDetails$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCardDetails$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract card()Lcom/squareup/sdk/pos/transaction/Card;
.end method

.method public abstract entryMethod()Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;
.end method
