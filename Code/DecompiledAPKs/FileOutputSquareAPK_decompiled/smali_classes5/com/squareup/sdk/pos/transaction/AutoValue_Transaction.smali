.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_Transaction;
.source "AutoValue_Transaction.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private static final TENDERS_TYPE_ADAPTER:Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->TENDERS_TYPE_ADAPTER:Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;

    .line 12
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Order;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/sdk/pos/transaction/DateTime;",
            "Lcom/squareup/sdk/pos/transaction/Order;",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct/range {p0 .. p6}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Transaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Order;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->TENDERS_TYPE_ADAPTER:Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->clientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->serverId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->serverId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->locationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->order()Lcom/squareup/sdk/pos/transaction/Order;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    sget-object p2, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->TENDERS_TYPE_ADAPTER:Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;

    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->autoTenders()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;->toParcel(Ljava/util/Set;Landroid/os/Parcel;)V

    return-void
.end method
