.class public abstract Lcom/squareup/sdk/pos/transaction/Card;
.super Ljava/lang/Object;
.source "Card.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/Card$Brand;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/squareup/sdk/pos/transaction/Card$Brand;Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Card;
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Card;

    invoke-direct {v0, p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Card;-><init>(Lcom/squareup/sdk/pos/transaction/Card$Brand;Ljava/lang/String;)V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Card;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Card$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Card$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract cardBrand()Lcom/squareup/sdk/pos/transaction/Card$Brand;
.end method

.method public abstract last4()Ljava/lang/String;
.end method
