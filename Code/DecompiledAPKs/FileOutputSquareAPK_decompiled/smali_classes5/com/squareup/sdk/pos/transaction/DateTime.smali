.class public abstract Lcom/squareup/sdk/pos/transaction/DateTime;
.super Ljava/lang/Object;
.source "DateTime.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final ISO_8601:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 15
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/DateTime;->ISO_8601:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/DateTime;
    .locals 1

    .line 23
    :try_start_0
    invoke-static {p0}, Lcom/squareup/sdk/pos/transaction/DateTime;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_DateTime;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_DateTime;-><init>(Ljava/lang/String;)V

    return-object v0

    :catch_0
    move-exception p0

    .line 25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static create(Ljava/util/Date;)Lcom/squareup/sdk/pos/transaction/DateTime;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/sdk/pos/transaction/DateTime;->ISO_8601:Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/sdk/pos/transaction/DateTime;->create(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object p0

    return-object p0
.end method

.method private static parse(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "Z"

    const-string v1, "-0000"

    .line 45
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 48
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 49
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-ne v1, v2, :cond_0

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 50
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 54
    :cond_0
    sget-object v0, Lcom/squareup/sdk/pos/transaction/DateTime;->ISO_8601:Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/DateTime;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_DateTime$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_DateTime$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public asJavaDate()Ljava/util/Date;
    .locals 2

    .line 36
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/DateTime;->iso8601DateString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/sdk/pos/transaction/DateTime;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 38
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "this should never happen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract iso8601DateString()Ljava/lang/String;
.end method
