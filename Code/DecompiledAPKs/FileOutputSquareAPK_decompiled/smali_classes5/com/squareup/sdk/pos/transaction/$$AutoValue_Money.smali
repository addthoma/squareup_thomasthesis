.class abstract Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;
.super Lcom/squareup/sdk/pos/transaction/Money;
.source "$$AutoValue_Money.java"


# instance fields
.field private final amount:J

.field private final currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;


# direct methods
.method constructor <init>(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Money;-><init>()V

    .line 12
    iput-wide p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->amount:J

    if-eqz p3, :cond_0

    .line 16
    iput-object p3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null currencyCode"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public amount()J
    .locals 2

    .line 21
    iget-wide v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->amount:J

    return-wide v0
.end method

.method public currencyCode()Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 42
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/pos/transaction/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 43
    check-cast p1, Lcom/squareup/sdk/pos/transaction/Money;

    .line 44
    iget-wide v3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->amount:J

    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Money;->amount()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    .line 45
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Money;->currencyCode()Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public hashCode()I
    .locals 7

    const v0, 0xf4243

    int-to-long v1, v0

    .line 54
    iget-wide v3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->amount:J

    const/16 v5, 0x20

    ushr-long v5, v3, v5

    xor-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v2, v1

    mul-int v2, v2, v0

    .line 56
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Money{amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->amount:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Money;->currencyCode:Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
