.class public final enum Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
.super Ljava/lang/Enum;
.source "TenderCardDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EntryMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

.field public static final enum CHIP:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

.field public static final enum CONTACTLESS:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

.field public static final enum MANUALLY_ENTERED:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

.field public static final enum SWIPE:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 16
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    const/4 v1, 0x0

    const-string v2, "MANUALLY_ENTERED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->MANUALLY_ENTERED:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 19
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    const/4 v2, 0x1

    const-string v3, "SWIPE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->SWIPE:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 22
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    const/4 v3, 0x2

    const-string v4, "CHIP"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CHIP:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 25
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    const/4 v4, 0x3

    const-string v5, "CONTACTLESS"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 14
    sget-object v5, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->MANUALLY_ENTERED:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->SWIPE:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CHIP:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->$VALUES:[Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->$VALUES:[Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object v0
.end method
