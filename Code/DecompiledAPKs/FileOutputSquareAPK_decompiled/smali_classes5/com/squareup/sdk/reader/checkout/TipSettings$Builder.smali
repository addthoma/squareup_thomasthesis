.class public final Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
.super Ljava/lang/Object;
.source "TipSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/TipSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private showCustomTipField:Z

.field private showSeparateTipScreen:Z

.field private final tipPercentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 4

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 97
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showCustomTipField:Z

    .line 98
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showSeparateTipScreen:Z

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Integer;

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x2

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TipSettings$1;)V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Z
    .locals 0

    .line 90
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showCustomTipField:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Z
    .locals 0

    .line 90
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showSeparateTipScreen:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Ljava/util/List;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/checkout/TipSettings;
    .locals 2

    .line 163
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TipSettings;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings;-><init>(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;Lcom/squareup/sdk/reader/checkout/TipSettings$1;)V

    return-object v0
.end method

.method public showCustomTipField(Z)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 0

    .line 111
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showCustomTipField:Z

    return-object p0
.end method

.method public showSeparateTipScreen(Z)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 0

    .line 126
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showSeparateTipScreen:Z

    return-object p0
.end method

.method public tipPercentages(Ljava/util/List;)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;"
        }
    .end annotation

    const-string/jumbo v0, "tipPercentages"

    .line 150
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string/jumbo v1, "tip percentage"

    .line 153
    invoke-static {v0, v1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 154
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public varargs tipPercentages([I)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 4

    const-string/jumbo v0, "tipPercentages"

    .line 140
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 142
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget v2, p1, v1

    .line 143
    iget-object v3, p0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method
