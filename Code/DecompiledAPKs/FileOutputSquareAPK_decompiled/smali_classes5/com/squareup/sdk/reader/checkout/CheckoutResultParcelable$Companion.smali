.class public final Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;
.super Ljava/lang/Object;
.source "CheckoutResultParcelable.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutResultParcelable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutResultParcelable.kt\ncom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,212:1\n1642#2,2:213\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutResultParcelable.kt\ncom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion\n*L\n96#1,2:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0008\u0007\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u000c\u0010\u000f\u001a\u00020\u0010*\u00020\u0011H\u0002J\u000c\u0010\u0012\u001a\u00020\u0013*\u00020\u0011H\u0002J\u000c\u0010\u0014\u001a\u00020\t*\u00020\u0011H\u0002J\u000c\u0010\u0015\u001a\u00020\u0016*\u00020\u0011H\u0002J\u000c\u0010\u0017\u001a\u00020\u0018*\u00020\u0011H\u0002J\u000e\u0010\u0019\u001a\u0004\u0018\u00010\u0004*\u00020\u0011H\u0002J\u000c\u0010\u001a\u001a\u00020\u001b*\u00020\u0011H\u0002J\u0012\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001d*\u00020\u0011H\u0002J\u0014\u0010\u001e\u001a\u00020\r*\u00020\u00112\u0006\u0010\u001f\u001a\u00020\u0018H\u0002J\u0016\u0010 \u001a\u00020\r*\u00020\u00112\u0008\u0010!\u001a\u0004\u0018\u00010\u0004H\u0002J\u0014\u0010\"\u001a\u00020\r*\u00020\u00112\u0006\u0010#\u001a\u00020\u001bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;",
        "",
        "()V",
        "CHECKOUT_RESULT_EXTRA_KEY",
        "",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;",
        "readCheckoutResultFromIntent",
        "Lcom/squareup/sdk/reader/checkout/CheckoutResult;",
        "intent",
        "Landroid/content/Intent;",
        "writeCheckoutResultToIntent",
        "",
        "result",
        "readCardDetails",
        "Lcom/squareup/sdk/reader/checkout/TenderCardDetails;",
        "Landroid/os/Parcel;",
        "readCashDetails",
        "Lcom/squareup/sdk/reader/checkout/TenderCashDetails;",
        "readCheckoutResultFromParcel",
        "readDate",
        "Ljava/util/Date;",
        "readMoney",
        "Lcom/squareup/sdk/reader/checkout/Money;",
        "readOptionalString",
        "readTender",
        "Lcom/squareup/sdk/reader/checkout/Tender;",
        "readTenders",
        "",
        "writeMoneyParcelable",
        "money",
        "writeNullableString",
        "nullable",
        "writeTenderToParcel",
        "tender",
        "reader-sdk_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$readCheckoutResultFromParcel(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readCheckoutResultFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeMoneyParcelable(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    return-void
.end method

.method public static final synthetic access$writeNullableString(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Ljava/lang/String;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeNullableString(Landroid/os/Parcel;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$writeTenderToParcel(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Tender;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeTenderToParcel(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Tender;)V

    return-void
.end method

.method private final readCardDetails(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
    .locals 7

    .line 134
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "readString()!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/sdk/reader/checkout/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-direct {v0, v1, v3}, Lcom/squareup/sdk/reader/checkout/Card;-><init>(Lcom/squareup/sdk/reader/checkout/Card$Brand;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    move-result-object v1

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    move-object v2, v3

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez v2, :cond_6

    const/4 v2, 0x1

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_a

    move-object v2, v4

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_7

    const/4 v2, 0x1

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_a

    .line 141
    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_8

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_9

    goto :goto_3

    :cond_9
    const/4 p1, 0x0

    goto :goto_4

    .line 143
    :cond_a
    :goto_3
    new-instance v2, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    invoke-direct {v2, v3, v4, p1}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v2

    .line 148
    :goto_4
    invoke-static {v0, v1, p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->newBuilder(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->build()Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object p1

    const-string v0, "TenderCardDetails.newBui\u2026     )\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readCashDetails(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
    .locals 2

    .line 157
    move-object v0, p0

    check-cast v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-direct {v0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->newBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    move-result-object v1

    .line 158
    invoke-direct {v0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->build()Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object p1

    const-string v0, "TenderCashDetails.newBui\u2026ney())\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readCheckoutResultFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 4

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v1, p0

    check-cast v1, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v0, v2, v3}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->newBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    move-result-object v0

    const-string v2, "CheckoutResult.newBuilde\u2026dMoney(), readString()!!)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readOptionalString(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    .line 93
    :cond_2
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readDate(Landroid/os/Parcel;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    move-result-object v2

    .line 94
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    .line 95
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readTenders(Landroid/os/Parcel;)Ljava/util/Set;

    move-result-object p1

    .line 96
    check-cast p1, Ljava/lang/Iterable;

    .line 213
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/sdk/reader/checkout/Tender;

    .line 97
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->addTender(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    goto :goto_0

    .line 99
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->build()Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object p1

    const-string v0, "builder.build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readDate(Landroid/os/Parcel;)Ljava/util/Date;
    .locals 1

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "readString()!!"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->parseISO8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method private final readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 4

    .line 105
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v3, "readString()!!"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    return-object v0
.end method

.method private final readOptionalString(Landroid/os/Parcel;)Ljava/lang/String;
    .locals 1

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final readTender(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Tender;
    .locals 4

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "readString()!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/sdk/reader/checkout/Tender$Type;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v0

    .line 121
    move-object v1, p0

    check-cast v1, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    .line 122
    sget-object v3, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Tender$Type;->ordinal()I

    move-result v0

    aget v0, v3, v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 125
    invoke-static {v2}, Lcom/squareup/sdk/reader/checkout/Tender;->newOtherTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v0

    const-string v2, "Tender.newOtherTenderBuilder(totalMoney)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 124
    :cond_2
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readCashDetails(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/squareup/sdk/reader/checkout/Tender;->newCashTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v0

    const-string v2, "Tender.newCashTenderBuil\u2026Money, readCashDetails())"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readCardDetails(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/squareup/sdk/reader/checkout/Tender;->newCardTenderBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v0

    const-string v2, "Tender.newCardTenderBuil\u2026Money, readCardDetails())"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    :goto_0
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readDate(Landroid/os/Parcel;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v0

    .line 129
    invoke-direct {v1, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readMoney(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->build()Lcom/squareup/sdk/reader/checkout/Tender;

    move-result-object p1

    const-string v0, "builder.createdAt(readDa\u2026ney())\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readTenders(Landroid/os/Parcel;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/Tender;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 112
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    move-object v4, p0

    check-cast v4, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-direct {v4, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readTender(Landroid/os/Parcel;)Lcom/squareup/sdk/reader/checkout/Tender;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    :cond_1
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method private final writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V
    .locals 2

    .line 207
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Money;->getAmount()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 208
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method private final writeNullableString(Landroid/os/Parcel;Ljava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p2, 0x1

    .line 197
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 199
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private final writeTenderToParcel(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Tender;)V
    .locals 5

    .line 169
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getType()Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Tender$Type;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    move-object v0, p0

    check-cast v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getTotalMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    const-string v2, "tender.totalMoney"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getType()Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v1, v2, :cond_3

    .line 173
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getTenderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getCardDetails()Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object v1

    .line 175
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getCard()Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v2

    const-string v3, "card"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Card;->getBrand()Lcom/squareup/sdk/reader/checkout/Card$Brand;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Card$Brand;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 176
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getCard()Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Card;->getLastFourDigits()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getEntryMethod()Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getCardReceiptDetails()Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object v2

    const-string v3, ""

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->getAuthorizationCode()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 179
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getCardReceiptDetails()Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->getApplicationIdentifier()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->getCardReceiptDetails()Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->getApplicationPreferredName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    move-object v1, v3

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 182
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getType()Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CASH:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v1, v2, :cond_4

    .line 183
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getCashDetails()Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object v1

    .line 184
    sget-object v2, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->getBuyerTenderedMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v3

    const-string v4, "buyerTenderedMoney"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1, v3}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 185
    sget-object v2, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->getChangeBackMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    const-string v3, "changeBackMoney"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 189
    :cond_4
    :goto_3
    invoke-static {}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->getISO_8601()Ljava/text/SimpleDateFormat;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getCreatedAt()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Tender;->getTipMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p2

    const-string v1, "tender.tipMoney"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeMoneyParcelable(Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    return-void
.end method


# virtual methods
.method public final readCheckoutResultFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "com.squareup.reader.sdk.CHECKOUT_RESULT"

    .line 84
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;

    if-nez p1, :cond_0

    .line 87
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->access$getResult$p(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object p1

    return-object p1
.end method

.method public final writeCheckoutResultToIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Landroid/content/Intent;)V
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/os/Parcelable;

    const-string p1, "com.squareup.reader.sdk.CHECKOUT_RESULT"

    .line 76
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-void
.end method
