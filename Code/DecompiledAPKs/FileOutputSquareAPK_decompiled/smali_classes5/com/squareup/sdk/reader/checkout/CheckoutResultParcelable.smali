.class public final Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;
.super Ljava/lang/Object;
.source "CheckoutResultParcelable.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutResultParcelable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutResultParcelable.kt\ncom/squareup/sdk/reader/checkout/CheckoutResultParcelable\n*L\n1#1,212:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;",
        "Landroid/os/Parcelable;",
        "result",
        "Lcom/squareup/sdk/reader/checkout/CheckoutResult;",
        "(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "dest",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "reader-sdk_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKOUT_RESULT_EXTRA_KEY:Ljava/lang/String; = "com.squareup.reader.sdk.CHECKOUT_RESULT"

.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;


# instance fields
.field private final result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    .line 61
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)V

    return-void
.end method

.method public static final synthetic access$getResult$p(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    return-object p0
.end method

.method public static final readCheckoutResultFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->readCheckoutResultFromIntent(Landroid/content/Intent;)Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object p0

    return-object p0
.end method

.method public static final writeCheckoutResultToIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Landroid/content/Intent;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->writeCheckoutResultToIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "dest"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getLocationId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32
    sget-object p2, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getTotalMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    const-string v1, "result.totalMoney"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->access$writeMoneyParcelable(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 33
    iget-object p2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getTransactionClientId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    sget-object p2, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getTransactionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p1, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->access$writeNullableString(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Ljava/lang/String;)V

    .line 35
    invoke-static {}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->getISO_8601()Ljava/text/SimpleDateFormat;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getCreatedAt()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    sget-object p2, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getTotalTipMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    const-string v1, "result.totalTipMoney"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->access$writeMoneyParcelable(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->result:Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->getTenders()Ljava/util/Set;

    move-result-object p2

    const-string v0, "result.tenders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/reader/checkout/Tender;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 44
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    sget-object v1, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->Companion:Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;

    invoke-static {v1, p1, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;->access$writeTenderToParcel(Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable$Companion;Landroid/os/Parcel;Lcom/squareup/sdk/reader/checkout/Tender;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    return-void
.end method
