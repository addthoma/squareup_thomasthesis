.class public final Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;
.super Ljava/lang/Object;
.source "TransferReportsViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;",
        "",
        "transferReportsLayoutRunnerFactory",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
        "transferReportsDetailLayoutRunnerFactory",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
        "(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V",
        "create",
        "Lcom/squareup/transferreports/TransferReportsViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transferReportsDetailLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

.field private final transferReportsLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transferReportsLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferReportsDetailLayoutRunnerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->transferReportsLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->transferReportsDetailLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Class;)Lcom/squareup/transferreports/TransferReportsViewFactory;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/transferreports/TransferReportsViewFactory;"
        }
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/transferreports/TransferReportsViewFactory;

    .line 20
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->transferReportsLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->transferReportsDetailLayoutRunnerFactory:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

    .line 19
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/transferreports/TransferReportsViewFactory;-><init>(Ljava/lang/Class;Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V

    return-object v0
.end method
