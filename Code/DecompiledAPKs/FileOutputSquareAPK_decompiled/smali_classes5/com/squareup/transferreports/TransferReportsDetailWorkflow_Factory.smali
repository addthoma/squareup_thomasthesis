.class public final Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;
.super Ljava/lang/Object;
.source "TransferReportsDetailWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final transferReportsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->transferReportsLoaderProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->transferReportsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLoader;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->newInstance(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow_Factory;->get()Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    move-result-object v0

    return-object v0
.end method
