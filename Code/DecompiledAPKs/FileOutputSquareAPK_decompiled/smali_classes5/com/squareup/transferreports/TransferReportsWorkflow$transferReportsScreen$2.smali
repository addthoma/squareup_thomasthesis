.class final Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsScreen(Lcom/squareup/workflow/Sink;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;->$reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 131
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;->$reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
