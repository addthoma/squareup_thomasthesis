.class public Lcom/squareup/thread/executor/StoppableHandlerExecutor;
.super Ljava/lang/Object;
.source "StoppableHandlerExecutor.java"

# interfaces
.implements Lcom/squareup/thread/executor/StoppableSerialExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;
    }
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private volatile isShutdown:Z

.field private final lock:Ljava/lang/Object;

.field private final queuedRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final quitLooperOnShutdown:Z

.field private final runningCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final terminationLatch:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method protected constructor <init>(Landroid/os/Handler;Z)V
    .locals 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->runningCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 33
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->terminationLatch:Ljava/util/concurrent/CountDownLatch;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    const/4 v0, 0x0

    .line 39
    iput-boolean v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    .line 42
    iput-object p1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    .line 43
    iput-boolean p2, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->quitLooperOnShutdown:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/lang/Object;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/List;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->runningCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/CountDownLatch;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->terminationLatch:Ljava/util/concurrent/CountDownLatch;

    return-object p0
.end method

.method private removeOnRun(Ljava/lang/Runnable;)Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;
    .locals 1

    .line 127
    new-instance v0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;-><init>(Lcom/squareup/thread/executor/StoppableHandlerExecutor;Ljava/lang/Runnable;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 118
    iget-boolean v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    const-string v1, "Not shutdown."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->terminationLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    return p1
.end method

.method public cancel(Ljava/lang/Runnable;)V
    .locals 4

    .line 75
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 78
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    .line 81
    invoke-virtual {v2}, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->getWrapped()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 83
    iget-object v3, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 86
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 58
    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 60
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->removeOnRun(Ljava/lang/Runnable;)Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 61
    monitor-exit v0

    return-void

    .line 63
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    :catchall_0
    move-exception p1

    .line 63
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public executeDelayed(Ljava/lang/Runnable;J)Z
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 69
    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    monitor-exit v0

    return p1

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->removeOnRun(Ljava/lang/Runnable;)Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    move-result-object p1

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 71
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public isShutdown()Z
    .locals 1

    .line 113
    iget-boolean v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    return v0
.end method

.method public post(Ljava/lang/Runnable;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 48
    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    if-nez v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->removeOnRun(Ljava/lang/Runnable;)Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v1, "Could not post."

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 53
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public shutdown()V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 91
    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 93
    iput-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->isShutdown:Z

    .line 94
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->runningCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->terminationLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;

    .line 101
    iget-object v3, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->queuedRunnables:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 106
    iget-boolean v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->quitLooperOnShutdown:Z

    if-eqz v1, :cond_3

    .line 107
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 109
    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
