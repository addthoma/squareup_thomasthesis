.class public final Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;
.super Ljava/lang/Object;
.source "AndroidMainThreadEnforcer.kt"

# interfaces
.implements Lcom/squareup/thread/enforcer/ThreadEnforcer;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eH\u0007J\u0012\u0010\u000f\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eH\u0007R\u001a\u0010\u0003\u001a\u00020\u00048FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0005\u0010\u0002\u001a\u0004\u0008\u0003\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0006R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "()V",
        "isMainThread",
        "",
        "isMainThread$annotations",
        "()Z",
        "isTargetThread",
        "mainLooper",
        "Landroid/os/Looper;",
        "kotlin.jvm.PlatformType",
        "checkMainThread",
        "",
        "message",
        "",
        "checkNotMainThread",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

.field private static final mainLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    invoke-direct {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;-><init>()V

    sput-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    .line 13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->mainLooper:Landroid/os/Looper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final checkMainThread()V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        message = "Inject @Main ThreadEnforcer and call confine()."
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final checkMainThread(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Inject @Main ThreadEnforcer and call confine()."
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "message"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->confine(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const-string p0, "Current thread must be Main thread."

    .line 32
    :cond_0
    invoke-static {p0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    return-void
.end method

.method public static final checkNotMainThread()V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        message = "Inject @Main ThreadEnforcer and call forbid()."
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkNotMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final checkNotMainThread(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Inject @Main ThreadEnforcer and call forbid()."
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "message"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->forbid(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic checkNotMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const-string p0, "Operation must not be performed on the main thread!"

    .line 39
    :cond_0
    invoke-static {p0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkNotMainThread(Ljava/lang/String;)V

    return-void
.end method

.method public static final isMainThread()Z
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    invoke-virtual {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->isTargetThread()Z

    move-result v0

    return v0
.end method

.method public static synthetic isMainThread$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Inject @Main ThreadEnforcer"
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method


# virtual methods
.method public confine()V
    .locals 0

    .line 12
    invoke-static {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method public confine(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-static {p0, p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V

    return-void
.end method

.method public forbid()V
    .locals 0

    .line 12
    invoke-static {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method public forbid(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-static {p0, p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V

    return-void
.end method

.method public isTargetThread()Z
    .locals 2

    .line 21
    sget-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->mainLooper:Landroid/os/Looper;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
