.class public abstract Lcom/squareup/text/ListItemSpan;
.super Ljava/lang/Object;
.source "ListItemSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;


# instance fields
.field private final leadingMargin:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/squareup/text/ListItemSpan;->leadingMargin:I

    return-void
.end method


# virtual methods
.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 0

    .line 23
    instance-of p4, p8, Landroid/text/Spanned;

    if-eqz p4, :cond_0

    .line 24
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result p4

    if-ne p4, p9, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/text/ListItemSpan;->getListMarker()Ljava/lang/String;

    move-result-object p4

    int-to-float p3, p3

    int-to-float p5, p6

    invoke-virtual {p1, p4, p3, p5, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getLeadingMargin(Z)I
    .locals 0

    .line 17
    iget p1, p0, Lcom/squareup/text/ListItemSpan;->leadingMargin:I

    return p1
.end method

.method protected abstract getListMarker()Ljava/lang/String;
.end method
