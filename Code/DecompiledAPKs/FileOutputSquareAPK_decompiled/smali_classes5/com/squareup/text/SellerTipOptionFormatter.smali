.class public Lcom/squareup/text/SellerTipOptionFormatter;
.super Ljava/lang/Object;
.source "SellerTipOptionFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final tipRowFormat:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    sget v0, Lcom/squareup/widgets/pos/R$string;->tip_row_format:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/text/SellerTipOptionFormatter;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/text/SellerTipOptionFormatter;->context:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/squareup/text/SellerTipOptionFormatter;->tipRowFormat:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/squareup/text/SellerTipOptionFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 40
    iput-object p4, p0, Lcom/squareup/text/SellerTipOptionFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/protos/common/tipping/TipOption;)Ljava/lang/CharSequence;
    .locals 4

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 51
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/squareup/text/SellerTipOptionFormatter;->tipRowFormat:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/text/SellerTipOptionFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    .line 53
    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/text/SellerTipOptionFormatter;->context:Landroid/content/Context;

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 54
    invoke-static {v2, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v2

    .line 53
    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/text/SellerTipOptionFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    .line 56
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/text/SellerTipOptionFormatter;->context:Landroid/content/Context;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 57
    invoke-static {v1, v2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v1

    .line 55
    invoke-static {p1, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    const-string v1, "label"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/squareup/text/SellerTipOptionFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/text/SellerTipOptionFormatter;->context:Landroid/content/Context;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 61
    invoke-static {v0, v1}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v0

    .line 60
    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/protos/common/tipping/TipOption;

    invoke-virtual {p0, p1}, Lcom/squareup/text/SellerTipOptionFormatter;->format(Lcom/squareup/protos/common/tipping/TipOption;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
