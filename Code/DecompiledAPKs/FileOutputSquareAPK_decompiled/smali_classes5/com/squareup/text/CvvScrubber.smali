.class public Lcom/squareup/text/CvvScrubber;
.super Ljava/lang/Object;
.source "CvvScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# instance fields
.field private brand:Lcom/squareup/Card$Brand;

.field private onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object v0, p0, Lcom/squareup/text/CvvScrubber;->brand:Lcom/squareup/Card$Brand;

    return-void
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v2, v1, :cond_2

    .line 16
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 17
    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 18
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    const/4 v3, 0x1

    .line 23
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    iget-object v6, p0, Lcom/squareup/text/CvvScrubber;->brand:Lcom/squareup/Card$Brand;

    invoke-virtual {v6}, Lcom/squareup/Card$Brand;->cvvLength()I

    move-result v6

    if-ne v5, v6, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 26
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-eq p1, v1, :cond_3

    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_4

    .line 30
    iget-object p1, p0, Lcom/squareup/text/CvvScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    if-eqz p1, :cond_4

    .line 31
    invoke-interface {p1}, Lcom/squareup/text/OnInvalidContentListener;->onInvalidContent()V

    .line 34
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public setBrand(Lcom/squareup/Card$Brand;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/text/CvvScrubber;->brand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method public setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/text/CvvScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    return-void
.end method
