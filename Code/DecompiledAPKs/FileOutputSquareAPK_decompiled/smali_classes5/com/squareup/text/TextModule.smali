.class public abstract Lcom/squareup/text/TextModule;
.super Ljava/lang/Object;
.source "TextModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field public static final LONG_FORM_NO_YEAR_PATTERN:Ljava/lang/String; = "MMMM d"

.field public static final MEDIUM_FORM_NO_YEAR_PATTERN:Ljava/lang/String; = "MMM d"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCompactNumberFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Lcom/squareup/text/CompactNumber;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/text/CompactNumberFormatter;

    invoke-direct {v0, p1, p0}, Lcom/squareup/text/CompactNumberFormatter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method static provideDiscountPercentageFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/text/DiscountPercentageFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/text/DiscountPercentageFormatter;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method static provideLongDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "MMMM d"

    .line 65
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v1
.end method

.method static provideLongDateFormatter(Landroid/app/Application;)Ljava/text/DateFormat;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 61
    invoke-static {p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static provideMediumDateFormat(Landroid/app/Application;)Ljava/text/DateFormat;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 70
    invoke-static {p0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static provideMediumDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "MMM d"

    .line 74
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v1
.end method

.method static provideMediumDateTime(Ljava/util/Locale;)Ljava/text/DateFormat;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string/jumbo v0, "yyyyMMMd hh:mm"

    .line 95
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v1
.end method

.method static provideNumberFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/text/NumberFormatter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/text/NumberFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;)V

    return-object v0
.end method

.method static providePercentageFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/text/PercentageFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method static provideShortDateFormat(Landroid/app/Application;)Ljava/text/DateFormat;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 79
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static provideShortDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x3

    .line 84
    invoke-static {v0, p0}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p0

    check-cast p0, Ljava/text/SimpleDateFormat;

    .line 85
    invoke-virtual {p0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^\\p{Alpha}]*y+[^\\p{Alpha}]*"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-object p0
.end method

.method static provideShortDateTime(Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/text/DateFormat;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 100
    sget v0, Lcom/squareup/utilities/R$string;->short_form_date_time_template:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 101
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-object v0
.end method

.method static provideShortFractionalPercentage(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Lcom/squareup/text/SingleDigitFractionalPercentage;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/text/PercentageFormatter;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1, v1}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method static provideSingleDigitDecimalNumberFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Lcom/squareup/text/SingleDigitDecimalNumberFormatter;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/text/NumberFormatter;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/text/NumberFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;)V

    return-object v0
.end method

.method static provideTimeFormat(Landroid/app/Application;)Ljava/text/DateFormat;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 90
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static provideWholeNumberPercentageFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/text/PercentageFormatter;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;)V

    return-object v0
.end method
