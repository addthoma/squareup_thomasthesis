.class public final Lcom/squareup/text/SimpleDecimalScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "SimpleDecimalScrubber.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0014R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/text/SimpleDecimalScrubber;",
        "Lcom/squareup/money/WholeUnitAmountScrubber;",
        "zeroState",
        "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
        "bigDecimalFormatter",
        "Lcom/squareup/quantity/BigDecimalFormatter;",
        "(Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Lcom/squareup/quantity/BigDecimalFormatter;)V",
        "formatAmount",
        "",
        "proposedAmount",
        "",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 2

    const-string/jumbo v0, "zeroState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bigDecimalFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p2}, Lcom/squareup/quantity/BigDecimalFormatter;->getRoundingScale()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/money/WholeUnitAmountScrubber;->maxValueFor(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    iput-object p2, p0, Lcom/squareup/text/SimpleDecimalScrubber;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 0

    .line 24
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    .line 25
    iget-object p2, p0, Lcom/squareup/text/SimpleDecimalScrubber;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {p2}, Lcom/squareup/quantity/BigDecimalFormatter;->getRoundingScale()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->movePointLeft(I)Ljava/math/BigDecimal;

    move-result-object p1

    .line 26
    iget-object p2, p0, Lcom/squareup/text/SimpleDecimalScrubber;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {p2, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
