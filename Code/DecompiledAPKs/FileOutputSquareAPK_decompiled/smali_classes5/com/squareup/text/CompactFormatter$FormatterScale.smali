.class public abstract Lcom/squareup/text/CompactFormatter$FormatterScale;
.super Ljava/lang/Object;
.source "CompactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/CompactFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FormatterScale"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;,
        Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;,
        Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000b\u000c\rB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068G\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0003\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter$FormatterScale;",
        "",
        "magnitude",
        "Ljava/math/BigDecimal;",
        "(Ljava/math/BigDecimal;)V",
        "formatPhraseId",
        "",
        "getFormatPhraseId",
        "()I",
        "getMagnitude",
        "()Ljava/math/BigDecimal;",
        "FormatterScaleMyriad",
        "FormatterScaleThousands",
        "ONE",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final magnitude:Ljava/math/BigDecimal;


# direct methods
.method private constructor <init>(Ljava/math/BigDecimal;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/CompactFormatter$FormatterScale;->magnitude:Ljava/math/BigDecimal;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/text/CompactFormatter$FormatterScale;-><init>(Ljava/math/BigDecimal;)V

    return-void
.end method


# virtual methods
.method public final getFormatPhraseId()I
    .locals 1

    .line 127
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_unformatted:I

    goto :goto_0

    .line 128
    :cond_0
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_thousand:I

    goto :goto_0

    .line 129
    :cond_1
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_million:I

    goto :goto_0

    .line 130
    :cond_2
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_billion:I

    goto :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_myriad:I

    goto :goto_0

    .line 132
    :cond_4
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_myllion:I

    goto :goto_0

    .line 133
    :cond_5
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Lcom/squareup/utilities/text/R$string;->compact_number_format_myriad_myllion:I

    :goto_0
    return v0

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public getMagnitude()Ljava/math/BigDecimal;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/text/CompactFormatter$FormatterScale;->magnitude:Ljava/math/BigDecimal;

    return-object v0
.end method
