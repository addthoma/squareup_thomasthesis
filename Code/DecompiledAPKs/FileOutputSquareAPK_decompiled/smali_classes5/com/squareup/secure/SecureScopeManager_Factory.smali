.class public final Lcom/squareup/secure/SecureScopeManager_Factory;
.super Ljava/lang/Object;
.source "SecureScopeManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/secure/SecureScopeManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/secure/SecureScopeManager_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/secure/SecureScopeManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/secure/SecureScopeManager_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/secure/SecureScopeManager_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/secure/SecureScopeManager_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/secure/SecureScopeManager;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/secure/SecureScopeManager;

    invoke-direct {v0, p0}, Lcom/squareup/secure/SecureScopeManager;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/secure/SecureScopeManager;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/secure/SecureScopeManager_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/secure/SecureScopeManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/secure/SecureScopeManager_Factory;->get()Lcom/squareup/secure/SecureScopeManager;

    move-result-object v0

    return-object v0
.end method
