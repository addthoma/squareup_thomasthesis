.class public final Lcom/squareup/tutorialv2/TutorialSeed$Companion;
.super Ljava/lang/Object;
.source "TutorialSeed.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialSeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0004H\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialSeed$Companion;",
        "",
        "()V",
        "NONE",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "clearAvailable",
        "",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialSeed$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$clearAvailable(Lcom/squareup/tutorialv2/TutorialSeed$Companion;Lcom/squareup/tutorialv2/TutorialSeed;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialSeed$Companion;->clearAvailable(Lcom/squareup/tutorialv2/TutorialSeed;)V

    return-void
.end method

.method private final clearAvailable(Lcom/squareup/tutorialv2/TutorialSeed;)V
    .locals 1

    const/4 v0, 0x0

    .line 84
    invoke-static {p1, v0}, Lcom/squareup/tutorialv2/TutorialSeed;->access$setAvailable$p(Lcom/squareup/tutorialv2/TutorialSeed;Z)V

    return-void
.end method
