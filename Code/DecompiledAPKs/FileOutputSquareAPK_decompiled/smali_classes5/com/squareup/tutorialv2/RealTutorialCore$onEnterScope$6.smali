.class final Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "+",
        "Lcom/squareup/tutorialv2/Tutorial;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tutorialv2/RealTutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;->this$0:Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 64
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;->test(Lkotlin/Pair;)Z

    move-result p1

    return p1
.end method

.method public final test(Lkotlin/Pair;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
            "+",
            "Lcom/squareup/tutorialv2/Tutorial;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tutorialv2/Tutorial;

    .line 211
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;->this$0:Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-static {v0}, Lcom/squareup/tutorialv2/RealTutorialCore;->access$getActive$p(Lcom/squareup/tutorialv2/RealTutorialCore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialAndPriority;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialAndPriority;->asTutorial()Lcom/squareup/tutorialv2/Tutorial;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method
