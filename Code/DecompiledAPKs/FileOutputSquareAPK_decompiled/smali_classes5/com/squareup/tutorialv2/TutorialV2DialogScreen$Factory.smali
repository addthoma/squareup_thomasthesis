.class public final Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Factory;
.super Ljava/lang/Object;
.source "TutorialV2DialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTutorialV2DialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TutorialV2DialogScreen.kt\ncom/squareup/tutorialv2/TutorialV2DialogScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,68:1\n52#2:69\n*E\n*S KotlinDebug\n*F\n+ 1 TutorialV2DialogScreen.kt\ncom/squareup/tutorialv2/TutorialV2DialogScreen$Factory\n*L\n48#1:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    const-class v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    .line 48
    invoke-interface {v0}, Lcom/squareup/register/tutorial/TutorialsComponent;->tutorialCore()Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    .line 49
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    invoke-virtual {v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->getPrompt()Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object v1

    .line 50
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v2

    check-cast v2, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    invoke-static {v2}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;->access$isX2$p(Lcom/squareup/tutorialv2/TutorialV2DialogScreen;)Z

    move-result v2

    const-string v3, "core"

    if-eqz v2, :cond_0

    .line 54
    new-instance v2, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;-><init>(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    invoke-virtual {v2}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->getDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(X2TutorialV2\u2026xt, core, prompt).dialog)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_0
    new-instance v2, Lcom/squareup/tutorialv2/TutorialV2Dialog;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/tutorialv2/TutorialV2Dialog;-><init>(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(TutorialV2Di\u2026g(context, core, prompt))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method
