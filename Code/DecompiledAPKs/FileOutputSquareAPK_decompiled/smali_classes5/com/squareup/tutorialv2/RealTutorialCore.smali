.class public final Lcom/squareup/tutorialv2/RealTutorialCore;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/TutorialCore;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTutorialCore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTutorialCore.kt\ncom/squareup/tutorialv2/RealTutorialCore\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n+ 4 RxKotlin.kt\ncom/squareup/util/rx2/RxKotlinKt\n*L\n1#1,318:1\n1642#2,2:319\n1360#2:326\n1429#2,3:327\n704#2:330\n777#2,2:331\n1587#2,3:333\n57#3,4:321\n73#3:336\n57#3,4:337\n505#4:325\n*E\n*S KotlinDebug\n*F\n+ 1 RealTutorialCore.kt\ncom/squareup/tutorialv2/RealTutorialCore\n*L\n178#1,2:319\n240#1:326\n240#1,3:327\n241#1:330\n241#1,2:331\n265#1,3:333\n187#1,4:321\n98#1:336\n101#1,4:337\n220#1:325\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u001c\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001:B0\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0011\u0010\u0005\u001a\r\u0012\t\u0012\u00070\u0007\u00a2\u0006\u0002\u0008\u00080\u0006\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\u0016\u0010\u001e\u001a\u00020\u00192\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0006H\u0002J\u0008\u0010 \u001a\u00020\u0016H\u0007J\u001c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u00062\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#H\u0002J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u0018H\u0016J\u0010\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020\u000fH\u0002J\u001e\u0010)\u001a\u00020\u00192\u0006\u0010\r\u001a\u00020\u000f2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0006H\u0002J\u0010\u0010+\u001a\u00020\u00162\u0006\u0010,\u001a\u00020\u001dH\u0016J\u0008\u0010-\u001a\u00020\u0016H\u0016J\u0010\u0010.\u001a\u00020\u00162\u0006\u0010/\u001a\u000200H\u0016J\u001a\u0010.\u001a\u00020\u00162\u0006\u0010/\u001a\u0002002\u0008\u00101\u001a\u0004\u0018\u00010$H\u0016J\u001e\u00102\u001a\u00020\u00162\u0006\u00103\u001a\u0002002\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001605H\u0016J\u0008\u00106\u001a\u00020\u0016H\u0016J\u000e\u00107\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0018H\u0016J\u0014\u00108\u001a\u00020\u0016*\u00020\u001d2\u0006\u00109\u001a\u00020\u0019H\u0002R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\u0005\u001a\r\u0012\t\u0012\u00070\u0007\u00a2\u0006\u0002\u0008\u00080\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0014\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00160\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u001b0\u001b0\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/RealTutorialCore;",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "Lmortar/Scoped;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "creators",
        "",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "tutorialContainer",
        "Ldagger/Lazy;",
        "Lcom/squareup/tutorialv2/TutorialContainer;",
        "(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)V",
        "active",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialAndPriority;",
        "kotlin.jvm.PlatformType",
        "allStates",
        "Lio/reactivex/observables/ConnectableObservable;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "exitRequests",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "readyToStart",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "tutorialEvents",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "tutorialScope",
        "Lmortar/MortarScope;",
        "bestAvailable",
        "realSeeds",
        "endTutorial",
        "findRealSeeds",
        "maybeSeeds",
        "",
        "",
        "hasActiveTutorial",
        "",
        "isRealTutorialPair",
        "tutorial",
        "maybeProduceSeed",
        "seeds",
        "onEnterScope",
        "scope",
        "onExitScope",
        "post",
        "name",
        "",
        "value",
        "postCancelable",
        "event",
        "action",
        "Lkotlin/Function0;",
        "postExitRequest",
        "state",
        "startTutorial",
        "tutorialSeed",
        "TutorialEvent",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final active:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialAndPriority;",
            ">;"
        }
    .end annotation
.end field

.field private final allStates:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private final creators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final exitRequests:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final readyToStart:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialContainer:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialEvents:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialScope:Lmortar/MortarScope;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creators"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialContainer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->creators:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialContainer:Ldagger/Lazy;

    .line 74
    sget-object p1, Lcom/squareup/tutorialv2/TutorialAndPriority;->Companion:Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;->getNO_TUTORIAL_PAIR$tutorial_release()Lcom/squareup/tutorialv2/TutorialAndPriority;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(NO_TUTORIAL_PAIR)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 76
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<TutorialEvent>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 78
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->exitRequests:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 88
    new-instance p1, Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->creators:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 90
    iget-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->creators:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/tutorialv2/TutorialCreator;

    .line 93
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialCreator;->triggeredTutorial()Lio/reactivex/Observable;

    move-result-object p3

    sget-object v0, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_0
    sget-object p2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 336
    check-cast p1, Ljava/lang/Iterable;

    new-instance p2, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$1;

    invoke-direct {p2, p0}, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$1;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-static {p1, p2}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ements.map { it as T }) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    sget-object p2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p2, Lio/reactivex/Observable;

    .line 338
    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 339
    new-instance p3, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$2;

    invoke-direct {p3, p0}, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$2;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast p3, Lio/reactivex/functions/BiFunction;

    .line 337
    invoke-static {p2, p1, p3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object p2, Lcom/squareup/tutorialv2/RealTutorialCore$2;->INSTANCE:Lkotlin/reflect/KMutableProperty1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    if-eqz p2, :cond_1

    new-instance p3, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;

    invoke-direct {p3, p2}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, p3

    :cond_1
    check-cast p2, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "combineLatest(active, se\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->readyToStart:Lio/reactivex/Observable;

    .line 105
    iget-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 106
    new-instance p2, Lcom/squareup/tutorialv2/RealTutorialCore$3;

    move-object p3, p0

    check-cast p3, Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-direct {p2, p3}, Lcom/squareup/tutorialv2/RealTutorialCore$3;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    new-instance p3, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;

    invoke-direct {p3, p2}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 108
    sget-object p2, Lcom/squareup/tutorialv2/RealTutorialCore$4;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$4;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object p1

    .line 114
    sget-object p2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 115
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    const-string p2, "active\n        .filter(:\u2026CLEAR)\n        .replay(1)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->allStates:Lio/reactivex/observables/ConnectableObservable;

    return-void
.end method

.method public static final synthetic access$findRealSeeds(Lcom/squareup/tutorialv2/RealTutorialCore;Ljava/lang/Iterable;)Ljava/util/List;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore;->findRealSeeds(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getActive$p(Lcom/squareup/tutorialv2/RealTutorialCore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$isRealTutorialPair(Lcom/squareup/tutorialv2/RealTutorialCore;Lcom/squareup/tutorialv2/TutorialAndPriority;)Z
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore;->isRealTutorialPair(Lcom/squareup/tutorialv2/TutorialAndPriority;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$maybeProduceSeed(Lcom/squareup/tutorialv2/RealTutorialCore;Lcom/squareup/tutorialv2/TutorialAndPriority;Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore;->maybeProduceSeed(Lcom/squareup/tutorialv2/TutorialAndPriority;Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startTutorial(Lcom/squareup/tutorialv2/RealTutorialCore;Lmortar/MortarScope;Lcom/squareup/tutorialv2/TutorialSeed;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore;->startTutorial(Lmortar/MortarScope;Lcom/squareup/tutorialv2/TutorialSeed;)V

    return-void
.end method

.method private final bestAvailable(Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;)",
            "Lcom/squareup/tutorialv2/TutorialSeed;"
        }
    .end annotation

    .line 265
    check-cast p1, Ljava/lang/Iterable;

    sget-object v0, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    .line 334
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tutorialv2/TutorialSeed;

    .line 266
    invoke-virtual {v1}, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable$tutorial_release()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/tutorialv2/TutorialSeed;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialSeed;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v3

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-lez v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private final findRealSeeds(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 326
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 327
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 240
    check-cast v1, Lcom/squareup/tutorialv2/TutorialSeed;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.tutorialv2.TutorialSeed"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 329
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 330
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 331
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/tutorialv2/TutorialSeed;

    .line 241
    invoke-virtual {v2}, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable$tutorial_release()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 332
    :cond_3
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final isRealTutorialPair(Lcom/squareup/tutorialv2/TutorialAndPriority;)Z
    .locals 1

    .line 119
    sget-object v0, Lcom/squareup/tutorialv2/TutorialAndPriority;->Companion:Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;->getNO_TUTORIAL_PAIR$tutorial_release()Lcom/squareup/tutorialv2/TutorialAndPriority;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final maybeProduceSeed(Lcom/squareup/tutorialv2/TutorialAndPriority;Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tutorialv2/TutorialAndPriority;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;)",
            "Lcom/squareup/tutorialv2/TutorialSeed;"
        }
    .end annotation

    .line 252
    invoke-direct {p0, p2}, Lcom/squareup/tutorialv2/RealTutorialCore;->bestAvailable(Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;

    move-result-object p2

    .line 253
    invoke-virtual {p2}, Lcom/squareup/tutorialv2/TutorialSeed;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialAndPriority;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 256
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialAndPriority;->getTutorial()Lcom/squareup/tutorialv2/Tutorial;

    move-result-object p1

    const/4 v0, 0x0

    const-string v1, "Tutorial being replaced by one of a higher priority"

    invoke-interface {p1, v1, v0}, Lcom/squareup/tutorialv2/Tutorial;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p2

    .line 259
    :cond_1
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    return-object p1
.end method

.method private final startTutorial(Lmortar/MortarScope;Lcom/squareup/tutorialv2/TutorialSeed;)V
    .locals 3

    .line 274
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialScope:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmortar/MortarScope;->destroy()V

    .line 277
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/tutorialv2/TutorialSeed;->plant$tutorial_release()Lcom/squareup/tutorialv2/Tutorial;

    move-result-object v0

    .line 278
    invoke-virtual {p1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object p1

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tutorial-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object p1

    .line 280
    move-object v1, v0

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialScope:Lmortar/MortarScope;

    .line 283
    iget-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;

    invoke-direct {v1, p0, v0, p2}, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {p1, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final endTutorial()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialScope:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {v0}, Lmortar/MortarScope;->destroy()V

    const/4 v0, 0x0

    .line 290
    check-cast v0, Lmortar/MortarScope;

    iput-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialScope:Lmortar/MortarScope;

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialAndPriority;->Companion:Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

    invoke-virtual {v1}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;->getNO_TUTORIAL_PAIR$tutorial_release()Lcom/squareup/tutorialv2/TutorialAndPriority;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public hasActiveTutorial()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$hasActiveTutorial$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-direct {v1, v2}, Lcom/squareup/tutorialv2/RealTutorialCore$hasActiveTutorial$1;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;

    invoke-direct {v2, v1}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "active.map(::isRealTutorialPair)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->allStates:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "allStates.connect()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->creators:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 319
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tutorialv2/TutorialCreator;

    .line 179
    check-cast v1, Lmortar/Scoped;

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialContainer:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialContainer;

    .line 183
    invoke-interface {v0}, Lcom/squareup/tutorialv2/TutorialContainer;->hasViewNow()Lio/reactivex/Observable;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 188
    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->readyToStart:Lio/reactivex/Observable;

    .line 322
    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 323
    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 321
    invoke-static {v1, v0, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$3;->INSTANCE:Lkotlin/reflect/KMutableProperty1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;

    invoke-direct {v2, v1}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Predicate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 191
    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$4;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;Lmortar/MortarScope;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n        r\u2026tTutorial(tutorialSeed) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 196
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v3, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$5;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$5;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    if-eqz v3, :cond_2

    new-instance v4, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;

    invoke-direct {v4, v3}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v3, v4

    :cond_2
    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "active.map(TutorialAndPriority::asTutorial)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 197
    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;

    invoke-direct {v1, p0}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$6;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 213
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(tutorialEv\u2026orial(tutorial)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->exitRequests:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    .line 220
    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->active:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 325
    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$$inlined$withLatestFrom$1;

    invoke-direct {v2}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$$inlined$withLatestFrom$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "withLatestFrom(other, Bi\u2026 combiner.invoke(t, u) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$9;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$9;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_3

    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;

    invoke-direct {v2, v1}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 222
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$10;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$10;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_4

    new-instance v2, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/tutorialv2/RealTutorialCore$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v1, v2

    :cond_4
    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "exitRequests\n        .wi\u2026utorial::onExitRequested)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->allStates:Lio/reactivex/observables/ConnectableObservable;

    .line 227
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 228
    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$12;

    invoke-direct {v1, p0}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$12;-><init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "allStates\n        .filte\u2026bscribe { endTutorial() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public post(Ljava/lang/String;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0, p1, v0}, Lcom/squareup/tutorialv2/RealTutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public post(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 140
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    const/4 v0, 0x0

    if-nez p2, :cond_0

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    const-string v0, "TutorialCore.post(): %s"

    .line 142
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    .line 144
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    const-string v0, "TutorialCore.post(): %s (%s)"

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    :goto_0
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ValueEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ValueEvent;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public postCancelable(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 159
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 160
    new-instance v0, Lcom/squareup/tutorialv2/TutorialCore$PendingAction;

    invoke-direct {v0, p2}, Lcom/squareup/tutorialv2/TutorialCore$PendingAction;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 161
    iget-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->tutorialEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;

    invoke-direct {v1, p1, v0}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;-><init>(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V

    invoke-virtual {p2, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 162
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialCore$PendingAction;->maybeExecute()V

    return-void
.end method

.method public postExitRequest()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 170
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->exitRequests:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public state()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore;->allStates:Lio/reactivex/observables/ConnectableObservable;

    .line 126
    sget-object v1, Lcom/squareup/tutorialv2/RealTutorialCore$state$1;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$state$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "allStates\n      // For o\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
