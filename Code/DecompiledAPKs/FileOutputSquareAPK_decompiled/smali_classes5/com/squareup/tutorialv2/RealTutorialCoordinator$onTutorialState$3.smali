.class public final Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;
.super Ljava/lang/Object;
.source "RealTutorialCoordinator.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCoordinator;->onTutorialState(Lcom/squareup/tutorialv2/TutorialState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3",
        "Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;",
        "onDismissed",
        "",
        "onTapped",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 85
    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;->this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismissed()V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;->this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-static {v0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->access$getCore$p(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tutorialv2/TutorialCore;->postExitRequest()V

    return-void
.end method

.method public onTapped()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$3;->this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-static {v0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->access$getCore$p(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    const-string v1, "Tutorial content tapped"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method
