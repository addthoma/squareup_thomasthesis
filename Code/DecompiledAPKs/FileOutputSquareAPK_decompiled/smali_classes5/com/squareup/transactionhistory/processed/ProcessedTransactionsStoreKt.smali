.class public final Lcom/squareup/transactionhistory/processed/ProcessedTransactionsStoreKt;
.super Ljava/lang/Object;
.source "ProcessedTransactionsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\n\u0010\u0000\"\u00020\u00012\u00020\u0001*\n\u0010\u0002\"\u00020\u00032\u00020\u0003*\n\u0010\u0004\"\u00020\u00052\u00020\u0005*\n\u0010\u0006\"\u00020\u00072\u00020\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "FetchNextSummariesFailure",
        "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Failure;",
        "FetchNextSummariesSuccess",
        "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Success;",
        "FetchTransactionFailure",
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;",
        "FetchTransactionSuccess",
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Success;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
