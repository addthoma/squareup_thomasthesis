.class public final Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;
.super Ljava/lang/Object;
.source "TransactionSearchMatch.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;",
        "",
        "()V",
        "newTransactionSearchMatch",
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "matchedProperty",
        "",
        "searchMatchStartIndex",
        "",
        "searchMatchLength",
        "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final newTransactionSearchMatch(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    if-eqz p1, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    invoke-static {}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;->getEMPTY_SEARCH_MATCH()Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->access$getMatchedProperty$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    if-eqz p2, :cond_1

    .line 56
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;->getEMPTY_SEARCH_MATCH()Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->access$getSearchMatchStartIndex$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)I

    move-result p2

    :goto_1
    if-eqz p3, :cond_2

    .line 57
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_2

    :cond_2
    invoke-static {}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatchKt;->getEMPTY_SEARCH_MATCH()Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->access$getSearchMatchLength$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)I

    move-result p3

    :goto_2
    const/4 v1, 0x0

    .line 54
    invoke-direct {v0, p1, p2, p3, v1}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
