.class public final enum Lcom/squareup/transactionhistory/pending/StoreAndForwardState;
.super Ljava/lang/Enum;
.source "StoreAndForwardState.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
        "",
        "(Ljava/lang/String;I)V",
        "UNTRACKED",
        "STORED",
        "FORWARDED",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

.field public static final enum FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

.field public static final enum STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

.field public static final enum UNTRACKED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    new-instance v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v2, 0x0

    const-string v3, "UNTRACKED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->UNTRACKED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v2, 0x1

    const-string v3, "STORED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v2, 0x2

    const-string v3, "FORWARDED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->$VALUES:[Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/pending/StoreAndForwardState;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/pending/StoreAndForwardState;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->$VALUES:[Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    return-object v0
.end method
