.class public final enum Lcom/squareup/transactionhistory/TenderType;
.super Ljava/lang/Enum;
.source "TenderInfo.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/TenderType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/TenderType;",
        "",
        "(Ljava/lang/String;I)V",
        "UNKNOWN",
        "CARD",
        "CASH",
        "NO_SALE",
        "OTHER",
        "WALLET",
        "GENERIC",
        "ZERO_AMOUNT",
        "EXTERNAL",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/TenderType;

.field public static final enum CARD:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum CASH:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum EXTERNAL:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum GENERIC:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum NO_SALE:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum OTHER:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum WALLET:Lcom/squareup/transactionhistory/TenderType;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/transactionhistory/TenderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/transactionhistory/TenderType;

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x1

    const-string v3, "CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->CARD:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x2

    const-string v3, "CASH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->CASH:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x3

    const-string v3, "NO_SALE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->NO_SALE:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x4

    const-string v3, "OTHER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->OTHER:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x5

    const-string v3, "WALLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->WALLET:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x6

    const-string v3, "GENERIC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->GENERIC:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/4 v2, 0x7

    const-string v3, "ZERO_AMOUNT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->ZERO_AMOUNT:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/TenderType;

    const/16 v2, 0x8

    const-string v3, "EXTERNAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/transactionhistory/TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/transactionhistory/TenderType;->EXTERNAL:Lcom/squareup/transactionhistory/TenderType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/transactionhistory/TenderType;->$VALUES:[Lcom/squareup/transactionhistory/TenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/TenderType;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/TenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/TenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/TenderType;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/TenderType;->$VALUES:[Lcom/squareup/transactionhistory/TenderType;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/TenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/TenderType;

    return-object v0
.end method
