.class public final Lcom/squareup/transactionhistory/mappings/TenderInfoMappingKt;
.super Ljava/lang/Object;
.source "TenderInfoMapping.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTenderInfoMapping.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TenderInfoMapping.kt\ncom/squareup/transactionhistory/mappings/TenderInfoMappingKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,16:1\n1360#2:17\n1429#2,3:18\n*E\n*S KotlinDebug\n*F\n+ 1 TenderInfoMapping.kt\ncom/squareup/transactionhistory/mappings/TenderInfoMappingKt\n*L\n15#1:17\n15#1,3:18\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u001a\u001a\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0003*\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0003H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "toTenderInfo",
        "Lcom/squareup/transactionhistory/TenderInfo;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toTenderInfo(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)Lcom/squareup/transactionhistory/TenderInfo;
    .locals 4

    const-string v0, "$this$toTenderInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    new-instance v0, Lcom/squareup/transactionhistory/TenderInfo;

    .line 8
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const-string/jumbo v2, "tender_type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/TenderTypeMappingKt;->toTenderType(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/transactionhistory/TenderType;

    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v2}, Lcom/squareup/transactionhistory/mappings/CardBrandMappingKt;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/transactionhistory/CardBrand;

    move-result-object v2

    .line 10
    iget-object v3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-static {v3}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMappingKt;->toFelicaBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/transactionhistory/FelicaBrand;

    move-result-object v3

    .line 11
    iget-object p0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/CardEntryMappingKt;->toCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/transactionhistory/CardEntryMethod;

    move-result-object p0

    .line 7
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/transactionhistory/TenderInfo;-><init>(Lcom/squareup/transactionhistory/TenderType;Lcom/squareup/transactionhistory/CardBrand;Lcom/squareup/transactionhistory/FelicaBrand;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    return-object v0
.end method

.method public static final toTenderInfo(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 15
    check-cast p0, Ljava/lang/Iterable;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 18
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 19
    check-cast v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    .line 15
    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/TenderInfoMappingKt;->toTenderInfo(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)Lcom/squareup/transactionhistory/TenderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 20
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    .line 15
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0
.end method
