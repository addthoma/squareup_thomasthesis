.class public final Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;
.super Ljava/lang/Object;
.source "TransactionHistoryMappers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionHistoryMappers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionHistoryMappers.kt\ncom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,62:1\n1642#2,2:63\n*E\n*S KotlinDebug\n*F\n+ 1 TransactionHistoryMappers.kt\ncom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt\n*L\n43#1,2:63\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003\u001a\u000c\u0010\u0004\u001a\u00020\u0005*\u00020\u0003H\u0000\u001a\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00070\u0001\u001a\n\u0010\u0008\u001a\u00020\t*\u00020\u0003\u00a8\u0006\n"
    }
    d2 = {
        "extractTenderInfo",
        "",
        "Lcom/squareup/transactionhistory/TenderInfo;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "hasRefund",
        "",
        "toTenderInfo",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        "transactionType",
        "Lcom/squareup/transactionhistory/TransactionType;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final extractTenderInfo(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$extractTenderInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const-string/jumbo v0, "tenders"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;->toTenderInfo(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final hasRefund(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 5

    const-string v0, "$this$hasRefund"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v0

    const-string v1, "allRelatedBills"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v4, "id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    const/4 p0, 0x1

    if-eqz v1, :cond_2

    .line 35
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v0

    if-ne v0, p0, :cond_2

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static final toTenderInfo(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toTenderInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 43
    check-cast p0, Ljava/lang/Iterable;

    .line 63
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 45
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    .line 46
    instance-of v3, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    const-string v4, "tenderHistory.type"

    if-eqz v3, :cond_0

    .line 47
    new-instance v3, Lcom/squareup/transactionhistory/TenderInfo;

    .line 48
    iget-object v5, v1, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lcom/squareup/transactionhistory/mappings/TenderTypeMappingKt;->toTenderType(Lcom/squareup/billhistory/model/TenderHistory$Type;)Lcom/squareup/transactionhistory/TenderType;

    move-result-object v4

    .line 49
    check-cast v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    iget-object v5, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    invoke-static {v5}, Lcom/squareup/transactionhistory/mappings/CardBrandMappingKt;->toCardBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/transactionhistory/CardBrand;

    move-result-object v5

    .line 50
    iget-object v6, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-static {v6}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMappingKt;->toFelicaBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/transactionhistory/FelicaBrand;

    move-result-object v6

    .line 51
    iget-object v1, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/CardEntryMappingKt;->toCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/transactionhistory/CardEntryMethod;

    move-result-object v1

    .line 47
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/squareup/transactionhistory/TenderInfo;-><init>(Lcom/squareup/transactionhistory/TenderType;Lcom/squareup/transactionhistory/CardBrand;Lcom/squareup/transactionhistory/FelicaBrand;Lcom/squareup/transactionhistory/CardEntryMethod;)V

    goto :goto_1

    .line 54
    :cond_0
    new-instance v3, Lcom/squareup/transactionhistory/TenderInfo;

    .line 55
    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/TenderTypeMappingKt;->toTenderType(Lcom/squareup/billhistory/model/TenderHistory$Type;)Lcom/squareup/transactionhistory/TenderType;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xe

    const/4 v13, 0x0

    move-object v7, v3

    .line 54
    invoke-direct/range {v7 .. v13}, Lcom/squareup/transactionhistory/TenderInfo;-><init>(Lcom/squareup/transactionhistory/TenderType;Lcom/squareup/transactionhistory/CardBrand;Lcom/squareup/transactionhistory/FelicaBrand;Lcom/squareup/transactionhistory/CardEntryMethod;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 45
    :goto_1
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static final transactionType(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/TransactionType;
    .locals 1

    const-string v0, "$this$transactionType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->isExchange()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/squareup/transactionhistory/TransactionType;->EXCHANGE:Lcom/squareup/transactionhistory/TransactionType;

    goto :goto_0

    .line 24
    :cond_0
    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;->hasRefund(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/squareup/transactionhistory/TransactionType;->REFUND:Lcom/squareup/transactionhistory/TransactionType;

    goto :goto_0

    .line 25
    :cond_1
    sget-object p0, Lcom/squareup/transactionhistory/TransactionType;->SALE:Lcom/squareup/transactionhistory/TransactionType;

    :goto_0
    return-object p0
.end method
