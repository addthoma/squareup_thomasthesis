.class public final Lcom/squareup/tenderworkflow/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderworkflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final buyer_cart_layout:I = 0x7f0d009f

.field public static final customer_card_on_file:I = 0x7f0d01aa

.field public static final customer_no_cards_on_file:I = 0x7f0d01ac

.field public static final payment_cash_options_view:I = 0x7f0d0433

.field public static final payment_prompt_layout:I = 0x7f0d043d

.field public static final payment_type_layout:I = 0x7f0d0440

.field public static final payment_type_more_layout:I = 0x7f0d0441

.field public static final select_method_content_layout:I = 0x7f0d04bc

.field public static final seller_cash_received_layout:I = 0x7f0d04c4

.field public static final split_tender_amount_layout:I = 0x7f0d04e2

.field public static final split_tender_completed_payment_row:I = 0x7f0d04e3

.field public static final split_tender_completed_payments_header:I = 0x7f0d04e4

.field public static final split_tender_custom_amount_row:I = 0x7f0d04e5

.field public static final split_tender_custom_even_split_layout:I = 0x7f0d04e6

.field public static final split_tender_even_split_row:I = 0x7f0d04e7

.field public static final split_tender_help_banner_row:I = 0x7f0d04e8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
