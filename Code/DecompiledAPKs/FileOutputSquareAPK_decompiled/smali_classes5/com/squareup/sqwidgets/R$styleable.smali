.class public final Lcom/squareup/sqwidgets/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ConfirmButton:[I

.field public static final ConfirmButton_buttonHeight:I = 0x0

.field public static final ConfirmButton_confirmStageBackground:I = 0x1

.field public static final ConfirmButton_confirmStageText:I = 0x2

.field public static final ConfirmButton_confirmStageTextColor:I = 0x3

.field public static final ConfirmButton_initialStageBackground:I = 0x4

.field public static final ConfirmButton_initialStageText:I = 0x5

.field public static final ConfirmButton_initialStageTextColor:I = 0x6

.field public static final ConfirmButton_weight:I = 0x7

.field public static final DialogContentView:[I

.field public static final DialogContentView_dialogButtonCancelStyle:I = 0x0

.field public static final DialogContentView_dialogButtonConfirmStyle:I = 0x1

.field public static final DialogContentView_dialogMessageStyle:I = 0x2

.field public static final DialogContentView_dialogTitleStyle:I = 0x3

.field public static final NotificationButton:[I

.field public static final NotificationButton_highlightIfImportant:I = 0x0

.field public static final NotificationButton_importantColor:I = 0x1

.field public static final NotificationButton_summaryMessagePlural:I = 0x2

.field public static final NotificationButton_summaryMessageSingular:I = 0x3

.field public static final ResizingConfirmButton:[I

.field public static final ResizingConfirmButton_android_paddingLeft:I = 0x1

.field public static final ResizingConfirmButton_android_paddingRight:I = 0x2

.field public static final ResizingConfirmButton_android_textColor:I = 0x0

.field public static final ResizingConfirmButton_buttonBackground:I = 0x3

.field public static final ResizingConfirmButton_confirmStageText:I = 0x4

.field public static final ResizingConfirmButton_initialStageText:I = 0x5

.field public static final ResizingConfirmButton_weight:I = 0x6

.field public static final SwitcherButton:[I

.field public static final SwitcherButton_android_text:I = 0x0

.field public static final SwitcherButton_applet:I = 0x1

.field public static final SwitcherButton_glyph:I = 0x2

.field public static final WifiNameRow:[I

.field public static final WifiNameRow_wifiRowBackground:I = 0x0

.field public static final X2DatePicker:[I

.field public static final X2DatePicker_cellHeight:I = 0x0

.field public static final X2DatePicker_cellWidth:I = 0x1

.field public static final X2TimePicker:[I

.field public static final X2TimePicker_cellStyle:I = 0x0

.field public static final X2TimePicker_overlayBackground:I = 0x1

.field public static final X2TimePicker_paddingSides:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [I

    .line 219
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton:[I

    const/4 v0, 0x4

    new-array v1, v0, [I

    .line 228
    fill-array-data v1, :array_1

    sput-object v1, Lcom/squareup/sqwidgets/R$styleable;->DialogContentView:[I

    new-array v0, v0, [I

    .line 233
    fill-array-data v0, :array_2

    sput-object v0, Lcom/squareup/sqwidgets/R$styleable;->NotificationButton:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 238
    fill-array-data v0, :array_3

    sput-object v0, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton:[I

    const/4 v0, 0x3

    new-array v1, v0, [I

    .line 246
    fill-array-data v1, :array_4

    sput-object v1, Lcom/squareup/sqwidgets/R$styleable;->SwitcherButton:[I

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f040498

    aput v3, v1, v2

    .line 250
    sput-object v1, Lcom/squareup/sqwidgets/R$styleable;->WifiNameRow:[I

    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 252
    fill-array-data v1, :array_5

    sput-object v1, Lcom/squareup/sqwidgets/R$styleable;->X2DatePicker:[I

    new-array v0, v0, [I

    .line 255
    fill-array-data v0, :array_6

    sput-object v0, Lcom/squareup/sqwidgets/R$styleable;->X2TimePicker:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040083
        0x7f0400ec
        0x7f0400ed
        0x7f0400ee
        0x7f040213
        0x7f040214
        0x7f040215
        0x7f040497
    .end array-data

    :array_1
    .array-data 4
        0x7f04011d
        0x7f04011e
        0x7f040120
        0x7f040123
    .end array-data

    :array_2
    .array-data 4
        0x7f0401bd
        0x7f04020a
        0x7f040408
        0x7f040409
    .end array-data

    :array_3
    .array-data 4
        0x1010098
        0x10100d6
        0x10100d8
        0x7f040077
        0x7f0400ed
        0x7f040214
        0x7f040497
    .end array-data

    :array_4
    .array-data 4
        0x101014f
        0x7f040035
        0x7f0401a0
    .end array-data

    :array_5
    .array-data 4
        0x7f040099
        0x7f04009b
    .end array-data

    :array_6
    .array-data 4
        0x7f04009a
        0x7f040305
        0x7f040308
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
