.class public final Lcom/squareup/sqwidgets/time/X2DatePicker;
.super Landroid/widget/RelativeLayout;
.source "X2DatePicker.java"


# instance fields
.field private final marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1, v0}, Lcom/squareup/sqwidgets/time/X2DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/sqwidgets/time/X2DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    sget v0, Lcom/squareup/sqwidgets/R$layout;->x2_date_picker_layout:I

    invoke-static {p1, v0, p0}, Lcom/squareup/sqwidgets/time/X2DatePicker;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    sget-object v0, Lcom/squareup/sqwidgets/R$styleable;->X2DatePicker:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 38
    sget p2, Lcom/squareup/sqwidgets/R$styleable;->X2DatePicker_cellHeight:I

    const/4 p3, 0x0

    .line 39
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    float-to-int p2, p2

    .line 40
    sget v0, Lcom/squareup/sqwidgets/R$styleable;->X2DatePicker_cellWidth:I

    .line 41
    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    float-to-int p3, p3

    .line 43
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    sget p1, Lcom/squareup/sqwidgets/R$id;->market_date_picker:I

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/time/X2DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqwidgets/time/MarketDatePicker;

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    .line 46
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->setCellSize(II)V

    return-void
.end method


# virtual methods
.method public getTime()Ljava/util/Calendar;
    .locals 3

    .line 72
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->getMonth()I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 74
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->getDayOfMonth()I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 75
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->getYear()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .line 50
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    sub-int/2addr p4, p2

    .line 53
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {p1}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->getHeight()I

    move-result p1

    .line 54
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p3, 0x3

    div-int/2addr p1, p3

    invoke-direct {p2, p4, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 56
    sget p1, Lcom/squareup/sqwidgets/R$id;->overlay:I

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/time/X2DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    const/4 p4, 0x0

    const/4 p5, 0x0

    .line 57
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge p5, v0, :cond_1

    .line 58
    invoke-virtual {p1, p5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-array v1, p3, [Ljava/lang/Integer;

    .line 59
    sget v2, Lcom/squareup/sqwidgets/R$id;->overlay_top:I

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, p4

    sget v2, Lcom/squareup/sqwidgets/R$id;->overlay_middle:I

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    sget v3, Lcom/squareup/sqwidgets/R$id;->overlay_bottom:I

    .line 62
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 59
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 62
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public updateDate(III)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2DatePicker;->marketDatePicker:Lcom/squareup/sqwidgets/time/MarketDatePicker;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->updateDate(III)V

    return-void
.end method
