.class public Lcom/squareup/sqwidgets/EditTextPassword;
.super Landroid/widget/LinearLayout;
.source "EditTextPassword.java"


# instance fields
.field private contents:Landroid/widget/LinearLayout;

.field private password:Landroid/widget/EditText;

.field private showPasswordButton:Landroid/widget/TextView;

.field private showsPassword:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    sget p2, Lcom/squareup/sqwidgets/R$layout;->edit_text_password:I

    invoke-static {p1, p2, p0}, Lcom/squareup/sqwidgets/EditTextPassword;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x0

    .line 35
    iput-boolean p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showsPassword:Z

    .line 36
    sget p1, Lcom/squareup/sqwidgets/R$id;->edit_text_password_contents:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->contents:Landroid/widget/LinearLayout;

    .line 37
    sget p1, Lcom/squareup/sqwidgets/R$id;->password_field:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    .line 38
    sget p1, Lcom/squareup/sqwidgets/R$id;->password_show:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showPasswordButton:Landroid/widget/TextView;

    .line 39
    iget-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showPasswordButton:Landroid/widget/TextView;

    new-instance p2, Lcom/squareup/sqwidgets/EditTextPassword$1;

    invoke-direct {p2, p0}, Lcom/squareup/sqwidgets/EditTextPassword$1;-><init>(Lcom/squareup/sqwidgets/EditTextPassword;)V

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    new-instance p2, Lcom/squareup/sqwidgets/-$$Lambda$EditTextPassword$6Rc2VsbTDtuVE4_tW-UqwIH8zSs;

    invoke-direct {p2, p0}, Lcom/squareup/sqwidgets/-$$Lambda$EditTextPassword$6Rc2VsbTDtuVE4_tW-UqwIH8zSs;-><init>(Lcom/squareup/sqwidgets/EditTextPassword;)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/sqwidgets/EditTextPassword;)Z
    .locals 0

    .line 25
    iget-boolean p0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showsPassword:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/sqwidgets/EditTextPassword;Z)Z
    .locals 0

    .line 25
    iput-boolean p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showsPassword:Z

    return p1
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public debounceOnTextChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnChanged(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getPassword()Ljava/lang/CharSequence;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$0$EditTextPassword(Landroid/view/View;Z)V
    .locals 0

    .line 46
    invoke-virtual {p0, p2}, Lcom/squareup/sqwidgets/EditTextPassword;->setContentsStyle(Z)V

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setContentsStyle(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->contents:Landroid/widget/LinearLayout;

    sget v0, Lcom/squareup/marin/R$drawable;->marin_edit_text_focused:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object p1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->contents:Landroid/widget/LinearLayout;

    sget v0, Lcom/squareup/marin/R$drawable;->marin_edit_text_normal:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setHint(I)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method public setPassword(Ljava/lang/CharSequence;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    sget-object v1, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public setPasswordStyle()V
    .locals 5

    .line 76
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 78
    iget-object v2, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    .line 80
    iget-boolean v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showsPassword:Z

    if-eqz v3, :cond_0

    .line 81
    iget-object v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    const/16 v4, 0x91

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 82
    iget-object v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showPasswordButton:Landroid/widget/TextView;

    sget v4, Lcom/squareup/sqwidgets/R$string;->text_password_hide:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 84
    :cond_0
    iget-object v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    const/16 v4, 0x81

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 85
    iget-object v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->showPasswordButton:Landroid/widget/TextView;

    sget v4, Lcom/squareup/sqwidgets/R$string;->text_password_show:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 87
    :goto_0
    iget-object v3, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 88
    iget-object v2, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method

.method public showKeyboard()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 72
    iget-object v0, p0, Lcom/squareup/sqwidgets/EditTextPassword;->password:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method
