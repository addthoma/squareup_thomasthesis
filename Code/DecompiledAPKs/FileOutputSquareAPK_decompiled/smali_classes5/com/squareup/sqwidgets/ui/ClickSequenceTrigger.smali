.class public final Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;
.super Ljava/lang/Object;
.source "ClickSequenceTrigger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;,
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;,
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nClickSequenceTrigger.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ClickSequenceTrigger.kt\ncom/squareup/sqwidgets/ui/ClickSequenceTrigger\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,167:1\n19#2:168\n*E\n*S KotlinDebug\n*F\n+ 1 ClickSequenceTrigger.kt\ncom/squareup/sqwidgets/ui/ClickSequenceTrigger\n*L\n100#1:168\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0003\u0014\u0015\u0016B9\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003J \u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0003*\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;",
        "",
        "onLeftButtonClicked",
        "Lio/reactivex/Observable;",
        "",
        "onRightButtonClicked",
        "clock",
        "Lcom/squareup/util/Clock;",
        "maxTimeBetweenClicksMs",
        "",
        "numberOfClicksToTrigger",
        "",
        "(Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/util/Clock;JI)V",
        "hasListener",
        "",
        "onClickSequence",
        "toClicksWithTimestamp",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;",
        "which",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;",
        "ClickSequence",
        "ClicksWithTimestamp",
        "WhichClickEvent",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private hasListener:Z

.field private final maxTimeBetweenClicksMs:J

.field private final numberOfClicksToTrigger:I

.field private final onLeftButtonClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRightButtonClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/util/Clock;JI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "JI)V"
        }
    .end annotation

    const-string v0, "onLeftButtonClicked"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRightButtonClicked"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->onLeftButtonClicked:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->onRightButtonClicked:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->clock:Lcom/squareup/util/Clock;

    iput-wide p4, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->maxTimeBetweenClicksMs:J

    iput p6, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->numberOfClicksToTrigger:I

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)Lcom/squareup/util/Clock;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method public static final synthetic access$getMaxTimeBetweenClicksMs$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)J
    .locals 2

    .line 30
    iget-wide v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->maxTimeBetweenClicksMs:J

    return-wide v0
.end method

.method public static final synthetic access$getNumberOfClicksToTrigger$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)I
    .locals 0

    .line 30
    iget p0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->numberOfClicksToTrigger:I

    return p0
.end method

.method private final toClicksWithTimestamp(Lio/reactivex/Observable;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;",
            ">;"
        }
    .end annotation

    .line 107
    new-instance v0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;-><init>(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "map { ClicksWithTimestam\u2026ck.uptimeMillis, which) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final onClickSequence()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 41
    iget-boolean v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->hasListener:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 42
    iput-boolean v1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->hasListener:Z

    .line 44
    iget-object v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->onLeftButtonClicked:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;->LEFT:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->toClicksWithTimestamp(Lio/reactivex/Observable;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)Lio/reactivex/Observable;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->onRightButtonClicked:Lio/reactivex/Observable;

    sget-object v2, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;->RIGHT:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    invoke-direct {p0, v1, v2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->toClicksWithTimestamp(Lio/reactivex/Observable;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)Lio/reactivex/Observable;

    move-result-object v1

    .line 47
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    sget-object v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;->INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;

    new-instance v2, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;

    invoke-direct {v2, p0}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;-><init>(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "merge(leftClickWithTimes\u2026 }\n          }\n        })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    const-class v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceSuccess;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    sget-object v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$3;->INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "merge(leftClickWithTimes\u2026>()\n        .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already has a listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
