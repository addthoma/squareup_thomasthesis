.class public interface abstract Lcom/squareup/tape/ObjectQueue$Listener;
.super Ljava/lang/Object;
.source "ObjectQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tape/ObjectQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;TT;)V"
        }
    .end annotation
.end method

.method public abstract onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation
.end method
