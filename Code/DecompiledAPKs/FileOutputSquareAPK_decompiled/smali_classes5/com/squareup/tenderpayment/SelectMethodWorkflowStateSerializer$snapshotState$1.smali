.class final Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodWorkflowStateSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->snapshotState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 25
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    .line 26
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    if-eqz v1, :cond_0

    .line 27
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 30
    :cond_0
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 32
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 33
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getInstrumentToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 34
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer$snapshotState$1;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getCardNameAndNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 37
    :cond_1
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    if-eqz v1, :cond_2

    .line 38
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_0

    .line 41
    :cond_2
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    if-eqz v1, :cond_3

    .line 42
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_0

    .line 45
    :cond_3
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    if-eqz v1, :cond_4

    .line 46
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_0

    .line 49
    :cond_4
    instance-of v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    if-eqz v1, :cond_5

    .line 50
    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    :cond_5
    :goto_0
    return-void
.end method
