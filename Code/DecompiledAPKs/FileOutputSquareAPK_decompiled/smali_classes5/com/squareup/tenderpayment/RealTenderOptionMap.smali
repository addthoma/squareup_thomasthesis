.class public final Lcom/squareup/tenderpayment/RealTenderOptionMap;
.super Ljava/lang/Object;
.source "RealTenderOptionMap.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTenderOptionMap.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTenderOptionMap.kt\ncom/squareup/tenderpayment/RealTenderOptionMap\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,20:1\n1113#2,2:21\n1143#2,4:23\n*E\n*S KotlinDebug\n*F\n+ 1 RealTenderOptionMap.kt\ncom/squareup/tenderpayment/RealTenderOptionMap\n*L\n13#1,2:21\n13#1,4:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001c\u0008\u0007\u0012\u0013\u0008\u0001\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\tH\u0016R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealTenderOptionMap;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionMap;",
        "tenderOptionFactories",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "(Ljava/util/Set;)V",
        "mapOfTenderOptions",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOption",
        "tenderOptionKey",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mapOfTenderOptions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderOptionFactories:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ForMainActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tenderOptionFactories"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap;->tenderOptionFactories:Ljava/util/Set;

    .line 13
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap;->tenderOptionFactories:Ljava/util/Set;

    check-cast p1, Ljava/lang/Iterable;

    const/16 v0, 0xa

    .line 21
    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 22
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 23
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 24
    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;

    .line 13
    invoke-interface {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object v2

    invoke-interface {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;->getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 26
    :cond_0
    iput-object v1, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap;->mapOfTenderOptions:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getTenderOption(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 2

    const-string/jumbo v0, "tenderOptionKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealTenderOptionMap;->mapOfTenderOptions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    if-eqz v0, :cond_0

    return-object v0

    .line 17
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No matching tender option found in TenderOptionMap for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 16
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
