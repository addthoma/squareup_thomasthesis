.class public final Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;
.super Ljava/lang/Object;
.source "SelectMethodScreenWorkflow.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartArgs"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003JE\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0013\u0010 \u001a\u00020\u00052\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020\u001fH\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001J\u0019\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "Landroid/os/Parcelable;",
        "tenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "readerPaymentsEnabled",
        "",
        "splitTenderEnabled",
        "amountDue",
        "Lcom/squareup/protos/common/Money;",
        "restartSelectMethodData",
        "Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "hasEditedSplit",
        "(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V",
        "getAmountDue",
        "()Lcom/squareup/protos/common/Money;",
        "getHasEditedSplit",
        "()Z",
        "getReaderPaymentsEnabled",
        "getRestartSelectMethodData",
        "()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;",
        "getSplitTenderEnabled",
        "getTenderOptions",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final amountDue:Lcom/squareup/protos/common/Money;

.field private final hasEditedSplit:Z

.field private final readerPaymentsEnabled:Z

.field private final restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

.field private final splitTenderEnabled:Z

.field private final tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs$Creator;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs$Creator;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V
    .locals 1

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountDue"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodData"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iput-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    iput-boolean p3, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iput-boolean p6, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;ZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->copy(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    return v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component5()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    return v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;
    .locals 8

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountDue"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restartSelectMethodData"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    iget-boolean p1, p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getHasEditedSplit()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    return v0
.end method

.method public final getReaderPaymentsEnabled()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    return v0
.end method

.method public final getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    return-object v0
.end method

.method public final getSplitTenderEnabled()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    return v0
.end method

.method public final getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartArgs(tenderOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", readerPaymentsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", splitTenderEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", amountDue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", restartSelectMethodData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasEditedSplit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->readerPaymentsEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->splitTenderEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->amountDue:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->restartSelectMethodData:Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->hasEditedSplit:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
