.class public Lcom/squareup/tenderpayment/X2TenderCompleter;
.super Ljava/lang/Object;
.source "X2TenderCompleter.java"

# interfaces
.implements Lcom/squareup/tenderpayment/TenderCompleter;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/DefaultTenderCompleter;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    .line 46
    iput-object p2, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 47
    iput-object p3, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    .line 48
    iput-object p4, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    .line 49
    iput-object p5, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 50
    iput-object p6, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 51
    iput-object p7, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    return-void
.end method


# virtual methods
.method public cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderFlowCanceled()Z

    .line 60
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 68
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasInvoicePayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public onPrepareChangeHud()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/payment/CompletedPayment;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->onPrepareChangeHud()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    const/4 v0, 0x0

    .line 138
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/tenderpayment/X2TenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 85
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_CASH_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 89
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearCashTender()Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/CashTender$Builder;->setTendered(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderCash(Lcom/squareup/payment/tender/CashTender$Builder;)Z

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 97
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 96
    invoke-virtual {v1, v0, p1}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    .line 101
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderCash(Lcom/squareup/payment/tender/CashTender$Builder;)Z

    :goto_0
    const/4 p1, 0x0

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/X2TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    const/4 v0, 0x0

    .line 109
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/tenderpayment/X2TenderCompleter;->payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->defaultTenderCompleter:Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 121
    iget-object v0, p2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    iget-object p4, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p4, p2, p1, p3}, Lcom/squareup/payment/tender/TenderFactory;->createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    .line 127
    iget-object p2, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p2, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderOther(Lcom/squareup/payment/tender/OtherTender$Builder;)Z

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0, p2, p1, p3}, Lcom/squareup/payment/tender/TenderFactory;->createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 130
    invoke-virtual {p1, p4, p2}, Lcom/squareup/payment/tender/OtherTender$Builder;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    .line 131
    iget-object p2, p0, Lcom/squareup/tenderpayment/X2TenderCompleter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p2, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderOther(Lcom/squareup/payment/tender/OtherTender$Builder;)Z

    :goto_0
    const/4 p1, 0x0

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/X2TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method
