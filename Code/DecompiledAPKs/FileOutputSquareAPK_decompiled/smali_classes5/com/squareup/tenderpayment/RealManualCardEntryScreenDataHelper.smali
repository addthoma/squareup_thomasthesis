.class public final Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;
.super Ljava/lang/Object;
.source "RealManualCardEntryScreenDataHelper.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000c\u001a\u00020\rH\u0002J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;",
        "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
        "cnpFeesMessageHelper",
        "Lcom/squareup/cnp/CnpFeesMessageHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "formatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;)V",
        "getFormattedTotal",
        "",
        "getScreenData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cnpFeesMessageHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    iput-object p2, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->formatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private final getFormattedTotal()Ljava/lang/CharSequence;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPaymentWithAtLeastOneTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->formatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "formatter.format(transaction.tenderAmountDue)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->formatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "formatter.format(transaction.grandTotal)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public getScreenData()Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;
    .locals 5

    .line 22
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-virtual {v0}, Lcom/squareup/cnp/CnpFeesMessageHelper;->manuallyEnteredCreditCardMessageData()Lcom/squareup/cnp/LinkSpanData;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    .line 24
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/tenderworkflow/R$string;->pay_card_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 25
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealManualCardEntryScreenDataHelper;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "amount"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "info"

    .line 26
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 23
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;-><init>(Ljava/lang/String;Lcom/squareup/cnp/LinkSpanData;Z)V

    return-object v1
.end method
