.class public final Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;
.super Ljava/lang/Object;
.source "DefaultTenderCompleter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/DefaultTenderCompleter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final instrumentOnFileSellerWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final postReceiptOperationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PostReceiptOperations;",
            ">;"
        }
    .end annotation
.end field

.field private final printingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final synchronousLocalPaymentPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tipDeterminerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PostReceiptOperations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 107
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 108
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 109
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 110
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 111
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 112
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 113
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 114
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->instrumentOnFileSellerWorkflowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 115
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 116
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->postReceiptOperationsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 117
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 118
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 119
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 120
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->synchronousLocalPaymentPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 121
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 122
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 123
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 124
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 125
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 126
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->printingDispatcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 127
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 128
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 129
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PostReceiptOperations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)",
            "Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 159
    new-instance v24, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v24
.end method

.method public static newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/payment/PostReceiptOperations;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/tenderpayment/DefaultTenderCompleter;
    .locals 25

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 177
    new-instance v24, Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/payment/PostReceiptOperations;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V

    return-object v24
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/DefaultTenderCompleter;
    .locals 25

    move-object/from16 v0, p0

    .line 134
    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/api/ApiTransactionController;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->instrumentOnFileSellerWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->postReceiptOperationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/payment/PostReceiptOperations;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/receipt/ReceiptSender;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->synchronousLocalPaymentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/payment/TipDeterminerFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->printingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-static/range {v2 .. v24}, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/payment/PostReceiptOperations;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter_Factory;->get()Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    move-result-object v0

    return-object v0
.end method
