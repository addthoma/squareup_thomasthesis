.class final synthetic Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen$Key<",
        "**>;",
        "Lrx/Observable<",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0002j\u0008\u0012\u0004\u0012\u00020\u0003`\u00060\u00012!\u0010\u0007\u001a\u001d\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000c\u00a2\u0006\u0002\u0008\r"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "p1",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "Lkotlin/ParameterName;",
        "name",
        "screenKey",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "createScreen"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;"

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$1;->invoke(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 337
    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$createScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
