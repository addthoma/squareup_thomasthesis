.class public final Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;
.super Ljava/lang/Object;
.source "X2TenderCompleter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/X2TenderCompleter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultTenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/DefaultTenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/DefaultTenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->defaultTenderCompleterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/DefaultTenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;)",
            "Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;"
        }
    .end annotation

    .line 60
    new-instance v8, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/DefaultTenderCompleter;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)Lcom/squareup/tenderpayment/X2TenderCompleter;
    .locals 9

    .line 67
    new-instance v8, Lcom/squareup/tenderpayment/X2TenderCompleter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/tenderpayment/X2TenderCompleter;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/DefaultTenderCompleter;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/X2TenderCompleter;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->defaultTenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tenderpayment/DefaultTenderCompleter;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/tender/TenderFactory;

    invoke-static/range {v1 .. v7}, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/DefaultTenderCompleter;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)Lcom/squareup/tenderpayment/X2TenderCompleter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/X2TenderCompleter_Factory;->get()Lcom/squareup/tenderpayment/X2TenderCompleter;

    move-result-object v0

    return-object v0
.end method
