.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;
.super Ljava/lang/Object;
.source "SeparateTenderScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SeparateTenderData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\nH\u00c6\u0003J\t\u0010#\u001a\u00020\u000cH\u00c6\u0003J\u000f\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0003J_\u0010%\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0001J\u0013\u0010&\u001a\u00020\u000c2\u0008\u0010\'\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010(\u001a\u00020)H\u00d6\u0001J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0018R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "",
        "state",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;",
        "amountEntered",
        "Lcom/squareup/protos/common/Money;",
        "amountRemainingAfterAmountEntered",
        "billAmount",
        "billAmountRemaining",
        "maxSplits",
        "",
        "isDoneEnabled",
        "",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V",
        "getAmountEntered",
        "()Lcom/squareup/protos/common/Money;",
        "getAmountRemainingAfterAmountEntered",
        "getBillAmount",
        "getBillAmountRemaining",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "()Z",
        "getMaxSplits",
        "()J",
        "getState",
        "()Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountEntered:Lcom/squareup/protos/common/Money;

.field private final amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

.field private final billAmount:Lcom/squareup/protos/common/Money;

.field private final billAmountRemaining:Lcom/squareup/protos/common/Money;

.field private final completedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final isDoneEnabled:Z

.field private final maxSplits:J

.field private final state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "JZ",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountRemainingAfterAmountEntered"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmountRemaining"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    iput-wide p6, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    iput-boolean p8, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    iput-object p9, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-wide v7, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    goto :goto_5

    :cond_5
    move-wide/from16 v7, p6

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-boolean v9, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    goto :goto_6

    :cond_6
    move/from16 v9, p8

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p9

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-wide/from16 p6, v7

    move/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->copy(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    return-wide v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    return v0
.end method

.method public final component8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "JZ",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;"
        }
    .end annotation

    const-string v0, "state"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountRemainingAfterAmountEntered"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmountRemaining"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    move-object v1, v0

    move-wide/from16 v7, p6

    move/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    iget-wide v2, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountEntered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAmountRemainingAfterAmountEntered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getBillAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getBillAmountRemaining()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCompletedTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getMaxSplits()J
    .locals 2

    .line 34
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    return-wide v0
.end method

.method public final getState()Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    invoke-static {v2, v3}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public final isDoneEnabled()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SeparateTenderData(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->state:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountRemainingAfterAmountEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->amountRemainingAfterAmountEntered:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billAmountRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->billAmountRemaining:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxSplits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->maxSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", isDoneEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->isDoneEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", completedTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->completedTenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
