.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;
.super Ljava/lang/Object;
.source "SeparateTenderCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ$\u0010\u000c\u001a\u00020\r2\u001c\u0010\u000e\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0010j\u0002`\u00130\u000fR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V",
        "create",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/coordinators/Coordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;)",
            "Lcom/squareup/coordinators/Coordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    iget-object v3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 40
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator;

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 v3, 0x0

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Lcom/squareup/text/Formatter;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/coordinators/Coordinator;

    return-object v1
.end method
