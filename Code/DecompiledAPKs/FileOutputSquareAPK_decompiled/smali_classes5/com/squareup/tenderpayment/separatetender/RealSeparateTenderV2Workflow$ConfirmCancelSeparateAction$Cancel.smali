.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;
.super Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;
.source "RealSeparateTenderV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Cancel"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;",
        "()V",
        "apply",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 290
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 290
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    move-object v10, v0

    check-cast v10, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x7f

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 290
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Cancel;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    move-result-object p1

    return-object p1
.end method
