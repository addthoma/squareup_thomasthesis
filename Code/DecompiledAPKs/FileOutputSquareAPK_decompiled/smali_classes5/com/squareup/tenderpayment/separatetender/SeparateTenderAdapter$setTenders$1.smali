.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Landroidx/recyclerview/widget/ListUpdateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->setTenders(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0006*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0016J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u0005H\u0016J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1",
        "Landroidx/recyclerview/widget/ListUpdateCallback;",
        "onChanged",
        "",
        "position",
        "",
        "count",
        "payload",
        "",
        "onInserted",
        "onMoved",
        "fromPosition",
        "toPosition",
        "onRemoved",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tenders:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            ")V"
        }
    .end annotation

    .line 144
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;->$tenders:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(IILjava/lang/Object;)V
    .locals 0

    .line 197
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Dynamically changing tenders is unsupported."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onInserted(II)V
    .locals 1

    if-nez p1, :cond_0

    add-int/lit8 p2, p2, 0x1

    :cond_0
    add-int/lit8 p1, p1, 0x4

    .line 160
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public onMoved(II)V
    .locals 0

    .line 189
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Dynamically moving tenders is unsupported."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onRemoved(II)V
    .locals 1

    add-int/lit8 p1, p1, 0x4

    .line 174
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;->$tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p2, p2, 0x1

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method
