.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;
.super Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EditTenderRowViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,462:1\n1103#2,7:463\n*E\n*S KotlinDebug\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder\n*L\n328#1,7:463\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008\u0082\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J&\u0010\u001b\u001a\u00020\u001c2\u001c\u0010\u001d\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0015j\u0002`\u00180\u0014H\u0016J\u0008\u0010\u001e\u001a\u00020\u001cH\u0016J\u0008\u0010\u001f\u001a\u00020\u001cH\u0016J\u0008\u0010 \u001a\u00020\u001cH\u0002J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020\tH\u0002J\u0010\u0010#\u001a\u00020\u001c2\u0006\u0010$\u001a\u00020\tH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0015j\u0002`\u00180\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;",
        "view",
        "Landroid/view/View;",
        "(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V",
        "amountEditText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "amountEntered",
        "Lcom/squareup/protos/common/Money;",
        "getAmountEntered",
        "()Lcom/squareup/protos/common/Money;",
        "continueButton",
        "disposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "initialized",
        "",
        "maxMoneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "bind",
        "",
        "screenData",
        "onAttach",
        "onDetach",
        "setupViews",
        "updateAmount",
        "newAmount",
        "updateMaxAmount",
        "maxAmount",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountEditText:Lcom/squareup/widgets/SelectableEditText;

.field private final continueButton:Landroid/view/View;

.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private initialized:Z

.field private final maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

.field private workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 242
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_amount:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 243
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_confirm_button:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->continueButton:Landroid/view/View;

    .line 244
    new-instance p2, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p2}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    .line 245
    new-instance p2, Lcom/squareup/money/MaxMoneyScrubber;

    .line 246
    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyLocaleHelper$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/MoneyExtractor;

    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 247
    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getCurrencyCode$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 245
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method

.method public static final synthetic access$getAmountEditText$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 241
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method public static final synthetic access$getAmountEntered$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getContinueButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Landroid/view/View;
    .locals 0

    .line 241
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->continueButton:Landroid/view/View;

    return-object p0
.end method

.method public static final synthetic access$getInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Z
    .locals 0

    .line 241
    iget-boolean p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->initialized:Z

    return p0
.end method

.method public static final synthetic access$getWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1

    .line 241
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez p0, :cond_0

    const-string/jumbo v0, "workflow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Z)V
    .locals 0

    .line 241
    iput-boolean p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->initialized:Z

    return-void
.end method

.method public static final synthetic access$setWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 241
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public static final synthetic access$setupViews(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->setupViews()V

    return-void
.end method

.method public static final synthetic access$updateAmount(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 241
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->updateAmount(Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static final synthetic access$updateMaxAmount(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 241
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->updateMaxAmount(Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method private final getAmountEntered()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 250
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyLocaleHelper$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "0"

    check-cast v1, Ljava/lang/CharSequence;

    .line 250
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    .line 252
    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getCurrencyCode$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method private final setupViews()V
    .locals 4

    .line 289
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 291
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getRes$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/tenderworkflow/R$string;->split_tender_amount_done:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x6

    .line 290
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 294
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 299
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$2;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V

    check-cast v1, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 312
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;

    invoke-direct {v1, v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;-><init>(Lcom/squareup/widgets/SelectableEditText;)V

    check-cast v1, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 321
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyLocaleHelper$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v2

    const-string v3, "0"

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyLocaleHelper$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    check-cast v1, Lcom/squareup/text/HasSelectableText;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/squareup/money/MoneyLocaleHelper;->configure$default(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;ILjava/lang/Object;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    const/4 v1, 0x1

    .line 325
    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->setEnforceCursorAtEndOfDigits(Z)V

    .line 326
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 328
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->continueButton:Landroid/view/View;

    .line 463
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateAmount(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 332
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->amountEditText:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateMaxAmount(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    return-void
.end method


# virtual methods
.method public bind(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public onAttach()V
    .locals 3

    .line 263
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->screens:Lio/reactivex/Observable;

    if-nez v1, :cond_0

    const-string v2, "screens"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;

    invoke-direct {v2, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$onAttach$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
