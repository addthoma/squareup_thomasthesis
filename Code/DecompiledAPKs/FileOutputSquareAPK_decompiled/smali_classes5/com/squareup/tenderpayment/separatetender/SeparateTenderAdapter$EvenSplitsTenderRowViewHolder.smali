.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;
.super Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EvenSplitsTenderRowViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,462:1\n1103#2,7:463\n1103#2,7:470\n1103#2,7:477\n1103#2,7:484\n*E\n*S KotlinDebug\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder\n*L\n353#1,7:463\n356#1,7:470\n359#1,7:477\n362#1,7:484\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\t\n\u0002\u0008\u0003\u0008\u0082\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J&\u0010\u0016\u001a\u00020\u00172\u001c\u0010\u0018\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0002`\u00110\rH\u0016J\u0008\u0010\u0019\u001a\u00020\u0017H\u0016J\u0008\u0010\u001a\u001a\u00020\u0017H\u0016J\u0008\u0010\u001b\u001a\u00020\u0017H\u0002J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u00172\u0006\u0010 \u001a\u00020\u001eH\u0002R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u000c\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0002`\u00110\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0015X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;",
        "view",
        "Landroid/view/View;",
        "(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V",
        "customButton",
        "disposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "fourButton",
        "initialized",
        "",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "threeButton",
        "twoButton",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "bind",
        "",
        "screenData",
        "onAttach",
        "onDetach",
        "setupButtons",
        "split",
        "splits",
        "",
        "updateButtons",
        "maxSplits",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customButton:Landroid/view/View;

.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final fourButton:Landroid/view/View;

.field private initialized:Z

.field private screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

.field private final threeButton:Landroid/view/View;

.field private final twoButton:Landroid/view/View;

.field private workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 342
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_even_split_two:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->twoButton:Landroid/view/View;

    .line 343
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_even_split_three:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->threeButton:Landroid/view/View;

    .line 344
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_even_split_four:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->fourButton:Landroid/view/View;

    .line 345
    sget p1, Lcom/squareup/tenderworkflow/R$id;->split_tender_even_split_custom:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->customButton:Landroid/view/View;

    .line 346
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method

.method public static final synthetic access$getInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)Z
    .locals 0

    .line 340
    iget-boolean p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->initialized:Z

    return p0
.end method

.method public static final synthetic access$getWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1

    .line 340
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez p0, :cond_0

    const-string/jumbo v0, "workflow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setInitialized$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;Z)V
    .locals 0

    .line 340
    iput-boolean p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->initialized:Z

    return-void
.end method

.method public static final synthetic access$setWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public static final synthetic access$setupButtons(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V
    .locals 0

    .line 340
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->setupButtons()V

    return-void
.end method

.method public static final synthetic access$split(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;J)V
    .locals 0

    .line 340
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->split(J)V

    return-void
.end method

.method public static final synthetic access$updateButtons(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;J)V
    .locals 0

    .line 340
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->updateButtons(J)V

    return-void
.end method

.method private final setupButtons()V
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->twoButton:Landroid/view/View;

    .line 463
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->threeButton:Landroid/view/View;

    .line 470
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->fourButton:Landroid/view/View;

    .line 477
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$3;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->customButton:Landroid/view/View;

    .line 484
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$4;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$setupButtons$$inlined$onClickDebounced$4;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final split(J)V
    .locals 2

    .line 368
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez v0, :cond_0

    const-string/jumbo v1, "workflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;

    invoke-direct {v1, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;-><init>(J)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final updateButtons(J)V
    .locals 6

    .line 372
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->twoButton:Landroid/view/View;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v3, 0x1

    cmp-long v5, p1, v3

    if-lez v5, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 373
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->threeButton:Landroid/view/View;

    const/4 v3, 0x2

    int-to-long v3, v3

    cmp-long v5, p1, v3

    if-lez v5, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 374
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->fourButton:Landroid/view/View;

    const/4 v3, 0x3

    int-to-long v3, v3

    cmp-long v5, p1, v3

    if-lez v5, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 375
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->customButton:Landroid/view/View;

    const/4 v3, 0x4

    int-to-long v3, v3

    cmp-long v5, p1, v3

    if-lez v5, :cond_3

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public bind(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 379
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public onAttach()V
    .locals 3

    .line 383
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->screens:Lio/reactivex/Observable;

    if-nez v1, :cond_0

    const-string v2, "screens"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$onAttach$1;

    invoke-direct {v2, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder$onAttach$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;->disposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
