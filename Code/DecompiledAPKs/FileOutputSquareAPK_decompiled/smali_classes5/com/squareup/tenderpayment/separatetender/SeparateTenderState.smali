.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
.super Ljava/lang/Object;
.source "SeparateTenderState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 32\u00020\u0001:\u000234BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u000f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\t\u0010#\u001a\u00020\u000eH\u00c6\u0003Je\u0010$\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eH\u00c6\u0001J\u0008\u0010%\u001a\u00020&H\u0016J\u0013\u0010\'\u001a\u00020(2\u0008\u0010)\u001a\u0004\u0018\u00010*H\u00d6\u0003J\t\u0010+\u001a\u00020&H\u00d6\u0001J\t\u0010,\u001a\u00020-H\u00d6\u0001J\u0018\u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020&H\u0016R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0011R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Landroid/os/Parcelable;",
        "amountRemaining",
        "Lcom/squareup/protos/common/Money;",
        "maxSplits",
        "",
        "amountEntered",
        "billAmount",
        "maxSplitAmount",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "cancelledTenders",
        "screen",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;",
        "(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V",
        "getAmountEntered",
        "()Lcom/squareup/protos/common/Money;",
        "getAmountRemaining",
        "getBillAmount",
        "getCancelledTenders",
        "()Ljava/util/List;",
        "getCompletedTenders",
        "getMaxSplitAmount",
        "getMaxSplits",
        "()J",
        "getScreen",
        "()Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "i",
        "Companion",
        "SeparateTenderScreen",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion;


# instance fields
.field private final amountEntered:Lcom/squareup/protos/common/Money;

.field private final amountRemaining:Lcom/squareup/protos/common/Money;

.field private final billAmount:Lcom/squareup/protos/common/Money;

.field private final cancelledTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final completedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final maxSplitAmount:Lcom/squareup/protos/common/Money;

.field private final maxSplits:J

.field private final screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->Companion:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion;

    .line 47
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;",
            ")V"
        }
    .end annotation

    const-string v0, "amountRemaining"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxSplitAmount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelledTenders"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    iput-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    iput-object p6, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    iput-object p7, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    iput-object p8, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    iput-object p9, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    move/from16 v0, p10

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    .line 21
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    move-object v10, v0

    goto :goto_0

    :cond_0
    move-object/from16 v10, p9

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    goto :goto_1

    :cond_1
    move-wide v3, p2

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    move-object v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    goto :goto_3

    :cond_3
    move-object v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p8

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p9

    :goto_7
    move-object p1, v2

    move-wide p2, v3

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->copy(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    return-wide v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    return-object v0
.end method

.method public final component8()Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;",
            ")",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;"
        }
    .end annotation

    const-string v0, "amountRemaining"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxSplitAmount"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelledTenders"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-object v1, v0

    move-wide v3, p2

    invoke-direct/range {v1 .. v10}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    iget-wide v2, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    iget-object p1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountEntered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAmountRemaining()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getBillAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCancelledTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getCompletedTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getMaxSplitAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMaxSplits()J
    .locals 2

    .line 15
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    return-wide v0
.end method

.method public final getScreen()Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    invoke-static {v2, v3}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SeparateTenderState(amountRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxSplits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", amountEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxSplitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", completedTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->completedTenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cancelledTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->cancelledTenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountRemaining:Lcom/squareup/protos/common/Money;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 38
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplits:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 39
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->amountEntered:Lcom/squareup/protos/common/Money;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 40
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->billAmount:Lcom/squareup/protos/common/Money;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 41
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 42
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->screen:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
