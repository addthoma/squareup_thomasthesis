.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;
.super Ljava/lang/Object;
.source "RealSeparateTenderHandler.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSeparateTenderHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSeparateTenderHandler.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,110:1\n1642#2,2:111\n*E\n*S KotlinDebug\n*F\n+ 1 RealSeparateTenderHandler.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler\n*L\n96#1,2:111\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\nH\u0016J\u0010\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0016\u0010\u001e\u001a\u00020\u00192\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J\u0010\u0010 \u001a\u00020\n2\u0006\u0010!\u001a\u00020\"H\u0002R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderHandler;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V",
        "billAmount",
        "Lcom/squareup/protos/common/Money;",
        "getBillAmount",
        "()Lcom/squareup/protos/common/Money;",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "getTenderFactory",
        "()Lcom/squareup/payment/tender/TenderFactory;",
        "getTenderInEdit",
        "()Lcom/squareup/payment/TenderInEdit;",
        "getTransaction",
        "()Lcom/squareup/payment/Transaction;",
        "createSplitTender",
        "",
        "splitTenderAmount",
        "pushToTransaction",
        "result",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "removeTenders",
        "canceledTenders",
        "splitEven",
        "numSplits",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-void
.end method

.method private final getBillAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getCompletedTenders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026         .capturedTenders"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final removeTenders(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    .line 96
    check-cast p1, Ljava/lang/Iterable;

    .line 111
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender;

    .line 97
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    .line 98
    invoke-virtual {v1, v0}, Lcom/squareup/payment/BillPayment;->dropCapturedTender(Lcom/squareup/payment/tender/BaseTender;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final splitEven(J)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 89
    :goto_0
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, p1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "due.currency_code"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 90
    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->createSplitTender(Lcom/squareup/protos/common/Money;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->getCompletedTenders()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v2, p1

    invoke-virtual {v1, v2, v3}, Lcom/squareup/payment/Transaction;->setNumSplitsTotal(J)V

    return-object v0
.end method


# virtual methods
.method public createSplitTender(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string v0, "splitTenderAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSplitTenderBillPayment()Lcom/squareup/payment/BillPayment;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-nez v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireSmartCardTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-void
.end method

.method public final getTenderFactory()Lcom/squareup/payment/tender/TenderFactory;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    return-object v0
.end method

.method public final getTenderInEdit()Lcom/squareup/payment/TenderInEdit;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object v0
.end method

.method public final getTransaction()Lcom/squareup/payment/Transaction;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-object v0
.end method

.method public pushToTransaction(Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;)V
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;->getCancelledTenders()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->removeTenders(Ljava/util/List;)V

    .line 25
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CustomSplitEntered;

    if-eqz v0, :cond_3

    .line 26
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CustomSplitEntered;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CustomSplitEntered;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->createSplitTender(Lcom/squareup/protos/common/Money;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 30
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 32
    :goto_0
    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 33
    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_1

    .line 35
    :cond_1
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 38
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CustomSplitEntered;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 41
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 42
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    .line 44
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, 0x2

    :goto_2
    int-to-long v0, p1

    .line 46
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/Transaction;->setNumSplitsTotal(J)V

    goto :goto_3

    .line 48
    :cond_3
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    if-eqz v0, :cond_4

    .line 49
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getNumberOfSplits()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderHandler;->splitEven(J)Lcom/squareup/protos/common/Money;

    goto :goto_3

    .line 51
    :cond_4
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$CancelSplitTender;

    if-eqz v0, :cond_5

    :goto_3
    return-void

    .line 54
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
