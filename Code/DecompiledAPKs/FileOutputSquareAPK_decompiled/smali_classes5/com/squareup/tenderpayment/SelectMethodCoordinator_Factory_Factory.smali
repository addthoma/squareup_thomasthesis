.class public final Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "SelectMethodCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final paymentHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Ldagger/Lazy;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Ldagger/Lazy;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Ldagger/Lazy<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;"
        }
    .end annotation

    .line 81
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Ldagger/Lazy;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Ldagger/Lazy;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;
    .locals 10

    .line 64
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v9}, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Ldagger/Lazy;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Ldagger/Lazy;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator_Factory_Factory;->get()Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
