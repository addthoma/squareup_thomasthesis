.class public final Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;
.super Ljava/lang/Object;
.source "RealPreviewSelectMethodWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J3\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
        "",
        "tenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "screenData",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)V",
        "getScreenData",
        "()Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "getScreenKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "getTenderOptions",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

.field private final screenKey:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;ILjava/lang/Object;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->copy(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public final component2()Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    return-object v0
.end method

.method public final component3()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;"
        }
    .end annotation

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iget-object v1, p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v1, p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object p1, p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getScreenData()Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    return-object v0
.end method

.method public final getScreenKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public final getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RealPreviewSelectMethodWorkflowState(tenderOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screenData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenData:Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screenKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
