.class public final Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealPreviewSelectMethodWorkflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;",
        "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPreviewSelectMethodWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPreviewSelectMethodWorkflow.kt\ncom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,284:1\n180#2:285\n37#3,7:286\n*E\n*S KotlinDebug\n*F\n+ 1 RealPreviewSelectMethodWorkflow.kt\ncom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow\n*L\n86#1:285\n86#1,7:286\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 62\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u00016B7\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0014\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001cH\u0002J,\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001e2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u001e2\u0006\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020$H\u0002J\u001a\u0010%\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\u00032\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0016JN\u0010)\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010&\u001a\u00020\u00032\u0006\u0010*\u001a\u00020\u00042\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050,H\u0016J\u0008\u0010-\u001a\u00020$H\u0002J\u001c\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010*\u001a\u00020\u0004H\u0002J\u001c\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001c2\u0006\u0010*\u001a\u00020\u0004H\u0002J\u0010\u00100\u001a\u00020(2\u0006\u0010*\u001a\u00020\u0004H\u0016J\u0010\u00101\u001a\u0002022\u0006\u00103\u001a\u000204H\u0002J\u0010\u00105\u001a\u00020(2\u0006\u0010*\u001a\u00020\u0004H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;",
        "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "device",
        "Lcom/squareup/util/Device;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "renderer",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "(Lcom/squareup/util/Device;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V",
        "previewMax",
        "Lcom/squareup/protos/common/Money;",
        "previewMin",
        "exit",
        "Lcom/squareup/workflow/WorkflowAction;",
        "getTenderViewData",
        "",
        "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
        "tenders",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
        "previewAmount",
        "checkContactless",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showContactless",
        "showPrimary",
        "showSecondary",
        "snapshotState",
        "startScreenData",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "tenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "takeSnapshot",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PREVIEW_AMOUNT:J = 0x9c4L


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final previewMax:Lcom/squareup/protos/common/Money;

.field private final previewMin:Lcom/squareup/protos/common/Money;

.field private final renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->Companion:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    iput-object p4, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p6, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 153
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 p2, 0x9c5

    invoke-static {p2, p3, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMax:Lcom/squareup/protos/common/Money;

    .line 155
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 p2, 0x9c3

    invoke-static {p2, p3, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMin:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static final synthetic access$exit(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->exit()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showPrimary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->showPrimary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showSecondary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->showSecondary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final exit()Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 141
    sget-object v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$exit$1;->INSTANCE:Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$exit$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final getTenderViewData(Ljava/util/List;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    .line 265
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    if-eqz p3, :cond_0

    .line 266
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->showContactless()Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMax:Lcom/squareup/protos/common/Money;

    .line 267
    iget-object v11, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMin:Lcom/squareup/protos/common/Money;

    const/4 v4, 0x1

    move-object v2, p1

    move-object v3, p2

    .line 264
    invoke-virtual/range {v0 .. v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->tenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    const-string p2, "renderer.tenderViewData(\u2026\n        previewMin\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final showContactless()Z
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 273
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private final showPrimary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 143
    new-instance v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$showPrimary$1;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$showPrimary$1;-><init>(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showSecondary(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 148
    new-instance v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$showSecondary$1;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$showSecondary$1;-><init>(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final startScreenData(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 26

    move-object/from16 v0, p0

    .line 165
    new-instance v1, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    const-wide/16 v2, 0x9c4

    .line 166
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v1

    .line 167
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v1

    .line 168
    invoke-virtual {v1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 169
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v13

    .line 173
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    .line 175
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 176
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    const/4 v5, 0x1

    .line 177
    iget-object v7, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMax:Lcom/squareup/protos/common/Money;

    iget-object v8, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMin:Lcom/squareup/protos/common/Money;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, v1

    move v11, v13

    .line 174
    invoke-virtual/range {v2 .. v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->actionablePromptText(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v15

    const-string v2, "renderer.actionablePromp\u2026erCheckoutEnabled\n      )"

    invoke-static {v15, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    const-class v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    const-string v3, "EnumSet.noneOf(Connected\u2026Capabilities::class.java)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_0
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_SWIPE:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    check-cast v2, Ljava/lang/Enum;

    sget-object v3, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    check-cast v3, Ljava/lang/Enum;

    sget-object v4, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    check-cast v4, Ljava/lang/Enum;

    invoke-static {v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    const-string v3, "EnumSet.of(SUPPORTS_SWIP\u2026 SUPPORTS_TAP_AND_HAS_SS)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object v8, v2

    .line 187
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    const/4 v3, 0x1

    const/4 v4, 0x1

    .line 191
    iget-object v6, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMax:Lcom/squareup/protos/common/Money;

    .line 192
    iget-object v7, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->previewMin:Lcom/squareup/protos/common/Money;

    .line 194
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getPrimaryOptions()Ljava/util/List;

    move-result-object v9

    .line 195
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v10

    .line 196
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v11

    .line 199
    iget-object v5, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v5}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v14

    .line 200
    iget-object v5, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v5}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v5

    const-string v12, "settings.userSettings"

    invoke-static {v5, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v17

    move-object v5, v1

    const/4 v12, 0x0

    move-object/from16 v16, v15

    move-object/from16 v15, v17

    .line 187
    invoke-virtual/range {v2 .. v15}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentPromptText(ZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLcom/squareup/CountryCode;)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v2

    const-string v3, "renderer.paymentPromptTe\u2026tings.countryCode\n      )"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v13, v2

    move-object/from16 v12, v16

    goto :goto_1

    .line 203
    :cond_1
    new-instance v2, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 208
    sget-object v9, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v3, "DEFAULT"

    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v10, 0x8

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v4, v2

    .line 203
    invoke-direct/range {v4 .. v13}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    .line 214
    new-instance v4, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 219
    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v20, 0x8

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object v14, v4

    move-object/from16 v19, v5

    .line 214
    invoke-direct/range {v14 .. v23}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    move-object v13, v2

    move-object v12, v4

    .line 227
    :goto_1
    new-instance v25, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-object/from16 v2, v25

    const-string v3, "previewAmount"

    .line 228
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    .line 231
    new-instance v14, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-object v6, v14

    sget v15, Lcom/squareup/tenderworkflow/R$string;->split_tender:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v14 .. v19}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/fsm/SideEffect;Ljava/lang/String;)V

    const/4 v7, 0x1

    const/4 v8, 0x1

    .line 234
    sget-object v9, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->DISABLED:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    .line 236
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getPrimaryOptions()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x1

    .line 235
    invoke-direct {v0, v3, v1, v4}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->getTenderViewData(Ljava/util/List;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v10

    .line 241
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getSecondaryOptions()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    .line 240
    invoke-direct {v0, v3, v1, v4}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->getTenderViewData(Ljava/util/List;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v11

    .line 247
    iget-object v3, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    invoke-virtual {v3, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->getQuickCashAmounts(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v3

    move-object v14, v3

    const-string v4, "renderer.getQuickCashAmounts(previewAmount)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v15

    .line 249
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v16

    .line 250
    iget-object v3, v0, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v3}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v17

    const/16 v18, 0x1

    .line 252
    new-instance v3, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    move-object/from16 v19, v3

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v3, v4}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    const-wide/16 v20, 0x0

    const-wide/16 v22, 0x0

    .line 255
    sget-object v3, Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeEnabled;

    move-object/from16 v24, v3

    check-cast v24, Lcom/squareup/payment/CardOptionEnabled;

    move-object v3, v1

    move-object v4, v1

    .line 227
    invoke-direct/range {v2 .. v24}, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$TextData;ZZLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/tenderpayment/SelectMethod$TextData;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;JJLcom/squareup/payment/CardOptionEnabled;)V

    return-object v25
.end method

.method private final takeSnapshot(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 158
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$takeSnapshot$1;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$takeSnapshot$1;-><init>(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    .line 86
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    .line 285
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p2}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p2

    check-cast p2, Lokio/BufferedSource;

    .line 87
    invoke-static {p2}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 88
    :goto_0
    invoke-static {p2}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p2

    .line 286
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v2, "Parcel.obtain()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 288
    array-length v2, p2

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 289
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 290
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 88
    check-cast p2, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    .line 91
    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->startScreenData(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p2

    .line 92
    new-instance v1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    .line 92
    invoke-direct {v1, p1, p2, v0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)V

    goto :goto_1

    .line 99
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->startScreenData(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p2

    .line 100
    new-instance v1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    .line 101
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object p1

    .line 103
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 100
    invoke-direct {v1, p1, p2, v0}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/Screen$Key;)V

    :goto_1
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->initialState(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;

    check-cast p2, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->render(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->getScreenKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 114
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;->getScreenData()Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object v0

    .line 116
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 117
    new-instance v1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow$render$workflowInput$1;-><init>(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Sink;Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v1}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 134
    invoke-static {p1, v0, p2}, Lcom/squareup/tenderpayment/SelectMethod;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 135
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->takeSnapshot(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflow;->snapshotState(Lcom/squareup/tenderpayment/RealPreviewSelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
