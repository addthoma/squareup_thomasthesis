.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowServices_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
        ">;"
    }
.end annotation


# instance fields
.field private final changeHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final completerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final showModalListProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->showModalListProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->completerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;"
        }
    .end annotation

    .line 84
    new-instance v12, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/payment/OfflineModeMonitor;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;"
        }
    .end annotation

    .line 92
    new-instance v12, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;-><init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/payment/OfflineModeMonitor;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->showModalListProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->completerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/payment/OfflineModeMonitor;

    invoke-static/range {v1 .. v11}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->newInstance(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/payment/OfflineModeMonitor;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices_Factory;->get()Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-result-object v0

    return-object v0
.end method
