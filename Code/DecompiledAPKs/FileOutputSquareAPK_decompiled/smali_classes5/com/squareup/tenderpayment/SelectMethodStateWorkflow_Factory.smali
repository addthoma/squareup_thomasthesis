.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;
.super Ljava/lang/Object;
.source "SelectMethodStateWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final changeHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final completerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final screenDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodScreensFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final servicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 117
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 118
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 119
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 120
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 121
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 122
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 123
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->completerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 124
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 125
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 126
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 127
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 128
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 129
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 130
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 131
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 132
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 133
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->permissionWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 134
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->screenDataRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 135
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->selectMethodScreensFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 136
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->servicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 137
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 138
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 139
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 140
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 141
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 142
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 143
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 144
    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    .line 175
    new-instance v29, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;

    move-object/from16 v0, v29

    invoke-direct/range {v0 .. v28}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v29
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;
    .locals 30

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    .line 192
    new-instance v29, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    move-object/from16 v0, v29

    invoke-direct/range {v0 .. v28}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V

    return-object v29
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;
    .locals 30

    move-object/from16 v0, p0

    .line 149
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->completerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->permissionWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/permissionworkflow/PermissionWorkflow;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->screenDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->selectMethodScreensFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->servicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/swipe/SwipeValidator;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/TouchEventMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static/range {v2 .. v29}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow_Factory;->get()Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    move-result-object v0

    return-object v0
.end method
