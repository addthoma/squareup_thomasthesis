.class public Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowScreenDataRenderer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u00fa\u0001\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\u00112\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u00112\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u00112\u000e\u0010-\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.2\u000e\u00100\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.2\u0006\u00101\u001a\u00020\u0011J\u0082\u0001\u00102\u001a\u0008\u0012\u0004\u0012\u0002030.2\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u0002050.2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u00106\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\u00112\u000e\u0010-\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.2\u0006\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000f2\u0006\u00101\u001a\u00020\u0011H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
        "",
        "renderer",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;",
        "(Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;)V",
        "createSelectMethodScreenData",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "startArgs",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "selectMethodInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "nfcState",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "displayedAmount",
        "Lcom/squareup/protos/common/Money;",
        "isConnectedToNetwork",
        "",
        "toast",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "readerCapabilities",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "buyerCheckoutEnabled",
        "billAmount",
        "transactionMaximum",
        "transactionMinimum",
        "isZeroDollarPayment",
        "isActualSplitTender",
        "showContactlessRow",
        "hasCustomer",
        "hasSplitTenderBillPayment",
        "currentSplitNumber",
        "",
        "totalSplits",
        "splitTenderState",
        "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
        "splitTenderLabel",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "cardOption",
        "Lcom/squareup/payment/CardOptionEnabled;",
        "isHodor",
        "userCountryCode",
        "Lcom/squareup/CountryCode;",
        "isTablet",
        "customerGiftCardInstrumentDetails",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
        "customerNonGiftCardInstrumentDetails",
        "isInOfflineMode",
        "getTenderViewData",
        "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
        "tenders",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
        "checkContactless",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "renderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    return-void
.end method

.method private final getTenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZLjava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "ZZZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    .line 147
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    .line 155
    :goto_0
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object/from16 v5, p9

    invoke-virtual {v4, v5}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->cardsOnFile(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    :goto_1
    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v10, p12

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    .line 147
    invoke-virtual/range {v1 .. v12}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->tenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v1

    const-string v2, "renderer.tenderViewData(\u2026 transactionMinimum\n    )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZZZJJLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/payment/CardOptionEnabled;ZLcom/squareup/CountryCode;ZLjava/util/List;Ljava/util/List;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 52
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Lcom/squareup/protos/common/Money;",
            "Z",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "ZZZZZJJ",
            "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
            "Lcom/squareup/payment/CardOptionEnabled;",
            "Z",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;Z)",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;"
        }
    .end annotation

    move-object/from16 v13, p0

    move-object/from16 v15, p4

    move-object/from16 v14, p10

    move-object/from16 v12, p11

    move-object/from16 v29, p4

    move-object/from16 v45, p6

    move-object/from16 v30, p9

    move/from16 v31, p13

    move-wide/from16 v46, p17

    move-wide/from16 v48, p19

    move-object/from16 v35, p21

    move-object/from16 v32, p22

    move-object/from16 v50, p23

    move/from16 v43, p26

    const-string v0, "startArgs"

    move-object/from16 v11, p1

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodInput"

    move-object/from16 v10, p2

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcState"

    move-object/from16 v9, p3

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayedAmount"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toast"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerCapabilities"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    move-object/from16 v1, p9

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionMaximum"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionMinimum"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "splitTenderState"

    move-object/from16 v1, p21

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "splitTenderLabel"

    move-object/from16 v1, p22

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardOption"

    move-object/from16 v1, p23

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userCountryCode"

    move-object/from16 v7, p25

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getPrimaryOptions()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v3, p4

    move/from16 v4, p5

    move/from16 v6, p14

    move/from16 v7, p15

    move/from16 v8, p16

    move-object/from16 v9, p27

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object v14, v12

    move/from16 v12, p29

    .line 54
    invoke-direct/range {v0 .. v12}, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->getTenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZLjava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v22

    move-object/from16 v36, v22

    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getSecondaryOptions()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    .line 68
    invoke-direct/range {v0 .. v12}, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->getTenderViewData(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;Lcom/squareup/protos/common/Money;ZZZZZLjava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v10

    move-object/from16 v37, v10

    .line 82
    iget-object v0, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object/from16 v11, p10

    move-object v12, v14

    invoke-virtual {v0, v15, v11, v12}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->isPaymentInRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v34

    move/from16 v33, v34

    .line 85
    new-instance v51, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-object/from16 v28, v51

    .line 95
    iget-object v0, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object/from16 v2, p3

    move/from16 v3, p5

    move-object/from16 v4, p4

    move-object/from16 v5, p10

    move-object/from16 v6, p11

    move/from16 v7, p12

    move/from16 v8, p14

    move/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->actionablePromptText(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    move-object/from16 v38, v0

    const-string v1, "renderer.actionablePromp\u2026CheckoutEnabled\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v14, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    .line 107
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getReaderPaymentsEnabled()Z

    move-result v0

    .line 113
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;->getPrimaryOptions()Ljava/util/List;

    move-result-object v21

    move-object v1, v15

    move v15, v0

    move/from16 v16, p5

    move-object/from16 v17, p4

    move-object/from16 v18, p10

    move-object/from16 v19, p11

    move-object/from16 v20, p7

    move-object/from16 v23, v10

    move/from16 v24, p14

    move/from16 v25, p8

    move/from16 v26, p24

    move-object/from16 v27, p25

    .line 106
    invoke-virtual/range {v14 .. v27}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->paymentPromptText(ZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/EnumSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLcom/squareup/CountryCode;)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    move-object/from16 v39, v0

    const-string v2, "renderer.paymentPromptTe\u2026userCountryCode\n        )"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    invoke-virtual {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->getQuickCashAmounts(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v40, v0

    const-string v1, "renderer.getQuickCashAmounts(displayedAmount)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object/from16 v1, p28

    invoke-virtual {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->cardsOnFile(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v41, v0

    const-string v1, "renderer.cardsOnFile(cus\u2026iftCardInstrumentDetails)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, v13, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->renderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;

    move-object/from16 v2, p27

    invoke-virtual {v0, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;->cardsOnFile(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v42, v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v44, 0x0

    .line 85
    invoke-direct/range {v28 .. v50}, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$TextData;ZZLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/tenderpayment/SelectMethod$TextData;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;JJLcom/squareup/payment/CardOptionEnabled;)V

    return-object v51
.end method
