.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->updateSelectMethodStateAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt$action$2\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n+ 3 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow\n*L\n1#1,211:1\n181#2:212\n1037#3,27:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0018\u0010\u0004\u001a\u00020\u0005*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0016\u00a8\u0006\u0007\u00b8\u0006\u0008"
    }
    d2 = {
        "com/squareup/workflow/WorkflowActionKt$action$2",
        "Lcom/squareup/workflow/WorkflowAction;",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "workflow-core",
        "com/squareup/workflow/WorkflowActionKt$action$$inlined$action$2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $buyerCheckoutEnabled$inlined:Z

.field final synthetic $isConnectedToNetwork$inlined:Z

.field final synthetic $isInOfflineMode$inlined:Z

.field final synthetic $isRunningState$inlined:Z

.field final synthetic $name$inlined:Ljava/lang/String;

.field final synthetic $readerCapabilities$inlined:Ljava/util/EnumSet;

.field final synthetic $startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic $state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$name$inlined:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    iput-boolean p4, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isConnectedToNetwork$inlined:Z

    iput-boolean p5, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isInOfflineMode$inlined:Z

    iput-boolean p6, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$buyerCheckoutEnabled$inlined:Z

    iput-boolean p7, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isRunningState$inlined:Z

    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$readerCapabilities$inlined:Ljava/util/EnumSet;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "$this$apply"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v3

    .line 216
    iget-boolean v9, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isConnectedToNetwork$inlined:Z

    .line 217
    iget-boolean v10, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isInOfflineMode$inlined:Z

    .line 218
    iget-boolean v14, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$buyerCheckoutEnabled$inlined:Z

    .line 219
    iget-boolean v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isRunningState$inlined:Z

    .line 220
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$startArgs$inlined:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    invoke-virtual {v4}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getReaderPaymentsEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 221
    iget-object v15, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 222
    invoke-virtual {v3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getPreAuthTipRequired()Z

    move-result v16

    .line 223
    invoke-virtual {v3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getTipEnabled()Z

    move-result v17

    .line 224
    iget-boolean v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isConnectedToNetwork$inlined:Z

    .line 225
    iget-boolean v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$isInOfflineMode$inlined:Z

    .line 226
    iget-boolean v6, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$buyerCheckoutEnabled$inlined:Z

    move/from16 v18, v4

    move/from16 v19, v5

    move/from16 v20, v6

    .line 221
    invoke-static/range {v15 .. v20}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$shouldEnableNfcField(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;ZZZZZ)Z

    move-result v4

    if-nez v4, :cond_0

    .line 229
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$readerCapabilities$inlined:Ljava/util/EnumSet;

    sget-object v6, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    check-cast v6, Ljava/lang/Enum;

    invoke-static {v4, v5, v6}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$minus(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Ljava/util/EnumSet;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    goto :goto_0

    .line 231
    :cond_0
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$readerCapabilities$inlined:Ljava/util/EnumSet;

    goto :goto_0

    .line 234
    :cond_1
    const-class v4, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v4}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v4

    :goto_0
    move-object v12, v4

    const-string v4, "if (startArgs.readerPaym\u2026class.java)\n            }"

    .line 220
    invoke-static {v12, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v18, 0x1a9f

    const/16 v19, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v17, v2

    .line 215
    invoke-static/range {v3 .. v19}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v2

    .line 238
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$state$inlined:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-virtual {v3, v2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->copySelectMethodState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WorkflowAction("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->$name$inlined:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
