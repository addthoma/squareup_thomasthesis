.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/ui/main/errors/PaymentEvent;)V
    .locals 1

    .line 527
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/TakeSwipePayment;->getSwipe()Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$processSwipe(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_0

    .line 528
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/TakeDipPayment;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$processEmvDip(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 529
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/TakeTapPayment;->getSmartPaymentResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$processContactlessPayment(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/ui/main/SmartPaymentResult;)V

    goto :goto_0

    .line 530
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ReportReaderIssue;->getIssue()Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$exitWithReaderIssue(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 531
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/main/errors/CardFailed;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/ui/main/errors/CardFailed;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/CardFailed;->getFailureReason()Lcom/squareup/ui/main/errors/PaymentError;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$setToastDataForCardError(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/ui/main/errors/PaymentError;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/ui/main/errors/PaymentEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;->call(Lcom/squareup/ui/main/errors/PaymentEvent;)V

    return-void
.end method
