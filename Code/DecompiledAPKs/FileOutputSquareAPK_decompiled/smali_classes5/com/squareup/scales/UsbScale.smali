.class public final Lcom/squareup/scales/UsbScale;
.super Ljava/lang/Object;
.source "UsbScale.kt"

# interfaces
.implements Lcom/squareup/scales/Scale;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/UsbScale$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUsbScale.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UsbScale.kt\ncom/squareup/scales/UsbScale\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,170:1\n19#2:171\n*E\n*S KotlinDebug\n*F\n+ 1 UsbScale.kt\ncom/squareup/scales/UsbScale\n*L\n63#1:171\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 +2\u00020\u0001:\u0001+B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0007J\u0010\u0010$\u001a\u00020!2\u0006\u0010%\u001a\u00020&H\u0002J\u0008\u0010\'\u001a\u00020!H\u0002J\u0006\u0010(\u001a\u00020!J\u0006\u0010)\u001a\u00020!J\u0012\u0010*\u001a\u0004\u0018\u00010#2\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001aX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001aX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/scales/UsbScale;",
        "Lcom/squareup/scales/Scale;",
        "usbScaleInterpreter",
        "Lcom/squareup/scales/UsbScaleInterpreter;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "usbDevice",
        "Landroid/hardware/usb/UsbDevice;",
        "realScaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "serial",
        "Lcom/felhr/usbserial/UsbSerialDevice;",
        "analytics",
        "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
        "(Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Lcom/felhr/usbserial/UsbSerialDevice;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V",
        "active",
        "",
        "connectedScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "consecutiveReadErrors",
        "",
        "lastScaleEvent",
        "Lcom/squareup/scales/ScaleEvent;",
        "scaleEventRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "scaleEvents",
        "Lio/reactivex/Observable;",
        "getScaleEvents",
        "()Lio/reactivex/Observable;",
        "unitOfMeasurement",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "getUnitOfMeasurement",
        "handleWeightResponse",
        "",
        "weightData",
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;",
        "isConnected",
        "manufacturer",
        "Lcom/squareup/scales/ScaleTracker$Manufacturer;",
        "isNotConnected",
        "onDisconnected",
        "readScaleData",
        "tryReadFromScale",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/scales/UsbScale$Companion;

.field public static final MAX_BRECKNELL_RESPONSE_LENGTH:I = 0x12

.field public static final NUM_CONSECUTIVE_ERRORS_TO_DISCONNECT:I = 0x5

.field public static final READ_FROM_SCALE_PERIOD_MILLIS:J = 0x3e8L

.field public static final USB_TIMEOUT_MILLIS:I = 0x1f4


# instance fields
.field private active:Z

.field private final analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

.field private connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

.field private consecutiveReadErrors:I

.field private lastScaleEvent:Lcom/squareup/scales/ScaleEvent;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

.field private final scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final scaleEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final serial:Lcom/felhr/usbserial/UsbSerialDevice;

.field private final unitOfMeasurement:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDevice:Landroid/hardware/usb/UsbDevice;

.field private final usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/scales/UsbScale$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/scales/UsbScale$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/scales/UsbScale;->Companion:Lcom/squareup/scales/UsbScale$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Lcom/felhr/usbserial/UsbSerialDevice;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V
    .locals 1

    const-string/jumbo v0, "usbScaleInterpreter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usbDevice"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serial"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/UsbScale;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    iput-object p2, p0, Lcom/squareup/scales/UsbScale;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p3, p0, Lcom/squareup/scales/UsbScale;->usbDevice:Landroid/hardware/usb/UsbDevice;

    iput-object p4, p0, Lcom/squareup/scales/UsbScale;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    iput-object p5, p0, Lcom/squareup/scales/UsbScale;->serial:Lcom/felhr/usbserial/UsbSerialDevice;

    iput-object p6, p0, Lcom/squareup/scales/UsbScale;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    const/4 p1, 0x1

    .line 51
    iput-boolean p1, p0, Lcom/squareup/scales/UsbScale;->active:Z

    .line 57
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/UsbScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 59
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hide()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "scaleEventRelay.hide()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/UsbScale;->scaleEvents:Lio/reactivex/Observable;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/scales/UsbScale;->getScaleEvents()Lio/reactivex/Observable;

    move-result-object p1

    .line 171
    const-class p2, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "ofType(T::class.java)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object p2, Lcom/squareup/scales/UsbScale$unitOfMeasurement$1;->INSTANCE:Lcom/squareup/scales/UsbScale$unitOfMeasurement$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "scaleEvents\n        .ofT\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/UsbScale;->unitOfMeasurement:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getRealScaleTracker$p(Lcom/squareup/scales/UsbScale;)Lcom/squareup/scales/RealScaleTracker;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/scales/UsbScale;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    return-object p0
.end method

.method public static final synthetic access$getScaleEventRelay$p(Lcom/squareup/scales/UsbScale;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/scales/UsbScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method private final isConnected(Lcom/squareup/scales/ScaleTracker$Manufacturer;)V
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 128
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "usbDevice.deviceName"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    sget-object v2, Lcom/squareup/scales/ScaleTracker$ConnectionType;->USB:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    const/4 v3, 0x0

    .line 127
    invoke-direct {v0, v1, v3, p1, v2}, Lcom/squareup/scales/ScaleTracker$HardwareScale;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/scales/UsbScale$isConnected$$inlined$also$lambda$1;

    invoke-direct {v1, v0, p0}, Lcom/squareup/scales/UsbScale$isConnected$$inlined$also$lambda$1;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UsbScale;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {p1, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    invoke-virtual {p1, v0}, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->logScaleConnected(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    .line 132
    iput-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-void
.end method

.method private final isNotConnected()V
    .locals 3

    .line 120
    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    if-eqz v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/scales/UsbScale$isNotConnected$$inlined$let$lambda$1;

    invoke-direct {v2, v0, p0}, Lcom/squareup/scales/UsbScale$isNotConnected$$inlined$let$lambda$1;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UsbScale;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 122
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    invoke-virtual {v1, v0}, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->logScaleDisconnected(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    :cond_0
    return-void
.end method

.method private final tryReadFromScale(Lcom/squareup/scales/ScaleTracker$Manufacturer;)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;
    .locals 5

    .line 103
    sget-object v0, Lcom/squareup/scales/UsbScale$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/scales/ScaleTracker$Manufacturer;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/16 v0, 0x12

    new-array v0, v0, [B

    .line 107
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->serial:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v2, p0, Lcom/squareup/scales/UsbScale;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    invoke-virtual {v2}, Lcom/squareup/scales/UsbScaleInterpreter;->getBrecknellWeightDataRequest()[B

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->syncWrite([BI)I

    move-result v1

    .line 108
    iget-object v2, p0, Lcom/squareup/scales/UsbScale;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    invoke-virtual {v2}, Lcom/squareup/scales/UsbScaleInterpreter;->getBrecknellWeightDataRequest()[B

    move-result-object v2

    array-length v2, v2

    const/4 v4, 0x0

    if-eq v1, v2, :cond_0

    return-object v4

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->serial:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-virtual {v1, v0, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->syncRead([BI)I

    move-result v1

    if-gtz v1, :cond_1

    return-object v4

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/scales/UsbScaleInterpreter;->decodeWeightRequest([BLcom/squareup/scales/ScaleTracker$Manufacturer;)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    move-result-object p1

    return-object p1

    .line 115
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported manufacturer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public getScaleEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->scaleEvents:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getUnitOfMeasurement()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->unitOfMeasurement:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final handleWeightResponse(Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;)V
    .locals 7

    const-string/jumbo v0, "weightData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    .line 146
    iget-object v2, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 147
    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    invoke-virtual {p1}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->getUnit()Lcom/squareup/scales/UnitOfMeasurement;

    move-result-object v3

    .line 148
    invoke-virtual {p1}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->getWeight()D

    move-result-wide v4

    .line 149
    invoke-virtual {p1}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->getNumberOfDecimalPlaces()I

    move-result v6

    move-object v1, v0

    .line 145
    invoke-direct/range {v1 .. v6}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)V

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto/16 :goto_0

    .line 151
    :cond_0
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellOverMaximumWeightResponse;

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->OVER_MAXIMUM_WEIGHT:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 152
    :cond_1
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnderMinimumWeightResponse;

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->UNDER_MINIMUM_WEIGHT:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 153
    :cond_2
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnstableReadingResponse;

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-direct {p1, v0}, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 154
    :cond_3
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;

    if-eqz v0, :cond_4

    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 155
    :cond_4
    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnitErrorResponse;

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->DECODING_ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 156
    :cond_5
    instance-of p1, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    if-eqz p1, :cond_6

    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->DECODING_ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    goto :goto_0

    .line 157
    :cond_6
    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    iget-object v0, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    sget-object v1, Lcom/squareup/scales/Status;->ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, v1}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/ScaleEvent;

    .line 160
    :goto_0
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/scales/UsbScale$handleWeightResponse$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/scales/UsbScale$handleWeightResponse$1;-><init>(Lcom/squareup/scales/UsbScale;Lcom/squareup/scales/ScaleEvent;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {p1, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->lastScaleEvent:Lcom/squareup/scales/ScaleEvent;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_8

    .line 165
    iget-object p1, p0, Lcom/squareup/scales/UsbScale;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    invoke-virtual {p1, v0}, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->logScaleReadingEvent(Lcom/squareup/scales/ScaleEvent;)V

    .line 166
    iput-object v0, p0, Lcom/squareup/scales/UsbScale;->lastScaleEvent:Lcom/squareup/scales/ScaleEvent;

    :cond_8
    return-void
.end method

.method public final onDisconnected()V
    .locals 1

    .line 139
    invoke-direct {p0}, Lcom/squareup/scales/UsbScale;->isNotConnected()V

    const/4 v0, 0x0

    .line 140
    iput-boolean v0, p0, Lcom/squareup/scales/UsbScale;->active:Z

    return-void
.end method

.method public final readScaleData()V
    .locals 3

    .line 73
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScale;->active:Z

    if-eqz v0, :cond_6

    .line 74
    sget-object v0, Lcom/squareup/scales/ScaleTracker$Manufacturer;->BRECKNELL:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    invoke-direct {p0, v0}, Lcom/squareup/scales/UsbScale;->tryReadFromScale(Lcom/squareup/scales/ScaleTracker$Manufacturer;)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    .line 78
    instance-of v1, v0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 85
    iput v1, p0, Lcom/squareup/scales/UsbScale;->consecutiveReadErrors:I

    goto :goto_2

    .line 83
    :cond_1
    :goto_1
    iget v1, p0, Lcom/squareup/scales/UsbScale;->consecutiveReadErrors:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/scales/UsbScale;->consecutiveReadErrors:I

    iget v1, p0, Lcom/squareup/scales/UsbScale;->consecutiveReadErrors:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    goto :goto_3

    :cond_2
    :goto_2
    if-eqz v0, :cond_4

    .line 90
    iget-object v1, p0, Lcom/squareup/scales/UsbScale;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    if-nez v1, :cond_3

    instance-of v1, v0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    if-nez v1, :cond_3

    .line 91
    sget-object v1, Lcom/squareup/scales/ScaleTracker$Manufacturer;->BRECKNELL:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    invoke-direct {p0, v1}, Lcom/squareup/scales/UsbScale;->isConnected(Lcom/squareup/scales/ScaleTracker$Manufacturer;)V

    .line 93
    :cond_3
    invoke-virtual {p0, v0}, Lcom/squareup/scales/UsbScale;->handleWeightResponse(Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;)V

    .line 96
    :cond_4
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScale;->active:Z

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    const-wide/16 v0, 0x3e8

    .line 97
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 99
    :cond_6
    :goto_3
    invoke-direct {p0}, Lcom/squareup/scales/UsbScale;->isNotConnected()V

    return-void
.end method
