.class public final Lcom/squareup/scales/RealLimitedScalesWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealLimitedScalesWorkflow.kt"

# interfaces
.implements Lcom/squareup/scales/ScalesWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealLimitedScalesWorkflow$Action;,
        Lcom/squareup/scales/RealLimitedScalesWorkflow$ShowingConnectedScalesLimitWarningScreen;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/scales/LimitedScalesState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/scales/ScalesWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLimitedScalesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLimitedScalesWorkflow.kt\ncom/squareup/scales/RealLimitedScalesWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,99:1\n149#2,5:100\n149#2,5:105\n149#2,5:110\n149#2,5:115\n41#3:120\n56#3,2:121\n276#4:123\n*E\n*S KotlinDebug\n*F\n+ 1 RealLimitedScalesWorkflow.kt\ncom/squareup/scales/RealLimitedScalesWorkflow\n*L\n49#1,5:100\n53#1,5:105\n58#1,5:110\n60#1,5:115\n74#1:120\n74#1,2:121\n74#1:123\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t0\u0002:\u0002#$B\u001f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u001f\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00032\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016\u00a2\u0006\u0002\u0010\u0019JM\u0010\u001a\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001dH\u0016\u00a2\u0006\u0002\u0010\u001eJ*\u0010\u001f\u001a\u00020 2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001d2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u0010\u0010\"\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u0004H\u0016R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/scales/RealLimitedScalesWorkflow;",
        "Lcom/squareup/scales/ScalesWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/scales/LimitedScalesState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/scales/ScalesScreen;",
        "noConnectedScalesWorkflow",
        "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
        "showingConnectedScalesWorkflow",
        "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
        "connectedScalesRepository",
        "Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;",
        "(Lcom/squareup/scales/NoConnectedScalesWorkflow;Lcom/squareup/scales/ShowingConnectedScalesWorkflow;Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;)V",
        "connectedScalesWorker",
        "Lcom/squareup/workflow/Worker;",
        "",
        "Lcom/squareup/connectedscalesdata/ConnectedScale;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/scales/LimitedScalesState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/scales/LimitedScalesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "showingConnectedScalesScreen",
        "Lcom/squareup/scales/ShowingConnectedScalesScreen;",
        "connectedScales",
        "snapshotState",
        "Action",
        "ShowingConnectedScalesLimitWarningScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectedScalesWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;>;"
        }
    .end annotation
.end field

.field private final noConnectedScalesWorkflow:Lcom/squareup/scales/NoConnectedScalesWorkflow;

.field private final showingConnectedScalesWorkflow:Lcom/squareup/scales/ShowingConnectedScalesWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/NoConnectedScalesWorkflow;Lcom/squareup/scales/ShowingConnectedScalesWorkflow;Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "noConnectedScalesWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showingConnectedScalesWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectedScalesRepository"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->noConnectedScalesWorkflow:Lcom/squareup/scales/NoConnectedScalesWorkflow;

    iput-object p2, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->showingConnectedScalesWorkflow:Lcom/squareup/scales/ShowingConnectedScalesWorkflow;

    .line 74
    invoke-interface {p3}, Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;->getConnectedScales()Lio/reactivex/Observable;

    move-result-object p1

    .line 120
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string/jumbo p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 122
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 123
    const-class p2, Ljava/util/List;

    sget-object p3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v0, Lcom/squareup/connectedscalesdata/ConnectedScale;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    invoke-virtual {p3, v0}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 120
    iput-object p3, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->connectedScalesWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 122
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final showingConnectedScalesScreen(Lcom/squareup/workflow/RenderContext;Ljava/util/List;)Lcom/squareup/scales/ShowingConnectedScalesScreen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/scales/LimitedScalesState;",
            "-",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;)",
            "Lcom/squareup/scales/ShowingConnectedScalesScreen;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->showingConnectedScalesWorkflow:Lcom/squareup/scales/ShowingConnectedScalesWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 71
    new-instance v3, Lcom/squareup/scales/ShowingConnectedScalesProps;

    invoke-direct {v3, p2}, Lcom/squareup/scales/ShowingConnectedScalesProps;-><init>(Ljava/util/List;)V

    .line 72
    sget-object p2, Lcom/squareup/scales/RealLimitedScalesWorkflow$showingConnectedScalesScreen$1;->INSTANCE:Lcom/squareup/scales/RealLimitedScalesWorkflow$showingConnectedScalesScreen$1;

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 69
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/scales/ShowingConnectedScalesScreen;

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/scales/LimitedScalesState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object p1, Lcom/squareup/scales/LimitedScalesState$NoConnectedScales;->INSTANCE:Lcom/squareup/scales/LimitedScalesState$NoConnectedScales;

    check-cast p1, Lcom/squareup/scales/LimitedScalesState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/RealLimitedScalesWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/scales/LimitedScalesState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/scales/LimitedScalesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/scales/RealLimitedScalesWorkflow;->render(Lkotlin/Unit;Lcom/squareup/scales/LimitedScalesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/scales/LimitedScalesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/scales/LimitedScalesState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/scales/LimitedScalesState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->connectedScalesWorker:Lcom/squareup/workflow/Worker;

    sget-object p1, Lcom/squareup/scales/RealLimitedScalesWorkflow$render$1;->INSTANCE:Lcom/squareup/scales/RealLimitedScalesWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 47
    instance-of p1, p2, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScales;

    const-string v0, ""

    if-eqz p1, :cond_0

    .line 48
    check-cast p2, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScales;

    invoke-virtual {p2}, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScales;->getConnectedScales()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p3, p1}, Lcom/squareup/scales/RealLimitedScalesWorkflow;->showingConnectedScalesScreen(Lcom/squareup/workflow/RenderContext;Ljava/util/List;)Lcom/squareup/scales/ShowingConnectedScalesScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 101
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 102
    const-class p3, Lcom/squareup/scales/ShowingConnectedScalesScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 103
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 101
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 50
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 51
    :cond_0
    instance-of p1, p2, Lcom/squareup/scales/LimitedScalesState$NoConnectedScales;

    if-eqz p1, :cond_1

    .line 52
    iget-object p1, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow;->noConnectedScalesWorkflow:Lcom/squareup/scales/NoConnectedScalesWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/scales/RealLimitedScalesWorkflow$render$2;->INSTANCE:Lcom/squareup/scales/RealLimitedScalesWorkflow$render$2;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 106
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 107
    const-class p3, Lcom/squareup/scales/NoConnectedScalesScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 108
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 106
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 54
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_1
    instance-of p1, p2, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScalesLimitWarning;

    if-eqz p1, :cond_2

    .line 56
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 57
    check-cast p2, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScalesLimitWarning;

    invoke-virtual {p2}, Lcom/squareup/scales/LimitedScalesState$ShowingConnectedScalesLimitWarning;->getLimitedScales()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p3, p1}, Lcom/squareup/scales/RealLimitedScalesWorkflow;->showingConnectedScalesScreen(Lcom/squareup/workflow/RenderContext;Ljava/util/List;)Lcom/squareup/scales/ShowingConnectedScalesScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 111
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 112
    const-class p2, Lcom/squareup/scales/ShowingConnectedScalesScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 113
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 111
    invoke-direct {v2, p2, p1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 59
    sget-object p1, Lcom/squareup/scales/RealLimitedScalesWorkflow$ShowingConnectedScalesLimitWarningScreen;->INSTANCE:Lcom/squareup/scales/RealLimitedScalesWorkflow$ShowingConnectedScalesLimitWarningScreen;

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 116
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 117
    const-class p2, Lcom/squareup/scales/RealLimitedScalesWorkflow$ShowingConnectedScalesLimitWarningScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 118
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 116
    invoke-direct {v5, p2, p1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 56
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/scales/LimitedScalesState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/scales/LimitedScalesState;

    invoke-virtual {p0, p1}, Lcom/squareup/scales/RealLimitedScalesWorkflow;->snapshotState(Lcom/squareup/scales/LimitedScalesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
