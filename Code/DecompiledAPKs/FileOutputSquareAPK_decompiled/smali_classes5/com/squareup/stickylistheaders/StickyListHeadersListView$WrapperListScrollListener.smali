.class Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;
.super Ljava/lang/Object;
.source "StickyListHeadersListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperListScrollListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;


# direct methods
.method private constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V
    .locals 0

    .line 528
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$700(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$700(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 536
    :cond_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$800(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/WrapperViewList;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$900(Lcom/squareup/stickylistheaders/StickyListHeadersListView;I)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .line 540
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$700(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$700(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method
