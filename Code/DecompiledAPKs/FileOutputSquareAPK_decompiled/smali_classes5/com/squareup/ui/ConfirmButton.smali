.class public Lcom/squareup/ui/ConfirmButton;
.super Landroid/widget/FrameLayout;
.source "ConfirmButton.java"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton;


# static fields
.field public static final FADE_DURATION_MS:J = 0x64L


# instance fields
.field private final button:Lcom/squareup/marketfont/MarketButton;

.field private confirm:Z

.field private final confirmBackgroundResId:I

.field private confirmText:Ljava/lang/CharSequence;

.field private final confirmTextColors:Landroid/content/res/ColorStateList;

.field private final initialBackgroundResId:I

.field private initialText:Ljava/lang/CharSequence;

.field private final initialTextColors:Landroid/content/res/ColorStateList;

.field private listener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

.field private final overlayView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    sget-object v0, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_initialStageText:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/ConfirmButton;->initialText:Ljava/lang/CharSequence;

    .line 52
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_initialStageBackground:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/ConfirmButton;->initialBackgroundResId:I

    .line 53
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_initialStageTextColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/ConfirmButton;->initialTextColors:Landroid/content/res/ColorStateList;

    .line 55
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_confirmStageText:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/ConfirmButton;->confirmText:Ljava/lang/CharSequence;

    .line 56
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_confirmStageBackground:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/ConfirmButton;->confirmBackgroundResId:I

    .line 57
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_confirmStageTextColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/ConfirmButton;->confirmTextColors:Landroid/content/res/ColorStateList;

    .line 59
    new-instance v1, Lcom/squareup/marketfont/MarketButton;

    invoke-direct {v1, p1}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    .line 61
    sget-object v1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton:[I

    sget v3, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_weight:I

    invoke-static {p1, p2, v1, v3, v2}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p2

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    const/16 v3, 0x11

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketButton;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 67
    iget-object p2, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/ConfirmButton$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ConfirmButton$1;-><init>(Lcom/squareup/ui/ConfirmButton;)V

    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object p2, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2}, Lcom/squareup/util/Views;->performClickOnUp(Landroid/view/View;)V

    .line 80
    new-instance p2, Landroid/widget/ImageView;

    invoke-direct {p2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 83
    sget p1, Lcom/squareup/sqwidgets/R$styleable;->ConfirmButton_buttonHeight:I

    const/4 p2, -0x2

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p1

    .line 85
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 87
    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p2, v0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ConfirmButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ConfirmButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    invoke-direct {p0, v2}, Lcom/squareup/ui/ConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ConfirmButton;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/squareup/ui/ConfirmButton;->confirm:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ConfirmButton;Z)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/ConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/ConfirmButton;->listener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ConfirmButton;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/ConfirmButton;->showConfirmButton()V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/ConfirmButton;)Landroid/widget/ImageView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    return-object p0
.end method

.method private animateTransition(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V
    .locals 3

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 135
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 136
    new-instance v1, Lcom/squareup/ui/ConfirmButton$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ConfirmButton$3;-><init>(Lcom/squareup/ui/ConfirmButton;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const-wide/16 v1, 0x64

    .line 141
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 142
    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->overlayView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 145
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/ConfirmButton;->updateButton(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    return-void
.end method

.method private announceConfirmButtonText()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketButton;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/accessibility/Accessibility;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private resetToInitialStage(Z)V
    .locals 2

    .line 168
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    if-eqz p1, :cond_0

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->initialText:Ljava/lang/CharSequence;

    iget v0, p0, Lcom/squareup/ui/ConfirmButton;->initialBackgroundResId:I

    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->initialTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/ConfirmButton;->animateTransition(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 172
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->initialText:Ljava/lang/CharSequence;

    iget v0, p0, Lcom/squareup/ui/ConfirmButton;->initialBackgroundResId:I

    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->initialTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/ConfirmButton;->updateButton(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    :goto_0
    const/4 p1, 0x0

    .line 174
    iput-boolean p1, p0, Lcom/squareup/ui/ConfirmButton;->confirm:Z

    return-void
.end method

.method private showConfirmButton()V
    .locals 3

    .line 115
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    new-instance v2, Lcom/squareup/ui/ConfirmButton$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ConfirmButton$2;-><init>(Lcom/squareup/ui/ConfirmButton;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->confirmText:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/ui/ConfirmButton;->confirmBackgroundResId:I

    iget-object v2, p0, Lcom/squareup/ui/ConfirmButton;->confirmTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/ConfirmButton;->animateTransition(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    .line 121
    invoke-direct {p0}, Lcom/squareup/ui/ConfirmButton;->announceConfirmButtonText()V

    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/squareup/ui/ConfirmButton;->confirm:Z

    return-void
.end method

.method private updateButton()V
    .locals 3

    .line 153
    iget-boolean v0, p0, Lcom/squareup/ui/ConfirmButton;->confirm:Z

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->confirmText:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/ui/ConfirmButton;->confirmBackgroundResId:I

    iget-object v2, p0, Lcom/squareup/ui/ConfirmButton;->confirmTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/ConfirmButton;->updateButton(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->initialText:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/ui/ConfirmButton;->initialBackgroundResId:I

    iget-object v2, p0, Lcom/squareup/ui/ConfirmButton;->initialTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/ConfirmButton;->updateButton(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V

    :goto_0
    return-void
.end method

.method private updateButton(Ljava/lang/CharSequence;ILandroid/content/res/ColorStateList;)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setBackgroundResource(I)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketButton;->requestLayout()V

    return-void
.end method


# virtual methods
.method public setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setConfirmText(Ljava/lang/CharSequence;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/ConfirmButton;->confirmText:Ljava/lang/CharSequence;

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/ConfirmButton;->updateButton()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 96
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ConfirmButton;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method public setInitialText(Ljava/lang/CharSequence;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/ConfirmButton;->initialText:Ljava/lang/CharSequence;

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/ConfirmButton;->updateButton()V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 178
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/ui/ConfirmButton;->listener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    return-void
.end method
