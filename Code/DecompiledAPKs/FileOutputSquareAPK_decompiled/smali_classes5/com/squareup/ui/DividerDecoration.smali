.class public Lcom/squareup/ui/DividerDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "DividerDecoration.java"


# instance fields
.field private final dividerHeight:I

.field private final drawable:Landroid/graphics/drawable/Drawable;

.field private ignorePadding:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 22
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    .line 23
    iput p1, p0, Lcom/squareup/ui/DividerDecoration;->dividerHeight:I

    const/4 p1, 0x0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p1, p2}, Lcom/squareup/ui/DividerDecoration;-><init>(III)V

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .line 33
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    .line 34
    iput p1, p0, Lcom/squareup/ui/DividerDecoration;->dividerHeight:I

    .line 35
    new-instance p1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 36
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 37
    invoke-virtual {p1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 38
    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float p2, p2

    .line 39
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 40
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    .line 47
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/DividerDecoration;->dividerHeight:I

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 0

    .line 77
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildPosition(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p4}, Landroidx/recyclerview/widget/RecyclerView$State;->getItemCount()I

    move-result p3

    add-int/lit8 p3, p3, -0x1

    const/4 p4, 0x0

    if-ne p2, p3, :cond_0

    .line 78
    invoke-virtual {p1, p4, p4, p4, p4}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    .line 82
    :cond_0
    iget p2, p0, Lcom/squareup/ui/DividerDecoration;->dividerHeight:I

    invoke-virtual {p1, p4, p4, p4, p2}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public ignorePadding(Z)V
    .locals 0

    .line 86
    iput-boolean p1, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 7

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$State;->getItemCount()I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    goto :goto_3

    .line 55
    :cond_0
    iget-boolean p3, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    const/4 v1, 0x0

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingLeft()I

    move-result p3

    .line 58
    :goto_0
    iget-boolean v2, p0, Lcom/squareup/ui/DividerDecoration;->ignorePadding:Z

    if-eqz v2, :cond_2

    .line 59
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getWidth()I

    move-result v2

    goto :goto_1

    .line 60
    :cond_2
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 62
    :goto_1
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result v3

    :goto_2
    add-int/lit8 v4, v3, -0x1

    if-ge v1, v4, :cond_3

    .line 64
    invoke-virtual {p2, v1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 65
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 66
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget v5, v5, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    .line 68
    iget v5, p0, Lcom/squareup/ui/DividerDecoration;->dividerHeight:I

    iget-object v6, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 69
    iget-object v6, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    add-int/2addr v4, v5

    .line 70
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v5, v4

    .line 69
    invoke-virtual {v6, p3, v4, v2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 71
    iget-object v4, p0, Lcom/squareup/ui/DividerDecoration;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    :goto_3
    return-void
.end method
