.class Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;
.super Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.source "CartContainerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;->this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHiding(Landroid/view/View;)V
    .locals 0

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;->this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    if-eqz p1, :cond_0

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;->this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowDown()V

    :cond_0
    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 0

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;->this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    if-eqz p1, :cond_0

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;->this$0:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowUp()V

    :cond_0
    return-void
.end method
