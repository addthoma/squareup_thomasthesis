.class final enum Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;
.super Ljava/lang/Enum;
.source "CartHeaderTabletView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/header/CartHeaderTabletView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "VisualState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

.field public static final enum CURRENT_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

.field public static final enum NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 27
    new-instance v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    const/4 v1, 0x0

    const-string v2, "NO_SALE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    .line 28
    new-instance v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    const/4 v2, 0x1

    const-string v3, "CURRENT_SALE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->CURRENT_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    .line 26
    sget-object v3, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->CURRENT_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->$VALUES:[Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;
    .locals 1

    .line 26
    const-class v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->$VALUES:[Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    invoke-virtual {v0}, [Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    return-object v0
.end method
