.class public Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;
.super Lmortar/ViewPresenter;
.source "DiningOptionViewPagerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/DiningOptionViewPager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final availableDiningOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation
.end field

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

.field private final locale:Ljava/util/Locale;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/DiningOptionCache;Ljava/util/Locale;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->locale:Ljava/util/Locale;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private updateAvailableDiningOptions()V
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    invoke-virtual {v1}, Lcom/squareup/ui/main/DiningOptionCache;->getDiningOptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    const-string v1, "The cart must have a dining option when showing dining options."

    .line 112
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method currentDiningOptionIndex()I
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    const-string v1, "The cart must have a dining option when showing dining options."

    .line 87
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 88
    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method diningOptionCount()I
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getAvailableDiningOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    return-object v0
.end method

.method getDiningOptionDisplayName(I)Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p1}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$DiningOptionViewPagerPresenter(Lcom/squareup/ui/main/DiningOptionCache$Update;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->onAvailableDiningOptionChanged()V

    return-void
.end method

.method onAvailableDiningOptionChanged()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-direct {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->updateAvailableDiningOptions()V

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/DiningOptionViewPager;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->updateAdapter()V

    :cond_0
    return-void
.end method

.method onCurrentDiningOptionChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 73
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->updateAvailableDiningOptions()V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/DiningOptionViewPager;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/DiningOptionViewPager;->updateAdapter()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$S3ePA-jbPVR0gfjtAEOBMh43zcU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$S3ePA-jbPVR0gfjtAEOBMh43zcU;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;)V

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/ui/main/DiningOptionCache$Update;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$DiningOptionViewPagerPresenter$KndO4GyuyGR-kRZbmX57HTNLELw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$DiningOptionViewPagerPresenter$KndO4GyuyGR-kRZbmX57HTNLELw;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;)V

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 49
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 54
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->updateAvailableDiningOptions()V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/DiningOptionViewPager;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/DiningOptionViewPager;->installAdapter()V

    return-void
.end method

.method setCurrentDiningOption(I)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/DiningOption;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/DiningOption;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->setDiningOption(Lcom/squareup/checkout/DiningOption;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_CART_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :cond_0
    return-void
.end method
