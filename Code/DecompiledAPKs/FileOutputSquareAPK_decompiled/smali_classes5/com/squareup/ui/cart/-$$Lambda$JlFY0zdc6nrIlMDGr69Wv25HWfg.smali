.class public final synthetic Lcom/squareup/ui/cart/-$$Lambda$JlFY0zdc6nrIlMDGr69Wv25HWfg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/-$$Lambda$JlFY0zdc6nrIlMDGr69Wv25HWfg;->f$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/-$$Lambda$JlFY0zdc6nrIlMDGr69Wv25HWfg;->f$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    check-cast p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method
