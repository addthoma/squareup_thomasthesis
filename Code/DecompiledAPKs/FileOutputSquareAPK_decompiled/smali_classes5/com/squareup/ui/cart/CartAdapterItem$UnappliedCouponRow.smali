.class public Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnappliedCouponRow"
.end annotation


# instance fields
.field public final couponName:Ljava/lang/String;

.field public final couponToken:Ljava/lang/String;

.field public final props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V
    .locals 0

    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->couponName:Ljava/lang/String;

    .line 324
    iput-object p2, p0, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->couponToken:Ljava/lang/String;

    .line 325
    iput-object p3, p0, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->couponToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 329
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->UNAPPLIED_COUPON_ROW:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
