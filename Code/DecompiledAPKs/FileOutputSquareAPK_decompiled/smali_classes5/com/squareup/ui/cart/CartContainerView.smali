.class public Lcom/squareup/ui/cart/CartContainerView;
.super Landroid/widget/LinearLayout;
.source "CartContainerView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/VisualTransitionListener;


# instance fields
.field presenter:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const-class p2, Lcom/squareup/ui/cart/CartContainerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartContainerScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/cart/CartContainerScreen$Component;->inject(Lcom/squareup/ui/cart/CartContainerView;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 43
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerView;->presenter:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->dropView(Lcom/squareup/ui/cart/CartContainerView;)V

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/orderentry/R$id;->cart_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 32
    sget v1, Lcom/squareup/orderentry/R$layout;->cart_recycler_view:I

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerView;->presenter:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerView;->presenter:Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method
