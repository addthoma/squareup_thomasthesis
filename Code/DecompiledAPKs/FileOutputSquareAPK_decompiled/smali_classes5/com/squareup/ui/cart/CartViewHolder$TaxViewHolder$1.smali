.class Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->access$200(Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onTaxRowClicked(I)V

    return-void
.end method
