.class Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CartRecyclerViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->maybeRemoveFromCart(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field final synthetic val$orderItemIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;I)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;->this$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iput p2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;->val$orderItemIndex:I

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;->this$0:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->access$000(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;->val$orderItemIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->removeItem(I)V

    return-void
.end method
