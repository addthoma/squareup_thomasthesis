.class public interface abstract Lcom/squareup/ui/cart/CartEntryViewModelFactory;
.super Ljava/lang/Object;
.source "CartEntryViewModelFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u000f\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J*\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH&J \u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH&J \u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J \u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH&J+\u0010\u0015\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\n\u0008\u0001\u0010\u0016\u001a\u0004\u0018\u00010\u00082\u0006\u0010\u0017\u001a\u00020\u0018H&\u00a2\u0006\u0002\u0010\u0019J:\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u001b\u001a\u00020\n2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u001d\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u001e\u001a\u00020\nH&J\"\u0010\u001f\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH&J\u0018\u0010 \u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u00182\u0006\u0010\u0004\u001a\u00020\u0005H&J(\u0010!\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\nH&J*\u0010!\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\nH&J\"\u0010#\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH&J\"\u0010$\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\nH&J\u001a\u0010%\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010&\u001a\u00020\u0018H&\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
        "",
        "comp",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "discount",
        "labelId",
        "",
        "enabled",
        "",
        "showWarning",
        "discountItem",
        "label",
        "",
        "emptyCustomAmount",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "width",
        "lineItem",
        "name",
        "loyaltyPoints",
        "subLabelId",
        "formattedValue",
        "",
        "(ILjava/lang/Integer;Ljava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;",
        "orderItem",
        "allowGlyph",
        "cartDiningOption",
        "wrapModifierList",
        "canSeeSeating",
        "subTotal",
        "surcharge",
        "taxes",
        "forceShowAmount",
        "tip",
        "total",
        "unappliedCoupon",
        "subtitle",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract discount(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract discountItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract emptyCustomAmount(Lcom/squareup/checkout/CartItem;IZ)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract lineItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract loyaltyPoints(ILjava/lang/Integer;Ljava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract orderItem(Lcom/squareup/checkout/CartItem;ZLjava/lang/String;ZZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract subTotal(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract surcharge(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract taxes(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract taxes(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract tip(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract total(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method

.method public abstract unappliedCoupon(ILjava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;
.end method
