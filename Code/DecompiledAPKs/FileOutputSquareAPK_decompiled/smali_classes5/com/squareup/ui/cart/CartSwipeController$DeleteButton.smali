.class Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;
.super Ljava/lang/Object;
.source "CartSwipeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartSwipeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeleteButton"
.end annotation


# instance fields
.field private final clickRegion:Landroid/graphics/RectF;

.field private final paint:Landroid/graphics/Paint;

.field private final position:I

.field private final presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;


# direct methods
.method constructor <init>(Landroid/text/TextPaint;Landroid/graphics/Rect;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Landroid/graphics/Canvas;Landroid/view/View;FI)V
    .locals 2

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->paint:Landroid/graphics/Paint;

    .line 190
    iput-object p3, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 191
    iput p7, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->position:I

    .line 193
    invoke-virtual {p5}, Landroid/view/View;->getRight()I

    move-result p3

    int-to-float p3, p3

    .line 194
    new-instance p7, Landroid/graphics/RectF;

    add-float/2addr p6, p3

    invoke-virtual {p5}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p5}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p7, p6, v0, p3, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object p7, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->clickRegion:Landroid/graphics/RectF;

    .line 197
    invoke-virtual {p5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 198
    iget-object p6, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->paint:Landroid/graphics/Paint;

    sget p7, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-static {p3, p7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p6, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 199
    iget-object p3, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->clickRegion:Landroid/graphics/RectF;

    iget-object p6, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->paint:Landroid/graphics/Paint;

    invoke-virtual {p4, p3, p6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 202
    invoke-virtual {p5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    .line 203
    sget p5, Lcom/squareup/common/strings/R$string;->delete:I

    invoke-virtual {p3, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 204
    iget-object p5, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->clickRegion:Landroid/graphics/RectF;

    invoke-virtual {p5}, Landroid/graphics/RectF;->centerX()F

    move-result p5

    iget-object p6, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->clickRegion:Landroid/graphics/RectF;

    .line 205
    invoke-virtual {p6}, Landroid/graphics/RectF;->centerY()F

    move-result p6

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p2

    sub-float/2addr p6, p2

    .line 204
    invoke-virtual {p4, p3, p5, p6, p1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method


# virtual methods
.method public onClick(FF)Z
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->clickRegion:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget p2, p0, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->position:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->maybeRemoveFromCart(I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
