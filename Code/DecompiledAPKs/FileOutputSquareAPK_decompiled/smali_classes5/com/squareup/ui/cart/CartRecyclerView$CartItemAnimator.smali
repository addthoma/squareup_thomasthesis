.class Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;
.super Landroidx/recyclerview/widget/SimpleItemAnimator;
.source "CartRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CartItemAnimator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartRecyclerView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/cart/CartRecyclerView;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->this$0:Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/SimpleItemAnimator;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartRecyclerView$1;)V
    .locals 0

    .line 131
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;-><init>(Lcom/squareup/ui/cart/CartRecyclerView;)V

    return-void
.end method

.method private animateCartItem(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 2

    .line 186
    check-cast p2, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    .line 187
    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->getEntryView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v0

    .line 189
    instance-of v1, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    iget-boolean v1, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->recoveringSwipe:Z

    if-eqz v1, :cond_0

    const/4 p2, 0x0

    .line 192
    iput-boolean p2, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->recoveringSwipe:Z

    goto :goto_0

    .line 195
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->isDiscountChanged()Z

    move-result p1

    if-nez p1, :cond_2

    .line 196
    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->isCustomAmount()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 197
    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->isKeypadCommitted()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 200
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartEntryView;->pulseAmount()V

    goto :goto_0

    .line 203
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartEntryView;->pulsePreservedLabel()V

    :cond_2
    :goto_0
    return-void
.end method

.method private animateDiscountItem(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    .line 210
    check-cast p1, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    .line 211
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->isDiscountEvent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->getEntryView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryView;->pulseTitle()V

    :cond_0
    return-void
.end method


# virtual methods
.method public animateAdd(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 2

    .line 139
    instance-of v0, p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    if-eqz v0, :cond_0

    .line 140
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    .line 141
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->isCustomAmount()Z

    move-result v1

    if-nez v1, :cond_1

    .line 143
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->getEntryView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartEntryView;->fadeIn()V

    goto :goto_0

    .line 145
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    if-eqz v0, :cond_1

    .line 146
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->getEntryView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartEntryView;->fadeIn()V

    .line 149
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->dispatchAddFinished(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    const/4 p1, 0x0

    return p1
.end method

.method public animateChange(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 0

    .line 161
    instance-of p3, p2, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    if-eqz p3, :cond_0

    .line 162
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->animateCartItem(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto :goto_0

    .line 163
    :cond_0
    instance-of p3, p2, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    if-eqz p3, :cond_1

    .line 164
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->animateDiscountItem(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    :cond_1
    :goto_0
    const/4 p3, 0x1

    .line 167
    invoke-virtual {p0, p1, p3}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->dispatchChangeFinished(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    const/4 p1, 0x0

    .line 168
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->dispatchChangeFinished(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    return p1
.end method

.method public animateMove(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;IIII)Z
    .locals 0

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->dispatchMoveFinished(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    const/4 p1, 0x0

    return p1
.end method

.method public animateRemove(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;->dispatchRemoveFinished(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    const/4 p1, 0x0

    return p1
.end method

.method public endAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    return-void
.end method

.method public endAnimations()V
    .locals 0

    return-void
.end method

.method public isRunning()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public runPendingAnimations()V
    .locals 0

    return-void
.end method
