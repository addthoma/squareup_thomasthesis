.class public Lcom/squareup/ui/balance/BalanceMasterScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "BalanceMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/balance/BalanceApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;,
        Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;,
        Lcom/squareup/ui/balance/BalanceMasterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/balance/BalanceMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/BalanceMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen;->INSTANCE:Lcom/squareup/ui/balance/BalanceMasterScreen;

    .line 65
    sget-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen;->INSTANCE:Lcom/squareup/ui/balance/BalanceMasterScreen;

    .line 66
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 51
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->balance_applet_master_view:I

    return v0
.end method
