.class public final Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;
.super Ljava/lang/Object;
.source "BalanceAppletSectionsListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/balance/BalanceAppletSectionsListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSectionsListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView;",
            ">;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->presenter:Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletSectionsListView_MembersInjector;->injectPresenter(Lcom/squareup/applet/AppletSectionsListView;Lcom/squareup/applet/AppletSectionsListPresenter;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->injectMembers(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)V

    return-void
.end method
