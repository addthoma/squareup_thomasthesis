.class abstract Lcom/squareup/ui/balance/BalanceAppletScopeModule$OrderSquareCardModule;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceAppletScopeModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "OrderSquareCardModule"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCorrectedAddressConfiguration()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->order_card_label:I

    sget v2, Lcom/squareup/common/strings/R$string;->shipping_details:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;-><init>(II)V

    return-object v0
.end method

.method static provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 179
    new-instance v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->send_square_card_confirmation_subheading:I

    invoke-direct {v0, v1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;-><init>(I)V

    return-object v0
.end method

.method static provideOrderCoordinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    .locals 4
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 156
    new-instance v0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->order_card_label:I

    sget v2, Lcom/squareup/common/strings/R$string;->shipping_details:I

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->send_square_card_subheading:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(III)V

    return-object v0
.end method

.method static provideOrderReactorConfiguration()Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$Configuration;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v1}, Lcom/squareup/mailorder/OrderReactor$Configuration;-><init>(ZZ)V

    return-object v0
.end method

.method static provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 163
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->order_card_label:I

    sget v2, Lcom/squareup/common/strings/R$string;->shipping_details:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method abstract bindsAddMoneyAnalytics(Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;)Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindsOrderWorkflow(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/mailorder/OrderScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
