.class public Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TransferToBankCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private amountHint:Lcom/squareup/widgets/MessageView;

.field private amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

.field private feeRate:Lcom/squareup/marketfont/MarketTextView;

.field private feeRateTextWithNoAmount:Ljava/lang/String;

.field private instantText:Lcom/squareup/marketfont/MarketTextView;

.field private isFeeFixedAmount:Z

.field private final minTransfer:Lcom/squareup/protos/common/Money;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private moneyLocalScrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private netToBank:Landroid/view/ViewGroup;

.field private res:Landroid/content/res/Resources;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

.field private transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

.field private transferSpeedInstantGroup:Landroid/view/ViewGroup;

.field private transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

.field private final zeroBalance:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    const-string v0, ""

    .line 60
    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRateTextWithNoAmount:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    .line 66
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 67
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    const-wide/16 v0, 0x0

    .line 68
    invoke-static {v0, v1, p4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->zeroBalance:Ljava/lang/CharSequence;

    const-wide/16 p1, 0x64

    .line 69
    invoke-static {p1, p2, p4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->minTransfer:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/widgets/OnScreenRectangleEditText;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Z
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->primaryButtonEnabled()Z

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;Ljava/lang/CharSequence;)Z
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->shouldDisplayFeeAmount(Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRateTextWithNoAmount:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRate:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method

.method private addOnCheckedChangeListeners()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$ShjdOePGKXsE9gIkET7EuBja3Es;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$ShjdOePGKXsE9gIkET7EuBja3Es;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$Dn9TNZtO7CsA5P3wUVrSNLlFr8U;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$Dn9TNZtO7CsA5P3wUVrSNLlFr8U;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 206
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 207
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->netToBank:Landroid/view/ViewGroup;

    .line 208
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->amount_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/OnScreenRectangleEditText;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 209
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->amount_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountHint:Lcom/squareup/widgets/MessageView;

    .line 210
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_speed_instant_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstantGroup:Landroid/view/ViewGroup;

    .line 211
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_speed_instant_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->instantText:Lcom/squareup/marketfont/MarketTextView;

    .line 212
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_speed_instant_fee_rate:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRate:Lcom/squareup/marketfont/MarketTextView;

    .line 213
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_speed_instant:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinRadioButton;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    .line 214
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_speed_standard:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinRadioButton;

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

    return-void
.end method

.method private configureActionBar()V
    .locals 4

    .line 89
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->deposit_to_bank:I

    .line 90
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$d6FB-D6qroBqw2Wkmzxa46xx_pY;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$d6FB-D6qroBqw2Wkmzxa46xx_pY;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;)V

    .line 91
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    .line 92
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$qv6E6ZU_oLy5X-kSkMuQvLMrWXE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$qv6E6ZU_oLy5X-kSkMuQvLMrWXE;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    .line 93
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->primaryButtonEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private hasValidDesiredDeposit(Ljava/lang/CharSequence;)Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->minTransfer:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$6BtYqdaOu7J2iFdkstrKIPa9oCk(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;)V

    return-void
.end method

.method private onAmountValueFocusChanged(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->netToBank:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/applet/impl/R$drawable;->amount_border_focused:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 112
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->netToBank:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/applet/impl/R$drawable;->amount_border:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method private onInstantClicked()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;->onInstantClicked()V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;)V
    .locals 4

    .line 117
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->resetOnCheckedChangeListeners(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountHint:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->transfer_speed_hint:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedMaxDeposit:Ljava/lang/CharSequence;

    const-string v3, "max_amount"

    .line 120
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 119
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->instantDepositClickable:Z

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinRadioButton;->setEnabled(Z)V

    .line 125
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->instantText:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 126
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRate:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->instantText:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->instantText:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/marin/R$color;->marin_light_gray:I

    .line 131
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 130
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 135
    :goto_0
    new-instance v0, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->maxDeposit:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 137
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocalScrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v1}, Lcom/squareup/text/ScrubbingTextWatcher;->clearAddedScrubbers()V

    .line 138
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocalScrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->transfer_speed_instant_fee_rate:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 141
    iget-boolean v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->isFeeFixedAmount:Z

    iput-boolean v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->isFeeFixedAmount:Z

    .line 143
    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedFee:Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedFee:Ljava/lang/CharSequence;

    const-string v2, "fee_rate"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 146
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v0, ""

    .line 148
    :goto_1
    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRateTextWithNoAmount:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->shouldDisplayFeeAmount(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 150
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->transfer_speed_instant_fee_amount:I

    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedFeeTotalAmount:Ljava/lang/CharSequence;

    const-string v3, "fee_amount"

    .line 154
    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 156
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->feeRate:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstantGroup:Landroid/view/ViewGroup;

    iget-boolean p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->showInstantDeposit:Z

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private primaryButtonEnabled()Z
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->hasValidDesiredDeposit(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinRadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinRadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private removeOnCheckedChangeListeners()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private resetOnCheckedChangeListeners(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V
    .locals 4

    .line 179
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->removeOnCheckedChangeListeners()V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->setChecked(Z)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedStandard:Lcom/squareup/marin/widgets/MarinRadioButton;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->STANDARD:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinRadioButton;->setChecked(Z)V

    .line 182
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->addOnCheckedChangeListeners()V

    return-void
.end method

.method private shouldDisplayFeeAmount(Ljava/lang/CharSequence;)Z
    .locals 1

    .line 173
    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->isFeeFixedAmount:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    .line 174
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinRadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->hasValidDesiredDeposit(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->bindViews(Landroid/view/View;)V

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->res:Landroid/content/res/Resources;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/MoneyLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocalScrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->configureActionBar()V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->zeroBalance:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$k9O4OQ6kZILKJqSvoELSQnIJZC8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$k9O4OQ6kZILKJqSvoELSQnIJZC8;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$1;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$4_YxRpJHBdNGmRQa9FvNiURZYx4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$4_YxRpJHBdNGmRQa9FvNiURZYx4;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$addOnCheckedChangeListeners$3$TransferToBankCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->onInstantClicked()V

    .line 190
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->primaryButtonEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$addOnCheckedChangeListeners$4$TransferToBankCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;->onStandardClicked()V

    .line 196
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->primaryButtonEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$attach$0$TransferToBankCoordinator(Landroid/view/View;Z)V
    .locals 0

    .line 81
    invoke-direct {p0, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->onAmountValueFocusChanged(Z)V

    return-void
.end method

.method public synthetic lambda$attach$1$TransferToBankCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;->transferToBankScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$6BtYqdaOu7J2iFdkstrKIPa9oCk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferToBankCoordinator$6BtYqdaOu7J2iFdkstrKIPa9oCk;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$configureActionBar$2$TransferToBankCoordinator()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->amountValue:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->transferSpeedInstant:Lcom/squareup/marin/widgets/MarinRadioButton;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinRadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->STANDARD:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    .line 96
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    invoke-interface {v2, v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;->onNextClickedFromTransferToBank(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    return-void
.end method
