.class public Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ConfirmTransferCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;,
        Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$DepositsSettingsLinkSpan;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

.field private context:Landroid/content/Context;

.field private depositAccount:Lcom/squareup/widgets/MessageView;

.field private depositHint:Lcom/squareup/widgets/MessageView;

.field private desiredDeposit:Lcom/squareup/marketfont/MarketTextView;

.field private final idempotenceKey:Ljava/lang/String;

.field private instantTransferFee:Lcom/squareup/marketfont/MarketTextView;

.field private instantTransferFeeSection:Landroid/view/ViewGroup;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private remainingBalance:Lcom/squareup/marketfont/MarketTextView;

.field private res:Landroid/content/res/Resources;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

.field private final settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

.field private startingBalance:Lcom/squareup/marketfont/MarketTextView;

.field private transferArrivesSpeed:Lcom/squareup/widgets/MessageView;

.field private transferringToBank:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/SettingsAppletGateway;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    .line 59
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "-"

    const-string p3, ""

    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->idempotenceKey:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)Lcom/squareup/ui/settings/SettingsAppletGateway;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)Lcom/squareup/permissions/PermissionGatekeeper;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 167
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 168
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->desired_deposit:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->desiredDeposit:Lcom/squareup/marketfont/MarketTextView;

    .line 169
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_arrives_speed:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->transferArrivesSpeed:Lcom/squareup/widgets/MessageView;

    .line 170
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->deposit_account:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositAccount:Lcom/squareup/widgets/MessageView;

    .line 171
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->deposit_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositHint:Lcom/squareup/widgets/MessageView;

    .line 172
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->starting_balance:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->startingBalance:Lcom/squareup/marketfont/MarketTextView;

    .line 173
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transferring_to_bank:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->transferringToBank:Lcom/squareup/marketfont/MarketTextView;

    .line 174
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->instant_transfer_fee_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->instantTransferFeeSection:Landroid/view/ViewGroup;

    .line 175
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->instant_transfer_fee:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->instantTransferFee:Lcom/squareup/marketfont/MarketTextView;

    .line 176
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->remaining_balance:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->remainingBalance:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static synthetic lambda$t9fW8j7BGfsF6wwnsYVuHi0WQ5w(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V

    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$DZx0-M641Z8i-xVjO1cxo1uFMqc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$DZx0-M641Z8i-xVjO1cxo1uFMqc;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->desiredDeposit:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->startingBalance:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->balance:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->transferringToBank:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->res:Landroid/content/res/Resources;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->updateTransferAndDepositInfo(Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->updateInstantTransferFeeSection(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->updateRemainingBalance(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V

    return-void
.end method

.method private onTransfer()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->idempotenceKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;->onTransferClicked(Ljava/lang/String;)V

    return-void
.end method

.method private updateInstantTransferFeeSection(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V
    .locals 3

    .line 125
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->isInstant:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->instantTransferFee:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->feeAmount:Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->instantTransferFeeSection:Landroid/view/ViewGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private updateRemainingBalance(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V
    .locals 2

    .line 117
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->balance:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 118
    iget-boolean v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->isInstant:Z

    if-eqz v1, :cond_0

    .line 119
    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->feeAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 121
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->remainingBalance:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTransferAndDepositInfo(Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;)V
    .locals 4

    .line 90
    iget-boolean v0, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->isInstant:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->transferArrivesSpeed:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->transfer_arrives_instantly:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->transferArrivesSpeed:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->transfer_arrives_one_to_two_business_days:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 95
    :goto_0
    iget-boolean v0, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->isInstant:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->instantDepositRequiresLinkedCard:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositAccount:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_linked_card_hint:I

    .line 97
    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget v2, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->brandNameId:I

    .line 98
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "card_brand"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p2, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->panSuffix:Ljava/lang/String;

    const-string v2, "card_suffix"

    .line 99
    invoke-virtual {v1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 100
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 96
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositHint:Lcom/squareup/widgets/MessageView;

    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->debit_card_hint:I

    const-string v2, "deposits_settings"

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->deposits_settings:I

    .line 103
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$DepositsSettingsLinkSpan;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$DepositsSettingsLinkSpan;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;Landroid/content/res/Resources;)V

    .line 104
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 101
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositAccount:Lcom/squareup/widgets/MessageView;

    iget-object p2, p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;->bankAccount:Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->depositHint:Lcom/squareup/widgets/MessageView;

    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->bank_account_hint:I

    const-string v2, "bank_account_settings"

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->bank_account_settings:I

    .line 110
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;Landroid/content/res/Resources;)V

    .line 111
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 108
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->bindViews(Landroid/view/View;)V

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->context:Landroid/content/Context;

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->res:Landroid/content/res/Resources;

    .line 67
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->confirm_transfer_title:I

    .line 68
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$FA1s0pc5lDFC7TIn9A1mFKatqeo;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$FA1s0pc5lDFC7TIn9A1mFKatqeo;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;)V

    .line 69
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->confirm_transfer_button:I

    .line 70
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 72
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$Faj9408sqcbXbi1N1rKBW-5fyrQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$Faj9408sqcbXbi1N1rKBW-5fyrQ;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$ConfirmTransferCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;->confirmTransferScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$t9fW8j7BGfsF6wwnsYVuHi0WQ5w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$ConfirmTransferCoordinator$t9fW8j7BGfsF6wwnsYVuHi0WQ5w;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)V

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onScreenData$1$ConfirmTransferCoordinator()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->onTransfer()V

    return-void
.end method
