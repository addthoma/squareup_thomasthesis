.class public final Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalyticsKt;
.super Ljava/lang/Object;
.source "RealAddMoneyAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "ADD_MONEY_BUTTON_CLICK",
        "",
        "ADD_MONEY_CONFIRM_CLICK",
        "ADD_MONEY_DEPOSIT_SETTINGS_CLICK",
        "ADD_MONEY_FAILURE_SCREEN",
        "ADD_MONEY_SEE_BUTTON",
        "ADD_MONEY_SUCCESS_SCREEN",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ADD_MONEY_BUTTON_CLICK:Ljava/lang/String; = "Deposits: Add Money"

.field private static final ADD_MONEY_CONFIRM_CLICK:Ljava/lang/String; = "Deposits: Add Money Confirm Amount"

.field private static final ADD_MONEY_DEPOSIT_SETTINGS_CLICK:Ljava/lang/String; = "Deposits: Add Money Deposit Settings"

.field private static final ADD_MONEY_FAILURE_SCREEN:Ljava/lang/String; = "Deposits: Add Money Error"

.field private static final ADD_MONEY_SEE_BUTTON:Ljava/lang/String; = "Deposits: Add Money Button"

.field private static final ADD_MONEY_SUCCESS_SCREEN:Ljava/lang/String; = "Deposits: Add Money Success"
