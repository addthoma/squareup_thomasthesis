.class abstract Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;
.super Ljava/lang/Object;
.source "RealSubmitAmountWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$FinishWith;,
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;,
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;,
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "AmountAddedSuccessfully",
        "AmountCouldNotBeAdded",
        "FinishWith",
        "GenericFailure",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$FinishWith;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 218
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
            ">;)",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$FinishWith;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$FinishWith;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$FinishWith;->getResult()Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;

    move-result-object v1

    goto :goto_0

    .line 222
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;

    if-eqz v0, :cond_1

    .line 223
    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingSuccessScreen;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;

    invoke-virtual {v2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingSuccessScreen;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 226
    :cond_1
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;

    if-eqz v0, :cond_2

    .line 227
    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;

    invoke-virtual {v2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 230
    :cond_2
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :goto_0
    return-object v1

    .line 232
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 218
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
