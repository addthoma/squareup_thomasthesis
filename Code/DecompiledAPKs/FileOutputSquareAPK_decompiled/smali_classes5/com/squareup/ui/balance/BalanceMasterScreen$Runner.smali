.class public interface abstract Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;
.super Ljava/lang/Object;
.source "BalanceMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract balanceHeaderScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onAddMoneyClicked()V
.end method

.method public abstract onCardSpendClicked()V
.end method

.method public abstract onDepositRowClicked(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
.end method

.method public abstract onManageSquareCardClicked()V
.end method

.method public abstract onRefreshBalanceClicked()V
.end method

.method public abstract onSectionSelected()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/AppletSection;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onTransferReportsClicked()V
.end method

.method public abstract onTransferToBankClicked(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V
.end method
