.class public final Lcom/squareup/ui/balance/BalanceAppletSectionsList;
.super Lcom/squareup/applet/AppletSectionsList;
.source "BalanceAppletSectionsList.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BO\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletSectionsList;",
        "Lcom/squareup/applet/AppletSectionsList;",
        "res",
        "Lcom/squareup/util/Res;",
        "balanceAppletEntryPoint",
        "Lcom/squareup/ui/balance/BalanceAppletEntryPoint;",
        "squareCardActivitySection",
        "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
        "transferReportsSection",
        "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
        "manageSquareCardSection",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
        "capitalFlexLoanSection",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "cardUpseller",
        "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
        "unifiedActivitySection",
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
        "capitalFlexLoanAnalytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceAppletEntryPoint;Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAppletEntryPoint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardActivitySection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferReportsSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manageSquareCardSection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanSection"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardUpseller"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unifiedActivitySection"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanAnalytics"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p2, Lcom/squareup/applet/AppletEntryPoint;

    invoke-direct {p0, p2}, Lcom/squareup/applet/AppletSectionsList;-><init>(Lcom/squareup/applet/AppletEntryPoint;)V

    .line 31
    invoke-virtual {p8}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;->showUnifiedActivitySection()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 32
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;->visibleEntries:Ljava/util/List;

    .line 33
    new-instance p3, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 34
    check-cast p8, Lcom/squareup/applet/AppletSection;

    sget p4, Lcom/squareup/balance/applet/impl/R$string;->unified_activity_section_label:I

    .line 33
    invoke-direct {p3, p8, p4, p1}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    .line 32
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 39
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;->visibleEntries:Ljava/util/List;

    .line 40
    new-instance p8, Lcom/squareup/applet/AppletSectionsListEntry;

    check-cast p3, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->card_spend:I

    invoke-direct {p8, p3, v0, p1}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    .line 39
    invoke-interface {p2, p8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_1
    invoke-virtual {p4}, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 44
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;->visibleEntries:Ljava/util/List;

    .line 45
    new-instance p3, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 46
    check-cast p4, Lcom/squareup/applet/AppletSection;

    sget p8, Lcom/squareup/transferreports/R$string;->deposits_report:I

    .line 45
    invoke-direct {p3, p4, p8, p1}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    .line 44
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_2
    :goto_0
    invoke-virtual {p5}, Lcom/squareup/balance/squarecard/ManageSquareCardSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 52
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;->visibleEntries:Ljava/util/List;

    .line 53
    new-instance p3, Lcom/squareup/ui/balance/ManageSquareCardListEntry;

    invoke-direct {p3, p5, p1, p7}, Lcom/squareup/ui/balance/ManageSquareCardListEntry;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/util/Res;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;)V

    .line 52
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_3
    invoke-virtual {p6}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 57
    invoke-interface {p9}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalBalanceAppletRow()V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsList;->visibleEntries:Ljava/util/List;

    invoke-virtual {p6}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;->getAppletSectionListEntry()Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void
.end method
