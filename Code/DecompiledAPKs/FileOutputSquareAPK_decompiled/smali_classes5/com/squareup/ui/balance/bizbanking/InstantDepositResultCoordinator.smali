.class public final Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstantDepositResultCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstantDepositResultCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstantDepositResultCoordinator.kt\ncom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator\n*L\n1#1,108:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\rH\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\rH\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;",
        "(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "confirmButton",
        "Lcom/squareup/noho/NohoButton;",
        "helpButton",
        "message",
        "Lcom/squareup/noho/NohoMessageView;",
        "resultContainer",
        "Landroid/view/View;",
        "getRunner",
        "()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;",
        "spinner",
        "Landroid/view/ViewGroup;",
        "upsell",
        "Lcom/squareup/balance/core/views/SquareCardForUpsell;",
        "attach",
        "",
        "view",
        "bindViews",
        "onScreenData",
        "screenData",
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private confirmButton:Lcom/squareup/noho/NohoButton;

.field private helpButton:Lcom/squareup/noho/NohoButton;

.field private message:Lcom/squareup/noho/NohoMessageView;

.field private resultContainer:Landroid/view/View;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

.field private spinner:Landroid/view/ViewGroup;

.field private upsell:Lcom/squareup/balance/core/views/SquareCardForUpsell;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    return-void
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 99
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 100
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_spinner_holder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->spinner:Landroid/view/ViewGroup;

    .line 101
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_result:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    .line 102
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_upsell:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026d_instant_deposit_upsell)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/core/views/SquareCardForUpsell;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->upsell:Lcom/squareup/balance/core/views/SquareCardForUpsell;

    .line 103
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_confirm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026_instant_deposit_confirm)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    .line 104
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_help:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026ard_instant_deposit_help)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->helpButton:Lcom/squareup/noho/NohoButton;

    .line 105
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->square_card_instant_deposit_result_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.s\u2026deposit_result_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->resultContainer:Landroid/view/View;

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;)V
    .locals 6

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 61
    :cond_0
    new-instance v2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 62
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 63
    sget-object v3, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v4, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$onScreenData$1;

    iget-object v5, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-direct {v4, v5}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$onScreenData$1;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 64
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v2, "spinner"

    const-string v3, "resultContainer"

    if-nez v0, :cond_4

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->spinner:Landroid/view/ViewGroup;

    if-nez p1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->resultContainer:Landroid/view/View;

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->resultContainer:Landroid/view/View;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getShowSquareCardUpsell()Z

    move-result v0

    const-string/jumbo v3, "upsell"

    const-string v4, "confirmButton"

    if-eqz v0, :cond_8

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget-object v4, Lcom/squareup/noho/NohoButtonType;->LINK:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->upsell:Lcom/squareup/balance/core/views/SquareCardForUpsell;

    if-nez v0, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 78
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_9

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget-object v4, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->upsell:Lcom/squareup/balance/core/views/SquareCardForUpsell;

    if-nez v0, :cond_a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    const-string v3, "message"

    if-nez v0, :cond_b

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getGlyphId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_c

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getMessage()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v4, :cond_d

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v4, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 86
    :cond_e
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->getShowLearnMoreButton()Z

    move-result p1

    const-string v0, "helpButton"

    if-eqz p1, :cond_11

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->helpButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_f

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->helpButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_10

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    sget v0, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setText(I)V

    goto :goto_1

    .line 90
    :cond_11
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->helpButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_12

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 93
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->spinner:Landroid/view/ViewGroup;

    if-nez p1, :cond_14

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_15

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$1;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    const-string v1, "confirmButton"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/common/strings/R$string;->done:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->confirmButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 46
    :cond_2
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$2;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$2;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->helpButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_3

    const-string v1, "helpButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 50
    :cond_3
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$3;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$3;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 49
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->upsell:Lcom/squareup/balance/core/views/SquareCardForUpsell;

    if-nez v0, :cond_4

    const-string/jumbo v1, "upsell"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$4;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$4;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/squareup/balance/core/views/SquareCardForUpsell;->onPrimaryActionClickListener(Lkotlin/jvm/functions/Function0;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;->instantDepositResultScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$5;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$attach$5;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.instantDepositRes\u2026subscribe(::onScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public final getRunner()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    return-object v0
.end method
