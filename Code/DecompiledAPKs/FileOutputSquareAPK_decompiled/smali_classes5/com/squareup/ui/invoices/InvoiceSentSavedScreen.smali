.class public Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "InvoiceSentSavedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/InInvoiceSentSavedScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/ui/invoices/-$$Lambda$InvoiceSentSavedScreen$Wt0c5a-5DCIMreB15rBzBcfob5E;->INSTANCE:Lcom/squareup/ui/invoices/-$$Lambda$InvoiceSentSavedScreen$Wt0c5a-5DCIMreB15rBzBcfob5E;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;
    .locals 1

    .line 33
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 34
    new-instance v0, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 28
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 38
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->invoice_sent_save_view:I

    return v0
.end method
