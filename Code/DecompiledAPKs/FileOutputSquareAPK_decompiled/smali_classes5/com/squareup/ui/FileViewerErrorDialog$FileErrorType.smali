.class public final enum Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;
.super Ljava/lang/Enum;
.source "FileViewerErrorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/FileViewerErrorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FileErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum DEFAULT_NO_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum LOAD_DEFAULT_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum LOAD_IMAGE_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum LOAD_PDF_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum NO_IMAGE_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

.field public static final enum NO_PDF_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 25
    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v1, 0x0

    const-string v2, "NO_IMAGE_VIEWER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_IMAGE_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v2, 0x1

    const-string v3, "NO_PDF_VIEWER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_PDF_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v3, 0x2

    const-string v4, "LOAD_PDF_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_PDF_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v4, 0x3

    const-string v5, "LOAD_IMAGE_ERROR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_IMAGE_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v5, 0x4

    const-string v6, "DEFAULT_NO_VIEWER"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->DEFAULT_NO_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    .line 26
    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v6, 0x5

    const-string v7, "LOAD_DEFAULT_ERROR"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_DEFAULT_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    .line 24
    sget-object v7, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_IMAGE_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_PDF_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_PDF_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_IMAGE_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->DEFAULT_NO_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_DEFAULT_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->$VALUES:[Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->$VALUES:[Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v0}, [Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-object v0
.end method
