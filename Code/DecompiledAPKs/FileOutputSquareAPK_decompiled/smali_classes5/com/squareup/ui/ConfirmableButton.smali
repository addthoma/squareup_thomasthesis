.class public interface abstract Lcom/squareup/ui/ConfirmableButton;
.super Ljava/lang/Object;
.source "ConfirmableButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;
    }
.end annotation


# virtual methods
.method public abstract setConfirmText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V
.end method
