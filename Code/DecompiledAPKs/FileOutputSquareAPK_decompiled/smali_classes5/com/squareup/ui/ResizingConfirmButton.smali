.class public Lcom/squareup/ui/ResizingConfirmButton;
.super Landroid/widget/FrameLayout;
.source "ResizingConfirmButton.java"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;
    }
.end annotation


# static fields
.field private static final ENABLED_STATE:Ljava/lang/String; = "enabled-state"

.field private static final PIVOT_DURING_ANIMATION_TO_CONFIRM:F = 0.6f

.field private static final PIVOT_DURING_ANIMATION_TO_INITIAL:F = 0.4f

.field private static final SUPER_STATE:Ljava/lang/String; = "super"

.field private static final TRANSITION_DURATION_MS:I = 0xfa


# instance fields
.field private final backgroundView:Landroid/view/View;

.field private confirm:Z

.field private confirmListener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

.field private final confirmView:Lcom/squareup/marketfont/MarketTextView;

.field private initialClickListener:Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;

.field private final initialView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    sget-object v0, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    sget v1, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_buttonBackground:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 58
    sget v3, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_confirmStageText:I

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 59
    sget v4, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_initialStageText:I

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 60
    sget v5, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_android_textColor:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 62
    sget-object v6, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton:[I

    sget v7, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_weight:I

    invoke-static {p1, p2, v6, v7, v2}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p2

    .line 64
    sget v6, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_android_paddingLeft:I

    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 66
    sget v7, Lcom/squareup/sqwidgets/R$styleable;->ResizingConfirmButton_android_paddingRight:I

    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {v0, p1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    .line 71
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {v0, p1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    .line 72
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v5}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v5}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v6, v2, v7, v2}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v6, v2, v7, v2}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 82
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/squareup/ui/ResizingConfirmButton;->setPadding(IIII)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x1

    const/4 v2, 0x5

    invoke-direct {p2, v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ResizingConfirmButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x15

    invoke-direct {p2, v0, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ResizingConfirmButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p2, v0, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ResizingConfirmButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->performClickOnUp(Landroid/view/View;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    new-instance p2, Lcom/squareup/ui/ResizingConfirmButton$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/ResizingConfirmButton$1;-><init>(Lcom/squareup/ui/ResizingConfirmButton;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    invoke-virtual {p0, v3}, Lcom/squareup/ui/ResizingConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {p0, v4}, Lcom/squareup/ui/ResizingConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ResizingConfirmButton;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirm:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ResizingConfirmButton;Z)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmListener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ResizingConfirmButton;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->showConfirmButton()V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/ResizingConfirmButton;)Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialClickListener:Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/ResizingConfirmButton;)Landroid/view/View;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/ResizingConfirmButton;I)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->updateBackgroundWidth(I)V

    return-void
.end method

.method private animateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;F)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 168
    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ResizingConfirmButton;->updateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;)V

    const/high16 v3, 0x437a0000    # 250.0f

    mul-float v3, v3, p3

    float-to-int v3, v3

    const/16 v4, 0xfa

    rsub-int v5, v3, 0xfa

    .line 175
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/marketfont/MarketTextView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/marketfont/MarketTextView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v10, v6, v7

    .line 176
    new-instance v6, Landroid/view/animation/ScaleAnimation;

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x1

    const/high16 v16, 0x3f800000    # 1.0f

    move-object v8, v6

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    int-to-long v7, v4

    .line 178
    invoke-virtual {v6, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 179
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 180
    new-instance v4, Lcom/squareup/ui/ResizingConfirmButton$3;

    invoke-direct {v4, v0, v1}, Lcom/squareup/ui/ResizingConfirmButton$3;-><init>(Lcom/squareup/ui/ResizingConfirmButton;Lcom/squareup/marketfont/MarketTextView;)V

    invoke-virtual {v6, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 190
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v4, v8, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    int-to-long v9, v3

    .line 191
    invoke-virtual {v4, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 192
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 193
    new-instance v3, Lcom/squareup/ui/ResizingConfirmButton$4;

    invoke-direct {v3, v0, v2}, Lcom/squareup/ui/ResizingConfirmButton$4;-><init>(Lcom/squareup/ui/ResizingConfirmButton;Lcom/squareup/marketfont/MarketTextView;)V

    invoke-virtual {v4, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 200
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v7, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    int-to-long v7, v5

    .line 201
    invoke-virtual {v3, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 202
    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 203
    new-instance v5, Lcom/squareup/ui/ResizingConfirmButton$5;

    invoke-direct {v5, v0, v1}, Lcom/squareup/ui/ResizingConfirmButton$5;-><init>(Lcom/squareup/ui/ResizingConfirmButton;Lcom/squareup/marketfont/MarketTextView;)V

    invoke-virtual {v3, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 210
    iget-object v5, v0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 211
    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 212
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v9

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setStartTime(J)V

    .line 213
    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private animateToConfirmStage()V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/ResizingConfirmButton;->animateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;F)V

    return-void
.end method

.method private animateToInitialStage()V
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    const v2, 0x3ecccccd    # 0.4f

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/ResizingConfirmButton;->animateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;F)V

    return-void
.end method

.method private static cancelAnimation(Landroid/view/View;)V
    .locals 2

    .line 253
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 255
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 256
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    :cond_0
    return-void
.end method

.method private cancelAnimations()V
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0}, Lcom/squareup/ui/ResizingConfirmButton;->cancelAnimation(Landroid/view/View;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0}, Lcom/squareup/ui/ResizingConfirmButton;->cancelAnimation(Landroid/view/View;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/ui/ResizingConfirmButton;->cancelAnimation(Landroid/view/View;)V

    return-void
.end method

.method private static getTargetWidth(Lcom/squareup/marketfont/MarketTextView;)I
    .locals 2

    .line 261
    new-instance v0, Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 262
    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 265
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketTextView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketTextView;->getPaddingRight()I

    move-result p0

    add-int/2addr v0, p0

    return v0
.end method

.method private resetToInitialStage(Z)V
    .locals 1

    .line 143
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->cancelAnimations()V

    .line 145
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    if-eqz p1, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->animateToInitialStage()V

    goto :goto_0

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->updateToInitialStage()V

    :goto_0
    const/4 p1, 0x0

    .line 151
    iput-boolean p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirm:Z

    return-void
.end method

.method private showConfirmButton()V
    .locals 3

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->cancelAnimations()V

    .line 130
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/ResizingConfirmButton$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ResizingConfirmButton$2;-><init>(Lcom/squareup/ui/ResizingConfirmButton;)V

    .line 131
    invoke-virtual {v0, v1, p0, v2}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    .line 137
    invoke-direct {p0}, Lcom/squareup/ui/ResizingConfirmButton;->animateToConfirmStage()V

    const/4 v0, 0x1

    .line 138
    iput-boolean v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirm:Z

    return-void
.end method

.method private updateBackgroundWidth(I)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 242
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private updateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 1

    const/4 v0, 0x0

    .line 159
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    const/4 v0, 0x4

    .line 160
    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 161
    invoke-static {p1}, Lcom/squareup/ui/ResizingConfirmButton;->getTargetWidth(Lcom/squareup/marketfont/MarketTextView;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->updateBackgroundWidth(I)V

    return-void
.end method

.method private updateToInitialStage()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/ResizingConfirmButton;->updateTo(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/marketfont/MarketTextView;)V

    return-void
.end method


# virtual methods
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 277
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "super"

    .line 278
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "enabled-state"

    .line 279
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    .line 280
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->setEnabled(Z)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 269
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 270
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "super"

    .line 271
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 272
    invoke-virtual {p0}, Lcom/squareup/ui/ResizingConfirmButton;->isEnabled()Z

    move-result v0

    const-string v2, "enabled-state"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1
.end method

.method public setConfirmText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 109
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->backgroundView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    const/4 p1, 0x0

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method public setInitialText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 118
    invoke-direct {p0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->resetToInitialStage(Z)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 225
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->confirmListener:Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    return-void
.end method

.method public setOnInitialClickListener(Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/ui/ResizingConfirmButton;->initialClickListener:Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;

    return-void
.end method
