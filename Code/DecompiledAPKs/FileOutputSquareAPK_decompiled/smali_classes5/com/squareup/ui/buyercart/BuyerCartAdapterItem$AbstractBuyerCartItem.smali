.class public abstract Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;
.super Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.source "BuyerCartAdapterItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbstractBuyerCartItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;,
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BuyerCartItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u000f\u0010B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0002\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "displayItem",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "quantity",
        "",
        "(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;)V",
        "getDisplayItem",
        "()Lcom/squareup/comms/protos/seller/DisplayItem;",
        "id",
        "",
        "getId",
        "()I",
        "getQuantity",
        "()Ljava/lang/String;",
        "BranCartItem",
        "BuyerCartItem",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BuyerCartItem;",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

.field private final quantity:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    iput-object p2, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->quantity:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->client_id:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public getQuantity()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->quantity:Ljava/lang/String;

    return-object v0
.end method
