.class public final Lcom/squareup/ui/buyercart/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final cart_diff_highlight:I = 0x7f0a02cc

.field public static final cart_title:I = 0x7f0a02e7

.field public static final item:I = 0x7f0a08c9

.field public static final item_description:I = 0x7f0a08d2

.field public static final item_name:I = 0x7f0a08dd

.field public static final item_per_unit_price_and_quantity:I = 0x7f0a08de

.field public static final item_price:I = 0x7f0a08df

.field public static final item_quantity:I = 0x7f0a08e0

.field public static final per_item_discounts:I = 0x7f0a0c09

.field public static final price:I = 0x7f0a0c4f

.field public static final section_title:I = 0x7f0a0e3c

.field public static final tax_breakdown:I = 0x7f0a0f76

.field public static final total:I = 0x7f0a1057


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
