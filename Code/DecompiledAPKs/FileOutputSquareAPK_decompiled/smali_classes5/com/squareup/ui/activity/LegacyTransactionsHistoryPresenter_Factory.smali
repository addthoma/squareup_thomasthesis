.class public final Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activitySearchPaymentStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final bulkSettleButtonPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final showFullHistoryPermissionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->legacyListPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->showFullHistoryPermissionProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->bulkSettleButtonPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->activitySearchPaymentStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;)",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 119
    new-instance v18, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/CardReaderHubUtils;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 132
    new-instance v18, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/CardReaderHubUtils;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 100
    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->legacyListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->showFullHistoryPermissionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->bulkSettleButtonPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->activitySearchPaymentStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/CardReaderHubUtils;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter_Factory;->get()Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    move-result-object v0

    return-object v0
.end method
