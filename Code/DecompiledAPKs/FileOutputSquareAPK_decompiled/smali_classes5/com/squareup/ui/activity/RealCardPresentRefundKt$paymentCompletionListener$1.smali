.class public final Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/cardreader/PaymentCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefundKt;->paymentCompletionListener(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/cardreader/PaymentCompletionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J0\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J(\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00020\rH\u0016J(\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00020\rH\u0016J \u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H\u0016\u00a8\u0006\u0014"
    }
    d2 = {
        "com/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1",
        "Lcom/squareup/cardreader/PaymentCompletionListener;",
        "onPaymentApproved",
        "",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "encryptedCardData",
        "",
        "standardMessage",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        "approvedOffline",
        "",
        "paymentTimings",
        "Lcom/squareup/cardreader/PaymentTimings;",
        "onPaymentDeclined",
        "onPaymentReversed",
        "onPaymentTerminated",
        "onPaymentTerminatedDueToSwipe",
        "swipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V
    .locals 0

    .line 284
    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const-string p4, "cardReaderInfo"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "encryptedCardData"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "standardMessage"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "paymentTimings"

    invoke-static {p5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object p2, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object p3, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "encryptedCardData"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "standardMessage"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "paymentTimings"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object p2, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object p3, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "encryptedCardData"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "standardMessage"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "paymentTimings"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object p2, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object p3, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "standardMessage"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "paymentTimings"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object p2, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object p3, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "swipe"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$paymentCompletionListener$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object p2, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method
