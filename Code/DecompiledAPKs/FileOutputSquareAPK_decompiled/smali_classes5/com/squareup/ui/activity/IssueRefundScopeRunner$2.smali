.class Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;
.super Ljava/lang/Object;
.source "IssueRefundScopeRunner.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueRefundScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V
    .locals 0

    .line 1071
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .line 1085
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$100(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    return-void
.end method

.method public onClear()V
    .locals 1

    .line 1077
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinPadReset()V

    return-void
.end method

.method public onFocusLost()V
    .locals 1

    .line 1093
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$100(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    return-void
.end method

.method public onPinEntered(I)V
    .locals 1

    .line 1073
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->onPinDigitEntered(I)V

    return-void
.end method

.method public onSkip()V
    .locals 1

    .line 1089
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinBypass()V

    return-void
.end method

.method public onSubmit()V
    .locals 1

    .line 1081
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;->this$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->submitPinBlock()V

    return-void
.end method
