.class Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "LegacyTransactionsHistoryListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onInstantDepositRequested(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

.field final synthetic val$depositAmount:Lcom/squareup/protos/common/Money;

.field final synthetic val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    iput-object p3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$depositAmount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 545
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    iget-boolean v0, v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$300(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$depositAmount:Lcom/squareup/protos/common/Money;

    invoke-direct {v1, v2}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 547
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$600(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lio/reactivex/disposables/CompositeDisposable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$500(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/instantdeposit/InstantDepositRunner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->sendInstantDeposit()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 550
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$700(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_HEADER_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

    iget-object v3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$depositAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2, v3, v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapDepositAvailableFunds(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;Z)V

    .line 552
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->val$snapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-virtual {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->showPriceChangeModal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$300(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/instantdeposit/PriceChangeDialog;

    sget-object v2, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    invoke-direct {v1, v2}, Lcom/squareup/instantdeposit/PriceChangeDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$2;->this$0:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->access$500(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)Lcom/squareup/instantdeposit/InstantDepositRunner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->setIsConfirmingInstantTransfer()V

    :goto_0
    return-void
.end method
