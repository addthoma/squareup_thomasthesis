.class public Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "ErrorOnRequestResidualBillScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$Factory;,
        Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$ErrorOnRequestResidualBillScreen$Qk0L7L3von9CciRrFq2pXrriclU;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$ErrorOnRequestResidualBillScreen$Qk0L7L3von9CciRrFq2pXrriclU;

    .line 70
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;
    .locals 1

    .line 71
    const-class v0, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 72
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 73
    new-instance v0, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method
