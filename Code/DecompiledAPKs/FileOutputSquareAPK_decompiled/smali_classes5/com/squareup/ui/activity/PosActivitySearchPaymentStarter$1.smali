.class Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;
.super Ljava/lang/Object;
.source "PosActivitySearchPaymentStarter.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->register(Lmortar/MortarScope;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

.field final synthetic val$nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

.field final synthetic val$searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    iput-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->val$searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    iput-object p3, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->val$nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$100(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-interface {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 150
    new-instance v0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setEmvListener(Lcom/squareup/cardreader/EmvListener;)V

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->val$searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$402(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$500(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$500(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1$1;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcListener(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$500(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->val$nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerErrorListenerForScope(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetEmvListener()V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPinRequestListener()V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$402(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$500(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/NfcProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    return-void
.end method
