.class public final Lcom/squareup/ui/activity/TransactionsHistoryScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "TransactionsHistoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/activity/ActivityApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/activity/TransactionsHistoryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/ui/activity/TransactionsHistoryScreen;

    invoke-direct {v0}, Lcom/squareup/ui/activity/TransactionsHistoryScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/activity/TransactionsHistoryScreen;->INSTANCE:Lcom/squareup/ui/activity/TransactionsHistoryScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 55
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 51
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_transactions_history_view:I

    return v0
.end method
