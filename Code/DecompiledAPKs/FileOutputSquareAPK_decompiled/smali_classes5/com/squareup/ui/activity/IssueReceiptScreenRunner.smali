.class public Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
.super Ljava/lang/Object;
.source "IssueReceiptScreenRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;
    }
.end annotation


# static fields
.field static final SELECTED_ACTION_KEY:Ljava/lang/String; = "selectedAction"


# instance fields
.field private final analytics:Lcom/squareup/receipt/ReceiptAnalytics;

.field private final animateOutDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;",
            ">;"
        }
    .end annotation
.end field

.field private final autoFinisher:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private currentEmailString:Ljava/lang/String;

.field private currentSmsString:Ljava/lang/String;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

.field private selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/receipt/ReceiptAnalytics;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p6    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 84
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->animateOutDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 87
    sget-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->features:Lcom/squareup/settings/server/Features;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    .line 101
    iput-object p4, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->countryCodeProvider:Ljavax/inject/Provider;

    .line 102
    iput-object p7, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 103
    iput-object p8, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 104
    iput-object p9, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 105
    iput-object p10, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 106
    iput-object p11, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->res:Lcom/squareup/util/Res;

    .line 107
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p5, p6}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    .line 108
    iput-object p12, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    .line 109
    iput-object p13, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method private afterActionPerformed(Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;)V
    .locals 3

    .line 270
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 272
    new-instance p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    invoke-direct {p1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;-><init>()V

    .line 273
    sget-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$1;->$SwitchMap$com$squareup$ui$activity$IssueReceiptScreenRunner$ReceiptAction:[I

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 283
    sget v0, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_printed:I

    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setTitleText(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 284
    invoke-virtual {v0, v2}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    goto :goto_0

    .line 287
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid action performed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 279
    :cond_1
    sget v0, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setTitleText(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 280
    invoke-virtual {v0, v2}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    goto :goto_0

    .line 275
    :cond_2
    sget v0, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_email:I

    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setTitleText(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 276
    invoke-virtual {v0, v2}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    .line 290
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->animateOutDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 291
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setIconGlyphVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object p1

    .line 292
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setTitleVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object p1

    const/4 v1, 0x0

    .line 293
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setEmailReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object p1

    .line 294
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setSmsReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object p1

    .line 295
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->setPrintReceiptEnabled(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    move-result-object p1

    .line 296
    invoke-virtual {p1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;->build()Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;

    move-result-object p1

    .line 290
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private closeIssueReceiptScreen()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->reset()V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->closeIssueReceiptScreen()V

    return-void
.end method

.method private printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    if-eq v0, v1, :cond_0

    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v2}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getTender()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    .line 265
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/receipt/ReceiptAnalytics;->logPrint(Z)V

    .line 266
    sget-object p1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->PRINT:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->afterActionPerformed(Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;)V

    return-void
.end method

.method private scheduleAutoFinish()V
    .locals 5

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$integer;->receipt_auto_finish_delay_millis:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 128
    const-class v0, Lcom/squareup/ui/activity/IssueReceiptScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$IssueReceiptScreenRunner(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {p1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->goBackBecauseBillIdChanged()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$IssueReceiptScreenRunner(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 119
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->closeIssueReceiptScreen()V

    return-void
.end method

.method public languageSelectionClicked()V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->showSelectBuyerLanguageScreen()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->closeIssueReceiptScreen()V

    return-void
.end method

.method public onEmailTextChanged(Ljava/lang/String;)V
    .locals 2

    .line 245
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentEmailString:Ljava/lang/String;

    .line 246
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    .line 247
    invoke-virtual {v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->buildUpon()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentEmailString:Ljava/lang/String;

    .line 248
    invoke-static {v1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setEmailValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object v0

    .line 249
    invoke-virtual {v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->build()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    move-result-object v0

    .line 246
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Cannot issue receipt, no bill selected."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->onBillIdChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueReceiptScreenRunner$gtowRb9dUKZeQhrkUmnV0bkZeKs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueReceiptScreenRunner$gtowRb9dUKZeQhrkUmnV0bkZeKs;-><init>(Lcom/squareup/ui/activity/IssueReceiptScreenRunner;)V

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 115
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueReceiptScreenRunner$Rt1KRNm67ZGI4X6h-mIJqr366TM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueReceiptScreenRunner$Rt1KRNm67ZGI4X6h-mIJqr366TM;-><init>(Lcom/squareup/ui/activity/IssueReceiptScreenRunner;)V

    .line 119
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 118
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 5

    if-eqz p1, :cond_0

    .line 133
    invoke-static {}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->values()[Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    move-result-object v0

    const-string v1, "selectedAction"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    aget-object p1, v0, p1

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    .line 138
    :cond_0
    new-instance p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    invoke-direct {p1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->countryCodeProvider:Ljavax/inject/Provider;

    .line 139
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_1

    sget v0, Lcom/squareup/activity/R$string;->receipt_reissue:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/activity/R$string;->receipt_new:I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setUpButtonTextId(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 145
    invoke-virtual {p1, v2}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setInputsVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->build()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    move-result-object p1

    .line 144
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->afterActionPerformed(Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;)V

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->scheduleAutoFinish()V

    goto/16 :goto_1

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v3, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v4, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v4, v3, v2

    invoke-interface {v0, v3}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    iget-object v3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    .line 153
    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    .line 154
    invoke-interface {v3}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistory;->hasSuccessfulOrPendingRefund()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    .line 155
    invoke-interface {v3}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v2, 0x1

    .line 157
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 158
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setInputsVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setIconGlyphVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 160
    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setEmailReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentEmailString:Ljava/lang/String;

    .line 161
    invoke-static {v1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setEmailValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 162
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->supportsSms()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setSmsReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v4, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentSmsString:Ljava/lang/String;

    .line 163
    invoke-interface {v1, v4}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setSmsValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 164
    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setPrintReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1, v2}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setPrintFormalReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 166
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setSwitchLanguageVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object p1

    .line 167
    invoke-virtual {p1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->build()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    move-result-object p1

    .line 157
    invoke-virtual {v3, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public onReceiptAnimationEnd()V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->scheduleAutoFinish()V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->ordinal()I

    move-result v0

    const-string v1, "selectedAction"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onSmsTextChanged(Ljava/lang/String;)V
    .locals 3

    .line 237
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentSmsString:Ljava/lang/String;

    .line 238
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    .line 239
    invoke-virtual {v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->buildUpon()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentSmsString:Ljava/lang/String;

    .line 240
    invoke-interface {v1, v2}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->setSmsValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->build()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    move-result-object v0

    .line 238
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public printFormalReceipt()V
    .locals 2

    .line 224
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/receipt/ReceiptAnalytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public printReceipt()V
    .locals 2

    .line 219
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/receipt/ReceiptAnalytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public screenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData;",
            ">;"
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->animateOutDataRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public sendEmailReceipt()V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    if-eq v0, v1, :cond_0

    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentEmailString:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentEmailString:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/queue/EmailReceipt;->taskToResend(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/receipt/ReceiptAnalytics;->logEmail(ZZ)V

    .line 215
    sget-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->EMAIL:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->afterActionPerformed(Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;)V

    return-void
.end method

.method public sendSmsReceipt()V
    .locals 3

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->selectedAction:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    sget-object v1, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->NONE:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    if-eq v0, v1, :cond_0

    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentSmsString:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->currentSmsString:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/queue/SmsReceipt;->taskToResend(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/receipt/ReceiptAnalytics;->logSms(ZZ)V

    .line 197
    sget-object v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;->SMS:Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;->afterActionPerformed(Lcom/squareup/ui/activity/IssueReceiptScreenRunner$ReceiptAction;)V

    return-void
.end method
