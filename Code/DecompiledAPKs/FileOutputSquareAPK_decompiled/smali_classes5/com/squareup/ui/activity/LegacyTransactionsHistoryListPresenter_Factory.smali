.class public final Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryListPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final dayAndDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final expiryCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final instantDepositRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final showFullHistoryPermissionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->instantDepositRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->showFullHistoryPermissionProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->dayAndDateFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 119
    new-instance v18, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Lcom/squareup/activity/CurrentBill;Ljavax/inject/Provider;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/activity/CurrentBill;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/text/DayAndDateFormatter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 131
    new-instance v18, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;-><init>(Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Lcom/squareup/activity/CurrentBill;Ljavax/inject/Provider;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 102
    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/activity/CurrentBill;

    iget-object v9, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->instantDepositRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->showFullHistoryPermissionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/activity/ExpiryCalculator;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->dayAndDateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/text/DayAndDateFormatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/settings/server/Features;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Lcom/squareup/activity/CurrentBill;Ljavax/inject/Provider;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter_Factory;->get()Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    move-result-object v0

    return-object v0
.end method
