.class public final Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;
.super Ljava/lang/Object;
.source "BillHistoryDetailView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/BillHistoryDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final legacyPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/BillHistoryDetailView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectLegacyPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;->injectLegacyPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailView_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    return-void
.end method
