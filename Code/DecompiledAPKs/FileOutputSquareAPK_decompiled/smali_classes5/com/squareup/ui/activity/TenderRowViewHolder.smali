.class public Lcom/squareup/ui/activity/TenderRowViewHolder;
.super Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
.source "TenderRowViewHolder.java"


# instance fields
.field private final adapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

.field private amount:Lcom/squareup/marketfont/MarketTextView;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private employeeName:Lcom/squareup/marketfont/MarketTextView;

.field private focusedSpacer:Landroid/view/View;

.field private final presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

.field private quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

.field private receiptNumber:Landroid/widget/TextView;

.field private rowTotal:Landroid/widget/TextView;

.field private timeAndEmployeeNameContainer:Landroid/view/ViewGroup;

.field private timeWithEmployeeName:Lcom/squareup/marketfont/MarketTextView;

.field private timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;)V
    .locals 0

    .line 67
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;-><init>(Landroid/view/View;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->bindViews()V

    .line 70
    iput-object p3, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->adapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    .line 72
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/QuickTipEditor;->setHorizontalGravity(I)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p3, Lcom/squareup/ui/activity/-$$Lambda$Jqkufmu7EjmMJrimm6gWGRjuKso;

    invoke-direct {p3, p2}, Lcom/squareup/ui/activity/-$$Lambda$Jqkufmu7EjmMJrimm6gWGRjuKso;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/TenderRowViewHolder;)Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->adapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    return-object p0
.end method

.method private bindViews()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_time_and_employee_name_container:I

    .line 210
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeAndEmployeeNameContainer:Landroid/view/ViewGroup;

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_time_with_no_employee_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_time_with_employee_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_employee_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->employeeName:Lcom/squareup/marketfont/MarketTextView;

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_receipt_number:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->receiptNumber:Landroid/widget/TextView;

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_quicktip_editor:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/QuickTipEditor;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_amount:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->amount:Lcom/squareup/marketfont/MarketTextView;

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_focused_spacer:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->focusedSpacer:Landroid/view/View;

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tender_row_total:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->rowTotal:Landroid/widget/TextView;

    return-void
.end method

.method public static inflate(Landroid/content/Context;Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;)Lcom/squareup/ui/activity/TenderRowViewHolder;
    .locals 2

    .line 45
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bulk_settle_tender_row:I

    const/4 v1, 0x0

    .line 46
    invoke-virtual {p0, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup;

    .line 48
    new-instance p2, Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-direct {p2, p1, p0, p3}, Lcom/squareup/ui/activity/TenderRowViewHolder;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;)V

    return-object p2
.end method

.method static synthetic lambda$setTimeAndEmployeeName$1(Lcom/squareup/util/Res;Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 142
    iget-object v0, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-static {p0, v0, v1, p1}, Lcom/squareup/permissions/Employee;->getShortDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private setTipAmountBorderSpaceVisible(Z)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->focusedSpacer:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public getTenderPosition()I
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getAdapterPosition()I

    move-result v0

    return v0
.end method

.method public getTipOptionAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->getTipOptionAmounts()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTipOptionLabels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->getTipOptionLabels()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isDisplayingPercentages()Z
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isDisplayingPercentages()Z

    move-result v0

    return v0
.end method

.method public isQuickTipOptionUsed()Z
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isQuickTipOptionUsed()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onBind$0$TenderRowViewHolder(Landroid/view/View;Z)V
    .locals 0

    .line 96
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setTipAmountBorderSpaceVisible(Z)V

    return-void
.end method

.method public synthetic lambda$setTimeAndEmployeeName$2$TenderRowViewHolder(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->employeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setEmployeeNameVisible(Z)V

    return-void
.end method

.method public onBind(I)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->clearTipAmount()V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->clearTipAmount()V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->bindTenderRow(Lcom/squareup/ui/activity/TenderRowViewHolder;I)V

    const/4 p1, 0x0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setTipAmountBorderSpaceVisible(Z)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$G8AJeJjfjnOtNH7OQaorM-IlklA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$G8AJeJjfjnOtNH7OQaorM-IlklA;-><init>(Lcom/squareup/ui/activity/TenderRowViewHolder;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/QuickTipEditor;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    new-instance v0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/TenderRowViewHolder$1;-><init>(Lcom/squareup/ui/activity/TenderRowViewHolder;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/QuickTipEditor;->setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->adapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->canKeyboardMoveToNextRowFrom(I)Z

    move-result p1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->setTipAmountShowImeNext(Z)V

    return-void
.end method

.method onKeyboardMovedFromPreviousRow()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isTipAmountEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipAmount(Ljava/lang/Long;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->focusOnTipAmount()V

    return-void
.end method

.method onViewRecycled()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V

    return-void
.end method

.method public setAmount(Ljava/lang/String;)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->amount:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setBoldColumn(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-ne p1, v1, :cond_0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-ne p1, v1, :cond_1

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->employeeName:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-ne p1, v1, :cond_2

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    :goto_2
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->amount:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->AMOUNT:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-ne p1, v1, :cond_3

    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_3

    :cond_3
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    :goto_3
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setEmployeeNameVisible(Z)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeAndEmployeeNameContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setReceiptNumber(Ljava/lang/String;)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->receiptNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->employeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->receiptNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->amount:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setTimeAndEmployeeName(Ljava/lang/String;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/permissions/Employees;)V
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/util/Res$RealRes;

    iget-object v1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object p2, p2, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    .line 141
    invoke-virtual {p3, p2}, Lcom/squareup/permissions/Employees;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$JKHrnZlSlqVUGtW14KHw0HPPRLo;

    invoke-direct {p3, v0}, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$JKHrnZlSlqVUGtW14KHw0HPPRLo;-><init>(Lcom/squareup/util/Res;)V

    .line 142
    invoke-virtual {p2, p3}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p2

    .line 143
    invoke-static {v0}, Lcom/squareup/permissions/Employee;->getUnassignedName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$AidHHTXpnr3oN9RgN-TJ-SK9jfs;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$TenderRowViewHolder$AidHHTXpnr3oN9RgN-TJ-SK9jfs;-><init>(Lcom/squareup/ui/activity/TenderRowViewHolder;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2, p3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 140
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public setTimeWithNoEmployeeName(Ljava/lang/String;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->timeWithNoEmployeeName:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setEmployeeNameVisible(Z)V

    return-void
.end method

.method public updateQuickTipEditor(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method

.method public updateTotal(Ljava/lang/String;)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->rowTotal:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder;->rowTotal:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
