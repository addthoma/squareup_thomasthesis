.class public Lcom/squareup/ui/activity/BillHistoryRunner;
.super Ljava/lang/Object;
.source "BillHistoryRunner.java"

# interfaces
.implements Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;


# instance fields
.field private final activityIssueRefundStarter:Lcom/squareup/ui/activity/ActivityIssueRefundStarter;

.field private final clock:Lcom/squareup/util/Clock;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private settleTipFailurePopupResult:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/CurrentBill;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ActivityIssueRefundStarter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->settleTipFailurePopupResult:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 61
    iput-object p4, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 62
    iput-object p5, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->clock:Lcom/squareup/util/Clock;

    .line 63
    iput-object p6, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 64
    iput-object p7, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    .line 65
    iput-object p8, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->activityIssueRefundStarter:Lcom/squareup/ui/activity/ActivityIssueRefundStarter;

    .line 66
    iput-object p9, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 67
    iput-object p10, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 69
    sget-object p1, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method private getFailurePopupScreen()Lcom/squareup/ui/activity/RefundFailureDialogScreen;
    .locals 5

    .line 225
    new-instance v0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->no_internet_connection:I

    .line 226
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->restore_connectivity_to_refund:I

    .line 227
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 228
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 225
    invoke-static {v1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/RefundFailureDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-object v0
.end method

.method private getLearnMoreScreen(I)Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;
    .locals 8

    .line 214
    new-instance v0, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;

    new-instance v7, Lcom/squareup/register/widgets/LearnMoreMessages;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->refund_help_url:I

    .line 215
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/billhistoryui/R$string;->refund_past_deadline_title:I

    const/4 v1, 0x1

    if-le p1, v1, :cond_0

    sget p1, Lcom/squareup/billhistoryui/R$string;->refund_past_deadline_message_plural:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/billhistoryui/R$string;->refund_past_deadline_message:I

    :goto_0
    move v4, p1

    sget v5, Lcom/squareup/common/strings/R$string;->learn_more:I

    sget v6, Lcom/squareup/common/strings/R$string;->cancel:I

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/register/widgets/LearnMoreMessages;-><init>(Ljava/lang/String;IIII)V

    invoke-direct {v0, v7}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;-><init>(Lcom/squareup/register/widgets/LearnMoreMessages;)V

    return-object v0
.end method

.method private selectFirstTenderIfSingleTender()V
    .locals 3

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    .line 208
    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v1, v1, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    sget-object v2, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->BILL:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/squareup/activity/CurrentBill;->setTenderId(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public closeIssueReceiptScreen()V
    .locals 4

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->clearTenderId()V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/IssueReceiptScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen()V
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->clearTenderId()V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/InIssueRefundScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/activity/SelectRefundTenderScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->closeIssueRefundScreen()V

    return-void
.end method

.method continueToRefund()V
    .locals 3

    .line 145
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/billhistory/model/BillHistory;->areAllTendersPastRefundDate(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->numCardTenders()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getLearnMoreScreen(I)Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v1, v0, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    invoke-direct {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getFailurePopupScreen()Lcom/squareup/ui/activity/RefundFailureDialogScreen;

    move-result-object v1

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/activity/BillHistoryRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/BillHistoryRunner$1;-><init>(Lcom/squareup/ui/activity/BillHistoryRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public getBill()Lcom/squareup/billhistory/model/BillHistory;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentIdForReceipt()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTender()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->hasTenderId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 78
    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getTender(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public goBack()V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goBackBecauseBillIdChanged()V
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/InIssueRefundScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/activity/IssueReceiptScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/activity/SelectRefundTenderScreen;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public onBillIdChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method onSettleFailure(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 4

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->paper_signature_settle_failed_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_settle_tip_failed_body:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    const-string/jumbo v2, "transaction"

    .line 187
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 190
    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-static {v0, p1, v2, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, p1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method onTipSettlementFailedPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->settleTipFailurePopupResult:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onTransactionsHistoryChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public printGiftReceiptForSelectedTender(Ljava/lang/String;)V
    .locals 4

    .line 95
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void
.end method

.method public reprintTicket()V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method settleTipFailurePopupResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->settleTipFailurePopupResult:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method showFirstIssueReceiptScreen()V
    .locals 4

    .line 164
    invoke-direct {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->selectFirstTenderIfSingleTender()V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->hasTenderId()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    :goto_0
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method showFirstIssueRefundScreen(Ljava/lang/String;)V
    .locals 2

    .line 180
    invoke-direct {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->selectFirstTenderIfSingleTender()V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->activityIssueRefundStarter:Lcom/squareup/ui/activity/ActivityIssueRefundStarter;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->showFirstIssueRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/String;)V

    return-void
.end method

.method public showIssueReceiptScreen(Ljava/lang/String;)V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->setTenderId(Ljava/lang/String;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showSelectBuyerLanguageScreen()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;->INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showStartRefundScreen(Ljava/lang/String;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->setTenderId(Ljava/lang/String;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->activityIssueRefundStarter:Lcom/squareup/ui/activity/ActivityIssueRefundStarter;

    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->showStartRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method startPrintGiftReceiptFlow()V
    .locals 3

    .line 171
    invoke-direct {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->selectFirstTenderIfSingleTender()V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->hasTenderId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BillHistoryRunner;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/BillHistoryRunner;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
