.class public final Lcom/squareup/ui/activity/RealActivityAppletStarter;
.super Ljava/lang/Object;
.source "RealActivityAppletStarter.kt"

# interfaces
.implements Lcom/squareup/ui/activity/ActivityAppletStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/activity/RealActivityAppletStarter;",
        "Lcom/squareup/ui/activity/ActivityAppletStarter;",
        "applet",
        "Lcom/squareup/ui/activity/ActivityApplet;",
        "(Lcom/squareup/ui/activity/ActivityApplet;)V",
        "activateActivityApplet",
        "",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final applet:Lcom/squareup/ui/activity/ActivityApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/ActivityApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "applet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityAppletStarter;->applet:Lcom/squareup/ui/activity/ActivityApplet;

    return-void
.end method


# virtual methods
.method public activateActivityApplet()V
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletStarter;->applet:Lcom/squareup/ui/activity/ActivityApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityApplet;->activate()V

    return-void
.end method
