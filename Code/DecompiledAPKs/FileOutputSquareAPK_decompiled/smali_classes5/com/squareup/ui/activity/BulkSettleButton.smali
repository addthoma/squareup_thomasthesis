.class public Lcom/squareup/ui/activity/BulkSettleButton;
.super Lcom/squareup/widgets/CenteredFrameLayout;
.source "BulkSettleButton.java"

# interfaces
.implements Lcom/squareup/marin/widgets/Badgeable;


# instance fields
.field private badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

.field currency:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/CenteredFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class v0, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;->inject(Lcom/squareup/ui/activity/BulkSettleButton;)V

    .line 27
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bulk_settle_button:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/activity/BulkSettleButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettleButton;->bindViews()V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleButton;->hideBadge()V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleButton;->setLocalizedGlyph()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 75
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_bulk_settle_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 76
    sget v0, Lcom/squareup/billhistoryui/R$id;->activity_applet_bulk_settle_badge:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleBadgeView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

    return-void
.end method

.method private setGlyphIsRed(Z)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    return-void
.end method


# virtual methods
.method public hideBadge()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->hideBadge()V

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleButton;->setGlyphIsRed(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 34
    invoke-super {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->onAttachedToWindow()V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->presenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->presenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->dropView(Lcom/squareup/ui/activity/BulkSettleButton;)V

    .line 40
    invoke-super {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setLocalizedGlyph()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleButton;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public showBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->showBadge(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleButton;->setGlyphIsRed(Z)V

    return-void
.end method

.method public showFatalPriorityBadge()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->showFatalPriorityBadge()V

    const/4 v0, 0x1

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleButton;->setGlyphIsRed(Z)V

    return-void
.end method

.method public showHighPriorityBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton;->badgeView:Lcom/squareup/ui/activity/BulkSettleBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleButton;->setGlyphIsRed(Z)V

    return-void
.end method
