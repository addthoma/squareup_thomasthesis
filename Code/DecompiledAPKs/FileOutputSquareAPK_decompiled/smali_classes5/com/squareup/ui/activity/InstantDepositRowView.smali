.class public Lcom/squareup/ui/activity/InstantDepositRowView;
.super Landroid/widget/FrameLayout;
.source "InstantDepositRowView.java"


# instance fields
.field private button:Lcom/squareup/marketfont/MarketButton;

.field private container:Landroid/view/View;

.field private error:Lcom/squareup/marketfont/MarketTextView;

.field private hint:Lcom/squareup/widgets/MessageView;

.field legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private onButtonClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private progress:Landroid/widget/ProgressBar;

.field private final sub:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance p2, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p2}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->sub:Lio/reactivex/disposables/CompositeDisposable;

    .line 44
    const-class p2, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;->inject(Lcom/squareup/ui/activity/InstantDepositRowView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 127
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposit_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->container:Landroid/view/View;

    .line 128
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposit_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->progress:Landroid/widget/ProgressBar;

    .line 129
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposit_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    .line 130
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposit_error:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->error:Lcom/squareup/marketfont/MarketTextView;

    .line 131
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposit_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->hint:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method static synthetic lambda$updateView$0(Lkotlin/Unit;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method


# virtual methods
.method public hideButton()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$updateView$1$InstantDepositRowView(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->updateInstantDepositRowView(Lcom/squareup/ui/activity/InstantDepositRowView;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 56
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->updateView()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->sub:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 62
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 48
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->bindViews()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->onButtonClicked:Lio/reactivex/Observable;

    return-void
.end method

.method public setButtonColorBlue()V
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$drawable;->instant_transfer_button_background_blue:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$color;->instant_transfer_button_text_white:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    return-void
.end method

.method public setButtonColorWhite()V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$drawable;->instant_transfer_button_background_white:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$color;->instant_transfer_button_text_blue:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->button:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setErrorHintText(Ljava/lang/String;)V
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->hint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->hint:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/InstantDepositRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable_learn_more:I

    const-string v3, "learn_more"

    .line 113
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable_troubleshooting:I

    .line 115
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 116
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "error_message"

    .line 118
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 112
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setErrorText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->error:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHintText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->hint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->hint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLoading(Z)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->progress:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->container:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public updateView()V
    .locals 4

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->sub:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->sub:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->onButtonClicked:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    .line 69
    invoke-virtual {v2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunnerSnapshot()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/activity/-$$Lambda$InstantDepositRowView$fvHhQDiEFkW03t79ZQTnvbrOUDA;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$InstantDepositRowView$fvHhQDiEFkW03t79ZQTnvbrOUDA;

    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/activity/-$$Lambda$iOTXjir2NwT0R1yY-_DumRpwn6g;

    invoke-direct {v3, v2}, Lcom/squareup/ui/activity/-$$Lambda$iOTXjir2NwT0R1yY-_DumRpwn6g;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)V

    .line 71
    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->sub:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/activity/InstantDepositRowView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->instantDepositRunnerSnapshot()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$InstantDepositRowView$_FPdywROxZa7wDiIh0WYORVGQ2U;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/-$$Lambda$InstantDepositRowView$_FPdywROxZa7wDiIh0WYORVGQ2U;-><init>(Lcom/squareup/ui/activity/InstantDepositRowView;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
