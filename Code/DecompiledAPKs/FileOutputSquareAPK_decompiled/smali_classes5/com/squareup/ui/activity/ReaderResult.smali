.class public final Lcom/squareup/ui/activity/ReaderResult;
.super Ljava/lang/Object;
.source "CardPresentRefund.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ReaderResult$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ReaderResult;",
        "",
        "success",
        "",
        "readerType",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "authorizationBytes",
        "Lokio/ByteString;",
        "messageResources",
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)V",
        "getAuthorizationBytes",
        "()Lokio/ByteString;",
        "getMessageResources",
        "()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "getReaderType",
        "()Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "getSuccess",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;


# instance fields
.field private final authorizationBytes:Lokio/ByteString;

.field private final messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

.field private final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private final success:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/activity/ReaderResult$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/ReaderResult$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)V
    .locals 1

    const-string v0, "readerType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationBytes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageResources"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    iput-object p2, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object p3, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    iput-object p4, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    return-void
.end method

.method public static final authorizationOf(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/ui/activity/ReaderResult;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->authorizationOf(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/activity/ReaderResult;ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;ILjava/lang/Object;)Lcom/squareup/ui/activity/ReaderResult;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/activity/ReaderResult;->copy(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static final errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    return v0
.end method

.method public final component2()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method public final component3()Lokio/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;
    .locals 1

    const-string v0, "readerType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationBytes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageResources"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/activity/ReaderResult;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/activity/ReaderResult;-><init>(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/activity/ReaderResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/activity/ReaderResult;

    iget-boolean v0, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    iget-boolean v1, p1, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v1, p1, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    iget-object v1, p1, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    iget-object p1, p1, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAuthorizationBytes()Lokio/ByteString;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    return-object v0
.end method

.method public final getMessageResources()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    return-object v0
.end method

.method public final getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method public final getSuccess()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReaderResult(success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/activity/ReaderResult;->success:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", readerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", authorizationBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->authorizationBytes:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messageResources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/ReaderResult;->messageResources:Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
