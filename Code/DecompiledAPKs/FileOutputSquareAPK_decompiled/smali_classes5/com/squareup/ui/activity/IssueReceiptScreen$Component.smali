.class public interface abstract Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.super Ljava/lang/Object;
.source "IssueReceiptScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/activity/IssueReceiptScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueReceiptScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract customLocale()Lcom/squareup/buyer/language/BuyerLocaleOverride;
.end method

.method public abstract inject(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V
.end method

.method public abstract issueReceiptCoordinatorFactory()Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;
.end method

.method public abstract issueReceiptScreenRunner()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
.end method
