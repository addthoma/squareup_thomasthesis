.class public final Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;
.super Ljava/lang/Object;
.source "RefundPinDialog_RefundPinDialogModule_PinListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/IssueRefundScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/IssueRefundScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/IssueRefundScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;-><init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static pinListener(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 0

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;->pinListener(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    iget-object v1, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->pinListener(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinListenerFactory;->get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object v0

    return-object v0
.end method
