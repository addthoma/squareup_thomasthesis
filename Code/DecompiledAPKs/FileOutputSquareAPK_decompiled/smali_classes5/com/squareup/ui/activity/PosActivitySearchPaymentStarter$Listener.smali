.class final Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;
.super Ljava/lang/Object;
.source "PosActivitySearchPaymentStarter.java"

# interfaces
.implements Lcom/squareup/cardreader/EmvListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/PinRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V
    .locals 0

    .line 214
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 260
    iget-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$700(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    const/4 p3, 0x0

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/CardReader;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public onCardError()V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardError()V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardRemovedDuringPayment()V

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 327
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$800(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {v0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$700(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "onMagFallbackSwipeFailed in PosActivitySearchPaymentStarter"

    .line 286
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 282
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "onMagSwipeFallbackSuccess should not be called here."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const-string p3, "Activity Search should only see onPaymentApproved for offline approvals!"

    .line 293
    invoke-static {p4, p3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 295
    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 296
    invoke-static {p1, p2, p3}, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->instrumentSearchForBytes(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p1

    .line 297
    iget-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 308
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 303
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 314
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onPaymentTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    .line 315
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 320
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-interface {p1, p2}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onPaymentTerminatedDueToSwipe(Lcom/squareup/Card;)V

    .line 321
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onSigRequested()V
    .locals 2

    .line 267
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onSigRequested should not be called here."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 0

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$800(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 0

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardError()V

    .line 233
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method

.method public onUseChipCardDuringFallback()V
    .locals 2

    .line 228
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onUseChipCardDuringFallback should not be called here."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    .line 272
    iget-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    .line 273
    invoke-static {p2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$700(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p2

    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {p2, p1, p3}, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->instrumentSearchForBytes(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p1

    .line 275
    iget-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    .line 276
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method
