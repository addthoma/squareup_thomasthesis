.class public final Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;
.super Ljava/lang/Object;
.source "QuickTipEditorPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/QuickTipEditorPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final tipOptionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tipping/TipOptions;",
            ">;"
        }
    .end annotation
.end field

.field private final tippingCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tipping/TipOptions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->tipOptionsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->tippingCalculatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tipping/TipOptions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;)",
            "Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/tipping/TipOptions;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)Lcom/squareup/ui/activity/QuickTipEditorPresenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/tipping/TipOptions;",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ")",
            "Lcom/squareup/ui/activity/QuickTipEditorPresenter;"
        }
    .end annotation

    .line 60
    new-instance v6, Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/tipping/TipOptions;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/QuickTipEditorPresenter;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->tipOptionsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tipping/TipOptions;

    iget-object v4, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->tippingCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/tipping/TipOptions;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter_Factory;->get()Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    move-result-object v0

    return-object v0
.end method
