.class public Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;
.super Ljava/lang/Object;
.source "HardwarePeripheralListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/account/HardwarePeripheralListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StringHolder"
.end annotation


# static fields
.field public static final UNUSED_STRING:I


# instance fields
.field public final availableSection:I

.field public final emptyRow:I

.field public final itemHelpMessage:I

.field public final unsupportedSection:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->availableSection:I

    .line 36
    iput p3, p0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->emptyRow:I

    .line 37
    iput p2, p0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->unsupportedSection:I

    .line 38
    iput p4, p0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->itemHelpMessage:I

    return-void
.end method
