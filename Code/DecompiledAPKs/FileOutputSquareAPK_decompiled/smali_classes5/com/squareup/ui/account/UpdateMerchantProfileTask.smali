.class public Lcom/squareup/ui/account/UpdateMerchantProfileTask;
.super Ljava/lang/Object;
.source "UpdateMerchantProfileTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 17
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/UpdateMerchantProfileTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/UpdateMerchantProfileTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Lcom/squareup/ui/account/UpdateMerchantProfileTask;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/account/UpdateMerchantProfileTask;->secureCopyWithoutPIIForLogs()Lcom/squareup/ui/account/UpdateMerchantProfileTask;

    move-result-object v0

    return-object v0
.end method
