.class final enum Lcom/squareup/ui/LocationActivity$MessageType;
.super Ljava/lang/Enum;
.source "LocationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/LocationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/LocationActivity$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/LocationActivity$MessageType;

.field public static final enum SERVICES_DISABLED:Lcom/squareup/ui/LocationActivity$MessageType;

.field public static final enum SERVICES_ENABLED_BOTH:Lcom/squareup/ui/LocationActivity$MessageType;

.field public static final enum SERVICES_ENABLED_ONE:Lcom/squareup/ui/LocationActivity$MessageType;


# instance fields
.field private final animate:Z

.field private final locationVisibility:I

.field private final message:I

.field private final title:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 177
    new-instance v7, Lcom/squareup/ui/LocationActivity$MessageType;

    sget v3, Lcom/squareup/common/bootstrap/R$string;->location_off_title:I

    sget v4, Lcom/squareup/common/bootstrap/R$string;->location_off_message:I

    const-string v1, "SERVICES_DISABLED"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/LocationActivity$MessageType;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v7, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_DISABLED:Lcom/squareup/ui/LocationActivity$MessageType;

    .line 178
    new-instance v0, Lcom/squareup/ui/LocationActivity$MessageType;

    sget v11, Lcom/squareup/common/bootstrap/R$string;->location_waiting_title:I

    sget v12, Lcom/squareup/common/bootstrap/R$string;->location_waiting_speed_up_message:I

    const-string v9, "SERVICES_ENABLED_ONE"

    const/4 v10, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/ui/LocationActivity$MessageType;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_ONE:Lcom/squareup/ui/LocationActivity$MessageType;

    .line 179
    new-instance v0, Lcom/squareup/ui/LocationActivity$MessageType;

    sget v4, Lcom/squareup/common/bootstrap/R$string;->location_waiting_title:I

    sget v5, Lcom/squareup/common/bootstrap/R$string;->location_waiting_message:I

    const-string v2, "SERVICES_ENABLED_BOTH"

    const/4 v3, 0x2

    const/16 v6, 0x8

    const/4 v7, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/LocationActivity$MessageType;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_BOTH:Lcom/squareup/ui/LocationActivity$MessageType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/LocationActivity$MessageType;

    .line 176
    sget-object v1, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_DISABLED:Lcom/squareup/ui/LocationActivity$MessageType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_ONE:Lcom/squareup/ui/LocationActivity$MessageType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/LocationActivity$MessageType;->SERVICES_ENABLED_BOTH:Lcom/squareup/ui/LocationActivity$MessageType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->$VALUES:[Lcom/squareup/ui/LocationActivity$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ)V"
        }
    .end annotation

    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 188
    iput p3, p0, Lcom/squareup/ui/LocationActivity$MessageType;->title:I

    .line 189
    iput p4, p0, Lcom/squareup/ui/LocationActivity$MessageType;->message:I

    .line 190
    iput p5, p0, Lcom/squareup/ui/LocationActivity$MessageType;->locationVisibility:I

    .line 191
    iput-boolean p6, p0, Lcom/squareup/ui/LocationActivity$MessageType;->animate:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/LocationActivity$MessageType;)I
    .locals 0

    .line 176
    iget p0, p0, Lcom/squareup/ui/LocationActivity$MessageType;->title:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/LocationActivity$MessageType;)I
    .locals 0

    .line 176
    iget p0, p0, Lcom/squareup/ui/LocationActivity$MessageType;->message:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/LocationActivity$MessageType;)I
    .locals 0

    .line 176
    iget p0, p0, Lcom/squareup/ui/LocationActivity$MessageType;->locationVisibility:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/LocationActivity$MessageType;)Z
    .locals 0

    .line 176
    iget-boolean p0, p0, Lcom/squareup/ui/LocationActivity$MessageType;->animate:Z

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/LocationActivity$MessageType;
    .locals 1

    .line 176
    const-class v0, Lcom/squareup/ui/LocationActivity$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/LocationActivity$MessageType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/LocationActivity$MessageType;
    .locals 1

    .line 176
    sget-object v0, Lcom/squareup/ui/LocationActivity$MessageType;->$VALUES:[Lcom/squareup/ui/LocationActivity$MessageType;

    invoke-virtual {v0}, [Lcom/squareup/ui/LocationActivity$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/LocationActivity$MessageType;

    return-object v0
.end method
