.class public Lcom/squareup/ui/NfcAuthData;
.super Ljava/lang/Object;
.source "NfcAuthData.java"


# instance fields
.field public final authorizationData:[B

.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;[B)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 11
    iput-object p2, p0, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    return-void
.end method
