.class public abstract Lcom/squareup/ui/RangerRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "RangerRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Enum;",
        ":",
        "Lcom/squareup/ui/Ranger$RowType<",
        "TH;>;H:",
        "Ljava/lang/Enum;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final holderTypeValues:[Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TH;"
        }
    .end annotation
.end field

.field protected ranger:Lcom/squareup/ui/Ranger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/Ranger<",
            "TR;TH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TH;>;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 45
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Enum;

    iput-object p1, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->holderTypeValues:[Ljava/lang/Enum;

    .line 46
    new-instance p1, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    return-void
.end method


# virtual methods
.method public final getItemCount()I
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    invoke-virtual {v0}, Lcom/squareup/ui/Ranger;->count()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/Ranger;->getHolderType(I)Ljava/lang/Enum;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/RangerRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;I)V

    return-void
.end method

.method public final onBindViewHolder(Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;I)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/Ranger;->getRowType(I)Ljava/lang/Enum;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    invoke-virtual {v1, v0, p2}, Lcom/squareup/ui/Ranger;->getRowTypeCountUpTo(Ljava/lang/Enum;I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1, p2}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;->bindRow(Ljava/lang/Enum;II)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/RangerRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "TR;TH;>.RangerHolder;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->holderTypeValues:[Ljava/lang/Enum;

    aget-object p2, v0, p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/RangerRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method public abstract onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "TH;)",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "TR;TH;>.RangerHolder;"
        }
    .end annotation
.end method

.method public setRanger(Lcom/squareup/ui/Ranger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Ranger<",
            "TR;TH;>;)V"
        }
    .end annotation

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/RangerRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/RangerRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
