.class public interface abstract Lcom/squareup/ui/DropDownContainer$DropDownListener;
.super Ljava/lang/Object;
.source "DropDownContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/DropDownContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DropDownListener"
.end annotation


# virtual methods
.method public abstract onDropDownHidden(Landroid/view/View;)V
.end method

.method public abstract onDropDownHiding(Landroid/view/View;)V
.end method

.method public abstract onDropDownOpening(Landroid/view/View;)V
.end method

.method public abstract onDropDownShown(Landroid/view/View;)V
.end method

.method public abstract onDropDownSlide(Landroid/view/View;F)V
.end method
