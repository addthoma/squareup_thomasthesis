.class Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;
.super Ljava/lang/Object;
.source "GiftCardActivationView.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnPanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext(Lcom/squareup/Card;)V
    .locals 0

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    iget-object p1, p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onPanNext()V

    :cond_0
    return-void
.end method

.method public onPanInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 0

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    iget-object p1, p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onPanInvalid()V

    :cond_0
    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;)V
    .locals 0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$2;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    iget-object p1, p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onPanValid()V

    :cond_0
    return-void
.end method
