.class Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "BaseGlyphButtonEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/BaseGlyphButtonEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isFocused:Z

.field private final text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 263
    new-instance v0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 252
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->text:Ljava/lang/String;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->isFocused:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/ui/library/BaseGlyphButtonEditText$1;)V
    .locals 0

    .line 241
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;Z)V
    .locals 0

    .line 246
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 247
    iput-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->text:Ljava/lang/String;

    .line 248
    iput-boolean p3, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->isFocused:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;ZLcom/squareup/ui/library/BaseGlyphButtonEditText$1;)V
    .locals 0

    .line 241
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;)Ljava/lang/String;
    .locals 0

    .line 241
    iget-object p0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->text:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;)Z
    .locals 0

    .line 241
    iget-boolean p0, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->isFocused:Z

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 258
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 259
    iget-object p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->text:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 260
    iget-boolean p2, p0, Lcom/squareup/ui/library/BaseGlyphButtonEditText$SavedState;->isFocused:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
