.class public Lcom/squareup/ui/library/HideOnClickLayout;
.super Landroid/widget/FrameLayout;
.source "HideOnClickLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/HideOnClickLayout$SavedState;
    }
.end annotation


# instance fields
.field private bottomChild:Landroid/view/View;

.field private onTopClickListener:Lcom/squareup/util/NonDebouncedOnClickListener;

.field private topChild:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/library/HideOnClickLayout;)Landroid/view/View;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/HideOnClickLayout;)Landroid/view/View;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->bottomChild:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/library/HideOnClickLayout;)Lcom/squareup/util/NonDebouncedOnClickListener;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->onTopClickListener:Lcom/squareup/util/NonDebouncedOnClickListener;

    return-object p0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .line 27
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const/4 v0, 0x1

    .line 29
    invoke-virtual {p0, v0}, Lcom/squareup/ui/library/HideOnClickLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    const/4 v0, 0x0

    .line 30
    invoke-virtual {p0, v0}, Lcom/squareup/ui/library/HideOnClickLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->bottomChild:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/library/HideOnClickLayout$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/library/HideOnClickLayout$1;-><init>(Lcom/squareup/ui/library/HideOnClickLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 52
    check-cast p1, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 54
    invoke-static {p1}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->access$400(Lcom/squareup/ui/library/HideOnClickLayout$SavedState;)I

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout;->bottomChild:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 58
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout;->bottomChild:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 48
    new-instance v0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/HideOnClickLayout;->topChild:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;-><init>(Landroid/os/Parcelable;ILcom/squareup/ui/library/HideOnClickLayout$1;)V

    return-object v0
.end method

.method public setOnTopClickListener(Lcom/squareup/util/NonDebouncedOnClickListener;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout;->onTopClickListener:Lcom/squareup/util/NonDebouncedOnClickListener;

    return-void
.end method
