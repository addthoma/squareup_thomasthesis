.class Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;
.super Ljava/lang/Object;
.source "PriceEntryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/PriceEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WorkingItemState"
.end annotation


# instance fields
.field private final transaction:Lcom/squareup/payment/Transaction;

.field private workingItem:Lcom/squareup/configure/item/WorkingItem;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/configure/item/WorkingItem;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)V"
        }
    .end annotation

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->transaction:Lcom/squareup/payment/Transaction;

    .line 190
    iput-object p2, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 191
    iput-object p3, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;Landroid/os/Bundle;)V
    .locals 0

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->load(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->setAmount(Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Ljava/lang/CharSequence;
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->getName()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;Landroid/os/Bundle;)V
    .locals 0

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->save(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/configure/item/WorkingItem;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->commit()V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Ljava/lang/String;
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static buildEmptyWorkingItemState(Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)",
            "Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;"
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)V

    return-object v0
.end method

.method static buildLoadedWorkingItemState(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/configure/item/WorkingItem;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)",
            "Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;"
        }
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)V

    return-object v0
.end method

.method private commit()V
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v1}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method private getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method private getName()Ljava/lang/CharSequence;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method private getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 233
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private load(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    :cond_0
    return-void
.end method

.method private save(Landroid/os/Bundle;)V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method private setAmount(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    return-void
.end method
