.class Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "GiftCardActivationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/BundleKey;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 118
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->access$000(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 116
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
