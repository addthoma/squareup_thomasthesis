.class public final Lcom/squareup/ui/library/PriceEntryScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "PriceEntryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/library/PriceEntryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/PriceEntryScreen$Component;,
        Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;,
        Lcom/squareup/ui/library/PriceEntryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/library/PriceEntryScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final workingItem:Lcom/squareup/configure/item/WorkingItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 244
    sget-object v0, Lcom/squareup/ui/library/-$$Lambda$PriceEntryScreen$N6TYiEum6p33iqjs3YGQEWAr9Gc;->INSTANCE:Lcom/squareup/ui/library/-$$Lambda$PriceEntryScreen$N6TYiEum6p33iqjs3YGQEWAr9Gc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/library/PriceEntryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/library/PriceEntryScreen;)Lcom/squareup/configure/item/WorkingItem;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/library/PriceEntryScreen;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/library/PriceEntryScreen;
    .locals 1

    .line 247
    new-instance p0, Lcom/squareup/ui/library/PriceEntryScreen;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/squareup/ui/library/PriceEntryScreen;-><init>(Lcom/squareup/configure/item/WorkingItem;)V

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 251
    sget v0, Lcom/squareup/orderentry/R$layout;->price_entry_view:I

    return v0
.end method
