.class public interface abstract Lcom/squareup/ui/SquareActivity$ParentAppComponent;
.super Ljava/lang/Object;
.source "SquareActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/SquareActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentAppComponent"
.end annotation


# virtual methods
.method public abstract componentByActivity()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end method

.method public abstract persistentBundleManager()Lcom/squareup/persistentbundle/PersistentBundleManager;
.end method
