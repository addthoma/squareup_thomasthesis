.class Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultNfcAuthDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;)V
    .locals 0

    .line 1007
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 2

    .line 1010
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$500(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    .line 1011
    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$500(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    .line 1012
    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$500(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    .line 1015
    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContinueContactlessTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 1019
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    goto :goto_0

    .line 1021
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    .line 1022
    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContactlessTenderInBuyerFlowResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 1025
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    :goto_0
    return-void
.end method
