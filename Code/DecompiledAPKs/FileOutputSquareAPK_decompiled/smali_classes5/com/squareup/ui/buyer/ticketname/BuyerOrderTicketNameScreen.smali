.class public final Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "BuyerOrderTicketNameScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameScreen$y1_3_Tc9LV4TN-aS5aBvGmm81ZQ;->INSTANCE:Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameScreen$y1_3_Tc9LV4TN-aS5aBvGmm81ZQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;
    .locals 1

    .line 38
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 39
    new-instance v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 33
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_AFTER_AUTH:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 43
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->buyer_order_ticket_name_view:I

    return v0
.end method
