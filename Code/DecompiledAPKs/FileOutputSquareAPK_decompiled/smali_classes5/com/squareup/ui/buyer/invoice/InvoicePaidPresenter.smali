.class public Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;
.super Lmortar/ViewPresenter;
.source "InvoicePaidPresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/invoice/InvoicePaidView;",
        ">;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;"
    }
.end annotation


# instance fields
.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field protected final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field protected final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field protected final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

.field protected final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field protected final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private remainingBalance:Lcom/squareup/protos/common/Money;

.field private screen:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

.field private smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private transactionEnded:Z

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field protected uniqueKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 62
    iput-object p4, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    .line 63
    iput-object p5, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 64
    iput-object p6, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    .line 65
    iput-object p7, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 66
    iput-object p8, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 67
    iput-object p9, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    return-void
.end method

.method private endTransaction()V
    .locals 2

    .line 169
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transactionEnded:Z

    if-nez v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isPaymentComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->uniqueKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    .line 173
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transactionEnded:Z

    :cond_1
    return-void
.end method

.method private exit()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->endTransaction()V

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->finish()V

    return-void
.end method

.method private finish()V
    .locals 4

    .line 178
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v1

    .line 181
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 182
    iget-object v2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v0, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAddCustomerToSaleEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->enqueueAttachContactTask()V

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method private getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method

.method private initActionBar()V
    .locals 3

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;

    .line 104
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->getActionBar()Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->actionBarTitle()Lcom/squareup/util/ViewString$TextString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->actionBarSubtitle()Lcom/squareup/util/ViewString$TextString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    .line 109
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideUpGlyph()V

    .line 110
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->view_invoices:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    new-instance v2, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$heVoAm38xTE7XbrLAO6C-YzrPno;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$heVoAm38xTE7XbrLAO6C-YzrPno;-><init>(Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private maybeShowSaveCardUpGlyph()V
    .locals 3

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->getActionBar()Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->addCardGlyph(Lcom/squareup/payment/Payment;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 157
    new-instance v2, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$E4eX_ng43H-tQDf2vR8F_rPZi4A;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$E4eX_ng43H-tQDf2vR8F_rPZi4A;-><init>(Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method private onCustomerAddCardClicked()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V

    return-void
.end method

.method private updateMessages()V
    .locals 4

    .line 120
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;

    .line 122
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    iget-object v2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object v3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->remainingBalance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->glyphTitle(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->setTitle(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dataFactory:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    iget-object v2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->remainingBalance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->glyphSubtitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->updateMessages()V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$initActionBar$2$InvoicePaidPresenter()Lkotlin/Unit;
    .locals 1

    .line 113
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->exit()V

    .line 114
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$maybeShowSaveCardUpGlyph$3$InvoicePaidPresenter()Lkotlin/Unit;
    .locals 1

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->onCustomerAddCardClicked()V

    .line 161
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$null$0$InvoicePaidPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->maybeShowSaveCardUpGlyph()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$InvoicePaidPresenter(Lcom/squareup/payment/Payment;)Lrx/Subscription;
    .locals 1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$Y8R7_Nz4GvjpqoBkqInh1t1UYHg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$Y8R7_Nz4GvjpqoBkqInh1t1UYHg;-><init>(Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;)V

    .line 97
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 139
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 71
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    iput-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->screen:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->screen:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->uniqueKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->uniqueKey:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->screen:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->screen:Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->remainingBalance:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->remainingBalance:Lcom/squareup/protos/common/Money;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 81
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 86
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->initActionBar()V

    .line 92
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->updateMessages()V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v1}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->isCard()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    new-instance v1, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$n3J7Ws2O4VoggKhMpIF59o4G-kM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidPresenter$n3J7Ws2O4VoggKhMpIF59o4G-kM;-><init>(Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;Lcom/squareup/payment/Payment;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method
