.class public final Lcom/squareup/ui/buyer/BuyerAmountTextProvider;
.super Ljava/lang/Object;
.source "BuyerAmountTextProvider.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/FormattedTotalProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u000fJ\u001e\u0010\u0012\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0013\u001a\u00020\u00052\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007J\u0008\u0010\u0015\u001a\u00020\u0011H\u0016J\u0016\u0010\u0015\u001a\u00020\u00112\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0016J\u001c\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007J\u0008\u0010\u0017\u001a\u00020\u0008H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/CountryCode;)V",
        "getBuyerFormattedAmountDueAutoGratuityAndTip",
        "",
        "getBuyerFormattedTotalAmount",
        "",
        "getFormattedAmountDueAutoGratuityAndTip",
        "buyerRes",
        "buyerMoneyFormatter",
        "getFormattedTotalAmount",
        "getFormattedTotalTenderAndTipBreakdown",
        "getTenderAmount",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/CountryCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p5, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method private final getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requirePayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.requirePayment().tenderAmount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final getBuyerFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 38
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getBuyerFormattedTotalAmount()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    .line 88
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "buyerRes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerMoneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasNonZeroTip()Z

    move-result v0

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string/jumbo v2, "tip"

    const-string v3, "amount"

    if-eqz v1, :cond_4

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    const-string v5, "auto_grat"

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->countryCode:Lcom/squareup/CountryCode;

    sget-object v8, Lcom/squareup/ui/buyer/BuyerAmountTextProvider$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v8, v0

    if-eq v0, v7, :cond_1

    if-eq v0, v6, :cond_1

    .line 61
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_plus_auto_grat_plus_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    .line 60
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_plus_service_charge_plus_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 63
    :goto_0
    invoke-interface {p2, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 64
    invoke-interface {p2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_2

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->countryCode:Lcom/squareup/CountryCode;

    sget-object v2, Lcom/squareup/ui/buyer/BuyerAmountTextProvider$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v2, v0

    if-eq v0, v7, :cond_3

    if-eq v0, v6, :cond_3

    .line 70
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_no_tip_amount_plus_auto_grat:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_1

    .line 69
    :cond_3
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_no_tip_amount_plus_service_charge:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 71
    :goto_1
    invoke-interface {p2, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 72
    invoke-interface {p2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v5, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_2
    return-object p1

    .line 77
    :cond_4
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_plus_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getFormattedTotalAmount()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "buyerMoneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 95
    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getFormattedTotalTenderAndTipBreakdown(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_tip_action_bar:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string/jumbo v1, "total"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "amount"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string/jumbo v0, "tip"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
