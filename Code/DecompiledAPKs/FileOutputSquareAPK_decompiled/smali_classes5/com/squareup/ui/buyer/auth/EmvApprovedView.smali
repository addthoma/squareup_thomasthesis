.class public Lcom/squareup/ui/buyer/auth/EmvApprovedView;
.super Landroid/widget/LinearLayout;
.source "EmvApprovedView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-class p2, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;->inject(Lcom/squareup/ui/buyer/auth/EmvApprovedView;)V

    .line 34
    sget p2, Lcom/squareup/checkout/R$layout;->auth_spinner_glyph:I

    const/4 v0, 0x0

    .line 35
    invoke-static {p1, p2, v0}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 80
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 81
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->titleView:Landroid/widget/TextView;

    .line 82
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 83
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->presenter:Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->presenter:Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->bindViews()V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->presenter:Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 55
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 56
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method setText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method transitionToCheck()V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    return-void
.end method
