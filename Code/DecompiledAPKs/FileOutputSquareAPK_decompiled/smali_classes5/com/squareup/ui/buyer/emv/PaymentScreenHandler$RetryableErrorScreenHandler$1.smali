.class Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;->retryPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler$1;->this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler$1;->this$0:Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;

    iget-object p1, p1, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->reauthorize()V

    return-void
.end method
