.class public interface abstract Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;
.super Ljava/lang/Object;
.source "BuyerFlowWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 \t2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\t\nJ\u0008\u0010\u0003\u001a\u00020\u0004H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "isWorkflowRunning",
        "",
        "start",
        "",
        "initEvent",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "Companion",
        "ParentComponent",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->Companion:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract isWorkflowRunning()Z
.end method

.method public abstract start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V
.end method
