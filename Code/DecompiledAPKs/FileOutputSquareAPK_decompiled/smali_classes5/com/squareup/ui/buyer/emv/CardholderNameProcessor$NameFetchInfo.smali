.class public Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;
.super Ljava/lang/Object;
.source "CardholderNameProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NameFetchInfo"
.end annotation


# instance fields
.field private applications:[Lcom/squareup/cardreader/EmvApplication;

.field private canSkipPin:Z

.field private cardInfo:Lcom/squareup/cardreader/CardInfo;

.field private dipPaymentHasStarted:Z

.field private isFinalPinAttempt:Z

.field private result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field private secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382
    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-void
.end method


# virtual methods
.method public getApplications()[Lcom/squareup/cardreader/EmvApplication;
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->applications:[Lcom/squareup/cardreader/EmvApplication;

    return-object v0
.end method

.method public getCanSkipPin()Z
    .locals 1

    .line 437
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->canSkipPin:Z

    return v0
.end method

.method public getCardInfo()Lcom/squareup/cardreader/CardInfo;
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-object v0
.end method

.method public getFinalPinAttempt()Z
    .locals 1

    .line 441
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->isFinalPinAttempt:Z

    return v0
.end method

.method public getPinRequestInfo()Lcom/squareup/cardreader/PinRequestData;
    .locals 4

    .line 451
    new-instance v0, Lcom/squareup/cardreader/PinRequestData;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->canSkipPin:Z

    iget-boolean v3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->isFinalPinAttempt:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/PinRequestData;-><init>(Lcom/squareup/cardreader/CardInfo;ZZ)V

    return-object v0
.end method

.method public getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-object v0
.end method

.method public getSecureTouchPinRequestData()Lcom/squareup/securetouch/SecureTouchPinRequestData;
    .locals 2

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    const-string v1, "secureTouchPinRequestData"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/securetouch/SecureTouchPinRequestData;

    return-object v0
.end method

.method public hasDipPaymentStarted()Z
    .locals 1

    .line 400
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->dipPaymentHasStarted:Z

    return v0
.end method

.method public reset()V
    .locals 1

    .line 404
    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v0, 0x0

    .line 405
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->dipPaymentHasStarted:Z

    const/4 v0, 0x0

    .line 406
    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->applications:[Lcom/squareup/cardreader/EmvApplication;

    .line 407
    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    return-void
.end method

.method public setApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 2

    .line 425
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->applications:[Lcom/squareup/cardreader/EmvApplication;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "CardholderNameProcessor::setApplications applications is not null"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    const-string v0, "EmvApplication[] cannot be null"

    .line 427
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 428
    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_APPLICATION:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 429
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->applications:[Lcom/squareup/cardreader/EmvApplication;

    return-void
.end method

.method public setDipPaymentStarted()V
    .locals 1

    const/4 v0, 0x1

    .line 417
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->dipPaymentHasStarted:Z

    return-void
.end method

.method public setPinRequestInfo(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 1

    .line 445
    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getCardInfo()Lcom/squareup/cardreader/CardInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 446
    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getCanSkip()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->canSkipPin:Z

    .line 447
    invoke-virtual {p1}, Lcom/squareup/cardreader/PinRequestData;->getFinalPinAttempt()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->isFinalPinAttempt:Z

    return-void
.end method

.method public setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V
    .locals 2

    .line 411
    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_APPLICATION:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "CardholderNameProcessor::setResult result is dip application"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 413
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->result:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-void
.end method

.method public setSecureTouchPinRequestData(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    return-void
.end method
