.class public Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;
.super Lmortar/ViewPresenter;
.source "RetryTenderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/retry/RetryTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final billPayment:Lcom/squareup/payment/BillPayment;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

.field private final strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/payment/offline/StoreAndForwardKeys;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 59
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 61
    iput-object p2, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 62
    iput-object p3, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 63
    iput-object p4, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    .line 65
    iput-object p6, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 66
    iput-object p7, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 67
    iput-object p8, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 68
    iput-object p9, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 69
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->billPayment:Lcom/squareup/payment/BillPayment;

    .line 70
    iput-object p10, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    return-void
.end method

.method private allowEditingDeviceProfileBackedSettings()Z
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseDeviceSettings()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private automaticallyRetryInOfflineMode()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;->maybeAutomaticallyRetryInOfflineMode()V

    return-void
.end method

.method private showOfflineButtonIfAvailable(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardKeys;->hasAllKeys()Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    sget-object v1, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->DISABLED:Lcom/squareup/analytics/StoreAndForwardAnalytics$State;

    invoke-virtual {v0, v1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logOptInEnabledState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V

    .line 164
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->offline_mode_unavailable:I

    invoke-virtual {p1, v0, v2}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineButton(IZ)V

    .line 165
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->offline_mode_unavailable_hint:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isOfflineTransactionLimitExceeded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logTransactionLimitExceeded()V

    .line 171
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->offline_mode_unavailable:I

    invoke-virtual {p1, v0, v2}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineButton(IZ)V

    .line 172
    sget v0, Lcom/squareup/transaction/R$string;->offline_mode_transaction_limit_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    sget-object v2, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->ENABLED:Lcom/squareup/analytics/StoreAndForwardAnalytics$State;

    invoke-virtual {v0, v2}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logOptInEnabledState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V

    .line 178
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->enter_offline_mode:I

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineButton(IZ)V

    .line 179
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->setOfflineButtonToEnterOnClick()V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineMessage()V

    return-void

    .line 184
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->allowEditingDeviceProfileBackedSettings()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->canOptInToStoreAndForwardPayments()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    .line 186
    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardKeys;->hasAllKeys()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 187
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isOfflineTransactionLimitExceeded()Z

    move-result v0

    if-nez v0, :cond_3

    .line 189
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->try_offline_mode:I

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineButton(IZ)V

    .line 190
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->try_offline_mode_hint:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->setOfflineButtonToShowQuickEnableCardOnClick()V

    return-void

    .line 196
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineButton()V

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineMessage()V

    return-void
.end method

.method private updateOfflineButtonAndMessage()V
    .locals 3

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/retry/RetryTenderView;

    .line 117
    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineButton()V

    .line 119
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->invoice_unsupported_offline_mode:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    return-void

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasGiftCardItem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineButton()V

    .line 126
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->offline_mode_cannot_purchase_gift_cards:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    return-void

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    invoke-interface {v1}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;->checkBillPaymentForOfflineModeButton()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->billPayment:Lcom/squareup/payment/BillPayment;

    if-eqz v1, :cond_3

    .line 133
    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->isLocalPayment()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->showOfflineButtonIfAvailable(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    return-void

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->billPayment:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object v1

    goto :goto_0

    .line 139
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    .line 142
    iget-object v2, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v2, v1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 143
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineButton()V

    .line 144
    sget v1, Lcom/squareup/transaction/R$string;->offline_mode_non_whitelisted_brand:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->showOfflineMessage(I)V

    return-void

    :cond_4
    if-eqz v1, :cond_5

    .line 148
    invoke-virtual {v1}, Lcom/squareup/Card;->isManual()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 149
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineButton()V

    .line 150
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->hideOfflineMessage()V

    return-void

    .line 154
    :cond_5
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->showOfflineButtonIfAvailable(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    return-void
.end method


# virtual methods
.method onBackPressed()Z
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;->confirmCancelPayment()V

    const/4 v0, 0x1

    return v0
.end method

.method onEnterOfflineClicked()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logGoOfflinePressed()V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->enterOfflineMode()V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;->onEnterOfflineMode()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {p1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->automaticallyRetryInOfflineMode()V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/retry/RetryTenderView;

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->setTotal(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 87
    invoke-direct {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->updateOfflineButtonAndMessage()V

    return-void
.end method

.method onShowOfflineEnableCardClicked()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToStoreAndForward()V

    return-void
.end method

.method retryPayment()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logOptInRetry()V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->strategy:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;->retryPayment()V

    return-void
.end method
