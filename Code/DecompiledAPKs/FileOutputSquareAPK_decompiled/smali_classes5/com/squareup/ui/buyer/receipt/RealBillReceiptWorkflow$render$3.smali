.class final Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBillReceiptWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->render(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/ui/buyer/receipt/BillReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
        "+",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "it",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

.field final synthetic this$0:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;->this$0:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;->$props:Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;->$props:Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/PaymentReceipt;->updateBuyerLanguage(Ljava/util/Locale;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;->this$0:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;

    sget-object v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3$1;->INSTANCE:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;->invoke(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
