.class public final Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "BuyerScopeViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerScopeViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerScopeViewFactory.kt\ncom/squareup/ui/buyer/workflow/BuyerScopeViewFactory\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,30:1\n37#2,2:31\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerScopeViewFactory.kt\ncom/squareup/ui/buyer/workflow/BuyerScopeViewFactory\n*L\n20#1,2:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B,\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0013\u0008\u0001\u0010\u0006\u001a\r\u0012\t\u0012\u00070\u0008\u00a2\u0006\u0002\u0008\t0\u0007\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "storeAndForwardQuickEnableFactory",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
        "separatedPrintoutsViewFactory",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
        "viewFactories",
        "",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;Ljava/util/Set;)V",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;Ljava/util/Set;)V
    .locals 23
    .param p3    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "storeAndForwardQuickEnableFactory"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "separatedPrintoutsViewFactory"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "viewFactories"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v3, Lkotlin/jvm/internal/SpreadBuilder;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lkotlin/jvm/internal/SpreadBuilder;-><init>(I)V

    .line 20
    check-cast v2, Ljava/util/Collection;

    const/4 v4, 0x0

    new-array v5, v4, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 32
    invoke-interface {v2, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v3, v2}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    .line 21
    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v3, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    .line 22
    new-instance v1, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory$1;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 23
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 24
    const-class v6, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v6

    .line 25
    sget v7, Lcom/squareup/ui/buyerflow/R$layout;->store_and_forward_quick_enable_view:I

    .line 26
    new-instance v8, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory$2;

    invoke-direct {v8, v0}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory$2;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;)V

    move-object v10, v8

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x0

    .line 27
    new-instance v8, Lcom/squareup/workflow/ScreenHint;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    sget-object v17, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1df

    const/16 v22, 0x0

    move-object v11, v8

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v11, 0x8

    .line 23
    invoke-static/range {v5 .. v12}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v5

    check-cast v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    aput-object v5, v2, v4

    invoke-direct {v1, v0, v2}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory$1;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v3, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lkotlin/jvm/internal/SpreadBuilder;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v3, v0}, Lkotlin/jvm/internal/SpreadBuilder;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    move-object/from16 v1, p0

    .line 19
    invoke-direct {v1, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void

    :cond_0
    move-object/from16 v1, p0

    .line 32
    new-instance v0, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
