.class public final Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;
.super Ljava/lang/Object;
.source "BuyerScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final autoCaptureControlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;"
        }
    .end annotation
.end field

.field private final autoCaptureNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoCaptureNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final autoVoidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFacingScreensStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFacingScreensState;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final changeHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final displayNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final emailAndLoyaltyScopeHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEventPublisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final separatedPrintoutsLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarEventManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoCaptureNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFacingScreensState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoCaptureNotifierProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerWorkflowProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFacingScreensStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->softInputProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 189
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 194
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 195
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 196
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFlowWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->displayNameProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->emailAndLoyaltyScopeHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p37

    .line 206
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->separatedPrintoutsLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p38

    .line 207
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->receiptHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p39

    .line 208
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p40

    .line 209
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;
    .locals 42
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoCaptureNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFacingScreensState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    move-object/from16 v40, p39

    .line 254
    new-instance v41, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;

    move-object/from16 v0, v41

    invoke-direct/range {v0 .. v40}, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v41
.end method

.method public static newInstance(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/notifications/AutoCaptureNotifier;Lcom/squareup/payment/AutoVoid;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadBus;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/ui/NfcProcessor;Ljava/lang/Object;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/buyer/BuyerFacingScreensState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiTransactionState;Lflow/Flow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/loyalty/LoyaltyEventPublisher;ZLcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/ui/buyer/DisplayNameProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;Ldagger/Lazy;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/buyer/BuyerScopeRunner;
    .locals 42
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/notifications/AutoCaptureNotifier;",
            "Lcom/squareup/payment/AutoVoid;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/ui/buyer/BuyerFacingScreensState;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lflow/Flow;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            "Z",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            "Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
            ">;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ")",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    move-object/from16 v40, p39

    .line 278
    new-instance v41, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-object/from16 v0, v41

    move-object/from16 v10, p9

    check-cast v10, Lcom/squareup/ui/buyer/BuyerWorkflow;

    invoke-direct/range {v0 .. v40}, Lcom/squareup/ui/buyer/BuyerScopeRunner;-><init>(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/notifications/AutoCaptureNotifier;Lcom/squareup/payment/AutoVoid;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadBus;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/buyer/BuyerWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/buyer/BuyerFacingScreensState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiTransactionState;Lflow/Flow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/loyalty/LoyaltyEventPublisher;ZLcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/ui/buyer/DisplayNameProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;Ldagger/Lazy;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V

    return-object v41
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/BuyerScopeRunner;
    .locals 42

    move-object/from16 v0, p0

    .line 214
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/autocapture/AutoCaptureControl;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoCaptureNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/notifications/AutoCaptureNotifier;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/payment/AutoVoid;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/TouchEventMonitor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFacingScreensStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/buyer/BuyerFacingScreensState;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->softInputProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/api/ApiTransactionController;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/loyalty/LoyaltyEventPublisher;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/statusbar/event/StatusBarEventManager;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerFlowWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->displayNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/ui/buyer/DisplayNameProvider;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->emailAndLoyaltyScopeHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v36, v1

    check-cast v36, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v37, v1

    check-cast v37, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->separatedPrintoutsLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v38, v1

    check-cast v38, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->receiptHelperProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v39

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v40, v1

    check-cast v40, Lcom/squareup/receipt/ReceiptSender;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v41, v1

    check-cast v41, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-static/range {v2 .. v41}, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->newInstance(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/notifications/AutoCaptureNotifier;Lcom/squareup/payment/AutoVoid;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadBus;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/ui/NfcProcessor;Ljava/lang/Object;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/buyer/BuyerFacingScreensState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiTransactionState;Lflow/Flow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/loyalty/LoyaltyEventPublisher;ZLcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/ui/buyer/DisplayNameProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsLauncher;Ldagger/Lazy;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeRunner_Factory;->get()Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-result-object v0

    return-object v0
.end method
