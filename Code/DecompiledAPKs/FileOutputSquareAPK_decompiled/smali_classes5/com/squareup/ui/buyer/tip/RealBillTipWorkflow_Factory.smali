.class public final Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealBillTipWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final tipReaderHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tipWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->tipReaderHandlerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->tipWorkflowProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->mainDispatcherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;)",
            "Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->tipReaderHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    iget-object v2, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->tipWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->mainDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow_Factory;->get()Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;

    move-result-object v0

    return-object v0
.end method
