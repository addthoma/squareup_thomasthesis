.class public abstract Lcom/squareup/ui/buyer/signature/SignScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "SignScreen.java"

# interfaces
.implements Lcom/squareup/ui/buyer/InSignScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/signature/SignScreen$Component;,
        Lcom/squareup/ui/buyer/signature/SignScreen$PortraitSignatureScreen;,
        Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;
    }
.end annotation


# static fields
.field public static final SHOWN:Ljava/lang/String; = "Shown SignScreen"


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 122
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/signature/SignScreen$1;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method public static getSignScreen(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/signature/SignScreen;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->PORTRAIT_SIGNATURE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 52
    new-instance p1, Lcom/squareup/ui/buyer/signature/SignScreen$PortraitSignatureScreen;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/signature/SignScreen$PortraitSignatureScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/signature/SignScreen$1;)V

    return-object p1

    .line 54
    :cond_0
    new-instance p1, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/signature/SignScreen$1;)V

    return-object p1
.end method

.method static synthetic lambda$provideCoordinator$0(Lcom/squareup/ui/buyer/signature/SignScreenData;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    const-class v1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    invoke-direct {v1, v0, p0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 126
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 108
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 135
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 4

    .line 139
    const-class v0, Lcom/squareup/ui/buyer/signature/SignScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/signature/SignScreen$Component;

    .line 140
    invoke-interface {p1}, Lcom/squareup/ui/buyer/signature/SignScreen$Component;->signCoordinatorFactory()Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;

    move-result-object v0

    .line 141
    invoke-interface {p1}, Lcom/squareup/ui/buyer/signature/SignScreen$Component;->signScreenRunner()Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->screenData()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v1, Lcom/squareup/ui/buyer/signature/-$$Lambda$SignScreen$2PXFzBFCy84HNtJprOVWTd8-8FQ;->INSTANCE:Lcom/squareup/ui/buyer/signature/-$$Lambda$SignScreen$2PXFzBFCy84HNtJprOVWTd8-8FQ;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 152
    new-instance v1, Lcom/squareup/workflow/LayoutRunnerCoordinator;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/buyer/signature/-$$Lambda$SgqjcvJ1SQHIo_717rELhGRFEug;

    invoke-direct {v2, v0}, Lcom/squareup/ui/buyer/signature/-$$Lambda$SgqjcvJ1SQHIo_717rELhGRFEug;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;)V

    new-instance v0, Lcom/squareup/workflow/ui/ContainerHints;

    const-class v3, Lcom/squareup/workflow/ui/ViewRegistry;

    .line 155
    invoke-static {v3}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-direct {v0, v3}, Lcom/squareup/workflow/ui/ContainerHints;-><init>(Lcom/squareup/workflow/ui/ViewRegistry;)V

    invoke-direct {v1, p1, v2, v0}, Lcom/squareup/workflow/LayoutRunnerCoordinator;-><init>(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-object v1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 160
    const-class v0, Lcom/squareup/ui/buyer/signature/SignScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/signature/SignScreen$Component;

    .line 161
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 162
    invoke-interface {v0}, Lcom/squareup/ui/buyer/signature/SignScreen$Component;->signScreenRunner()Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 131
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->sign_view:I

    return v0
.end method
