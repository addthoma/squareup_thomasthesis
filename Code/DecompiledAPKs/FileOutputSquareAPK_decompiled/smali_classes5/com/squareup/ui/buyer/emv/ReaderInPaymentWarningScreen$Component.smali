.class public interface abstract Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;
.super Ljava/lang/Object;
.source "ReaderInPaymentWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/ReaderWarningTypeHandlerFactory$ReaderInPaymentWarningScreenParentComponent;
.implements Lcom/squareup/ui/main/errors/ReaderWarningView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract dipRequiredFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;
.end method

.method public abstract emvSchemeFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;
.end method

.method public abstract emvTechnicalFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;
.end method

.method public abstract retryableError()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;
.end method
