.class public final Lcom/squareup/ui/buyer/BuyerScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "BuyerScope.java"

# interfaces
.implements Lcom/squareup/container/spot/ModalBodyScreen;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/BuyerScopeComponent;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/BuyerScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/ui/buyer/-$$Lambda$BuyerScope$KhZ4CSdp5xvN5GAKJtYezmq5zoQ;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$BuyerScope$KhZ4CSdp5xvN5GAKJtYezmq5zoQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/BuyerScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    return-void
.end method

.method public static create()Lcom/squareup/ui/buyer/BuyerScope;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/ui/buyer/BuyerScope;

    sget-object v1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->NONE:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/BuyerScope;-><init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-object v0
.end method

.method public static fromContactlessPin(Lcom/squareup/ui/buyer/ContactlessPinRequest;)Lcom/squareup/ui/buyer/BuyerScope;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/BuyerScope;-><init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/BuyerScope;
    .locals 5

    .line 69
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 70
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v2, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 71
    :goto_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    if-ne p0, v2, :cond_2

    const/4 v1, 0x1

    .line 72
    :cond_2
    new-instance p0, Lcom/squareup/ui/buyer/BuyerScope;

    new-instance v2, Lcom/squareup/ui/buyer/ContactlessPinRequest;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v1, v4}, Lcom/squareup/ui/buyer/ContactlessPinRequest;-><init>(ZZZLcom/squareup/cardreader/CardInfo;)V

    invoke-direct {p0, v2}, Lcom/squareup/ui/buyer/BuyerScope;-><init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-object p0
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    .line 34
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 36
    const-class v1, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$ParentComponent;

    .line 37
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$ParentComponent;

    .line 39
    invoke-interface {p1}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$ParentComponent;->buyerScopeWorkflowRunner()Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 63
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/ContactlessPinRequest;->canSkip:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 19
    const-class v0, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    .line 20
    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->scopeRunner()Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 21
    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->displayNameProvider()Lcom/squareup/ui/buyer/DisplayNameProvider;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 22
    new-instance v1, Lcom/squareup/ui/buyer/BuyerScope$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/buyer/BuyerScope$1;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/BuyerScopeComponent;)V

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
