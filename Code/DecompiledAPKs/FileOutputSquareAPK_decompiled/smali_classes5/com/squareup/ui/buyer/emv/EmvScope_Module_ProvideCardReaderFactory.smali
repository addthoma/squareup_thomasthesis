.class public final Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;
.super Ljava/lang/Object;
.source "EmvScope_Module_ProvideCardReaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReader;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReader(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/CardReader;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->provideCardReader(Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/CardReader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReader;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->provideCardReader(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideCardReaderFactory;->get()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    return-object v0
.end method
