.class public final Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;
.super Ljava/lang/Object;
.source "PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final magicBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->magicBusProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;-><init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Res;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->magicBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->newInstance(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvSchemeFallbackScreenHandler_Factory;->get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method
