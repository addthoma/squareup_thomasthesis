.class public final Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;
.super Ljava/lang/Object;
.source "InvoicePaidScreenDataFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final offlineModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)",
            "Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ")",
            "Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory_Factory;->get()Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;

    move-result-object v0

    return-object v0
.end method
