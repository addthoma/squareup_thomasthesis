.class public final Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;
.super Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction;
.source "StoreAndForwardQuickEnableWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CancelPayment"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0005\u001a\u00020\u0003H\u00c2\u0003J\u0013\u0010\u0006\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\u000cH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;",
        "Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction;",
        "analytics",
        "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
        "(Lcom/squareup/analytics/StoreAndForwardAnalytics;)V",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/StoreAndForwardAnalytics;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 100
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    return-void
.end method

.method private final component1()Lcom/squareup/analytics/StoreAndForwardAnalytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;Lcom/squareup/analytics/StoreAndForwardAnalytics;ILjava/lang/Object;)Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->copy(Lcom/squareup/analytics/StoreAndForwardAnalytics;)Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
            ">;)",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logQuickEnableRefused()V

    .line 103
    sget-object p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult$Cancel;->INSTANCE:Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult$Cancel;

    check-cast p1, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 98
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;

    move-result-object p1

    return-object p1
.end method

.method public final copy(Lcom/squareup/analytics/StoreAndForwardAnalytics;)Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;-><init>(Lcom/squareup/analytics/StoreAndForwardAnalytics;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;

    iget-object v0, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object p1, p1, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CancelPayment(analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/storeandforward/ShowingScreenAction$CancelPayment;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
