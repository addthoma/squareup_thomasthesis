.class Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CancelEmvPaymentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;->lambda$create$1(Landroid/content/Context;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;

.field final synthetic val$emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;->this$0:Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;->val$emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen$Factory$1;->val$emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    return-void
.end method
