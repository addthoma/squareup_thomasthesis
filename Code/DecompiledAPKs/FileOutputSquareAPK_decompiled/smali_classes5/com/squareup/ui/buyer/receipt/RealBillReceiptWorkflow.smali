.class public final Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealBillReceiptWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;,
        Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptInput;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBillReceiptWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBillReceiptWorkflow.kt\ncom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,230:1\n41#2:231\n56#2,2:232\n276#3:234\n*E\n*S KotlinDebug\n*F\n+ 1 RealBillReceiptWorkflow.kt\ncom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow\n*L\n56#1:231\n56#1,2:232\n56#1:234\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u0000 12\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002:\u000212BA\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0003H\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J\u0015\u0010$\u001a\u00020%2\u0006\u0010\u001f\u001a\u00020\u0003H\u0001\u00a2\u0006\u0002\u0008&J\u001a\u0010\'\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00032\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0016JR\u0010*\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020\u00032\u0006\u0010+\u001a\u00020\u00042\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050-H\u0016J\u0010\u0010.\u001a\u00020/2\u0006\u0010\"\u001a\u00020#H\u0002J\u0010\u00100\u001a\u00020)2\u0006\u0010+\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptInput;",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "receiptWorkflow",
        "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
        "buyerFlowReceiptManager",
        "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
        "checkoutInformationEventLogger",
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "customerManagementSettings",
        "Lcom/squareup/crm/CustomerManagementSettings;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "offlineMode",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "buyerLanguageWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "createReceiptInput",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
        "props",
        "getPaymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "init",
        "",
        "init$buyer_flow_release",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showAddCardButton",
        "",
        "snapshotState",
        "Companion",
        "CustomerChangedWorker",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BUYER_LANGUAGE_WORKER:Ljava/lang/String; = "buyer language worker"

.field public static final CUSTOMER_CHANGED_WORKER:Ljava/lang/String; = "customer changed worker"

.field public static final Companion:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$Companion;

.field public static final INIT_WORKER:Ljava/lang/String; = "init worker"


# instance fields
.field private final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final buyerLanguageWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private final receiptWorkflow:Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->Companion:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .param p1    # Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;
        .annotation runtime Lcom/squareup/checkoutflow/datamodels/ForBills;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "receiptWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowReceiptManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutInformationEventLogger"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerManagementSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineMode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->receiptWorkflow:Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    iput-object p4, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p6, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object p7, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {p1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object p1

    .line 231
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string/jumbo p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 233
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 234
    const-class p2, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 231
    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerLanguageWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 233
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final createReceiptInput(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;
    .locals 22

    move-object/from16 v0, p0

    .line 114
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    .line 115
    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    const-string v3, "receipt.payment"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v5

    .line 117
    iget-object v2, v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v2

    const/4 v4, 0x1

    const/4 v6, 0x0

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 119
    :goto_0
    iget-object v7, v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v7}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    instance-of v7, v1, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v7, 0x1

    .line 120
    :goto_2
    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v8, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 121
    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result v9

    .line 122
    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->showAddCardButton(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result v10

    .line 123
    iget-object v3, v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v3}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerLocale()Ljava/util/Locale;

    move-result-object v19

    .line 124
    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Payment;->shouldAutoSendReceipt()Z

    move-result v20

    .line 125
    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->getPaymentTypeInfo(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v21

    .line 127
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    move-object v4, v1

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v11

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getShowLanguageSelection()Z

    move-result v12

    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    .line 137
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v14

    .line 138
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getSupportsSms()Z

    move-result v15

    .line 139
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getAutoReceiptCompleteTimeout()Ljava/lang/Long;

    move-result-object v16

    .line 140
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getShowSmsMarketing()Z

    move-result v17

    .line 141
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getBusinessName()Ljava/lang/String;

    move-result-object v18

    move v6, v2

    .line 127
    invoke-direct/range {v4 .. v21}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;-><init>(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V

    return-object v1
.end method

.method private final getPaymentTypeInfo(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 10

    .line 187
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-lez v0, :cond_0

    .line 188
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    const-string v4, "receipt.remainingAmountDue"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    goto :goto_0

    .line 190
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$NoRemainingAmount;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$NoRemainingAmount;

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    :goto_0
    move-object v4, v0

    .line 193
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string v3, "receipt.payment"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 195
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 196
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;

    .line 198
    invoke-interface {v5}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v3, "it.tendered"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-interface {v5}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v1

    if-eqz v3, :cond_2

    :goto_1
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$HasChange;

    invoke-interface {v5}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const-string v3, "it.change"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$HasChange;-><init>(Lcom/squareup/protos/common/Money;)V

    :goto_2
    check-cast v1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig;

    goto :goto_3

    :cond_2
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$NoChange;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$NoChange;

    goto :goto_2

    .line 196
    :goto_3
    invoke-direct {p1, v4, v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig;)V

    .line 195
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    goto :goto_6

    .line 203
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    const-string v6, "receipt.payment.tenderAmount"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v6

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v6

    const-string v3, "receipt.payment.total"

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 206
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$HasRemainingBalance;

    new-instance v7, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;

    invoke-direct {v7, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;-><init>(Lcom/squareup/payment/PaymentReceipt;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-direct {v3, v7}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$HasRemainingBalance;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 205
    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    move-object v7, v3

    goto :goto_4

    .line 207
    :cond_4
    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$NoRemainingBalance;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$NoRemainingBalance;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    move-object v7, p1

    :goto_4
    if-eqz v0, :cond_5

    .line 208
    iget-object p1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long p1, v8, v1

    if-lez p1, :cond_5

    .line 209
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$HasTip;

    invoke-direct {p1, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$HasTip;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    goto :goto_5

    .line 211
    :cond_5
    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$NoTip;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$NoTip;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    :goto_5
    move-object v8, p1

    .line 201
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;)V

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    :goto_6
    return-object p1
.end method

.method private final showAddCardButton(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 5

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->isCard()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 153
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_8

    instance-of v0, p1, Lcom/squareup/payment/BillPayment;

    if-nez v0, :cond_1

    goto/16 :goto_1

    .line 157
    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasMagStripeTenderInFlight()Z

    move-result v2

    .line 158
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasEmvTenderInFlight()Z

    move-result v3

    .line 159
    iget-object v4, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v4, v2, v3}, Lcom/squareup/crm/CustomerManagementSettings;->showCardButtonEnabledForTenderType(ZZ)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 167
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 168
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object v0

    const-string v2, "card"

    .line 169
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    const-string v3, "card.pan"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 172
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    if-eqz v3, :cond_5

    .line 174
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v3}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v4

    if-ne v3, v4, :cond_4

    return v1

    .line 169
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    const/4 p1, 0x1

    return p1

    :cond_8
    :goto_1
    return v1
.end method


# virtual methods
.method public final init$buyer_flow_release(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;)V
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/log/CheckoutInformationEventLogger;->setEmailOnFile(Z)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lio/reactivex/Observable;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public initialState(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/buyer/receipt/BillReceiptState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object p1, Lcom/squareup/ui/buyer/receipt/BillReceiptState;->INSTANCE:Lcom/squareup/ui/buyer/receipt/BillReceiptState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->initialState(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/buyer/receipt/BillReceiptState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    check-cast p2, Lcom/squareup/ui/buyer/receipt/BillReceiptState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->render(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/ui/buyer/receipt/BillReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lcom/squareup/ui/buyer/receipt/BillReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptInput;",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "context"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$1;-><init>(Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptInput;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p2

    const-string v0, "init worker"

    invoke-static {p3, p2, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 77
    new-instance p2, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;-><init>(Lcom/squareup/payment/PaymentReceipt;)V

    check-cast p2, Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$2;-><init>(Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "customer changed worker"

    invoke-interface {p3, p2, v1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->buyerLanguageWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$3;-><init>(Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "buyer language worker"

    invoke-interface {p3, p2, v1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->receiptWorkflow:Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->createReceiptInput(Lcom/squareup/ui/buyer/receipt/BillReceiptInput;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    move-result-object v2

    .line 93
    sget-object p1, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$4;->INSTANCE:Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$render$4;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 90
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/ui/buyer/receipt/BillReceiptState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/ui/buyer/receipt/BillReceiptState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->snapshotState(Lcom/squareup/ui/buyer/receipt/BillReceiptState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
