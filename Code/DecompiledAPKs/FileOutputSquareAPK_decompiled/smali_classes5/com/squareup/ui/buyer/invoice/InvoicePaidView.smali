.class public Lcom/squareup/ui/buyer/invoice/InvoicePaidView;
.super Landroid/widget/LinearLayout;
.source "InvoicePaidView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-class p2, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;->inject(Lcom/squareup/ui/buyer/invoice/InvoicePaidView;)V

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 34
    new-instance v0, Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {v0, v1, v2}, Lcom/squareup/glyph/SquareGlyphView;-><init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 37
    sget v1, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 38
    sget v1, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->titleView:Landroid/widget/TextView;

    .line 39
    sget v1, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    iput-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 40
    sget v1, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->presenter:Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->presenter:Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->presenter:Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidPresenter;->dropView(Ljava/lang/Object;)V

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
