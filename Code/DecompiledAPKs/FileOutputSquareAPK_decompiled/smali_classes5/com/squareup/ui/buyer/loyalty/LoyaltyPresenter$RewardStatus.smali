.class Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;
.super Ljava/lang/Object;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RewardStatus"
.end annotation


# instance fields
.field final congratulationsTitle:Ljava/lang/String;

.field final newEnrollment:Z

.field final newZeroStarEnrollment:Z

.field final obfuscatedPhoneNumber:Ljava/lang/String;

.field final phoneToken:Ljava/lang/String;

.field final pointsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

.field final pointsTerm:Ljava/lang/String;

.field final programName:Ljava/lang/String;

.field final rewardsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

.field final rewardsTerm:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->pointsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

    .line 154
    iput-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->pointsTerm:Ljava/lang/String;

    .line 155
    iput-object p3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->programName:Ljava/lang/String;

    .line 156
    iput-object p4, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->congratulationsTitle:Ljava/lang/String;

    .line 157
    iput-object p5, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->rewardsAmount:Lcom/squareup/loyalty/LoyaltyRewardText;

    .line 158
    iput-object p6, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->rewardsTerm:Ljava/lang/String;

    .line 159
    iput-object p7, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->obfuscatedPhoneNumber:Ljava/lang/String;

    .line 160
    iput-object p8, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->phoneToken:Ljava/lang/String;

    .line 161
    iput-boolean p9, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->newZeroStarEnrollment:Z

    .line 162
    iput-boolean p10, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;->newEnrollment:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$1;)V
    .locals 0

    .line 136
    invoke-direct/range {p0 .. p10}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$RewardStatus;-><init>(Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method
