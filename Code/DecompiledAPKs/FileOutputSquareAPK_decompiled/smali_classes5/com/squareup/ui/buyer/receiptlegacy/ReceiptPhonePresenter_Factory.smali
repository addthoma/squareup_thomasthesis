.class public final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;
.super Ljava/lang/Object;
.source "ReceiptPhonePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final printSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptAutoCloseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptEmailAndLoyaltyHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptAutoCloseProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptEmailAndLoyaltyHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;)",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 200
    new-instance v32, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v32
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ")",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 218
    new-instance v32, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    return-object v32
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;
    .locals 33

    move-object/from16 v0, p0

    .line 169
    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/receipt/ReceiptSender;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptAutoCloseProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/print/PrintSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->receiptEmailAndLoyaltyHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static/range {v2 .. v32}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter_Factory;->get()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    move-result-object v0

    return-object v0
.end method
