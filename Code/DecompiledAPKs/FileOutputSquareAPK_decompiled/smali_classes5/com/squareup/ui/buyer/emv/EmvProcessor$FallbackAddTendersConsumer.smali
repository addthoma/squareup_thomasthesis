.class Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;
.super Ljava/lang/Object;
.source "EmvProcessor.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FallbackAddTendersConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V
    .locals 0

    .line 779
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private finishedWithFallbackCardReader()V
    .locals 2

    .line 823
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)V"
        }
    .end annotation

    .line 783
    new-instance v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$FallbackAddTendersConsumer$BobtxJtUCemTEbRDZFPvUjB8DN0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$FallbackAddTendersConsumer$BobtxJtUCemTEbRDZFPvUjB8DN0;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;)V

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$FallbackAddTendersConsumer$45PmPsg5CXrwS7FGbdB4OMqXQwQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$FallbackAddTendersConsumer$45PmPsg5CXrwS7FGbdB4OMqXQwQ;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 779
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public synthetic lambda$accept$0$EmvProcessor$FallbackAddTendersConsumer(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 784
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentApproved()V

    .line 785
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1300(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    .line 786
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->finishedWithFallbackCardReader()V

    return-void
.end method

.method public synthetic lambda$accept$1$EmvProcessor$FallbackAddTendersConsumer(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 789
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 791
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    .line 792
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 793
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    .line 796
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1600(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/protos/client/Status;)V

    .line 798
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 799
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentDeclined(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V

    .line 801
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1400(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    .line 802
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->finishedWithFallbackCardReader()V

    goto :goto_0

    .line 803
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_1

    .line 804
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->retryableError()V

    goto :goto_0

    .line 805
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_3

    .line 809
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->square_unavailable_title:I

    sget v3, Lcom/squareup/ui/buyerflow/R$string;->square_unavailable_message:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1700(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 812
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1800(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    .line 814
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->finishedWithFallbackCardReader()V

    goto :goto_0

    .line 815
    :cond_3
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_4

    .line 816
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    .line 817
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;->finishedWithFallbackCardReader()V

    :cond_4
    :goto_0
    return-void
.end method
