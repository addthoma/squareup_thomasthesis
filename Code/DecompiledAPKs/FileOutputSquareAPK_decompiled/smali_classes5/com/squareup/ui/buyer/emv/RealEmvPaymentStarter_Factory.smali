.class public final Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;
.super Ljava/lang/Object;
.source "RealEmvPaymentStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;"
        }
    .end annotation

    .line 73
    new-instance v10, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;)Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;
    .locals 11

    .line 80
    new-instance v10, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;-><init>(Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;
    .locals 10

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/log/ReaderSessionIds;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->newInstance(Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;)Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter_Factory;->get()Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;

    move-result-object v0

    return-object v0
.end method
