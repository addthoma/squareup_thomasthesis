.class public abstract Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;
.super Landroid/widget/LinearLayout;
.source "BuyerActionBarLayout.java"

# interfaces
.implements Lcom/squareup/ui/buyer/actionbar/BuyerActionBarHost;


# instance fields
.field protected buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->setOrientation(I)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 35
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public hideUpButton()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->hideUpButton()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    return-void
.end method

.method public setCallToAction(Ljava/lang/CharSequence;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setCallToAction(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnUpClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setOnUpGlyphClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTicketName(Ljava/lang/String;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTicketName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTotal(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showUpAsBack()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showBackArrow()V

    return-void
.end method
