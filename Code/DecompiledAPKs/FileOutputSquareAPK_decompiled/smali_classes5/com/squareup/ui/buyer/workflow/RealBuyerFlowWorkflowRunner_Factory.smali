.class public final Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealBuyerFlowWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerScopeWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->buyerScopeWorkflowProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
            ">;)",
            "Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;)Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;-><init>(Lcom/squareup/ui/buyer/BuyerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->buyerScopeWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v2, p0, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;)Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner_Factory;->get()Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
