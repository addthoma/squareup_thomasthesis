.class public Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
.super Ljava/lang/Object;
.source "CardholderNameProcessor.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
.implements Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;,
        Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;,
        Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;,
        Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

.field private listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/log/ReaderEventLogger;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;-><init>(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$1;)V

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 79
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 80
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 81
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 82
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    .line 83
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 84
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method private logFailedCardholderFetch()V
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 245
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void

    .line 247
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No active card reader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private logFailedCardholderFetch(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_FAIL:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 253
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 252
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method private resetTransaction()V
    .locals 3

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 356
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 357
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getEmvListener()Lcom/squareup/cardreader/EmvListener;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    return-object v0
.end method

.method public getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    return-object v0
.end method

.method public getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    return-object v0
.end method

.method protected handleHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 2

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->HARDWARE_PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setSecureTouchPinRequestData(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    return-void
.end method

.method protected handleOnCardError()V
    .locals 2

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    .line 119
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->resetTransaction()V

    return-void
.end method

.method protected handleOnCardRemovedDuringPayment()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 126
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    .line 129
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->resetTransaction()V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method

.method protected handleOnCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 5

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 161
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 172
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 173
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    goto :goto_1

    .line 162
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onNameReceived(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    const-string v1, "CardholderNameProcessor::handleOnCardholderNameReceived no active card reader"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 168
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 167
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardholderName(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected handleOnListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setApplications([Lcom/squareup/cardreader/EmvApplication;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    return-void
.end method

.method protected handleOnPaymentDeclined()V
    .locals 2

    .line 187
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    return-void
.end method

.method protected handleOnPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 3

    .line 194
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 202
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    if-eq p1, v0, :cond_1

    .line 203
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 204
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    :cond_1
    return-void
.end method

.method protected handleSendAuthorization([B)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    return-void
.end method

.method protected handleSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 2

    .line 142
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->logFailedCardholderFetch()V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setPinRequestInfo(Lcom/squareup/cardreader/PinRequestData;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setEmvListener(Lcom/squareup/cardreader/EmvListener;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getEmvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    if-ne v0, v1, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetEmvListener()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 100
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    if-ne v0, v1, :cond_1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->emvListener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameListener;

    if-ne v0, v1, :cond_2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPinRequestListener()V

    :cond_2
    const/4 v0, 0x0

    .line 108
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->setListener(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;)V

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 3

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 210
    invoke-virtual {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartContactlessSingleTenderInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    .line 211
    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->setContactlessSingleTenderAuthDataOnTransaction([B)V

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;->onFailure()V

    return-void

    .line 217
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t start contactless single tender. Result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setListener(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->listener:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;

    return-void
.end method

.method public setPaymentCompletionListener()V
    .locals 0

    return-void
.end method

.method public unsetPaymentCompletionListener()V
    .locals 0

    return-void
.end method
