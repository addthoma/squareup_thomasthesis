.class public Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;
.super Ljava/lang/Object;
.source "PinPadDialogScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PinScreenModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method pinPresenter(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 49
    invoke-interface {p3}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object p3

    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$YvANhBkKndTCN0HVw8v5x2CHhlI;->INSTANCE:Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$YvANhBkKndTCN0HVw8v5x2CHhlI;

    .line 50
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p3

    .line 51
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-direct {v0, p1, p3, p2}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lio/reactivex/Observable;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)V

    return-object v0
.end method

.method starGroupPresenter()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 55
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;-><init>()V

    return-object v0
.end method
