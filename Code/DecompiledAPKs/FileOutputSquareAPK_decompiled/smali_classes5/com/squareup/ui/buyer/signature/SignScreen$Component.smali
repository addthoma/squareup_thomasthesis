.class public interface abstract Lcom/squareup/ui/buyer/signature/SignScreen$Component;
.super Ljava/lang/Object;
.source "SignScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/SignScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V
.end method

.method public abstract signCoordinatorFactory()Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;
.end method

.method public abstract signScreenRunner()Lcom/squareup/ui/buyer/signature/SignScreenRunner;
.end method
