.class public Lcom/squareup/ui/buyer/error/PaymentErrorView;
.super Landroid/widget/ScrollView;
.source "PaymentErrorView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private glyph:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinnerMessage:Lcom/squareup/widgets/MessageView;

.field private spinnerTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const-class p2, Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;->inject(Lcom/squareup/ui/buyer/error/PaymentErrorView;)V

    .line 45
    sget p2, Lcom/squareup/checkout/R$layout;->auth_glyph_view:I

    const/4 v0, 0x0

    .line 46
    invoke-static {p1, p2, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 100
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 101
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerTitle:Landroid/widget/TextView;

    .line 102
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    .line 103
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 65
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_TWEEN_Y:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$PaymentErrorView()Lkotlin/Unit;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->presenter:Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->onBackPressed()Z

    .line 54
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->presenter:Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->presenter:Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->dropView(Ljava/lang/Object;)V

    .line 61
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 50
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->bindViews()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/error/-$$Lambda$PaymentErrorView$_1Psloi0AeDzY-1JDuScLIb1Kvw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/error/-$$Lambda$PaymentErrorView$_1Psloi0AeDzY-1JDuScLIb1Kvw;-><init>(Lcom/squareup/ui/buyer/error/PaymentErrorView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->presenter:Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setSpinnerMessageText(Ljava/lang/String;)V
    .locals 2

    .line 87
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 91
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setSpinnerTitleText(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    .line 82
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->spinnerTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 77
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 78
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method
