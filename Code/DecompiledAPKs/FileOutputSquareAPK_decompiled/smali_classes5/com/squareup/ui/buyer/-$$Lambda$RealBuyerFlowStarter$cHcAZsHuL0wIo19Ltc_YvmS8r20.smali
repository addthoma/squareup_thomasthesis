.class public final synthetic Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

.field private final synthetic f$1:Lcom/squareup/ui/buyer/BuyerScope;

.field private final synthetic f$2:Z


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;Lcom/squareup/ui/buyer/BuyerScope;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    iput-object p2, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$1:Lcom/squareup/ui/buyer/BuyerScope;

    iput-boolean p3, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$2:Z

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    iget-object v1, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$1:Lcom/squareup/ui/buyer/BuyerScope;

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$cHcAZsHuL0wIo19Ltc_YvmS8r20;->f$2:Z

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->lambda$startBuyerFlow$3$RealBuyerFlowStarter(Lcom/squareup/ui/buyer/BuyerScope;ZLflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
