.class public final Lcom/squareup/ui/buyer/BuyerFlowTutorialConstantsKt;
.super Ljava/lang/Object;
.source "BuyerFlowTutorialConstants.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "AUTH_SPINNER_SCREEN_SHOWN",
        "",
        "RECEIPT_SCREEN_LEGACY_SHOWN_ALL_DONE",
        "RECEIPT_SCREEN_LEGACY_SHOWN_RECEIPT",
        "SIGN_SCREEN_SHOWN",
        "buyer-flow_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AUTH_SPINNER_SCREEN_SHOWN:Ljava/lang/String; = "Shown AuthSpinnerScreen"

.field public static final RECEIPT_SCREEN_LEGACY_SHOWN_ALL_DONE:Ljava/lang/String; = "Shown Receipt -- All Done"

.field public static final RECEIPT_SCREEN_LEGACY_SHOWN_RECEIPT:Ljava/lang/String; = "Shown ReceiptScreenLegacy"

.field public static final SIGN_SCREEN_SHOWN:Ljava/lang/String; = "Shown SignScreen"
