.class public final Lcom/squareup/ui/inventory/AdjustInventoryScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "AdjustInventoryScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/inventory/AdjustInventoryScope$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;,
        Lcom/squareup/ui/inventory/AdjustInventoryScope$ComponentFactory;,
        Lcom/squareup/ui/inventory/AdjustInventoryScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/inventory/AdjustInventoryScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final currentCost:Lcom/squareup/protos/common/Money;

.field final hasServerStockCount:Z

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field final precision:I

.field final serverCount:Ljava/math/BigDecimal;

.field final unitAbbreviation:Ljava/lang/String;

.field final variationId:Ljava/lang/String;

.field final variationMerchantCatalogToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 93
    sget-object v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryScope$lm1p9kwloR1H8r5HfN9uwBcU6i4;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryScope$lm1p9kwloR1H8r5HfN9uwBcU6i4;

    .line 94
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 36
    iput-boolean p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->hasServerStockCount:Z

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationId:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationMerchantCatalogToken:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->serverCount:Ljava/math/BigDecimal;

    .line 40
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->currentCost:Lcom/squareup/protos/common/Money;

    .line 41
    iput p7, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->precision:I

    .line 42
    iput-object p8, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryScope;
    .locals 14

    .line 95
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 96
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 97
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 98
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 99
    const-class v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/math/BigDecimal;

    .line 100
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 101
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    const/4 v0, 0x0

    .line 103
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    const-wide/high16 v12, -0x8000000000000000L

    cmp-long v1, v10, v12

    if-eqz v1, :cond_1

    .line 105
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/protos/common/CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    invoke-static {v10, v11, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    move-object v7, p0

    goto :goto_1

    :cond_1
    move-object v7, v0

    .line 107
    :goto_1
    new-instance p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/inventory/AdjustInventoryScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    return-object p0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 56
    iget-boolean p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->hasServerStockCount:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationMerchantCatalogToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->serverCount:Ljava/math/BigDecimal;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 60
    iget p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->precision:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->currentCost:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_0

    .line 63
    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 64
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->currentCost:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p2}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    .line 66
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    :goto_0
    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 46
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    .line 47
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    invoke-interface {v0}, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;->adjustInventoryController()Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
