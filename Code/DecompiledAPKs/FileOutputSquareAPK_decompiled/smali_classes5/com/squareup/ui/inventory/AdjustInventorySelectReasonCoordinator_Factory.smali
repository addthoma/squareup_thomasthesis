.class public final Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;
.super Ljava/lang/Object;
.source "AdjustInventorySelectReasonCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final adjustInventoryControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->adjustInventoryControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;)",
            "Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/inventory/AdjustInventoryController;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->adjustInventoryControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-static {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/inventory/AdjustInventoryController;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator_Factory;->get()Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;

    move-result-object v0

    return-object v0
.end method
