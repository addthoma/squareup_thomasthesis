.class final enum Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;
.super Ljava/lang/Enum;
.source "AdjustInventoryController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "InventoryAdjustmentSaveStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

.field public static final enum ERROR:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

.field public static final enum NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

.field public static final enum SAVING:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 566
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    const/4 v1, 0x0

    const-string v2, "NOT_REQUESTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    const/4 v2, 0x1

    const-string v3, "SAVING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->SAVING:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->ERROR:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    .line 565
    sget-object v4, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->SAVING:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->ERROR:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->$VALUES:[Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 565
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;
    .locals 1

    .line 565
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;
    .locals 1

    .line 565
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->$VALUES:[Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-virtual {v0}, [Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    return-object v0
.end method
