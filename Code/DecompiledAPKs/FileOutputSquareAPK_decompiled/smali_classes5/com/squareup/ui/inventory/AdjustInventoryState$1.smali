.class final Lcom/squareup/ui/inventory/AdjustInventoryState$1;
.super Ljava/lang/Object;
.source "AdjustInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/inventory/AdjustInventoryState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryState;
    .locals 12

    .line 297
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 299
    const-class v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/math/BigDecimal;

    .line 300
    const-class v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/math/BigDecimal;

    .line 301
    const-class v0, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/protos/common/Money;

    .line 302
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 303
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 304
    invoke-static {}, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->values()[Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    aget-object v8, v0, v7

    .line 306
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    .line 307
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 309
    new-instance p1, Lcom/squareup/ui/inventory/AdjustInventoryState;

    const/4 v11, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/inventory/AdjustInventoryState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;ILjava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryState$1;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 295
    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/inventory/AdjustInventoryState;
    .locals 0

    .line 314
    new-array p1, p1, [Lcom/squareup/ui/inventory/AdjustInventoryState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 295
    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState$1;->newArray(I)[Lcom/squareup/ui/inventory/AdjustInventoryState;

    move-result-object p1

    return-object p1
.end method
