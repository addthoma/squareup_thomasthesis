.class public Lcom/squareup/ui/inventory/AdjustInventoryState;
.super Ljava/lang/Object;
.source "AdjustInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/inventory/AdjustInventoryState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

.field private precision:I

.field private reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

.field private request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

.field private serverCount:Ljava/math/BigDecimal;

.field private unitAbbreviation:Ljava/lang/String;

.field private userInputCount:Ljava/math/BigDecimal;

.field private userInputUnitCost:Lcom/squareup/protos/common/Money;

.field private variationId:Ljava/lang/String;

.field private variationMerchantCatalogToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 295
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryState$1;

    invoke-direct {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;ILjava/lang/String;)V
    .locals 0

    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    .line 284
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationMerchantCatalogToken:Ljava/lang/String;

    .line 285
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->serverCount:Ljava/math/BigDecimal;

    .line 286
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    .line 287
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    .line 288
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 289
    iput-object p7, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    .line 290
    iput-object p8, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    .line 291
    iput p9, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    .line 292
    iput-object p10, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;ILjava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryState$1;)V
    .locals 0

    .line 84
    invoke-direct/range {p0 .. p10}, Lcom/squareup/ui/inventory/AdjustInventoryState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;ILjava/lang/String;)V

    return-void
.end method

.method static createAdjustInventoryState()Lcom/squareup/ui/inventory/AdjustInventoryState;
    .locals 12

    .line 125
    new-instance v11, Lcom/squareup/ui/inventory/AdjustInventoryState;

    sget-object v8, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->FINISHED:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-string v10, ""

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/inventory/AdjustInventoryState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;ILjava/lang/String;)V

    return-object v11
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method failToAdjustInventory(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SPECIFYING_NUMBER:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    .line 158
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    .line 159
    sget-object p1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->EXPERIENCING_ERROR:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    return-void

    .line 155
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected current phase: SPECIFYING_NUMBER, but actual current phase: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method finishAdjustInventory()V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SPECIFYING_NUMBER:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->EXPERIENCING_ERROR:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SELECTING_REASON:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->RECEIVING_INITIAL_STOCK:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected current phase: SELECTING_REASON, SPECIFYING_NUMBER, EXPERIENCING_ERROR or RECEIVING_INITIAL_STOCK, but actual current phase: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 211
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    .line 212
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationMerchantCatalogToken:Ljava/lang/String;

    .line 213
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->serverCount:Ljava/math/BigDecimal;

    .line 214
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    .line 215
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    .line 216
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 217
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    const/4 v1, 0x0

    .line 218
    iput v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    .line 219
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    .line 220
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->FINISHED:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    return-void
.end method

.method getPhase()Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    return-object v0
.end method

.method getPrecision()I
    .locals 1

    .line 256
    iget v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    return v0
.end method

.method getReason()Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    return-object v0
.end method

.method getRequest()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    return-object v0
.end method

.method getServerCount()Ljava/math/BigDecimal;
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->serverCount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method getUserInputCount()Ljava/math/BigDecimal;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method getUserInputUnitCost()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method getVariationId()Ljava/lang/String;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    return-object v0
.end method

.method getVariationMerchantCatalogToken()Ljava/lang/String;
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationMerchantCatalogToken:Ljava/lang/String;

    return-object v0
.end method

.method receiveInitialStock(Ljava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->FINISHED:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_1

    .line 188
    iget-object v0, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    if-ne v0, v1, :cond_0

    .line 192
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->RECEIVING_INITIAL_STOCK:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    .line 193
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    .line 194
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 195
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    .line 196
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    .line 197
    iput p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    .line 198
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    return-void

    .line 189
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Expected reason: RECEIVED, but actual reason: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 185
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Expected current phase: FINISHED, but actual current phase: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method reselectReason()V
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SPECIFYING_NUMBER:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    .line 167
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SELECTING_REASON:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 169
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    .line 170
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    return-void

    .line 164
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected current phase: SPECIFYING_NUMBER, but actual current phase: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method retryOrDismissAdjustment()V
    .locals 3

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->EXPERIENCING_ERROR:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    .line 178
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SPECIFYING_NUMBER:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    const/4 v0, 0x0

    .line 179
    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    return-void

    .line 175
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected current phase: EXPERIENCING_ERROR, but actual current phase: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method selectReasonToAdjustInventory(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SELECTING_REASON:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    .line 149
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SPECIFYING_NUMBER:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    .line 150
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    return-void

    .line 146
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected current phase: SELECTING_REASON, but actual current phase: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method startAdjustInventory(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;)V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->FINISHED:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    .line 136
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->SELECTING_REASON:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    .line 137
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    .line 138
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationMerchantCatalogToken:Ljava/lang/String;

    .line 139
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->serverCount:Ljava/math/BigDecimal;

    .line 140
    iput p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    .line 141
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    return-void

    .line 133
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Inventory Adjustment cannot be start when phase is "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method updateUserInputCount(Ljava/math/BigDecimal;)Z
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 227
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    const/4 p1, 0x1

    return p1
.end method

.method updateUserInputUnitCost(Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 235
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x1

    return p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->variationMerchantCatalogToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->serverCount:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputCount:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 329
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->phase:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    invoke-virtual {p2}, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 330
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 331
    iget p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->precision:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryState;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
