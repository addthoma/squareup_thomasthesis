.class public Lcom/squareup/ui/DiagnosticCrasher;
.super Ljava/lang/Object;
.source "DiagnosticCrasher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;,
        Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;
    }
.end annotation


# static fields
.field private static final ARBITRARY_PRIME:I = 0x1f

.field private static final CRASH_DELIM:Ljava/lang/String; = "xxx"

.field private static final DELIM_LENGTH:I = 0x3

.field private static final DUMP_DELIM:Ljava/lang/String; = "ddd"

.field private static final MAX_CODE:I = 0xf4240

.field private static final NATIVE_CRASH_DELIM:Ljava/lang/String; = "nnn"

.field private static final PST:Ljava/lang/String; = "GMT-8:00"


# instance fields
.field private final dumper:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;

.field private final remoteLogger:Lcom/squareup/logging/RemoteLogger;

.field private final rpcScheduler:Lrx/Scheduler;

.field private final todaysCode:Ljava/lang/String;

.field private final tomorrowsCode:Ljava/lang/String;

.field private final yesterdaysCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/logging/RemoteLogger;Lrx/Scheduler;)V
    .locals 0
    .param p6    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/logging/RemoteLogger;",
            "Lrx/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/DiagnosticCrasher;->dumper:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/DiagnosticCrasher;->gson:Lcom/google/gson/Gson;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/DiagnosticCrasher;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/DiagnosticCrasher;->rpcScheduler:Lrx/Scheduler;

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/ui/DiagnosticCrasher;->tokenToHash(Ljava/lang/String;)I

    move-result p1

    const/4 p3, -0x1

    if-ne p1, p3, :cond_0

    const/4 p1, 0x0

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/DiagnosticCrasher;->tomorrowsCode:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/DiagnosticCrasher;->yesterdaysCode:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p4, "GMT-8:00"

    .line 60
    invoke-static {p4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object p2

    const/16 p4, 0xb

    const/4 p5, 0x0

    .line 62
    invoke-virtual {p2, p4, p5}, Ljava/util/Calendar;->set(II)V

    const/16 p4, 0xc

    .line 63
    invoke-virtual {p2, p4, p5}, Ljava/util/Calendar;->set(II)V

    const/16 p4, 0xd

    .line 64
    invoke-virtual {p2, p4, p5}, Ljava/util/Calendar;->set(II)V

    const/16 p4, 0xe

    .line 65
    invoke-virtual {p2, p4, p5}, Ljava/util/Calendar;->set(II)V

    .line 67
    invoke-static {p1, p2}, Lcom/squareup/ui/DiagnosticCrasher;->dateHash(ILjava/util/Calendar;)Ljava/lang/String;

    move-result-object p4

    iput-object p4, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    const/4 p4, 0x6

    .line 69
    invoke-virtual {p2, p4, p3}, Ljava/util/Calendar;->add(II)V

    .line 70
    invoke-static {p1, p2}, Lcom/squareup/ui/DiagnosticCrasher;->dateHash(ILjava/util/Calendar;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/DiagnosticCrasher;->yesterdaysCode:Ljava/lang/String;

    const/4 p3, 0x5

    const/4 p4, 0x2

    .line 72
    invoke-virtual {p2, p3, p4}, Ljava/util/Calendar;->add(II)V

    .line 73
    invoke-static {p1, p2}, Lcom/squareup/ui/DiagnosticCrasher;->dateHash(ILjava/util/Calendar;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/DiagnosticCrasher;->tomorrowsCode:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/DiagnosticCrasher;)Lcom/google/gson/Gson;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/DiagnosticCrasher;->gson:Lcom/google/gson/Gson;

    return-object p0
.end method

.method private static dateHash(ILjava/util/Calendar;)Ljava/lang/String;
    .locals 2

    mul-int/lit8 p0, p0, 0x1f

    const/4 v0, 0x5

    .line 164
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr p0, v0

    const v0, 0xf4240

    rem-int/2addr p0, v0

    mul-int/lit8 p0, p0, 0x1f

    const/4 v1, 0x2

    .line 165
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/2addr p0, v1

    rem-int/2addr p0, v0

    mul-int/lit8 p0, p0, 0x1f

    const/4 v1, 0x1

    .line 167
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    rem-int/lit8 p1, p1, 0x64

    add-int/2addr p0, p1

    rem-int/2addr p0, v0

    .line 169
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$logSupportDiagnostics$0(Lcom/squareup/queue/QueueDumper;)Lcom/squareup/queue/QueueDumper$Dump;
    .locals 1

    const-string v0, "support_request"

    .line 113
    invoke-virtual {p0, v0}, Lcom/squareup/queue/QueueDumper;->dump(Ljava/lang/String;)Lcom/squareup/queue/QueueDumper$Dump;

    move-result-object p0

    return-object p0
.end method

.method private tokenToHash(Ljava/lang/String;)I
    .locals 8

    .line 140
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 141
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    .line 142
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v6, 0x61

    if-lt v5, v6, :cond_0

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v7, 0x7a

    if-gt v5, v7, :cond_0

    .line 143
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    :goto_1
    sub-int/2addr v4, v6

    goto :goto_2

    .line 144
    :cond_0
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v6, 0x41

    if-lt v5, v6, :cond_1

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v7, 0x5a

    if-gt v5, v7, :cond_1

    .line 145
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    goto :goto_1

    .line 146
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v6, 0x30

    if-lt v5, v6, :cond_2

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v5

    const/16 v7, 0x39

    if-gt v5, v7, :cond_2

    .line 147
    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v4

    goto :goto_1

    :goto_2
    mul-int/lit8 v3, v3, 0x1f

    add-int/2addr v3, v4

    const v4, 0xf4240

    .line 152
    rem-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "Bad char \'%s\' in token \"%s\", no diagnostics for you!"

    .line 149
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p1, -0x1

    return p1

    :cond_3
    return v3
.end method


# virtual methods
.method public synthetic lambda$logSupportDiagnostics$1$DiagnosticCrasher(Lcom/squareup/queue/QueueDumper$Dump;)Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;
    .locals 1

    .line 115
    new-instance v0, Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;-><init>(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/queue/QueueDumper$Dump;)V

    return-object v0
.end method

.method public synthetic lambda$logSupportDiagnostics$3$DiagnosticCrasher(Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;)Lrx/Completable;
    .locals 1

    .line 116
    new-instance v0, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$g5JC2GuFGG4dBCsACDMf5ex36oM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$g5JC2GuFGG4dBCsACDMf5ex36oM;-><init>(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;)V

    invoke-static {v0}, Lrx/Completable;->fromAction(Lrx/functions/Action0;)Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$DiagnosticCrasher(Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;)V
    .locals 4

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "support_request"

    aput-object v3, v1, v2

    const-string v2, "Diagnostics requested as: \"%s\""

    .line 118
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-interface {v0, p1, v1}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public logSupportDiagnostics()Lrx/Completable;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher;->dumper:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$YQ2kQ9U-wNp-cKCB2K5NPmQa_lw;->INSTANCE:Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$YQ2kQ9U-wNp-cKCB2K5NPmQa_lw;

    .line 113
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/DiagnosticCrasher;->rpcScheduler:Lrx/Scheduler;

    .line 114
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$BTHbP-nPYP25OZuGMAbHgeP8FpM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$BTHbP-nPYP25OZuGMAbHgeP8FpM;-><init>(Lcom/squareup/ui/DiagnosticCrasher;)V

    .line 115
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$OI0hnQimgoKY8wLb8YeVPeD-dQU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$DiagnosticCrasher$OI0hnQimgoKY8wLb8YeVPeD-dQU;-><init>(Lcom/squareup/ui/DiagnosticCrasher;)V

    .line 116
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMapCompletable(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    return-object v0
.end method

.method public maybeCrashOrLog(Ljava/lang/String;)Z
    .locals 7

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 79
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v2, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    if-ge v0, v2, :cond_1

    return v1

    .line 81
    :cond_1
    invoke-static {p1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/LanguageUtils;->wideToNarrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "nnn"

    .line 83
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    const-string/jumbo v3, "xxx"

    .line 84
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_4

    const-string v5, "ddd"

    .line 85
    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    if-nez v2, :cond_5

    if-nez v3, :cond_5

    if-eqz v5, :cond_7

    .line 88
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    sub-int/2addr v5, v6

    invoke-virtual {v0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v5, p0, Lcom/squareup/ui/DiagnosticCrasher;->yesterdaysCode:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/squareup/ui/DiagnosticCrasher;->tomorrowsCode:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 91
    :cond_6
    new-instance v5, Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;

    iget-object v6, p0, Lcom/squareup/ui/DiagnosticCrasher;->dumper:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/queue/QueueDumper;

    invoke-virtual {v6, v0}, Lcom/squareup/queue/QueueDumper;->dump(Ljava/lang/String;)Lcom/squareup/queue/QueueDumper$Dump;

    move-result-object v0

    invoke-direct {v5, p0, v0}, Lcom/squareup/ui/DiagnosticCrasher$Diagnostics;-><init>(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/queue/QueueDumper$Dump;)V

    if-eqz v2, :cond_8

    .line 93
    invoke-static {}, Lcom/squareup/crashnado/CrashnadoNative;->crash()V

    :cond_7
    return v1

    :cond_8
    if-nez v3, :cond_9

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v1

    const-string p1, "Diagnostics requested as: \"%s\""

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v5, p1}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return v4

    .line 95
    :cond_9
    throw v5
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiagnosticCrasher{yesterdaysCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/DiagnosticCrasher;->yesterdaysCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", todaysCode=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/DiagnosticCrasher;->todaysCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", tomorrowsCode=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/DiagnosticCrasher;->tomorrowsCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
